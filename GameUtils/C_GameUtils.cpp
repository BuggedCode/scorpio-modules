/*
* filename:			C_GameUtils.cpp
*
* author:			Kral Jozef
* date:				04/12/2010		21:49
* version:			1.00
* brief:
*/

#include "C_GameUtils.h"
#include <GameUtils/Others/C_ConvertUtils.h>


namespace scorpio{ namespace gameutils{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_GameUtils::Initialize(const S_InitParams & initParams)
	{
		T_ConvertUtils::CreateInstance();
		T_ConvertUtils::GetInstance().SetConversionParams(initParams.m_screenWidth, initParams.m_screenHeight, initParams.fRatio);
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Deinitialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_GameUtils::Deinitialize()
	{
		T_ConvertUtils::DestroyInstance();
		return true;
	}


}}