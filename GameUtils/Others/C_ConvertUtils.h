/*
* filename:			C_ConvertUtils.h
*
* author:			Kral Jozef
* date:				04/12/2010		21:38
* version:			1.00
* brief:				Convert from absolute screen coord to physic coord and back
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include <Common/C_Singleton.h>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Point.h>

namespace scorpio{ namespace gameutils{

	namespace scmath = scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ C_ConvertUtils
	class GAMEUTILS_API C_ConvertUtils
	{
		friend class C_Singleton<C_ConvertUtils>;
	public:
		//@ CreateInstance
		//@ fRatio - multiplicator for scaling range from screen to physics, mostly < 1.f
		void						SetConversionParams(u16 screenWidth, u16 screenHeight, float fRatio);


		//@ ConvertToPhysics
		scmath::C_Vector2		ConvertPosToPhysics(const scmath::C_Point & absScreenPos) const;
		//@ ConvertToPhysics
		scmath::C_Vector2		ConvertPosToPhysics(const scmath::C_Vector2 & absScreenPos) const;
		//@ ConvertToScreen
		scmath::C_Vector2		ConvertPosToScreen(const scmath::C_Vector2 & absPhysPos) const;
		//@ ConvertSizeToPhysics
		scmath::C_Vector2		ConvertSizeToPhysics(const scmath::C_Point & size) const;
		//@ ConvertSizeToPhysics
		float						ConvertSizeToPhysics(u32 nSize) const;


	private:
		//@ c-tor
		C_ConvertUtils() {};
		//@ d-tor
		~C_ConvertUtils() {};

	private:
		u16		m_screenWidth;
		u16		m_screenHeight;
		float		m_fRatio;	//multiplicator for extending/descending range of values into physics engine, some physics doesn't work gut with small/big ranges
	};


	typedef C_Singleton<C_ConvertUtils>	T_ConvertUtils;
}}

GAMEUTILS_EXP_TEMP_INST template class GAMEUTILS_API C_Singleton<scorpio::gameutils::C_ConvertUtils>;