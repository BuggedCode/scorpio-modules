/*
* filename:			C_Trigger.cpp
*
* author:			Kral Jozef
* date:				10/22/2010		0:32
* version:			1.00
* brief:
*/

#include "C_Trigger.h"

namespace scorpio{ namespace gameutils{

	using namespace sysmath;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsInTrigger
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Trigger::IsInTrigger(const sysmath::C_Vector2 & vctPos) const
	{
		float fDist = C_Vector2(vctPos - m_pivot).length2();
		if (fDist < m_radius2)
			return true;

		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsInTrigger
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Trigger::IsInTrigger(const sysmath::C_Point & pointPos) const
	{
		float fDist = C_Vector2(pointPos.x - m_pivot.m_x, pointPos.y - m_pivot.m_y).length2();
		if (fDist < m_radius2)
			return true;

		return false;
	}
}}