/*
* filename:			C_ConvertUtils.cpp
*
* author:			Kral Jozef
* date:				04/12/2010		21:39
* version:			1.00
* brief:
*/

#include "C_ConvertUtils.h"

namespace scorpio{ namespace gameutils{

	using namespace scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetConversionParams
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ConvertUtils::SetConversionParams(u16 screenWidth, u16 screenHeight, float fRatio)
	{
		m_screenWidth = screenWidth;
		m_screenHeight = screenHeight;
		m_fRatio = fRatio;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ConvertToPhysics
	//
	//////////////////////////////////////////////////////////////////////////
	C_Vector2 C_ConvertUtils::ConvertPosToPhysics(const C_Point & absScreenPos) const
	{
		return C_Vector2(absScreenPos.x * m_fRatio, (m_screenHeight - absScreenPos.y) * m_fRatio);
	}

	C_Vector2 C_ConvertUtils::ConvertPosToPhysics(const C_Vector2 & absScreenPos) const
	{
		return C_Vector2(absScreenPos.m_x * m_fRatio, (m_screenHeight - absScreenPos.m_y) * m_fRatio);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ConvertToScreen
	//
	//////////////////////////////////////////////////////////////////////////
	C_Vector2 C_ConvertUtils::ConvertPosToScreen(const C_Vector2 & absPhysPos) const
	{
		return C_Vector2(absPhysPos.m_x/m_fRatio, m_screenHeight - (absPhysPos.m_y / m_fRatio));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ConvertSizeToPhysics
	//
	//////////////////////////////////////////////////////////////////////////
	C_Vector2 C_ConvertUtils::ConvertSizeToPhysics(const scmath::C_Point & size) const
	{
		return C_Vector2(size.x * m_fRatio, size.y * m_fRatio);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ConvertSizeToPhysics
	//
	//////////////////////////////////////////////////////////////////////////
	float C_ConvertUtils::ConvertSizeToPhysics(u32 nSize) const
	{
		return nSize * m_fRatio;
	}


}}