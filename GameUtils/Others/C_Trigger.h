/*
* filename:			C_Trigger.h
*
* author:			Kral Jozef
* date:				10/22/2010		0:27
* version:			1.00
* brief:
*/

#pragma once

#include <SysMath/C_Point.h>
#include <SysMath/C_Vector2.h>
#include <GameUtils/GameUtilsApi.h>
#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace gameutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Trigger
	class GAMEUTILS_API C_Trigger
	{
	public:
		enum E_TriggerType
		{
			E_TT_CIRCLE,
		};

	public:
		//@ c-tor
		C_Trigger(float fRadius, const sysmath::C_Vector2 & vctPos)
			: m_type(E_TT_CIRCLE), m_pivot(vctPos), m_radius(fRadius), m_radius2(fRadius*fRadius) {};

		//@ c-tor
		C_Trigger(float fRadius, const sysmath::C_Point & pointPos)
			: m_type(E_TT_CIRCLE), m_pivot((float)pointPos.x, (float)pointPos.y), m_radius(fRadius), m_radius2(fRadius*fRadius) {};
		//@ d-tor
		virtual ~C_Trigger() {};

		//@ SetPosition
		void					SetPosition(const sysmath::C_Vector2 & vctPos) { m_pivot = vctPos; }
		//@ SetPosition
		void					SetPosition(const sysmath::C_Point & pointPos) { m_pivot.m_x = float(pointPos.x); m_pivot.m_y = float(pointPos.y); }
		//@ SetRadius
		void					SetRadius(float fRadius) { m_radius = fRadius; m_radius2 = fRadius * fRadius; }
		//@ IsInTrigger
		bool					IsInTrigger(const sysmath::C_Vector2 & vctPos) const;
		//@ IsInTrigger
		bool					IsInTrigger(const sysmath::C_Point & pointPos) const;

	private:
		E_TriggerType			m_type;
		sysmath::C_Vector2	m_pivot;	//position of pivot
		float						m_radius;
		float						m_radius2;
	};

	typedef boost::shared_ptr<C_Trigger>	T_Trigger;

}}