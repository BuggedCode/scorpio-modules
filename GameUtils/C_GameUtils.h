/*
* filename:			C_GameUtils.h
*
* author:			Kral Jozef
* date:				04/12/2010		21:48
* version:			1.00
* brief:
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include <Common/types.h>

namespace scorpio{ namespace gameutils{

	struct S_InitParams
	{
		u16	m_screenWidth;
		u16	m_screenHeight;
		float fRatio;
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_GameFramework
	class GAMEUTILS_API C_GameUtils
	{
	public:
		//@ Initialize module
		static bool			Initialize(const S_InitParams & initParams);

		//@ Deinitialize module
		static bool			Deinitialize();
	};
}}