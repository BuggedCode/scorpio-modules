/*
* filename:			C_VisualObjective.h
*
* author:			Kral Jozef
* date:				10/12/2011		21:54
* version:			1.00
* brief:
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include <boost/shared_ptr.hpp>
#include <SysUtils/C_String.h>
#include <SysCore/Frames/C_BitmapText.h>
#include <SysAudio/C_Sound.h>
#include <SysMath/C_Vector4.h>
#include <SysUtils/C_ContextScheduler.h>
#include <GameFramework/Operators/C_FadeOperator.h>


namespace scorpio{ namespace gameutils{

	namespace scmath = scorpio::sysmath;
	namespace scutils = scorpio::sysutils;
	namespace scaudio = scorpio::sysaudio;
	namespace sccore = scorpio::syscore;
	namespace scgutils = scorpio::gameutils;
	namespace scframework = scorpio::framework;


	//////////////////////////////////////////////////////////////////////////
	struct S_VisualObjectiveDescriptor
	{
	public:
		u16						m_Delay;
		float 					m_FontScale;
		sccore::T_BitmapFont	m_Font;
		scmath::C_Vector4		m_FontColor;
		scaudio::T_Sound		m_Sound;
		scmath::C_Vector2		m_InitPos;	//position where to place BitmapText
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_GraphicObjective
	class GAMEUTILS_API C_VisualObjective
	{
	public:
		//@ c-tor
		C_VisualObjective(const T_String & strText, const S_VisualObjectiveDescriptor & desc, u32 renderLayer);
		//@ IsDone
		bool							IsDone() const { return m_bIsDone; }
		//@ Update
		void							Update(u32 inDeltaTime);
		//@ MoveBy
		void							MoveBy(const scmath::C_Vector2 & vctOffset);

		//@ OnNewCharacter
		void							OnNewCharacter();
		//@ StartFadeOut
		void							StartFadeOut();

	private:
		u16									m_Delay;
		u16									m_CurrCharCount;	//current number of displayed characters from whole text
		bool									m_bIsDone;	//is visual repre finished (whole text is displayed/animation finished...)
		T_String								m_StrText;
		syscore::T_BitmapText			m_BitmapText;
		scaudio::T_Sound					m_CharacterSound;
		
		typedef scutils::C_ContextScheduler<C_VisualObjective>	T_Scheduler;
		T_Scheduler							m_CharacterScheduler;
		scframework::T_FadeOperator	m_FadeOutOp;
	};


	typedef boost::shared_ptr<C_VisualObjective>	T_VisualObjective;

}}