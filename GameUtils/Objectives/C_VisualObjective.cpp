/*
* filename:			C_VisualObjective.cpp
*
* author:			Kral Jozef
* date:				10/12/2011		21:54
* version:			1.00
* brief:
*/

#include "C_VisualObjective.h"
#include <SysCore/Core/C_RenderManager.h>

#pragma warning(push)
#pragma warning(disable:4355)	//suppres warning for using this in initializer list		

namespace scorpio{ namespace gameutils{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::framework;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_VisualObjective::C_VisualObjective(const T_String & strText, const S_VisualObjectiveDescriptor & desc, u32 renderLayer)
		: m_bIsDone(false), m_StrText(strText), m_CurrCharCount(0), m_CharacterScheduler(*this)
	{
		m_BitmapText = syscore::T_BitmapText(new syscore::C_BitmapText("", desc.m_Font, syscore::E_TT_CZSK));
		m_BitmapText->SetScale(desc.m_FontScale);
		m_BitmapText->SetPosition(desc.m_InitPos);
		m_BitmapText->SetColor(desc.m_FontColor);

		m_CharacterSound = desc.m_Sound;

		syscore::T_RenderManager::GetInstance().Add(m_BitmapText, renderLayer);

		m_Delay = desc.m_Delay;

		m_CharacterScheduler.Schedule(&C_VisualObjective::OnNewCharacter, m_Delay);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VisualObjective::Update(u32 inDeltaTime)
	{
		m_CharacterScheduler.Update(inDeltaTime);

		if (m_FadeOutOp)
		{
			m_FadeOutOp->Update(inDeltaTime);
			if (!m_FadeOutOp->IsActive())
			{
				//@ fadeout done
				m_bIsDone = true;
			}
		}

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ OnNewCharacter
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VisualObjective::OnNewCharacter()
	{
		T_String strText = T_String::SubString(m_StrText, 0, m_CurrCharCount);
		m_BitmapText->SetText(strText);
		
		if (m_CharacterSound)
			m_CharacterSound->Play();

		++m_CurrCharCount;

		if (m_CurrCharCount < m_StrText.length())
			m_CharacterScheduler.Schedule(&C_VisualObjective::OnNewCharacter, m_Delay);
		else
			m_CharacterScheduler.Schedule(&C_VisualObjective::StartFadeOut, 6000);	//4sec

	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ StartFadeOut
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VisualObjective::StartFadeOut()
	{
		//m_StrText
		m_FadeOutOp = T_FadeOperator(new C_FadeOperator(m_BitmapText->GetColor().m_w, NULL, -0.002f));
		m_FadeOutOp->Activate();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ MoveBy
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VisualObjective::MoveBy(const scmath::C_Vector2 & vctOffset)
	{
		C_Vector2 vctPos = m_BitmapText->GetPosition();
		vctPos += vctOffset;
		m_BitmapText->SetPosition(vctPos);
	}


}}

#pragma warning(pop)