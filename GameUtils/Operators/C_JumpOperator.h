/*
* filename:			C_JumpOperator.h
*
* author:			Kral Jozef
* date:				10/23/2010		21:25
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/Operators/I_BaseOperator.h>
#include <SysCore/Frames/C_Frame.h>

namespace scorpio{ namespace gameutils{

#define D_SPEED_FACTOR 0.7f;

	//////////////////////////////////////////////////////////////////////////
	//@ C_JumpOperator
	class C_JumpOperator : public framework::I_OffsetOperator<float, float>
	{
	public:
		//@ c-tor
		C_JumpOperator(syscore::T_Frame frame, framework::I_OperatorListener * inListener, float inOffset, float fVelocity)
			: I_OffsetOperator(m_posY, inListener, inOffset)
		{
			m_fVelocity = fVelocity;
			m_trajectLen = 0;
			m_posY = frame->GetPosition().m_y;
			m_frame = frame;
			m_jumpUp = true;
		};


		//////////////////////////////////////////////////////////////////////////
		//@ Update
		virtual void			Update(u32 inDeltaTime)
		{
			if (m_active)
			{
				//s = v.t
				float s = m_fVelocity * inDeltaTime;

				//if (m_jumpUp)
				{
					if (fabs(m_trajectLen + s) > m_offset)
					{
						s = fabs(m_trajectLen) - m_offset;

						if (m_jumpUp == false)
						{
							m_active = false;
							if (m_listener)
								m_listener->NotifyOperatorFinish(this);
							s *= -1;
						}

						
						
						//@ change dir
						m_fVelocity *= -1;
						m_fVelocity *= D_SPEED_FACTOR;
						m_jumpUp = false;
						m_trajectLen = 0;
					}
					else
					{
						m_trajectLen += s;
					}
					
					m_variable += s;
				}

				const sysmath::C_Vector2 & vctPos = m_frame->GetPosition();
				m_frame->SetPosition(sysmath::C_Vector2(vctPos.m_x, m_variable));
			}
		}

	private:
		float			m_fVelocity;
		float			m_trajectLen;
		float			m_posY;
		syscore::T_Frame m_frame;
		bool			m_jumpUp;	//flag ktorym smero sa skace
	};

}}