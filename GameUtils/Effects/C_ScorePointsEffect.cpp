/*
* filename:			C_ScorePointsEffect.cpp
*
* author:			Kral Jozef
* date:				09/17/2010		20:56
* version:			1.00
* brief:
*/

#include "C_ScorePointsEffect.h"
#include <SysMemManager/globalNewDelete.h>
#include <SysCore/Core/C_RenderManager.h>
#include <GameFramework/Operators/C_FadeOperator.h>
#include <SysCore/Frames/C_TextVisual.h>
#include <GameFramework/Operators/RectangleOperators.h>

namespace scorpio{ namespace gameutils{

	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;
	using namespace scorpio::framework;

	static const u32 C_OFFSET_SIZE = 20;	//do konfiguraku!

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_ScorePointsEffect::C_ScorePointsEffect(sccore::T_BitmapText text, const scmath::C_Vector2 & startPos, const scmath::C_Vector2 & endPos, float speed, u32 renderLayer)
	{
		m_bitmapText = text;
		const C_Point & size = m_bitmapText->GetSize();

		//@ startPos is center of entity, need set to center of textVisual
		m_bitmapText->SetPosition(startPos - C_Vector2(size.x/2.f, size.y/2.f));

		m_movingEffect = new C_PointOnTrajectory(false);

		std::vector<scmath::C_Vector2> tmpVec;
		tmpVec.push_back(startPos);
		tmpVec.push_back(endPos);
		m_movingEffect->Initialize(tmpVec);
		m_movingEffect->SetSpeed(speed);

		T_RenderManager::GetInstance().Add(m_bitmapText, renderLayer);

		//@spocitanie rychlosti(stepu fadovania)
		C_Vector2 vctDist = startPos - endPos;
		float s = vctDist.length();
		float deltaTime = s / speed;

		m_blendOp = T_Operator(new C_FadeOperator(m_bitmapText->GetColor().m_w, NULL, 1.f / -deltaTime));
		m_blendOp->Activate();
		
		m_fScale = m_bitmapText->GetScale();
		m_scaleOp = T_Operator(new C_FadeOperator(m_fScale, NULL, 1.f / (8*deltaTime)));	//scale sa zvacsi v celom intervale o 1/8
		m_scaleOp->Activate();

/*#ifdef _DEBUG
		T_TextVisual textVis = boost::dynamic_pointer_cast<C_TextVisual>(m_bitmapText);
		SC_ASSERT(textVis->IsDrawingFromLeft());
#endif*/
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_ScorePointsEffect::~C_ScorePointsEffect()
	{
		SC_SAFE_DELETE(m_movingEffect);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Tick
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ScorePointsEffect::Tick(u32 inDeltaTime)
	{
		if (m_movingEffect->IsDone())
			return;

		m_movingEffect->Tick(inDeltaTime);

		const C_Vector2 & effPos = m_movingEffect->GetCurrentPos();

		m_scaleOp->Update(inDeltaTime);
		m_bitmapText->SetScale(m_fScale);

		//update effect position
		const C_Point & size = m_bitmapText->GetSize();
		m_bitmapText->SetPosition(C_Vector2(effPos.m_x - size.x/2, effPos.m_y - size.y/2));
		
		m_blendOp->Update(inDeltaTime);
	}

}}