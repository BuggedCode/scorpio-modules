/*
* filename:			C_FloatingEffect.cpp
*
* author:			Kral Jozef
* date:				09/14/2010		22:13
* version:			1.00
* brief:
*/

#include "C_FloatingEffect.h"
#include <SysMemManager/globalNewDelete.h>
#include <SysCore/Core/C_RenderManager.h>

#include <SysMath/C_Spline2d.h>

namespace scorpio{ namespace gameutils{

	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;
	using namespace scorpio::effects;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_FloatingEffect::C_FloatingEffect(T_Effect fgxEffect, u32 renderLayer, const C_Vector2 & startPos, const C_Vector2 & endPos, float speed)
		: m_Active(false), m_bDeactivating(false), m_bDone(false), m_TrigerEnabled(false), m_RenderLayer(renderLayer)
	{
		m_gfxEffect = fgxEffect;
		m_gfxEffect->SetPosition(startPos);
		m_gfxEffect->Deactivate(true);

		m_movingEffect = new C_PointOnTrajectory(false);

		std::vector<scmath::C_Vector2> tmpVec;
		tmpVec.push_back(startPos);
		tmpVec.push_back(endPos);

		m_movingEffect->Initialize(tmpVec);
		m_movingEffect->SetSpeed(speed);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_FloatingEffect::C_FloatingEffect(T_Effect fgxEffect, u32 renderLayer, const C_Spline2d & spline, float speed)
		: m_Active(false), m_bDeactivating(false), m_bDone(false), m_RenderLayer(renderLayer)
	{
		m_movingEffect = new C_PointOnTrajectory(spline);
		m_movingEffect->SetSpeed(speed);

		C_Vector2 effPos = m_movingEffect->GetCurrentPos();
		effPos += m_pivot;
		m_gfxEffect = fgxEffect;
		m_gfxEffect->SetPosition(effPos);
		m_gfxEffect->Deactivate(true);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_FloatingEffect::C_FloatingEffect(T_Effect gfxEffect, u32 renderLayer, const sccore::T_PolyShape polyShape, float speed)
		: m_Active(false), m_bDeactivating(false), m_bDone(false), m_RenderLayer(renderLayer)
	{
		m_pivot = polyShape->GetPosition();
		m_movingEffect = new C_PointOnTrajectory(polyShape, speed);	//TRAJECTORY HAS LOCALE POSITION RELATIVE TO PIVOT!
		m_movingEffect->SetSpeed(speed);

		C_Vector2 effPos = m_movingEffect->GetCurrentPos();
		effPos += m_pivot;	//convert to world
		m_gfxEffect = gfxEffect;
		m_gfxEffect->SetPosition(effPos);
		m_gfxEffect->Deactivate(true);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_FloatingEffect::~C_FloatingEffect()
	{
		SC_SAFE_DELETE(m_movingEffect);
		m_gfxEffect->Deactivate(false);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetActive
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FloatingEffect::Activate(bool bActive)
	{
		if (bActive && !m_Active)
		{
			m_bDone = false;
			m_bDeactivating = false;
			m_movingEffect->Reset();
			m_gfxEffect->Activate();
			T_RenderManager::GetInstance().Add(m_gfxEffect, m_RenderLayer);
		}
		else if (!bActive && m_Active)
		{
			m_gfxEffect->Deactivate(true);	//strong
			T_RenderManager::GetInstance().Remove(m_gfxEffect, m_RenderLayer);
		}

		m_Active = bActive;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Tick
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FloatingEffect::Tick(u32 inDeltaTime)
	{
		if (!m_Active)
			return;

		if (m_bDone)
			return;

		if (!m_bDeactivating)
		{
			if (m_movingEffect->IsDone())
			{
				m_gfxEffect->Deactivate(false);
				m_bDeactivating = true;	//efekt je v stave deaktivacie
			}
			else
			{
				m_movingEffect->Tick(inDeltaTime);
			}
		}

		//update effect position
		C_Vector2 effPos = m_movingEffect->GetCurrentPos();
		effPos += m_pivot;
		m_gfxEffect->SetPosition(effPos);
		m_gfxEffect->Update(inDeltaTime);

		if (!m_gfxEffect->HasParticles() && m_bDeactivating)	//if effect start emits particles later and we do not check m_bDeactivating, it will stop before it start
		{
			m_bDone = true;
			//if effect is done -> it means itis deactivated!
			m_Active = false;
			T_RenderManager::GetInstance().Remove(m_gfxEffect, m_RenderLayer);
		}


#if defined NO_FINAL || !defined MASTER
		m_movingEffect->DebugDraw(m_pivot);
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsInTrigger
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FloatingEffect::IsInTrigger() const
	{
		if (m_TrigerEnabled)
			return m_Trigger->IsInTrigger(m_gfxEffect->GetPosition());

		return false;
	}

}}