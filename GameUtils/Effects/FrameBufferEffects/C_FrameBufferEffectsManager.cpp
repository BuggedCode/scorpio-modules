/*
* filename:			C_FrameBufferEffectsManager.cpp
*
* author:			Kral Jozef
* date:				11/1/2011		13:56
* version:			1.00
* brief:
*/

#include "C_FrameBufferEffectsManager.h"
#include "C_ShockWaveEffect.h"
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Managers/C_FrameBufferManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace gameutils{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysrender;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_FrameBufferEffectsManager::~C_FrameBufferEffectsManager()
	{
		m_Container.clear();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FrameBufferEffectsManager::Initialize(u32 bufferWidth, u32 bufferHeight)
	{
		m_FrameBuffer = T_FrameBufferManager::GetInstance().GetRenderTarget(E_RT_MAIN);
		m_BufferWidth = bufferWidth;
		m_BufferHeight = bufferHeight;
		
		C_ShockWaveEffect * effect = new C_ShockWaveEffect(m_BufferWidth, m_BufferHeight, 20, 20);
		m_Container.insert(T_FBEffectContainer::value_type(E_FBET_SHOCKWAVE, T_FrameBufferEffect(effect)));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetCurrentEffect
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FrameBufferEffectsManager::SetCurrent(T_FrameBufferEffect effect)
	{
		if (!effect)
		{
			m_CurrentEffect = effect;
			m_FrameBuffer->ClearEffect();
			return;
		}

		SC_ASSERT(effect->IsActive());
		m_CurrentEffect = effect;
		m_FrameBuffer->SetEffect(m_CurrentEffect);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetEffect
	//
	//////////////////////////////////////////////////////////////////////////
	T_FrameBufferEffect C_FrameBufferEffectsManager::GetEffect(E_FrameBufferEffectType type)
	{
		T_FBEffectContainer::iterator iter = m_Container.find(type);
		if (iter != m_Container.end())
			return iter->second;

		TRACE_FE("Unknown type of FrameBufferEffect: %d", (u32)type);
		return T_FrameBufferEffect();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FrameBufferEffectsManager::Update(u32 inDeltaTime)
	{
		if (!m_CurrentEffect)
			return;

		if (!m_CurrentEffect->IsActive())
		{
			m_FrameBuffer->ClearEffect();
			m_CurrentEffect.reset();
			return;
		}

		m_CurrentEffect->Update(inDeltaTime);
	}
}}
