/*
* filename:			C_ShockWaveEffect.cpp
*
* author:			Kral Jozef
* date:				10/25/2011		0:22
* version:			1.00
* brief:
*/

#include "C_ShockWaveEffect.h"
#include <SysMath/MathUtils.h>
#include <SysCore/Managers/C_ShaderManager.h>
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace gameutils{

	using namespace sysrender;
	using namespace scorpio::sysmath;
	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_ShockWaveEffect::C_ShockWaveEffect(u32 gridWidth, u32 gridHeight, u8 xCells, u8 yCells)
		: m_Grid(NULL), m_TimeAccumulator(0)
	{
		m_Grid = new C_GridMesh(gridWidth, gridHeight, xCells, yCells);
		m_GridWidth = gridWidth;
		m_GridHeight = gridHeight;

		m_Shader = T_ShaderManager::GetInstance().GetShader(C_HashName("SHOCK_WAVE"));
		if (!m_Shader)
		{
			TRACE_E("Can not initialize shader for ShockWave effect");
			m_bActive = false;
			return;
		}
		m_WavePositionUni = m_Shader->CreateUniformVariable("wavePosition");
		m_ShockParamsUni = m_Shader->CreateUniformVariable("shockParams");
		m_TimeUni = m_Shader->CreateUniformVariable("time");
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_ShockWaveEffect::~C_ShockWaveEffect()
	{
		SC_SAFE_DELETE(m_Grid);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Activate
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ShockWaveEffect::Activate(const C_Vector2 & vctPos)
	{
		AddRainDrop(vctPos);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddRainDrop
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ShockWaveEffect::AddRainDrop(const C_Vector2 & vctPos)
	{
		m_RainDropPos.m_x = vctPos.m_x / m_GridWidth;
		m_RainDropPos.m_y = 1.f - vctPos.m_y / m_GridHeight;

		m_ShockParams = C_Vector(10.0f, 0.4f, 0.1f);
		m_TimeAccumulator = 0;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ShockWaveEffect::Update(u32 inDeltaTime)
	{
		if (!m_bActive)
			return;

		m_TimeAccumulator += inDeltaTime;

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ShockWaveEffect::Render(T_Texture fboTexture)
	{
		m_Shader->Bind();
		m_Shader->BindUniform(m_WavePositionUni, m_RainDropPos);
		m_Shader->BindUniform(m_ShockParamsUni, m_ShockParams);
		m_Shader->BindUniform(m_TimeUni, m_TimeAccumulator * 0.001f);

		m_Grid->Render(fboTexture);
		m_Shader->Unbind();
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Bind
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ShockWaveEffect::Bind()
	{
		m_Shader->Bind();
		m_Shader->BindUniform(m_WavePositionUni, m_RainDropPos);
		m_Shader->BindUniform(m_ShockParamsUni, m_ShockParams);
		m_Shader->BindUniform(m_TimeUni, m_TimeAccumulator * 0.001f);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Unbind
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ShockWaveEffect::Unbind()
	{
		m_Shader->Unbind();
	}
}}
