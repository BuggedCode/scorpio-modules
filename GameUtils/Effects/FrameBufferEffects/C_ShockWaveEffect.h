/*
* filename:			C_ShockWaveEffect.h
*
* author:			Kral Jozef
* date:				10/25/2011		0:18
* version:			1.00
* brief:
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include <SysCore/Render/I_FrameBufferEffect.h>
#include <SysCore/Render/C_GridMesh.h>
#include <SysCore/Render/C_Shader.h>
#include <SysMath/C_Vector.h>

namespace scorpio{ namespace gameutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ShockWaveEffect
	class GAMEUTILS_API C_ShockWaveEffect : public sysrender::I_FrameBufferEffect
	{
	public:
		//@ c-tor
		//@ xCells / yCells - count of cells
		C_ShockWaveEffect(u32 gridWidth, u32 gridHeight, u8 xCells, u8 yCells);
		//@ d-tor
		~C_ShockWaveEffect();

		//@ Bind
		virtual void		Bind();
		//@ Unbind
		virtual void		Unbind();

		//@ Activate
		virtual void		Activate(const sysmath::C_Vector2 & vctPos);

		//@ Render
		virtual void		Render(sysrender::T_Texture fboTexture);

		//@ Update
		virtual void		Update(u32 inDeltaTime);
		//@ SetGridDebugDraw
		void					SetGridDebugDraw(bool bEnable) { m_Grid->SetDebugDraw(bEnable); }

	private:
		//@ AddRainDrop
		void					AddRainDrop(const sysmath::C_Vector2 & vctPos);

	private:
		sysrender::C_GridMesh*	m_Grid;
		u32							m_GridWidth;
		u32							m_GridHeight;

		sysrender::T_Shader		m_Shader;
		sysmath::C_Vector2		m_RainDropPos;
		s32							m_WavePositionUni;	//uniform for shader
		sysmath::C_Vector			m_ShockParams;
		s32							m_ShockParamsUni;		//uniform for shader
		u32							m_TimeAccumulator;
		s32							m_TimeUni;				//uniform for shader
	};
}}
