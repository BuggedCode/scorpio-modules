/*
* filename:			C_FrameBufferEffectsManager.h
*
* author:			Kral Jozef
* date:				11/1/2011		13:52
* version:			1.00
* brief:
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include <Common/C_Singleton.h>
#include <SysCore/Render/I_FrameBufferEffect.h>
#include <map>
#include <SysCore/Render/C_FrameBuffer.h>

namespace scorpio{ namespace gameutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_FrameBufferEffectsManager
	class GAMEUTILS_API C_FrameBufferEffectsManager
	{
		friend class C_Singleton<C_FrameBufferEffectsManager>;

	public:
		enum E_FrameBufferEffectType
		{
			E_FBET_SHOCKWAVE = 1,
		};

	public:
		//@ Initialize
		void										Initialize(u32 bufferWidth, u32 bufferHeight);
		//@ SetEffect
		void										SetCurrent(sysrender::T_FrameBufferEffect effect);
		//@ GetEffect
		sysrender::T_FrameBufferEffect	GetEffect(E_FrameBufferEffectType type);
		//@ Update
		void										Update(u32 inDeltaTime);
		//@ ClearCurrent
		void										ClearCurrent() { SetCurrent(sysrender::T_FrameBufferEffect()); }

	private:
		//@ c-tor
		C_FrameBufferEffectsManager()
			: m_BufferWidth(800), m_BufferHeight(600) {};
		//@ d-tor
		~C_FrameBufferEffectsManager();

	private:
		u32						m_BufferWidth;
		u32						m_BufferHeight;
		typedef std::map<E_FrameBufferEffectType, sysrender::T_FrameBufferEffect>	T_FBEffectContainer;
		T_FBEffectContainer	m_Container;

		sysrender::T_FrameBufferEffect	m_CurrentEffect;
		sysrender::T_FrameBuffer			m_FrameBuffer;
	};


	typedef C_Singleton<C_FrameBufferEffectsManager>	T_FrameBufferEffectsManager;
}}

GAMEUTILS_EXP_TEMP_INST template class GAMEUTILS_API C_Singleton<scorpio::gameutils::C_FrameBufferEffectsManager>;