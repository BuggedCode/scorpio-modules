/*
* filename:			C_PointOnTrajectory.h
*
* author:			Kral Jozef
* date:				07/07/2009		21:57
* version:			1.00
* brief:				Movable point on trajectory in time
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Point.h>
#include <vector>
#include <SysCore/Geometry/C_PolyShape.h>

namespace scorpio{
	namespace sysmath{
		class C_Spline2d;
	}
}

namespace scorpio{ namespace gameutils{

	namespace scmath = scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ C_PointOnTrajectory
	class GAMEUTILS_API C_PointOnTrajectory
	{
		typedef std::vector<scmath::C_Vector2>	T_PositionContainer;
	public:
		//@ c-tor
		C_PointOnTrajectory(const syscore::T_PolyShape polyShape, float fSpeed);
		//@ c-tor
		C_PointOnTrajectory(const scmath::C_Spline2d & spline);
		//@ c-tor
		C_PointOnTrajectory(bool bClosed);
		//@ d-tor
		~C_PointOnTrajectory() {};

		//@ Initialize
		void							Initialize(const std::vector<scmath::C_Vector2> & inPositions);
		//@ Tick
		//@ param inDeltaTime - different time between last and actual called update in ms
		void							Tick(u32 inDeltaTime);

		//////////////////////////////////////////////////////////////////////////
		//@ SET/GET FUNCTIONS

		//@ SetCurrentPos
		void							SetCurrentPos(const scmath::C_Vector2 & currPos) { m_currPos = currPos; }
		//@ GetCurrentPos
		const scmath::C_Vector2	GetCurrentPos() const { return m_currPos; }
		//@ SetSpeed
		void							SetSpeed(float fSpeed) { m_speed = fSpeed; }
		//@ GetLength - vrati dlzku uzavretej trajektorie
		float							GetLength() const;
		//@ Reset - resetuje index a curr Pos na 1. poziciu v trajektorie
		void							Reset();
		//@ IsDone
		bool							IsDone() const { return m_bDone; }

		//@ DebugDraw
		void							DebugDraw(const sysmath::C_Vector2 & worldPos) const;

	private:
		bool							m_bClosed;	//closedtrajectory - point obchadza dookola
		bool							m_bDone;		//done
		float							m_speed;		//pix in ms
		scmath::C_Vector2			m_currPos;	//aktualna pozicia v case, na zaciatku je v pociatku trajektorie
		T_PositionContainer		m_trajectory;
		u16							m_nextIndex;		//index do nasledujuceho pointu
	};

}}

