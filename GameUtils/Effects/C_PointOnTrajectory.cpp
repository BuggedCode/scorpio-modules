/*
* filename:			C_PointOnTrajectory.cpp
*
* author:			Kral Jozef
* date:				07/07/2009		22:00
* version:			1.00
* brief:				Movable point on trajectory in time
*/

#include "C_PointOnTrajectory.h"
#include <SysMemManager/globalNewDelete.h>
#include <GameFramework/DbgUtils/C_DebugDrawTool.h>
#include <SysMath/C_Spline2d.h>

namespace scorpio{ namespace gameutils{

	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;
	using namespace scorpio::framework;

	static const float C_BASE_SPEED = 0.4f;	//pix/ms


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_PointOnTrajectory::C_PointOnTrajectory(bool bClosed)
		: m_bClosed(bClosed), m_bDone(false), m_speed(C_BASE_SPEED), m_nextIndex(0)
	{
		//////////////////////////////////////////////////////////////////////////
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_PointOnTrajectory::C_PointOnTrajectory(const C_Spline2d & spline)
		: m_bClosed(false), m_bDone(false), m_speed(C_BASE_SPEED)
	{
		spline.Fill(m_trajectory);
		m_currPos = *m_trajectory.begin();
		m_nextIndex = 1;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_PointOnTrajectory::C_PointOnTrajectory(const T_PolyShape polyShape, float fSpeed)
		: m_bDone(false), m_speed(fSpeed)
	{
		m_bClosed = polyShape->IsClosed();

		const T_Polygon poly = polyShape->GetPolygon();
		for (u32 i = 0; i < poly->GetSize(); ++i)
		{
			m_trajectory.push_back((*poly)[i]);
		}
	
		m_currPos = *m_trajectory.begin();
		m_nextIndex = 1;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PointOnTrajectory::Initialize(const std::vector<C_Vector2> & inPositions)
	{
		for (std::vector<C_Vector2>::const_iterator iter = inPositions.begin(); iter != inPositions.end(); ++iter)
			m_trajectory.push_back(*iter);

		m_currPos = *m_trajectory.begin();
		m_nextIndex = 1;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Tick
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PointOnTrajectory::Tick(u32 inDeltaTime)
	{
		C_Vector2 dir(m_trajectory[m_nextIndex] - m_currPos);
		float fDist = dir.length2();

		dir.normalize();
		dir *= (inDeltaTime * m_speed);
		float fNewDist = dir.length2();

		if (fNewDist > fDist)
		{
			//nova pozice lezi az za kontrolnym pivotom - musime posunut za pivot
			float fRatio = 1.f - (fDist/fNewDist);	//pomer jaky je posun do najblizsieho pivotu, odcitame od 1 a mame relativnu vzdialenost pd dalsieho pivota
			u16 oldIndex = m_nextIndex;
			++m_nextIndex;
			if (m_nextIndex == (u16)m_trajectory.size())
			{
				if (!m_bClosed)
					m_bDone = true;

				m_nextIndex = 0;
			}

			C_Vector2 newDir(m_trajectory[m_nextIndex] - m_trajectory[oldIndex]);
			newDir.normalize();
			newDir *= fRatio;
			m_currPos = m_trajectory[oldIndex] + newDir;
		}
		else
		{
			m_currPos += dir;
		}
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PointOnTrajectory::DebugDraw(const C_Vector2 & worldPos) const
	{
		for (size_t i = 0; i < m_trajectory.size() - 1; ++i)
		{
			T_DebugDrawTool::GetInstance().DrawLine(worldPos + m_trajectory[i], worldPos + m_trajectory[i+1]);
		}
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetLength
	//
	//////////////////////////////////////////////////////////////////////////
	float C_PointOnTrajectory::GetLength() const
	{
		if (m_trajectory.empty())
			return 0.f;

		float fLength = 0.f;
		for (T_PositionContainer::const_iterator iter = m_trajectory.begin(); iter != m_trajectory.end(); ++iter)
		{
			T_PositionContainer::const_iterator iterNext = iter;
			std::advance(iterNext, 1);
			if (iterNext == m_trajectory.end())
				iterNext = m_trajectory.begin();

			fLength += C_Vector2(iterNext->m_x - iter->m_x, iterNext->m_y - iter->m_y).length();
		}

		return fLength;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Reset
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PointOnTrajectory::Reset()
	{
		if (m_bDone)
			m_bDone = false;

		m_nextIndex = 1;
		if (!m_trajectory.empty())
			m_currPos = *m_trajectory.begin();
		else
			m_currPos = C_Vector2();
	}

}}