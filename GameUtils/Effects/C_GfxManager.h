/*
* filename:			C_GfxManager.h
*
* author:			Kral Jozef
* date:				09/15/2010		21:38
* version:			1.00
* brief:
*/

#pragma once

#include <vector>
#include <GameUtils/Others/C_Trigger.h>
#include <GameUtils/GameUtilsApi.h>

namespace scorpio{ namespace gameutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_GfxManager
	template<class T>
	class C_GfxManager
	{
      typedef std::vector<T>	T_GfxEffectContainer;
      
	public:
		//@ c-tor
		C_GfxManager() {};

		//@ Update
		//@ retval - number of comets died in current Update cycle
		u32					Update(u32 deltaTick)
		{
			u32 nDeadCounter = 0;
			for (typename T_GfxEffectContainer::iterator iter = m_gfxEffects.begin(); iter != m_gfxEffects.end(); ++iter)
			{
				(*iter)->Tick(deltaTick);
				if ( (*iter)->IsDone() )
				{
					++nDeadCounter;
				}
			}

			IsDoneFunctor fnc;
			typename T_GfxEffectContainer::iterator _First = std::remove_if(m_gfxEffects.begin(), m_gfxEffects.end(), fnc);//predikat sa kopiroval tak som counter nedal do predicateClass
			m_gfxEffects.erase(_First, m_gfxEffects.end());
          return nDeadCounter;
		}

		//@ Update
		//@ retval - number of comets entered to the trigger Area
		u32					Update(u32 deltaTick, bool & outTrigerActivated)
		{
			//@ check & store positions
			for (typename T_GfxEffectContainer::iterator iter = m_gfxEffects.begin(); iter != m_gfxEffects.end(); ++iter)
			{
				(*iter)->Tick(deltaTick);
			}

			//@ check new positions & compare against former
			u32 internalCounter = 0;
			for (size_t index = 0; index < m_gfxEffects.size(); ++index)
			{
				if (m_gfxEffects[index]->IsInTrigger())
				{
					m_gfxEffects[index]->EnableTrigger(false);
					outTrigerActivated = true;
					++internalCounter;
				}
			}

			//@ Physiscal removing of dead comets from container
			IsDoneFunctor fnc;
			typename T_GfxEffectContainer::iterator _First = std::remove_if(m_gfxEffects.begin(), m_gfxEffects.end(), fnc);//predikat sa kopiroval tak som counter nedal do predicateClass
			m_gfxEffects.erase(_First, m_gfxEffects.end());

			return internalCounter;
		}

		//@ AddComet
		void					AddEffect(T gfxEffect)
		{
			m_gfxEffects.push_back(gfxEffect);
		}
		//@ SetTrigger
		void					SetTrigger(T_Trigger trigger) { m_trigger = trigger; }
		//@ SetTriggerPosition
		void					SetTriggerPosition(const sysmath::C_Point & pointPos) { SC_ASSERT(m_trigger); m_trigger->SetPosition(pointPos); }

		//@ Clear
		void					Clear()
		{
			m_gfxEffects.clear();
		}


	private:
		T_GfxEffectContainer	m_gfxEffects;
		T_Trigger				m_trigger;


		//@ IsDOneFuncor
		class IsDoneFunctor
		{
		public:
			bool operator()(T eff)
			{
				return eff->IsDone();
			}
		};
	};
}}