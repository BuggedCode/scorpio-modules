/*
* filename:			C_WallEffectBrick.h
*
* author:			Kral Jozef
* date:				9/19/2011		0:05
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/Frames/C_Visual.h>
#include <GameUtils/Effects/C_FloatingEffect.h>
#include <SysCore/Core/Animation/C_AnimationTrack.h>
#include <GameFramework/Controllers/C_AnimControllerWeak.h>

namespace scorpio{ namespace gameutils{

	namespace sccore = scorpio::syscore;
	namespace scframework = scorpio::framework;
	namespace scmath = scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ C_WallEffectBrick
	class C_WallEffectBrick : public scframework::I_Animatable
	{
	public:
		//@ c-tor
		C_WallEffectBrick(sccore::T_Visual visual, T_FloatingEffect effect, sccore::T_AnimationTrack animTrack, T_RenderLayer layerId);

		//@ Update
		void						Update(u32 inDeltaTime);
		//@ ActivateFadeIn
		void						ActivateFadeIn();
		//@ ActivateFallAnim
		void						ActivateFallAnim();
		//@ RemoveFromRender
		void						Reset();

		//@ I_Animatable INTERFACE
		virtual void			SetPositionRel(const scmath::C_Vector2 & vctPos);

	private:
		scmath::C_Vector2			m_InitPos;
		sccore::T_Visual			m_Visual;
		T_FloatingEffect			m_Effect;
		scframework::T_AnimControllerWeak	m_AnimController;
		scframework::T_Operator	m_FadeOp;
		T_RenderLayer				m_RenderLayer;
		bool							m_AnimActivated;
	};

	typedef boost::shared_ptr<C_WallEffectBrick>	T_WallEffectBrick;
}}