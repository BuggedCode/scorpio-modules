/*
* filename:			C_WallEffect.cpp
*
* author:			Kral Jozef
* date:				9/18/2011		18:40
* version:			1.00
* brief:
*/

#include "C_WallEffect.h"
#include <GameFramework/Particles/C_ParticleSystem.h>
#include <GameUtils/Effects/C_FloatingEffect.h>
#include <SysMath/C_Random.h>
#include <boost/mem_fn.hpp>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysCore/Core/C_AnimationManager.h>

namespace scorpio{ namespace gameutils{

	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;
	using namespace scorpio::sysutils;
	using namespace scorpio::effects;
	using namespace scorpio::gameutils;

	static const float C_EFFECT_VELOCITY = 0.2f;
	static const C_HashName C_BRICK_FALL_ANIM_TRACK_NAME1 = C_HashName("WallBrickFallAnim1");
	static const C_HashName C_BRICK_FALL_ANIM_TRACK_NAME2 = C_HashName("WallBrickFallAnim2");
	static const u8 C_GENERATION_STEP = 2;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_WallEffect::~C_WallEffect()
	{
		m_WallBricks.clear();
		m_WallBricksFadedIn.clear();
		m_AnimTrackList.clear();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_WallEffect::Initialize(T_Graphics logoTexture, u16 brickSize, const C_Vector2 & logoCenterPos, C_HashName sparklesEffTemplUID)
	{
		T_Stream stream = T_FileSystemManager::GetInstance().Open(m_BrickFallAnimFile);
		/*bool bRes = */T_AnimationManager::GetInstance().LoadAnimation(stream);

		T_AnimationTrack animTrack = T_AnimationManager::GetInstance().GetAnimation(C_BRICK_FALL_ANIM_TRACK_NAME1);
		m_AnimTrackList.push_back(animTrack);
		animTrack = T_AnimationManager::GetInstance().GetAnimation(C_BRICK_FALL_ANIM_TRACK_NAME2);
		m_AnimTrackList.push_back(animTrack);

		u8 xCount = (u8)(logoTexture->GetWidth() / brickSize);
		u8 yCount = (u8)(logoTexture->GetHeight() / brickSize);

		float brickSizeHalf = brickSize / 2.f;

		C_Vector2 vctCenterScreen(logoCenterPos);
		C_Vector2 brickPos = vctCenterScreen - C_Vector2(logoTexture->GetWidth()/2.f, logoTexture->GetHeight()/2.f);
		brickPos += C_Vector2(brickSizeHalf, brickSizeHalf);

		float tmpXStartPos = brickPos.m_x;

		C_Vector2 vctClip;

		for (int j = 0; j < yCount; ++j)
		{
			for (int i = 0; i < xCount; ++i)
			{
				T_Visual vis = T_Visual( new C_Visual(brickPos, brickSize, brickSize));
				vis->SetTexture(logoTexture);
				
				vctClip.m_x = i / (float)xCount;
				vctClip.m_y = (i+1) / (float)xCount;
				vis->SetTexCoordX(vctClip);

				vctClip.m_x = j / (float)yCount;
				vctClip.m_y = (j+1) / (float)yCount;
				vis->SetTexCoordY(vctClip);

				//effect
				T_FloatingEffect effect;
				if (!sparklesEffTemplUID.IsClear())
				{
					T_Effect eff = T_ParticleSystem::GetInstance().GetManager().CreateEffect(sparklesEffTemplUID);
					T_Polygon poly = T_Polygon(new C_Polygon());
					poly->Add(C_Vector2(-brickSizeHalf, -brickSizeHalf));
					poly->Add(C_Vector2(+brickSizeHalf, -brickSizeHalf));
					poly->Add(C_Vector2(+brickSizeHalf, +brickSizeHalf));
					poly->Add(C_Vector2(-brickSizeHalf, +brickSizeHalf));
					poly->Add(C_Vector2(-brickSizeHalf, -brickSizeHalf));


					T_PolyShape polyShape = T_PolyShape(new C_PolyShape("BrickWallPoly", poly));
					polyShape->SetPosition(brickPos);
					if (eff)
						effect = T_FloatingEffect(new C_FloatingEffect(eff, m_RenderLayerId, polyShape, C_EFFECT_VELOCITY));
				}

				u32 animId = C_Random::Random((u32)2);
				T_WallEffectBrick wallBrick = T_WallEffectBrick(new C_WallEffectBrick(vis, effect, m_AnimTrackList[animId], m_RenderLayerId));
				m_WallBricks.push_back(wallBrick);

				brickPos += C_Vector2(brickSize, 0.f);
			}

			brickPos += C_Vector2(0.f, brickSize);
			brickPos.m_x = tmpXStartPos;
		}

		return true;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Deinitialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_WallEffect::Deinitialize()
	{
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Tick
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffect::Tick(u32 inDeltaTime)
	{
		if (!m_bActivated)
			return;

		m_Scheduler.Update(inDeltaTime);

		for(T_WallBricks::iterator iter = m_WallBricksFadedIn.begin(); iter != m_WallBricksFadedIn.end(); ++iter)
			(*iter)->Update(inDeltaTime);

		for(T_WallBricks::iterator iter = m_FallenBricks.begin(); iter != m_FallenBricks.end(); ++iter)
			(*iter)->Update(inDeltaTime);

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ FadeInWallBrick
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffect::FadeInWallBrick()
	{
		for (u8 i = 0; i < C_GENERATION_STEP; ++i)
		{
			if (m_WallBricks.empty())
				break;
			//@ proceed fadeIn for 1Brick
			u32 nIndex = C_Random::Random((u32)m_WallBricks.size());

			T_WallEffectBrick brick = m_WallBricks[nIndex];
			SC_ASSERT(brick);
			brick->ActivateFadeIn();
			m_WallBricksFadedIn.push_back(brick);
			T_WallEffectBrick lastBrick = m_WallBricks.back();
			m_WallBricks[nIndex] = lastBrick;
			m_WallBricks.pop_back();
		}

		if (!m_WallBricks.empty())
			m_Scheduler.Schedule(&C_WallEffect::FadeInWallBrick, 1);
		else
			m_Scheduler.Schedule(&C_WallEffect::FallOffWallBrick, 2000);
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ FallOffWallBrick
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffect::FallOffWallBrick()
	{
		for (u8 i = 0; i < C_GENERATION_STEP+4; ++i)
		{
			if (m_WallBricksFadedIn.empty())
				break;

			u32 nIndex = C_Random::Random((u32)m_WallBricksFadedIn.size());

			T_WallEffectBrick brick = m_WallBricksFadedIn[nIndex];
			SC_ASSERT(brick);
			brick->ActivateFallAnim();
			m_FallenBricks.push_back(brick);
			T_WallEffectBrick lastBrick = m_WallBricksFadedIn.back();
			m_WallBricksFadedIn[nIndex] = lastBrick;
			m_WallBricksFadedIn.pop_back();
		}

		if (!m_WallBricksFadedIn.empty())
			m_Scheduler.Schedule(&C_WallEffect::FallOffWallBrick, 1);
		else
			m_Scheduler.Schedule(&C_WallEffect::FinishLogoAnim, 500);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ FinishLogoAnim
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffect::FinishLogoAnim()
	{
		m_bFinished = true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Reset
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffect::Reset()
	{
		m_WallBricks.insert(m_WallBricks.end(), m_WallBricksFadedIn.begin(), m_WallBricksFadedIn.end());
		m_WallBricks.insert(m_WallBricks.end(), m_FallenBricks.begin(), m_FallenBricks.end());

		m_WallBricksFadedIn.clear();
		m_FallenBricks.clear();
		std::for_each(m_WallBricks.begin(), m_WallBricks.end(), boost::mem_fn(&C_WallEffectBrick::Reset));

		m_Scheduler.Clear();
		m_Scheduler.Schedule(&C_WallEffect::FadeInWallBrick, 1);
		return;
	}
}}