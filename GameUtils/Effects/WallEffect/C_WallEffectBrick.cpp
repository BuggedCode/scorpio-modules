/*
* filename:			C_WallEffectBrick.cpp
*
* author:			Kral Jozef
* date:				9/19/2011		0:30
* version:			1.00
* brief:
*/

#include "C_WallEffectBrick.h"
#include <SysCore/Core/C_RenderManager.h>
#include <GameFramework/Operators/C_FadeOperator.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace gameutils{

	using namespace scorpio::syscore;
	using namespace scorpio::framework;
	using namespace scorpio::sysmath;

	static const float C_FADE_ALPHA_STEP = 0.002f;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_WallEffectBrick::C_WallEffectBrick(sccore::T_Visual visual, T_FloatingEffect effect, sccore::T_AnimationTrack animTrack, T_RenderLayer layerId)
		: m_Visual(visual)/*, m_Effect(effect)*/, m_AnimActivated(false), m_RenderLayer(layerId)
	{
		m_InitPos = m_Visual->GetPosition();
		m_AnimController = T_AnimControllerWeak(new C_AnimControllerWeak(animTrack, this, NULL));
		m_Visual->GetColor().m_w = 0.f;
		m_FadeOp = T_Operator( new C_FadeOperator(m_Visual->GetColor().m_w, NULL, C_FADE_ALPHA_STEP) );
		m_FadeOp->Deactivate();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffectBrick::Update(u32 inDeltaTime)
	{
		if (m_Effect)
			m_Effect->Tick(inDeltaTime);

		m_FadeOp->Update(inDeltaTime);

		if (m_AnimActivated)
			m_AnimController->Update(inDeltaTime);

		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Activate
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffectBrick::ActivateFadeIn()
	{
		if (m_Effect)
			m_Effect->Activate(true);

		T_RenderManager::GetInstance().Add(m_Visual, m_RenderLayer);
		m_FadeOp->Activate();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ActivateFallAnim
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffectBrick::ActivateFallAnim()
	{
		m_AnimActivated = true;
		C_FadeOperator * fadeOp = dynamic_cast<C_FadeOperator*>(m_FadeOp.get());
		fadeOp->SetStep(-C_FADE_ALPHA_STEP);
		m_FadeOp->Activate();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Reset
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffectBrick::Reset()
	{
		m_AnimController->Reset();
		m_AnimController->Activate();
		m_AnimActivated = false;
		m_Visual->SetPosition(m_InitPos);
		m_Visual->GetColor().m_w = 0.f;
		if (m_Effect)
			m_Effect->Activate(false);
		T_RenderManager::GetInstance().Remove(m_Visual, m_RenderLayer);

		C_FadeOperator * fadeOp = dynamic_cast<C_FadeOperator*>(m_FadeOp.get());
		fadeOp->SetStep(C_FADE_ALPHA_STEP);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetPositionRel
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WallEffectBrick::SetPositionRel(const C_Vector2 & vctPos)
	{
		m_Visual->SetPosition(m_InitPos + vctPos);
		return;
	}
}}