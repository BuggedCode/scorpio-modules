/*
* filename:			C_WallEffect.h
*
* author:			Kral Jozef
* date:				9/18/2011		18:39
* version:			1.00
* brief:
*/

#pragma once

#include <vector>
#include <SysCore/Render/C_Graphic.h>
#include <SysMath/C_Vector2.h>
#include <SysCore/Frames/C_Visual.h>
#include <SysUtils/C_ContextScheduler.h>
#include "C_WallEffectBrick.h"

namespace scorpio{ namespace gameutils{

#pragma warning(push)
#pragma warning(disable:4355)	//suppres warning for using this in initializer list		


	namespace scmath = scorpio::sysmath;
	namespace sccore = scorpio::syscore;
	namespace scutils = scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//@ C_WallEffect
	class GAMEUTILS_API C_WallEffect
	{
	public:
		//@ c-tor
		C_WallEffect(sysutils::C_String brickFallAnimFile, T_RenderLayer layerId) : m_bActivated(false), m_bFinished(false), m_Scheduler(*this), m_BrickFallAnimFile(brickFallAnimFile), m_RenderLayerId(layerId) {};
		//@ d-tor
		~C_WallEffect();

		//@ Initialize
		bool					Initialize(T_Graphics logoTexture, u16 brickSize, const scmath::C_Vector2 & logoCenterPos, scutils::C_HashName sparklesEffTemplUID);
		//@ Deinitialize
		bool					Deinitialize();
		//@ Tick
		void					Tick(u32 inDeltaTime);

		//@ Activate
		void					Activate() { m_bActivated = true; m_Scheduler.Schedule(&C_WallEffect::FadeInWallBrick, 100); }
		//@ Reset
		void					Reset();


		//@ 1st phase of generation
		void					FadeInWallBrick();
		//@ 2nd phase of generation
		void					FallOffWallBrick();
		//@ 3rd phase of generation - set finished flag
		void					FinishLogoAnim();

		
	private:
		bool					m_bActivated;
		bool					m_bFinished;
		typedef std::vector<T_WallEffectBrick> T_WallBricks;
		T_WallBricks		m_WallBricks;
		typedef scutils::C_ContextScheduler<C_WallEffect>	T_LogoScheduler;
		T_LogoScheduler	m_Scheduler;

		T_WallBricks		m_WallBricksFadedIn;	//list of fadedIn of bricks
		T_WallBricks		m_FallenBricks;	//list of fallen bricks
		T_RenderLayer		m_RenderLayerId;
		sysutils::C_String	m_BrickFallAnimFile;
		std::vector<sccore::T_AnimationTrack>	m_AnimTrackList;
	};

#pragma warning(pop)

}}