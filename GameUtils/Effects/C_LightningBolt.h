/*
* filename:			C_LightningBolt.h
*
* author:			Kral Jozef
* date:				09/14/2010		20:50
* version:			1.00
* brief:
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include <boost/shared_ptr.hpp>
#include <SysMath/C_Vector2.h>
#include <vector>
#include <SysCore/Core/I_RenderObject.h>
#include <SysCore/Render/C_Quad.h>
#include <SysCore/Render/C_Graphic.h>
#include <GameFramework/Operators/I_BaseOperator.h>

namespace scorpio{ namespace gameutils{

	namespace scmath = scorpio::sysmath;
	namespace scrender = scorpio::sysrender;
	namespace scutils = scorpio::sysutils;
	namespace scframework = scorpio::framework;

	//////////////////////////////////////////////////////////////////////////
	//@ C_LightningBolt
	class GAMEUTILS_API C_LightningBolt : public scorpio::syscore::I_RenderObject, public scframework::I_OperatorListener
	{
		typedef std::vector<scmath::C_Vector2>	T_Points;
		
	public:
		//@ c-tor
		//@ vctStart - startPosition of Bolt
		//@ vctEnd - endPosition of Bolt
		//@ segments - number of segments to which is bolt divided
		//@ offste - is offset of new point generated for new segments /*\,  this point os offseted in perpendicular direction from orig line
		//@ graph - texture
		C_LightningBolt(const scmath::C_Vector2 & vctStart, const scmath::C_Vector2 & vctEnd, u8 segments, float offset, T_Graphics graph, const scmath::C_Vector4 & color, float boltWidth);

		//@ Draw
		void				Render() const;
#ifndef MASTER
		//@ DebugDraw
		virtual void	DebugDraw() const;
#endif
		//@ GetZCoord
		float				GetZCoord() const { return 0.5f; }
		//@ Update
		void				Tick(u32 deltaTime);
		//@ Activate
		//@ lifeTime - life in ms
		void				Activate(u16 lifeTime);
		//@ IsDone
		bool				IsDone() const { return m_IsDead; } 

		//@ NotifyOperatorFinish
		virtual void	NotifyOperatorFinish(scframework::I_BaseOperator * callerOp);

		//@ StartSecondBolt
		void				StartSecondBolt();

		//@ SetEndColor
		void				SetEndColor(const scmath::C_Vector4 & endColor);

	private:
		//@ GeneratePoints
		void				GeneratePoints(const scmath::C_Vector2 & vctStart, const scmath::C_Vector2 & vctEnd, u8 steps, float Offset, T_Points & outPoints);
		//@ CreateGeometry
		void				CreateGeometry(const T_Points & points, sysrender::T_Quads & quads);
		//@ CopyGeometry
		void				CopyGeometry(const sysrender::T_Quads & srcQuads, sysrender::T_Quads & dstQuads);
		//@ CopyPoints
		void				CopyPoints(const T_Points & srcPoints, T_Points & dstPoints);
		//@ RenderBolt
		void				RenderBolt(const sysrender::T_Quads & quads, const scmath::C_Vector4 & vctColor) const;
		//@ DebugDrawBolt
		void				DebugDrawBolt(const sysrender::T_Quads & quads, const scmath::C_Vector4 & vctColor) const;
		//@ Aplyjittering, jitteringOffste - in pixels
		void				ApplyJittering(const T_Points & points, T_Points & jitteredPoints, u8 jitteringOffset);


	private:
		scmath::C_Vector2	m_Start;
		scmath::C_Vector2	m_End;
		u32					m_AccumTime;
		u16					m_LifeTime;	//life time of all bolts together
		u8						m_SegmentsCount;	//count of splitting
		float					m_Offset;	//initial length of perpendicular vector for middle point, when splitting / generating new point
		float					m_HalfWidth;	//halfWidth of BoltSegment

		scmath::C_Vector4	m_OrigColor;	//color for new bolt, in case current color is interpolating by operator
		scmath::C_Vector4	m_Color1;		//Current Color for bolt1 rendering
		scmath::C_Vector4	m_Color2;		//Current Color for bolt1 rendering

		T_Points		m_Bolt1Ppoints;
		T_Points		m_Bolt2Ppoints;
		sysrender::T_Quads		m_Bolt1Geom;
		sysrender::T_Quads		m_Bolt2Geom;

		scframework::T_Operator	m_FadeOutOp1;
		scframework::T_Operator	m_FadeOutOp2;
		scframework::T_Operator	m_ColorOp1;
		scframework::T_Operator	m_ColorOp2;

		T_Graphics		m_Graph;

		u32				m_StartSecondBoltDelay;		//scheduled delayTime for startOfSecondBolt
		bool				m_SecondAlreadyGenerated;	//flag if second bolt was fired

		bool				m_IsDead;

	public:
		static const float C_DEFAULT_BOLT_WIDTH;	//width of bolt segment
	};


	typedef boost::shared_ptr<C_LightningBolt>	T_LightningBolt;

}};