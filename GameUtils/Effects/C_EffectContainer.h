/*
* filename:			C_EffectContainer.h
*
* author:			Kral Jozef
* date:				10/21/2010		0:43
* version:			1.00
* brief:				Specialized updatable container for ParticleEffects
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include <Common/I_NonCopyAble.h>
#include <GameFramework/Particles/C_Effect.h>
#include <GameUtils/Others/C_Trigger.h>
#include <vector>

namespace scorpio{ namespace gameutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_EffectContainer
	class GAMEUTILS_API C_EffectContainer : public I_NonCopyAble
	{
	public:
		//@ c-tor
		C_EffectContainer() {};
		//@ d-tor
		virtual ~C_EffectContainer() {};

		//@ Update
		//@ retval - number of comets died in current Update cycle
		virtual u32			Update(u32 deltaTick);
		//@ Update
		//@ retval - number of comets died in current Update cycle
		virtual u32			Update(u32 deltaTick, bool & outTrigerActivated);
		//@ Add
		virtual void		Add(effects::T_Effect effect);
		//@ SetTrigger
		void					SetTrigger(T_Trigger trigger) { m_trigger = trigger; }

	protected:
		typedef std::vector<effects::T_Effect>	T_EffectContainer;
		T_EffectContainer	m_container;
		T_Trigger			m_trigger;
	};

}}