/*
* filename:			C_EffectContainer.cpp
*
* author:			Kral Jozef
* date:				10/21/2010		0:43
* version:			1.00
* brief:
*/

#include "C_EffectContainer.h"
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace gameutils{

	using namespace effects;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_EffectContainer::Update(u32 deltaTick)
	{
		u32 nDeadCounter = 0;
		for (T_EffectContainer::iterator iter = m_container.begin(); iter != m_container.end(); ++iter)
		{
			(*iter)->Update(deltaTick);
			if ( (*iter)->HasParticles() == false)
			{
				++nDeadCounter;
			}
		}

		//////////////////////////////////////////////////////////////////////////
		class IsDoneFunctor
		{
		public:
			bool operator()(T_Effect eff)
			{
				return (eff->HasParticles() == false);
			}
		};

		IsDoneFunctor fnc;
		T_EffectContainer::iterator _First = std::remove_if(m_container.begin(), m_container.end(), fnc);//predikat sa kopiroval tak som counter nedal do predicateClass
		m_container.erase(_First, m_container.end());
		return nDeadCounter;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_EffectContainer::Update(u32 deltaTick, bool & outTrigerActivated)
	{
		SC_ASSERT(m_trigger);
		if (!m_trigger)
			return -1;

		size_t nCount = m_container.size();
		bool * inTriggerArea = new bool[nCount];

		for (u8 index = 0; index < nCount; ++index)
		{
			inTriggerArea[index] = m_trigger->IsInTrigger(m_container[index]->GetPosition());
		}

		u32 uCount = this->Update(deltaTick);

		for (u8 index = 0; index < nCount; ++index)
		{
			bool bInTriggerArea = m_trigger->IsInTrigger(m_container[index]->GetPosition());
			if (bInTriggerArea != inTriggerArea[index])
			{
				outTrigerActivated = true;
				break;
			}
		}

		return uCount;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Add
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EffectContainer::Add(T_Effect effect)
	{
		m_container.push_back(effect);
	}

}}