/*
* filename:			C_FloatingEffect.h
*
* author:			Kral Jozef
* date:				09/14/2010		22:13
* version:			1.00
* brief:
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include "C_PointOnTrajectory.h"
#include <GameFramework/Particles/C_Effect.h>
#include <SysMath/C_Point.h>
#include <boost/shared_ptr.hpp>
#include <GameUtils/Others/C_Trigger.h>
#include <SysCore/Geometry/C_PolyShape.h>

namespace scorpio{ namespace gameutils{

	namespace sceffects = scorpio::effects;
	namespace scmath = scorpio::sysmath;
	namespace sccore = scorpio::syscore;

	//////////////////////////////////////////////////////////////////////////
	//@ C_FloatingEffect
	class GAMEUTILS_API C_FloatingEffect
	{
	public:
		//@ c-tor
		C_FloatingEffect(sceffects::T_Effect fgxEffect, u32 renderLayer, const scmath::C_Vector2 & startPos, const scmath::C_Vector2 & endPos, float speed);
		//@ c-tor
		C_FloatingEffect(sceffects::T_Effect fgxEffect, u32 renderLayer, const scmath::C_Spline2d & spline, float speed);
		//@ c-tor
		C_FloatingEffect(sceffects::T_Effect gfxEffect, u32 renderLayer, const sccore::T_PolyShape polyShape, float speed);
		//@ d-tor
		~C_FloatingEffect();

		//@ Tick
		void							Tick(u32 inDeltaTime);
		//@ SetActive
		void							Activate(bool bActive);
		//@ IsDone
		bool							IsDone() const { return m_bDone; }
		//@ SetPosition
		void							SetPosition(const scmath::C_Vector2 & vctPos) { m_pivot = vctPos; }
		//@ GetPosition
		const scmath::C_Vector2&GetPosition() const { return m_pivot; }

		//@ SetTrigger
		void							SetTrigger(const T_Trigger trigger) { m_Trigger = trigger; m_TrigerEnabled = true; }
		//@ IsInTrigger
		bool							IsInTrigger() const;
		//@ EnableTrigger
		void							EnableTrigger(bool bEnable) { m_TrigerEnabled = bEnable; }
		//@ Set NonInteractiveTrigger
		//void							SetNITrigger(const T_Trigger niTrigger) { m_niTrigger = niTrigger; }
		//@ CanInteractWith
		//bool							CanInteractWith(const T_Trigger trigger) { return m_niTrigger != trigger; }


	private:
		sysmath::C_Vector2		m_pivot;			//world pos of effect
		C_PointOnTrajectory	*	m_movingEffect;	//logic of moving
		sceffects::T_Effect		m_gfxEffect;	//particle effect
		bool							m_Active;		//effect is active for ticking

		bool							m_bDeactivating;	//efekt dosel ku koncu a uz jen caka na dotikanie particlom
		bool							m_bDone;			//efekt je done - i partikle dotikane

		//T_Trigger					m_niTrigger;	//nonInteractivetrigger! later in case of need list!
		T_Trigger					m_Trigger;	//triger for this effect
		bool							m_TrigerEnabled;
		u32							m_RenderLayer;
	};

	typedef boost::shared_ptr<C_FloatingEffect>	T_FloatingEffect;
	typedef std::vector<T_FloatingEffect>			T_FloatingEffectList;

}}