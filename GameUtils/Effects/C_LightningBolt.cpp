/*
* filename:			C_LightningBolt.cpp
*
* author:			Kral Jozef
* date:				09/14/2010		20:50
* version:			1.00
* brief:
*/

#include "C_LightningBolt.h"
#include <SysMath/C_Random.h>
#include <SysMath/C_Line2d.h>
#include <GameFramework/Operators/C_FadeOperator.h>
#include <GameFramework/Operators/C_ColorInterpolator.h>

namespace scorpio{ namespace gameutils{

	const float	C_LightningBolt::C_DEFAULT_BOLT_WIDTH = 26.f;
	static const float	C_FADE_OUT_STEP = -0.003125f;	//means after 20 Frames will be zero 20frames*16ms * value = 1.f
	static const u8		C_JITTERING_OFFSET = 2;	//max value for jittering points iin every frame in pixels 
	static const u16		C_DEFAULT_LIFE_TIME = 1000;

	static const u16		C_ONE_BOLT_HALF_LIFETIME = 160;

	using namespace scorpio::sysmath;
	using namespace scorpio::sysrender;
	using namespace scorpio::sysutils;
	using namespace scorpio::framework;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_LightningBolt::C_LightningBolt(const C_Vector2 & vctStart, const C_Vector2 & vctEnd, u8 segments, float offset, T_Graphics graph, const C_Vector4 & color, float boltWidth)
		: m_Start(vctStart), m_End(vctEnd), m_AccumTime(0), m_SegmentsCount(segments), m_Offset(offset), m_Graph(graph), m_LifeTime(C_DEFAULT_LIFE_TIME), m_Color1(color), m_Color2(color), m_OrigColor(color), m_IsDead(false)
	{
		SC_ASSERT(graph);

		m_HalfWidth = boltWidth / 2.f;

		m_FadeOutOp1 = T_Operator( new C_FadeOperator(m_Color1.m_w, this, C_FADE_OUT_STEP) );
		m_FadeOutOp2 = T_Operator( new C_FadeOperator(m_Color2.m_w, this, C_FADE_OUT_STEP) );

		m_StartSecondBoltDelay = 0;
		m_SecondAlreadyGenerated = true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Activate
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::Activate(u16 lifeTime)
	{
		m_LifeTime = lifeTime;

		m_Bolt1Ppoints.push_back(m_Start);
		GeneratePoints(m_Start, m_End, m_SegmentsCount, m_Offset, m_Bolt1Ppoints);
		m_FadeOutOp1->Activate();
		if (m_ColorOp1)
			m_ColorOp1->Activate();

		m_StartSecondBoltDelay = C_ONE_BOLT_HALF_LIFETIME;
		m_SecondAlreadyGenerated = false;
		return;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ StartSecondBolt
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::StartSecondBolt()
	{
		CopyPoints(m_Bolt1Ppoints, m_Bolt2Ppoints);
		m_FadeOutOp2->Activate();
		if (m_ColorOp2)
			m_ColorOp2->Activate();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GeneratePoints
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::GeneratePoints(const scmath::C_Vector2 & vctStart, const scmath::C_Vector2 & vctEnd, u8 steps, float Offset, T_Points & outPoints)
	{
		//////////////////////////////////////////////////////////////////////////
		float Len = C_Vector2(vctStart - vctEnd).length();
		C_Vector2 vctDir(vctEnd - vctStart);
		vctDir.normalize();

		u32 rnd = C_Random::Random((u32)2);
		C_Vector2 vctPerp(vctDir);
		vctPerp.ortho();
		vctPerp *= Offset;
		if (rnd)
			vctPerp *= -1;

		vctDir *= Len/2.f;
		
		C_Vector2 vctMid(vctDir + vctStart);
		
		vctMid += vctPerp;


		--steps;

		if (steps == 0)
		{
			outPoints.push_back(vctMid);
			outPoints.push_back(vctEnd);
		}
		else
		{
			GeneratePoints(vctStart, vctMid, steps, Offset / 2.f, outPoints);
			GeneratePoints(vctMid, vctEnd, steps, Offset / 2.f, outPoints);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateGeometry
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::CreateGeometry(const T_Points & points, T_Quads & quads)
	{
		if (points.empty())
			return;

		quads.clear();
		for (u32 i = 0; i < (u32)points.size() - 1; ++i)
		{
			const C_Vector2 & vctStart = points[i];
			const C_Vector2 & vctEnd = points[i+1];

			C_Vector2 vctDir(vctEnd - vctStart);
			vctDir.ortho();
			vctDir.normalize();
			vctDir *= m_HalfWidth;

			C_Quad quad;
			
			//generate start
			if (i == 0)
			{
				vctDir *= -1;
				quad.m_verticies[0] = vctStart + vctDir;
				vctDir *= -1;
				quad.m_verticies[3] = vctStart + vctDir;
			}
			else
			{
				//z predosleho
				const C_Quad & tmpQuad = quads[i-1];
				quad.m_verticies[0] = tmpQuad.m_verticies[1];
				quad.m_verticies[3] = tmpQuad.m_verticies[2];
			}
			

			//generate end
			if (i == ((u32)points.size() - 2))
			{
				vctDir *= -1;
				quad.m_verticies[1] = vctEnd + vctDir;
				vctDir *= -1;
				quad.m_verticies[2] = vctEnd + vctDir;
			}
			else
			{
				C_Line2d line1(vctStart + vctDir, vctEnd + vctDir);

				C_Vector2 vctStart2(vctEnd);
				C_Vector2 vctEnd2 = points[i+2];

				C_Vector2 vctDir_(vctEnd2 - vctStart2);
				vctDir_.ortho();
				vctDir_.normalize();
				vctDir_ *= m_HalfWidth;

				C_Line2d line2(vctStart2 + vctDir_, vctEnd2 + vctDir_);

				C_Vector2 vctInters1;
				bool bInt = line1.Intersection(line2, vctInters1);
				
				//@ nie su rovnobezne
				if (bInt)
				{
					C_Line2d line3(vctStart - vctDir, vctEnd - vctDir);
					C_Line2d line4(vctStart2 - vctDir_, vctEnd2 - vctDir_);

					C_Vector2 vctInters2;
					bInt = line3.Intersection(line4, vctInters2);

					//@ here can be intersect FALSE => they are parralel, but first intersect can be found and seconf none! because of rounding error!

					if (!bInt)
					{
						quad.m_verticies[1] = vctEnd - vctDir;
					}
					else
					{
						quad.m_verticies[1] = vctInters2;
					}

					quad.m_verticies[2] = vctInters1;
				}
				//@ su rovnobezne
				else
				{
					quad.m_verticies[2] = vctEnd + vctDir;
					quad.m_verticies[1] = vctEnd - vctDir;
				}
			}

			quads.push_back(quad);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CopyGeometry
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::CopyGeometry(const T_Quads & srcQuads, T_Quads & dstQuads)
	{
		dstQuads.clear();
		for (T_Quads::const_iterator iter = srcQuads.begin(); iter != srcQuads.end(); ++iter)
		{
			const C_Quad & quad = (*iter);
			dstQuads.push_back(quad);
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CopyPoints
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::CopyPoints(const T_Points & srcPoints, T_Points & dstPoints)
	{
		dstPoints.clear();
		for (T_Points::const_iterator iter = srcPoints.begin(); iter != srcPoints.end(); ++iter)
		{
			const C_Vector2 & vctPoint = (*iter);
			dstPoints.push_back(vctPoint);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::Tick(u32 deltaTime)
	{
		if (m_IsDead)
			return;

		m_StartSecondBoltDelay -= deltaTime;
		if (!m_SecondAlreadyGenerated && m_StartSecondBoltDelay <= 0)
		{
			m_StartSecondBoltDelay = 0;
			StartSecondBolt();
			m_SecondAlreadyGenerated = true;
		}

		m_AccumTime += deltaTime;

		if (m_AccumTime >= m_LifeTime)
		{
			m_IsDead = true;
			SetVisible(false);
			return;
		}

		m_FadeOutOp1->Update(deltaTime);
		m_FadeOutOp2->Update(deltaTime);
		if (m_ColorOp1)
			m_ColorOp1->Update(deltaTime);
		if (m_ColorOp2)
			m_ColorOp2->Update(deltaTime);

		T_Points jitteredBoltPoints1;
		ApplyJittering(m_Bolt1Ppoints, jitteredBoltPoints1, C_JITTERING_OFFSET);
		CreateGeometry(jitteredBoltPoints1, m_Bolt1Geom);
		
		T_Points jitteredBoltPoints2;
		ApplyJittering(m_Bolt2Ppoints, jitteredBoltPoints2, C_JITTERING_OFFSET);
		CreateGeometry(jitteredBoltPoints2, m_Bolt2Geom);

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ApplyJittering
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::ApplyJittering(const T_Points & points, T_Points & jitteredPoints, u8 jitteringOffset)
	{
		if (!jitteringOffset)
		{
			jitteredPoints.assign(points.begin(), points.end());
			return;
		}

		for (T_Points::const_iterator iter = points.begin(); iter != points.end(); ++iter)
		{
			const C_Vector2 & vctPoint = *iter;
			//get random offste for X & Y
			s8 offsetX = (s8)C_Random::Random((u32)-jitteringOffset, (u32)jitteringOffset);
			s8 offsetY = (s8)C_Random::Random((u32)-jitteringOffset, (u32)jitteringOffset);

			jitteredPoints.push_back(C_Vector2(vctPoint.m_x + offsetX, vctPoint.m_y + offsetY));
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetEndColor
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::SetEndColor(const C_Vector4 & endColor)
	{
		//create color interpolator
		m_ColorOp1 = T_Operator(new C_ColorInterpolator(m_Color1, this, endColor, C_ONE_BOLT_HALF_LIFETIME));
		m_ColorOp2 = T_Operator(new C_ColorInterpolator(m_Color2, this, endColor, C_ONE_BOLT_HALF_LIFETIME));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ NotifyOperatorFinish
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::NotifyOperatorFinish(I_BaseOperator * callerOp)
	{
		//@ genberate new bolt
		if (callerOp == m_FadeOutOp1.get())
		{
			m_Bolt1Ppoints.clear();
			m_Bolt1Geom.clear();
			m_Color1 = m_OrigColor;

			m_Bolt1Ppoints.push_back(m_Start);
			GeneratePoints(m_Start, m_End, m_SegmentsCount, m_Offset, m_Bolt1Ppoints);
			m_FadeOutOp1->Activate();
		}

		//@ generate second highlighted bolt
		else if (callerOp == m_FadeOutOp2.get())
		{
			m_Bolt2Geom.clear();
			m_Color2 = m_OrigColor;
			CopyPoints(m_Bolt1Ppoints, m_Bolt2Ppoints);
			m_FadeOutOp2->Activate();
		}

		else if (callerOp == m_ColorOp1.get())
		{
			if (m_ColorOp1)
				m_ColorOp1->Activate();
		}

		else if (callerOp == m_ColorOp2.get())
		{
			if (m_ColorOp2)
				m_ColorOp2->Activate();
		}

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderBolt
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::RenderBolt(const T_Quads & quads, const C_Vector4 & vctColor) const
	{
		//TODO SLOW - use batches!
		for (T_Quads::const_iterator iter = quads.begin(); iter != quads.end(); ++iter)
		{
			const C_Quad & quad = *iter;
			SC_ASSERT(false);//m_Graph->RenderQuad(quad.m_verticies[0], quad.m_verticies[1], quad.m_verticies[2], quad.m_verticies[3], vctColor);
		}

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::Render() const
	{
		m_Graph->SetBlendingMode(E_BM_SRC_ALPHA__DST_ONE);

		__asm nop;//m_Graph->RenderBatchQuad(m_Bolt1Geom, m_Color1);
		__asm nop;//m_Graph->RenderBatchQuad(m_Bolt2Geom, m_Color2);

		m_Graph->SetBlendingMode(E_BM_SRC_ALPHA__DST_ONE_MINUS_ALPHA);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::DebugDraw() const
	{
		DebugDrawBolt(m_Bolt1Geom, m_Color1);
		DebugDrawBolt(m_Bolt2Geom, m_Color2);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDrawBolt
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LightningBolt::DebugDrawBolt(const T_Quads & quads, const C_Vector4 & vctColor) const
	{
		for (T_Quads::const_iterator iter = quads.begin(); iter != quads.end(); ++iter)
		{
			const C_Quad & quad = *iter;
			m_Graph->RenderLine(quad.m_verticies[0], quad.m_verticies[1], vctColor);
			m_Graph->RenderLine(quad.m_verticies[1], quad.m_verticies[2], vctColor);
			m_Graph->RenderLine(quad.m_verticies[2], quad.m_verticies[3], vctColor);
			m_Graph->RenderLine(quad.m_verticies[3], quad.m_verticies[0], vctColor);
		}

		//m_Graph->RenderLines(m_Points, vctColor);
	}

}}