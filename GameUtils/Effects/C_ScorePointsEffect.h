/*
* filename:			C_ScorePointsEffect.h
*
* author:			Kral Jozef
* date:				09/17/2010		20:56
* version:			1.00
* brief:
*/

#pragma once

#include <GameUtils/GameUtilsApi.h>
#include "C_PointOnTrajectory.h"
#include <SysCore/Frames/C_BitmapText.h>
#include <SysMath/C_Point.h>
#include <boost/shared_ptr.hpp>
#include <GameFramework/Operators/I_BaseOperator.h>

namespace scorpio{ namespace gameutils{

	namespace sccore = scorpio::syscore;
	namespace scmath = scorpio::sysmath;
	namespace scframework = scorpio::framework;

	//////////////////////////////////////////////////////////////////////////
	//@ C_ScorePointsEffect
	class GAMEUTILS_API C_ScorePointsEffect
	{
	public:
		//@ c-tor
		//@ startPos - center of entity
		C_ScorePointsEffect(sccore::T_BitmapText text, const scmath::C_Vector2 & startPos, const scmath::C_Vector2 & endPos, float speed, u32 renderLayer);
		//@ d-tor
		~C_ScorePointsEffect();


		//@ Tick
		void							Tick(u32 inDeltaTime);
		//@ IsDone
		bool							IsDone() const { return m_movingEffect->IsDone(); }

	private:
		C_PointOnTrajectory	*	m_movingEffect;	//logic of moving
		sccore::T_BitmapText		m_bitmapText;
		float							m_fScale;

		scframework::T_Operator	m_blendOp;	//operator fadeout alphy pointov
		scframework::T_Operator	m_scaleOp;	//operator for scaling text
	};


	typedef boost::shared_ptr<C_ScorePointsEffect>	T_ScorePointsEffect;

}}