#pragma once

#include <Common/Pragmas.h>

#ifdef _USRDLL
	#ifdef GAMEUTILS_EXPORTS
		#define GAMEUTILS_API __declspec(dllexport)
		#define GAMEUTILS_EXP_TEMP_INST
	#else
		#define GAMEUTILS_API __declspec(dllimport)
		#define GAMEUTILS_EXP_TEMP_INST extern
	#endif
#else
	#define GAMEUTILS_API
	#define GAMEUTILS_EXP_TEMP_INST
#endif
