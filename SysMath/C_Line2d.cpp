/*
	This file is part of the Hyperion project.
	Copyright (C) 2005 Stormheads. All rights reserved.
	http://www.stormheads.com

	For conditions of distribution and use, see the accompanying COPYING file.

	Author:
		Jozef 'oiko' Kral : Sat Oct 15 12:53:12 CET 2005
	Description:
		implementation file for C_Line2d class
*/

#include "C_Line2d.h"
#include "MathUtils.h"
#include <math.h>

namespace scorpio{ namespace sysmath{


	float C_Line2d::getLength() const
	{
		return ( float )sqrt( ( m_pEnd.m_x - m_pStart.m_x ) * ( m_pEnd.m_x - m_pStart.m_x ) + 
			( m_pEnd.m_y - m_pStart.m_y ) * ( m_pEnd.m_y - m_pStart.m_y ) );
	}

	bool C_Line2d::Intersection( const C_Line2d & pLine, C_Vector2 & vctIntOut ) const
	{
		//line p( this ) a1*X + b1*Y + c1 = 0;  [a1,b1] - normal of p
		float a1 = m_pEnd.m_y - m_pStart.m_y;
		float b1 = m_pStart.m_x - m_pEnd.m_x;
		//c1 = -a1*X - b1*Y [X,Y] - point from line p
		float c1 = -a1*m_pStart.m_x - b1*m_pStart.m_y;

		//line q( parameter ) a2*X + b2*Y + c2 = 0; [a2,b2] - normal of q
		float a2 = pLine.GetEnd().m_y - pLine.GetStart().m_y;
		float b2 = pLine.GetStart().m_x - pLine.GetEnd().m_x;
		//c2 = -a2*X_ - b2*Y_ [X_,Y_] - point from line q
		float c2 = -a2*pLine.GetStart().m_x - b2*pLine.GetStart().m_y;

		//lines can not be parralel!
		float ka = a1 / a2;
		float kb = b1 / b2;

		if ( isZero( ka - kb ) )
			return false;  //su rovnobezne ( mozno aj splivajuce ) - 0 alebo N priesecnikov



		//now we have common form of both lines
		vctIntOut.m_x = ( c1*b2 - b1*c2 ) / ( b1*a2 - a1*b2 );
		vctIntOut.m_y = ( a1*c2 - c1*a2 ) / ( b1*a2 - a1*b2 );

		return true;
	}

	float C_Line2d::distFromLine( C_Vector2 & pVct )
	{
		float a = m_pEnd.m_y - m_pStart.m_y;
		float b = m_pStart.m_x - m_pEnd.m_x;
		float c = -a*m_pStart.m_x - b*m_pStart.m_y;

		if ( isZero( a - b ) )
			return 0;

		float res = ( float )( ( a*pVct.m_x + b*pVct.m_y + c ) / sqrt( a*a + b*b ) );
		if ( res < 0 )
			res *= -1;

		return res;
	}

	float C_Line2d::distFromSegment( C_Vector2 & pVct )
	{
		if ( isInSegment( pVct ) )
			return distFromLine( pVct );

		float lnS = C_Vector2( m_pStart - pVct ).length();
		float lnE = C_Vector2( m_pEnd - pVct ).length();

		return ( lnS < lnE ) ? lnS : lnE;
	}

	void C_Line2d::projectToLine( C_Vector2 & pVctIn, C_Vector2 & pVctOut )
	{
		C_Vector2  direct( m_pEnd.m_x - m_pStart.m_x, m_pEnd.m_y - m_pStart.m_y );
		float u = paramInSegment( pVctIn );
		pVctOut. set( m_pStart.m_x + u*direct.m_x, m_pStart.m_y + u*direct.m_y );
	}

	bool C_Line2d::isInSegment( C_Vector2 & pVct )
	{
		float u = paramInSegment( pVct );
		if ( u >= 0. && u <= 1. )
			return true;
		return false;
	}

	//vypocet z parametrickeho tvaru vyjadrenia priamky a dot productu
	float C_Line2d::paramInSegment( C_Vector2 & pVct )
	{
		float u = ( ( pVct.m_x - m_pStart.m_x ) * ( m_pEnd.m_x - m_pStart.m_x ) ) +
			( ( pVct.m_y - m_pStart.m_y ) * ( m_pEnd.m_y - m_pStart.m_y ) );
		u /= ( ( m_pEnd.m_x - m_pStart.m_x ) * ( m_pEnd.m_x - m_pStart.m_x ) ) +
			( ( m_pEnd.m_y - m_pStart.m_y ) * ( m_pEnd.m_y - m_pStart.m_y ) );

		return u;
	}

	T_SidePosition C_Line2d::whichSide( C_Vector2 & pVct )
	{
		/*C_Vector2  vctDir( *m_pEnd - *m_pStart );
		vctDir.normalize();
		C_Vector2  vctDir_( pVct - *m_pStart );
		vctDir_.normalize();

		float dot = vctDir % vctDir_;

		if ( isZero( fabs( dot ) - 1.0 ) )
		return ONIT;

		//line p( this ) a*X + b*Y + c = 0;  [a,b] - normal of p
		float a = m_pEnd.m_y - m_pStart.m_y;
		float b = m_pStart.m_x - m_pEnd.m_x;
		//c1 = -a*X - b*Y [X,Y] - point from line p
		float c = -a*m_pStart.m_x - b*m_pStart.m_y;

		float d = a*pVct.m_x + b*pVct.m_y + c;
		if ( d < 0.0 )
		return ONLEFT;
		else
		return ONRIGHT;*/

		if ( isLeft( pVct ) )
			return E_SP_LEFT;
		else if ( isRight( pVct ) )
			return E_SP_RIGHT;
		else
			return E_SP_ON;
	}

	float C_Line2d::area2( C_Vector2 & pVct )
	{
		float area = m_pStart.m_x * m_pEnd.m_y - m_pStart.m_y * m_pEnd.m_x +
			m_pStart.m_y * pVct.m_x - m_pStart.m_x * pVct.m_y +
			m_pEnd.m_x * pVct.m_y - m_pEnd.m_y * pVct.m_x;
		return ( float )area;
	}

	bool C_Line2d::isLeft( C_Vector2 & pVct )
	{
		return area2( pVct ) > 0.F;
	}
	bool C_Line2d::isRight( C_Vector2 & pVct )
	{
		return area2( pVct ) < 0.F;
	}
	bool C_Line2d::isCollinear( C_Vector2 & pVct )
	{
		return area2( pVct ) == 0.F;
	}
	bool C_Line2d::isBetween( C_Vector2 & pVct )
	{
		if ( !isCollinear( pVct ) )
			return false;

		bool bRes = false;
		if ( !isZero( m_pStart.m_x - m_pEnd.m_x ) )
			bRes = ( ( m_pStart.m_x <= pVct.m_x ) && ( pVct.m_x <= m_pEnd.m_x ) ) ||
			( ( m_pStart.m_x >= pVct.m_x ) && ( pVct.m_x >= m_pEnd.m_x ) );
		else
			bRes = ( ( m_pStart.m_y <= pVct.m_y ) && ( pVct.m_y <= m_pEnd.m_y ) ) ||
			( ( m_pStart.m_y >= pVct.m_y ) && ( pVct.m_y >= m_pEnd.m_y ) );

		return bRes;
	}
}}