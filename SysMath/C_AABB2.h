/*
* filename:			C_AABB2.h
*
* author:			Kral Jozef
* date:				Januar 2007
* version:			1.00
* description:		2D AABB
*/

#pragma once

#include "C_Vector2.h"
#include <float.h>

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ C_AABB2
	class SYSMATH_API C_AABB2
	{
	public:
		C_AABB2() : m_min(C_Vector2(FLT_MAX, FLT_MAX)), m_max(C_Vector2(-FLT_MAX, -FLT_MAX)) {};
		//@ c-tor
		C_AABB2(const C_Vector2 & vct1) : m_min(vct1), m_max(vct1) {};
		//@ c-tor
		C_AABB2(const C_Vector2 & vct1, const C_Vector2 & vct2);

		//@ Translate
		//@ Move AABB by center to vctPos
		void						Translate(const C_Vector2 & vctPos);
		//@ TranslateBy - offset - move min/max by offset value
		void						TranslateBy(const C_Vector2 & offsetValue);
		//@ Include
		void						Include(const C_Vector2 & vct);
		//@ Include
		void						Include(const C_AABB2 & aabb);
		//@ GetWidth
		float						GetWidth() const { return m_max.m_x - m_min.m_x; }
		//@ GetHeight
		float						GetHeight() const { return m_max.m_y - m_min.m_y; }
		//@ GetMin
		const C_Vector2	&	GetMin() const { return m_min; }
		//@ GetMax
		const C_Vector2	&	GetMax() const { return m_max; }
		//@ Invalidate
		void						Invalidate() { m_min = C_Vector2(FLT_MAX, FLT_MAX); m_max = C_Vector2(FLT_MIN, FLT_MIN); }
		//@ IsInvalid
		bool						IsInvalid() const { return ((m_min.m_x > m_max.m_x) || (m_min.m_y > m_max.m_y)); }
		//@ IsValid
		bool						IsValid() const { return !IsInvalid(); }
		//@ GetCenter
		C_Vector2				GetCenter() const { return C_Vector2(m_min.m_x + (m_max.m_x - m_min.m_x) / 2.f, m_min.m_y + (m_max.m_y - m_min.m_y) / 2.f); }
		//@ IsIntersect
		bool						IsIntersect(const C_AABB2 & aabb) const;
		//@ IsIntersect
		bool						IsIntersect(const C_Vector2 & vctPos) const;

	private:
		C_Vector2	m_min;
		C_Vector2	m_max;
	};
}}