/*
* filename:			C_Point.h
*
* author:			Kral Jozef
* date:				03/18/2009		20:33
* version:			1.00
* brief:				simple class for point holding
*/

#pragma once

#include <Common/types.h>
#include <SysMath/MathUtils.h>

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Point
	class C_Point
	{
	public:
		//@ c-tor
		C_Point() : x(0), y(0) {};
		//@ init c-tor
		C_Point(s32 inX, s32 inY) : x(inX), y(inY) {};
		//@ copy c-tor
		C_Point(const C_Point & copy) : x(copy.x), y(copy.y) {}
		//@ assignment operator
		const C_Point & operator=(const C_Point & copy)
		{
			x = copy.x;
			y = copy.y;
			return *this;
		}

		//@ operatior += 
		const C_Point & operator += (const C_Point & inPoint)
		{
			x += inPoint.x;
			y += inPoint.y;
			return *this;
		}

		//@ operatior -= 
		const C_Point & operator -= (const C_Point & inPoint)
		{
			x -= inPoint.x;
			y -= inPoint.y;
			return *this;
		}

		//@ operator *-
		const C_Point & operator *= (float fVal)
		{
			x = round(x*fVal);
			y = round(y*fVal);
			return *this;
		}

		//@ operator +
		C_Point operator + (const C_Point & inPoint) const
		{
			return C_Point(x + inPoint.x, y + inPoint.y);
		}

		//@ operator -
		C_Point operator - (const C_Point & inPoint) const
		{
			return C_Point(x - inPoint.x, y - inPoint.y);
		}
	
	public:
		s32		x;	//x-coord
		s32		y;	//y-coord
	};


	inline C_Point operator * (float fVal, const C_Point & pos)
	{
		return C_Point(round(pos.x * fVal), round(pos.y * fVal));
	}

	inline C_Point operator * (u32 uVal, const C_Point & pos)
	{
		return C_Point(pos.x * uVal, pos.y * uVal);
	}

}}