/*
* filename:			C_Vector.h
*
* author:				Kral Jozef
* date:					September 2002
* update:				December 2006
* version:			1.00
* description:	Defines 3D/2D Vector operation
*/

#include "C_Vector.h"
#include "C_Matrix.h"

namespace scorpio{ namespace sysmath{

	C_Vector3::C_Vector3(const C_Vector3 & copy)
	{
		m_x = copy.m_x;
		m_y = copy.m_y;
		m_z = copy.m_z;
	}

	C_Vector3::~C_Vector3()
	{
#ifdef DEBUG
		m_x = m_y = m_z = 0;
#endif
	}

	void C_Vector3::operator=(const C_Vector3 & copy)
	{
		m_x = copy.m_x;
		m_y = copy.m_y;
		m_z = copy.m_z;
	}
	void C_Vector3::operator=(const float fVal)
	{
		m_x = fVal;
		m_y = fVal;
		m_z = fVal;
	}

	C_Vector3 C_Vector3::operator/(const float fVal)
	{
		C_Vector3 pom;
		pom.m_x	=	m_x / fVal;
		pom.m_y	=	m_y / fVal;
		pom.m_z	=	m_z / fVal;
		return pom;
	}

	C_Vector3 C_Vector3::operator/(const u32 iVal)
	{
		C_Vector3 pom;
		pom.m_x	=	m_x / iVal;
		pom.m_y	=	m_y / iVal;
		pom.m_z	=	m_z / iVal;
		return pom;
	}

	void C_Vector3::operator +=(const C_Vector3 & pVct)
	{
		m_x += pVct.m_x;
		m_y += pVct.m_y;
		m_z += pVct.m_z;
	}
	void C_Vector3::operator -=(const C_Vector3 & pVct)
	{
		m_x -= pVct.m_x;
		m_y -= pVct.m_y;
		m_z -= pVct.m_z;
	}
	void C_Vector3::operator *=(const double dVal)
	{
		m_x *= (float)dVal;
		m_y *= (float)dVal;
		m_z *= (float)dVal;
	}
	void C_Vector3::operator /=(const double dVal)
	{
		m_x /= (float)dVal;
		m_y /= (float)dVal;
		m_z /= (float)dVal;
	}


	float C_Vector3::length() const
	{	
		return (float)sqrt((m_x*m_x) + (m_y*m_y) + (m_z*m_z));
	}

	void C_Vector3::normalize()
	{
		float fLen = length();
		m_x /= fLen;
		m_y /= fLen;
		m_z /= fLen;
	}

	void C_Vector3::operator *=(const C_Matrix4x4 & matrix)
	{
		C_Vector3	vctTmp;
		vctTmp.m_x = m_x*matrix.m_matrix[0] + m_y*matrix.m_matrix[4] + m_z*matrix.m_matrix[8];
		vctTmp.m_y = m_x*matrix.m_matrix[1] + m_y*matrix.m_matrix[5] + m_z*matrix.m_matrix[9];
		vctTmp.m_z = m_x*matrix.m_matrix[2] + m_y*matrix.m_matrix[6] + m_z*matrix.m_matrix[10];
		m_x = vctTmp.m_x;
		m_y = vctTmp.m_y;
		m_z = vctTmp.m_z;
	}
}}