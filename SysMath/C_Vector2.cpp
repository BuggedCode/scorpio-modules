/*
* filename:			C_Vector2.h
*
* author:			Kral Jozef
* date:				October 2005
* version:			1.00
* description:		2D vector math, this class was my part of the Hyperion project by Stormheads(2005)
*/

#include "C_Vector2.h"
#include <math.h>
#include "MathUtils.h"
#include <SysMath/C_Matrix2d.h>

namespace scorpio{ namespace sysmath{

	const C_Vector2 C_Vector2::C_ONE = C_Vector2(1.f, 1.f);
	const C_Vector2 C_Vector2::C_ZERO = C_Vector2(0.f, 0.f);

	//////////////////////////////////////////////////////////////////////////
	//
	//@ operator ==
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Vector2::operator ==(const C_Vector2 & vct) const
	{
		return (isZero(m_x-vct.m_x) && isZero(m_y-vct.m_y));
	}
	/////////////////////////////////////////////////////////////////////////////
	float C_Vector2::length()
	{
		return sqrt( m_x*m_x + m_y*m_y );
	}

	/////////////////////////////////////////////////////////////////////////////
	float C_Vector2::length2()
	{
		return ( m_x*m_x + m_y*m_y );
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_Vector2::normalize()
	{
		float fLen = length();
		if ( !isZero( fLen ) )
		{
			m_x = m_x / fLen;
			m_y = m_y / fLen;
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_Vector2::ortho()
	{
		float fVal = m_x;
		m_x = m_y;
		m_y = -fVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_Vector2::rotateBy( float fAng )
	{
		double radAng = DEG2RAD( fAng );
		float newX = (float)( m_x*cos( radAng ) - m_y*sin( radAng ) );
		float newY = (float)( m_x*sin( radAng ) + m_y*cos( radAng ) );
		m_x = newX;
		m_y = newY;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ getAngle
	//
	//////////////////////////////////////////////////////////////////////////
	float C_Vector2::getAngle(const C_Vector2 & vctNorm) const
	{
		double thisAngle = atan2(m_y, m_x);
		double vctAngle = atan2(vctNorm.m_y, vctNorm.m_x);
		return (float)RAD2DEG(vctAngle - thisAngle);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ getAngle
	//
	//////////////////////////////////////////////////////////////////////////
	float C_Vector2::getAngle() const
	{
		double thisAngle = atan2(m_y, m_x);
		return (float)RAD2DEG(thisAngle);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Transform by matrix
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Vector2::operator *= (const C_Matrix3x3 & matrix)
	{
		C_Vector2	vctTmp;
		vctTmp.m_x = m_x * matrix.m_Matrix[0] + m_y * matrix.m_Matrix[3] + matrix.m_Matrix[6];
		vctTmp.m_y = m_x * matrix.m_Matrix[1] + m_y * matrix.m_Matrix[4] + matrix.m_Matrix[7];
		m_x = vctTmp.m_x;
		m_y = vctTmp.m_y;
	}
}}