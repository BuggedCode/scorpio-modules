/*
* filename:			C_Matrix2d.h
*
* author:			Kral Jozef
* date:				02/16/2011		13:35
* version:			1.00
* brief:				Martix with rotation and translation in 2D
*/

#pragma once

#include <SysMath/SysMathApi.h>
#include <Common/macros.h>
#include <Common/Types.h>

//	m[i,j] i-row, j-column
//	m0	m1	m2	
//	m3	m4	m5
//	m6	m7	m8

/*Translation 	
|  1  0 0 |
|  0  1 0 |
| tx ty 1 |

Scaling 	
| sx  0 0 |
| 0  sy 0 |
| 0  0  1 |

Rotation 	

| cos(v)  sin(v) 0 |
| -sin(v) cos(v) 0 |
|     0    0     1 |
*/

namespace scorpio{ namespace sysmath{

	class C_Vector2;

	//////////////////////////////////////////////////////////////////////////
	//
	class SYSMATH_API C_Matrix3x3
	{
		friend class C_Vector2;
	public:
		//@ c-tor - create identity matrix
		C_Matrix3x3();
		//@ copy c-tor
		C_Matrix3x3(const C_Matrix3x3 & copy);
		
		//@ Identity
		void			Identity()
		{ 
			ZERO_ARRAY(m_Matrix); 
			m_Matrix[0] = m_Matrix[4] = m_Matrix[8] = 1.f; 
		}


		//@ Algebraic operations on matrixes
		C_Matrix3x3	operator*(const C_Matrix3x3 & mat);

		//@ SetTranslation
		void			SetTranslation(const C_Vector2 & vctPos);
		//@ SetRotation
		void			SetRotation(float fAngle);
		//@ SetScale
		//void			SetScale(float fScale);	//uniform scale
		//@ SetScale
		//void			SetScale(const C_Vector2 & vctScale);	//non uniform scale

		//@ AddPos
		void			AddPos(const C_Vector2 & vctPos);
		//@ AddRot
		void			AddRot(float fAngle);
		//@ AddScale
		void			AddScale(float fScale);	//uniform scale
		//@ AddScale
		//void			AddScale(const C_Vector2 & vctScale);	//non uniform scale

	private:
		const float	&	operator[](const u32 index) const { return m_Matrix[index]; }


	private:
		float	m_Matrix[9];
	};

}}