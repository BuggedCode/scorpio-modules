#pragma once

#ifndef SYSMATH_H__
#define SYSMATH_H__

	#ifdef _USRDLL
		#ifdef SYSMATH_EXPORTS
			#define SYSMATH_API __declspec(dllexport)
		#else
			#define SYSMATH_API __declspec(dllimport)
		#endif
	#else
		#define SYSMATH_API
	#endif

#endif //SYSMATH_H__

#ifdef USE_BOOST_SMARTPTR
	#include <boost/shared_ptr.hpp>
#else
	#include <Common/C_SmartPtr.h>
#endif


#ifdef USE_BOOST_SMARTPTR
	#undef DECLARE_SHARED_PTR
	#define	DECLARE_SHARED_PTR(ClassType, ClassName) \
		typedef boost::shared_ptr<ClassType>	ClassName;
#else
	#undef DECLARE_SHARED_PTR
	#define	DECLARE_SHARED_PTR(ClassType, ClassName) \
		typedef C_SharedPtr<ClassType>			ClassName;
#endif