/*
* filename:			C_Plane.h
*
* author:				Kral Jozef
* date:					Januar 2007
* version:			1.00
* description:	Plane class
*/

#pragma once


#include "SysMathApi.h"

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@
	class SYSMATH_API C_Plane
	{
	public:
		C_Plane(void);
	public:
		~C_Plane(void);
	};
}}