/*
* filename:			C_BSphere2.cpp
*
* author:			Kral Jozef
* date:				03/04/2011		13:22
* version:			1.00
* brief:
*/

#include "C_BSphere2.h"


namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsIntersection
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_BSphere2::IsIntersection(const C_AABB2 & aabb) const
	{
		//@ check all corners
		C_Vector2 vct = aabb.GetMin() - m_Pivot;
		if (vct.length2() < m_Radius2)
			return true;

		vct = aabb.GetMax() - m_Pivot;
		if (vct.length2() < m_Radius2)
			return true;

		vct.m_x = aabb.GetMin().m_x - m_Pivot.m_x;
		vct.m_y = aabb.GetMax().m_y - m_Pivot.m_y;
		if (vct.length2() < m_Radius2)
			return true;

		vct.m_x = aabb.GetMax().m_x - m_Pivot.m_x;
		vct.m_y = aabb.GetMin().m_y - m_Pivot.m_y;
		if (vct.length2() < m_Radius2)
			return true;

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ComputeAABB
	//
	//////////////////////////////////////////////////////////////////////////
	C_AABB2 C_BSphere2::ComputeAABB() const
	{
		C_AABB2 aabb;
		aabb.Include(m_Pivot + C_Vector2(m_Radius, m_Radius));
		aabb.Include(m_Pivot - C_Vector2(m_Radius, m_Radius));
		return aabb;
	}
}}
