/*
* filename:			C_Vector2.h
*
* author:			Kral Jozef
* date:				October 2005
* update:			Januar 2007
* version:			1.00
* description:		2D vector math, this class was my part of the Hyperion project by Stormheads(2005)
*/

#pragma once

#include "SysMathApi.h"
#include <Common/Types.h>

namespace scorpio{ namespace sysmath{

	class C_Matrix3x3;

	/////////////////////////////////////////////////////////////////////////////
	class SYSMATH_API C_Vector2
	{
	public:
		float		m_x;
		float		m_y;


		static const C_Vector2 C_ZERO;
		static const C_Vector2 C_ONE;

	public:
		C_Vector2() : m_x(0), m_y(0) {};
		C_Vector2( float pX, float pY ) : m_x(pX), m_y(pY) { ; }
		C_Vector2( const C_Vector2 & toCopy ) { m_x = toCopy.m_x; m_y = toCopy.m_y; }
		//virtual ~C_Vector2() {};

		//Set/Get methods
		void			set(float pValX, float pValY) { m_x = pValX; m_y = pValY; }

		//Operators  
		C_Vector2	operator=(const C_Vector2 & vct) { m_x = vct.m_x; m_y = vct.m_y; return *this; }
		void			operator += (const C_Vector2 & vct) { m_x += vct.m_x;	m_y += vct.m_y; }
		void			operator -= (const C_Vector2 & vct) { m_x -= vct.m_x;	m_y -= vct.m_y; }
		void			operator *= (float value) { m_x *= value;	m_y *= value; }
		void			operator /= (float value) { m_x /= value;	m_y /= value; }

		C_Vector2	operator+(const C_Vector2 & vct) const { return C_Vector2(m_x+vct.m_x, m_y+vct.m_y); }
		C_Vector2	operator-(const C_Vector2 & vct) const { return C_Vector2(m_x-vct.m_x, m_y-vct.m_y); }
		C_Vector2	operator*(const C_Vector2 & vct) const { return C_Vector2( m_x*vct.m_y - m_y*vct.m_x, m_y*vct.m_x - m_x*vct.m_y ); }	//cross product
		C_Vector2	operator*(float pVal) const { return C_Vector2( m_x*pVal, m_y*pVal ); }
		float  		operator%(const C_Vector2 & vct) const { return m_x*vct.m_x + m_y*vct.m_y; }	//dot product
		bool			operator==(const C_Vector2 & vct) const;
		bool			operator!=(const C_Vector2 & vct) { return !(*this == vct); }	
		
		//@ Transform vector by C_Matrix3x3
		void			operator*=(const C_Matrix3x3 & matrix);

		//Other methods
		float     length();
		float     length2();
		void      normalize();
		void      rotateBy( float fAng );
		void      ortho();

		//@ getAngle - return angle between 2 vectors
		//@ this & vctNorm has to be normalized!
		//@ return angle need to be rotated from this to vctNorm pos - ccw orientation!
		float			getAngle(const C_Vector2 & vctNorm) const;
		//@ getAngle
		float			getAngle() const;
	};

	inline C_Vector2 operator * (float fVal, const C_Vector2 & vct)
	{
		return C_Vector2(vct.m_x * fVal, vct.m_y * fVal);
	}

	inline C_Vector2 operator * (u32 nVal, const C_Vector2 & vct)
	{
		return C_Vector2(vct.m_x * nVal, vct.m_y * nVal);
	}

	inline C_Vector2 operator / (const C_Vector2 & vct, float fVal)
	{
		return C_Vector2(vct.m_x / fVal, vct.m_y / fVal);
	}
}}