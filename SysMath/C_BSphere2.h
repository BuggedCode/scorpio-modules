/*
* filename:			C_BSphere2.h
*
* author:			Kral Jozef
* date:				Januar 2007
* version:			1.00
* description:		2D Bound Sphere class
*/

#pragma once

#include "C_Vector2.h"
#include "C_AABB2.h"

namespace scorpio{ namespace sysmath{


	//////////////////////////////////////////////////////////////////////////
	//@ C_BSphere
	class SYSMATH_API C_BSphere2
	{
	public:
		//@ c-tor
		C_BSphere2() : m_Radius(0), m_Radius2(0) {};
		//@ c-tor
		C_BSphere2(const C_Vector2 & pivot, const float radius) 
			: m_Pivot(pivot), m_Radius(radius), m_Radius2(radius*radius) {};
		//C_BSphere(const C_BBox & box);
		~C_BSphere2() {};

		//void Include(const C_BSphere2 & sphere);
		//void Include(const C_AABB2 & box);

		//@ IsIntersection
		bool							IsIntersection(const C_AABB2 & aabb) const;
		bool							IsIntersection(const C_Vector2 & vctPos) const { return (C_Vector2(vctPos - m_Pivot).length2() < m_Radius2);}

		//@ GetCenter
		const C_Vector2	&		GetCenter() const { return m_Pivot; }
		//@ GetRadius
		float							GetRadius() const { return m_Radius; }
		//@ GetRadius2
		float							GetRadius2() const { return m_Radius2; }
		//@ ComputeAABB
		C_AABB2						ComputeAABB() const;


	private:
		C_Vector2	m_Pivot;
		float			m_Radius;
		float			m_Radius2;
	};
}}