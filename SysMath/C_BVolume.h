/*
* filename:			C_BVolume.h
*
* author:				Kral Jozef
* date:					Januar 2007
* version:			1.00
* description:	Bounding volume class
*/

#pragma once

#include "C_BBox.h"
#include "C_BSphere.h"

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ C_BVolume
	class SYSMATH_API C_BVolume
	{
	public:
		C_BVolume(const C_BBox & box, const C_BSphere & sphere) : m_bBox(box), m_bSphere(sphere) {};
		~C_BVolume() {};

		//private:
		__declspec (deprecated) C_BVolume() : m_bBox(C_Vector3(1,2,3),C_Vector3(4,5,6)), m_bSphere(m_bBox) {};	//not implemented

	private:
		C_BBox			m_bBox;
		C_BSphere		m_bSphere;
	};
}}