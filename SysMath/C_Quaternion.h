/*
* filename:			C_Quaternion.h
*
* author:				Kral Jozef
* date:					Januar 2007
* version:			1.00
* description:	Quaternion operation
*/

#pragma once

#include <math.h>
#include "SysMathApi.h"

namespace scorpio{ namespace sysmath{

	class C_Matrix4x4;
	class C_Vector3;

	/////////////////////////////////////////////////////////////////////////////
	//@ C_Quaternion
	class SYSMATH_API C_Quaternion
	{
	public:
		C_Quaternion();
		C_Quaternion(const C_Quaternion & copy);
		__declspec (deprecated)	C_Quaternion(float xAngle, float yAngle, float zAngle);	//construct from euler angles
		C_Quaternion(const C_Vector3 & vctDir, float fAngle);	//construct from axis & angle
		//C_Quaternion(const C_Matrix4x4 & matrix);	//construct from matrix http://www.gamasutra.com/features/19980703/quaternions_01.htm
		virtual ~C_Quaternion();
		void operator= (const C_Quaternion & copy);

		void				normalize();
		float				length() const { return sqrt(m_x*m_x + m_y*m_y + m_z*m_z + m_w*m_w); }
		float				length2() const { return (m_x*m_x + m_y*m_y + m_z*m_z + m_w*m_w); }
		void				toMatrix(C_Matrix4x4 & matrix) const;
		void				toAxisAngle(C_Vector3 & vctDir, float & fAngle) const;
		//void				toEuler(float & xAngle, float & yAngle, float & zAngle) const;	not implemented

		void				operator*=(const C_Quaternion & quat);
		C_Quaternion	operator*(const C_Quaternion & quat);

	private:
		float		m_x;
		float		m_y;
		float		m_z;
		float		m_w;
	};
}}