/*
* filename:			MathTypes.h
*
* author:			Kral Jozef
* date:				October 2005
* update:			Januar 2008
* version:			1.00
* description:		own types for math
*/

#pragma once

namespace scorpio{ namespace sysmath{
	
	enum T_SidePosition
	{
		//Line position
		E_SP_UNDEF,
		E_SP_ON,
		E_SP_LEFT,
		E_SP_RIGHT,

		//poly position
		E_SP_INSIDE,
		E_SP_OUTSIDE,
	};
}}