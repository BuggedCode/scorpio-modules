/*
* filename:			C_BSphere.h
*
* author:				Kral Jozef
* date:					Januar 2007
* version:			1.00
* description:	Sphere class
*/

#pragma once

#include "C_Vector.h"

namespace scorpio{ namespace sysmath{

	class C_BBox;

	//////////////////////////////////////////////////////////////////////////
	//@ C_BSphere
	class SYSMATH_API C_BSphere
	{
	public:
		C_BSphere(const C_Vector3 & pivot, const float radius) : m_pivot(pivot), m_radius(radius) {};
		C_BSphere(const C_BBox & box);
		~C_BSphere() {};

		void include(const C_BSphere & sphere);	

	private:
		C_BSphere();	//not implemented

	private:
		C_Vector3	m_pivot;
		float		m_radius;
	};
}}