/*
* filename:			MathUtils.h
*
* author:				Kral Jozef
* date:					September 2002
* update:				October 2005
* version:			1.10
* description:	MathUtils
*/

#pragma once

#include "SysMathApi.h"
#include <Common/Types.h>
#include <Common/Assert.h>

//@ Prototypes
namespace scorpio{ namespace sysmath{
   
   bool isZero( double pValue );
   bool isZero( double pValue, float pEpsilon );
   float correctAngle(float pValue);
   s32 round(double pValue);
}}


#include "MathUtils.inl"

namespace scorpio{ namespace sysmath{

#ifndef PI
	#define	PI	3.1415926535897932384626433832795f
#endif

#ifndef	DEG
	#define	DEG	PI/180
	#define	DEG2RAD(x)	((x)*PI/180)
#endif
#ifndef	RAD
	#define	RAD	180/PI
	#define	RAD2DEG(x)	((x)*180/PI)
#endif


	//Mathematic functions
	inline bool	isZero( double pValue );
	inline bool	isZero( double pValue, float pEpsilon );
	//inline void	correctAngle(double & pValue);
	inline float	correctAngle(float pValue);

	inline s32	round(double);

	//@ next power of 2
	inline s32 next_p2(int a)
	{
		int rval=1;
		while(rval<a) 
			rval<<=1;
		return rval;
	}


	template<typename T>
	T ClampValue(const T & value, const T & lowerBound, const T & upperBound)
	{
		SC_ASSERT(lowerBound <= upperBound);

		if ( value < lowerBound )
			return lowerBound;

		if (value > upperBound)
			return upperBound;

		return value;
	}

	template<typename T>
	T RemapValue(const T & value, const T & src0, const T & src1, const T & dst0, const T & dst1)
	{
		SC_ASSERT(value >= src0 && value <= src1);
		SC_ASSERT(src0 != src1);
		return dst0 + (value - src0) / (src1 - src0) * (dst1 - dst0);
	}

}}