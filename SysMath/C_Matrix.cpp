/*
* filename:			C_Matrix.cpp
*
* author:			Kral Jozef
* date:				December 2006
* version:			1.00
* description:		Matrix operation
*/

#include "C_Matrix.h"
#include <math.h>
#include "MathUtils.h"
#include "C_Vector.h"
#include <Common/macros.h>
#include <Common/Assert.h>

namespace scorpio{ namespace sysmath{

	C_Matrix4x4::C_Matrix4x4()
	{
		identity();
	}

	C_Matrix4x4::C_Matrix4x4(const float matrix[16])
	{
      MEM_CPY(m_matrix, 16, matrix, 16);
	}

	C_Matrix4x4::C_Matrix4x4(const C_Matrix4x4 &copy)
	{
      MEM_CPY(m_matrix, 16, copy.m_matrix, 16);
	}

	C_Matrix4x4::~C_Matrix4x4()
	{
#ifdef DEBUG
		ZERO_ARRAY(m_matrix);
#endif
	}

	void C_Matrix4x4::operator=(const C_Matrix4x4 & copy)
	{
      MEM_CPY(m_matrix, 16, copy.m_matrix, 16);
	}

	void C_Matrix4x4::identity()
	{
		ZERO_ARRAY(m_matrix);
		m_matrix[0] = m_matrix[5] = m_matrix[10] = m_matrix[15] = 1;
	}

	C_Matrix4x4	C_Matrix4x4::operator+(const C_Matrix4x4 & mat)
	{
		C_Matrix4x4	result;

		result.m_matrix[0] = m_matrix[0] + mat[0];
		result.m_matrix[1] = m_matrix[1] + mat[1];
		result.m_matrix[2] = m_matrix[2] + mat[2];
		result.m_matrix[3] = m_matrix[3] + mat[3];

		result.m_matrix[4] = m_matrix[4] + mat[4];
		result.m_matrix[5] = m_matrix[5] + mat[5];
		result.m_matrix[6] = m_matrix[6] + mat[6];
		result.m_matrix[7] = m_matrix[7] + mat[7];

		result.m_matrix[8] = m_matrix[8] + mat[8];
		result.m_matrix[9] = m_matrix[9] + mat[9];
		result.m_matrix[10] = m_matrix[10] + mat[10];
		result.m_matrix[11] = m_matrix[11] + mat[11];

		result.m_matrix[12] = m_matrix[12] + mat[12];
		result.m_matrix[13] = m_matrix[13] + mat[13];
		result.m_matrix[14] = m_matrix[14] + mat[14];
		result.m_matrix[15] = m_matrix[15] + mat[15];

		return result;
	}
	C_Matrix4x4	C_Matrix4x4::operator-(const C_Matrix4x4 & mat)
	{
		C_Matrix4x4	result;

		result.m_matrix[0] = m_matrix[0] - mat[0];
		result.m_matrix[1] = m_matrix[1] - mat[1];
		result.m_matrix[2] = m_matrix[2] - mat[2];
		result.m_matrix[3] = m_matrix[3] - mat[3];

		result.m_matrix[4] = m_matrix[4] - mat[4];
		result.m_matrix[5] = m_matrix[5] - mat[5];
		result.m_matrix[6] = m_matrix[6] - mat[6];
		result.m_matrix[7] = m_matrix[7] - mat[7];

		result.m_matrix[8] = m_matrix[8] - mat[8];
		result.m_matrix[9] = m_matrix[9] - mat[9];
		result.m_matrix[10] = m_matrix[10] - mat[10];
		result.m_matrix[11] = m_matrix[11] - mat[11];

		result.m_matrix[12] = m_matrix[12] - mat[12];
		result.m_matrix[13] = m_matrix[13] - mat[13];
		result.m_matrix[14] = m_matrix[14] - mat[14];
		result.m_matrix[15] = m_matrix[15] - mat[15];

		return result;
	}
	C_Matrix4x4	C_Matrix4x4::operator*(const C_Matrix4x4 & mat)
	{
		C_Matrix4x4 result;

		result.m_matrix[0] = m_matrix[0]*mat[0] + m_matrix[4]*mat[1] + m_matrix[8]*mat[2] + m_matrix[12]*mat[3];
		result.m_matrix[1] = m_matrix[1]*mat[0] + m_matrix[5]*mat[1] + m_matrix[9]*mat[2] + m_matrix[13]*mat[3];
		result.m_matrix[2] = m_matrix[2]*mat[0] + m_matrix[6]*mat[1] + m_matrix[10]*mat[2] + m_matrix[14]*mat[3];
		result.m_matrix[3] = m_matrix[3]*mat[0] + m_matrix[7]*mat[1] + m_matrix[11]*mat[2] + m_matrix[15]*mat[3];

		result.m_matrix[4] = m_matrix[0]*mat[4] + m_matrix[4]*mat[5] + m_matrix[8]*mat[6] + m_matrix[12]*mat[7];
		result.m_matrix[5] = m_matrix[1]*mat[4] + m_matrix[5]*mat[5] + m_matrix[9]*mat[6] + m_matrix[13]*mat[7];
		result.m_matrix[6] = m_matrix[2]*mat[4] + m_matrix[6]*mat[5] + m_matrix[10]*mat[6] + m_matrix[14]*mat[7];
		result.m_matrix[7] = m_matrix[3]*mat[4] + m_matrix[7]*mat[5] + m_matrix[11]*mat[6] + m_matrix[15]*mat[7];

		result.m_matrix[8] = m_matrix[0]*mat[8] + m_matrix[4]*mat[9] + m_matrix[8]*mat[10] + m_matrix[12]*mat[11];
		result.m_matrix[9] = m_matrix[1]*mat[8] + m_matrix[5]*mat[9] + m_matrix[9]*mat[10] + m_matrix[13]*mat[11];
		result.m_matrix[10] = m_matrix[2]*mat[8] + m_matrix[6]*mat[9] + m_matrix[10]*mat[10] + m_matrix[14]*mat[11];
		result.m_matrix[11] = m_matrix[3]*mat[8] + m_matrix[7]*mat[9] + m_matrix[11]*mat[10] + m_matrix[15]*mat[11];

		result.m_matrix[12] = m_matrix[0]*mat[12] + m_matrix[4]*mat[13] + m_matrix[8]*mat[14] + m_matrix[12]*mat[15];
		result.m_matrix[13] = m_matrix[1]*mat[12] + m_matrix[5]*mat[13] + m_matrix[9]*mat[14] + m_matrix[13]*mat[15];
		result.m_matrix[14] = m_matrix[2]*mat[12] + m_matrix[6]*mat[13] + m_matrix[10]*mat[14] + m_matrix[14]*mat[15];
		result.m_matrix[15] = m_matrix[3]*mat[12] + m_matrix[7]*mat[13] + m_matrix[11]*mat[14] + m_matrix[15]*mat[15];

		return result;
	}
	C_Matrix4x4	C_Matrix4x4::operator*(const float & fVal)
	{
		C_Matrix4x4	result;

		result.m_matrix[0] = m_matrix[0] * fVal;
		result.m_matrix[1] = m_matrix[1] * fVal;
		result.m_matrix[2] = m_matrix[2] * fVal;
		result.m_matrix[3] = m_matrix[3] * fVal;

		result.m_matrix[4] = m_matrix[4] * fVal;
		result.m_matrix[5] = m_matrix[5] * fVal;
		result.m_matrix[6] = m_matrix[6] * fVal;
		result.m_matrix[7] = m_matrix[7] * fVal;

		result.m_matrix[8] = m_matrix[8] * fVal;
		result.m_matrix[9] = m_matrix[9] * fVal;
		result.m_matrix[10] = m_matrix[10] * fVal;
		result.m_matrix[11] = m_matrix[11] * fVal;

		result.m_matrix[12] = m_matrix[12] * fVal;
		result.m_matrix[13] = m_matrix[13] * fVal;
		result.m_matrix[14] = m_matrix[14] * fVal;
		result.m_matrix[15] = m_matrix[15] * fVal;

		return result;
	}
	void C_Matrix4x4::operator+=(const C_Matrix4x4 & mat)
	{
		m_matrix[0] += mat[0];
		m_matrix[1] += mat[1];
		m_matrix[2] += mat[2];
		m_matrix[3] += mat[3];

		m_matrix[4] += mat[4];
		m_matrix[5] += mat[5];
		m_matrix[6] += mat[6];
		m_matrix[7] += mat[7];

		m_matrix[8] += mat[8];
		m_matrix[9] += mat[9];
		m_matrix[10] += mat[10];
		m_matrix[11] += mat[11];

		m_matrix[12] += mat[12];
		m_matrix[13] += mat[13];
		m_matrix[14] += mat[14];
		m_matrix[15] += mat[15];
	}
	void C_Matrix4x4::operator-=(const C_Matrix4x4 & mat)
	{
		m_matrix[0] -= mat[0];
		m_matrix[1] -= mat[1];
		m_matrix[2] -= mat[2];
		m_matrix[3] -= mat[3];

		m_matrix[4] -= mat[4];
		m_matrix[5] -= mat[5];
		m_matrix[6] -= mat[6];
		m_matrix[7] -= mat[7];

		m_matrix[8] -= mat[8];
		m_matrix[9] -= mat[9];
		m_matrix[10] -= mat[10];
		m_matrix[11] -= mat[11];

		m_matrix[12] -= mat[12];
		m_matrix[13] -= mat[13];
		m_matrix[14] -= mat[14];
		m_matrix[15] -= mat[15];
	}
	void C_Matrix4x4::operator*=(const C_Matrix4x4 & mat)
	{
		C_Matrix4x4	result;
		result.m_matrix[0] = m_matrix[0]*mat[0] + m_matrix[4]*mat[1] + m_matrix[8]*mat[2] + m_matrix[12]*mat[3];
		result.m_matrix[1] = m_matrix[1]*mat[0] + m_matrix[5]*mat[1] + m_matrix[9]*mat[2] + m_matrix[13]*mat[3];
		result.m_matrix[2] = m_matrix[2]*mat[0] + m_matrix[6]*mat[1] + m_matrix[10]*mat[2] + m_matrix[14]*mat[3];
		result.m_matrix[3] = m_matrix[3]*mat[0] + m_matrix[7]*mat[1] + m_matrix[11]*mat[2] + m_matrix[15]*mat[3];

		result.m_matrix[4] = m_matrix[0]*mat[4] + m_matrix[4]*mat[5] + m_matrix[8]*mat[6] + m_matrix[12]*mat[7];
		result.m_matrix[5] = m_matrix[1]*mat[4] + m_matrix[5]*mat[5] + m_matrix[9]*mat[6] + m_matrix[13]*mat[7];
		result.m_matrix[6] = m_matrix[2]*mat[4] + m_matrix[6]*mat[5] + m_matrix[10]*mat[6] + m_matrix[14]*mat[7];
		result.m_matrix[7] = m_matrix[3]*mat[4] + m_matrix[7]*mat[5] + m_matrix[11]*mat[6] + m_matrix[15]*mat[7];

		result.m_matrix[8] = m_matrix[0]*mat[8] + m_matrix[4]*mat[9] + m_matrix[8]*mat[10] + m_matrix[12]*mat[11];
		result.m_matrix[9] = m_matrix[1]*mat[8] + m_matrix[5]*mat[9] + m_matrix[9]*mat[10] + m_matrix[13]*mat[11];
		result.m_matrix[10] = m_matrix[2]*mat[8] + m_matrix[6]*mat[9] + m_matrix[10]*mat[10] + m_matrix[14]*mat[11];
		result.m_matrix[11] = m_matrix[3]*mat[8] + m_matrix[7]*mat[9] + m_matrix[11]*mat[10] + m_matrix[15]*mat[11];

		result.m_matrix[12] = m_matrix[0]*mat[12] + m_matrix[4]*mat[13] + m_matrix[8]*mat[14] + m_matrix[12]*mat[15];
		result.m_matrix[13] = m_matrix[1]*mat[12] + m_matrix[5]*mat[13] + m_matrix[9]*mat[14] + m_matrix[13]*mat[15];
		result.m_matrix[14] = m_matrix[2]*mat[12] + m_matrix[6]*mat[13] + m_matrix[10]*mat[14] + m_matrix[14]*mat[15];
		result.m_matrix[15] = m_matrix[3]*mat[12] + m_matrix[7]*mat[13] + m_matrix[11]*mat[14] + m_matrix[15]*mat[15];
		*this = result;
	}
	void C_Matrix4x4::operator*=(const float & fVal)
	{
		m_matrix[0] *= fVal;
		m_matrix[1] *= fVal;
		m_matrix[2] *= fVal;
		m_matrix[3] *= fVal;

		m_matrix[4] *= fVal;
		m_matrix[5] *= fVal;
		m_matrix[6] *= fVal;
		m_matrix[7] *= fVal;

		m_matrix[8] *= fVal;
		m_matrix[9] *= fVal;
		m_matrix[10] *= fVal;
		m_matrix[11] *= fVal;

		m_matrix[12] *= fVal;
		m_matrix[13] *= fVal;
		m_matrix[14] *= fVal;
		m_matrix[15] *= fVal;
	}

	void C_Matrix4x4::transpose()
	{
		float tmp;
		tmp = m_matrix[1];
		m_matrix[1] = m_matrix[4];
		m_matrix[4] = tmp;

		tmp = m_matrix[2];
		m_matrix[2] = m_matrix[8];
		m_matrix[8] = tmp;

		tmp = m_matrix[3];
		m_matrix[3] = m_matrix[12];
		m_matrix[12] = tmp;

		tmp = m_matrix[6];
		m_matrix[6] = m_matrix[9];
		m_matrix[9] = tmp;

		tmp = m_matrix[2];
		m_matrix[2] = m_matrix[8];
		m_matrix[8] = tmp;

		tmp = m_matrix[7];
		m_matrix[7] = m_matrix[13];
		m_matrix[13] = tmp;

		tmp = m_matrix[11];
		m_matrix[11] = m_matrix[14];
		m_matrix[14] = tmp;
	}

	void C_Matrix4x4::invert()
	{
		float det = getDeterminant();
		if (isZero(det))
			return;

		float fDivider = 1/det;	

		//Kramer's rule with optimizing for mult without SSE instructions
		float tmp[12]; /* temp array for pairs */		

		/* calculate pairs for first 8 elements (cofactors) */
		tmp[0] = m_matrix[10] * m_matrix[15];
		tmp[1] = m_matrix[11] * m_matrix[14];
		tmp[2] = m_matrix[9] * m_matrix[15];
		tmp[3] = m_matrix[11] * m_matrix[13];
		tmp[4] = m_matrix[9] * m_matrix[14];
		tmp[5] = m_matrix[10] * m_matrix[13];
		tmp[6] = m_matrix[8] * m_matrix[15];
		tmp[7] = m_matrix[11] * m_matrix[12];
		tmp[8] = m_matrix[8] * m_matrix[14];
		tmp[9] = m_matrix[10] * m_matrix[12];
		tmp[10] = m_matrix[8] * m_matrix[13];
		tmp[11] = m_matrix[9] * m_matrix[12];

		C_Matrix4x4	matrix;
		/* calculate first 8 elements (cofactors) */
		matrix.m_matrix[0] = tmp[0]*m_matrix[5] + tmp[3]*m_matrix[6] + tmp[4]*m_matrix[7];
		matrix.m_matrix[0] -= tmp[1]*m_matrix[5] + tmp[2]*m_matrix[6] + tmp[5]*m_matrix[7];
		matrix.m_matrix[1] = tmp[1]*m_matrix[4] + tmp[6]*m_matrix[6] + tmp[9]*m_matrix[7];
		matrix.m_matrix[1] -= tmp[0]*m_matrix[4] + tmp[7]*m_matrix[6] + tmp[8]*m_matrix[7];
		matrix.m_matrix[2] = tmp[2]*m_matrix[4] + tmp[7]*m_matrix[5] + tmp[10]*m_matrix[7];
		matrix.m_matrix[2] -= tmp[3]*m_matrix[4] + tmp[6]*m_matrix[5] + tmp[11]*m_matrix[7];
		matrix.m_matrix[3] = tmp[5]*m_matrix[4] + tmp[8]*m_matrix[5] + tmp[11]*m_matrix[6];
		matrix.m_matrix[3] -= tmp[4]*m_matrix[4] + tmp[9]*m_matrix[5] + tmp[10]*m_matrix[6];
		matrix.m_matrix[4] = tmp[1]*m_matrix[1] + tmp[2]*m_matrix[2] + tmp[5]*m_matrix[3];
		matrix.m_matrix[4] -= tmp[0]*m_matrix[1] + tmp[3]*m_matrix[2] + tmp[4]*m_matrix[3];
		matrix.m_matrix[5] = tmp[0]*m_matrix[0] + tmp[7]*m_matrix[2] + tmp[8]*m_matrix[3];
		matrix.m_matrix[5] -= tmp[1]*m_matrix[0] + tmp[6]*m_matrix[2] + tmp[9]*m_matrix[3];
		matrix.m_matrix[6] = tmp[3]*m_matrix[0] + tmp[6]*m_matrix[1] + tmp[11]*m_matrix[3];
		matrix.m_matrix[6] -= tmp[2]*m_matrix[0] + tmp[7]*m_matrix[1] + tmp[10]*m_matrix[3];
		matrix.m_matrix[7] = tmp[4]*m_matrix[0] + tmp[9]*m_matrix[1] + tmp[10]*m_matrix[2];
		matrix.m_matrix[7] -= tmp[5]*m_matrix[0] + tmp[8]*m_matrix[1] + tmp[11]*m_matrix[2];

		/* calculate pairs for second 8 elements (cofactors) */
		tmp[0] = m_matrix[2]*m_matrix[7];
		tmp[1] = m_matrix[3]*m_matrix[6];
		tmp[2] = m_matrix[1]*m_matrix[7];
		tmp[3] = m_matrix[3]*m_matrix[5];
		tmp[4] = m_matrix[1]*m_matrix[6];
		tmp[5] = m_matrix[2]*m_matrix[5];	
		tmp[6] = m_matrix[0]*m_matrix[7];
		tmp[7] = m_matrix[3]*m_matrix[4];
		tmp[8] = m_matrix[0]*m_matrix[6];
		tmp[9] = m_matrix[2]*m_matrix[4];
		tmp[10] = m_matrix[0]*m_matrix[5];
		tmp[11] = m_matrix[1]*m_matrix[4];
		/* calculate second 8 elements (cofactors) */
		matrix.m_matrix[8] = tmp[0]*m_matrix[13] + tmp[3]*m_matrix[14] + tmp[4]*m_matrix[15];
		matrix.m_matrix[8] -= tmp[1]*m_matrix[13] + tmp[2]*m_matrix[14] + tmp[5]*m_matrix[15];
		matrix.m_matrix[9] = tmp[1]*m_matrix[12] + tmp[6]*m_matrix[14] + tmp[9]*m_matrix[15];
		matrix.m_matrix[9] -= tmp[0]*m_matrix[12] + tmp[7]*m_matrix[14] + tmp[8]*m_matrix[15];
		matrix.m_matrix[10] = tmp[2]*m_matrix[12] + tmp[7]*m_matrix[13] + tmp[10]*m_matrix[15];
		matrix.m_matrix[10]-= tmp[3]*m_matrix[12] + tmp[6]*m_matrix[13] + tmp[11]*m_matrix[15];
		matrix.m_matrix[11] = tmp[5]*m_matrix[12] + tmp[8]*m_matrix[13] + tmp[11]*m_matrix[14];
		matrix.m_matrix[11]-= tmp[4]*m_matrix[12] + tmp[9]*m_matrix[13] + tmp[10]*m_matrix[14];
		matrix.m_matrix[12] = tmp[2]*m_matrix[10] + tmp[5]*m_matrix[11] + tmp[1]*m_matrix[9];
		matrix.m_matrix[12]-= tmp[4]*m_matrix[11] + tmp[0]*m_matrix[9] + tmp[3]*m_matrix[10];
		matrix.m_matrix[13] = tmp[8]*m_matrix[11] + tmp[0]*m_matrix[8] + tmp[7]*m_matrix[10];
		matrix.m_matrix[13]-= tmp[6]*m_matrix[10] + tmp[9]*m_matrix[11] + tmp[1]*m_matrix[8];
		matrix.m_matrix[14] = tmp[6]*m_matrix[9] + tmp[11]*m_matrix[11] + tmp[3]*m_matrix[8];
		matrix.m_matrix[14]-= tmp[10]*m_matrix[11] + tmp[2]*m_matrix[8] + tmp[7]*m_matrix[9];
		matrix.m_matrix[15] = tmp[10]*m_matrix[10] + tmp[4]*m_matrix[8] + tmp[9]*m_matrix[9];
		matrix.m_matrix[15]-= tmp[8]*m_matrix[9] + tmp[11]*m_matrix[10] + tmp[5]*m_matrix[8];

		for (u32 j = 0; j < 16; j++)
			m_matrix[j] = matrix[j] * fDivider;

		m_matrix[12] = m_matrix[3];
		m_matrix[13] = m_matrix[7];
		m_matrix[14] = m_matrix[11];
	}

	float C_Matrix4x4::getDeterminant() const
	{
		return m_matrix[0]*m_matrix[5]*m_matrix[10]*m_matrix[15] - m_matrix[0]*m_matrix[7]*m_matrix[10]*m_matrix[13]
		+ m_matrix[1]*m_matrix[6]*m_matrix[11]*m_matrix[12] - m_matrix[1]*m_matrix[4]*m_matrix[11]*m_matrix[14]
		+ m_matrix[2]*m_matrix[7]*m_matrix[8]* m_matrix[13] - m_matrix[2]*m_matrix[5]*m_matrix[8]* m_matrix[15]
		+ m_matrix[3]*m_matrix[4]*m_matrix[9]* m_matrix[14] - m_matrix[3]*m_matrix[6]*m_matrix[9]* m_matrix[12];
	}

	void C_Matrix4x4::decompose(C_Vector3 & trans, C_Vector3 & rot, C_Vector3 & scale) const
	{
		SC_ASSERT(false);
		/*void decompose( Vec3f &trans, Vec3f &rot, Vec3f &scale ) const
		{
		// Getting translation is trivial
		trans = Vec3f( c[3][0], c[3][1], c[3][2] );

		// Scale is length of columns
		scale.m_x = sqrt( c[0][0] * c[0][0] + c[0][1] * c[0][1] + c[0][2] * c[0][2] );
		scale.m_y = sqrt( c[1][0] * c[1][0] + c[1][1] * c[1][1] + c[1][2] * c[1][2] );
		scale.m_z = sqrt( c[2][0] * c[2][0] + c[2][1] * c[2][1] + c[2][2] * c[2][2] );

		if( scale.m_x == 0 || scale.m_y == 0 || scale.m_z == 0 ) return;

		// Detect negative scale with determinant and flip one arbitrary axis
		if( determinant() < 0 ) scale.m_x = -scale.m_x;

		// Combined rotation matrix ZXY
		//
		// Cos[y]*Cos[z]+Sin[x]*Sin[y]*Sin[z]	Cos[z]*Sin[x]*Sin[y]-Cos[y]*Sin[z]	Cos[x]*Sin[y]	
		// Cos[x]*Sin[z]						Cos[x]*Cos[z]						-Sin[x]
		// -Cos[z]*Sin[y]+Cos[y]*Sin[x]*Sin[z]	Cos[y]*Cos[z]*Sin[x]+Sin[y]*Sin[z]	Cos[x]*Cos[y]

		rot.m_x = asinf( -c[2][1] / scale.m_z );

		// Special case: Cos[x] == 0 (when Sin[x] is +/-1)
		float f = fabsf( c[2][1] / scale.m_z );
		if( f > 0.999f && f < 1.001f )
		{
		// Pin arbitrarily one of y or z to zero
		// Mathematical equivalent of gimbal lock
		rot.m_y = 0;

		// Now: Cos[x] = 0, Sin[x] = +/-1, Cos[y] = 1, Sin[y] = 0
		// => m[0][0] = Cos[z] and m[1][0] = Sin[z]
		rot.m_z = atan2f( -c[1][0] / scale.m_y, c[0][0] / scale.m_x );
		}
		// Standard case
		else
		{
		rot.m_y = atan2f( c[2][0] / scale.m_z, c[2][2] / scale.m_z );
		rot.m_z = atan2f( c[0][1] / scale.m_x, c[1][1] / scale.m_y );
		}
		}*/
		return;
	}

	void C_Matrix4x4::SetRotationX(float fAngle)
	{
		float cosDelta = (float)cos(DEG2RAD(fAngle));
		float sinDelta = (float)sin(DEG2RAD(fAngle));

		m_matrix[5] = cosDelta;
		m_matrix[6] = sinDelta;
		m_matrix[9] = -sinDelta;
		m_matrix[10] = cosDelta;
	}
	void C_Matrix4x4::SetRotationY(float fAngle)
	{
		float cosDelta = (float)cos(DEG2RAD(fAngle));
		float sinDelta = (float)sin(DEG2RAD(fAngle));

		m_matrix[0] = cosDelta;
		m_matrix[2] = -sinDelta;
		m_matrix[8] = sinDelta;
		m_matrix[10] = cosDelta;
	}
	void C_Matrix4x4::SetRotationZ(float fAngle)
	{
		float cosDelta = (float)cos(DEG2RAD(fAngle));
		float sinDelta = (float)sin(DEG2RAD(fAngle));

		m_matrix[0] = cosDelta;
		m_matrix[1] = sinDelta;
		m_matrix[4] = -sinDelta;
		m_matrix[5] = cosDelta;
	}

	void C_Matrix4x4::addRotationByX(const float fAngle)
	{
		//create tmp matrix, rotate and then multiply with actual
		C_Matrix4x4	tmpMat;
		tmpMat.SetRotationX(fAngle);
		*this *= tmpMat;
	}
	void C_Matrix4x4::addRotationByY(const float fAngle)
	{
		//create tmp matrix, rotate and then multiply with actual
		C_Matrix4x4	tmpMat;
		tmpMat.SetRotationY(fAngle);
		*this *= tmpMat;
	}
	void C_Matrix4x4::addRotationByZ(const float fAngle)
	{
		//create tmp matrix, rotate and then multiply with actual
		C_Matrix4x4	tmpMat;
		tmpMat.SetRotationZ(fAngle);
		*this *= tmpMat;
	}

	void C_Matrix4x4::SetPosition(const C_Vector3 & vct)
	{
		m_matrix[12] = vct.m_x;
		m_matrix[13] = vct.m_y;
		m_matrix[14] = vct.m_z;
	}
	void C_Matrix4x4::SetPosition(float fPosX, float fPosY, float fPosZ)
	{
		m_matrix[12] = fPosX;
		m_matrix[13] = fPosY;
		m_matrix[14] = fPosZ;
	}

	void C_Matrix4x4::addTranslation(const C_Vector3 & vct)
	{
		m_matrix[12] += vct.m_x;
		m_matrix[13] += vct.m_y;
		m_matrix[14] += vct.m_z;
	}

	void C_Matrix4x4::setScale(const C_Vector3 & vct)
	{
		m_matrix[0] = vct.m_x;
		m_matrix[5] = vct.m_y;
		m_matrix[10] = vct.m_z;
	}

	void C_Matrix4x4::addScale(const C_Vector3 & vct)
	{
		m_matrix[0] *= vct.m_x;
		m_matrix[5] *= vct.m_y;
		m_matrix[10] *= vct.m_z;
	}

	C_Vector3	C_Matrix4x4::operator*(const C_Vector3 & vct)
	{
		C_Vector3	vctTmp;

		vctTmp.m_x = m_matrix[0]*vct.m_x + m_matrix[4]*vct.m_y + m_matrix[8]*vct.m_z;
		vctTmp.m_y = m_matrix[1]*vct.m_x + m_matrix[5]*vct.m_y + m_matrix[9]*vct.m_z;
		vctTmp.m_z = m_matrix[2]*vct.m_x + m_matrix[6]*vct.m_y + m_matrix[10]*vct.m_z;
		return vctTmp;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetMatrix
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Matrix4x4::GetMatrix(float matrix[3][4])
	{
		/*matrix[0][0] = m_matrix[0];
		matrix[1][0] = m_matrix[1];
		matrix[2][0] = m_matrix[2];

		matrix[0][1] = m_matrix[4];
		matrix[1][1] = m_matrix[5];
		matrix[2][1] = m_matrix[6];

		matrix[0][2] = m_matrix[8];
		matrix[1][2] = m_matrix[9];
		matrix[2][2] = m_matrix[10];

		matrix[0][3] = m_matrix[12];
		matrix[1][3] = m_matrix[13];
		matrix[2][3] = m_matrix[14];*/

		matrix[0][0] = m_matrix[0];
		matrix[0][1] = m_matrix[1];
		matrix[0][2] = m_matrix[2];
		matrix[0][3] = m_matrix[12];	//xPos

		matrix[1][0] = m_matrix[4];
		matrix[1][1] = m_matrix[5];
		matrix[1][2] = m_matrix[6];
		matrix[1][3] = m_matrix[13];

		matrix[2][0] = m_matrix[8];
		matrix[2][1] = m_matrix[9];
		matrix[2][2] = m_matrix[10];
		matrix[2][3] = m_matrix[14];
	}

	//////////////////////// ROTACIA PODLA VEKTORA A UHLU ///////////////////////////
	/*	CMatrix4 T,Rx,Ry,Rz,Ryi,Rxi,Ti;
	CVector v(vec);
	float a;

	T.Translate( -p );		// posunutie do bodu -p
	Ti.Translate( p );		// posunutie do bodu p

	v.Normalise();
	a = (float) sqrt( v.m_y*v.m_y + v.m_z*v.m_z);		// a - velkost vektora premietnutneho do yz

	if(a!=0)				// ak a==0 nema zmysel rotovat okolo x, pretoze v=(1,0,0)
	{
	// 1. rotacia v okolo x tak aby sa v dostal do roviny xz
	//  v\   | y   v sa ma dostat na z (xz), uhol pre rotaciu je uhol medzi v a z
	//    \  |     velkost v v tomto priemete je a
	//     \ |     sin = v.m_y/a
	//      \|     cos = v.m_z/a = [(0, v.m_y, v.m_z)dot(0, 0, v.m_z)]/[a*v.m_z]
	// z-----+                         (  1    0     0  )
	// rotacna matica pre rotaciu x :  (  0   cos  -sin )
	//                                 (  0   sin   cos )
	Rx.m22 = v.m_z/a; Rx.m23 =-v.m_y/a;
	Rx.m32 = v.m_y/a; Rx.m33 = v.m_z/a;

	Rxi.m22 = v.m_z/a; Rxi.m23 = v.m_y/a;
	Rxi.m32 =-v.m_y/a; Rxi.m33 = v.m_z/a;
	}
	// 2. rotacia v okolo osi y tak aby sa v stotoznil so z
	//        ^ y           vektor v je uz v rovine xz
	//        |             v budeme rotavat okolo osi y, tak aby sa stotoznil so z
	//        |             -----+----- x  vektor v rotujeme v smere pravotocivej skrutky okolo osi y
	//        +----- x     -x   /|         velkost vektora je v je 1 = (v.m_x^2+a^2)^0.5
	//       / \               / |         sin = -v.m_x
	//      /   \             /  |         cos = a
	//    z/     \v         v/   | z                           (  cos  0  sin )
	//                          rotacna matica pre rotaciu y : (   0   1   0  )
	//                                                         ( -sin  0  cos )
	Ry.m11 = a;    Ry.m13 = -v.m_x;
	Ry.m31 = v.m_x;  Ry.m33 = a;

	Ryi.m11 = a;    Ryi.m13 = v.m_x;
	Ryi.m31 = -v.m_x; Ryi.m33 = a;

	// 3. rotacia okolo z o uhol angle
	Rz.RotZ( angle);

	*this = Ti*Rxi*Ryi*Rz*Ry*Rx*T;*/

	/*
	void matrix4x4f::rotate( const float &angle, vector3f &axis )
	{
	float s = sin(DEGTORAD(angle));
	float c = cos(DEGTORAD(angle));

	axis.normalize();

	float ux = axis.m_x;
	float uy = axis.m_y;
	float uz = axis.m_z;

	m[0]  = c + (1-c) * ux;
	m[1]  = (1-c) * ux*uy + s*uz;
	m[2]  = (1-c) * ux*uz - s*uy;
	m[3]  = 0;

	m[4]  = (1-c) * uy*ux - s*uz;
	m[5]  = c + (1-c) * pow(uy,2);
	m[6]  = (1-c) * uy*uz + s*ux;
	m[7]  = 0;

	m[8]  = (1-c) * uz*ux + s*uy;
	m[9]  = (1-c) * uz*uz - s*ux;
	m[10] = c + (1-c) * pow(uz,2);
	m[11] = 0;

	m[12] = 0;
	m[13] = 0;
	m[14] = 0;
	m[15] = 1;
	}*/
}}