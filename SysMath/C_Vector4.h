/*
* filename:			C_Vector4.h
*
* author:			Kral Jozef
* date:				02/25/2008		21:45
* version:			1.00
* brief:				Defines 4D Vector operation
*/

#pragma once

#include "SysMathApi.h"
#include <math.h>
#include "MathUtils.h"

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Vector4
	class C_Vector4
	{
	public:
		float	m_x;
		float m_y;
		float m_z;
		float m_w;

	public:
		//@ c-tor
		C_Vector4() : m_x(0.f), m_y(0.f), m_z(0.f), m_w(0.f) {};
		//@ c-tor
		C_Vector4(float x,float y,float z, float w) : m_x(x), m_y(y), m_z(z), m_w(w) {};
		//@ copy c-tor
		C_Vector4(const C_Vector4 & copy)
			: m_x(copy.m_x), m_y(copy.m_y), m_z(copy.m_z), m_w(copy.m_w) {};

		//@ Sucin skalara
		C_Vector4 operator*(const float fVal) const { return C_Vector4(m_x * fVal, m_y * fVal, m_z * fVal, m_w * fVal); }
		//@ Sucet skalara
		C_Vector4 operator+(const float fVal) const { return C_Vector4(m_x + fVal, m_y + fVal, m_z + fVal, m_w + fVal); }
		//@ Podiel vektora a skalara
		C_Vector4 operator/(const float fVal) const
		{
			float f = 1 / fVal;
			return C_Vector4(m_x * f, m_y * f, m_z * f, m_w * f);
		}

		//@ operator ==
		bool operator==(const C_Vector4 & vct) const
		{
			return (isZero(vct.m_x - m_x) && isZero(vct.m_y - m_y) && isZero(vct.m_z - m_z) && isZero(vct.m_w - m_w));
		}
		//@ operator !=
		bool operator!=(const C_Vector4 & vct) const { return !(*this == vct); }


		//@ Spocitaj velkost vektora
		//float	length() const;
		//@ Normalizuj na jednotkovy vektor
		//void	normalize();
	};

}}