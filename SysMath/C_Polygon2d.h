/*
	This file is part of the Hyperion project.
	Copyright (C) 2005 Stormheads. All rights reserved.
	http://www.stormheads.com

	For conditions of distribution and use, see the accompanying COPYING file.

	Author:
		Jozef 'oiko' Kral : Mon Oct 17 22:02:12 CET 2005
	Description:
		Class for CPolygon2d manipulation uses vector container from STL
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <boost/shared_ptr.hpp>
#include <vector>
#include "C_Vector2.h"
#include "C_AABB2.h"
#include "MathTypes.h"
#include <Common/macros.h>
#include <Common/Assert.h>

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ CPolygon2d
	class SYSMATH_API C_Polygon2d
	{
	public:
		//@ c-tor
		C_Polygon2d() {};
		//@ ctor
		C_Polygon2d(u32 reservedSize)
		{
			m_polyg.reserve(reservedSize);
		}

		//@ d-tor
		virtual ~C_Polygon2d() {};

		//@ Add
		void						Add( float pX, float pY );
		void						Add( const C_Vector2 & pVct );
		//@ GetSize
		u32						GetSize() { return (u32)m_polyg.size(); }
		//@ Reserve
		void						Reserve(u32 reservedSize) { m_polyg.reserve(reservedSize); }

		void						removeAll();
		//void				deleteAll();
		void						removeAt( u32 pIndex );
		void						DeleteAt( u32 pIndex );
		void						insertAt( C_Vector2 * pVct, u32 pIndex );
		C_AABB2				*	boundingBox();
		C_Polygon2d			*	makeACopyOfMe();

		T_SidePosition			WhichSide( const C_Vector2 & pVct );
		//void        cutPolyToConvexPoly...
		//float       distFromPoly( Vector2d & pVct );      //min dist from vertex of poly
		//Vector2d  * intersectWithPoly( Line2d & pLine );  //intersect which has minimal distant from start

		//@ acces operator
		const C_Vector2	&	operator[](u32 index)
		{
			SC_ASSERT(index < m_polyg.size() && "Index out of range!");
			return m_polyg[index];
		}

	public:
		std::vector<C_Vector2>  m_polyg;
	};

	/*#ifdef _DEBUG
	void          trace_poly( CPolygon2d * pPoly );
	#endif*/

	typedef boost::shared_ptr<C_Polygon2d>	T_Polygon2d;
}}
