/*
* filename:			C_Matrix3x3.cpp
*
* author:			Kral Jozef
* date:				02/16/2011		13:52
* version:			1.00
* brief:
*/

#include "C_Matrix2d.h"
#include <SysMath/C_Vector2.h>
#include <math.h>
#include "MathUtils.h"
#include <Common/macros.h>

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Matrix3x3::C_Matrix3x3()
	{
		Identity();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ copy c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Matrix3x3::C_Matrix3x3(const C_Matrix3x3 & copy)
	{
		MEM_CPY(m_Matrix, 9, copy.m_Matrix, 9);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetPos
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Matrix3x3::SetTranslation(const C_Vector2 & vctPos)
	{
		m_Matrix[6] = vctPos.m_x;
		m_Matrix[7] = vctPos.m_y;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddPos
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Matrix3x3::AddPos(const C_Vector2 & vctPos)
	{
		m_Matrix[6] += vctPos.m_x;
		m_Matrix[7] += vctPos.m_y;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetRot
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Matrix3x3::SetRotation(float fAngle)
	{
		m_Matrix[0] = cosf(DEG2RAD(fAngle));
		m_Matrix[1] = sinf(DEG2RAD(fAngle));
		m_Matrix[3] = -m_Matrix[1];
		m_Matrix[4] = m_Matrix[0];
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddRot
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Matrix3x3::AddRot(float fAngle)
	{
		float tmpCos = cosf(DEG2RAD(fAngle));
		float tmpSin = sinf(DEG2RAD(fAngle));

		m_Matrix[0] *= tmpCos;
		m_Matrix[1] *= tmpSin;
		m_Matrix[3] = -m_Matrix[1];
		m_Matrix[4] = -m_Matrix[0];
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddScale
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Matrix3x3::AddScale(float fScale)
	{
		m_Matrix[0] *= fScale;
		m_Matrix[4] *= fScale;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@
	//
	//////////////////////////////////////////////////////////////////////////
	C_Matrix3x3	C_Matrix3x3::operator*(const C_Matrix3x3 & mat)
	{
		C_Matrix3x3 result;

		result.m_Matrix[0] = m_Matrix[0]*mat[0] + m_Matrix[3]*mat[1] + m_Matrix[6]*mat[2];
		result.m_Matrix[1] = m_Matrix[1]*mat[0] + m_Matrix[4]*mat[1] + m_Matrix[7]*mat[2];
		result.m_Matrix[2] = m_Matrix[2]*mat[0] + m_Matrix[5]*mat[1] + m_Matrix[8]*mat[2];
		
		result.m_Matrix[3] = m_Matrix[0]*mat[3] + m_Matrix[3]*mat[4] + m_Matrix[6]*mat[5];
		result.m_Matrix[4] = m_Matrix[1]*mat[3] + m_Matrix[4]*mat[4] + m_Matrix[7]*mat[5];
		result.m_Matrix[5] = m_Matrix[2]*mat[3] + m_Matrix[5]*mat[4] + m_Matrix[8]*mat[5];
		
		result.m_Matrix[6] = m_Matrix[0]*mat[6] + m_Matrix[3]*mat[7] + m_Matrix[6]*mat[8];
		result.m_Matrix[7] = m_Matrix[1]*mat[6] + m_Matrix[4]*mat[7] + m_Matrix[7]*mat[8];
		result.m_Matrix[8] = m_Matrix[2]*mat[6] + m_Matrix[5]*mat[7] + m_Matrix[8]*mat[8];

		return result;
	}



}}