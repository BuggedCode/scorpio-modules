/*
* filename:			C_BSphere.cpp
*
* author:				Kral Jozef
* date:					Januar 2007
* version:			1.00
* description:	Sphere class
*/

#include "C_BSphere.h"
#include "C_BBox.h"

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_BSphere::C_BSphere(const C_BBox & box)
	{
		m_pivot = box.getPivot();
		C_Vector3	vct(m_pivot - box.m_max);
		m_radius = vct.length();
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_BSphere::include(const C_BSphere & sphere)
	{
		float len = C_Vector3(m_pivot-sphere.m_pivot).length() + sphere.m_radius;
		if (len > m_radius)
			m_radius = len;
	}

}}