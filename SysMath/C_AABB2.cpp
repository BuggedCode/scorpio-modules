/*
* filename:			C_AABB2.cpp
*
* author:			Kral Jozef
* date:				Januar 2006
* update:			Januar 2008
* version:			1.00
* description:		2D Bounding box class
*/

#include "C_AABB2.h"

namespace scorpio{ namespace sysmath{
	
	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_AABB2::C_AABB2(const C_Vector2 & vct1, const C_Vector2 & vct2)
	{
		if ( vct1.m_x < vct2.m_x )
		{
			m_min.m_x = vct1.m_x;
			m_max.m_x = vct2.m_x;
		}
		else
		{
			m_min.m_x = vct2.m_x;
			m_max.m_x = vct1.m_x;
		}

		if (vct1.m_y < vct2.m_y)
		{
			m_min.m_y = vct1.m_y;
			m_max.m_y = vct2.m_y;
		}
		else
		{
			m_min.m_y = vct2.m_y;
			m_max.m_y = vct1.m_y;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Translate
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AABB2::Translate(const C_Vector2 & vctPos)
	{
		C_Vector2 vctCenter = GetCenter();
		C_Vector2 vctOffset = vctPos - vctCenter;

		m_min += vctOffset;
		m_max += vctOffset;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TranslateBy
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AABB2::TranslateBy(const C_Vector2 & offsetValue)
	{
		m_min += offsetValue;
		m_max += offsetValue;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ include
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AABB2::Include(const C_Vector2 & vct)
	{
		if (this->IsInvalid())
		{
			m_max.m_x = m_min.m_x = vct.m_x;
			m_max.m_y = m_min.m_y = vct.m_y;
			return;
		}

		vct.m_x < m_min.m_x ? m_min.m_x = vct.m_x : ( vct.m_x > m_max.m_x ) ? m_max.m_x = vct.m_x : 0;//nop operacia
		vct.m_y < m_min.m_y ? m_min.m_y = vct.m_y : ( vct.m_y > m_max.m_y ) ? m_max.m_y = vct.m_y : 0;//nop operacia		
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Include
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AABB2::Include(const C_AABB2 & aabb)
	{
		if (aabb.IsInvalid())
			return;

		m_min.m_x = ( aabb.GetMin().m_x < m_min.m_x ) ? aabb.GetMin().m_x : 0;
		m_min.m_y = ( aabb.GetMin().m_y < m_min.m_y ) ? aabb.GetMin().m_y : 0;

		m_max.m_x = ( aabb.GetMax().m_x < m_max.m_x ) ? aabb.GetMax().m_x : 0;
		m_max.m_y = ( aabb.GetMax().m_y < m_max.m_y ) ? aabb.GetMax().m_y : 0;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsIntersect
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AABB2::IsIntersect(const C_AABB2 & aabb) const
	{
		if ((aabb.GetMin().m_x >= m_min.m_x && aabb.GetMin().m_x <= m_max.m_x) && (aabb.GetMin().m_y >= m_min.m_y && aabb.GetMin().m_y <= m_max.m_y))
			return true;

		if ((aabb.GetMax().m_x >= m_min.m_x && aabb.GetMax().m_x <= m_max.m_x) && (aabb.GetMax().m_y >= m_min.m_y && aabb.GetMax().m_y <= m_max.m_y))
			return true;

		C_Vector2 d1, d2;
		d1 = aabb.GetMin() - m_max;
		d2 = m_min - aabb.GetMax();

		if (d1.m_x > 0.0f || d1.m_y > 0.0f)
			return false;

		if (d2.m_x > 0.0f || d2.m_y > 0.0f)
			return false;

		return true;


		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsIntersect
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AABB2::IsIntersect(const C_Vector2 & vctPos) const
	{
		if ((vctPos.m_x >= m_min.m_x && vctPos.m_x <= m_max.m_x) && (vctPos.m_y >= m_min.m_y && vctPos.m_y <= m_max.m_y))
			return true;

		return false;
	}

}}