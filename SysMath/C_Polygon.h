/*
* filename:			C_Polygon.h
*
* author:			Kral Jozef
* date:				Januar 2006
* version:			1.00
* description:		Class for CPolygon add manipulation with new m_closed-atribut  for class CPolygon2d
*/


#pragma once

#include "C_Polygon2d.h"
#include <SysMath/SysMathApi.h>

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Polygon
	class C_Polygon : public C_Polygon2d
	{
	public:
		//@ c-tor
		C_Polygon() : m_closed(false) {};
		//@ d-tor
		virtual ~C_Polygon() {};

		//SET/GET FUNCTIONS
		void					SetClosed( bool pEnable ) { m_closed = pEnable; }
		bool					IsClosed() const { return m_closed; }

	private:
		bool					m_closed;
	};

	DECLARE_SHARED_PTR(C_Polygon,	T_Polygon);
}}
