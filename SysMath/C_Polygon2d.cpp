/*
	This file is part of the Hyperion project.
	Copyright (C) 2005 Stormheads. All rights reserved.
	http://www.stormheads.com

	For conditions of distribution and use, see the accompanying COPYING file.

	Author:
		Jozef 'oiko' Kral : Mon Oct 17 22:02:12 CET 2005
	Description:
		implementation file for C_Polygon2d class
*/

#include "C_Polygon2d.h"
#include "C_Line2d.h"
//#include "MathUtil.h"
#include <Common/types.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace sysmath{

	/*C_Polygon2d::~C_Polygon2d()
	{
		deleteAll();
	}*/

	void C_Polygon2d::Add( float pX, float pY )
	{
		m_polyg.push_back( C_Vector2( pX, pY ) );
	}

	void C_Polygon2d::Add( const C_Vector2 & pVct )
	{
		m_polyg.push_back( pVct );
	}

	void C_Polygon2d::removeAll()
	{
		m_polyg.clear();
	}
	/*void C_Polygon2d::deleteAll()
	{
		for ( size_t i = 0; i < m_polyg.size(); i++ )
			delete m_polyg[ i ];
		m_polyg.clear();
	}*/
	void C_Polygon2d::removeAt( u32 pIndex )
	{
		m_polyg.erase( m_polyg.begin() + pIndex );
	}

	void C_Polygon2d::DeleteAt( u32 pIndex )
	{
		m_polyg.erase( m_polyg.begin() + pIndex );
	}

	void C_Polygon2d::insertAt( C_Vector2 * pVct, u32 pIndex )
	{
		SC_ASSERT(false);
		/*
		vector<C_Vector2 *>::iterator	iter = m_polyg.begin();
		iter.Add(pIndex);
		m_polyg.insert( iter, pVct );*/
	}

	/******************************************************************/
	/***                CreateBBox of polygon                       ***/
	/******************************************************************/
	C_AABB2 * C_Polygon2d::boundingBox()
	{
		SC_ASSERT(false);	//tohle bych predelal na posilani parametra a alokaci na stacku a ne na heapu!
		C_AABB2 * pBBox = NULL;
		for (std::vector<C_Vector2>::iterator iter = m_polyg.begin(); iter != m_polyg.end(); ++iter)
		{
			if ( iter == m_polyg.begin() )
				pBBox = new C_AABB2( *iter );
			else
				pBBox->Include( *iter );
		}
		return pBBox;
	}

	/******************************************************************/
	/***      Create a copy of polygon with copy of his vectors     ***/
	/******************************************************************/
	C_Polygon2d * C_Polygon2d::makeACopyOfMe()
	{
		C_Polygon2d * newPoly = new C_Polygon2d();
		if ( !newPoly )
			return NULL;

		for (std::vector<C_Vector2>::iterator iter = m_polyg.begin(); iter != m_polyg.end(); ++iter)
			newPoly->Add(*iter);

		return newPoly;
	}
	/******************************************************************/
	/***            On which side is an input Vector                ***/
	/******************************************************************/
	//Ray crossing method || quadrant method not implemented yet maybe it will be faster
	T_SidePosition C_Polygon2d::WhichSide( const C_Vector2 & pVct )
	{
		//C_Vector2  * vctS = NULL;
		//C_Vector2  * vctE = NULL;
		size_t index = 0;
		u32 crossing = 0;
		double  intX;  
		//float dist = 100.F;

		//test for ONIT
		for ( size_t i = 0; i < m_polyg.size(); i++ )
		{
			if ( i != m_polyg.size() - 1 )
				index = i + 1;
			else
				index = 0;

			//JK C_Line2d line(m_polyg[ i ], m_polyg[ index ]); - current but not compilable because not rewritted C_Line2d
			//JK line.set( m_polyg[ i ], m_polyg[ index ] ); - original! obsolete need rewrite C_Line2d

			//JK dist = line.distFromSegment( pVct );
			//JK if ( isZero( dist ) )
			//JK	return E_SP_ON;
		}

		//center of SS is in tested point pVct
		for ( size_t i = 0; i < m_polyg.size(); i++ )
			m_polyg[ i ].set( m_polyg[ i ].m_x - pVct.m_x, m_polyg[ i ].m_y - pVct.m_y );

		for ( size_t i = 0; i < m_polyg.size(); i++ )
		{    
			if ( i != m_polyg.size() - 1 )
				index = i+1;
			else
				index = 0;   

			if ( ( m_polyg[ i ].m_y > 0 && m_polyg[ index ].m_y <= 0 ) ||
				( m_polyg[ i ].m_y <= 0 && m_polyg[ index ].m_y > 0 ) )
			{
				//vypocet na zaklade pomerov stran v zhodnom pravouhlom trojuholniku
				intX = ( m_polyg[ i ].m_x * m_polyg[ index ].m_y - m_polyg[ index ].m_x * m_polyg[ i ].m_y ) / ( double )( m_polyg[ index ].m_y - m_polyg[ i ].m_y );
				if ( intX > 0 )
					crossing++;
			}
		}

		//back transformation
		for ( size_t i = 0; i < m_polyg.size(); i++ )
			m_polyg[ i ].set( m_polyg[ i ].m_x + pVct.m_x, m_polyg[ i ].m_y + pVct.m_y );

		if ( ( crossing % 2 ) == 1 )
			return E_SP_INSIDE;

		return E_SP_OUTSIDE;
	}

	//this algorithm is correct but slower than that over
	/*SidePosition C_Polygon2d::whichSide( Vector2d & pVct ) 
	{
	Vector2d  vctS;
	Vector2d  vctE;
	Line2d  line;
	double dist = 1000000;
	u32 nCount = 0; //num of intersections
	float param = 10.F;

	Box2d * bbox = boundingBox();
	if ( !bbox )
	return UNDEF;

	Vector2d  vctOut( bbox->getXMin() - 1000, bbox->getYMin() - 1000 );
	delete bbox;

	Line2d  lineTest( &pVct, &vctOut );
	Vector2d  * pInt = NULL;

	for ( u32 i = 0; i < m_polyg.size(); i++ )
	{
	vctS.set( m_polyg[ i ] );
	if ( i == m_polyg.size() - 1 )
	vctE.set( m_polyg[ 0 ] );
	else
	vctE.set( m_polyg[ i + 1 ] );

	line.set( &vctS, &vctE );
	dist = line.distFromLine( pVct );
	if ( isZero( dist ) )
	return ONIT;

	//find intersection
	pInt = lineTest.intersection( line );
	if ( pInt )
	{
	param = lineTest.paramInSegment( *pInt );
	if ( line.isInSegment( *pInt ) && param > 0.F )  //must be in poly line segment and must be on halfline testpoint->outpoint
	nCount++;

	delete pInt;
	}
	}

	if ( nCount % 2 == 0 )
	return OUTSIDE;
	else
	return INSIDE;
	}*/
}}