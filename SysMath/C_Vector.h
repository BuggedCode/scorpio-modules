/*
* filename:			C_Vector.h
*
* author:			Kral Jozef
* date:				September 2002
* update:			December 2006
* version:			1.00
* description:		Defines 3D Vector operation
*/

#pragma once

#include "SysMathApi.h"
#include <math.h>
#include "MathUtils.h"
#include <Common/Types.h>

namespace scorpio{ namespace sysmath{

	class C_Matrix4x4;

	//////////////////////////////////////////////////////////////////////////
	//@
	class SYSMATH_API C_Vector3
	{
	public:
		float	m_x;
		float m_y;
		float m_z;

	public:
		C_Vector3()	:	m_x(0), m_y(0), m_z(0) {};
		C_Vector3(float X,float Y,float Z)	:	m_x(X), m_y(Y), m_z(Z) {};
		C_Vector3(const C_Vector3 & copy);
		~C_Vector3();
		void operator=(const C_Vector3 & copy);
		void operator=(const float fVal);

		//@ Sucin skalara
		C_Vector3 operator*(const float fVal) const { return C_Vector3(m_x * fVal, m_y * fVal, m_z * fVal); }
		//@ Skalarny sucin
		float operator%(const C_Vector3 & vct) const { return (m_x*vct.m_x + m_y*vct.m_y + m_z*vct.m_z); }
		//@ Vektorovy sucin
		C_Vector3 operator*(const C_Vector3 & vct) const { return C_Vector3(m_y*vct.m_z - m_z*vct.m_y, m_z*vct.m_x - m_x*vct.m_z, m_x*vct.m_y - m_y*vct.m_x); }
		//@ Sucet skalara
		C_Vector3 operator+(const float fVal) const { return C_Vector3(m_x + fVal, m_y + fVal, m_z + fVal); }
		//Vektorovy sucet
		C_Vector3 operator+(const C_Vector3 & vct) const { return C_Vector3(m_x + vct.m_x, m_y + vct.m_y, m_z + vct.m_z); }
		//@ Rozdiel skalara
		C_Vector3 operator-(const float fVal) const { return C_Vector3(m_x - fVal, m_y - fVal, m_z - fVal); }
		//@ Rozdiel vektorov
		C_Vector3 operator-(const C_Vector3 & vct) const { return C_Vector3(m_x - vct.m_x, m_y - vct.m_y, m_z - vct.m_z); }
		//@ Podiel vektora a skalara
		C_Vector3 operator/(const float fVal);
		//@ Podiel vektora a skalara
		C_Vector3 operator/(const u32 iVal);
		//@ Arithmetic assignment operators +=
		void	operator += (const C_Vector3 & pVct);
		//@ Arithmetic assignment operators -=
		void	operator -= (const C_Vector3 & pVct);
		//@ Arithmetic assignment operators *=
		void	operator *= (const double dVal);
		//@ Arithmetic assignment operators /=
		void	operator /= (const double dVal);
		//@ Transform vector by Matrix4x4
		void	operator *= (const C_Matrix4x4 & matrix);
		//@ operator ==
		bool operator==(const C_Vector3 & vct) const
		{
			return (isZero(vct.m_x - m_x) && isZero(vct.m_y - m_y) && isZero(vct.m_z - m_z));
		}
		//@ operator !=
		bool operator!=(const C_Vector3 & vct) const { return !(*this == vct); }


		//@ Vrat vektor z 2 bodov
		void Vector(const C_Vector3 pBod1, const C_Vector3 pBod2)
		{
			m_x = pBod1.m_x - pBod2.m_x;
			m_y = pBod1.m_y - pBod2.m_y;
			m_z = pBod1.m_z - pBod2.m_z;
		}

		//@ Spocitaj velkost vektora
		float	length() const;
		//@ Normalizuj na jednotkovy vektor
		void	normalize();

	};//class C_Vector3

	typedef C_Vector3 C_Vector;
}}