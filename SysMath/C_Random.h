/*
* filename:			C_Random.h
*
* author:			Kral Jozef
* date:				10/31/2010		20:17
* version:			1.00
* brief:
*/

#pragma once

#include <SysMath/SysMathApi.h>
#include <boost/random/mersenne_twister.hpp>
#include <SysMath/C_Vector4.h>

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Random
	class SYSMATH_API C_Random
	{
	public:
		static void		Init(u32 seed);
		//@ Random - generate random number from range <lowerBound,upperBound)
		static float	Random(float lowerBound, float upperBound);
		//@ Random - generate random number from range <lowerBound,upperBound)
		static u32		Random(u32 lowerBound, u32 upperBound);
		//@ Random - generate random number from range <0,upperBound)
		static float	Random(float upperBound);
		//@ Random - generate random number from range <0,upperBound)
		static u32		Random(u32 upperBound);
		//@ RandomColor
		static C_Vector4	RandomColor();

	private:
		static boost::mt19937	m_EngGen;	//engine of generator
	};

}}