/*
* filename:			C_Spline2d.cpp
*
* author:			Kral Jozef
* date:				10/04/2010		21:49
* version:			1.00
* brief:
*/

#include "C_Spline2d.h"
#include "C_Point.h"
#include <Common/Assert.h>


namespace scorpio{ namespace sysmath{

	static const float C_WEIGHT_FACTOR = 2.f;
	static const float C_MULTIPLIER = 3.f;
	static const float C_ON_SPLINE_PARAM = 0.5f;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ComputeSpline
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Spline2d::ComputeSpline(u8 nIterCount)
	{
		SC_ASSERT(m_vctPos.size() > 2);
		if (m_vctPos.empty())
			return;

		for (u8 i = 0; i < nIterCount; ++i)
		{
			DoSpline();
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	// DoSpline
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Spline2d::DoSpline()
	{
		std::vector<S_WeightPos>	vctWeights;
		vctWeights.reserve(m_vctPos.size());

		for (size_t i = 1; i < m_vctPos.size()-1; ++i)
		{
			C_Vector2 & vctPrev = m_vctPos[i-1];
			C_Vector2 & vctAct = m_vctPos[i];
			C_Vector2 & vctNext = m_vctPos[i+1];

			//findWeights
			S_WeightPos outWeight;
			C_Spline2d::GetWeight(vctPrev, vctAct, vctNext, outWeight);

			vctWeights.push_back(outWeight);
		}

		T_Container	spline;
		spline.push_back(m_vctPos[0]);

		for (size_t i = 0; i < vctWeights.size() - 1; ++i)
		{
			C_Vector2 vctPos = C_Spline2d::GetPointAtParam(vctWeights[i], vctWeights[i+1], C_ON_SPLINE_PARAM);
			spline.push_back(vctWeights[i].m_pos);
			spline.push_back(vctPos);
		}
		spline.push_back(vctWeights[vctWeights.size()-1].m_pos);

		spline.push_back(m_vctPos[m_vctPos.size()-1]);

		//@ preklop obsah
		m_vctPos.clear();
		for (T_Container::iterator iter = spline.begin(); iter != spline.end(); ++iter)
		{
			m_vctPos.push_back(*iter);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetWeight
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Spline2d::GetWeight(const C_Vector2 & vctPrev, const C_Vector2 & vctAct, const C_Vector2 & vctNext, S_WeightPos & actWeight)
	{
		C_Vector2  tang = vctNext - vctPrev;
		tang.normalize();

		float prevDist = C_Vector2(vctPrev - vctAct).length();
		float nextDist = C_Vector2(vctAct - vctNext).length();

		actWeight.m_pos = vctAct;

		actWeight.m_prevWeight.m_x = vctAct.m_x - ( prevDist / C_WEIGHT_FACTOR * tang.m_x );
		actWeight.m_prevWeight.m_y = vctAct.m_y - ( prevDist / C_WEIGHT_FACTOR * tang.m_y );

		actWeight.m_nextWeight.m_x = vctAct.m_x + ( nextDist / C_WEIGHT_FACTOR * tang.m_x );
		actWeight.m_nextWeight.m_y = vctAct.m_y + ( nextDist / C_WEIGHT_FACTOR * tang.m_y );
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetPointAtParam
	//
	//////////////////////////////////////////////////////////////////////////
	C_Vector2 C_Spline2d::GetPointAtParam(const S_WeightPos & segStart, const S_WeightPos & segEnd, float fParam)
	{
		C_Vector2 v0 = segStart.m_pos;
		C_Vector2 v1 = segStart.m_nextWeight;
		C_Vector2 v2 = segEnd.m_prevWeight;
		C_Vector2 v3 = segEnd.m_pos;

		//////////////////////////////////////////////////////////////////////////
		C_Vector2  q  = C_MULTIPLIER * ( v2 - v1 );
		C_Vector2  a1 = C_MULTIPLIER * ( v1 - v0 );
		C_Vector2  a2 = q - a1;
		C_Vector2  a3 = ( v3 - v0 ) - q;

		C_Vector2  out = fParam * ( fParam * ( fParam * a3 + a2 ) + a1 ) + v0;

		return out;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Fill
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Spline2d::Fill(std::vector<C_Vector2> & outputContainer) const
	{
		//@ bCutFirstLast
		for (size_t i = 1; i < m_vctPos.size() - 1; ++i)
		{
			outputContainer.push_back(m_vctPos[i]);
		}
	}



	//////////////////////////////////////////////////////////////////////////
	//
	// ToPointVector
	//
	//////////////////////////////////////////////////////////////////////////
	std::vector<C_Point> C_Spline2d::ToPointVector() const
	{
		std::vector<C_Point> vctRes;
		for (T_Container::const_iterator iter = m_vctPos.begin(); iter != m_vctPos.end(); ++iter)
		{
			vctRes.push_back(C_Point((s32)iter->m_x, (s32)iter->m_y));
		}

		return vctRes;
	}

}}