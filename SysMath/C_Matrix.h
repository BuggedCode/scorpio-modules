/*
* filename:			C_Matrix.h
*
* author:			Kral Jozef
* date:				December 2006
* version:			1.00
* brief:				Matrix operation
*/

#pragma once

#include "SysMathApi.h"
#include <Common/types.h>

//	m[i,j] i-row, j-column
//	m0	m1	m2	m3
//	m4	m5	m6	m7
//	m8	m9	m10	m11
//	m12	m13	m14	m15

namespace scorpio{ namespace sysmath{

	class C_Vector3;
	class C_Quaternion;

	class SYSMATH_API C_Matrix4x4
	{
		friend class C_Vector3;
		friend class C_Quaternion;

	public:
		C_Matrix4x4();
		C_Matrix4x4(const float matrix[16]);
		C_Matrix4x4(const C_Matrix4x4 & copy);	
		virtual ~C_Matrix4x4();
		void		operator=(const C_Matrix4x4 & copy);

		//@ Algebraic operations on matrixes
		C_Matrix4x4	operator+(const C_Matrix4x4 & mat);
		C_Matrix4x4	operator-(const C_Matrix4x4 & mat);
		C_Matrix4x4	operator*(const C_Matrix4x4 & mat);
		C_Matrix4x4	operator*(const float & fVal);

		//@ Transform vector
		C_Vector3	operator*(const C_Vector3 & vct);

		//@ Arithmetic assignment operator
		void	operator+=(const C_Matrix4x4 & mat);
		void	operator-=(const C_Matrix4x4 & mat);
		void	operator*=(const C_Matrix4x4 & mat);
		void	operator*=(const float & fVal);//scalar


		//@ Set identity matrix
		void		identity();
		void		transpose();
		void		invert();
		float		getDeterminant() const;
		void		decompose(C_Vector3 & trans, C_Vector3 & rot, C_Vector3 & scale) const;

		//@ Set matrix transformations
		void		SetRotationX(float fAngle);
		void		SetRotationY(float fAngle);
		void		SetRotationZ(float fAngle);
		void		SetPosition(const C_Vector3 & vct);
		void		SetPosition(float fPosX, float fPosY, float fPosZ);
		void		setScale(const C_Vector3 & vct);

		//@ Add matrix transformation
		void		addRotationByX(const float fAngle);
		void		addRotationByY(const float fAngle);
		void		addRotationByZ(const float fAngle);
		void		addTranslation(const C_Vector3 & vct);
		void		addScale(const C_Vector3 & vct);
		//@ GetMatrix
		void		GetMatrix(float matrix[3][4]);

		//TODO
		//frustum(left, right, bottom, top, near, far) - Returns a matrix that represents a perspective depth transformation. This method is equivalent to the OpenGL command glFrustum()
		//perspective(fovy, aspect, near, far) - This method returns a perspective transformation, the only difference is the meaning of the input parameters
		//orthographic(left, right, bottom, top, near, far) - Returns a matrix that represents an orthographic transformation
		//lookAt(pos, target, up=(0,0,1)) - Returns a transformation that moves the coordinate system to position pos and rotates it so that the z-axis points onto point target

	private:
		const float	&	operator[](const u32 index) const { return m_matrix[index]; }


	private:	
		float		m_matrix[16];
	};

	typedef C_Matrix4x4 C_Matrix;
}}