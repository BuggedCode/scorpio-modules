/*
* filename:			C_BBox.cpp
*
* author:				Kral Jozef
* date:					Januar 2007
* version:			1.00
* description:	Bounding box class
*/

#include "C_BBox.h"

namespace scorpio{ namespace sysmath{

	C_BBox::C_BBox(const C_Vector3 & vct1, const C_Vector3 & vct2)
	{
		vct1.m_x < vct2.m_x ? m_min.m_x = vct1.m_x,	m_max.m_x = vct2.m_x : m_min.m_x = vct2.m_x,	m_max.m_x = vct1.m_x;
		vct1.m_y < vct2.m_y ? m_min.m_y = vct1.m_y,	m_max.m_y = vct2.m_y : m_min.m_y = vct2.m_y,	m_max.m_y = vct1.m_y;
		vct1.m_x < vct2.m_z ? m_min.m_z = vct1.m_z,	m_max.m_z = vct2.m_z : m_min.m_z = vct2.m_z,	m_max.m_z = vct1.m_z;
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_BBox::include(const C_Vector3 & vct)
	{	
		vct.m_x < m_min.m_x ? m_min.m_x = vct.m_x : ( vct.m_x > m_max.m_x ) ? m_max.m_x = vct.m_x : m_max.m_x += 0;//nop operacia
		vct.m_y < m_min.m_y ? m_min.m_y = vct.m_y : ( vct.m_y > m_max.m_y ) ? m_max.m_y = vct.m_y : m_max.m_x += 0;//nop operacia
		vct.m_z < m_min.m_z ? m_min.m_z = vct.m_z : ( vct.m_z > m_max.m_z ) ? m_max.m_z = vct.m_z : m_max.m_x += 0;//nop operacia
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_BBox::include(const C_BBox & box)
	{
		include(box.m_max);
		include(box.m_min);
	}

	/////////////////////////////////////////////////////////////////////////////
	C_Vector3 C_BBox::getPivot() const
	{	
		float x = fabs(m_max.m_x - m_min.m_x) / 2.f;
		float y = fabs(m_max.m_y - m_min.m_y) / 2.f;
		float z = fabs(m_max.m_z - m_min.m_z) / 2.f;
		return C_Vector3(m_min.m_x+x, m_min.m_y+y, m_min.m_z+z);
	}
}}