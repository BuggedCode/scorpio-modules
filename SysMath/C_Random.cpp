/*
* filename:			C_Random.cpp
*
* author:			Kral Jozef
* date:				10/31/2010		20:34
* version:			1.00
* brief:
*/

#include "C_Random.h"
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_real.hpp>
#include <SysMath/MathUtils.h>
#include <Common/Assert.h>


namespace scorpio{ namespace sysmath{

	boost::mt19937 C_Random::m_EngGen;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Init
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Random::Init(u32 seed)
	{
		srand(seed);
		m_EngGen.seed((boost::uint32_t)seed);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Random
	//
	//////////////////////////////////////////////////////////////////////////
	float C_Random::Random(float lowerBound, float upperBound)
	{
		SC_ASSERT(!isZero(upperBound - lowerBound));
		boost::uniform_real<float>	distribution(lowerBound, upperBound);
		boost::variate_generator<boost::mt19937&, boost::uniform_real<float> > rndFltGen(m_EngGen, distribution);
		return rndFltGen();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Random
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_Random::Random(u32 lowerBound, u32 upperBound)
	{
		SC_ASSERT((upperBound != lowerBound));
		u32 val = rand();
		val %= (upperBound - lowerBound);
		val += lowerBound;
		return val;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Random
	//
	//////////////////////////////////////////////////////////////////////////
	float C_Random::Random(float upperBound)
	{
		boost::uniform_real<float>	distribution(0.f, upperBound);
		boost::variate_generator<boost::mt19937&, boost::uniform_real<float> > rndFltGen(m_EngGen, distribution);
		return rndFltGen();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Random
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_Random::Random(u32 upperBound)
	{
		SC_ASSERT(upperBound != 0);
		u32 val = rand();
		val %= upperBound;
		return val;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RandomColor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Vector4 C_Random::RandomColor()
	{
		C_Vector4 vctColor;
		vctColor.m_x = C_Random::Random(1.f);
		vctColor.m_y = C_Random::Random(1.f);
		vctColor.m_z = C_Random::Random(1.f);
		vctColor.m_w = 1.f;	//non transparent
		return vctColor;
	}


}}