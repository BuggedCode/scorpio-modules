/*
* filename:			C_Quaternion.cpp
*
* author:				Kral Jozef
* date:					December 2006
* version:			1.00
* description:	Quaternion operation
*/

#include "C_Quaternion.h"
#include "C_Matrix.h"
#include "C_Vector.h"
#include "MathUtils.h"

namespace scorpio{ namespace sysmath{

	/////////////////////////////////////////////////////////////////////////////
	C_Quaternion::C_Quaternion()
	{
		m_x = m_y = m_z = m_w = 0.f;
	}

	/////////////////////////////////////////////////////////////////////////////
	C_Quaternion::C_Quaternion(const C_Quaternion & copy)
	{
		m_x = copy.m_x;
		m_y = copy.m_y;
		m_z = copy.m_z;
		m_w = copy.m_w;
	}

	/////////////////////////////////////////////////////////////////////////////
	C_Quaternion::C_Quaternion(float x, float y, float z)
	{
		/*Qx = [ cos(a/2), (sin(a/2), 0, 0)]
		Qy = [ cos(b/2), (0, sin(b/2), 0)]
		Qz = [ cos(c/2), (0, 0, sin(c/2))]
		this = Qx * Qy * Qz*/

		/*float halfx = DEGTORAD(x) * 0.5f;
		float halfy = DEGTORAD(y) * 0.5f;
		float halfz = DEGTORAD(z) * 0.5f;

		float cosx = cosf(halfx);
		float cosy = cosf(halfy);
		float cosz = cosf(halfz);

		float sinx = sinf(halfx);
		float siny = sinf(halfy);
		float sinz = sinf(halfz);

		float cosycosz = cosy * cosz;
		float sinysinz = siny * sinz;

		w = cosx * cosycosz + sinx * sinysinz;
		v.m_x = sinx * cosycosz - cosx * sinysinz;
		v.m_y = cosx * siny * cosz + sinx * cosy * sinz;
		v.m_z = cosx * cosy * sinz - sinx * siny * cosz;

		Normalize();*/
	}

	/////////////////////////////////////////////////////////////////////////////
	C_Quaternion::C_Quaternion(const C_Vector3 & vctDir, float fAngle)
	{
		float sin_ = (float)sin(fAngle/2);
		m_w = cos(fAngle/2);
		m_x = vctDir.m_x * sin_;
		m_y = vctDir.m_y * sin_;
		m_z = vctDir.m_z * sin_;
		normalize();
	}

	/////////////////////////////////////////////////////////////////////////////
	C_Quaternion::~C_Quaternion()
	{
#ifdef DEBUG
		m_x = m_y = m_z = m_w = 0.f;
#endif
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_Quaternion::operator=(const C_Quaternion & copy)
	{
		m_x = copy.m_x;
		m_y = copy.m_y;
		m_z = copy.m_z;
		m_w = copy.m_w;
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_Quaternion::operator*=(const C_Quaternion & quat)
	{
		float w_ = m_w*quat.m_w - m_x*quat.m_x - m_y*quat.m_y - m_z*quat.m_z;
		float x_ = m_w*quat.m_x + m_x*quat.m_w + m_y*quat.m_z - m_z*quat.m_y;
		float y_ = m_w*quat.m_y + m_y*quat.m_w + m_z*quat.m_x - m_x*quat.m_z;
		float z_ = m_w*quat.m_z + m_z*quat.m_w + m_x*quat.m_y - m_y*quat.m_x;
		m_w = w_; 
		m_x = x_;
		m_y = y_;
		m_z = z_;
	}

	/////////////////////////////////////////////////////////////////////////////
	C_Quaternion C_Quaternion::operator*(const C_Quaternion & quat)
	{
		float w_ = m_w*quat.m_w - m_x*quat.m_x - m_y*quat.m_y - m_z*quat.m_z;
		float x_ = m_w*quat.m_x + m_x*quat.m_w + m_y*quat.m_z - m_z*quat.m_y;
		float y_ = m_w*quat.m_y + m_y*quat.m_w + m_z*quat.m_x - m_x*quat.m_z;
		float z_ = m_w*quat.m_z + m_z*quat.m_w + m_x*quat.m_y - m_y*quat.m_x;
		m_w = w_;
		m_x = x_;
		m_y = y_;
		m_z = z_;

		return C_Quaternion(*this);
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_Quaternion::normalize()
	{
		float len = length();
		if (isZero(len))
			len = 1.f;

		float fDivider = 1.f / len;
		m_x *= fDivider;
		m_y *= fDivider;
		m_z *= fDivider;
		m_w *= fDivider;
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_Quaternion::toMatrix(C_Matrix4x4 & matrix) const
	{
		float x2_ = m_x + m_x,		y2_ = m_y + m_y,		z2_ = m_z + m_z;
		float xx2_ = x2_ * m_x,		xy2_ = x2_ * m_y,		xz2_ = x2_ * m_z;
		float yy2_ = y2_ * m_y,		yz2_ = y2_ * m_z,		wy2_ = y2_ * m_w;
		float zz2_ = z2_ * m_z,    wz2_ = z2_ * m_w,		wx2_ = x2_ * m_w;

		matrix.m_matrix[0] = 1.f-(yy2_+zz2_);
		matrix.m_matrix[1] = xy2_ + wz2_;
		matrix.m_matrix[2] = xz2_ + wy2_;
		matrix.m_matrix[3] = 0.f;

		matrix.m_matrix[4] = xy2_ - wz2_;
		matrix.m_matrix[5] = 1.f-(xx2_+zz2_);
		matrix.m_matrix[6] = yz2_ + wx2_;
		matrix.m_matrix[7] = 0.f;

		matrix.m_matrix[8]  = xz2_ + wy2_;
		matrix.m_matrix[9] = yz2_ - wz2_;
		matrix.m_matrix[10] = 1.f-(xx2_+yy2_);
		matrix.m_matrix[11] = 0.f;

		matrix.m_matrix[12] = 0.f;
		matrix.m_matrix[13] = 0.f;
		matrix.m_matrix[14] = 0.f;
		matrix.m_matrix[15] = 1.f;
	}

	/////////////////////////////////////////////////////////////////////////////
	void C_Quaternion::toAxisAngle(C_Vector3 & vctDir, float & fAngle) const
	{
		float len = sqrt(m_x*m_x + m_y*m_y + m_z*m_z);
		if (isZero(len))
		{
			fAngle = 0.f;
			vctDir.m_x = 1.f;
			vctDir.m_y = 0.f;
			vctDir.m_z = 0.f;
		}
		else
		{
			float scale = 1.f / len;
			fAngle = 2*acos(m_w);
			vctDir.m_x = m_x * scale;
			vctDir.m_y = m_y * scale;
			vctDir.m_z = m_z * scale;
		}
	}
}}