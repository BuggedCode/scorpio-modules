/*
* filename:			C_Rect.h
*
* author:			Kral Jozef
* date:				03/18/2009		20:19
* version:			1.00
* brief:				simple rectangle class for integer/float
*/

#pragma once

#include "C_Point.h"
#include "C_Vector2.h"

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Rect<T>
	template<class T>
	class C_RectBase
	{
	public:
		//@ default c-tor
		C_RectBase()
			: m_left(0), m_top(0), m_right(0), m_bottom(0) {};

		//@ c-tor
		C_RectBase(T inLeft, T inTop, s32 inWidth, s32 inHeight)
			: m_left(inLeft), m_top(inTop), m_right(inLeft + inWidth), m_bottom(inTop + inHeight) {};


		//@ copy c-tor
		C_RectBase(const C_RectBase & copy)
		{
			m_left	= copy.m_left;
			m_top		= copy.m_top;
			m_right	= copy.m_right;
			m_bottom	= copy.m_bottom;
		}

		//@ assignment operator
		const C_RectBase & operator=(const C_RectBase & copy)
		{
			m_left	= copy.m_left;
			m_top		= copy.m_top;
			m_right	= copy.m_right;
			m_bottom	= copy.m_bottom;
			return *this;
		}

		//@ d-tor
		~C_RectBase() {};


		//@ SetWidth
		void	SetWidth(u32 inWidth) { m_right = m_left + inWidth; }
		//@ GetWidth()
		T		GetWidth() const { return m_right - m_left; }
		//@ SetHeight
		void	SetHeight(u32 inHeight) { m_bottom = m_top + inHeight; }
		//@ GetHeight
		T		GetHeight() const{ return m_bottom - m_top; }
		//@ MoveTo
		void	MoveTo(s32 inPosX, s32 inPosY)
		{
			m_right = inPosX + GetWidth();
			m_left = inPosX;
			m_bottom = inPosY + GetHeight();
			m_top = inPosY;
		}
		//@ MoveCenterTo
		void	MoveCenterTo(const sysmath::C_Vector2 & vctCenter)
		{
			C_Vector2 off = vctCenter - GetCenter();
			MoveBy(off);
		}

		//@ MoveBy
		void	MoveBy(C_Vector2 & vctOffset)
		{
			m_right += (T)vctOffset.m_x;
			m_left += (T)vctOffset.m_x;
			m_bottom += (T)vctOffset.m_y;
			m_top += (T)vctOffset.m_y;
		}

		//@ MoveBy
		void	MoveBy(u32 inOffsetX, u32 inOffsetY)
		{
			m_right += inOffsetX;
			m_left += inOffsetX;
			m_bottom += inOffsetY;
			m_top += inOffsetY;
		}

		//@ IsInside
		bool	IsInside(const C_Point & point) const
		{
			if (point.x < m_left || point.x >= m_right || point.y < m_top || point.y >= m_bottom)
				return false;

			return true;
		}

		//@ GetCenter
		C_Vector2	GetCenter() const
		{
			C_Vector2 vct((m_left + m_right) / 2.f, (m_top + m_bottom) / 2.f);
			return vct;
		}

		//@ Scale - scale in local, center for Scale is in rectangle pivot
		void Scale(float fScale)
		{
			C_Vector2 vctCenter = GetCenter();
			float fHalfWidth = (GetWidth() * fScale) / 2.f;
			float fHalfHeight = (GetHeight() * fScale) / 2.f;

			m_left = (T)(vctCenter.m_x - fHalfWidth);
			m_right = (T)(vctCenter.m_x + fHalfWidth);
			
			m_top = (T)(vctCenter.m_y - fHalfHeight);
			m_bottom = (T)(vctCenter.m_y + fHalfHeight);
		}

	public:
		T		m_left;
		T		m_top;
		T		m_right;
		T		m_bottom;
	};

	typedef C_RectBase<s32>		C_Rect;
	typedef C_RectBase<float>	C_FloatRect;

}}
