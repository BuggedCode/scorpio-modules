/*
	This file is part of the Hyperion project.
	Copyright (C) 2005 Stormheads. All rights reserved.
	http://www.stormheads.com

	For conditions of distribution and use, see the accompanying COPYING file.

	Author:
		Jozef 'oiko' Kral : Sat Oct 15 12:53:12 CET 2005
	Description:
		Class for C_Line2d manipulation
*/

#ifndef _LINE2D_H
#define _LINE2D_H

#pragma once

#include "C_Vector2.h"
#include "MathTypes.h"
#include <SysMath/SysMathApi.h>

namespace scorpio{ namespace sysmath{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Line2d
	class SYSMATH_API C_Line2d
	{
	public:
		//@ default c-tor
		C_Line2d() {};
		//@ c-tor
		C_Line2d( const C_Vector2 & pVctStart, const C_Vector2 & pVctEnd )
			: m_pStart(pVctStart), m_pEnd(pVctEnd) {};

		//@ d-tor
		~C_Line2d() {};

		//Set/Get methods
		void				SetStart(const C_Vector2 & vctStart) { m_pStart = vctStart; }
		void				SetEnd(const C_Vector2 & vctEnd) { m_pEnd = vctEnd; }
		const C_Vector2	GetStart() const { return m_pStart; }
		const C_Vector2	GetEnd() const { return m_pEnd; }  

		//Others method  
		float				getLength() const;
		bool				Intersection( const C_Line2d & pLine, C_Vector2 & vctIntOut ) const;
		float				distFromLine( C_Vector2 & pVct );
		float				distFromSegment( C_Vector2 & pVct );
		void				projectToLine( C_Vector2 & pVctIn, C_Vector2 & pVctOut );
		bool				isInSegment( C_Vector2 & pVct );
		float				paramInSegment( C_Vector2 & pVct );
		T_SidePosition	whichSide( C_Vector2 & pVct );
		bool				isLeft( C_Vector2 & pVct );
		bool				isRight( C_Vector2 & pVct );
		bool				isCollinear( C_Vector2 & pVct );
		bool				isBetween( C_Vector2 & pVct );

	private:
		float				area2( C_Vector2 & pVct );


	private:
		C_Vector2		m_pStart;
		C_Vector2		m_pEnd;
	};
}}

#endif