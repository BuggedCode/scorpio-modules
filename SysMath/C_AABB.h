/*
* filename:			C_AABB.h
*
* author:			Kral Jozef
* date:				02/12/2008		19:44
* version:			1.00
* brief:
*/

#pragma once

#include "SysMathApi.h"
#include "C_Vector.h"

namespace scorpio{ namespace sysmath{

#define C_INVALID_VALUE	1e+16f;		//nepouzil sem static const float protoze jenom integral type can be initialized within class, a nexcel som pridavat cpp

	//////////////////////////////////////////////////////////////////////////
	//@ C_AABB
	class C_AABB
	{
	public:
		//////////////////////////////////////////////////////////////////////////
		//@ c-tor
		C_AABB() { invalidate(); }
		//////////////////////////////////////////////////////////////////////////
		//@ c-tor
		C_AABB(const C_Vector & min, const C_Vector & max) : m_min(min), m_max(max) {};

		//////////////////////////////////////////////////////////////////////////
		//@ d-tor
		virtual ~C_AABB() {};

		//////////////////////////////////////////////////////////////////////////
		//@ Get/Set
		const C_Vector			&	getMin() const { return m_min; }
		const C_Vector			&	getMax() const { return m_max; }
		const C_Vector				getCenter() const { return (m_max + m_min) * 0.5f; }

		void							setMin(const C_Vector & min) { m_min = min; }
		void							setMax(const C_Vector & max) { m_max = max; }


		//////////////////////////////////////////////////////////////////////////
		//@ Modify methods
		inline void					absorb(const C_Vector & point);
		inline void					absorb(const C_AABB & box);

		//void							expand() const;

		//////////////////////////////////////////////////////////////////////////
		//@ Arithmetic methods
		inline float				distance2(const C_Vector & point) const;	//vzialenost na druhu
		float							distance(const C_Vector & point) const { return sqrtf(distance2(point)); }

		//////////////////////////////////////////////////////////////////////////
		//@ clamp point into bounding box
		inline C_Vector			snap(const C_Vector & point) const;

		//////////////////////////////////////////////////////////////////////////
		//@ invalidate
		inline void					invalidate() { m_min.m_x = m_min.m_y = m_min.m_z = m_max.m_x = m_max.m_y = m_max.m_z = C_INVALID_VALUE; }

	private:
		C_Vector		m_min;
		C_Vector		m_max;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ implementacia inlinovanych metod

	//////////////////////////////////////////////////////////////////////////
	//@ absorb
	void C_AABB::absorb(const C_Vector & point)
	{
		if(m_min.m_x > point.m_x) m_min.m_x = point.m_x;
		if(m_max.m_x < point.m_x) m_max.m_x = point.m_x;
		if(m_min.m_y > point.m_y) m_min.m_y = point.m_y;
		if(m_max.m_y < point.m_y) m_max.m_y = point.m_y;
		if(m_min.m_z > point.m_z) m_min.m_z = point.m_z;
		if(m_max.m_z < point.m_z) m_max.m_z = point.m_z;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ absorb
	void C_AABB::absorb(const C_AABB & box)
	{
		if(m_min.m_x > box.m_min.m_x) m_min.m_x = box.m_min.m_x;
		if(m_max.m_x < box.m_max.m_x) m_max.m_x = box.m_max.m_x;
		if(m_min.m_y > box.m_min.m_y) m_min.m_y = box.m_min.m_y;
		if(m_max.m_y < box.m_max.m_y) m_max.m_y = box.m_max.m_y;
		if(m_min.m_z > box.m_min.m_z) m_min.m_z = box.m_min.m_z;
		if(m_max.m_z < box.m_max.m_z) m_max.m_z = box.m_max.m_z;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ distance2
	float C_AABB::distance2(const C_Vector & point) const
	{
		float dst2 = 0.f;
		if(point.m_x < m_min.m_x) 
		{
			dst2 = point.m_x - m_min.m_x;
			dst2 *= dst2;
		}
		else if(point.m_x > m_max.m_x) 
		{
			dst2 = point.m_x - m_max.m_x;
			dst2 *= dst2;
		} 
		
		if(point.m_y < m_min.m_y) 
		{
			float dy = point.m_y - m_min.m_y;
			dst2 += dy*dy;
		}
		else if(point.m_y > m_max.m_y) 
		{
			float dy = point.m_y - m_max.m_y;
			dst2 += dy*dy;
		}

		if(point.m_z < m_min.m_z)
		{
			float dz = point.m_z - m_min.m_z;
			return dst2 + dz*dz;
		}
		else if(point.m_z > m_max.m_z) 
		{
			float dz = point.m_z - m_max.m_z;
			return dst2 + dz*dz;
		}
		return dst2;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ snap
	C_Vector C_AABB::snap(const C_Vector & point) const
	{
		C_Vector sVct;
		if(point.m_x < m_min.m_x) 
			sVct.m_x = m_min.m_x;
		else if(point.m_x > m_max.m_x) 
			sVct.m_x = m_max.m_x;
		else 
			sVct.m_x = point.m_x;

		if(point.m_y < m_min.m_y) 
			sVct.m_y = m_min.m_y;
		else if(point.m_y > m_max.m_y) 
			sVct.m_y = m_max.m_y;
		else 
			sVct.m_y = point.m_y;

		if(point.m_z < m_min.m_z)
			sVct.m_z = m_min.m_z;
		else if(point.m_z > m_max.m_z)
			sVct.m_z = m_max.m_z;
		else 
			sVct.m_z = point.m_z;
		
		return sVct;
	}
}}