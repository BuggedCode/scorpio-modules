/*
* filename:			MathUtils.inl
*
* author:			Kral Jozef
* date:				06/13/2009		17:48
* version:			1.00
* brief:
*/

#pragma once

#include <Common/Types.h>

namespace scorpio{ namespace sysmath{

#ifndef EPSILON
	#define EPSILON 0.000001F
#endif


	//////////////////////////////////////////////////////////////////////////
	//@ isZero
	//////////////////////////////////////////////////////////////////////////
	inline bool isZero( double pValue )
	{
		if ( pValue < EPSILON && pValue > -EPSILON )
			return true;
		else
			return false;
	}



	//////////////////////////////////////////////////////////////////////////
	//@ isZero
	//////////////////////////////////////////////////////////////////////////
	inline bool isZero( double pValue, float pEpsilon )
	{
		if ( pValue < pEpsilon && pValue > -pEpsilon )
			return true;
		else
			return false;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ correctAngle
	//////////////////////////////////////////////////////////////////////////
	inline float correctAngle(float pValue)
	{
		while (pValue > 360)
			pValue -= 360;

		while (pValue < 0)
			pValue += 360;

		return pValue;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ round
	//////////////////////////////////////////////////////////////////////////
	inline s32 round(double pValue)
	{
		return (pValue >= 0) ? s32(pValue + 0.5) : s32(pValue - 0.5);
	}


}}