/*
* filename:			C_BBox.h
*
* author:			Kral Jozef
* date:				Januar 2007
* version:			1.00
* description:		3D Bounding box class
*/

#pragma once

#include "C_Vector.h"

namespace scorpio{ namespace sysmath{

	class C_BSphere;

	//////////////////////////////////////////////////////////////////////////
	//@ C_BBox
	class SYSMATH_API C_BBox
	{
		friend class C_BSphere;

	public:	
		C_BBox(const C_Vector3 & vct1, const C_Vector3 & vct2);
		~C_BBox() {};

		void				include(const C_BBox & box);
		C_Vector3				getPivot() const;
		const C_Vector3	&	getMax() { return m_max; }
		const C_Vector3	&	getMin() { return m_min; }

	private:
		C_BBox();		//not implemented

		void	include(const C_Vector3 & vct);

	private:
		C_Vector3	m_min;
		C_Vector3	m_max;
	};

}}