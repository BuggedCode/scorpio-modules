/*
* filename:			C_Spline2d.h
*
* author:			Kral Jozef
* date:				10/04/2010		21:47
* version:			1.00
* brief:
*/

#pragma once

#include <SysMath/SysMathApi.h>
#include <vector>
#include "C_Vector2.h"

namespace scorpio{ namespace sysmath{

	class C_Point;

	struct S_WeightPos
	{
		C_Vector2	m_pos;
		C_Vector2	m_prevWeight;
		C_Vector2	m_nextWeight;
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_Spline2d
	class SYSMATH_API C_Spline2d
	{
	public:
		//@ c-tor
		C_Spline2d() {};
		//@ d-tor
		virtual ~C_Spline2d() {};

		//@ AddPosition
		void					AddPosition(const C_Vector2 & vctPos) { m_vctPos.push_back(vctPos); }

		//@ DoSpline
		//@ nIterCount - iteration count
		void					ComputeSpline(u8 nIterCount);

		//@ OBSOLETE
		std::vector<C_Point>	ToPointVector() const;

		//@ Fill output container with positions
		//@ bCutFirstLast - exclude first & last position - suitable when spline use help positions - most of time
		void					Fill(std::vector<C_Vector2> & outputContainer/*, bool bCutFirstLast*/) const;

	private:
		//@ DoSpline
		void					DoSpline();
		//@ GetWeight
		//@ outPrevWeight - left weight for actualVct
		//@ outNextWeight - right weight for actualVct
		static void			GetWeight(const C_Vector2 & vctPrev, const C_Vector2 & vctAct, const C_Vector2 & vctNext, S_WeightPos & actWeight);

		//@ GetPointAtParam
		static C_Vector2	GetPointAtParam(const S_WeightPos & segStart, const S_WeightPos & segEnd, float fParam);


		typedef std::vector<C_Vector2>	T_Container;
		T_Container	m_vctPos;
	};

}}