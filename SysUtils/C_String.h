/*
* filename:			C_String.cpp
*
* author:			Kral Jozef
* date:				October 2006
* version:			1.00
* description:		Class for manipulate with string
*/

#pragma once

#include "string.h"
#include "SysUtilsApi.h"
#include <vector>
#include <Common/types.h>
#include <Common/Assert.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_StringInfoData
	class C_StringInfoData
	{
	public:
		u32		m_refCount;
		u32		m_length;		//size of string + 1 - number of characters in string including termination character
		u32		m_allocated;	//size of allocated memory for string
		char	*	getData() { return (char*)(this + 1); }
	};


	class C_String;
	typedef std::vector<C_String>	T_StringVector;

	//////////////////////////////////////////////////////////////////////////
	//@ C_String
	class SYSUTILS_API C_String
	{
	public:
		//////////////////////////////////////////////////////////////////////////
		//@ implicit c-tor
		C_String();

		//@ c-tor
		C_String(u32 preallocLen);

		//////////////////////////////////////////////////////////////////////////
		//@ c-tor
		C_String(const char * string);

		//////////////////////////////////////////////////////////////////////////
		//@ copy c-tor
		C_String(const C_String & string) { m_string = string.m_string; increaseReference(); }

		//////////////////////////////////////////////////////////////////////////
		//@ init c-tor
		C_String(const char * string, u32 start, u32 end);

		//////////////////////////////////////////////////////////////////////////
		//@ d-tor
		~C_String() { decreaseReference(); }

		//////////////////////////////////////////////////////////////////////////
		//@ assigment operator
		C_String & operator=(const C_String &copy)
		{
			copy.increaseReference();
			decreaseReference();
			m_string = copy.m_string;
			increaseReference();
			copy.decreaseReference();
			return *this;
		}

		//////////////////////////////////////////////////////////////////////////
		C_String & operator+=(const char * string);
		//////////////////////////////////////////////////////////////////////////
		//C_String & operator+(const C_String & str);
		//////////////////////////////////////////////////////////////////////////
		operator const char *() const { return m_string; }
		//////////////////////////////////////////////////////////////////////////
		bool operator==(const C_String & str) const { return strcmp(m_string, str) == 0; }
		//////////////////////////////////////////////////////////////////////////
		bool operator==(const char * str) const { return strcmp(m_string, str) == 0; }
		//////////////////////////////////////////////////////////////////////////
		bool operator!=(const C_String & str) const { return strcmp(m_string, str) != 0; }
		//////////////////////////////////////////////////////////////////////////
		bool operator!=(const char * str) const { return strcmp(m_string, str) != 0; }
		//////////////////////////////////////////////////////////////////////////
		bool operator<(const C_String & str) const { return strcmp(m_string, str) < 0; }
		//////////////////////////////////////////////////////////////////////////
		bool operator<(const char * str) const { return strcmp(m_string, str) < 0; }
		//////////////////////////////////////////////////////////////////////////
		char operator[] (u32 index) const
		{
			C_StringInfoData * info = (C_StringInfoData *)m_string - 1;
			if (index < (u32)(info->m_length - 1))
				return m_string[index];

			SC_ASSERT(false);
			return 0;
		}
		/*//////////////////////////////////////////////////////////////////////////
		char operator[] (size_t index) const
		{
			C_StringInfoData * info = (C_StringInfoData *)m_string - 1;
			if (index < info->m_length - 1)
				return m_string[index];

			SC_ASSERT(false);
			return 0;
		}*/


		const char *	c_str() const { return m_string; }
		u32				length() const;
		u32				size() const { return length(); }
		bool			empty() const { return m_string[0] == 0; }

		//@ Format
		static C_String	Format(const char * format, ...);
		//@ Substring - return substring
		static C_String	SubString(const C_String & str, u32 start, u32 end);
		//@ Find first occurance of character
		u32				Find(char character) const;
		//@ Find first occurance of substring
		u32				Find(C_String subStr) const;
		//@ FindLast - find last occurance of character
		u32				FindLast(char character) const;
		//@ Left - return new substring form left into nLastIndex - vratane indexu
		C_String			Left(u32 nLastIndex) const;
		//@ Right - return new substring form right from firstIndex to end
		C_String			Right(u32 nFirstIndex) const;
		//@ Split - return list of strings splitted by character
		void				Split(char character, T_StringVector & outStrList) const;
		//@ Split - return list of strings splitted by string
		void				Split(C_String strSeparator, T_StringVector & outStrList) const;
		//@ TrimLeft
		C_String			TrimLeft(char character) const;
		//@ TrimRight
		C_String			TrimRight(char character) const;
		//@ ToLower
		C_String			ToLower() const;
		//@ ToUpper
		C_String			ToUpper() const;
		//@ Replace
		C_String			replace(char oldChar, char newChar) const;

		//@ ToHex
#ifndef _HYBRID
		u32				ToHex() const;
#endif
		//@ ToInt
		s32				ToInt() const;
		//@ ToFloat
		float				ToFloat() const;
		//@ ToDouble
		double			ToDouble() const;
		//@ ToBool
		bool				ToBool() const;



	private:
		//////////////////////////////////////////////////////////////////////////
		//@ alocate data for string
		bool allocStringInfo(u32 pLength);

		//////////////////////////////////////////////////////////////////////////
		void increaseReference() const
		{
			C_StringInfoData * info = (C_StringInfoData*)m_string - 1;
			info->m_refCount++;
		}

		//////////////////////////////////////////////////////////////////////////
		void decreaseReference() const
		{		
			C_StringInfoData * info = (C_StringInfoData *)m_string - 1;
			info->m_refCount--;
			if (!info->m_refCount)
				delete info;
		}

		//////////////////////////////////////////////////////////////////////////
		//@ expandData
		bool expandData(u32 newLength);

	private:
		char	*	m_string;
	};
}}

typedef scorpio::sysutils::C_String		T_String;