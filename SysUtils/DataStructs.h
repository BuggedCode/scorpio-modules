/*
* filename:			DataStructs.h
*
* author:			Kral Jozef
* date:				1/25/2012		17:31
* version:			1.00
* brief:
*/

#pragma once

#include "C_RingBuffer.h"

namespace scorpio{

	template<typename Value>
	struct T_Queue
	{
		typedef sysutils::C_RingBuffer<Value> type;
	};
}