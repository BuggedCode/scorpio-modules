/*
* filename:			C_Image.h
*
* author:			Kral Jozef
* date:				Jun 2003
* version:			1.00
* brief:				computation of 32/62 bit hash
*/

#pragma once

#include <Common/types.h>
#include "SysUtilsApi.h"

namespace scorpio{ namespace sysutils
{
	//////////////////////////////////////////////////////////////////////////
	//@ C_FNVHash - static class
	class SYSUTILS_API C_FNVHash
	{
	public:
		//////////////////////////////////////////////////////////////////////////
		//@ computeHash32
		static T_Hash32		computeHash32(const char * str, T_Hash32 hval = FNV1_32_INIT);

		//////////////////////////////////////////////////////////////////////////
		//@ computeHash32
		static T_Hash32		computeHash32(void * buffer, size_t size, T_Hash32 hval = FNV1_32_INIT);

		//////////////////////////////////////////////////////////////////////////
		//@ computeHash32
		static T_Hash64		computeHash64(const char * str, T_Hash64 hval);

		//////////////////////////////////////////////////////////////////////////
		//@ computeHash32
		static T_Hash64		computeHash64(void * buffer, size_t size, T_Hash64 hval);

	private:
		static const T_Hash32	FNV1_32_INIT = (T_Hash32)0x811c9dc5;
		static const T_Hash64	FNV1_64_INIT = (T_Hash64)0xcbf29ce484222325;
	};
}}