/*
* filename:			C_Image.h
*
* author:			Kral Jozef
* date:				Jun 2003
* version:			1.00
* brief:				C_Image class
*/

#pragma once

/*#include "SysUtilsApi.h"
#include <FreeImage/FreeImage.h>

#ifdef SYSUTILS_EXPORTS
	#pragma comment(lib, "FreeImage.lib")
#endif


namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Image
	class SYSUTILS_API C_Image
	{
	public:
		C_Image();
		~C_Image();
		const char		*	fileName;
		int					width;
		int					height;
		int					bpp;    //bites per pixel
		unsigned char	*	data;

	private:
		FIBITMAP			*	bitmapFI;

	public:
		void        setBitmapFI( FIBITMAP * pBitmapFI ) { bitmapFI = pBitmapFI; }
		FIBITMAP  * getBitmapFI() { return bitmapFI; }

		bool			Load( const char * pFileName );
	};
}}*/