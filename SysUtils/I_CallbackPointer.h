/*
* filename:			I_CallbackPointer.h
*
* author:			Kral Jozef
* date:				05/08/2011		9:22
* version:			1.00
* brief:
*/

#pragma once

#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ I_ContextDescriptor
	class I_ContextDescriptor
	{
	public:
		//@ c-tor
		I_ContextDescriptor(void * contextPtr) : m_Context(contextPtr) {};

		void		*	m_Context;	//BLEEE
	};


	DECLARE_SHARED_PTR(I_ContextDescriptor,	T_ContextDescriptor);


	//////////////////////////////////////////////////////////////////////////
	//@ I_CallbackPointer
	class I_CallbackPointer
	{
	public:
		//@ c-tor
		virtual ~I_CallbackPointer() {}
		//@ Execute
		virtual void		Execute() {};
		//@ Execute
		virtual void		Execute(T_Hash32 param1, T_Hash32 param2) {};
		//@ IsOwnedByContext - function for checking if callback pointer is for I_ContextDescriptor wrapper
		virtual bool		IsOwnedByContext(T_ContextDescriptor) { return false; }
	};

	DECLARE_SHARED_PTR(I_CallbackPointer,	T_CallbackPtr);

}}