/*
* filename:			C_CharacterTable.h
*
* author:			Kral Jozef
* date:				9/22/2011		21:06
* version:			1.00
* brief:			Define table for conversion form UTF8 into ASCII
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <Common/C_Singleton.h>
#include <SysUtils/FileSystem/C_Stream.h>
#include <map>

namespace scorpio{ namespace sysutils{


	//////////////////////////////////////////////////////////////////////////
	// C_CharacterTable
	class SYSUTILS_API C_CharacterTable
	{
		friend class C_Singleton<C_CharacterTable>;
	public:
		//@ Initialize
		bool					Initialize(const T_Stream inStream);
		//@ ConvertCharacter
		char					ConvertCharacter(u8 utf8code, u8 utf8character) const;

	private:
		typedef u16	T_Utf8Code;
		typedef std::map<T_Utf8Code, char>	T_CharacterMap;
		T_CharacterMap		m_characterMap;
	};


	typedef C_Singleton<C_CharacterTable>	T_CharacterTable;

}}

SYSUTILS_EXP_TEMP_INST template class SYSUTILS_API C_Singleton<scorpio::sysutils::C_CharacterTable>;