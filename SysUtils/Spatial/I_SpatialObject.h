/*
* filename:			I_SpatialObject.h
*
* author:			Kral Jozef
* date:				02/06/2008		21:57
* version:			1.00
* brief:				Interface of base objects from spatial subdivision
*/

#pragma once

#include <SysMath/C_AABB2.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ I_SpatialObject
	class I_SpatialObject
	{
	public:
		//@ c-tor
		I_SpatialObject() {};
		//@ d-tor
		virtual ~I_SpatialObject() {};

		//////////////////////////////////////////////////////////////////////////
		//@ vrati hierarchicky AABB
		virtual const sysmath::C_AABB2	&	GetAABB() const = 0;
	};
}}