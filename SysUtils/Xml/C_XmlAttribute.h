/*
* filename:			C_XmlAttribute.h
*
* author:			Kral Jozef
* date:				04/23/2009		22:20
* version:			1.00
* brief:				XML Attribute - attr can hold only string value
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/C_String.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_XmlAttribute
	class SYSUTILS_API C_XmlAttribute
	{
	public:
		//@ c-tor
		C_XmlAttribute(const char * attName, const char * strVal)
			: m_name(attName), m_strValue(strVal) {};

		//@ d-tor
		virtual ~C_XmlAttribute() {};

		//@ GetName
		const T_String		&	GetName() const { return m_name; }
		//@ GetValue
		const T_String		&	GetValue() const { return m_strValue; }


	private:
		T_String		m_name;
		T_String		m_strValue;	//attribut can hold only string data
	};

}}