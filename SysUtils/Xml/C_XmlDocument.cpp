/*
* filename:			C_XmlDocument.cpp
*
* author:			Kral Jozef
* date:				01/11/2011		14:39
* version:			1.00
* brief:
*/

#include "C_XmlDocument.h"
#include <SysUtils/C_TraceClient.h>
#include <boost/tokenizer.hpp>
//#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace sysutils{


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Load
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_XmlDocument::Load(const T_Stream stream)
	{
		if (!stream)
		{
			TRACE_E("Invalid stream");
			return false;
		}

		if (!m_file.Load(stream))
		{
			TRACE_FE("Can not open file: %s", stream->GetFileName());
			return false;
		}

		//parse xml
		if (!m_file.IsValid())
			return false;

		m_root = m_file.GetRoot();
		if (!m_root || !m_root->IsValid())
			return false;

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SelectNode
	//
	//////////////////////////////////////////////////////////////////////////
	const C_XmlNode * C_XmlDocument::SelectNode(const char * nodeNameXPath) const
	{
		//@ parse 1.st part of path
		//////////////////////////////////////////////////////////////////////////
		typedef boost::tokenizer< boost::char_separator<char> > T_Tokenizer;
		boost::char_separator<char> separator("/");
		std::string tmpStr(nodeNameXPath);
		T_Tokenizer tokenizer(tmpStr, separator);

		const C_XmlNode * parentNode = NULL;
		for (T_Tokenizer::iterator iter = tokenizer.begin(); iter != tokenizer.end(); ++iter)
		{
			//@ check root first
			if (iter == tokenizer.begin())
			{
				T_String pathStep(iter->c_str());
				if (m_root->GetName() != pathStep)
				{
					TRACE_FE("Root difier from XPath! Root: %s  AND  XPath: %s", m_root->GetName().c_str(), nodeNameXPath);
					return NULL;
				}
				parentNode = m_root;
			}
			else
			{
				parentNode = parentNode->GetNode(iter->c_str());
				if (!parentNode)
				{
					//@ comment JK 25-01-11 just generic ask fro node which has not to be in xml file
					//@ TRACE_FE("Unknown part of XPath: %s in XPath: %s", iter->c_str(), nodeNameXPath);
					return NULL;
				}
			}
		}

		return parentNode;	//can be NULL
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SelectNodes
	//
	//////////////////////////////////////////////////////////////////////////
	const T_XmlNodeList C_XmlDocument::SelectNodes(const T_String & XPath) const
	{
		SC_ASSERT(false);
		//@ JK_TODO - port
		//u32 nIndex = XPath.FindLast('/');
		//T_String tmp = XPath.Left(nIndex - 1);

		//const C_XmlNode * parent = this->SelectNode(tmp);
		//if (!parent)
		//	return T_XmlNodeList(new std::vector<const C_XmlNode *>());;

		//T_String strNodeName = XPath.Right(nIndex + 1);
		//return parent->SelectNodes(strNodeName);
		return T_XmlNodeList();
	}

}}