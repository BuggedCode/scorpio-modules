/*
* filename:			C_XmlDocument.h
*
* author:			Kral Jozef
* date:				01/11/2011		14:39
* version:			1.00
* brief:				HighLevelAPI for Xml
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include "C_XmlFile.h"
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_XmlDocument
	class SYSUTILS_API C_XmlDocument
	{
	public:
		//@ c-tor
		C_XmlDocument() {};
		//@ d-tor
		~C_XmlDocument() {};

		//@ Laod
		bool				Load(const T_Stream stream);

		//@ SelectNode
		//@ nodeNameXPath - not exactly xpath
		const C_XmlNode	*	SelectNode(const char * nodeNameXPath) const;
		//@ SeletNodes
		const T_XmlNodeList	SelectNodes(const T_String & XPath) const;

	private:
		C_XmlFile			m_file;
		const C_XmlNode * m_root;
	};

}}