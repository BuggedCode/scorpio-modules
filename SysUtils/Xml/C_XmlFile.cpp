/*
* filename:			C_XmlFile.cpp
*
* author:			Kral Jozef
* date:				04/21/2009		22:28
* version:			1.00
* brief:
*/

#include "C_XmlFile.h"
#include <SysUtils/C_TraceClient.h>
#include "C_XmlConvertorUtils.h"
//#include <SysMemManager/globalNewDelete.h>
#include <Common/macros.h>

#pragma warning(push)
#pragma warning(disable:4996)		//-D_SCL_SECURE_NO_WARNINGS
//@ coz using of m_file.readsome(buffer, BUFFER_SIZE); which give warnings in MSVC 2005 but secure version is not in MSVC 2010:( //portability

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_XmlFile::C_XmlFile()
		: m_parser(NULL)
	{
		m_root = NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	C_XmlFile::~C_XmlFile()
	{
		SC_SAFE_DELETE(m_root);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ InitParser
	bool C_XmlFile::InitParser()
	{
		m_parser = XML_ParserCreate(NULL);
		if (!m_parser)
		{
			TRACE_E("Couldn't create XML_Parser");
			return false;
		}

		XML_SetUnknownEncodingHandler(m_parser, EncodingHandler, NULL);
		XML_SetUserData(m_parser,(void*)this);
		XML_SetElementHandler(m_parser, StartElementHandler, EndElementHandler);
		XML_SetCharacterDataHandler(m_parser, CharacterDataHandler);

		SC_ASSERT(m_currentNodeStack.empty());

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ DoneParser
	void C_XmlFile::DoneParser()
	{
		XML_ParserFree(m_parser);
		m_parser = NULL;
		if (m_file.is_open())
			m_file.close();
	}

	//////////////////////////////////////////////////////////////////////////
	bool C_XmlFile::LoadXml(const char * fileName)
	{
		m_fileName = fileName;
		SC_ASSERT(!m_parser);

		m_file.open(fileName, std::ios_base::in);
		if (!m_file)
		{
			TRACE_FE("Couldn't open xml-file %s", fileName);
			return false;
		}

		bool bRes = InitParser();
		if (!bRes)
			return false;


		const u32 BUFFER_SIZE = 1024*256;
		//char buffer[BUFFER_SIZE];
		bool done = false;
		char * buffer = (char*)XML_GetBuffer(m_parser, BUFFER_SIZE);

		while(!done)
		{
			//m_file._Read_s(buffer, BUFFER_SIZE, BUFFER_SIZE);
			m_file.read(buffer, BUFFER_SIZE);
			u32 count = (u32)m_file.gcount();
			done = count < BUFFER_SIZE;

			u32 res = XML_ParseBuffer(m_parser, count, done);
			if (res == XML_STATUS_ERROR)
			{
				TRACE_FE("Error in file '%s': '%s' at line %d", fileName, 
					XML_ErrorString(XML_GetErrorCode(m_parser)), 
					XML_GetCurrentLineNumber(m_parser));
				
				DoneParser();
				return false;
			}
		}

		DoneParser();
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Load
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_XmlFile::Load(const sysutils::T_Stream inputStream)
	{
		if (!inputStream)
			return false;

		m_fileName = inputStream->GetFileName();
		SC_ASSERT(!m_parser);

		bool bRes = InitParser();
		if (!bRes)
			return false;


		const u32 BUFFER_SIZE = 1024*256;
		//char buffer[BUFFER_SIZE];
		bool done = false;
		unsigned char * buffer = (unsigned char*)XML_GetBuffer(m_parser, BUFFER_SIZE);

		while(!done)
		{
			u32 count = inputStream->Read(buffer, BUFFER_SIZE);
			done = count < BUFFER_SIZE;

			u32 res = XML_ParseBuffer(m_parser, count, done);
			if (res == XML_STATUS_ERROR)
			{
				TRACE_FE("Error in file '%s': '%s' at line %d", m_fileName.c_str(), 
					XML_ErrorString(XML_GetErrorCode(m_parser)), 
					XML_GetCurrentLineNumber(m_parser));

				DoneParser();
				return false;
			}
		}

		DoneParser();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ EncodingHandler
	s32 C_XmlFile::EncodingHandler(void *encodingHandlerData, const XML_Char *name, XML_Encoding *info)
	{
		//if (_stricmp(name, "windows-1250") != 0)
		TRACE_FE("Unknow encoding of xml file '%s'", name);
		return XML_STATUS_ERROR;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ OnStartElement
	void C_XmlFile::OnStartElement(const XML_Char *name, const XML_Char **atts)
	{
		C_XmlNode * newNode = new C_XmlNode(name);
		E_XmlDataType dataType = E_XDT_STRING;

		//proceed attributes
		const XML_Char ** attPtr = atts;
		while (*attPtr)
		{
			//test ci sa jedna o attribut udavajuci typ!
			if (strcmp("__type", *attPtr) == 0)
			{
				dataType = C_XmlConvertorUtils::ToTypeFromString(*++attPtr);
				if (dataType == E_XDT_UNKNOWN)
					TRACE_FE("Unknown attribute type in xml node: '%s', file: '%s'", name, m_fileName.c_str())

				attPtr++;
			}
			else
			{
				T_String attName(*attPtr++);
				T_String attVal(*attPtr++);
				newNode->AddAttribute(attName.c_str(), attVal.c_str());	//Atts can have only string value
			}
		}

		newNode->SetDataType(dataType);

		if (!m_root)
		{
			m_root = newNode;
		}
		else
		{
			C_XmlNode * currNode = m_currentNodeStack.top();
			SC_ASSERT(currNode);
			currNode->AddChild(newNode);
		}

		m_currentNodeStack.push(newNode);
		m_currentDataStack.push(T_String());	//vlozim prazdny string
		return;
	}
	//////////////////////////////////////////////////////////////////////////
	//@ StartElementHandler
	void C_XmlFile::StartElementHandler(void *userData, const XML_Char *name, const XML_Char **atts)
	{
		C_XmlFile * _this = reinterpret_cast<C_XmlFile*>(userData);
		SC_ASSERT(_this);
		_this->OnStartElement(name, atts);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ OnEndElement
	void C_XmlFile::OnEndElement(const XML_Char *name)
	{
		//vyhod current parenta
		C_XmlNode * xmlNode = m_currentNodeStack.top();
		SC_ASSERT(xmlNode);

		T_String strTop = m_currentDataStack.top();
		m_currentDataStack.pop();

		u32 len = strTop.length();
		const char * buffer = strTop.c_str();

		while (len && (*(buffer+len-1) == ' ' || *(buffer+len-1) == '\t' || *(buffer+len-1) == '\n'))
			len--;

		if (len)
		{
			char * tmpBuff = new char[len+1];
			STR_CPY(tmpBuff, buffer, len+1);
			xmlNode->SetValueFromString(tmpBuff);	//vlozi hodnotu a prekonvertuje podla typu nody
			delete[]  tmpBuff;
		}

		m_currentNodeStack.pop();

		return;
	}
	//////////////////////////////////////////////////////////////////////////
	//@ EndElementHandler
	void C_XmlFile::EndElementHandler(void *userData, const XML_Char *name)
	{
		C_XmlFile * _this = reinterpret_cast<C_XmlFile*>(userData);
		SC_ASSERT(_this);
		_this->OnEndElement(name);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ OnCharacterData
	void C_XmlFile::OnCharacterData(const XML_Char *s, s32 len)
	{
		static const u32 TMP_CHAR_BUFFER_SIZE = 1024;
		char buffer[TMP_CHAR_BUFFER_SIZE];
      MEM_CPY(buffer, TMP_CHAR_BUFFER_SIZE, s, len);
		buffer[len] = 0;	//termination;


		T_String & strTop = m_currentDataStack.top();
		strTop += buffer;
		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ CharacterDataHandler
	void C_XmlFile::CharacterDataHandler(void *userData, const XML_Char *s, s32 len)
	{
		C_XmlFile * _this = reinterpret_cast<C_XmlFile*>(userData);
		SC_ASSERT(_this);
		_this->OnCharacterData(s, len);
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateRoot
	//
	//////////////////////////////////////////////////////////////////////////
	C_XmlNode * C_XmlFile::CreateRoot(const char * rootName)
	{
		SC_ASSERT(m_root == NULL);
		m_root = new C_XmlNode(rootName);
		return m_root;
		return NULL;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SaveXml
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_XmlFile::SaveXml(const char * fileName)
	{
		SC_ASSERT(m_root);
		if (m_root == NULL)
			return false;

		std::ofstream stream;
		stream.open(fileName, std::ios::out);
		if (!stream.is_open())
		{
			TRACE_FE("Can not open file %s for write", fileName);
			return false;
		}

		stream << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";

		T_String prefix = "";
		bool bRes = m_root->SaveNode(stream, prefix);

		stream.close();
		return bRes;
	}

}}

#pragma warning(pop)