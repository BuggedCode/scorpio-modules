/*
* filename:			C_XmlNode.cpp
*
* author:			Kral Jozef
* date:				04/23/2009		21:07
* version:			1.00
* brief:				XmlNode
*/

#include "C_XmlNode.h"
#include "C_XmlFile.h"
#include "C_XmlConvertorUtils.h"
//#include <SysMemManager/globalNewDelete.h>
#include <Common/macros.h>
#include <SysUtils/Utils/C_StringUtils.h>

namespace scorpio{ namespace sysutils{

	using namespace scorpio::sysmath;

	static const char * C_TAG_PREFIX = "  ";

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_XmlNode::C_XmlNode(const char * nodeName)
		: m_nodeName(nodeName), m_dataType(E_XDT_UNKNOWN), m_bValid(true)
	{
		ZERO_ARRAY(m_dataBuffer);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ AddAttribute
	void C_XmlNode::AddAttribute(const char * attName, const char * strVal)
	{
		m_attributes.push_back(C_XmlAttribute(attName, strVal));
	}

	//////////////////////////////////////////////////////////////////////////
	//@ AddChild
	void C_XmlNode::AddChild(C_XmlNode * child)
	{
		SC_ASSERT(child);
		m_children.push_back(T_XmlNode(child));
	}

	//////////////////////////////////////////////////////////////////////////
	//@ GetNode
	const C_XmlNode * C_XmlNode::GetNode(const char * nodeName) const
	{
		if (!m_bValid)
			return NULL;

		for (T_ChildrenContainer::const_iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			T_XmlNode nodeChild = *iter;
			SC_ASSERT(nodeChild);
			if (nodeChild->GetName() == nodeName)
				return nodeChild.get();
		}

		return NULL;	//nenasli sme nodu s pozadovanym menom
	}

	//////////////////////////////////////////////////////////////////////////
	//@ GetAttribute
	const C_XmlAttribute * C_XmlNode::GetAttribute(const char * attrName) const
	{
		for (T_AttributeContainer::const_iterator iter = m_attributes.begin(); iter != m_attributes.end(); ++iter)
		{
			if (iter->GetName() == attrName)
				return &(*iter);
		}
		return NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ operator[]
	//
	//////////////////////////////////////////////////////////////////////////
	const T_String & C_XmlNode::operator [](const char * attrName) const
	{
		const C_XmlAttribute * att = this->GetAttribute(attrName);
		if (!att)
		{
			return C_StringUtils::C_EMPTY_STD_STRING;
		}

		return att->GetValue();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetValueFromString
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_XmlNode::SetValueFromString(const char * strVal)
	{
		switch (m_dataType)
		{
		case E_XDT_BOOL:
			m_bValid = C_XmlConvertorUtils::BoolFromString(strVal, *(bool*)m_dataBuffer);
			break;

		case E_XDT_STRING:
		case E_XDT_UNKNOWN:
			m_stringData = strVal;
			m_bValid = true;
			break;

		case E_XDT_INT:
			m_bValid = C_XmlConvertorUtils::IntFromString(strVal, *(s32*)m_dataBuffer);
			break;

		case E_XDT_SHORT:
			m_bValid = C_XmlConvertorUtils::ShortFromString(strVal, *(s16*)m_dataBuffer);
			break;
			
		case E_XDT_FLOAT:
			m_bValid = C_XmlConvertorUtils::FloatFromString(strVal, *(float*)m_dataBuffer);
			break;

		case E_XDT_VECTOR2:
			m_bValid = C_XmlConvertorUtils::Vector2FromString(strVal, *(scmath::C_Vector2*)m_dataBuffer);
			break;

		case E_XDT_VECTOR3:
			m_bValid = C_XmlConvertorUtils::Vector3FromString(strVal, *(scmath::C_Vector3*)m_dataBuffer);
			break;
			
		case E_XDT_VECTOR4:
			m_bValid = C_XmlConvertorUtils::Vector4FromString(strVal, *(scmath::C_Vector4*)m_dataBuffer);
			break;

		default:
			SC_ASSERT(false);
			m_bValid = false;
		};	//switch

		return m_bValid;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetValueToString
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_XmlNode::SetValueToString(T_String & outString) const
	{
		switch (m_dataType)
		{
		case E_XDT_BOOL:
			m_bValid = C_XmlConvertorUtils::NativeToString<bool>(outString, *(bool*)m_dataBuffer);
			break;

		case E_XDT_STRING:
		case E_XDT_UNKNOWN:
			outString = m_stringData;
			m_bValid = true;
			break;

		case E_XDT_INT:
			m_bValid = C_XmlConvertorUtils::NativeToString<s32>(outString, *(s32*)m_dataBuffer);
			break;

		case E_XDT_SHORT:
			m_bValid = C_XmlConvertorUtils::NativeToString<s16>(outString, *(s16*)m_dataBuffer);
			break;

		case E_XDT_FLOAT:
			m_bValid = C_XmlConvertorUtils::NativeToString<float>(outString, *(float*)m_dataBuffer);
			break;

		case E_XDT_VECTOR2:
			m_bValid = C_XmlConvertorUtils::Vector2ToString(outString, *(scmath::C_Vector2*)m_dataBuffer);
			break;

		case E_XDT_VECTOR3:
			m_bValid = C_XmlConvertorUtils::Vector3ToString(outString, *(scmath::C_Vector3*)m_dataBuffer);
			break;

		case E_XDT_VECTOR4:
			m_bValid = C_XmlConvertorUtils::Vector4ToString(outString, *(scmath::C_Vector4*)m_dataBuffer);
			break;

		default:
			SC_ASSERT(false);
			m_bValid = false;
		};	//switch

		return m_bValid;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Read
	//
	//////////////////////////////////////////////////////////////////////////

#define READ_VALUE_IMPL(nativeType, enumType)	\
	bool C_XmlNode::Read(nativeType & outValue) const	\
	{	\
		if (!m_bValid)	\
		{	\
			TRACE_FE("Attemp to read from error node: %s", m_nodeName.c_str());	\
			return false;	\
		}	\
	\
		if (m_dataType != enumType)	\
		{	\
			TRACE_FE("Attemp to read %s value form %s node: %s", C_XmlConvertorUtils::ToStringFromType(enumType).c_str(), C_XmlConvertorUtils::ToStringFromType(m_dataType).c_str(), m_nodeName.c_str());	\
			return false;	\
		}	\
	\
		outValue = *((nativeType*)m_dataBuffer);	\
		return true;	\
	}	\
	

READ_VALUE_IMPL(bool, E_XDT_BOOL);
READ_VALUE_IMPL(u32, E_XDT_INT);
READ_VALUE_IMPL(s32, E_XDT_INT);
READ_VALUE_IMPL(u16, E_XDT_SHORT);
READ_VALUE_IMPL(s16, E_XDT_SHORT);
READ_VALUE_IMPL(float, E_XDT_FLOAT);
READ_VALUE_IMPL(C_Vector2, E_XDT_VECTOR2);
READ_VALUE_IMPL(C_Vector, E_XDT_VECTOR2);
READ_VALUE_IMPL(C_Vector4, E_XDT_VECTOR4);

	bool C_XmlNode::Read(T_String & outValue) const
	{
		if (!m_bValid)
		{
			TRACE_FE("Attemp to read from error node: %s", m_nodeName.c_str());
			return false;
		}

		if (m_dataType != E_XDT_STRING)
		{
			TRACE_FE("Attemp to read %s value form %s node: %s", C_XmlConvertorUtils::ToStringFromType(E_XDT_STRING).c_str(), C_XmlConvertorUtils::ToStringFromType(m_dataType).c_str(), m_nodeName.c_str());
			return false;
		}

		outValue = m_stringData;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Write
	//
	//////////////////////////////////////////////////////////////////////////

#define WRITE_VALUE_IMPL(nativeType, enumType)	\
	bool C_XmlNode::Write(const nativeType & inValue)	\
	{	\
		if (!m_bValid)	\
		{	\
			TRACE_FE("Attemp to write into error node: %s", m_nodeName.c_str());	\
			return false;	\
		}	\
	\
		if (m_dataType != enumType && m_dataType != E_XDT_UNKNOWN)	\
		{	\
			TRACE_FE("Attemp to write %s value into %s node: %s", C_XmlConvertorUtils::ToStringFromType(enumType).c_str(), C_XmlConvertorUtils::ToStringFromType(m_dataType).c_str(), m_nodeName.c_str());	\
			return false;	\
		}	\
	\
		(*(nativeType*)m_dataBuffer) = inValue;	\
	\
		m_dataType = enumType;	\
		return true;	\
	}	\


WRITE_VALUE_IMPL(bool, E_XDT_BOOL);
WRITE_VALUE_IMPL(u32, E_XDT_INT);
WRITE_VALUE_IMPL(s32, E_XDT_INT);
WRITE_VALUE_IMPL(u16, E_XDT_SHORT);
WRITE_VALUE_IMPL(s16, E_XDT_SHORT);
WRITE_VALUE_IMPL(float, E_XDT_FLOAT);
WRITE_VALUE_IMPL(C_Vector2, E_XDT_VECTOR2);
WRITE_VALUE_IMPL(C_Vector, E_XDT_VECTOR2);
WRITE_VALUE_IMPL(C_Vector4, E_XDT_VECTOR4);

	bool C_XmlNode::Write(const T_String & inValue)
	{
		if (!m_bValid)
		{
			TRACE_FE("Attemp to write into error node: %s", m_nodeName.c_str());
			return false;
		}

		if (m_dataType != E_XDT_STRING && m_dataType != E_XDT_UNKNOWN)
		{
			TRACE_FE("Attemp to write %s value into %s node: %s", C_XmlConvertorUtils::ToStringFromType(E_XDT_STRING).c_str(), C_XmlConvertorUtils::ToStringFromType(m_dataType).c_str(), m_nodeName.c_str());
			return false;
		}

		m_dataType = E_XDT_STRING;
		m_stringData = inValue;
		return true;
	}



	//////////////////////////////////////////////////////////////////////////
	const C_XmlNode::C_XmlNodeChildConstIterator C_XmlNode::BeginChild() const
	{
		C_XmlNodeChildConstIterator	beginIter(this);
		return beginIter;
	}

	const C_XmlNode::C_XmlNodeChildConstIterator C_XmlNode::BeginChild(const char * constraint) const
	{
		C_XmlNodeChildConstIterator	beginIter(this, constraint);
		return beginIter;
	}

	const C_XmlNode::C_XmlNodeChildConstIterator C_XmlNode::EndChild() const
	{
		C_XmlNodeChildConstIterator	endIter(this);
		endIter.m_iter = m_children.end();
		return endIter;
	}

	const C_XmlNode::C_XmlNodeChildConstIterator C_XmlNode::EndChild(const char * constraint) const
	{
		C_XmlNodeChildConstIterator	endIter(this, constraint);
		endIter.m_iter = m_children.end();
		return endIter;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ C_XmlNodeChildConstIterator
	//
	//////////////////////////////////////////////////////////////////////////
	C_XmlNode::C_XmlNodeChildConstIterator::C_XmlNodeChildConstIterator(const C_XmlNode *parentNode)
	: m_parentNode(parentNode), m_constraint(NULL)
	{
		SC_ASSERT(m_parentNode);
		m_iter = m_parentNode->m_children.begin();
	}

	C_XmlNode::C_XmlNodeChildConstIterator::C_XmlNodeChildConstIterator(const C_XmlNode *parentNode, const char *constraint)
		: m_parentNode(parentNode), m_constraint(constraint)
	{
		SC_ASSERT(m_parentNode && m_constraint);
		m_iter = m_parentNode->m_children.begin();
		
		if (m_iter != m_parentNode->m_children.end())
			if ( (*m_iter)->GetName() != m_constraint)
			{
				++(*this);	//move m_iter to correct place
			}
	}

	//////////////////////////////////////////////////////////////////////////
	//@ prefix plus operator
	C_XmlNode::C_XmlNodeChildConstIterator & C_XmlNode::C_XmlNodeChildConstIterator::operator ++()
	{
		++m_iter;

		if (m_constraint && m_iter != m_parentNode->m_children.end())
		{
			//tu uz moze byt iterator neplatny takze podmienka na end musi byt skor ako dotaz na iterator hodnotu
			while (m_iter != m_parentNode->m_children.end() && (*m_iter)->GetName() != m_constraint)
				++m_iter;
		}

		return *this;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ postfix plus operator
	C_XmlNode::C_XmlNodeChildConstIterator C_XmlNode::C_XmlNodeChildConstIterator::operator ++(int)
	{
		C_XmlNode::C_XmlNodeChildConstIterator	tmpIter = *this;

		++(*this);
		return tmpIter;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ class member acces operator overloading
	const C_XmlNode * C_XmlNode::C_XmlNodeChildConstIterator::operator->() const
	{
		return (*m_iter).get();
	}

	//////////////////////////////////////////////////////////////////////////
	//@ dereferencation operator overloading
	const C_XmlNode & C_XmlNode::C_XmlNodeChildConstIterator::operator*() const
	{
		SC_ASSERT(*m_iter);	//noda je NULL

		return (*(*m_iter).get());
	}

	//////////////////////////////////////////////////////////////////////////
	bool C_XmlNode::C_XmlNodeChildConstIterator::operator==(const C_XmlNodeChildConstIterator & right) const
	{
		SC_ASSERT((!m_constraint && !right.m_constraint) || (m_constraint && right.m_constraint && (strcmp(m_constraint, right.m_constraint) == 0)));
		return m_iter == right.m_iter;

	}
	//////////////////////////////////////////////////////////////////////////
	bool C_XmlNode::C_XmlNodeChildConstIterator::operator!=(const C_XmlNodeChildConstIterator & right) const
	{
		return (!(*this == right));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateNode
	//
	//////////////////////////////////////////////////////////////////////////
	C_XmlNode * C_XmlNode::CreateNode(const char * nodeName)
	{
		C_XmlNode * childNode = new C_XmlNode(nodeName);
		AddChild(childNode);
		return childNode;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SaveNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_XmlNode::SaveNode(std::ofstream & outfstream, const T_String & prefix) const
	{
		outfstream << prefix;
		outfstream << "<" << m_nodeName;

		//@ save atributov only if not set value!
		if (m_dataType != E_XDT_UNKNOWN)
		{
			if (m_attributes.size() || m_children.size())
			{
				SC_ASSERT(false);	//atribute shouldn't be set in value node!	//children shouldn't be set in value node!
				TRACE_FE("Attribute/Children should't be set in value node: %s", m_nodeName.c_str());
			}
			
			outfstream << " __type=\"";
			outfstream << C_XmlConvertorUtils::ToStringFromType(m_dataType);
			outfstream << "\"" << ">";
			
			T_String strVal;
			bool bRes = SetValueToString(strVal);
			if (!bRes || !m_bValid)
			{
				TRACE_FE("Write of value for node %s FAILED", m_nodeName.c_str());
				outfstream << "???"; 
			}
			else
			{
				outfstream << strVal.c_str(); 
			}
			outfstream << "</" << m_nodeName << ">\n";
			return true;
		}
		else
		{
			for (T_AttributeContainer::const_iterator iter = m_attributes.begin(); iter != m_attributes.end(); ++iter)
			{
				outfstream << " " << iter->GetName() << "=\"" << iter->GetValue() << "\"";
			}
		}

		if (m_children.size())
		{
			outfstream << ">\n";

			T_String newPrefix = prefix;
			newPrefix += C_TAG_PREFIX;
			for (T_ChildrenContainer::const_iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
			{
				T_XmlNode child = (*iter);
				child->SaveNode(outfstream, newPrefix);
			}

			outfstream << prefix;
			outfstream << "</" << m_nodeName << ">" << "\n";
		}
		else
		{
			outfstream << "/>" << "\n";
		}
		

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SelectNodes
	//
	//////////////////////////////////////////////////////////////////////////
	const T_XmlNodeList C_XmlNode::SelectNodes(const T_String & XPath) const
	{
		T_XmlNodeList nodeList = T_XmlNodeList(new std::vector<const C_XmlNode *>());

		//@ parse 1.st part of path
		//////////////////////////////////////////////////////////////////////////
		typedef boost::tokenizer< boost::char_separator<char> > T_Tokenizer;
		boost::char_separator<char> separator("/");
		std::string tmpStr(XPath);
		T_Tokenizer tokenizer(tmpStr, separator);

		const C_XmlNode * parentNode = this;
		T_String lastName;
		for (T_Tokenizer::iterator iter = tokenizer.begin(); iter != tokenizer.end(); ++iter)
		{
			//@ last - 1 "semifinal" valid iterator - exit loop after that get from that node all children
			T_Tokenizer::iterator tmpIter = iter;
			if (++tmpIter == tokenizer.end())
			{
				lastName = iter->c_str();
				break;
			}

			parentNode = parentNode->GetNode(iter->c_str());
			if (!parentNode)
			{
				//TRACE_FE("Unknown part of XPath: %s in path: %s", iter->c_str(), XPath);
				return nodeList;
			}
		}


		C_XmlNode::const_child_iterator it_begin = parentNode->BeginChild(lastName.c_str());
		C_XmlNode::const_child_iterator it_end	= parentNode->EndChild(lastName.c_str());
		for (; it_begin != it_end; ++it_begin)
		{
			const C_XmlNode & nodeRef = *it_begin;
			nodeList->push_back(&nodeRef);
		}

		return nodeList;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SelectNode
	//
	//////////////////////////////////////////////////////////////////////////
	const C_XmlNode * C_XmlNode::SelectNode(const T_String & XPath) const
	{
		//@ parse 1.st part of path
		//////////////////////////////////////////////////////////////////////////
		typedef boost::tokenizer< boost::char_separator<char> > T_Tokenizer;
		boost::char_separator<char> separator("/");
		std::string tmpStr(XPath);
		T_Tokenizer tokenizer(tmpStr, separator);

		const C_XmlNode * parentNode = this;
		T_String lastName;
		for (T_Tokenizer::iterator iter = tokenizer.begin(); iter != tokenizer.end(); ++iter)
		{
			parentNode = parentNode->GetNode(iter->c_str());
			if (!parentNode)
			{
				//TRACE_FE("Unknown part of XPath: %s in path: %s", iter->c_str(), XPath);
				return NULL;
			}
		}

		return parentNode;
	}





}}
