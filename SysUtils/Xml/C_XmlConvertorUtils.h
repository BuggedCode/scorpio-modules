/*
* filename:			C_XmlConvertorUtils.h
*
* author:			Kral Jozef
* date:				04/27/2009		21:02
* version:			1.00
* brief:				static class for xml types convertor
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include "XmlTypes.h"
#include <SysUtils/C_TraceClient.h>

//#include <sstream>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <boost/token_functions.hpp>

#include <SysMath/C_Vector2.h>
#include <SysMath/C_Vector.h>
#include <SysMath/C_Vector4.h>

namespace scorpio{ namespace sysutils{

	namespace scmath = scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@
	class SYSUTILS_API C_XmlConvertorUtils
	{
	public:
		static E_XmlDataType		ToTypeFromString(const char * typeName);
		static const T_String		ToStringFromType(E_XmlDataType type);

		//@ ret value - true if succed
		static bool					BoolFromString(const char * strValue, bool & outValue);
		static bool					IntFromString(const char * strValue, s32 & outValue);
		static bool					HexFromString(const char * strValue, u32 & outValue);
		static bool					ShortFromString(const char * strValue, s16 & outValue);
		static bool					FloatFromString(const char * strValue, float & outValue);
		static bool					DoubleFromString(const char * strValue, double & outValue);
		static bool					Vector2FromString(const char * strValue, scmath::C_Vector2 & outValue);
		static bool					Vector3FromString(const char * strValue, scmath::C_Vector & outValue);
		static bool					Vector4FromString(const char * strValue, scmath::C_Vector4 & outValue);

		template<typename T>
		static bool					NativeToString(T_String & outStrVal, T & val)
		{
			try
			{
				std::string outStrVal_ = boost::lexical_cast<std::string>(val);
				outStrVal = outStrVal_.c_str();
			}
			catch (boost::bad_lexical_cast & lex)
			{
				TRACE_E(lex.what());
				return false;
			}

			return true;
		}

		static bool					Vector2ToString(T_String & outStrVal, scmath::C_Vector2 & val);
		static bool					Vector3ToString(T_String & outStrVal, scmath::C_Vector & val);
		static bool					Vector4ToString(T_String & outStrVal, scmath::C_Vector4 & val);
	};



	//////////////////////////////////////////////////////////////////////////
	//@ ToTypeFromString
	inline E_XmlDataType C_XmlConvertorUtils::ToTypeFromString(const char * typeName)
	{
		if (strcmp(typeName, "Bool") == 0 ) return E_XDT_BOOL;
		if (strcmp(typeName, "String") == 0 ) return E_XDT_STRING;
		if (strcmp(typeName, "Int") == 0 ) return E_XDT_INT;
		if (strcmp(typeName, "Short") == 0 ) return E_XDT_SHORT;
		if (strcmp(typeName, "Float") == 0 ) return E_XDT_FLOAT;
		if (strcmp(typeName, "Vector2") == 0 ) return E_XDT_VECTOR2;
		if (strcmp(typeName, "Vector3") == 0 ) return E_XDT_VECTOR3;
		if (strcmp(typeName, "Vector4") == 0 ) return E_XDT_VECTOR4;
		return E_XDT_UNKNOWN;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ ToStringFromType
	inline const T_String C_XmlConvertorUtils::ToStringFromType(E_XmlDataType type)
	{
		switch(type)
		{
		case E_XDT_UNKNOWN:
			return "Unknown";
			break;
		case E_XDT_BOOL:
			return "Bool";
			break;
		case E_XDT_STRING:
			return "String";
			break;
		case E_XDT_INT:
			return "Int";
			break;
		case E_XDT_SHORT:
			return "Short";
			break;
		case E_XDT_FLOAT:
			return "Float";
			break;
		case E_XDT_VECTOR2:
			return "Vector2";
			break;
		case E_XDT_VECTOR3:
			return "Vector3";
			break;
		case E_XDT_VECTOR4:
			return "Vector4";
			break;
		default:
			SC_ASSERT(false);
		};	//swotch

		return "Undefined";
	}



	/*std::istringstream istrStream;
	istrStream.str(value);
	bool bVal;
	istrStream >> bVal;
	if (istrStream.fail())*/

	//////////////////////////////////////////////////////////////////////////
	//
	//@ BoolFromString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::BoolFromString(const char * strValue, bool & outValue)
	{
		try
		{
			//@ boost::lexical_cast<bool> - work just with ASCII code representation of bool states "1"/"0" instead locale DEPENDENT names
			//@ http://lists.boost.org/Archives/boost/2002/05/30074.php
			//Using static stringstream object we can locale dependent names "True"/"False"/Others...
			outValue = boost::lexical_cast<bool>(strValue);
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ IntFromString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::IntFromString(const char * strValue, s32 & outValue)
	{
		try
		{
			outValue = boost::lexical_cast<s32>(strValue);
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ HexFromString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::HexFromString(const char * strValue, u32 & outValue)
	{
		char * p;
		long n = strtoul( strValue, & p, 16 );
		if ( * p != 0 ) 
		{
			TRACE_FE("Parsing error %s", strValue);
			return false;
		}
		else 
		{
			outValue = (u32)n;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ShortFromString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::ShortFromString(const char * strValue, short & outValue)
	{
		try
		{
			outValue = boost::lexical_cast<short>(strValue);
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ FloatFromString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::FloatFromString(const char * strValue, float & outValue)
	{
		try
		{
			outValue = boost::lexical_cast<float>(strValue);
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DoubleFromString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::DoubleFromString(const char * strValue, double & outValue)
	{
		try
		{
			outValue = boost::lexical_cast<double>(strValue);
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Vector2FromString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::Vector2FromString(const char * strValue, scmath::C_Vector2 & outValue)
	{
		try
		{
			typedef boost::tokenizer<boost::char_separator<char> > T_Tokenizer;

			std::string str = strValue;
			boost::char_separator<char> separator(", ");
			T_Tokenizer tok(str, separator);

			T_Tokenizer::iterator iter = tok.begin();
			if (iter == tok.end())
				return false;

			outValue.m_x = boost::lexical_cast<float>(*iter);
			++iter;

			if (iter == tok.end())
				return false;

			outValue.m_y = boost::lexical_cast<float>(*iter);
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Vector3FromString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::Vector3FromString(const char * strValue, scmath::C_Vector & outValue)
	{
		try
		{
			typedef boost::tokenizer<boost::char_separator<char> > T_Tokenizer;

			std::string str = strValue;
			boost::char_separator<char> separator(", ");
			T_Tokenizer tok(str, separator);

			T_Tokenizer::iterator iter = tok.begin();
			if (iter == tok.end())
				return false;

			outValue.m_x = boost::lexical_cast<float>(*iter);
			++iter;

			if (iter == tok.end())
				return false;

			outValue.m_y = boost::lexical_cast<float>(*iter);
			++iter;

			if (iter == tok.end())
				return false;

			outValue.m_z = boost::lexical_cast<float>(*iter);
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Vector4FromString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::Vector4FromString(const char * strValue, scmath::C_Vector4 & outValue)
	{
		try
		{
			typedef boost::tokenizer<boost::char_separator<char> > T_Tokenizer;

			std::string str = strValue;
			boost::char_separator<char> separator(", ");
			T_Tokenizer tok(str, separator);

			T_Tokenizer::iterator iter = tok.begin();
			if (iter == tok.end())
				return false;

			outValue.m_x = boost::lexical_cast<float>(*iter);
			++iter;

			if (iter == tok.end())
				return false;

			outValue.m_y = boost::lexical_cast<float>(*iter);
			++iter;

			if (iter == tok.end())
				return false;

			outValue.m_z = boost::lexical_cast<float>(*iter);
			++iter;

			if (iter == tok.end())
				return false;

			outValue.m_w = boost::lexical_cast<float>(*iter);
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Vector2ToString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::Vector2ToString(T_String & outStrVal, scmath::C_Vector2 & val)
	{
		try
		{
			std::string xVal = boost::lexical_cast<std::string>(val.m_x);
			std::string yVal = boost::lexical_cast<std::string>(val.m_y);
			outStrVal = xVal.c_str();
			outStrVal += ", ";
			outStrVal += yVal.c_str();
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Vector3ToString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::Vector3ToString(T_String & outStrVal, scmath::C_Vector & val)
	{
		try
		{
			std::string xVal = boost::lexical_cast<std::string>(val.m_x);
			std::string yVal = boost::lexical_cast<std::string>(val.m_y);
			std::string zVal = boost::lexical_cast<std::string>(val.m_z);
			outStrVal = xVal.c_str();
			outStrVal += ", ";
			outStrVal += yVal.c_str();
			outStrVal += ", ";
			outStrVal += zVal.c_str();
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Vector4ToString
	//
	//////////////////////////////////////////////////////////////////////////
	inline bool C_XmlConvertorUtils::Vector4ToString(T_String & outStrVal, scmath::C_Vector4 & val)
	{
		try
		{
			std::string xVal = boost::lexical_cast<std::string>(val.m_x);
			std::string yVal = boost::lexical_cast<std::string>(val.m_y);
			std::string zVal = boost::lexical_cast<std::string>(val.m_z);
			std::string wVal = boost::lexical_cast<std::string>(val.m_w);
			outStrVal = xVal.c_str();
			outStrVal += ", ";
			outStrVal += yVal.c_str();
			outStrVal += ", ";
			outStrVal += zVal.c_str();
			outStrVal += ", ";
			outStrVal += wVal.c_str();
		}
		catch (boost::bad_lexical_cast & lex)
		{
			TRACE_E(lex.what());
			return false;
		}

		return true;
	}


}}