/*
* filename:			C_XmlNode.h
*
* author:			Kral Jozef
* date:				04/23/2009		21:05
* version:			1.00
* brief:				XmlNode
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/C_String.h>
#include "C_XmlAttribute.h"
#include <vector>
#include "XmlTypes.h"

#include <SysMath/C_Vector2.h>
#include <SysMath/C_Vector.h>
#include <SysMath/C_Vector4.h>

namespace scorpio{ namespace sysutils{

	namespace scmath = scorpio::sysmath;

	//# forward
	class C_XmlFile;
	class C_XmlNode;

	DECLARE_SHARED_PTR(C_XmlNode, T_XmlNode);
	//typedef boost::shared_ptr<const C_XmlNode>	T_ConstXmlNode; - not need now - not implemented here in holding nodes through smart ptrs
	//typedef boost::shared_ptr< std::vector<T_XmlNode> >	T_XmlNodeList; CAN NOT USED - need rewrite iterator for using Shared_ptr :-(
	typedef std::vector<const C_XmlNode*>	T_ConstXmlNodePtrVector;
	DECLARE_SHARED_PTR(std::vector<const C_XmlNode *>, T_XmlNodeList);



	//////////////////////////////////////////////////////////////////////////
	//@ C_XmlNode
	class SYSUTILS_API C_XmlNode
	{
		friend class C_XmlFile;

		typedef std::vector<C_XmlAttribute>		T_AttributeContainer;
		typedef std::vector<T_XmlNode>			T_ChildrenContainer;

	public:

		//////////////////////////////////////////////////////////////////////////
		//@ C_XmlNodeChildConstIterator
		class SYSUTILS_API C_XmlNodeChildConstIterator
		{
			friend class C_XmlNode;
		public:

			C_XmlNodeChildConstIterator	&	operator++();		//prefix operator
			C_XmlNodeChildConstIterator		operator++(int);	//postfix operator
			const C_XmlNode	*	operator->() const;
			const C_XmlNode	&	operator*() const;
			bool operator==(const C_XmlNodeChildConstIterator & right) const;
			bool operator!=(const C_XmlNodeChildConstIterator & right) const;


		private:
			//@ c-tor
			C_XmlNodeChildConstIterator(const C_XmlNode * parentNode);
			C_XmlNodeChildConstIterator(const C_XmlNode * parentNode, const char * constraint);

			const C_XmlNode	*	m_parentNode;	//xml noda, ktoru preiterovavam
			const char			*	m_constraint;	//meno child nody ktore hledam "constraint"
			C_XmlNode::T_ChildrenContainer::const_iterator	m_iter;	//iterator do kontainera decek m_parentNode-y
		};

		typedef C_XmlNodeChildConstIterator	const_child_iterator;


		//@ c-tor
		C_XmlNode(const char * nodeName);
		//@ d-tor
		virtual ~C_XmlNode() {};
		//@ AddAttribute
		//template<typename T>
		void					AddAttribute(const char * attName, const char * strVal);
		//@ AddChild
		void					AddChild(C_XmlNode * child);
		//@ SetDataType
		void					SetDataType(E_XmlDataType type) { m_dataType = type; }
		//@ Is node valid
		bool					IsValid() const { return m_bValid; }
		//@ GetName
		const T_String		&	GetName() const { return m_nodeName; }
		//@ GetNode
		const C_XmlNode		*	GetNode(const char * nodeName) const;
		//@ GetAttribute
		const C_XmlAttribute	*	GetAttribute(const char * attrName) const;

		//@ operator[]
		const T_String		&	operator[](const char * attrName) const;


		//@ CreateNode
		C_XmlNode	*	CreateNode(const char * nodeName);
		//@ Save
		bool				SaveNode(std::ofstream & outfstream, const T_String & prefix) const;

		//@ BeginChild
		const C_XmlNodeChildConstIterator	BeginChild() const;
		const C_XmlNodeChildConstIterator	BeginChild(const char * constraint) const;
		//@ EndChild
		const C_XmlNodeChildConstIterator	EndChild() const;
		const C_XmlNodeChildConstIterator	EndChild(const char * constraint) const;

		//@ Read overloaded
		bool			Read(bool & outVal) const;
		bool			Read(s32 & outVal) const;
		bool			Read(u32 & outVal) const;
		bool			Read(s16 & outVal) const;
		bool			Read(u16 & outVal) const;
		bool			Read(float & outVal) const;
		bool			Read(scmath::C_Vector2 & outVal) const;
		bool			Read(scmath::C_Vector & outVal) const;
		bool			Read(scmath::C_Vector4 & outVal) const;
		bool			Read(T_String & outValue) const;

		//@ Write overloaded
		bool			Write(const bool & inVal);
		bool			Write(const s32 & inVal);
		bool			Write(const u32 & inVal);
		bool			Write(const s16 & inVal);
		bool			Write(const u16 & inVal);
		bool			Write(const float & inVal);
		bool			Write(const scmath::C_Vector2 & inVal);
		bool			Write(const scmath::C_Vector & inVal);
		bool			Write(const scmath::C_Vector4 & inVal);
		bool			Write(const T_String & inValue);

		//@ SelectNodes
		const T_XmlNodeList		SelectNodes(const T_String & XPath) const;
		//@ SelectNode	//can not return T_ConstXmlNode - automatic destruction because not every is holded in sharet_ptr
		const C_XmlNode *			SelectNode(const T_String & XPath) const;

	private:
		//@ SetValueFromString
		bool			SetValueFromString(const char * value);
		//@ SetValueToString
		bool			SetValueToString(T_String & outString) const;

	private:
		T_String			m_nodeName;
		E_XmlDataType		m_dataType;		

		T_AttributeContainer		m_attributes;
		T_ChildrenContainer			m_children;

		unsigned char			m_dataBuffer[16];	//16 byte buffer for 'bool/int/short/float/C_Vector2/C_Vector/C_Vector4'
		T_String				m_stringData;		//tie sa alokuju uplne jinak 'C_String for unknown & string type'
		mutable bool			m_bValid;
	};

}}