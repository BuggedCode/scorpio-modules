/*
* filename:			XmlEnums.h
*
* author:			Kral Jozef
* date:				04/27/2009		21:04
* version:			1.00
* brief:				types in xml
*/

#pragma once

namespace scorpio{ namespace sysutils{

	enum E_XmlDataType
	{
		E_XDT_UNKNOWN = -1,
		E_XDT_BOOL,			//bool type
		E_XDT_STRING,		//string type
		E_XDT_INT,			//32bit integral type
		E_XDT_SHORT,		//16bit integral type
		E_XDT_FLOAT,		//float with 5dec places precision
		E_XDT_VECTOR2,		//2D vector type
		E_XDT_VECTOR3,		//3D vector type
		E_XDT_VECTOR4,		//4D vector type
		// time, quat, guid, matrix
	};


}}