/*
* filename:			C_XmlFile.h
*
* author:			Kral Jozef
* date:				04/21/2009		22:28
* version:			1.00
* brief:				XML file
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <fstream>
#include <Expat/Source/lib/expat.h>
#include "C_XmlNode.h"
#include <stack>
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_XmlFile
	class SYSUTILS_API C_XmlFile
	{
	public:
		//@ c-tor
		C_XmlFile();
		//@ d-tor
		virtual ~C_XmlFile();

		//@ LoadXml
		bool				LoadXml(const char * fileName);
		//@ Load
		bool				Load(const sysutils::T_Stream inputStream);
		//@ SaveXml
		bool				SaveXml(const char * fileName);
		//@ GetRoot
		C_XmlNode	*	GetRoot() { return m_root; }
		//@ GetRoot
		const C_XmlNode	*	GetRoot() const { return m_root; }

		//@ CreateRoot
		C_XmlNode	*	CreateRoot(const char * rootName);
		//@ IsValid
		bool				IsValid() const { return m_root != NULL; }

	private:
		//@ InitParser
		bool				InitParser();
		//@ DoneParser
		void				DoneParser();

		/*XMLCALL*/
		static s32		EncodingHandler(void *encodingHandlerData, const XML_Char *name, XML_Encoding *info);
		static void		StartElementHandler(void *userData, const XML_Char *name, const XML_Char **atts);
		static void		EndElementHandler(void *userData, const XML_Char *name);
		static void		CharacterDataHandler(void *userData, const XML_Char *s, s32 len);

		void				OnStartElement(const XML_Char *name, const XML_Char **atts);
		void				OnEndElement(const XML_Char *name);
		void				OnCharacterData(const XML_Char *s, s32 len);

	private:
		std::ifstream	m_file;
		T_String			m_fileName;
		XML_Parser		m_parser;

		C_XmlNode	*	m_root;
		std::stack<C_XmlNode*>		m_currentNodeStack;		//stack for actual processing node
		std::stack<T_String>		m_currentDataStack;				//stack for character data

	};

}}