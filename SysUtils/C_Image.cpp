/*
* filename:			C_Image.cpp
*
* author:			Kral Jozef
* date:				Jun 2003
* version:			1.00
* brief:				C_Image class
*/

/*#include "C_Image.h"
#include <stdlib.h>

namespace scorpio{ namespace sysutils{

	//////////////////// Implementacia triedy C_Image ////////////////////
	C_Image::C_Image()
	{
		height = 0;
		width = 0;
		bpp = 0;
		fileName = NULL;
		data = NULL;
		bitmapFI = NULL;
	}

	C_Image::~C_Image()
	{
		if ( bitmapFI )
			FreeImage_Unload( bitmapFI );
	}


	//Image loading of this type *.TGA, *.JPG, *.BMP, *.DDS
	bool C_Image::Load( const char * pFileName )
	{
		char	c_drive[_MAX_DRIVE];
		char	c_dir[_MAX_DIR];
		char	c_file[_MAX_FNAME];
		char	c_ext[_MAX_EXT];

		_splitpath_s(pFileName, c_drive, c_dir, c_file, c_ext);

		//kod pre pouzite FreeImage  
		FREE_IMAGE_FORMAT	fif = FIF_UNKNOWN;
		FIBITMAP  * handleFI = NULL;

		fif = FreeImage_GetFIFFromFilename( pFileName );
		handleFI = FreeImage_Load(fif, pFileName);
		if ( handleFI )
		{
			setBitmapFI( handleFI );
			bpp = FreeImage_GetBPP( handleFI );
			width = FreeImage_GetWidth( handleFI );
			height = FreeImage_GetHeight( handleFI );
			data = FreeImage_GetBits( handleFI );
			fileName = pFileName;
			return true;
		}
		return false;
	}
}}*/