/*
* filename:			C_MountConfig.cpp
*
* author:			Kral Jozef
* date:				01/04/2011		18:58
* version:			1.00
* brief:
*/

#include "C_MountConfig.h"
#include "xml/C_XmlFile.h"
#include "C_TraceClient.h"
#include <SysUtils/Utils/C_StringUtils.h>
//#include <SysMemManager/globalNewDelete.h>


namespace scorpio{ namespace sysutils{
   
#ifdef USE_MOUNT_POINT

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_MountConfig::C_MountConfig()
	{
		m_container = new T_MappingContainer();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_MountConfig::~C_MountConfig()
	{
		SC_SAFE_DELETE(m_container);
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_MountConfig::Initialize(C_String cfgFileName)
	{
		C_XmlFile file;
		bool bRes = file.LoadXml(cfgFileName);
		if (!bRes)
			return false;

		//parse xml
		if (!file.IsValid())
			return false;

		const C_XmlNode * root = file.GetRoot();
		if (!root || !root->IsValid())
			return false;

		if (root->GetName() != "Scorpio_mount_configuration")
			return false;

		C_XmlNode::const_child_iterator it_begin = root->BeginChild("MountNode");
		C_XmlNode::const_child_iterator it_end	= root->EndChild("MountNode");
      
		//@ spracuju vsechny dialog infa v souboru
		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			const C_XmlAttribute * relPathAtt = tempNode.GetAttribute("RelativePath");
			if (!relPathAtt)
			{
				TRACE_FE("Missing 'RelativePath' attribute in node: %s", tempNode.GetName().c_str());
				continue;
			}

			C_String relPath = relPathAtt->GetValue();
			//@ modify rel path (remove // \ and to lower)
			relPath = relPath.TrimLeft('\\');
			relPath = relPath.TrimLeft('/');
			relPath = relPath.TrimRight('\\');
			relPath = relPath.TrimRight('/');
			relPath = relPath.ToLower();

			const C_XmlAttribute * absPathAtt = tempNode.GetAttribute("AbsolutePath");
			if (!absPathAtt)
			{
				TRACE_FE("Missing 'AbsolutePath' attribute in node: %s", tempNode.GetName().c_str());
				continue;
			}

			C_String absPath = absPathAtt->GetValue();
			C_HashName hash(relPath);
			std::pair<T_MappingContainer::iterator, bool> bRes = m_container->insert(T_MappingContainer::value_type(hash, absPath));
			if (!bRes.second)
			{
				TRACE_FE("Relative path %s already in mapping container!", relPath.c_str());
			}
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetAbsDirectory
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_MountConfig::GetAbsDirectory(C_String relDir)
	{
		relDir = relDir.TrimLeft('\\');
		relDir = relDir.TrimLeft('/');
		relDir = relDir.TrimRight('\\');
		relDir = relDir.TrimRight('/');
		relDir = relDir.ToLower();

		C_HashName hash(relDir);
		T_MappingContainer::iterator iter = m_container->find(hash);
		if (iter == m_container->end())
		{
			TRACE_FE("Unknown mapping for relative directory: %s", relDir.c_str());
			return C_String();
		}

		return iter->second;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RemapToAbsolute
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_MountConfig::RemapToAbsolute(C_String relFileName)
	{
		if (relFileName.empty())
			return relFileName;

		T_String tmpStr(relFileName);
		tmpStr = C_StringUtils::Replace(tmpStr, '\\', '/');
		
		if (tmpStr[(u32)0] == '/')
			tmpStr = C_String::SubString(tmpStr, 1, tmpStr.length() - 1);

		//check for mountPoint
		
		u32 id = tmpStr.Find('/');
		if (id == -1)
		{
			TRACE_FE("Relative fileName %s without MountPoint!", relFileName.c_str());
			return relFileName;
		}

		SC_ASSERT(id != 0);
		C_String relPathWithoutMountPoint = C_String::SubString(tmpStr, id+1, tmpStr.length() - 1);
		C_String mountPoint = C_String::SubString(tmpStr, 0, id - 1);
		//@ ToLOwer
		mountPoint = mountPoint.ToLower();


		C_HashName mountPointHash(mountPoint);
		T_MappingContainer::iterator iter = m_container->find(mountPointHash);
		if (iter == m_container->end())
		{
			TRACE_FE("Unknown mount point: %s", mountPoint.c_str());
			return relFileName;
		}
		
		//@ TODO we assume that there is just 1 mountPoint!!
		C_String absPath = iter->second;
		if (absPath[absPath.length()-1] != '\\' && absPath[absPath.length()-1] != '/')
			absPath += "\\";
		absPath += relPathWithoutMountPoint;

		return absPath;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ RemapToRelative
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_MountConfig::RemapToRelative(C_String absFileName)
	{
		C_String absPath = T_MountConfig::GetInstance().GetAbsDirectory("/Data/");
		C_String pref = C_String::SubString(absFileName, 0, absPath.length()-1);

		u32 dataIndex = absFileName.Find("/Data/");
		if (dataIndex == -1)
		{
			dataIndex = absFileName.Find("\\Data\\");
			if (dataIndex == -1)
			{
				TRACE_E("Can not find '/Data/' in texture path!");
				return C_String();
			}
		}

		C_String relativeTexPath = C_String::SubString(absFileName, dataIndex + 6, absFileName.length());

		C_String relPath = "/Data/";
		relPath += relativeTexPath;

		return relPath;
	}

#endif


}}