/*
* filename:			C_SysUtils.h
*
* author:			Kral Jozef
* date:				01/04/2011		21:01
* version:			1.00
* brief:				Initialization part for module SysUtils
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_SysCore
	class SYSUTILS_API C_SysUtils
	{
	public:
		//@ Initialize module
		static bool			Initialize();

		//@ Deinitialize module
		static bool			Deinitialize();
	};

}}