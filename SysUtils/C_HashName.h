/*
* filename:			C_HashName.h
*
* author:			Kral Jozef
* date:				Januar 2008
* version:			1.00
* description:		class for storing string -> hashed to 32 bit hash
*/

#pragma once

#include "C_FNVHash.h"
#include "C_String.h"
#include <Common/types.h>
#include <vector>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_HashName
	class C_HashName
	{

	public:
		//////////////////////////////////////////////////////////////////////////
		//@ c-tor
		C_HashName() : m_Hash(0) {};

		//////////////////////////////////////////////////////////////////////////
		//@ c-tor
		explicit C_HashName(const char * name) : m_Name(name), m_Hash(C_FNVHash::computeHash32(m_Name)) {};

		//////////////////////////////////////////////////////////////////////////
		//@ copy c-tor
		C_HashName(const C_HashName & hashName) : m_Hash(hashName.m_Hash), m_Name(hashName.m_Name) {};
		//@ inti c-tor
		C_HashName(T_Hash32 hash) : m_Hash(hash) {};

		//@ IsClear
		bool		IsClear() const { return m_Hash == 0; }

		//////////////////////////////////////////////////////////////////////////
		//@ operators
		const C_HashName & operator=(const char * name) { m_Name = name; m_Hash = C_FNVHash::computeHash32(name); return *this; }
		bool		operator==(const T_Hash32 & hash) const { return m_Hash == hash; }
		bool		operator!=(const T_Hash32 & hash) const { return m_Hash != hash; }
		bool		operator==(const C_HashName & hashName) const { return m_Hash == hashName.m_Hash; }
		bool		operator!=(const C_HashName & hashName) const { return m_Hash != hashName.m_Hash; }
		bool		operator<=(const C_HashName & hashName) const { return m_Hash <= hashName.m_Hash; }
		bool		operator>=(const C_HashName & hashName) const { return m_Hash >= hashName.m_Hash; }
		bool		operator<(const C_HashName & hashName) const { return m_Hash < hashName.m_Hash; }
		bool		operator>(const C_HashName & hashName) const  { return m_Hash > hashName.m_Hash; }

		T_Hash32	operator()(T_Hash32) { return m_Hash; }
		operator T_Hash32() const { return m_Hash; }
		operator const char*() const { return m_Name; }

		//@ GetName
		const char	*	GetName() const { return m_Name; }
		//@ GetHash
		T_Hash32			GetHash() const { return m_Hash; }

	private:
		C_String			m_Name;
		T_Hash32			m_Hash;
	};


	typedef std::vector<C_HashName>	T_HashNameList;
}}