/*
* filename:			C_CharacterTable.cpp
*
* author:			Kral Jozef
* date:				9/22/2011		21:10
* version:			1.00
* brief:
*/

#include "C_CharacterTable.h"
#include <SysUtils/C_TraceClient.h>
#include <Common/macros.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_CharacterTable::Initialize(const T_Stream inStream)
	{
		const u32 C_BUFFER_SIZE = 1024;
		char buffer[C_BUFFER_SIZE];
		ZERO_ARRAY(buffer);

		while(!inStream->EndOfStream())
		{
			inStream->ReadLine(buffer, C_BUFFER_SIZE);
			
			C_String str(buffer);
			std::vector<C_String> outVector;

			str.Split("//", outVector);
			if (outVector.empty())
				continue;	//empty line

			str = outVector[0];
			str = str.TrimRight('\t');
			str = str.TrimRight(' ');

			outVector.clear();
			str.Split(' ', outVector);
			if (outVector.empty() || outVector.size() < 2)
				continue;

			C_String oldValue = "0x";
			oldValue += outVector[0];
			C_String newValue = "0x";
			newValue += outVector[1];

			if (oldValue.length() != 6)
			{
				TRACE_FE("Error in Utf8 code: %s", oldValue.c_str());
				continue;
			}

			if (newValue.length() != 4)
			{
				TRACE_FE("Error in Utf8 conversion value: %s", newValue.c_str());
				continue;
			}

			u16 key = oldValue.ToHex();

			u8 character = newValue.ToHex();
			m_characterMap.insert(T_CharacterMap::value_type(key, character));
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ConvertCharacter
	//
	//////////////////////////////////////////////////////////////////////////
	char C_CharacterTable::ConvertCharacter(u8 utf8code, u8 utf8character) const
	{
		u16 key = utf8code;
		key = key << 8;
		key |= utf8character;

		T_CharacterMap::const_iterator iter = m_characterMap.find(key);
		if (iter == m_characterMap.end())
		{
			TRACE_FE("Uknown conversion: 0x%X", key);
			return (char)'?';
		}

		return iter->second;
	}
}}
