/*
* filename:			C_LogFile.cpp
*
* author:			Kral Jozef
* date:				09/17/2002		23:35
* version:			1.00
* brief:				loguje do suboru hlasky
*/

#include "C_LogFile.h"
#include <stdio.h>
#include <stdarg.h>
#include <Common/Assert.h>
#include <errno.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_LogFile::C_LogFile(char* fileName)
		: m_fileName(fileName)
	{
		FILE * file = NULL;
#ifdef PLATFORM_WIN32
		errno_t res = fopen_s(&file, m_fileName, "wt");
#elif defined PLATFORM_MACOSX
      file = fopen(m_fileName, "wt");
#endif
		SC_ASSERT(file);
		fclose(file);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ Write
	bool C_LogFile::Write(char* text, ...)
	{
		va_list arg_list;

		va_start(arg_list,text);

		FILE * file = NULL;
#ifdef PLATFORM_WIN32      
		errno_t res = fopen_s(&file, m_fileName,"a+");
#elif defined PLATFORM_MACOSX
      file = fopen(m_fileName,"a+");
      if (!file)
         return false;
#endif
		SC_ASSERT(file);

		vfprintf(file, text, arg_list);

		fclose(file);
		va_end(arg_list);

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ Writeln
	bool C_LogFile::Writeln(char* text, ...)
	{
		va_list arg_list;

		va_start(arg_list,text);

		FILE * file = NULL;
#ifdef PLATFORM_WIN32      
		errno_t res = fopen_s(&file, m_fileName,"a+");
#elif defined PLATFORM_MACOSX
      file = fopen(m_fileName,"a+");
      if (!file)
         return false;
#endif      
		SC_ASSERT(file);

		vfprintf(file, text, arg_list);
		putc('\n',file);

		fclose(file);

		va_end(arg_list);

		return true;
	}
}}