/*
* filename:			TraceClient.h
*
* author:			Petr Slivon
* modify:			Jozef Kral
* date:				07/26/2008		12:33
* version:			2.00
* brief:				Trace client
*/

#pragma once

#include "SysUtilsApi.h"
#include <Common/Types.h>
#include <Common/Assert.h>
#include <Common/GlobalDefines.h>

namespace scorpio{

	//////////////////////////////////////////////////////////////////////////
	//@ Messages
	enum E_TraceMsgType
	{
		E_TMT_CLEAR_LIST,
		E_TMT_INFO,
		E_TMT_WARNING,
		E_TMT_ERROR,
		E_TMT_STOP_ERROR,
		// ...
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_TraceClient
	class SYSUTILS_API C_TraceClient{
	public:
		//@ runServer
		static void runServer(const char* serverPath = 0);
		//@ sendTrace
		static void sendTrace(const E_TraceMsgType traceMsgType, const char* file, u32 line, const char* format, ...);
	private:
	};
}

#ifdef _TRACELOG_
	#define INIT_TRACE_CLIENT(serverPath)	scorpio::C_TraceClient::runServer(serverPath);
	#define TRACE_I(msg)							scorpio::C_TraceClient::sendTrace(scorpio::E_TMT_INFO, __FILE__, __LINE__, msg);
	#define TRACE_FI(msg, ...)					scorpio::C_TraceClient::sendTrace(scorpio::E_TMT_INFO, __FILE__, __LINE__, msg, __VA_ARGS__);
	#define TRACE_W(msg)							scorpio::C_TraceClient::sendTrace(scorpio::E_TMT_WARNING, __FILE__, __LINE__, msg);
	#define TRACE_FW(msg, ...)					scorpio::C_TraceClient::sendTrace(scorpio::E_TMT_WARNING, __FILE__, __LINE__, msg, __VA_ARGS__);
	#define TRACE_E(msg)							scorpio::C_TraceClient::sendTrace(scorpio::E_TMT_ERROR, __FILE__, __LINE__, msg);
	#define TRACE_FE(msg, ...)					scorpio::C_TraceClient::sendTrace(scorpio::E_TMT_ERROR, __FILE__, __LINE__, msg, __VA_ARGS__);
	#define TRACE_FU(SuccesFlag, msg, ...)	scorpio::C_TraceClient::sendTrace(SuccesFlag ? scorpio::E_TMT_INFO : scorpio::E_TMT_ERROR, __FILE__, __LINE__, msg, __VA_ARGS__);

#else
	#define INIT_TRACE_CLIENT(serverPath)	((void)0);
	#define TRACE_I(msg)							((void)0);
	#define TRACE_FI(msg, ...)					((void)0);
	#define TRACE_W(msg)							((void)0);
	#define TRACE_FW(msg, ...)					((void)0);
	#define TRACE_E(msg)							((void)0);
	#define TRACE_FE(msg, ...)					((void)0);
	#define TRACE_FU(SuccesFlag, msg, ...)	((void)0);
#endif
