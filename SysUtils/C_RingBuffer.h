/*
* filename:			C_RingBuffer.h
*
* author:			Kral Jozef
* date:				1/25/2012		16:39
* version:			1.00
* brief:				Generic ring buffer + implemented queue interface so Queue
*/

#pragma once

#include <Common/types.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_RingBuffer
	template<class T>
	class C_RingBuffer
	{
	public:
		//@ c-tor
		C_RingBuffer(u32 bufferSize, const T & clearValue)
			: m_Size(bufferSize), m_HeadId(0)
		{
			m_Data = new T[bufferSize]();
			memset(m_Data, sizeof(T) * bufferSize, clearValue);
		}


		//@ d-tor
		~C_RingBuffer()
		{
			SAFE_DELETE_ARRAY(m_Data);
		};


		//@ queue Interface
		//@ Push
		void			Push(const T & value)
		{
			m_Data[m_HeadId] = value;
			IncrementHead();
		}

		//@ const T & is not thread safe
		T				GetValueAt(u32 nIndex)
		{
			SC_ASSERT(nIndex < m_Size);
			//@ recalc Id
			s32 tmpId = m_HeadId - (nIndex + 1);	//head point to next free // last item
			if (tmpId  < 0)
				tmpId = m_Size - tmpId;

			return m_Data[tmpId];
		}

		//@ GetLast - return item which will be popped (was first inserted)
		T				GetLast()
		{
			return GetValueAt((u32) m_Size - 1);
		}


		//@ Reset
		void			Reset(const T & clearValue)
		{
			m_HeadId = 0;
			memset(m_Data, sizeof(T) * m_Size, clearValue);
		}


	private:
		//@ IncrementHead
		void			IncrementHead() 
		{
			++m_HeadId;
			if (m_HeadId == m_Size)
				m_HeadId = 0;
		}

	private:
		u32			m_Size;		//bufferSize
		u32			m_HeadId;	//pointer to start of buffer
		T			*	m_Data;		//pointer to buffer data
	};
}}
