/*
* filename:			C_SysUtils.cpp
*
* author:			Kral Jozef
* date:				01/04/2011		21:01
* version:			1.00
* brief:
*/

#include "C_SysUtils.h"
#include <SysUtils/C_MountConfig.h>
#include <SysUtils/C_Scheduler.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysUtils/Utils/C_FileSystemUtils.h>
#include <SysUtils/C_CharacterTable.h>
#include <SysUtils/C_MountConfig.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace sysutils{


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_SysUtils::Initialize()
	{
		bool bRes = true;
#ifdef USE_MOUNT_POINT
		bRes &= T_MountConfig::CreateInstance();
#endif
		bRes &= T_Scheduler::CreateInstance();
		bRes &= T_FileSystemManager::CreateInstance();
      bRes &= C_FileSystemUtils::Initialize();
		bRes &= T_CharacterTable::CreateInstance();
		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Deinitialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_SysUtils::Deinitialize()
	{
		T_CharacterTable::DestroyInstance();
		T_FileSystemManager::DestroyInstance();
		T_Scheduler::DestroyInstance();
#ifdef USE_MOUNT_POINT
		T_MountConfig::DestroyInstance();
#endif
		return true;
	}


}}