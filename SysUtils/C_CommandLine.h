/*
* filename:			C_CommandLine.h
*
* author:			Kral Jozef
* date:				01/15/2011		10:33
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include "C_String.h"
#include <vector>
#include <map>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_CommandLine
	class SYSUTILS_API C_CommandLine
	{
	public:
		//@ c-tor
		C_CommandLine() {};

		//@ SetCmdLine
		//@ ex. /Level:Mission01 /Demo:true
		void							SetCmdLine(const char * cmdLine);
		//@ HasArgument
		bool							HasArg(C_String argName) const;
		//@ GetArgValue
		template<typename T>
		T								GetArgValue(C_String argName) const { SC_CTASSERT(false); }

	private:
		C_String						m_cmdLine;
		typedef std::vector<C_String>	T_StringVector;
		typedef std::map<C_String, C_String>	T_StringMap;
		T_StringMap					m_cmdArgs;
	};
   
   
   
   //////////////////////////////////////////////////////////////////////////
   //@ explicit specialization   
   template<>
   inline bool C_CommandLine::GetArgValue(C_String argName) const
   {
      T_StringMap::const_iterator iter = m_cmdArgs.find(argName);
      if (iter != m_cmdArgs.end())
      {
         C_String strTmp = iter->second.ToLower();
         return (strTmp == "true");
      }
      return false;
   }   
   
  
   //////////////////////////////////////////////////////////////////////////
   //@ explicit specialization
   template<>
   inline C_String C_CommandLine::GetArgValue(C_String argName) const
   {
      T_StringMap::const_iterator iter = m_cmdArgs.find(argName);
      if (iter != m_cmdArgs.end())
         return iter->second;
      return C_String();
   }

}}