#pragma once

#ifndef SYSUTILS_H__
#define SYSUTILS_H__

	#ifdef _USRDLL
		#ifdef SYSUTILS_EXPORTS
			#define SYSUTILS_API __declspec(dllexport)
			#define SYSUTILS_EXP_TEMP_INST
		#else
			#define SYSUTILS_API __declspec(dllimport)
			#define SYSUTILS_EXP_TEMP_INST extern
		#endif
	#else
		#define SYSUTILS_API
		#define SYSUTILS_EXP_TEMP_INST
	#endif

#ifdef USE_BOOST_SMARTPTR
	#include <boost/shared_ptr.hpp>
#else
	#include <Common/C_SmartPtr.h>
#endif


#ifdef USE_BOOST_SMARTPTR
	#undef DECLARE_SHARED_PTR
	#define	DECLARE_SHARED_PTR(ClassType, ClassName) \
		typedef boost::shared_ptr<ClassType>	ClassName;
#else
	#undef DECLARE_SHARED_PTR
	#define	DECLARE_SHARED_PTR(ClassType, ClassName) \
		typedef C_SharedPtr<ClassType>			ClassName;
#endif



#endif
