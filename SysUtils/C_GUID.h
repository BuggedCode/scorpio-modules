/*
* filename:			C_GUID.h
*
* author:			Kral Jozef
* date:				02/06/2008		19:48
* version:			1.00
* brief:				Global Unique IDentificator
*/

#pragma once

#include <Common/Pragmas.h>
#include <Common/types.h>
#include <stdlib.h>
#include <boost/serialization/access.hpp>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	// C_GUID
	class C_GUID
	{
	public:
		//////////////////////////////////////////////////////////////////////////
		//@ c-tor
		C_GUID() : m_part1(0xffffffff), m_part2(0xffffffff) {};
		C_GUID(const C_GUID & guid) : m_part1(guid.m_part1), m_part2(guid.m_part2) {};
		C_GUID(u32 part1, u32 part2) : m_part1(part1), m_part2(part2) {};

		bool	operator==(const C_GUID & guid) const { return (m_part1 == guid.m_part1 && m_part2 == guid.m_part2); }
		bool	operator!=(const C_GUID & guid) const { return (m_part1 != guid.m_part1 || m_part2 != guid.m_part2); }
		bool	operator<(const C_GUID & guid) const
		{ 
			return ((m_part1 < guid.m_part1) || ((m_part1 == guid.m_part1) && (m_part2 < guid.m_part2)));
		}

		//@ IsValid
		bool	IsValid() const { return !((m_part1 & 0xffffffff) && (m_part2 & 0xffffffff)); }
		void	Generate()
		{
			//dik Tonik
			m_part1 = (rand()<<16 | rand());
			m_part2 = (rand()<<16 | rand());
		}
		
		friend class boost::serialization::access;
		template<class Archive>
		void	serialize(Archive & ar, const u32 version)
		{
			ar & m_part1;
			ar & m_part2;
		}

	private:
		u32	m_part1;
		u32	m_part2;
	};
}}