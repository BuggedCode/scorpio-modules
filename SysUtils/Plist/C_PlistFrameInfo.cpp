/*
* filename:			C_PlistFrameInfo.cpp
*
* author:			Kral Jozef aka BuggedCode
* date:				10/2/2014		22:28
* version:			1.00
* brief:
*/

#include "C_PlistFrameInfo.h"
#include <Common/Assert.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ C_PlistFrameInfo
	//
	//////////////////////////////////////////////////////////////////////////
	C_PlistFrameInfo::C_PlistFrameInfo(const char * pFrameName)
	{
		SC_ASSERT(pFrameName);	
		//if (pFrameName)
		m_FrameName = pFrameName;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsIntersection
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PlistFrameInfo::IsIntersection(const sysmath::C_Point & ptIntex) const
	{
		if ((ptIntex.x >= m_Rect.m_left && ptIntex.x <= m_Rect.m_right) && (ptIntex.y >= m_Rect.m_top && ptIntex.y <= m_Rect.m_bottom))
			return true;

		return false;
	}

}};
