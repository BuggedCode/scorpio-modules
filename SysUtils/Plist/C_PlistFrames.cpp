/*
* filename:			C_PlistFrames.cpp
*
* author:			Kral Jozef aka BuggedCode
* date:				10/2/2014		21:26
* version:			1.00
* brief:
*/

#include "C_PlistFrames.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/C_FNVHash.h>
#include <SysUtils/Utils/C_StringUtils.h>
#include <SysMath/C_Point.h>

namespace scorpio{ namespace sysutils{

	using namespace sysmath;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddFrame
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PlistFrames::AddFrame(T_PlistFrameInfo frameInfo)
	{
		T_Hash32 hash32 = C_FNVHash::computeHash32(frameInfo->GetFrameName().c_str());

		std::pair<T_Container::iterator, bool> ret = m_FrameContainer.insert(T_Container::value_type(hash32, frameInfo));
		if (!ret.second)
		{
			TRACE_FE("Duplicated frame: %s in FramesContainer.", frameInfo->GetFrameName().c_str());
			return;
		}

		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ FindFrameName
	//
	//////////////////////////////////////////////////////////////////////////
	const T_String & C_PlistFrames::FindFrameName(const sysmath::C_Vector2 & texCoordCenter) const
	{
		C_Point ptInTex((s32)(texCoordCenter.m_x * m_MetaData.m_Size.x), (s32)(texCoordCenter.m_y * m_MetaData.m_Size.y));

		for (T_Container::const_iterator iter = m_FrameContainer.begin(); iter != m_FrameContainer.end(); ++iter)
		{
			bool bRes = iter->second->IsIntersection(ptInTex);
			if (bRes)
				return iter->second->GetFrameName();
		}
		
		return C_StringUtils::C_EMPTY_STD_STRING;
	}
	
}};
