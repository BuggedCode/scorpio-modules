/*
* filename:			C_PlistParser.h
*
* author:			Kral Jozef aka BuggedCode
* date:				10/2/2014		19:48
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <Sysutils/FileSystem/C_Stream.h>
#include <SysUtils/Plist/C_PlistFrames.h>
#include <SysUtils/C_String.h>

namespace scorpio{ namespace sysutils{

	class C_XmlFile;
	class C_XmlNode;

	//////////////////////////////////////////////////////////////////////////
	//@ C_PlistParser
	class SYSUTILS_API C_PlistParser
	{
	public:
		//@ LoadFrames
		static T_PlistFrames		LoadFrames(T_Stream inputStream);

	private:
		//@ ValidatePlist
		static bool					ValidateFile(C_XmlFile & xmlFile);
		//@ ReadFrames
		static bool					ParseFrames(T_PlistFrames plistFrames, const C_XmlNode & inNode);
		//@ ReadMetadata
		static bool					ParseMetadata(T_PlistFrames plistFrames, const C_XmlNode & inNode);
		//@ ParseFrameInfo
		static bool					ParseFrameInfo(T_PlistFrameInfo frameInfo, const C_XmlNode & inNode);

		//@ ParseRect
		static bool					ParseRect(const T_String & strValue, sysmath::C_Rect & outRect);
		//@ ParsePoint
		static bool					ParsePoint(const T_String & strValue, sysmath::C_Point & outPoint);
	};


	typedef C_PlistParser	T_PlistParser;
}}