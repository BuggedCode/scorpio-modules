/*
* filename:			C_PlistFrames.h
*
* author:			Kral Jozef aka BuggedCode
* date:				10/2/2014		21:24
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/Plist/C_PlistFrameInfo.h>
#include <SysUtils/C_String.h>
#include <map>

namespace scorpio{
	namespace sysmath{
		class C_Vector2;

	}
}

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_PlistFrames
	class SYSUTILS_API C_PlistFrames
	{
		struct S_MetaData
		{
			sysmath::C_Point	m_Size;
			std::string			m_RealTextureFileName;
			std::string			m_TextureFileName;
			s32					m_Format;
		};

	public:
		//@ AddFrame
		void				AddFrame(T_PlistFrameInfo frameInfo);

		//@ SetTextureSize
		void				SetTextureSize(const sysmath::C_Point & size) { m_MetaData.m_Size = size; }
		//@ SetFormat
		void				SetFormat(s32 nFormat) { m_MetaData.m_Format = nFormat; }
		//@ SetRealTextureFileName
		void				SetRealTextureFileName(const T_String & realTexFileName) { m_MetaData.m_RealTextureFileName = realTexFileName; }
		//@ SetTextureFileName
		void				SetTextureFileName(const T_String & texFileName) { m_MetaData.m_TextureFileName = texFileName; }

		//@ FindFrameName
		const T_String &	FindFrameName(const scorpio::sysmath::C_Vector2 & texCoordCenter) const;

	private:
		S_MetaData		m_MetaData;
		typedef std::map<T_Hash32, T_PlistFrameInfo>	T_Container;
		T_Container		m_FrameContainer;
	};


	DECLARE_SHARED_PTR(scorpio::sysutils::C_PlistFrames, T_PlistFrames);


}};
