/*
* filename:			C_PlistFrameInfo.h
*
* author:			Kral Jozef aka BuggedCode
* date:				10/2/2014		22:28
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/C_String.h>
#include <SysMath/C_Rect.h>


namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_PlistFrames
	class SYSUTILS_API C_PlistFrameInfo
	{
	public:
		//@ c-tor
		C_PlistFrameInfo(const char * pFrameName);
		//@ SetRect
		void						SetRect(const sysmath::C_Rect & rect) { m_Rect = rect; }
		//@ SetOffset
		void						SetOffset(const sysmath::C_Point & offset) { m_Offset = offset; }
		//@ SetSourceSize
		void						SetSourceSize(const sysmath::C_Point & srcSize) { m_SourceSize = srcSize; }
		//@ GetFrameName
		const T_String			&	GetFrameName() const { return m_FrameName; }

		//@ IsIntersection
		bool						IsIntersection(const sysmath::C_Point & ptIntex) const;

	private:
		T_String			m_FrameName;
		sysmath::C_Rect		m_Rect;		//Top of texture is 0,0
		sysmath::C_Point	m_Offset;
		sysmath::C_Point	m_SourceSize;
	};


#ifdef USE_BOOST_SMARTPTR
	typedef boost::shared_ptr<scorpio::syscore::C_PlistFrameInfo>	T_PlistFrameInfo;
#else
	typedef C_SharedPtr<scorpio::sysutils::C_PlistFrameInfo>			T_PlistFrameInfo;
#endif

}};
