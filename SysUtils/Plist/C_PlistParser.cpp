/*
* filename:			C_PlistParser.cpp
*
* author:			Kral Jozef aka BuggedCode
* date:				10/2/2014		19:32
* version:			1.00
* brief:
*/

#include "C_PlistParser.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/Xml/C_XmlFile.h>
#include <SysUtils/Utils/C_StringUtils.h>


namespace scorpio{ namespace sysutils{

	using namespace sysmath;

	static const char * C_PLIST_NODE_NAME = "plist";
	static const char * C_STR_FRAMES_TAG_ID = "frames";
	static const char * C_STR_METADATA_TAG_ID = "metadata";
	static const char * C_STR_DICT_ID = "dict";


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Open
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PlistParser::ValidateFile(C_XmlFile & xmlFile)
	{
		//parse xml
		if (!xmlFile.IsValid())
			return false;

		const C_XmlNode * root = xmlFile.GetRoot();
		if (!root || !root->IsValid())
			return false;

		T_String strTmp(C_PLIST_NODE_NAME);
		if (root->GetName() != strTmp)
			return false;

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Open
	//
	//////////////////////////////////////////////////////////////////////////
	T_PlistFrames C_PlistParser::LoadFrames(T_Stream inputStream)
	{
		if (!inputStream)
			return T_PlistFrames(NULL);

		C_XmlFile xmlFile;
		bool bRes = xmlFile.Load(inputStream);
		if (!bRes)
			return T_PlistFrames(NULL);

		bRes = C_PlistParser::ValidateFile(xmlFile);
		if (!bRes)
			return T_PlistFrames(NULL);

		//@ get key-values
		const C_XmlNode * tmpNode = xmlFile.GetRoot()->GetNode(C_STR_DICT_ID);
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Missing 'dict' element in plist File: %s ", inputStream->GetFileName());
			return T_PlistFrames(NULL);
		}


		C_XmlNode::const_child_iterator it_begin = tmpNode->BeginChild();
		C_XmlNode::const_child_iterator it_end	= tmpNode->EndChild();

		T_PlistFrames plistFrames = T_PlistFrames(new C_PlistFrames());
		//@ spracuju vsechny templaty v subore
		bool bReadNextFrames = false;
		bool bReadNextMetadata = false;
		for (; it_begin != it_end; ++it_begin)
		{
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			if (bReadNextFrames)
			{
				ParseFrames(plistFrames, tempNode);
				bReadNextFrames  = false;
				continue;
			}
			else if (bReadNextMetadata)
			{
				ParseMetadata(plistFrames, tempNode);
				bReadNextMetadata = false;
				continue;
			}

			T_String outVal;
			bool bRes = tempNode.Read(outVal);
			if (!bRes)
				continue;
			

			if (outVal == T_String(C_STR_FRAMES_TAG_ID))
			{
				bReadNextFrames  = true;
				continue;
			}


			if (outVal == T_String(C_STR_METADATA_TAG_ID))
			{
				bReadNextMetadata = true;
				continue;
			}
		}

		return plistFrames;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ParseFrames
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PlistParser::ParseFrames(T_PlistFrames plistFrames, const C_XmlNode & inNode)
	{
		C_XmlNode::const_child_iterator it_begin = inNode.BeginChild();
		C_XmlNode::const_child_iterator it_end	= inNode.EndChild();

		T_PlistFrameInfo frameInfo;
		for (; it_begin != it_end; ++it_begin)
		{
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			if (frameInfo)
			{
				//@ parse FrameDetails

				if (tempNode.GetName() != T_String(C_STR_DICT_ID))
				{
					TRACE_E("Incorrect FrameInfo xmlNode");
					continue;
				}

				bool bRes = ParseFrameInfo(frameInfo, tempNode);
				if (!bRes)
				{
					TRACE_E("Error parsing FrameInfo");
					continue;
				}

				plistFrames->AddFrame(frameInfo);
				frameInfo.reset();
				continue;
			}

			//@ check if is frameName
			if (tempNode.GetName() == T_String("key"))
			{
				T_String outVal;
				bool bRes = tempNode.Read(outVal);
				if (!bRes)
					continue;

				frameInfo = T_PlistFrameInfo(new C_PlistFrameInfo(outVal.c_str()));
				continue;
			}
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ParseFrameInfo
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PlistParser::ParseFrameInfo(T_PlistFrameInfo frameInfo, const C_XmlNode & inNode)
	{
		C_XmlNode::const_child_iterator it_begin = inNode.BeginChild();
		C_XmlNode::const_child_iterator it_end	= inNode.EndChild();

		for (; it_begin != it_end; ++it_begin)
		{
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			if (tempNode.GetName() == T_String("key"))
			{
				T_String outVal;
				bool bRes = tempNode.Read(outVal);
				if (!bRes)
					continue;

				if (outVal == T_String("frame"))
				{
					++it_begin;
					const C_XmlNode & nodeFrame = *it_begin;
					if (!nodeFrame.IsValid())
						continue;

					bool bRes = nodeFrame.Read(outVal);
					if (!bRes)
						continue;

					C_Rect rect;
					if (ParseRect(outVal, rect))
						frameInfo->SetRect(rect);
				}
				else if (outVal == T_String("offset"))
				{
					++it_begin;
					const C_XmlNode & nodeFrame = *it_begin;
					if (!nodeFrame.IsValid())
						continue;

					bool bRes = nodeFrame.Read(outVal);
					if (!bRes)
						continue;

					C_Point offset;
					if (ParsePoint(outVal, offset))
						frameInfo->SetOffset(offset);
				}
				else if (outVal == T_String("rotated"))
				{
					++it_begin;
				}
				else if (outVal == T_String("sourceColorRect"))
				{
					++it_begin;
				}
				else if (outVal == T_String("sourceSize"))
				{
					++it_begin;
					const C_XmlNode & nodeFrame = *it_begin;
					if (!nodeFrame.IsValid())
						continue;

					bool bRes = nodeFrame.Read(outVal);
					if (!bRes)
						continue;

					C_Point srcSize;
					if (ParsePoint(outVal, srcSize))
						frameInfo->SetSourceSize(srcSize);
				}
			}
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ParseMetadata
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PlistParser::ParseMetadata(T_PlistFrames plistFrames, const C_XmlNode & inNode)
	{
		C_XmlNode::const_child_iterator it_begin = inNode.BeginChild();
		C_XmlNode::const_child_iterator it_end	= inNode.EndChild();

		for (; it_begin != it_end; ++it_begin)
		{
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			if (tempNode.GetName() == T_String("key"))
			{
				T_String outVal;
				bool bRes = tempNode.Read(outVal);
				if (!bRes)
					continue;

				if (outVal == T_String("format"))
				{
					++it_begin;
					const C_XmlNode & nodeFrame = *it_begin;
					if (!nodeFrame.IsValid())
						continue;

					bool bRes = nodeFrame.Read(outVal);
					if (!bRes)
						continue;

					s32 format;
					bool bRes_ = C_StringUtils::StringToInt(outVal, format);
					SC_ASSERT(bRes_);
					plistFrames->SetFormat(format);
				}
				else if (outVal == T_String("realTextureFileName"))
				{
					++it_begin;
					const C_XmlNode & nodeFrame = *it_begin;
					if (!nodeFrame.IsValid())
						continue;

					bool bRes = nodeFrame.Read(outVal);
					if (!bRes)
						continue;

					plistFrames->SetRealTextureFileName(outVal);
				}
				else if (outVal == T_String("size"))
				{
					++it_begin;
					const C_XmlNode & nodeFrame = *it_begin;
					if (!nodeFrame.IsValid())
						continue;

					bool bRes = nodeFrame.Read(outVal);
					if (!bRes)
						continue;

					C_Point srcSize;
					if (ParsePoint(outVal, srcSize))
						plistFrames->SetTextureSize(srcSize);
				}
				else if (outVal == T_String("textureFileName"))
				{
					++it_begin;
					const C_XmlNode & nodeFrame = *it_begin;
					if (!nodeFrame.IsValid())
						continue;

					bool bRes = nodeFrame.Read(outVal);
					if (!bRes)
						continue;

					plistFrames->SetTextureFileName(outVal);
				}
			}
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ParseRect
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PlistParser::ParseRect(const T_String & strValue, sysmath::C_Rect & outRect)
	{
		T_StringVector strVector;
		C_StringUtils::Split(strValue, ',', strVector);

		if (strVector.size() != 4)
			return false;

		for (s8 i = 0; i < 4; ++i)
		{
			//strVector[i] = strVector[i].TrimLeft('{');
			//strVector[i] = strVector[i].TrimRight('}');
			strVector[i] = C_StringUtils::TrimFromLeft(strVector[i], '{');
			strVector[i] = C_StringUtils::TrimFromRight(strVector[i], '}');
		}

		C_StringUtils::StringToInt(strVector[0], outRect.m_left);
		C_StringUtils::StringToInt(strVector[1], outRect.m_top);
		s32 width, height;
		C_StringUtils::StringToInt(strVector[2], width);
		C_StringUtils::StringToInt(strVector[3], height);
		outRect.SetWidth(width);
		outRect.SetHeight(height);
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ParsePoint
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PlistParser::ParsePoint(const T_String & strValue, sysmath::C_Point & outPoint)
	{
		T_StringVector strVector;
		C_StringUtils::Split(strValue, ',', strVector);

		if (strVector.size() != 2)
			return false;

		for (s8 i = 0; i < 2; ++i)
		{
			//strVector[i] = strVector[i].TrimLeft('{');
			//strVector[i] = strVector[i].TrimRight('}');
			strVector[i] = C_StringUtils::TrimFromLeft(strVector[i], '{');
			strVector[i] = C_StringUtils::TrimFromRight(strVector[i], '}');
		}

		C_StringUtils::StringToInt(strVector[0], outPoint.x);
		C_StringUtils::StringToInt(strVector[1], outPoint.y);

		return true;
	}
}}
