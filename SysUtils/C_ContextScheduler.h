/*
* filename:			C_ContextScheduler.h
*
* author:			Kral Jozef
* date:				05/21/2011		15:33
* version:			1.00
* brief:
*/

#pragma once

#include <map>
#include <Common/types.h>
#include <Common/Assert.h>

namespace scorpio{ namespace sysutils{

   //@ testJozo
   class C_TestClass
   {
   public:
      void     TestFunction();
   };
   
	//////////////////////////////////////////////////////////////////////////
	//@ C_ContextScheduler
	template<class T>
	class C_ContextScheduler
	{
	public:
		typedef void (T::*T_EventFunc)();
		typedef std::map<u64, T_EventFunc>	T_EventMap;
      
	public:
		//@ c-tor
		C_ContextScheduler(T & context) : m_Context(context), m_currentTick(0) {};
		//@ d-tor
		C_ContextScheduler() {};

		//@ Schedule
		//@ funcPtr - pointer to method of class T
		//@ dellayTime in milliseconds - after dellayTime from now will be funcPtr called
		void				Schedule(T_EventFunc funcPtr, u32 dellayTime)
		{
			u64 callingTime = m_currentTick + dellayTime;
			std::pair<typename T_EventMap::iterator, bool> res = m_eventMap.insert(typename T_EventMap::value_type(callingTime, funcPtr));
			SC_ASSERT(res.second);
		}

		//@ Update
		//@ param - tickDelta in milliseconds
		void				Update(u32 tickDelta)
		{
			m_currentTick += tickDelta;

			if (m_eventMap.empty())
				return;

			typename T_EventMap::iterator iter = m_eventMap.begin();
			while (iter != m_eventMap.end())
			{
				//@ ak aktualny cas este nedosiahol najblizsie naplanovanu ulohu tak von
				if (iter->first > m_currentTick)
					return;

				T_EventFunc	eventFunc = iter->second;

				//@ call event
				(m_Context.*eventFunc)();

				typename T_EventMap::iterator tmpIter = iter;

				++iter;
				m_eventMap.erase(tmpIter);	//return from erase 'bidirect iterator beyond' is not in standard
			}
		}

		//@ Clear
		void				Clear()
		{
			m_eventMap.clear();
		}

	private:
		T				&	m_Context;	//kontext nesmi zaniknut skor ako scheduler!
		u64				m_currentTick;

		T_EventMap		m_eventMap;	//sorted events	
	};

}}