/*
* filename:			C_String.cpp
*
* author:				Kral Jozef
* date:					October 2006
* version:			1.00
* description:	Class for manipulate with string
*/

#include "C_String.h"
#include <stdio.h>
#include <stdarg.h>

#include <SysUtils/Xml/C_XmlConvertorUtils.h>	//kvuli konverzim
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>
#include <Common/Types.h>
#include <Common/macros.h>

namespace scorpio{ namespace sysutils{

	//@ nullStrInitData - initialised memory for C_StringInfoData
	//@ u32 m_refCount = 1	//never release this nullStrInfoData
	//@ u32 m_length = 1		//only termination character
	//@ u32 m_allocated = 4	//allocated size for string is u32 => 4bytes
	//@ fourth item in array nullStrInitData is 0 - termination character(our string) - 4 bytes of 0
	static u32 nullStrInitData[4] = { 1, 1, 4, 0 };

	static C_StringInfoData * nullStringData = (C_StringInfoData*)nullStrInitData;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ implicit c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_String::C_String()
	{
		m_string = nullStringData->getData();
		nullStringData->m_refCount++;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ init c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_String::C_String(const char * string)
	{
		if (string)
		{
			int len = (int)strlen(string) + 1;
			//alokuj info pred samotnym stringom
			if (allocStringInfo(len))
            STR_CPY(m_string, string, len);            
		}
		else
      {
			m_string = nullStringData->getData();
         nullStringData->m_refCount++;
      }
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_String::C_String(u32 preallocLen)
	{
		allocStringInfo(preallocLen);
		C_StringInfoData * info = ((C_StringInfoData*)m_string) - 1;
		info->m_length = 1;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ init c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_String::C_String(const char * string, u32 start, u32 end)
	{
		if (string)
		{
			u32 len = (u32)strlen(string);
			if (start > end)
			{
				u32 tmp = end;
				end = start;
				start = tmp;
			}

			start = MAX(0, start);
			end = MIN(end, len);

			len = end - start + 1;

			//alokuj info pred samotnym stringom
			if (allocStringInfo(len+1))	//+1 for termination
			{
				MEM_CPY(m_string, len+1, string + start, len);
				m_string[len] = 0;
			}
		}
		else
      {
			m_string = nullStringData->getData();
         nullStringData->m_refCount++;
      }
	}


	/////////////////////////////////////////////////////////////////////////
	//
	//@ allocStringInfo
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_String::allocStringInfo(u32 pLength)
	{
		C_StringInfoData * info = reinterpret_cast<C_StringInfoData*>(new char[sizeof(C_StringInfoData) + (pLength << 1)]);	//length*2
		if (info)
		{		
			info->m_refCount = 1;
			info->m_length = pLength;
			info->m_allocated = pLength << 1;
			m_string = info->getData();
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ expandData
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_String::expandData(u32 newLength)
	{
		char * ptrToOldStr = m_string;
		C_StringInfoData * oldInfo = ((C_StringInfoData*)m_string) - 1;
		u32 newAllocLen = newLength > oldInfo->m_allocated << 1 ? newLength : oldInfo->m_allocated << 1;
		bool bRes = allocStringInfo(newAllocLen);

		if (!bRes)
			return false;

		//copy old data to new data
		C_StringInfoData * newInfo = ((C_StringInfoData*)m_string) - 1;
		MEM_CPY(m_string, newInfo->m_allocated, ptrToOldStr, oldInfo->m_allocated);
		newInfo->m_length = oldInfo->m_length;

		//dec old reference
		oldInfo->m_refCount--;
		if (oldInfo->m_refCount == 0)
			delete[] oldInfo;


		return bRes;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ operator +=
	//
	//////////////////////////////////////////////////////////////////////////
	C_String & C_String::operator +=(const char * string)
	{
		if (*string)//ak nie je prazdny
		{
			C_StringInfoData * info = ((C_StringInfoData*)m_string) - 1;
			u32 origLen = info->m_length - 1;			//-1 for termination
			u32 stringLen = (u32)strlen(string);
			if (info->m_refCount > 1)	//nie sme jedina instancia
			{
				decreaseReference();
				char * ptrOrigStr = m_string;	//store pointer to orig string
				allocStringInfo(origLen + stringLen + 1);		//+1 fot termination

				C_StringInfoData * newInfo = ((C_StringInfoData*)m_string) - 1;

				MEM_CPY(m_string, newInfo->m_allocated, ptrOrigStr, origLen);	//copy orig string
				MEM_CPY(m_string + origLen, newInfo->m_allocated - origLen, string, stringLen + 1);	//+1 for termination char
			}
			else
			{
				//sme jedina instancia, modifikujeme priamo retazec
				u32 sum = origLen + stringLen + 1;	//+1 for termination
				if (sum < info->m_allocated)
				{
					info->m_length = sum;
					MEM_CPY(m_string + origLen, info->m_allocated - origLen, string, stringLen + 1);	//+1 for termination char
				}
				else
				{
					/*bool bRes = */expandData(sum);
					C_StringInfoData * newInfo = ((C_StringInfoData*)m_string) - 1;
					MEM_CPY(m_string + origLen, newInfo->m_allocated - origLen, string, stringLen + 1);	//+1 for termination char
					newInfo->m_length += stringLen;
				}
			}
		}
		return *this;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetLength
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_String::length() const
	{
		C_StringInfoData * info = ((C_StringInfoData*)m_string) - 1;
		return info->m_length - 1;
	}

	
	
	//////////////////////////////////////////////////////////////////////////
	//
	//@ Format
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_String::Format(const char * format, ...)
	{
		va_list	arglist;
		va_start(arglist, format);
		char buffer[1024];
		VSNPRINTF(buffer, 1024, format, arglist);
		va_end(arglist);
		C_String str(buffer);
		return str;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SubString
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_String::SubString(const C_String & str, u32 start, u32 end)
	{
		return C_String(str.c_str(), start, end);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Left
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_String::Left(u32 nLastIndex) const
	{
		return SubString(m_string, 0, nLastIndex);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Right
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_String::Right(u32 nFirstIndex) const
	{
		return SubString(m_string, nFirstIndex, length()-1);
	}




	//////////////////////////////////////////////////////////////////////////
	//
	//@ Find
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_String::Find(char character) const
	{
		const char * begin = m_string;
		while (*begin && *begin != character)
			++begin;

		if (begin != m_string)
			return (u32)(begin - m_string);

		return -1;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Find
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_String::Find(C_String subStr) const
	{
		std::string strTmp(m_string);
		u32 index = (u32)strTmp.find(subStr);
		return index;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ FindLast
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_String::FindLast(char character) const
	{
		const char * end = m_string + length() - 1;
		while (*end && end != m_string && *end != character)
			--end;

		if (end != m_string)
			return (u32)(end - m_string);

		return -1;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Split
	//
	//////////////////////////////////////////////////////////////////////////
	void C_String::Split(char character, T_StringVector & outStrList) const
	{
		C_String copyStr = C_String(this->c_str());
		u32 index = copyStr.Find(character);
		while (index != -1)
		{
			C_String strTmp = copyStr.Left(index - 1);
			outStrList.push_back(strTmp);
			copyStr = C_String::SubString(copyStr, index + 1, copyStr.length());
			index = copyStr.Find(character);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Split
	//
	//////////////////////////////////////////////////////////////////////////
	void C_String::Split(C_String strSeparator, T_StringVector & outStrList) const
	{
		typedef boost::tokenizer<boost::char_separator<char> > T_Tokenizer;

		std::string str = m_string;
		boost::char_separator<char> separator(strSeparator);
		T_Tokenizer tok(str, separator);

		for (T_Tokenizer::iterator iter = tok.begin(); iter != tok.end(); ++iter)
		{
			outStrList.push_back(C_String(iter->c_str()));
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ TrimLeft
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_String::TrimLeft(char character) const
	{
		const char * c = m_string;
		u32 startId = 0;
		while (*c == character)
		{
			++c;
			++startId;
		}

		return C_String::SubString(*this, startId, length()-1);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ TrimRight
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_String::TrimRight(char character) const
	{
		const char * c = m_string + (length()-1);
		u32 id = 0;
		while (c >= m_string && *c == character)
		{
			--c;
			++id;
		}

		return C_String::SubString(*this, 0, length()-1-id);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ToLower
	//
	//////////////////////////////////////////////////////////////////////////
   
#ifdef PLATFORM_MACOSX
#include <algorithm>
#include <string>
//#include <cctype>
   
#endif   
	C_String C_String::ToLower() const
	{
#ifdef PLATFORM_WIN32
		C_String copy(this->c_str());
		C_StringInfoData * info = ((C_StringInfoData*)copy.m_string) - 1;
      
		STRLWR(copy.m_string, info->m_length);	//_strlwr_s doesn't use length without NULL term char!. it use length with NULL term char
      return copy;
      
#elif defined PLATFORM_MACOSX
      std::string s( m_string );	
      //http://www.codeguru.com/forum/showthread.php?threadid=489969
      //@ nezozralo std::tolower:( ambiguity
      std::transform( s.begin(),s.end(),s.begin(), ::tolower );
      
      C_String strTmp(s.c_str());
      return strTmp;
      
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ToUpper
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_String::ToUpper() const
	{
#ifdef PLATFORM_WIN32
		C_String copy(this->c_str());
		C_StringInfoData * info = ((C_StringInfoData*)copy.m_string) - 1;
		STRUPR(copy.m_string, info->m_length);
		return copy;
      
#elif defined PLATFORM_MACOSX
      std::string s( m_string );	
      //http://www.codeguru.com/forum/showthread.php?threadid=489969
      //@ nezozralo std::tolower:( ambiguity
      std::transform( s.begin(),s.end(),s.begin(), ::toupper );
      
      C_String strTmp(s.c_str());
      return strTmp;      
      
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Replace
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_String::replace(char oldChar, char newChar) const
	{
		C_String copy(this->c_str());

		char * ptr = copy.m_string;
		while (*ptr)
		{
			if (*ptr == oldChar)
				*ptr = newChar;
			++ptr;
		}
		return copy;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ ToInt
	//////////////////////////////////////////////////////////////////////////
	s32 C_String::ToInt() const
	{
		s32 retVal = 0;
		if (!C_XmlConvertorUtils::IntFromString(m_string, retVal))
		{
			TRACE_FE("Conversion from '%s' to Int failed!", m_string);
			return 0;
		}
			
		return retVal;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ ToHex
	//////////////////////////////////////////////////////////////////////////
#ifndef _HYBRID
	u32 C_String::ToHex() const
	{
		u32 retVal = 0;
		if (!C_XmlConvertorUtils::HexFromString(m_string, retVal))
		{
			TRACE_FE("Conversion from '%s' to Int failed!", m_string);
			return 0;
		}

		return retVal;
	}
#endif



	//////////////////////////////////////////////////////////////////////////
	//@ ToFloat
	//////////////////////////////////////////////////////////////////////////
	float C_String::ToFloat() const
	{
		float retVal = 0;
		if (!C_XmlConvertorUtils::FloatFromString(m_string, retVal))
		{
			TRACE_FE("Conversion from '%s' to Float failed!", m_string);
			return 0;
		}

		return retVal;
	}



	//////////////////////////////////////////////////////////////////////////
	//@ ToDouble
	//////////////////////////////////////////////////////////////////////////
	double C_String::ToDouble() const
	{
		double retVal = 0;
		if (!C_XmlConvertorUtils::DoubleFromString(m_string, retVal))
		{
			TRACE_FE("Conversion from '%s' to Double failed!", m_string);
			return 0;
		}

		return retVal;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ToBool
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_String::ToBool() const
	{
		bool retVal;
		if (!C_XmlConvertorUtils::BoolFromString(m_string, retVal))
		{
			TRACE_FE("Conversion from '%s' to Bool failed!", m_string)
			return false;
		}

		return retVal;
	}
}}