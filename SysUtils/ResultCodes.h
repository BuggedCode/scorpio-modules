/*
* filename:			ResultCodes.h
*
* author:			Kral Jozef
* date:				02/06/2008		22:27
* version:			1.00
* brief:
*/

#pragma once

namespace scorpio{ namespace sysutils{

	enum E_ResCode
	{
		E_RC_OK = 0,
		E_RC_ENUM_CANCEL,
		E_RC_ENUM_SKIPCHILDREN,
	};
}}