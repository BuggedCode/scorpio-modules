/*
* filename:			C_CommandLine.cpp
*
* author:			Kral Jozef
* date:				01/15/2011		10:33
* version:			1.00
* brief:
*/

#include "C_CommandLine.h"
#include "C_TraceClient.h"

namespace scorpio{ namespace sysutils{


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetCmdLine
	//
	//////////////////////////////////////////////////////////////////////////
	void C_CommandLine::SetCmdLine(const char * cmdLine)
	{
		m_cmdLine = cmdLine;
		T_StringVector cmdArgs;
		m_cmdLine.Split(" /", cmdArgs);

		for (T_StringVector::iterator iter = cmdArgs.begin(); iter != cmdArgs.end(); ++iter)
		{
			C_String arg = *iter;
			//@ split by :
			T_StringVector argVal;
			arg.Split(':', argVal);

			std::pair<T_StringMap::iterator, bool> res;
			if (argVal.size() == 2)
			{
				res = m_cmdArgs.insert(T_StringMap::value_type(argVal[0], argVal[1]));
			}
			else
			{
				res = m_cmdArgs.insert(T_StringMap::value_type(arg, ""));
			}
			if (!res.second)
			{
				TRACE_FE("Error in insert CmdLineArg: %s", arg.c_str());
			}
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ HasArgument
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_CommandLine::HasArg(C_String argName) const
	{
		T_StringMap::const_iterator iter = m_cmdArgs.find(argName);
		return (iter != m_cmdArgs.end());
	}


}}