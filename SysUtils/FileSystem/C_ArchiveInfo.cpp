/*
* filename:			C_ArchiveInfo.cpp
*
* author:			Kral Jozef
* date:				06/19/2011		22:38
* version:			1.00
* brief:
*/

#include "C_ArchiveInfo.h"
#include <SysUtils/C_TraceClient.h>

namespace scorpio{ namespace sysutils{

	const unzFile C_ArchiveInfo::C_INVALID_ARCHIVE_HANDLE = (unzFile)0xffffffffffffffff;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddFileInfo
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ArchiveInfo::AddFileInfo(const C_HashName & fileHashName)
	{
		std::pair<T_FileContainer::iterator, bool> pairib = m_FileMap.insert(T_FileContainer::value_type(fileHashName));
		if (!pairib.second)
		{
			TRACE_FE("Same file: %s already in archive", fileHashName.GetName());
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetFileInfo
	//
	//////////////////////////////////////////////////////////////////////////
	unzFile C_ArchiveInfo::GetFileInfo(const C_HashName & fileHashName) const
	{
		T_FileContainer::const_iterator iter = m_FileMap.find(fileHashName);
		if (iter != m_FileMap.end())
			return m_Archive;

		return C_INVALID_ARCHIVE_HANDLE;
	}
}}
