/*
* filename:			C_MemoryStream.cpp
*
* author:			Kral Jozef
* date:				06/25/2011		13:48
* version:			1.00
* brief:
*/

#include "C_MemoryStream.h"
//#include <boost/iostreams/stream.hpp>
//#include <boost/iostreams/stream_buffer.hpp>

#pragma warning(push)
#pragma warning(disable:4996)	//doesn't work on VC10compiler (bug inside VC 100 CRT)

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Write
	//
	//////////////////////////////////////////////////////////////////////////
	void C_MemoryStream::Write(const unsigned char * bufferFrom, u32 nCount)
	{
		if (m_Buffer.capacity() < (m_Buffer.size() + nCount))
			m_Buffer.reserve(m_Buffer.size() + 2*nCount);

		m_Buffer.insert(m_Buffer.end(), bufferFrom, bufferFrom+nCount);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Read
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_MemoryStream::Read(unsigned char * bufferOut, u32 maxCount)
	{
		if (!IsOpen())
			return 0;

		u32 copyLen = (maxCount < (u32)m_Buffer.size()) ? maxCount : (u32)m_Buffer.size();
		T_BufferType::iterator iter = m_Buffer.begin();
		advance(iter, copyLen);
		std::copy(m_Buffer.begin(), iter, bufferOut);
		return copyLen;
	}

   //////////////////////////////////////////////////////////////////////////
   //
   //@ ReadLine
   //
   //////////////////////////////////////////////////////////////////////////
   void C_MemoryStream::ReadLine(char * buffer, u32 bufferSize)
   {
      if (!IsOpen())
         return;

		T_BufferType::iterator begin = m_Buffer.begin();
		std::advance(begin, m_CurrPos);

      for (T_BufferType::iterator iter = begin; iter != m_Buffer.end(); ++iter)
      {
			++m_CurrPos;
         if (*iter == '\n')
         {
            std::copy(begin, iter, buffer);
            return;
         }
      }
   }
}}

#pragma warning(pop)