/*
* filename:			C_Stream.h
*
* author:			Kral Jozef
* date:				06/20/2011		22:24
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/C_String.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Stream - unexported class
	class C_Stream
	{
	public:
		//@ c-tor
		C_Stream() {};
		//@ c-tor
		C_Stream(const char * fileName) : m_FileName(fileName) {};
		//@ d-tor
		virtual ~C_Stream() {};
		//@ Write
		virtual void		Write(const unsigned char * bufferFrom, u32 nCount) = 0;
		//@ Read
		virtual u32			Read(unsigned char * bufferOut, u32 maxCount) = 0;
		//@ GetFileName
		const char *		GetFileName() const { return m_FileName.c_str(); }
		//@ GetBuffer :( !JK
		virtual const unsigned char	*	GetBuffer() const { return NULL; }
		//@ GetBufferLength
		virtual u32			GetBufferLength() const { return 0; }
		//@ IsOpen
		virtual bool		IsOpen() const { return true; }
		//@ ReadLine
		virtual void		ReadLine(char * buffer, u32 bufferSize) = 0;
		//EndOfStream
		virtual bool		EndOfStream() const = 0;

	private:
		T_String			m_FileName;	//for trace debug
	};

	DECLARE_SHARED_PTR(C_Stream, T_Stream);
}}
