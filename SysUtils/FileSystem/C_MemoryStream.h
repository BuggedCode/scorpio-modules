/*
* filename:			C_MemoryStream.h
*
* author:			Kral Jozef
* date:				06/20/2011		22:26
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include "C_Stream.h"
#include <vector>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_MemoryStream
	class SYSUTILS_API C_MemoryStream : public C_Stream
	{
	public:
		//@ c-tor
		//@ c-tor
		C_MemoryStream(const char * fileName) : C_Stream(fileName), m_CurrPos(0) {};
		//@ Write
		//@ bufferFrom - The buffer to write data from
		//@ nCount - The maximum number of bytes to write
		virtual void		Write(const unsigned char * bufferFrom, u32 nCount);
		//@ Read
		//@ maxCount - The maximum number of bytes to read.
		virtual u32			Read(unsigned char * bufferOut, u32 maxCount);
		//@ GetBuffer
		virtual const unsigned char	*	GetBuffer() const { return &(*m_Buffer.begin()); }
		//@ GetBufferLength
		virtual u32			GetBufferLength() const { return (u32)m_Buffer.size(); }
      //@ IsOpen
      virtual bool      IsOpen() const { return !m_Buffer.empty(); }
      //@ ReadLine
      virtual void      ReadLine(char * buffer, u32 bufferSize);
      //EndOfStream
      virtual bool      EndOfStream() const { return m_CurrPos == (u32)m_Buffer.size(); }


	private:
		typedef std::vector<unsigned char> T_BufferType;
		T_BufferType		m_Buffer;
		u32					m_CurrPos;	//curr pos in memory stream (for loading like form file)
	};
}}
