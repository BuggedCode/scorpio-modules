/*
* filename:			C_DiskFileSystem.h
*
* author:			Kral Jozef
* date:				8/31/2011		9:50
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace sysutils{

   //////////////////////////////////////////////////////////////////////////
   //@ C_DiskFileSystem
   class SYSUTILS_API C_DiskFileSystem
   {
   public:
      //@ fileName - relative path from archive
      T_Stream			Open(const char * fileName);

   private:
   };
}};