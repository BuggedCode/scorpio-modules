/*
* filename:			C_ArchiveInfo.h
*
* author:			Kral Jozef
* date:				06/19/2011		22:30
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/C_HashName.h>
#include <set>
#include <zlib/contrib/minizip/unzip.h>

namespace scorpio{ namespace sysutils{

	class C_ArchiveInfo
	{
	public:
		//@ c-tor
		C_ArchiveInfo(const C_HashName & archiveName, unzFile archiveHandle, const T_String & password)
			: m_ArchiveName(archiveName), m_Archive(archiveHandle), m_Password(password) {}

		//@ operator ==
		bool operator == (T_Hash32 hash) const
		{
			return hash == m_ArchiveName.GetHash();
		}


		//@ AddFile
		void				AddFileInfo(const C_HashName & fileHashName);
		//@ GetFileInfo
		unzFile				GetFileInfo(const C_HashName & fileHashName) const;
		//@ GetPassword
		const T_String	&	GetPassword() const { return m_Password; }
      //@ GetArchiveHandle
      unzFile				GetArchiveHandle() const { return m_Archive; }

	public:
		static const unzFile C_INVALID_ARCHIVE_HANDLE;

	private:
		C_HashName	m_ArchiveName;
		unzFile		m_Archive;
		typedef std::set<C_HashName>	T_FileContainer;
		T_FileContainer	m_FileMap;
		T_String		m_Password;
	};


	DECLARE_SHARED_PTR(C_ArchiveInfo, T_ArchiveInfo);
}}
