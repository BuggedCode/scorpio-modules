/*
* filename:			C_FileStream.cpp
*
* author:			Kral Jozef
* date:				8/31/2011		10:10
* version:			1.00
* brief:
*/

#include "C_FileStream.h"
#include <SysUtils/C_TraceClient.h>

#pragma warning(push)
#pragma warning(disable:4996)		//-D_SCL_SECURE_NO_WARNINGS
//@ coz using of m_file.readsome(buffer, BUFFER_SIZE); which give warnings in MSVC 2005 but secure version is not in MSVC 2010:( //portability

namespace scorpio{ namespace sysutils{

   //////////////////////////////////////////////////////////////////////////
   //
   //@ c-tor
   //
   //////////////////////////////////////////////////////////////////////////
   C_FileStream::C_FileStream(const char * fileName)
      : C_Stream(fileName)
   {
      m_file.open(fileName, std::ios_base::in);
      if (!m_file.good())
         TRACE_FE("Could not open file %s", fileName);
   }

   //////////////////////////////////////////////////////////////////////////
   //
   //@ d-tor
   //
   //////////////////////////////////////////////////////////////////////////
   C_FileStream::~C_FileStream()
   {
      if (m_file.is_open())
         m_file.close();
   }

   //////////////////////////////////////////////////////////////////////////
   //
   //@ Create
   //
   //////////////////////////////////////////////////////////////////////////
   T_Stream  C_FileStream::Create(const char * fileName)
   {
      //@ if exist file on disk
		T_Stream stream = T_Stream(new C_FileStream(fileName));
		if (stream->IsOpen())
			return stream;

      return T_Stream();
   }


   //////////////////////////////////////////////////////////////////////////
   //
   //@ Read
   //
   //////////////////////////////////////////////////////////////////////////
   u32 C_FileStream::Read(unsigned char * bufferOut, u32 maxCount)
   {
      m_file.read((char*)bufferOut, maxCount);
      return (u32)m_file.gcount();
   }


   //////////////////////////////////////////////////////////////////////////
   //
   //@ ReadLine
   //
   //////////////////////////////////////////////////////////////////////////
   void C_FileStream::ReadLine(char * buffer, u32 bufferSize)
   {
      m_file.getline(buffer, bufferSize);
   }

   //////////////////////////////////////////////////////////////////////////
   //
   //@ EndOfStream
   //
   //////////////////////////////////////////////////////////////////////////
   bool C_FileStream::EndOfStream() const
   {
      return m_file.eof();
   }
}}

#pragma warning(pop)