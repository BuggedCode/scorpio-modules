/*
* filename:			C_FileStream.h
*
* author:			Kral Jozef
* date:				8/31/2011		10:08
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include "C_Stream.h"
#include <fstream>
#include <Common/Assert.h>

namespace scorpio{ namespace sysutils{

   //////////////////////////////////////////////////////////////////////////
   //@ C_FileStream
   class SYSUTILS_API C_FileStream : public C_Stream
   {
   public:
      //@ c--tor
      C_FileStream(const char * fileName);
      //@ d-tor
      ~C_FileStream();

      //@ Create stream
      static T_Stream	Create(const char * fileName);

      //@ Write
      virtual void		Write(const unsigned char * bufferFrom, u32 nCount) { SC_ASSERT(false); }
      //@ Read
      virtual u32		Read(unsigned char * bufferOut, u32 maxCount);
      //@ IsOpen
      virtual bool      IsOpen() const { return m_file.is_open(); }
      //@ ReadLine
      virtual void      ReadLine(char * buffer, u32 bufferSize);
      //EndOfStream
      virtual bool      EndOfStream() const;

   private:
      std::ifstream	m_file;
   };

}};