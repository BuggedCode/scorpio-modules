/*
* filename:			C_FileSystemManager.h
*
* author:			Kral Jozef
* date:				06/19/2011		21:00
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <Common/C_Singleton.h>
#include "C_ZipFileSystem.h"
#include "C_DiskFileSystem.h"
#include "C_Stream.h"

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_FileSystem
	class SYSUTILS_API C_FileSystemManager
	{
		friend class C_Singleton<C_FileSystemManager>;
	public:


		//@ OpenArchive
		bool				OpenArchive(const char * fileName, const char * password = NULL) { return m_ZipFileSystem.OpenArchive(fileName, password); }
		//@ CloseArchive
		bool				CloseArchive(const char * fileName) { return m_ZipFileSystem.CloseArchive(fileName); }
		//@ Open
		//@ fileName - relative path
		T_Stream			Open(const char * fileName);

	private:
		//@ c-tor
		C_FileSystemManager() {};
		//@ d-tor
		~C_FileSystemManager() {};


		C_ZipFileSystem		m_ZipFileSystem;
		C_DiskFileSystem	m_DiskFileSystem;
	};

	typedef C_Singleton<C_FileSystemManager>	T_FileSystemManager;
}}

SYSUTILS_EXP_TEMP_INST template class SYSUTILS_API C_Singleton<scorpio::sysutils::C_FileSystemManager>;