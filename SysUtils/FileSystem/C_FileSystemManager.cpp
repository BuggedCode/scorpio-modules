/*
* filename:			C_FileSystemManager.cpp
*
* author:			Kral Jozef
* date:				06/19/2011		21:40
* version:			1.00
* brief:
*/

#include "C_FileSystemManager.h"
#include <SysUtils/Utils/C_StringUtils.h>
//#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace sysutils{


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Open
	//
	//////////////////////////////////////////////////////////////////////////
	T_Stream C_FileSystemManager::Open(const char * fileName)
	{
		//@ check if file is in archiveFileSystem
		T_String str(fileName);
		str = C_StringUtils::Replace(str, '\\', '/');

		T_Stream memStream = m_ZipFileSystem.Open(str.c_str());
		if (memStream)
			return memStream;

      //@ if file is not in memory in zipFileSystem try to search in DiskFileSystem
      return m_DiskFileSystem.Open(fileName);
	}

}}
