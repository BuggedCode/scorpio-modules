/*
* filename:			C_ZipFileSystem.h
*
* author:			Kral Jozef
* date:				06/19/2011		21:35
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/C_String.h>
#include <SysUtils/C_HashName.h>
#include <map>
#include <zlib/contrib/minizip/unzip.h>
#include "C_ArchiveInfo.h"
#include "C_MemoryStream.h"

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ZipFileSystem
	class SYSUTILS_API C_ZipFileSystem
	{
		friend class C_FileSystemManager;

	public:
		//@ OpenArchive
		//@ fileName - absolute path - use MAKE_PATH
		bool				OpenArchive(const char * fileName, const char * password);
		//@ CloseArchive
		//@ fileName - absolute path - use MAKE_PATH
		bool				CloseArchive(const char * fileName);
		//@ Open
		//@ fileName - relative path from archive
		T_Stream			Open(const char * fileName);


	private:
		//@ ExtractFileIntoMemory
		//@ fileName - relative path from archive
		T_Stream			ExtractFileIntoMemory(unzFile zipFileHandle, const char * fileName, const char * password);


	private:
		//@ c-tor
		C_ZipFileSystem() {};
		//@ d-tor
		~C_ZipFileSystem() {};


		typedef std::vector<T_ArchiveInfo>	T_ArchiveInfoList;
		T_ArchiveInfoList	m_ArchiveList;
	};
}}
