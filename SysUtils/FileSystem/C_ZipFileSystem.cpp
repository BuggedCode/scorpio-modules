/*
* filename:			C_ZipFileSystem.cpp
*
* author:			Kral Jozef
* date:				06/19/2011		21:35
* version:			1.00
* brief:
*/


#include "C_ZipFileSystem.h"
#include <SysUtils/Utils/C_StringUtils.h>
#include <SysUtils/C_TraceClient.h>
#include <algorithm>

namespace scorpio{ namespace sysutils{

	static const u32 C_CASESENSITIVITY = 0;
	static const u32 C_BUFFER_SIZE = 8132;



	//////////////////////////////////////////////////////////////////////////
	//
	//@ OpenArchive
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ZipFileSystem::OpenArchive(const char * fileName, const char * password)
	{
		unz_global_info unzGInfo;
		unzFile unzFile_ = unzOpen(fileName/*, &ffunc*/);
		s32 err = unzGetGlobalInfo(unzFile_, &unzGInfo);

		if (err != UNZ_OK)
			return false;

		T_ArchiveInfo archInfo = T_ArchiveInfo(new C_ArchiveInfo(C_HashName(fileName), unzFile_, T_String(password)));

		bool bRes = true;
		for (u32 i = 0; i < unzGInfo.number_entry; ++i)
		{
			char filename_inzip[256];
			unz_file_info fileInfo;

			err = unzGetCurrentFileInfo(unzFile_, &fileInfo, filename_inzip, sizeof(filename_inzip), NULL, 0, NULL, 0);
			if (err != UNZ_OK)
				continue;

			//int compFunc = fileInfo.compression_method;


			T_String ext = C_StringUtils::GetExtension(filename_inzip);
			if (!ext.empty())
			{
				//@ not directory

				archInfo->AddFileInfo(C_HashName(filename_inzip));
			}


			if (i < unzGInfo.number_entry - 1)
				err = unzGoToNextFile(unzFile_);

			if (err != UNZ_OK)
			{
				bRes = false;
				break;	//err
			}
		}

		if (bRes)
			m_ArchiveList.push_back(archInfo);

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CloseArchive
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ZipFileSystem::CloseArchive(const char * fileName)
	{
		class C_Predicat
		{
		public:
			C_Predicat(const char * fileName)
			{
				m_HashName = C_HashName(fileName);
			}

			//@ operator ()
			bool operator()(const T_ArchiveInfo archInfo) const
			{
				return (*archInfo == m_HashName.GetHash());
			}

		private:
			C_HashName m_HashName;


		};

		C_Predicat pred(fileName);
		T_ArchiveInfoList::iterator iter = std::find_if(m_ArchiveList.begin(), m_ArchiveList.end(), pred);
		if (iter != m_ArchiveList.end())
		{
			/*unzCloseCurrentFile((*iter)->GetArchiveHandle());*/
			m_ArchiveList.erase(iter);
			return true;
		}

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Open
	//
	//////////////////////////////////////////////////////////////////////////
	T_Stream C_ZipFileSystem::Open(const char * fileName)
	{
		if (!fileName)
			return T_Stream();

		unzFile zipFileHandle = C_ArchiveInfo::C_INVALID_ARCHIVE_HANDLE;
		const char * password = NULL;
		T_String strFileName = fileName;
		strFileName = C_StringUtils::TrimFromLeft(strFileName, '/');
		strFileName = C_StringUtils::TrimFromLeft(strFileName, '\\');

		for (T_ArchiveInfoList::iterator iter = m_ArchiveList.begin(); iter != m_ArchiveList.end(); ++iter)
		{
			T_ArchiveInfo archInfo = *iter;
			if (!archInfo)
				continue;

			zipFileHandle = archInfo->GetFileInfo(C_HashName(strFileName.c_str()));
			if (zipFileHandle != C_ArchiveInfo::C_INVALID_ARCHIVE_HANDLE)
			{
				T_String pass = archInfo->GetPassword();
				if (!pass.empty())
					password = pass.c_str();
				break;
			}
		}

		if (zipFileHandle == C_ArchiveInfo::C_INVALID_ARCHIVE_HANDLE)
			return T_Stream();

		//@ extract file
		T_Stream memStream = ExtractFileIntoMemory(zipFileHandle, strFileName.c_str(), password);
		return memStream;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ExtractFileIntoMemory
	//
	//////////////////////////////////////////////////////////////////////////
	T_Stream C_ZipFileSystem::ExtractFileIntoMemory(unzFile zipFileHandle, const char * fileName, const char * password)
	{
		if (unzLocateFile(zipFileHandle, fileName, C_CASESENSITIVITY) != UNZ_OK)
		{
			TRACE_FE("Can not locate file %s in zip package", fileName);
			return T_Stream();
		}

		unz_file_info fileInfo;
		char filename_inzip[256];
		s32 err = unzGetCurrentFileInfo(zipFileHandle, &fileInfo, filename_inzip, sizeof(filename_inzip), NULL, 0, NULL, 0);
		if (err != UNZ_OK)
			return T_Stream();

		err = unzOpenCurrentFilePassword(zipFileHandle, password);
		if (err != UNZ_OK)
			return T_Stream();

		T_Stream newMemStream = T_Stream(new C_MemoryStream(fileName));
		unsigned char buffer[C_BUFFER_SIZE];
		do 
		{
			err = unzReadCurrentFile(zipFileHandle, buffer, C_BUFFER_SIZE);
			if (err > 0)
			{
				//@ store bytes into stream / overhead coping data into intermediate buffer
				newMemStream->Write(buffer, err);
			}
			else if (err < 0)
			{
				TRACE_FE("Error when extracting: %s", fileName);
				newMemStream.reset();
				unzCloseCurrentFile(zipFileHandle);
				break;
			}
		} while(err > 0);
		
		//SAFE_DELETE(buffer);

		unzCloseCurrentFile(zipFileHandle);

		return newMemStream;
	}

}}
