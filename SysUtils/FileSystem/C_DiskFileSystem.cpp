/*
* filename:			C_DiskFileSystem.cpp
*
* author:			Kral Jozef
* date:				8/31/2011		9:59
* version:			1.00
* brief:
*/

#include "C_DiskFileSystem.h"
#include <Common/GlobalDefines.h>
#include <SysUtils/FileSystem/C_FileStream.h>
#ifdef USE_MOUNT_POINT
	#include <SysUtils/C_MountConfig.h>
#else
	#include <SysUtils/Utils/C_FileSystemUtils.h>
#endif

namespace scorpio{ namespace sysutils{

   //////////////////////////////////////////////////////////////////////////
   //
   //@ Open
   //
   //////////////////////////////////////////////////////////////////////////
   T_Stream C_DiskFileSystem::Open(const char * fileName)
   {
		//@ replace mount point with absolute Path
#ifdef USE_MOUNT_POINT
	   T_String absPath = T_MountConfig::GetInstance().RemapToAbsolute(fileName);
#else
		T_String absPath = C_FileSystemUtils::MakeFilePath(fileName);
#endif
      T_Stream stream = C_FileStream::Create(absPath.c_str());
      return stream;
   }
}}
