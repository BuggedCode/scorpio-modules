/*
* filename:			TraceClient.h
*
* author:			Petr Slivon
* modify:			Jozef Kral
* date:				07/26/2008		12:33
* version:			2.00
* brief:				Trace client
*/

#include <stdio.h>

#include "C_TraceClient.h"
#include <stdarg.h>
#include <Common/macros.h>

#ifdef PLATFORM_WIN32
	#include <Windows.h>
	#include <ShellAPI.h>

#define MAX_TRACE_BUFFER_SIZE		65536

namespace scorpio{

	//////////////////////////////////////////////////////////////////////////
	//@ runServer
	void C_TraceClient::runServer(const char* serverPath)
	{
		//	m_hPipe = NULL;
		int tryCnt = 0;
		DWORD error;
		HANDLE hMutex;
		// test zda server jiz nebezi
		hMutex = CreateMutex(NULL, TRUE, "single_instance_trace_server_mutex");
		if (hMutex) 
			CloseHandle(hMutex);

		error=GetLastError();

		// server nebezi a mam zadanou cestu kde je-> zkusim ho spustit
		if ((error!=ERROR_ALREADY_EXISTS) /*&& (serverPath!=NULL)*/) 
		{
			// zkusim server pustit
			HINSTANCE hInst = ShellExecute(NULL, "open", serverPath , NULL, NULL, SW_SHOWNORMAL);
			if ((*(DWORD*)&hInst)<=32) 
			{
				MessageBox(NULL,"Can't launch Trace Server!","Warning",MB_OK|MB_ICONEXCLAMATION);
			}
			else
			{
				// pockam az nabehne traceserver...(max. 5s);
				do
				{
					hMutex = CreateMutex(NULL,TRUE,"single_instance_trace_server_mutex");
					error = GetLastError();
					if (hMutex) 
						CloseHandle(hMutex);
					if (!error || error != ERROR_ALREADY_EXISTS) 
					{
						tryCnt++;
						Sleep(50);
					}
					else 
						break;
				}while(tryCnt<100);

				if (tryCnt>=100) 
				{
					// traceserver je vypnut;
					//				m_hPipe = NULL; 
					return;
				}
			}
		}

		C_TraceClient::sendTrace(E_TMT_CLEAR_LIST,0,0,"");
	}



	//////////////////////////////////////////////////////////////////////////
	//@ sendTrace
	void C_TraceClient::sendTrace(const E_TraceMsgType traceMsgType, const char* file, u32 line, const char* format, ...)
	{
		int size = sizeof(E_TraceMsgType)+sizeof(int)+sizeof(int);

		va_list list;
		va_start( list, format );
		int vaLength = _vscprintf( format, list );

		if (format) 
			size += vaLength + 1;

		if (file) 
			size += (int)(strlen(file)+1);

		char buffer[MAX_TRACE_BUFFER_SIZE];
		char * ptr = buffer;

		(*(int*)ptr)=size;
		SC_ASSERT(size < MAX_TRACE_BUFFER_SIZE);
		ptr += sizeof(int);

		(*(E_TraceMsgType*)ptr)=traceMsgType;
		ptr += sizeof(E_TraceMsgType);

		(*(int*)ptr)=line;
		ptr += sizeof(int);

		if (format) 
		{
			vsprintf_s(ptr, vaLength + 1, format, list);
			//strcpy_s(ptr, strlen(format) * sizeof(char), format);
			ptr += strlen(ptr) + 1;
		}

		va_end(list);

		if (file)
			strcpy_s(ptr, strlen(file) + 1, file);

		HWND hWnd = FindWindow("#32770","TraceServer");
		COPYDATASTRUCT myCD;
		myCD.dwData = 0;
		myCD.cbData = size;
		myCD.lpData = buffer;
		SendMessage( hWnd, WM_COPYDATA, NULL, (LPARAM) (LPVOID) &myCD );
	}
}

#elif defined PLATFORM_MACOSX

namespace scorpio
{
   //@ runServer
   void C_TraceClient::runServer(const char* serverPath) {};

   //////////////////////////////////////////////////////////////////////////
   //
   //@ sendTrace
   //
   //////////////////////////////////////////////////////////////////////////
   void C_TraceClient::sendTrace(const E_TraceMsgType traceMsgType, const char* file, u32 line, const char* format, ...)
   {
      char bufferType[100];
      if(traceMsgType == E_TMT_INFO)
         sprintf(bufferType, "%s", "INFO:");
      else if (traceMsgType == E_TMT_WARNING)
         sprintf(bufferType, "%s", "WARNING:");
      else if (traceMsgType == E_TMT_ERROR)
         sprintf(bufferType, "%s", "ERROR:");
      
		va_list	arglist;
		va_start(arglist, format);
		char buffer[1024];
		VSNPRINTF(buffer, 1024, format, arglist);
		va_end(arglist);
      
      printf("%s", bufferType);
      printf("%s\n", buffer);
   }
      
}
#endif
