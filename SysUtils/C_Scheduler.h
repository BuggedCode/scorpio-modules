/*
* filename:			C_Scheduler.h
*
* author:			Kral Jozef
* date:				05/20/2009		21:32
* version:			1.00
* brief:				Scheduler - planuju sa don eventy (metody kontextu), po dovrseni casu sa podla priority volaju a vyhazuju zo schedulera
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <Common/C_Singleton.h>
#include <map>
#include <SysUtils/I_CallbackPointer.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Scheduler
	class C_Scheduler
	{
		friend class C_Singleton<C_Scheduler>;

		template<class T>
		class C_EventHandler : public I_CallbackPointer
		{
			typedef void (T::*T_MemberFunc)();
		public:
			//@ c-tor
			C_EventHandler(T * obj, T_MemberFunc func) : m_Object(obj), m_Function(func) {};
			//@ Execute
			virtual void		Execute()
			{
				SC_ASSERT(m_Object);
				(m_Object->*m_Function)();
			}
			//@ IsOwnedByContext
			virtual bool		IsOwnedByContext(T_ContextDescriptor contextDesc)
			{
				return (m_Object == (T*)(contextDesc->m_Context));
			}

		private:
			T				*	m_Object;
			T_MemberFunc	m_Function;
		};


	public:
		//@ c-tor
		C_Scheduler() : m_currentTick(0) {};
		//@ d-tor
		~C_Scheduler() { m_EventHandlerMap.clear(); }

		//@ Schedule
		//@ obj - instance of context
		//@ func - pointer to member method of class T
		//@ dellayTime in milliseconds - after dellayTime from now will be funcPtr called
		template<class T, typename Func>
		void				Schedule(T * obj, Func func, u32 dellayTime)
		{
			u64 callingTime = m_currentTick + dellayTime;
			sysutils::T_CallbackPtr eventHandler = sysutils::T_CallbackPtr(new C_EventHandler<T>(obj, func));

			m_EventHandlerMap.insert(T_EventHandlerMap::value_type(callingTime, eventHandler));
		}

		//@ RemoveTasks
		//@ Remove all scheduled jobs for context T
		template<class T>
		void				RemoveTasks(T * context)
		{
			sysutils::T_ContextDescriptor contextDesc = T_ContextDescriptor(new sysutils::I_ContextDescriptor(context));
			bool bSearchAgain = false;

			do
			{
				T_EventHandlerMap::iterator iterForContext = m_EventHandlerMap.end();
				for (T_EventHandlerMap::iterator iter = m_EventHandlerMap.begin(); iter != m_EventHandlerMap.end(); ++iter)
				{

					sysutils::T_CallbackPtr eventHandler = iter->second;
					if (eventHandler->IsOwnedByContext(contextDesc))
					{
						iterForContext = iter;
						break;
					}
				}

				if (iterForContext != m_EventHandlerMap.end())
				{
					m_EventHandlerMap.erase(iterForContext);
					bSearchAgain = true;
				}
				else
				{
					bSearchAgain = false;
				}

			} while (bSearchAgain);
		}


		//@ ClearAll
		u32				ClearAll()
		{
			u32 nCount = (u32)m_EventHandlerMap.size();
			m_EventHandlerMap.clear();
			return nCount;
		}

		//@ Update
		//@ param - tickDelta in milliseconds
		void				Update(u32 tickDelta)
		{
			m_currentTick += tickDelta;

			if (m_EventHandlerMap.empty())
				return;

			T_EventHandlerMap::iterator iter = m_EventHandlerMap.begin();
			while (iter != m_EventHandlerMap.end())
			{
				//@ ak aktualny cas este nedosiahol najblizsie naplanovanu ulohu tak von
				if (iter->first > m_currentTick)
					return;

				T_CallbackPtr eventFunc = iter->second;
				
				//@ call event
				eventFunc->Execute();

				T_EventHandlerMap::iterator tmpIter = iter;
				
				++iter;
				m_EventHandlerMap.erase(tmpIter);	//return from erase 'bidirect iterator beyond' is not in standard
			}
		}

	private:
		u64					m_currentTick;
		typedef std::multimap<u64, sysutils::T_CallbackPtr>	T_EventHandlerMap;
		T_EventHandlerMap	m_EventHandlerMap;
	};


	typedef C_Singleton<C_Scheduler>	T_Scheduler;

}}

SYSUTILS_EXP_TEMP_INST template class SYSUTILS_API C_Singleton<scorpio::sysutils::C_Scheduler>;