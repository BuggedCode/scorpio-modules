/*
* filename:			C_StringUtils.h
*
* author:			Kral Jozef aka BuggedCode
* date:				11:10:2012		10:33
* version:			1.00
* brief:
*/


#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/C_String.h>
#include <Common/Types.h>
#include <Common/STLTypes.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_StringUtils
	class SYSUTILS_API C_StringUtils
	{
	public:
		//@ ExtractFileNameFromPath
		static T_String		ExtractFileNameFromPath(const char * path, bool withExtension = false);
		//@ TrimExtension
		static T_String		TrimExtension(const char * fileName);
		//@ GetExtensio
		static T_String		GetExtension(const T_String & inStrValue);
		//@ StringToInt
		static bool			StringToInt(const T_String & value, s32 & outValue);
		//@ IntToString
		static std::string	IntToString(const u32 & inValue);
		//@ StringToFloat
		static bool			StringToFloat(const T_String & value, float & outValue);
		//@ FormatString
		static std::string	FormatString(const char* format, ...);
		//@ TrimFromLeft
		static T_String		TrimFromLeft(const T_String & inText, char character);
		//@ TrimFromRight
		static T_String		TrimFromRight(const T_String & inText, char character);
		//@ TrimFromCharacter
		static T_String		TrimFromCharacter(const T_String & inText, char character);
		//@ Split
		static void			Split(const T_String & inStrText, char token, T_StringVector & strVectOut);
		//@ FindLast
		static u32			FindLast(const T_String & inStrValue, char character);
		//@ Replace
		static T_String		Replace(const T_String & inStrValue, char oldChar, char newChar);
		//@ ToLower
		static T_String		ToLower(const T_String & inString);

	public:
		static const T_String C_EMPTY_STD_STRING;

	private:
		//@ non implemented
		C_StringUtils();
		C_StringUtils(const C_StringUtils & copy);
	};

	//typedef C_StringUtils	C_Path;

}}