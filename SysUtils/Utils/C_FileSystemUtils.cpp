/*
* filename:			C_FileSystemUtils.cpp
*
* author:			Kral Jozef
* date:				28/8/2011		10:58
* version:			1.00
* brief:
*/

#include "C_FileSystemUtils.h"

#ifdef PLATFORM_WIN32
   #include <stdlib.h>
   #include <Windows.h>
#endif

#include <Common/macros.h>

namespace scorpio{ namespace sysutils{


   char C_FileSystemUtils::m_AppDir[SC_MAX_PATH];
   char C_FileSystemUtils::m_TempBuffer[SC_MAX_PATH];


   //////////////////////////////////////////////////////////////////////////
   //
   //@ Initialize
   //
   //////////////////////////////////////////////////////////////////////////
   bool C_FileSystemUtils::Initialize()
   {
#ifdef PLATFORM_WIN32
      ::GetModuleFileName(NULL, m_AppDir, _MAX_PATH);
      sysutils::C_String str(m_AppDir);
      u32 nInd = str.FindLast('\\');
      if (nInd != -1)
      {
         m_AppDir[nInd+1] = 0;
      }
#else
      getcwd(m_AppDir, SC_MAX_PATH);
      u32 len = strlen(m_AppDir);
      m_AppDir[len] = '\\';
      m_AppDir[len+1] = 0;
#endif

      return true;
   }



   //////////////////////////////////////////////////////////////////////////
   //
   //@ MakeFilePath
   //
   //////////////////////////////////////////////////////////////////////////
   const char * C_FileSystemUtils::MakeFilePath(const char * fileName)
   {
      if (fileName[0] == '\\' || fileName[0] == '/')
         ++fileName;

		SPRINTF(m_TempBuffer, SC_MAX_PATH, "%s%s", m_AppDir, fileName);
      
#ifdef PLATFORM_MACOSX
      //@ switch \\ => /
      C_String str(m_TempBuffer);
      str = str.Replace('\\', '/');
      SPRINTF(m_TempBuffer, SC_MAX_PATH, "%s", str.c_str());
#endif
      return m_TempBuffer;
   }


#ifdef PLATFORM_WIN32
   //////////////////////////////////////////////////////////////////////////
   //
   //@ MakeRelativePath
   //
   //////////////////////////////////////////////////////////////////////////
   C_String C_FileSystemUtils::MakeRelativePath(const char * absolutePath)
   {
      C_String retPath(absolutePath);
      retPath = retPath.Right((u32)strlen(m_AppDir));
      return retPath;
   }
#endif

}}
