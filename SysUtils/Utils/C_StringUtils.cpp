/*
* filename:			C_StringUtils.cpp
*
* author:			Kral Jozef aka BuggedCode
* date:				11:10:2012		10:33
* version:			1.00
* brief:
*/

#include "C_StringUtils.h"
#include <stdarg.h>
#include <Common/Macros.h>
#include <Common/Assert.h>
#include <sstream>
#include <algorithm>
#include <SysUtils/Xml/C_XmlConvertorUtils.h>

const T_String scorpio::sysutils::C_StringUtils::C_EMPTY_STD_STRING = "";

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ExtractFileName
	//
	//////////////////////////////////////////////////////////////////////////
	T_String C_StringUtils::ExtractFileNameFromPath(const char * path, bool withExtension)
	{
#ifdef USE_STD_STRING
		T_String strTmp(path);
		s32 id = strTmp.rfind('/');
		if (id == -1)
			id = strTmp.rfind('\\');

		strTmp = strTmp.substr(id + 1, strTmp.length() - (id + 1) - 1);

		if (!withExtension)
		{
			id = strTmp.find('.');
			strTmp = strTmp.substr(0, id-1);
		}
#else
		//@ Extract name from path
		T_String strTmp(path);
		s32 id = strTmp.FindLast('/');
		if (id == -1)
			id = strTmp.FindLast('\\');

		strTmp = strTmp.Right(id+1);

		if (!withExtension)
		{
			id = strTmp.Find('.');
			strTmp = strTmp.Left(id-1);
		}
#endif

		return strTmp;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TrimExtension
	//
	//////////////////////////////////////////////////////////////////////////
	T_String C_StringUtils::TrimExtension(const char * fileName)
	{
#ifdef USE_STD_STRING
		//@ Extract name from path
		T_String strTmp(fileName);
		s32 id = strTmp.rfind('.');
		if (id == -1)
			return strTmp;

		if (id == 0)
			return C_EMPTY_STD_STRING;

		strTmp = strTmp.substr(0, id - 1);
#else
		//@ Extract name from path
		T_String strTmp(fileName);
		s32 id = strTmp.FindLast('.');
		if (id == -1)
			return strTmp;

		if (id == 0)
			return C_EMPTY_STD_STRING;

		strTmp = strTmp.Left(id-1);
#endif
		return strTmp;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetExtension
	//
	//////////////////////////////////////////////////////////////////////////
	T_String C_StringUtils::GetExtension(const T_String & inStrValue)
	{
		//@ Extract name from path
		s32 nInd = C_StringUtils::FindLast(inStrValue, '.');
		if (nInd == -1)
			return C_EMPTY_STD_STRING;

		if (nInd < (s32)(inStrValue.length() - 1))
		{
#ifdef USE_STD_STRING
			CC_ASSERT(false);
			T_String strTmp = inStrValue.substr(nInd + 1, inStrValue.length() - (nInd + 1) - 1);
#else
			T_String strTmp = inStrValue.Right(nInd+1);
#endif
			return strTmp;
		}

		return T_String();
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ StringToInt
	//
	//////////////////////////////////////////////////////////////////////////
	/*
	* Returned int value from std::string is inside outValue parameter.
	* return bool if conversion was successful
	*/
	bool C_StringUtils::StringToInt(const T_String & value, s32 & outValue)
	{
		if (value.empty())
			return false;

		char * endChar;
		outValue = strtol(value.c_str(), &endChar, 10);
		return (*endChar == 0);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ IntToString
	//
	//////////////////////////////////////////////////////////////////////////
	std::string C_StringUtils::IntToString(const u32 & inValue)
	{
		char buffer[512];
		std::string tmpStr;
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
		sprintf_s(buffer, 512, "%d", inValue);
		tmpStr = buffer;
#else
		sprintf(buffer, "%d", inValue);
		tmpStr = buffer;
#endif
		return tmpStr;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ StringToFloat
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_StringUtils::StringToFloat(const T_String & value, float & outValue)
	{
		return C_XmlConvertorUtils::FloatFromString(value.c_str(), outValue);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ FormatString
	//
	//////////////////////////////////////////////////////////////////////////
	std::string C_StringUtils::FormatString(const char* format, ...)
	{
		char buffer[512];

		va_list list;
		va_start(list, format);

#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
		vsprintf_s(buffer, 512, format, list);
#else
		vsprintf(buffer, format, list);
#endif
		return std::string(buffer);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TrimFromLeft
	//
	//////////////////////////////////////////////////////////////////////////
	T_String C_StringUtils::TrimFromLeft(const T_String & inText, char character)
	{
#ifndef USE_STD_STRING
		return inText.TrimLeft(character);
#else

		s32 index = inText.find(character);
		if (index == -1)
			return inText;

		std::string strTmp = inText.substr(index + 1, inText.length() - index - 1);

		return strTmp;
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TrimFromRight
	//
	//////////////////////////////////////////////////////////////////////////
	T_String C_StringUtils::TrimFromRight(const T_String & inText, char character)
	{
#ifndef USE_STD_STRING
		return inText.TrimRight(character);
#else

		s32 index = inText.rfind(character);

		if (index == -1)
			return inText;

		std::string strTmp = inText.substr(0, index);

		return strTmp;
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TrimFromCharacter
	//
	//////////////////////////////////////////////////////////////////////////
	T_String C_StringUtils::TrimFromCharacter(const T_String & inText, char character)
	{
		SC_ASSERT(false);
		u32 nLen = (u32)inText.length();
		char * buffer = new char[nLen];

		u32 index = 0;
		for (u32 u = 0; u < nLen; ++u)
		{
			u8 chr = inText[u];
			if (chr == character)
				continue;

			buffer[index++] = inText[u];
		}

		if (!index)
			return inText;


		buffer[index] = 0;
		T_String strTmp(buffer);
		//SC_SAFE_DELETE(buffer);

		return strTmp;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Split
	//
	//////////////////////////////////////////////////////////////////////////
	void C_StringUtils::Split(const T_String & inStrText, char token, T_StringVector & strVectOut)
	{
		std::istringstream f(inStrText.c_str());
		std::string s;
		while (std::getline(f, s, token))
		{
			strVectOut.push_back(s.c_str());
		}

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Split
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_StringUtils::FindLast(const T_String & inStrValue, char character)
	{
#ifdef USE_STD_STRING
		s32 nInd = inStrValue.rfind(character);
#else
		s32 nInd = inStrValue.FindLast(character);
#endif
		return nInd;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Split
	//
	//////////////////////////////////////////////////////////////////////////
	T_String C_StringUtils::Replace(const T_String & inStrValue, char oldChar, char newChar)
	{
#ifdef USE_STD_STRING
		T_String strRet(inStrValue);
		std::replace(strRet.begin(), strRet.end(), oldChar, newChar);
		return strRet;
#else

		T_String strRet = inStrValue.replace(oldChar, newChar);
		return strRet;
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ToLower
	//
	//////////////////////////////////////////////////////////////////////////
	T_String C_StringUtils::ToLower(const T_String & inString)
	{
#ifdef USE_STD_STRING
		T_String tmpStr(inString);
		std::transform(tmpStr.begin(), tmpStr.end(), tmpStr.begin(), ::tolower);
		return tmpStr;
#else
		return inString.ToLower();
#endif
	}

}}