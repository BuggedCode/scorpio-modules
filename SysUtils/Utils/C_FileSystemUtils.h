/*
* filename:			C_FileSystemUtils.h
*
* author:			Kral Jozef
* date:				28/8/2011		10:29
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <SysUtils/C_String.h>
#include <Common/Limits.h>




namespace scorpio{ namespace sysutils{

   //////////////////////////////////////////////////////////////////////////
   //@ C_FileSystemUtils
   class SYSUTILS_API C_FileSystemUtils
   {
   public:
      //@ Initialize
      static bool          Initialize();

      //@ MakeFilePath
      static const char *	MakeFilePath(const char * fileName);
      
#ifdef PLATFORM_WIN32
      //@ MakeRelativePath
      static sysutils::C_String		MakeRelativePath(const char * absolutePath);
#endif

   private:
      static char				m_AppDir[SC_MAX_PATH];
      static char				m_TempBuffer[SC_MAX_PATH];
   };

}}
