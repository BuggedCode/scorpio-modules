/*
* filename:			C_ProfilerBlock.cpp
*
* author:			Kral Jozef
* date:				11/22/2011		9:42
* version:			1.00
* brief:
*/

#include "C_ProfilerBlock.h"
#include "C_Profiler.h"
#include <SysUtils/C_String.h>
#include <SysUtils/C_FNVHash.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_ProfilerBlock::C_ProfilerBlock(const char * userName, const char * file, u32 line)
	{
		C_String str = C_String::Format("%s_%s_%d", userName, file, line);
		m_Guid = C_FNVHash::computeHash32(str.c_str());
		T_Profiler::GetInstance().BeginProfile(m_Guid, userName, file, line);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ End
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilerBlock::End()
	{
		T_Profiler::GetInstance().EndProfile(m_Guid);
	}

}}