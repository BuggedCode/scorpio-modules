/*
* filename:			C_Profiler.cpp
*
* author:			Kral Jozef
* date:				11/22/2011		10:09
* version:			1.00
* brief:
*/

#include "C_Profiler.h"
#include <Common/C_Timer.h>

namespace scorpio{ namespace sysutils{

	u32 C_ProfilerItem::m_OrderIdGenerator = 0;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddChild
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilerItem::AddChild(T_Id32 itemUID, T_ProfilerItem item)
	{
		std::pair<T_ProfilerItemMap::iterator, bool> res = m_ItemList.insert(T_ProfilerItemMap::value_type(itemUID, item));
		SC_ASSERT(res.second);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetChild
	//
	//////////////////////////////////////////////////////////////////////////
	T_ProfilerItem C_ProfilerItem::GetChild(T_Id32 itemUID)
	{
		T_ProfilerItemMap::iterator iter = m_ItemList.find(itemUID);
		if (iter == m_ItemList.end())
			return T_ProfilerItem();
		
		return iter->second;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Begin
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilerItem::Begin()
	{
		m_StartTime = (u32)C_Timer::GetTimeInMiliSeconds();
		++m_Counter;
		++m_InUseRefCounter;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Visit
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilerItem::Visit(I_ProfilerItemsVisitor & visitor)
	{
		visitor.Ascend();

		T_ProfilerItemMap procesRes;
		for(T_ProfilerItemMap::iterator iter = m_ItemList.begin(); iter != m_ItemList.end(); ++iter)
			procesRes.insert(T_ProfilerItemMap::value_type(iter->second->GetOrderId(), iter->second));

		for(T_ProfilerItemMap::iterator iter = procesRes.begin(); iter != procesRes.end(); ++iter)
		{
			visitor.ProcessItem(iter->second);
			iter->second->Visit(visitor);
		}
		visitor.Descend();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ End
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilerItem::End()
	{
		u32 deltaT = (u32)C_Timer::GetTimeInMiliSeconds() - m_StartTime;
		m_SumTime += deltaT;
		--m_InUseRefCounter;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////
	//
	//////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Profiler::~C_Profiler()
	{
		class C_CheckInUseReferenceVisitor : public I_ProfilerItemsVisitor
		{
		public:
			void			ProcessItem(const T_ProfilerItem item)
			{
				u8 cnt = item->GetInUseCounter();
				SC_ASSERT(cnt == 0);
			}
		};
		
		C_CheckInUseReferenceVisitor visitor;
		ProcesItems(visitor);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ProcesItems
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Profiler::ProcesItems(I_ProfilerItemsVisitor & visitor)
	{
		//@ sort by chronological order
		T_ProfilerItemMap procesRes;
		for(T_ProfilerItemMap::iterator iter = m_ItemList.begin(); iter != m_ItemList.end(); ++iter)
			procesRes.insert(T_ProfilerItemMap::value_type(iter->second->GetOrderId(), iter->second));
		
		for(T_ProfilerItemMap::iterator iter = procesRes.begin(); iter != procesRes.end(); ++iter)
		{
			visitor.ProcessItem(iter->second);
			iter->second->Visit(visitor);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ BeginProfile
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Profiler::BeginProfile(T_Id32 itemUID, const char * userName, const char * file, u32 line)
	{
		if (!m_ItemStack.empty())
		{
			T_ProfilerItem top = m_ItemStack.top();
			T_ProfilerItem item = top->GetChild(itemUID);
			if (item)
			{
				m_ItemStack.push(item);
				item->Begin();
			}
			else
			{
				T_ProfilerItem newItem = T_ProfilerItem(new C_ProfilerItem(userName, file, line));
				top->AddChild(itemUID, newItem);
				m_ItemStack.push(newItem);
				newItem->Begin();
			}
		}
		else
		{
			T_ProfilerItemMap::iterator iter = m_ItemList.find(itemUID);
			if (iter == m_ItemList.end())
			{
				T_ProfilerItem newItem = T_ProfilerItem(new C_ProfilerItem(userName, file, line));
				std::pair<T_ProfilerItemMap::iterator, bool> res = m_ItemList.insert(T_ProfilerItemMap::value_type(itemUID, newItem));
				SC_ASSERT(res.second);
				m_ItemStack.push(newItem);
				newItem->Begin();
			}
			else
			{
				m_ItemStack.push(iter->second);
				iter->second->Begin();
			}
		}

		return;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ EndProfile
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Profiler::EndProfile(T_Id32 itemUID)
	{
		if (m_ItemStack.empty())
		{
			SC_ASSERT(false);

			T_ProfilerItemMap::iterator iter = m_ItemList.find(itemUID);
			if (iter == m_ItemList.end())
			{
				SC_ASSERT(false);
				return;
			}

			iter->second->End();
		}
		else
		{
			T_ProfilerItem top = m_ItemStack.top();
			top->End();
			m_ItemStack.pop();
		}

		return;
	}
}}
