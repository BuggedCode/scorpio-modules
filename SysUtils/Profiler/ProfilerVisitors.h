/*
* filename:			ProfilerVisitors.h
*
* author:			Kral Jozef
* date:				11/24/2011		10:08
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include "C_Profiler.h"
#include <SysUtils/C_String.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//
	class SYSUTILS_API C_ResultToTraceVisitor : public I_ProfilerItemsVisitor
	{
	public:
		//@ Ascend - ascend to children
		virtual void				Ascend();
		//@ Descend - descend from children
		virtual void				Descend();
		//@ VisitItem
		virtual void				ProcessItem(const T_ProfilerItem item);

	private:
		sysutils::C_String		m_Indentation;
	};
}}
