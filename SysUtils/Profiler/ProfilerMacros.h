/*
* filename:			ProfilerMacros.h
*
* author:			Kral Jozef
* date:				11/22/2011		10:04
* version:			1.00
* brief:
*/

#include <SysUtils/Profiler/C_Profiler.h>
#include <SysUtils/Profiler/C_ProfilerBlock.h>
#include <SysUtils/Profiler/ProfilerVisitors.h>

#ifdef SC_PROFILER_ENABLE
	#define INIT_PROFILER									T_Profiler::CreateInstance();
	#define DEINIT_PROFILER									T_Profiler::DestroyInstance();

	#define SC_BEGIN_PROF_BLOCK(userName)				scorpio::sysutils::C_ProfilerBlock	profBlck(userName, __FILE__, __LINE__);
	#define SC_END_PROF_BLOCK								profBlck.End();

	#define SC_BEGIN_PROF_BLOCK_INSTANCE(userName, instanceName)	scorpio::sysutils::C_ProfilerBlock	instanceName(userName, __FILE__, __LINE__);
	#define SC_END_PROF_BLOCK_INSTANCE(instanceName)					instanceName.End();

	#define SC_PROF_RESULT_TO_TRACE						C_ResultToTraceVisitor resultVisitor;	\
																	T_Profiler::GetInstance().ProcesItems(resultVisitor);


#else
	#define INIT_PROFILER
	#define DEINIT_PROFILER

	#define SC_BEGIN_PROF_BLOCK(userName)
	#define SC_END_PROF_BLOCK

	#define SC_BEGIN_PROF_BLOCK_INSTANCE(userName, instanceName)
	#define SC_END_PROF_BLOCK_INSTANCE(instanceName)

	#define SC_PROF_RESULT_TO_TRACE
#endif