/*
* filename:			ProfilerVisitors.cpp
*
* author:			Kral Jozef
* date:				11/24/2011		10:10
* version:			1.00
* brief:
*/

#include "ProfilerVisitors.h"
#include <SysUtils/C_TraceClient.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ProcessItem
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ResultToTraceVisitor::ProcessItem(const T_ProfilerItem item)
	{
		TRACE_FI("#B050F0   %s %s: AvgTime: %d  Total:", m_Indentation.c_str(), item->GetUserName(), item->GetAverageTime());
	}

	//////////////////////////////////////////////////////////////////////////
	void C_ResultToTraceVisitor::Ascend()
	{ 
		m_Indentation += "...";
	}

	//////////////////////////////////////////////////////////////////////////
	void C_ResultToTraceVisitor::Descend()
	{
		u32 len = m_Indentation.length();
		if (len < 4)
			m_Indentation = "";
		else
			m_Indentation = C_String::SubString(m_Indentation, 0, m_Indentation.length() - 4);
	}


}}
