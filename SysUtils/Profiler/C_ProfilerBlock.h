/*
* filename:			C_ProfilerBlock.h
*
* author:			Kral Jozef
* date:				11/22/2011		9:24
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <Common/types.h>

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ProfilerBlock
	class SYSUTILS_API C_ProfilerBlock
	{
	public:
		//@ c-tor
		C_ProfilerBlock(const char * userName, const char * file, u32 line);
		//@ End
		void			End();

	private:
		T_Id32		m_Guid;
	};
}}
