/*
* filename:			C_SysProfiler.h
*
* author:			Kral Jozef
* date:				11/21/2011		23:09
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/SysUtilsApi.h>
#include <boost/shared_ptr.hpp>
#include <map>
#include <stack>
#include <Common/C_Singleton.h>

namespace scorpio{ namespace sysutils{

	//# forward
	class C_ProfilerItem;
	typedef boost::shared_ptr<C_ProfilerItem>	T_ProfilerItem;

	typedef std::map<T_Id32, T_ProfilerItem>	T_ProfilerItemMap;


	//////////////////////////////////////////////////////////////////////////
	//@ I_ProfilerItemsVisitor
	class I_ProfilerItemsVisitor
	{
	public:
		//@ Ascend - ascend to children
		virtual void				Ascend() {};
		//@ Descend - descend from children
		virtual void				Descend() {};
		//@ VisitItem
		virtual void				ProcessItem(const T_ProfilerItem item) = 0;
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_ProfilerItem
	class SYSUTILS_API C_ProfilerItem
	{
	public:
		//@ c-tor
		C_ProfilerItem(const char * userName, const char * file, u32 line)
			: m_UserName(userName), m_File(file), m_Line(line), m_StartTime(0), m_SumTime(0), m_Counter(0), m_InUseRefCounter(0), m_OrderId(++m_OrderIdGenerator) {};

		//@ Begin
		void									Begin();
		//@ End
		void									End();

		// AddChild
		void									AddChild(T_Id32 itemUID, T_ProfilerItem item);
		//@ GetChild
		T_ProfilerItem						GetChild(T_Id32 itemUID);
		//@ Visit
		void									Visit(I_ProfilerItemsVisitor & visitor);

		//@ GetInUseCounter
		u8										GetInUseCounter() const { return m_InUseRefCounter; }
		//@ GetUserName
		const char *						GetUserName() const { return m_UserName; }
		//@ GetAverageTime - in ms
		u32									GetAverageTime() const { return (u32)(m_SumTime / m_Counter); }
		//@ GetSumTime
		u64									GetSumTime() const { return m_SumTime; }
		//@ GetOrderId
		u32									GetOrderId() const { return m_OrderId; }

		

	private:
		const char *		m_UserName;
		const char *		m_File;
		u32					m_Line;
		u32					m_StartTime;
		u64					m_SumTime;
		u32					m_Counter;	//count for cycles
		u32					m_OrderId;	//for chronological output sorting
		u8						m_InUseRefCounter;	//ref counter for using this instance -> in single thread should be 1
		T_ProfilerItemMap	m_ItemList;

	private:
		static u32			m_OrderIdGenerator;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_Profiler
	class SYSUTILS_API C_Profiler
	{
		friend class C_Singleton<C_Profiler>;
	public:
		//@ BeginProfile
		void									BeginProfile(T_Id32 itemUID, const char * userName, const char * file, u32 line);
		//@ EndProfile
		void									EndProfile(T_Id32 itemUID);
		//@ ProcessItems
		void									ProcesItems(I_ProfilerItemsVisitor & visitor);

	private:
		//@ d-tor
		~C_Profiler();

	private:
		T_ProfilerItemMap					m_ItemList;
		std::stack<T_ProfilerItem>		m_ItemStack;
	};

	typedef C_Singleton<C_Profiler>	T_Profiler;
}}

SYSUTILS_EXP_TEMP_INST template class SYSUTILS_API C_Singleton<scorpio::sysutils::C_Profiler>;