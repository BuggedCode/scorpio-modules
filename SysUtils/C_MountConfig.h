/*
* filename:			C_MountConfig.h
*
* author:			Kral Jozef
* date:				01/04/2011		18:58
* version:			1.00
* brief:
*/

#pragma once

#include <Common/GlobalDefines.h>

#ifdef USE_MOUNT_POINT

#include <SysUtils/SysUtilsApi.h>
#include <Common/C_Singleton.h>
#include "C_String.h"
#include <map>
#include "C_HashName.h"

namespace scorpio{ namespace sysutils{

	//////////////////////////////////////////////////////////////////////////
	//@ C_MountConfig
	class SYSUTILS_API C_MountConfig
	{
		friend class C_Singleton<C_MountConfig>;
	public:
		//@ Initialize
		bool					Initialize(C_String cfgFileName);
		//@ GetAbsDirectory
		C_String				GetAbsDirectory(C_String relDir);
		//@ RemapToAbsolute
		C_String				RemapToAbsolute(C_String relFileName);
		//@ RemapToRelative
		C_String				RemapToRelative(C_String absFileName);

	private:
		//@ c-tor
		C_MountConfig();
		//@ d-tor
		~C_MountConfig();

		typedef std::map<C_HashName, C_String>	T_MappingContainer;
		T_MappingContainer	*	m_container;
	};

	typedef C_Singleton<C_MountConfig>	T_MountConfig;

}}

//template <class T> T C_Singleton<T>::m_instance;
SYSUTILS_EXP_TEMP_INST template class SYSUTILS_API C_Singleton<scorpio::sysutils::C_MountConfig>;

#endif