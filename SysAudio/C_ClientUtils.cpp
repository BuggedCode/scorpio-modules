/*
* filename:			C_ClientUtils.cpp
*
* author:			Kral Jozef
* date:				03/08/2011		11:53
* version:			1.00
* brief:
*/

#include "C_ClientUtils.h"

#ifdef PLATFORM_WIN32
   #include <al.h>
#elif defined PLATFORM_MACOSX
   #include <OpenAL/al.h>
#endif

#include <SysUtils/C_TraceClient.h>
#include <SysUtils/C_String.h>

namespace scorpio{ namespace sysaudio{


	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CheckErr
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ClientUtils::CheckError(const char * funcName)
	{
		ALenum err = alGetError();
		if (err != AL_NO_ERROR)
		{
			C_String strErrName;
			switch(err)
			{
			case AL_INVALID_NAME:
				strErrName = "AL_INVALID_NAME";
				break;
			case AL_ILLEGAL_ENUM:
				strErrName = "AL_ILLEGAL_ENUM";
				break;
			case AL_INVALID_VALUE:
				strErrName = "AL_INVALID_VALUE";
				break;
			case AL_INVALID_OPERATION:	//AL_ILLEGAL_COMMAND
				strErrName = "AL_INVALID_OPERATION / AL_ILLEGAL_COMMAND";
				break;
			default:
				strErrName.Format("UNKNOWN_ERR id: %d", (u32)err);
			};	//switch
			TRACE_FE("OpenAL Error. Function: %s Error: %s", funcName, strErrName.c_str());
			return false;
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ValidateBuffer
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ClientUtils::ValidateBuffer(u32 bufferId)
	{
		bool bRes = alIsBuffer(bufferId) == 1;
		if (!bRes)
		{
			TRACE_FE("Buffer: 0x%X is INVALIDE", bufferId);
			return false;
		}

		return true;
	}

}}