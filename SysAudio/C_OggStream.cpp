/*
* filename:			C_OggStream.cpp
*
* author:			Kral Jozef
* date:				03/07/2011		18:37
* version:			1.00
* brief:
*/

#include "C_OggStream.h"
#include <SysUtils/C_TraceClient.h>
#include "C_ClientUtils.h"

namespace scorpio{ namespace sysaudio{

	static const u32 C_BUFFER_SIZE = (4096 * 8);//32KB

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Open
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_OggStream::Open(const sysutils::T_Stream inputStream)
	{
		if (!inputStream)
			return false;

		SC_ASSERT(!inputStream->GetBuffer());	//for now we don't support reading from memory

#ifdef PLATFORM_WIN32
		errno_t err = fopen_s(&m_File, inputStream->GetFileName(), "rb");
		if (err != 0)
		{
			TRACE_FE("Failed to open stream %s", inputStream->GetFileName());
			return false;
		}
#elif defined PLATFORM_MACOSX
		m_File = fopen(inputStream->GetFileName(), "rb");
		if (!m_File)
		{
			TRACE_FE("Failed to open stream %s", inputStream->GetFileName());
			return false;
		}      
#endif

		s32 ret = ov_open(m_File, &m_OggStream, NULL, 0);
		if (ret < 0)
		{
			TRACE_FE("Failed to create Vorbis stream %s", inputStream->GetFileName());
			fclose(m_File);
			return false;
		}

		m_VorbisInfo = ov_info(&m_OggStream, -1);
		m_VorbisComment = ov_comment(&m_OggStream, -1);

		if (m_VorbisInfo->channels == 1)
			m_Format = AL_FORMAT_MONO16;
		else
			m_Format = AL_FORMAT_STEREO16;

		//@ m_Format

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Close
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_OggStream::Close()
	{
		//@ Close only if stream was opened
		if (m_OggStream.datasource != NULL)
			return (ov_clear(&m_OggStream) == 0);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadSample
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_OggStream::LoadSample(std::vector<char> & outBuffer)
	{
		u32 endian = 0;	//little endian, 1 - big endian
		//@ 2 - 16bit samples
		//@ 1 - signed
		s32 bitStream;
		char buffer[C_BUFFER_SIZE];	//32KB
		s64 bytesRead = 0;
		do 
		{
			bytesRead = ov_read(&m_OggStream, buffer, C_BUFFER_SIZE, endian, 2, 1, &bitStream);
			outBuffer.insert(outBuffer.end(), buffer, buffer + bytesRead);
		} while (bytesRead != 0);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Stream
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_OggStream::Stream(ALuint bufferID, bool bLoop, bool & bStreamFinished)
	{
		bStreamFinished = false;

		u32 endian = 0;	//little endian, 1 - big endian
		char pcm[C_BUFFER_SIZE];
		u64  size = 0;
		s32  bitStream;
		s64 bytesRead = 0;

		while(size < C_BUFFER_SIZE)
		{
			bytesRead = ov_read(&m_OggStream, pcm + size, (int)(C_BUFFER_SIZE - size), endian, 2, 1, &bitStream);

			if(bytesRead > 0)
				size += bytesRead;
			else
			{
				if(bytesRead < 0)
				{
					//OV_HOLE/OV_EINVAL/OV_EBADLINK
					TRACE_FE("Error during vorbis data read: 0x%X", bytesRead);
					return false;
				}
				else
				{
					//bytesRead == 0 => EOF
					if (bLoop)
					{
						s32 ret = ov_raw_seek_lap(&m_OggStream, 0);
						if (ret != 0)
						{
							TRACE_FE("Error during stream seek: 0x%X", ret);
						}
					}
					else
					{
						bStreamFinished = true;
					}
					break;
				}
			}
		}

		alBufferData(bufferID, m_Format, pcm, (ALsizei)size, (ALsizei)m_VorbisInfo->rate);
		if (!C_ClientUtils::CheckError("alBufferData"))
			return false;

		return true;
	}
}}