/*
* filename:			C_Sound.cpp
*
* author:			Kral Jozef
* date:				03/07/2011		15:05
* version:			1.00
* brief:
*/

#include "C_Sound.h"
#include <SysUtils/C_String.h>
#include <SysUtils/Utils/C_StringUtils.h>
#include <SysUtils/C_TraceClient.h>
#include "C_OggStream.h"
#include "C_ClientUtils.h"
#include <boost/bind.hpp>
#include "C_OpenALRenderer.h"

namespace scorpio{ namespace sysaudio{

	using namespace scorpio::sysutils;

	static const char * C_AUDIO_EXT_1 = "ogg";
	static const ALuint C_IVALID_SOURCE_ID = (ALuint)-1;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ copy c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Sound::C_Sound(const C_Sound & copy)
	{
		m_BufferID = copy.m_BufferID;
		m_Prepared = copy.m_Prepared;
		m_SourceId = AL_NONE;	//!! don't want copy source ID
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Sound::~C_Sound()
	{
		Clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Sound::Clear()
	{
		if (!m_Prepared)
			return;

		alDeleteBuffers(1, &m_BufferID);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadSample
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Sound::Load(const T_Stream inputStream, bool bLoop, float fVolume)
	{
		if (m_Prepared)
		{
			Clear();
		}

		T_String strExt = C_StringUtils::GetExtension(inputStream->GetFileName());
		if (strExt != C_AUDIO_EXT_1)
		{
			TRACE_FE("Unsupported audio format: %s", strExt.c_str());
			return false;
		}

		C_OggStream oggFile;
		if (!oggFile.Open(inputStream))
			return false;

		std::vector<char> bufferData;
		oggFile.LoadSample(bufferData);

		alGenBuffers(1, &m_BufferID);
		alBufferData(m_BufferID, oggFile.GetFormat(), &bufferData[0], (ALsizei)bufferData.size(), oggFile.GetFrequency()); 

		bufferData.clear();

		m_Volume = fVolume;
		m_Loop = bLoop;

		m_Prepared = true;

		//////////////////////////////////////////////////////////////////////////
		oggFile.Close();

		return true; 
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ PlaySample
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Sound::Play()
	{
		if (IsPlaying())
		{
			Stop();

			//@ Create new instance of sound, which will share audioBuffers!!! possible RISK, this instance will be stored only in OpenALRenderer
			/*T_Sound newInst = T_Sound(new C_Sound(*this));
			C_Sound::PlayInstance(newInst);
			return;*/
		}
		
		if (m_SourceId == AL_NONE)
		{
			m_SourceId = T_OpenALRenderer::GetInstance().RequestSource(this);
			if (m_SourceId == AL_NONE)
			{
				TRACE_E("Failed to get free source");
				return;
			}
		}

		alSourcePlay(m_SourceId);
		C_ClientUtils::CheckError("Play");
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ PlayInstance
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Sound::PlayInstance(T_Sound soundInstance)
	{
		SC_ASSERT(soundInstance->m_SourceId == AL_NONE);
		
		soundInstance->m_SourceId = T_OpenALRenderer::GetInstance().RequestSource(soundInstance);
		if (soundInstance->m_SourceId == AL_NONE)
		{
			TRACE_E("Failed to get free source for SOUND instance");
			return;
		}

		alSourcePlay(soundInstance->m_SourceId);
		C_ClientUtils::CheckError("Play");
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Pause
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Sound::Pause()
	{
		if (!IsPlaying())
			return;

		alSourcePause(m_SourceId);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Stop
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Sound::Stop()
	{
		if (m_SourceId == AL_NONE)
			return;

		alSourceStop(m_SourceId);
		alSourceRewind(m_SourceId);

		T_OpenALRenderer::GetInstance().ReleaseSource(this);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ StopInstance
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Sound::StopInstance(T_Playable instance)
	{
		SC_ASSERT(this == instance.get());
		if (m_SourceId == AL_NONE)
			return;

		alSourceStop(m_SourceId);
		alSourceRewind(m_SourceId);
		C_ClientUtils::CheckError("Stop Instance");

		T_OpenALRenderer::GetInstance().ReleaseSource(instance);

		//@ clear bufferId for instance - instances uses shared buffer without ref counter:( coz lack of time:)
		m_Prepared = false;
		m_BufferID = AL_NONE;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsPlaying
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Sound::IsPlaying() const
	{
		if (m_SourceId == AL_NONE)
			return false;

		ALint state;
		alGetSourcei(m_SourceId, AL_SOURCE_STATE, &state);
		return state == AL_PLAYING;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetSource
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Sound::SetSource(u32 srcId)
	{
		if (srcId != AL_NONE)
		{
			// Attach new source
			m_SourceId = srcId;

			// Load audio data onto source
			alSourcei(m_SourceId, AL_BUFFER, m_BufferID);
			C_ClientUtils::CheckError("alSourcei");

			// Init source properties
			alSourcef(m_SourceId, AL_PITCH, 1.0f);
			alSourcef(m_SourceId, AL_GAIN, m_Volume * m_VolumeMultiplier);
			alSourcei(m_SourceId, AL_LOOPING, m_Loop ? AL_TRUE : AL_FALSE);

			alSource3f(m_SourceId, AL_POSITION,        0.0, 0.0, 0.0);
			alSource3f(m_SourceId, AL_VELOCITY,        0.0, 0.0, 0.0);
			alSource3f(m_SourceId, AL_DIRECTION,       0.0, 0.0, 0.0);
			alSourcef (m_SourceId, AL_ROLLOFF_FACTOR,  0.0          );
			alSourcei (m_SourceId, AL_SOURCE_RELATIVE, AL_TRUE      );
		}
		else
		{
			// Need to stop sound BEFORE unqueuing
			alSourceStop(m_SourceId);

			ALenum state;
			alGetSourcei(m_SourceId, AL_SOURCE_STATE, &state);

			// Unqueue buffer
			alSourcei(m_SourceId, AL_BUFFER, 0);
			C_ClientUtils::CheckError("alSourcei");

			// Attach new source
			m_SourceId = srcId;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ UpdateBuffers
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Sound::UpdateBuffers(u32 deltaTime)
	{
		if (m_SourceId == AL_NONE)
			return;

		ALenum state;
		alGetSourcei(m_SourceId, AL_SOURCE_STATE, &state);

		if (state == AL_STOPPED)
		{
			Stop();
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ UpdateInstanceBuffers
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Sound::UpdateInstanceBuffers(T_Playable instance)
	{
		if (m_SourceId == AL_NONE)
			return;

		ALenum state;
		alGetSourcei(m_SourceId, AL_SOURCE_STATE, &state);

		if (state == AL_STOPPED)
		{
			StopInstance(instance);
		}
	}

}}