/*
* filename:			C_OggStream.h
*
* author:			Kral Jozef
* date:				03/07/2011		18:37
* version:			1.00
* brief:
*/

#pragma once

#include <SysAudio/SysAudioApi.h>
#include <stdio.h>
#include <vorbis/vorbisfile.h>
#include <Common/PlatformDef.h>

#ifdef PLATFORM_WIN32
   #include <al.h>
#elif defined PLATFORM_MACOSX
   #include <OpenAL/al.h>
#endif

#include <vector>
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace sysaudio{

	//////////////////////////////////////////////////////////////////////////
	//@ C_OggStream
	class SYSAUDIO_API C_OggStream
	{
	public:
		//@ c-tor
		C_OggStream()
			: m_File(NULL), m_Format(AL_NONE) { m_OggStream.datasource = NULL; }

		//@ Open
		bool					Open(const sysutils::T_Stream inputStream);
		//@ Close
		bool					Close();
		//@ LoadSample
		bool					LoadSample(std::vector<char> & outBuffer);
		//@ Stream
		bool					Stream(ALuint bufferID, bool bLoop, bool & bStreamFinished);

		//@ GetFormat
		ALenum				GetFormat() const { return m_Format; }
		//@ GetFrequency
		u32					GetFrequency() const { return (u32)m_VorbisInfo->rate; }

	private:
		FILE				*	m_File;
		OggVorbis_File		m_OggStream;
		vorbis_info		*	m_VorbisInfo;
		vorbis_comment	*	m_VorbisComment;
		ALenum				m_Format;
	};

}}