/*
* filename:			C_AudioManager.h
*
* author:			Kral Jozef
* date:				03/08/2011		13:26
* version:			1.00
* brief:
*/

#pragma once

#include <SysAudio/SysAudioApi.h>
#include <Common/C_Singleton.h>
#include "C_Sound.h"
#include "C_Music.h"
#include <SysUtils/FileSystem/C_Stream.h>
#include <SysUtils/Xml/C_XmlNode.h>
#include <map>

namespace scorpio{ namespace sysaudio{

	//////////////////////////////////////////////////////////////////////////
	//@ C_AudioManager
	class SYSAUDIO_API C_AudioManager
	{
		friend class C_Singleton<C_AudioManager>;
	public:
		//@ LoadSounds
		bool					LoadSounds(const sysutils::T_Stream inputStream);
		//@ LoadSounds
		bool					LoadSounds(const sysutils::T_XmlNodeList soundsNodeList);
		//@ AddMusic
		bool					AddMusic(T_Music music);
		//@ RemoveMusic
		bool					RemoveMusic(T_Hash32 musicID);

		//@ PlaySound
		bool					PlaySoundEx(T_Hash32 soundID);	//can be slow
		//@ GetSound
		T_Sound				GetSound(T_Hash32 soundID);
		//@ GetMusic
		T_Music				GetMusic(T_Hash32 musicID);

		//@ SetMusicVolume
		void					SetMusicVolume(float fVal);
		//@ SetSoundVolume
		void					SetSoundVolume(float fVal);
		//@ GetMusicVolume
		float					GetMusicVolume() const { return m_MusicVolume; }
		//@ GetSoundVolume
		float					GetSoundVolume() const { return m_SoundVolume; }

		//@ Update - update audio buffers
		void					Update(u32 inDeltaTime);

	private:
		//@ CreateSoundFromXmlNode
		T_Sound				CreateSoundFromXmlNode(const sysutils::C_XmlNode & soundNode);

	private:
		//@ c-tor
		C_AudioManager()
			: m_MusicVolume(1.f), m_SoundVolume(1.f) {};
		//@ d-tor
		~C_AudioManager() {};
		

		typedef std::map<T_Hash32, T_Sound>	T_SoundMap;
		typedef std::map<T_Hash32, T_Music>	T_MusicMap;

		T_SoundMap		m_SoundContainer;
		T_MusicMap		m_MusicContainer;

		float				m_MusicVolume;	//can be extended through context
		float				m_SoundVolume;	//can be extended through context
	};


	typedef C_Singleton<C_AudioManager>	T_AudioManager;

}}

SYSAUDIO_EXP_TEMP_INST template class SYSAUDIO_API C_Singleton<scorpio::sysaudio::C_AudioManager>;