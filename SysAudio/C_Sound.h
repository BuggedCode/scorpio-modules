/*
* filename:			C_Sound.h
*
* author:			Kral Jozef
* date:				03/07/2011		15:05
* version:			1.00
* brief:
*/

#pragma once

#include <SysAudio/SysAudioApi.h>
#include "I_Playable.h"

#ifdef PLATFORM_WIN32
   #include <al.h>
#elif defined PLATFORM_MACOSX
   #include <OpenAL/al.h>
#endif

#include <boost/shared_ptr.hpp>
#include <vector>

namespace scorpio{ namespace sysaudio{
	
	class C_Sound;

	typedef boost::shared_ptr<C_Sound>	T_Sound;

	//////////////////////////////////////////////////////////////////////////
	//@ C_Sound
	class SYSAUDIO_API C_Sound : public I_Playable
	{
	public:
		//@ c-tor
		C_Sound()
			: m_Prepared(false), m_SourceId(AL_NONE) {};
		//@ copy c-tor
		C_Sound(const C_Sound & copy);
		//@ d-tor
		~C_Sound();

		//@ Load
		bool				Load(const sysutils::T_Stream inputStream, bool bLoop = false, float fVolume = 1.f);
		//@ LoadStream/PlayStream/Stop/SetVolume/SetPitch/SetPosition
		void				Play();
		//@ Pause
		void				Pause();
		//@ Stop
		void				Stop();

		//@ SetSource
		void				SetSource(u32 srcId);
		//@ GetSource
		u32				GetSource() const { return m_SourceId; }
		//@ UpdateBuffers
		void				UpdateBuffers(u32 deltaTime);
		//@ StopInstance
		void				StopInstance(T_Playable instance);
		//@ UpdateInstanceBuffers
		void				UpdateInstanceBuffers(T_Playable instance);

	private:
		//@ Clear
		void				Clear();
		//@ IsPlaying
		bool				IsPlaying() const;
		//@ SPECIAL INTERFACE FOR INSTANCED SOUNDS - coz we force ALRenderer to take ownership of instance(will take care about instance life/death)
		static void		PlayInstance(T_Sound soundInstance);

	private:
		static const u8	C_MAX_SOURCE_NUM = 6;
		ALuint				m_BufferID;
		bool					m_Prepared;
		ALuint				m_SourceId;
	};

}}