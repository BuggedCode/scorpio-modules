/*
* filename:			I_Playable.h
*
* author:			Kral Jozef
* date:				03/08/2011		10:46
* version:			1.00
* brief:
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace sysaudio{

	class I_Playable;

	typedef boost::shared_ptr<I_Playable>	T_Playable;

	//////////////////////////////////////////////////////////////////////////
	//@ I_Playable
	class I_Playable
	{
	public:
		//@ c-tor
		I_Playable()
			: m_VolumeMultiplier(1.f), m_Volume(1.f), m_Loop(false) {};

		//@ Load
		virtual bool				Load(const sysutils::T_Stream inputStream, bool bLoop = false, float fVolume = 1.f) = 0;
		//@ LoadStream/PlayStream/Stop/SetVolume/SetPitch/SetPosition
		virtual void				Play() = 0;
		//@ Pause
		virtual void				Pause() = 0;
		//@ Stop
		virtual void				Stop() = 0;
		//@ Tick - for updating fader, buffering stream,...
		virtual void				UpdateBuffers(u32 deltaTime) = 0;

		//@ SetVolume
		virtual void				SetVolume(float fVolume) { m_Volume = fVolume; };
		//@ SetVolumeMult
		void							SetVolumeMult(float fVal) { m_VolumeMultiplier = fVal; }

		//@ SetSource
		virtual void				SetSource(u32 srcId) = 0;
		//@ GetSource
		virtual u32					GetSource() const = 0;
		//@ IsPlaying
		virtual bool				IsPlaying() const { return false; }

		//@ StopInstance - use just for instanced sounds!
		virtual void				StopInstance(T_Playable instance) { SC_ASSERT(false); }
		//@ UpdateInstanceBuffers - use just for instanced sounds!
		virtual void				UpdateInstanceBuffers(T_Playable instance) { SC_ASSERT(false); }

	protected:
		float							m_VolumeMultiplier;
		float							m_Volume;
		bool							m_Loop;
	};

}}