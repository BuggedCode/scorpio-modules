/*
* filename:			C_OpenALRenderer.cpp
*
* author:			Kral Jozef
* date:				03/07/2011		9:37
* version:			1.00
* brief:
*/

#include "C_OpenALRenderer.h"
#include <SysUtils/C_TraceClient.h>

namespace scorpio{ namespace sysaudio{

	using namespace sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_OpenALRenderer::Initialize()
	{
		T_StringVector deviceList;
		GetDeviceList(deviceList);
		
		//@ log devices
		//@ Supported OpenAL Device List
		TRACE_I("#BB0066 Supported OpenAL Device List:")
		for (T_StringVector::iterator iter = deviceList.begin(); iter != deviceList.end(); ++iter)
		{
			TRACE_FI("#BB0066    %s",iter->c_str());
		}

		//@ CheckFeatureSupport

		m_Device = alcOpenDevice(NULL);
		if (!m_Device)
		{
			TRACE_E("Failed OpenDevice");
			//return false;
		}
		
		m_Context = alcCreateContext(m_Device, NULL);
		if (!m_Context)
		{
			TRACE_E("Failed CreateContext");
			//return false;
		}
		
		if (!alcMakeContextCurrent(m_Context))
		{
			TRACE_E("Failed MakeCurrentContext");
			//return false;
		}

		m_Error = alGetError();
		if (m_Error != 0)
			return false;


		//@ CreateSourcePool
		m_SourceCount = CreateSourcePool(64);
		TRACE_FI("#BB0066    %d Sources Suported", m_SourceCount);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Deinitialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_OpenALRenderer::Deinitialize()
	{
		alcMakeContextCurrent(NULL);
		alcDestroyContext(m_Context);
		alcCloseDevice(m_Device);

		m_Context = NULL;
		m_Device = NULL;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetDeviceList
	//
	//////////////////////////////////////////////////////////////////////////
	void C_OpenALRenderer::GetDeviceList(std::vector<C_String> & outDeviceList)
	{
		const ALchar * deviceList = alcGetString(NULL, ALC_DEVICE_SPECIFIER);
		/*
		** The list returned by the call to alcGetString has the names of the
		** devices seperated by NULL characters and the list is terminated by
		** two NULL characters, so we can cast the list into a string and it
		** will automatically stop at the first NULL that it sees, then we
		** can move the pointer ahead by the lenght of that string + 1 and we
		** will be at the begining of the next string.  Once we hit an empty
		** string we know that we've found the double NULL that terminates the
		** list and we can stop there.
		*/
		while(*deviceList != 0)
		{
			C_String errStr;
			ALCdevice * device = alcOpenDevice(deviceList);
			if (alcGetError(device) != ALC_NO_ERROR) errStr = "Unable to open device";
			if ( errStr.empty() && device)
			{
				ALCcontext * context = alcCreateContext(device, NULL);
				ALCenum err = alcGetError(device);
				if ( err == ALC_NO_ERROR && context)
				{
					alcMakeContextCurrent(context);
					if (alcGetError(device) == ALC_NO_ERROR)
					{
						C_String str = alcGetString(device, ALC_DEVICE_SPECIFIER);
						outDeviceList.push_back(str);
					}
					else
					{
						TRACE_FE("Unable to make context current %s", deviceList);
					}

					alcMakeContextCurrent(NULL);
					if (alcGetError(device) != ALC_NO_ERROR)
						TRACE_FE("Unable to clear context %s", deviceList);

					alcDestroyContext(context);
					if (alcGetError(device) != ALC_NO_ERROR)
						TRACE_FE("Unable to destroy context %s", deviceList);
				}
				else
				{
					TRACE_FE("Unable to create context %s", deviceList);
				}

				alcCloseDevice(device);
			}
			else
			{
				TRACE_FE("%s %s", errStr.c_str(), deviceList);
			}

			deviceList += strlen(deviceList) + 1;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateSourcePool
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_OpenALRenderer::CreateSourcePool(u32 nMaxSources)
	{
		ALuint sourceId;
		u32 numSources = 0;
		while(numSources < nMaxSources && alGetError() == AL_NO_ERROR)
		{
			sourceId = 0;
			alGenSources(1, &sourceId);
			if (sourceId != 0)
			{
				m_SourcePool.push_back(sourceId);
				++numSources;
			}
			else
			{
				/*ALenum err = */alGetError();
				break;
			}
		}

		return numSources;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RequestSource
	//
	//////////////////////////////////////////////////////////////////////////
	ALuint C_OpenALRenderer::RequestSource(I_Playable * playable)
	{
		//find free source
		if (!m_SourcePool.empty())
		{
			ALuint src = static_cast<ALuint>(m_SourcePool.back());
			m_SourcePool.pop_back();
			m_ActiveSounds.push_back(playable);
			playable->SetSource(src);
			return src;
		}

		u32 activeInstancesCount = (u32)m_ActiveInstances.size();
		u32 activeSoundsCount = (u32)m_ActiveSounds.size();

		if ((activeInstancesCount + activeSoundsCount) == 0)
		{
			TRACE_E("No free sources and no active Sources STRANGE!");
			return AL_NONE;
		}


		if (activeInstancesCount)
		{
			//@ try force stop of instance
			T_Playable sound = static_cast<T_Playable>(*m_ActiveInstances.begin());
			sound->Stop();	//force release source
		}
		else if (activeSoundsCount)
		{
			//@ try force stop of sound
			I_Playable * sound = static_cast<I_Playable*>(*m_ActiveSounds.begin());
			sound->Stop();	//force release source
		}


		if (!m_SourcePool.empty())
		{
			ALuint src = static_cast<ALuint>(m_SourcePool.back());
			m_SourcePool.pop_back();
			m_ActiveSounds.push_back(playable);
			playable->SetSource(src);
			return src;
		}
		else
		{
			TRACE_E("No free sources after forced Sound::Stop");
		}

		return AL_NONE;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RequestSource
	//
	//////////////////////////////////////////////////////////////////////////
	ALuint C_OpenALRenderer::RequestSource(T_Playable playable)
	{
		//find free source
		if (!m_SourcePool.empty())
		{
			ALuint src = static_cast<ALuint>(m_SourcePool.back());
			m_SourcePool.pop_back();
			m_ActiveInstances.push_back(playable);
			playable->SetSource(src);
			return src;
		}

		if (m_ActiveInstances.empty())
		{
			TRACE_E("No free sources from activeInstances ()dont want force stop active sounds!");
			return AL_NONE;
		}


		//@ try force stop of instance
		T_Playable sound = static_cast<T_Playable>(*m_ActiveInstances.begin());
		sound->StopInstance(sound);	//force release source

		if (!m_SourcePool.empty())
		{
			ALuint src = static_cast<ALuint>(m_SourcePool.back());
			m_SourcePool.pop_back();
			m_ActiveInstances.push_back(playable);
			playable->SetSource(src);
			return src;
		}
		else
		{
			TRACE_E("No free sources after forced Sound::Stop");
		}

		return AL_NONE;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseSource
	//
	//////////////////////////////////////////////////////////////////////////
	void C_OpenALRenderer::ReleaseSource(I_Playable * playable)
	{
		T_SoundList::iterator iter = std::find(m_ActiveSounds.begin(), m_ActiveSounds.end(), playable);
		SC_ASSERT(iter != m_ActiveSounds.end());
		m_ActiveSounds.erase(iter);

		ALuint srcId = playable->GetSource();
		m_SourcePool.push_back(srcId);
		playable->SetSource(AL_NONE);
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseSource
	//
	//////////////////////////////////////////////////////////////////////////
	void C_OpenALRenderer::ReleaseSource(T_Playable playable)
	{
		T_PlayableList::iterator iter = std::find(m_ActiveInstances.begin(), m_ActiveInstances.end(), playable);
		SC_ASSERT(iter != m_ActiveInstances.end());
		m_ActiveInstances.erase(iter);

		ALuint srcId = playable->GetSource();
		m_SourcePool.push_back(srcId);
		playable->SetSource(AL_NONE);
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_OpenALRenderer::Update(u32 inDeltaTime)
	{
		//@ UPDATE ACTIVE SOUNDS/MUSICS
		T_SoundList::iterator iter = m_ActiveSounds.begin();
		T_SoundList::iterator nextIter = iter;
		while (iter != m_ActiveSounds.end())
		{
			advance(nextIter, 1);
			//@ update buffers
			(*iter)->UpdateBuffers(inDeltaTime);
			
			//@ Next
			iter = nextIter;
		}


		//@ UPDATE ACTIVE INSTANCES
		T_PlayableList::iterator iter_ = m_ActiveInstances.begin();
		T_PlayableList::iterator nextIter_ = iter_;
		while (iter_ != m_ActiveInstances.end())
		{
			advance(nextIter_, 1);
			//@ update buffers
			(*iter_)->UpdateInstanceBuffers(*iter_);

			//@ Next
			iter_ = nextIter_;
		}
	}

}}