/*
* filename:			C_Music.cpp
*
* author:			Kral Jozef
* date:				03/08/2011		10:50
* version:			1.00
* brief:
*/

#include "C_Music.h"
#include <SysUtils/C_TraceClient.h>
#include "C_ClientUtils.h"
#include "C_OpenALRenderer.h"

namespace scorpio{ namespace sysaudio{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Music::~C_Music()
	{
		Clear();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Music::Clear()
	{
		if (!m_Prepared)
			return;

		m_Stream.Close();

		if (m_SourceId != AL_NONE)
			SetSource(AL_NONE);

		alDeleteBuffers(NUM_BUFFERS, m_Buffers);
		C_ClientUtils::CheckError("alDeleteBuffers");

		m_Prepared = false;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Load
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Music::Load(const sysutils::T_Stream inputStream, bool bLoop /* = false */, float fVolume /* = 1.f */)
	{
		m_Volume = fVolume;
		m_Loop = bLoop;

		if (!m_Stream.Open(inputStream))
		{
			if (inputStream)
			{
				TRACE_FE("Can not open stream: %s", inputStream->GetFileName());
				return false;
			}
			return false;
		}

		//////////////////////////////////////////////////////////////////////////
		alGenBuffers(NUM_BUFFERS, m_Buffers);

		C_ClientUtils::CheckError("alGenBuffers");

		m_Prepared = true;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Play
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Music::Play()
	{
		if (IsPlaying())
			return;

		if (IsPaused() && m_SourceId != AL_NONE)
		{
			alSourcePlay(m_SourceId);
			m_Paused = false;
			return;
		}

		if (m_SourceId == AL_NONE)
		{
			T_OpenALRenderer::GetInstance().RequestSource(this);
		}

		C_ClientUtils::CheckError("RequestSource");

		alSourcePlay(m_SourceId);

		C_ClientUtils::CheckError("alSourcePlay");
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Pause
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Music::Pause()
	{
		alSourcePause(m_SourceId);
		m_Paused = true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Stop
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Music::Stop()
	{
		if (m_SourceId == AL_NONE)
			return;

		//@ dequeue
		Dequeue();

		//@ seek stream to start
		T_OpenALRenderer::GetInstance().ReleaseSource(this);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsPlaying
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Music::IsPlaying() const
	{
		if (m_SourceId == AL_NONE)
			return false;

		if (IsPaused())
			return false;

		ALint state;
		alGetSourcei(m_SourceId, AL_SOURCE_STATE, &state);
		return state == AL_PLAYING;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Tick
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Music::UpdateBuffers(u32 deltaTime)
	{
		SC_ASSERT(m_SourceId != AL_NONE);

		ALenum state;
		alGetSourcei(m_SourceId, AL_SOURCE_STATE, &state);
		if (state == AL_PAUSED)
			return;

		//@ run out of data?
		//1) application lost focus and there is not prebuffered data
		//2) end of NO LOOPED stream
		if (state == AL_STOPPED)
		{
			if (m_StreamFinished)
			{
				Stop();
				return;
			}
			else
			{
				//clear audio data
				Dequeue();
				//@ fill with next chunk of data
				PrebufferData();

				//@ Play...
				alSourcePlay(m_SourceId);
			}
		}

		int processed;
		alGetSourcei(m_SourceId, AL_BUFFERS_PROCESSED, &processed);
		while(processed-- && !m_StreamFinished)
		{
			ALuint buffer;

			alSourceUnqueueBuffers(m_SourceId, 1, &buffer);
			C_ClientUtils::CheckError("alSourceUnqueueBuffers");

			m_Stream.Stream(buffer, m_Loop, m_StreamFinished);
			SC_ASSERT(!(m_StreamFinished && m_Loop));

			alSourceQueueBuffers(m_SourceId, 1, &buffer);
			C_ClientUtils::CheckError("alSourceQueueBuffers");
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetVolume
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Music::SetVolume(float fVolume)
	{
		I_Playable::SetVolume(fVolume);
		SC_ASSERT(m_Prepared);
		if (m_SourceId != AL_NONE)
			alSourcef(m_SourceId, AL_GAIN, m_Volume * m_VolumeMultiplier);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ PrebufferData
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Music::PrebufferData()
	{
		u32 i = 0;
		while( i < NUM_BUFFERS && !m_StreamFinished)
		{
			if (m_Stream.Stream(m_Buffers[i], m_Loop, m_StreamFinished))
			{
				SC_ASSERT(!(m_StreamFinished && m_Loop));
				alSourceQueueBuffers(m_SourceId, 1, &m_Buffers[i++]);
			}
			else
				break;
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetSource
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Music::SetSource(u32 srcId)
	{
		if (srcId != AL_NONE)
		{
			// Attach new source
			m_SourceId = srcId;

			// Load audio data onto source
			PrebufferData();

			// Init source properties
			alSourcef(m_SourceId, AL_PITCH, 1.0f);
			alSourcef(m_SourceId, AL_GAIN, m_Volume);
			alSourcei(m_SourceId, AL_LOOPING, AL_FALSE);	//streamed sound can not be Looped - this loop just currently played buffer

			alSource3f(m_SourceId, AL_POSITION,        0.0, 0.0, 0.0);
			alSource3f(m_SourceId, AL_VELOCITY,        0.0, 0.0, 0.0);
			alSource3f(m_SourceId, AL_DIRECTION,       0.0, 0.0, 0.0);
			alSourcef (m_SourceId, AL_ROLLOFF_FACTOR,  0.0          );
			alSourcei (m_SourceId, AL_SOURCE_RELATIVE, AL_TRUE      );
		}
		else
		{
			// Need to stop & dequeue
			Dequeue();

			// Attach new source
			m_SourceId = srcId;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Dequeue
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Music::Dequeue()
	{
		/** Check current state
		@remarks
			Fix for bug where prebuffering a streamed sound caused a buffer problem
			resulting in only 1st buffer repeatedly looping. This is because alSourceStop() 
			doesn't function correctly if the sources state hasn't previously been set!!???
		*/
		ALenum state;
		alGetSourcei(m_SourceId, AL_SOURCE_STATE, &state);

		// Force mSource to change state so the call to alSourceStop() will mark buffers correctly.
		if (state == AL_INITIAL)
			alSourcePlay(m_SourceId);

		alSourceStop(m_SourceId);

		// Unqueue buffer
		// Get number of buffers queued on source

		int processed;
		alGetSourcei(m_SourceId, AL_BUFFERS_PROCESSED, &processed);

		while (processed--)
		{
			ALuint buffer;
			// Remove number of buffers from source
			alSourceUnqueueBuffers(m_SourceId, 1, &buffer);
			C_ClientUtils::CheckError("alSourceUnqueueBuffers");
		}
	}

}}