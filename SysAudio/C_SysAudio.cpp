/*
* filename:			C_SysAudio.cpp
*
* author:			Kral Jozef
* date:				03/07/2011		9:33
* version:			1.00
* brief:
*/

#include "C_SysAudio.h"
#include "C_OpenALRenderer.h"
#include <SysUtils/C_TraceClient.h>
#include <SysAudio/C_AudioManager.h>

namespace scorpio{ namespace sysaudio{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_SysAudio::Initialize()
	{
		bool bRes = T_OpenALRenderer::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create OpenALRenderer singleton instance!");
			return false;
		}

		bRes = T_OpenALRenderer::GetInstance().Initialize();
		if (!bRes)
		{
			TRACE_E("Failed to Initialize OpenALRenderer!");
			return false;
		}

		bRes = T_AudioManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Failed to Create AudioManager!");
			return false;
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Deinitialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_SysAudio::Deinitialize()
	{
		T_AudioManager::DestroyInstance();

		bool bRes = T_OpenALRenderer::GetInstance().Deinitialize();
		if (!bRes)
		{
			TRACE_E("Failed to Deinitialize OpenALRenderer.");
		}
		T_OpenALRenderer::DestroyInstance();
		return true;
	}

}}