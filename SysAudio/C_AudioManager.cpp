/*
* filename:			C_AudioManager.cpp
*
* author:			Kral Jozef
* date:				03/08/2011		13:28
* version:			1.00
* brief:
*/

#include "C_AudioManager.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/Xml/C_XmlDocument.h>
#include <SysUtils/C_HashName.h>
#include "C_OpenALRenderer.h"
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace sysaudio{

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ fileName
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AudioManager::LoadSounds(const sysutils::T_Stream inputStream)
	{
		//parse xml and load/insert fonts into manager
		C_XmlDocument xmlDoc;
		bool bRes = xmlDoc.Load(inputStream);
		if (!bRes)
			return false;

		T_XmlNodeList nodeList = xmlDoc.SelectNodes("SoundDefinitionFile/Sounds/sound");
		for (T_ConstXmlNodePtrVector::iterator iter = nodeList->begin(); iter != nodeList->end(); ++iter)
		{
			const C_XmlNode * node = (*iter);
			C_String str = (*node)["uid"];
			T_Sound sound = CreateSoundFromXmlNode(*node);
			if (!sound)
				continue;
			
			m_SoundContainer.insert(T_SoundMap::value_type(C_HashName(str).GetHash(), sound));
		}

		nodeList = xmlDoc.SelectNodes("SoundDefinitionFile/Music/music");
		for (T_ConstXmlNodePtrVector::iterator iter = nodeList->begin(); iter != nodeList->end(); ++iter)
		{
			const C_XmlNode * node = (*iter);
			C_String str = (*node)["uid"];
			C_String strFileName = (*node)["file"];
			if (str.empty() || strFileName.empty())
			{
				TRACE_FE("No Id: %s or No music fileName: %s", str.c_str(), strFileName.c_str());
				continue;
			}
			
			T_Music music = T_Music(new C_Music(C_HashName(str).GetHash()));

			T_Stream stream = T_FileSystemManager::GetInstance().Open(strFileName.c_str());
			bool bRes = music->Load(stream);
			if (!bRes)
				continue;

			C_String str_ = (*node)["volMult"];
			if (!str_.empty())
			{
				float fVal = str_.ToFloat();
				music->SetVolumeMult(fVal);
			}
			m_MusicContainer.insert(T_MusicMap::value_type(C_HashName(str).GetHash(), music));
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadSounds
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AudioManager::LoadSounds(const T_XmlNodeList soundsNodeList)
	{
		SC_ASSERT(soundsNodeList);
		for (T_ConstXmlNodePtrVector::const_iterator iter = soundsNodeList->begin(); iter != soundsNodeList->end(); ++iter)
		{
			const C_XmlNode * soundNode = *iter;
			SC_ASSERT(soundNode);
			if (!soundNode || !soundNode->IsValid())
			{
				TRACE_E("INVALID sound node in soundNodeList.");
				continue;
			}

			C_String str = (*soundNode)["uid"];
			T_Sound sound = CreateSoundFromXmlNode(*soundNode);
			if (!sound)
				continue;

			m_SoundContainer.insert(T_SoundMap::value_type(C_HashName(str).GetHash(), sound));
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateSoundFromXmlNode
	//
	//////////////////////////////////////////////////////////////////////////
	T_Sound C_AudioManager::CreateSoundFromXmlNode(const C_XmlNode & soundNode)
	{
		T_Sound sound = T_Sound(new C_Sound());
		C_String strFileName = soundNode["file"];

		T_Stream stream = T_FileSystemManager::GetInstance().Open(strFileName.c_str());
		bool bRes = false;
		if (stream)
			bRes = sound->Load(stream);

		if (!bRes)
		{
			TRACE_FE("Can not open audio stream: %s", strFileName.c_str());
			return T_Sound();
		}

		C_String str_ = soundNode["volMult"];
		if (!str_.empty())
		{
			float fVal = str_.ToFloat();
			sound->SetVolumeMult(fVal);
		}

		return sound;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddMusic
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AudioManager::AddMusic(T_Music music)
	{
		std::pair<T_MusicMap::iterator, bool> ret = m_MusicContainer.insert(T_MusicMap::value_type(music->GetID(), music));
		if (!ret.second)
		{
			TRACE_FE("Music with same id already in container: %d", music->GetID());
			return false;
		}

		music->SetVolume(m_MusicVolume);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RemoveMusic
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AudioManager::RemoveMusic(T_Hash32 musicID)
	{
		T_MusicMap::iterator iter = m_MusicContainer.find(musicID);
		if (iter == m_MusicContainer.end())
		{
			TRACE_FE("Music with id: %d is not in manager", musicID);
			return false;
		}

		m_MusicContainer.erase(iter);
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ PlaySound
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AudioManager::PlaySoundEx(T_Hash32 soundID)
	{
		T_Sound snd = GetSound(soundID);
		if (snd)
		{
			//if (!snd->IsPlaying())
				snd->Play();
			return true;
		}

		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetSound
	//
	//////////////////////////////////////////////////////////////////////////
	T_Sound C_AudioManager::GetSound(T_Hash32 soundID)
	{
		T_SoundMap::iterator iter = m_SoundContainer.find(soundID);
		if (iter != m_SoundContainer.end())
			return iter->second;

		TRACE_FE("Sound with ID: %d is not in manager!", soundID);
		return T_Sound();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetMusic
	//
	//////////////////////////////////////////////////////////////////////////
	T_Music C_AudioManager::GetMusic(T_Hash32 musicID)
	{
		T_MusicMap::iterator iter = m_MusicContainer.find(musicID);
		if (iter != m_MusicContainer.end())
			return iter->second;

		TRACE_FE("Music with ID: %d is not in manager!", musicID);
		return T_Music();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetMusicVolume
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AudioManager::SetMusicVolume(float fVal)
	{
		m_MusicVolume = fVal;
		for (T_MusicMap::iterator iter = m_MusicContainer.begin(); iter != m_MusicContainer.end(); ++iter)
		{
			iter->second->SetVolume(m_MusicVolume);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetSoundVolume
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AudioManager::SetSoundVolume(float fVal)
	{
		m_SoundVolume = fVal;
		for (T_SoundMap::iterator iter = m_SoundContainer.begin(); iter != m_SoundContainer.end(); ++iter)
		{
			iter->second->SetVolume(m_SoundVolume);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AudioManager::Update(u32 inDeltaTime)
	{
		T_OpenALRenderer::GetInstance().Update(inDeltaTime);
	}
}}