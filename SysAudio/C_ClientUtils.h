/*
* filename:			C_ClientUtils.h
*
* author:			Kral Jozef
* date:				03/08/2011		11:52
* version:			1.00
* brief:
*/

#pragma once

#include <Common/types.h>

namespace scorpio{ namespace sysaudio{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ClientUtils
	class C_ClientUtils
	{
	public:
		//@ CheckErr - check OpenAL error
		static bool				CheckError(const char * funcName);
		//@ ValidateBuffer - check buffer validity
		static bool				ValidateBuffer(u32 bufferId);
	};



}}