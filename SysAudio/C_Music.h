
/*
* filename:			C_Music.h
*
* author:			Kral Jozef
* date:				03/08/2011		9:08
* version:			1.00
* brief:				Music is always looped, when we get to the end of the stream, we seek to it's start again
*/

#pragma once

#include <SysAudio/SysAudioApi.h>
#include "I_Playable.h"
#include "C_OggStream.h"
#include <boost/shared_ptr.hpp>
#include <Common/macros.h>

namespace scorpio{ namespace sysaudio{

	#define NUM_BUFFERS 4

	//////////////////////////////////////////////////////////////////////////
	//@ C_Music
	class SYSAUDIO_API C_Music : public I_Playable
	{
	public:
		//@ c-tor
		C_Music(T_Hash32 musicID)
			: m_MusicID(musicID), m_Prepared(false), m_Paused(false), m_SourceId(AL_NONE), m_StreamFinished(false)
		{
			ZERO_ARRAY(m_Buffers);
		};
		//@ d-tor
		~C_Music();

		//@ Load
		virtual bool				Load(const sysutils::T_Stream inputStream, bool bLoop = false, float fVolume = 1.f);
		//@ LoadStream/PlayStream/Stop/SetVolume/SetPitch/SetPosition
		virtual void				Play();
		//@ Pause
		virtual void				Pause();
		//@ Stop
		virtual void				Stop();
		//@ IsPlaying()
		virtual bool				IsPlaying() const;
		//@ Tick - for updating fader, buffering stream,...
		virtual void				UpdateBuffers(u32 deltaTime);

		//@ SetVolume
		virtual void				SetVolume(float fVolume);
		//@ SetLooped
		//void							SetLooped(bool bLooped) { m_Looped = bLooped; }
		//@ GetID
		T_Hash32						GetID() const { return m_MusicID; }

		//@ SetSource
		void							SetSource(u32 srcId);
		//@ GetSource
		u32							GetSource() const { return m_SourceId; }

	private:
		//@ Clear
		void							Clear();
		//@ IsPaused
		bool							IsPaused() const { return m_Paused; }
		//@ PrebufferData
		void							PrebufferData();
		//@ Dequeue
		void							Dequeue();


	private:
		T_Hash32			m_MusicID;
		bool				m_Prepared;
		bool				m_Paused;
		C_OggStream		m_Stream;	//ogg stream
		ALuint			m_Buffers[NUM_BUFFERS];	//Front/Back
		ALuint			m_SourceId;
		bool				m_StreamFinished;
	};

	typedef boost::shared_ptr<C_Music>	T_Music;

}}