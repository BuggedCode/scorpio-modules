/*
* filename:			C_OpenALRenderer.h
*
* author:			Kral Jozef
* date:				03/07/2011		9:36
* version:			1.00
* brief:
*/

#pragma once

#include <Common/PlatformDef.h>

#ifdef PLATFORM_WIN32
   #include <al.h>
   #include <alc.h>
#elif defined PLATFORM_MACOSX
   #include <OpenAL/al.h>
   #include <OpenAL/alc.h>
#endif

#include <SysAudio/SysAudioApi.h>
#include <Common/C_Singleton.h>
#include <vector>
#include <SysUtils/C_String.h>
#include "I_Playable.h"
#include <list>

namespace scorpio{ namespace sysaudio{

	//////////////////////////////////////////////////////////////////////////
	//@ C_OpenALRenderer
	class SYSAUDIO_API C_OpenALRenderer
	{
		friend class C_Singleton<C_OpenALRenderer>;

	public:
		//@ Initialize
		bool				Initialize();
		//@ Deinitialize
		bool				Deinitialize();
		//@ RequestSource
		ALuint			RequestSource(I_Playable * playable);
		//@ RequestSource
		ALuint			RequestSource(T_Playable playable);
		//@ ReleaseSource
		void				ReleaseSource(I_Playable * playable);
		//@ ReleaseSource
		void				ReleaseSource(T_Playable playable);
		//@ Update
		void				Update(u32 inDeltaTime);


	private:
		//@ c-tor
		C_OpenALRenderer()
			: m_Device(NULL), m_Context(NULL), m_Error(0) {};
		//@ d-tor
		~C_OpenALRenderer() {};

		//@ GetDeviceList
		void				GetDeviceList(sysutils::T_StringVector & outDeviceList);
		//@ CreateSourcePool
		//@ ret Number of sources created <= nMaxSources, nMaxSources = Maximum Number of sources to allocate
		u32				CreateSourcePool(u32 nMaxSources);

	private:
		ALCdevice	*	m_Device;
		ALCcontext	*	m_Context;

		ALenum			m_Error;

		typedef std::vector<ALuint>		T_SourceList;
		T_SourceList	m_SourcePool;	//sourcePool
		u32				m_SourceCount;	//Number of sources available for sounds
		typedef std::list<I_Playable*>	T_SoundList;
		T_SoundList		m_ActiveSounds;
		typedef std::list<T_Playable>		T_PlayableList;	//InstanceList
		T_PlayableList	m_ActiveInstances;
	};


	typedef C_Singleton<C_OpenALRenderer>	T_OpenALRenderer;

}}

SYSAUDIO_EXP_TEMP_INST template class SYSAUDIO_API C_Singleton<scorpio::sysaudio::C_OpenALRenderer>;