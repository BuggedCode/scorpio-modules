#pragma once

#include <Common/Pragmas.h>

#ifdef _USRDLL
	#ifdef SYSAUDIO_EXPORTS
		#define SYSAUDIO_API __declspec(dllexport)
		#define SYSAUDIO_EXP_TEMP_INST
	#else
		#define SYSAUDIO_API __declspec(dllimport)
		#define SYSAUDIO_EXP_TEMP_INST extern
	#endif
#else
	#define SYSAUDIO_API
	#define SYSAUDIO_EXP_TEMP_INST
#endif
	