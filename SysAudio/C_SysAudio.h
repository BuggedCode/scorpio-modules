/*
* filename:			C_SysAudio.h
*
* author:			Kral Jozef
* date:				03/07/2011		9:33
* version:			1.00
* brief:
*/

#pragma once

#include <SysAudio/SysAudioApi.h>


namespace scorpio{ namespace sysaudio{

	//////////////////////////////////////////////////////////////////////////
	//@ C_SysAudio
	class SYSAUDIO_API C_SysAudio
	{
	public:
		//@ Initialize module
		static bool			Initialize();
		//@ Deinitialize module
		static bool			Deinitialize();
	};

}};