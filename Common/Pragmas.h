/*
* filename:			Pragmas.h
*
* author:			Kral Jozef
* date:				02/07/2008		20:59
* version:			1.00
* brief:
*/

#pragma once

#pragma warning( disable : 4251 )
#pragma warning( disable : 4275 )
#pragma warning( disable : 4231 )	//warning C4231: nonstandard extension used : 'extern' before template explicit instantiation
#pragma warning( disable : 4311 )	//disable Wp64