/*
* filename:			GlobalDefines.h
*
* author:			Kral Jozef
* date:				03/17/2009		22:16
* version:			1.00
* brief:
*/

#pragma once

//#define USE_STD_STRING
#undef USE_STD_STRING

//@ podpora tracu
#define _TRACELOG_
#define SC_USE_NEW_ALLOCATOR	//use our new allocator defined in globalNewDelete
#define SC_LOG_STATISTICS_EASY
#define SC_LOG_STATISTICS_FULL

//@ enable collecting statistics about allocated sizes on the heap
//#define SC_COLLECT_SIZE_STATISTICS

#ifdef _USE_MOUNT_POINT
#define USE_MOUNT_POINT
#endif

//@ enable profiling 
#ifdef _SC_PROFILER_ENABLE
   #define SC_PROFILER_ENABLE
#endif

//#define USE_PTK