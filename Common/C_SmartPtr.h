/*
* filename:			SmartPtr.h
*
* author:			Kral Jozef aka BuggedCode
* date:				2/8/2012		15:08
* version:			1.00
* brief:			Just copy/modify form scorpio engine
*/

#pragma once

#include <Common/Types.h>
#include <Common/Macros.h>

	//////////////////////////////////////////////////////////////////////////
	//@ C_Deleter
	template<class T>
	class C_Deleter
	{
	public:
		void operator() (T * ptr) 
		{
			SC_SAFE_DELETE(ptr);
		}
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_RefCounterBase
	class C_RefCounterBase
	{
	public:
		//@ AddRef
		void			AddRef() const { ++m_RefCount; }
		//@ RemoveRef - ret true if m_RefCount > 0
		bool			RemoveRef() const 
		{ 
			if (--m_RefCount == 0) 
			{ 
				delete this; 
				return false; 
			} 
			else 
				return true; 
		}
		//@ GetRef
		int				GetRef() const { return m_RefCount; }

	protected:
		//@ c-tor
		C_RefCounterBase() : m_RefCount(0) {};
		//@ d-tor
		virtual ~C_RefCounterBase() {}
	
	private:
		mutable int		m_RefCount;
	};

	//////////////////////////////////////////////////////////////////////////
	//@ RefCounter
	template<typename T, class D = C_Deleter<T> >
	class C_RefCounter : public C_RefCounterBase
	{
	public:
		//@ c-tor
		C_RefCounter(T * ptr, D deleter) : m_Ptr(ptr), m_Deleter(deleter) {};
		//@ d-tor
		~C_RefCounter() { m_Deleter(m_Ptr); }

		T	*	m_Ptr;
		D		m_Deleter;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_SharedPtr
	//@ non intrusive version of ref counted smart ptr
	//@ D - deleter
	template<typename T, class D = C_Deleter<T> >
	class C_SharedPtr
	{
	public:
		typedef T	ElementType;
		//@ c-tor
		explicit C_SharedPtr(T * rawPtr = NULL, D deleter = D());
		//@ copy c-tor
		C_SharedPtr(const C_SharedPtr & copy);
		//@ d-tor
		~C_SharedPtr();

		//@ implicit conversions
		//operator T*() const;
		T &		operator*() const { return *m_RefCounter->m_Ptr; }
		T *		operator->() const { return m_RefCounter->m_Ptr; }
		//bool	operator!() const { return !m_RefCounter->m_Ptr}

		//@ assignment op
		//C_SharedPtr	&	operator=(T * rawPtr);
		C_SharedPtr	&	operator=(const C_SharedPtr & smartPtr)
		{
			if (m_RefCounter != smartPtr.m_RefCounter)
			{
				m_RefCounter->RemoveRef();
				m_RefCounter = smartPtr.m_RefCounter;
				Init();
			}

			return *this;
		}


		template<typename Y>
		C_SharedPtr	&	operator=(const C_SharedPtr<Y> & smartPtr)
		{
			if (m_RefCounter != smartPtr->m_RefCounter)
			{
				m_RefCounter->RemoveRef();
				m_RefCounter = smartPtr->m_RefCounter;
				Init();
			}

			return *this;
		}


		//@ comparision
		bool	operator==(T * rawPtr) const { return rawPtr == m_RefCounter->m_Ptr; }
		bool	operator!=(T * rawPtr) const { return rawPtr != m_RefCounter->m_Ptr; }
		bool	operator==(const C_SharedPtr & smartPtr) const { return smartPtr.m_RefCounter->m_Ptr == m_RefCounter->m_Ptr; }
		bool	operator!=(const C_SharedPtr & smartPtr) const { return smartPtr.m_RefCounter->m_Ptr == m_RefCounter->m_Ptr; }
		bool	operator!() { return (m_RefCounter->m_Ptr == NULL); }

		//@ bool conversion operator
		operator bool() const { return (m_RefCounter->m_Ptr != NULL); }

		//@ reset
		void	reset()
		{ 
			if (m_RefCounter)
			{
				m_RefCounter->RemoveRef();
				m_RefCounter = new C_RefCounter<T, D>(NULL, D());
			}
		}

		//@ get - use wisely!!!!
		T *		get() const { return m_RefCounter->m_Ptr; }


	private:
		//@ Init
		void	Init() { m_RefCounter->AddRef(); }

	private:
		C_RefCounter<T, D>	*	m_RefCounter;

	};



	//////////////////////////////////////////////////////////////////////////
	//
	//@ IMPLEMENTATION
	//
	//////////////////////////////////////////////////////////////////////////
	
	template<typename T, class D>
	C_SharedPtr<T, D>::C_SharedPtr(T * rawPtr, D deleter)
		: m_RefCounter(new C_RefCounter<T, D>(rawPtr, deleter))
	{
		Init();
	}

	template<typename T, class D>
	C_SharedPtr<T,D>::C_SharedPtr(const C_SharedPtr & copy)
	{
		m_RefCounter = copy.m_RefCounter;
		Init();
	}

	template<typename T, class D>
	C_SharedPtr<T, D>::~C_SharedPtr() 
	{ 
		if(m_RefCounter)
		{
			if (m_RefCounter->RemoveRef() == false)
				m_RefCounter = NULL;
		}
	}
