/*
* filename:			macros.h
*
* author:			Kral Jozef
* date:				Januar 2007
* version:			1.00
* description:		common macros
*/

#pragma once

#include <stdlib.h>
#include <memory.h>
#include "PlatformDef.h"

#ifdef PLATFORM_WIN32

   #define ARRAY_SIZE(array)				_countof(array)/*sizeof(array)/sizeof(*array)*/
   #define ARRAY_SIZE_IN_BYTES(array)	_countof(array) *sizeof(array[0])
   #define ZERO_STRUCT(struct)			memset(&struct, 0, sizeof(struct))
   #define ZERO_ARRAY(array)				memset(array, 0, _countof(array) * sizeof(array[0]))
   #define ZERO_MEMORY(memory, size)	memset(memory, 0, size)

   #define MAX(left, right)            __max(left, right)
   #define MIN(left, right)            __min(left, right)

   #define STR_CPY(dstStr, srcStr, length)		strcpy_s(dstStr, length, srcStr)
   #define MEM_CPY(dst, dstLen, src, srcLen)		memcpy_s(dst, dstLen, src, srcLen)
	#define SPRINTF(dstBuf, dstSize, ...)			sprintf_s(dstBuf, dstSize, __VA_ARGS__)
	#define VSNPRINTF(dstBuffer, dstSize, ...)	vsnprintf_s(dstBuffer, dstSize, dstSize - 1, __VA_ARGS__)
	#define STRLWR(str, length)						_strlwr_s(str, length)
	#define STRUPR(str, length)						_strupr_s(str, length)

   
#elif defined PLATFORM_MACOSX

   #define _countof(a) (sizeof(a)/sizeof(*(a)))

   #define ARRAY_SIZE(array)				_countof(array)/*sizeof(array)/sizeof(*array)*/
   #define ARRAY_SIZE_IN_BYTES(array)	_countof(array) *sizeof(array[0])
   #define ZERO_STRUCT(struct)			memset(&struct, 0, sizeof(struct))
   #define ZERO_ARRAY(array)				memset(array, 0, _countof(array) * sizeof(array[0]))
   #define ZERO_MEMORY(memory, size)	memset(memory, 0, size)

   #define MAX(left, right)            ((left) > (right) ? (left) : (right))
   #define MIN(left, right)            ((left) <= (right) ? (left) : (right))

   #define STR_CPY(dstStr, srcStr, size)			strcpy(dstStr, srcStr)
   #define MEM_CPY(dst, dstLen, src, srcLen)    memcpy(dst, src, srcLen)
	#define SPRINTF(dstBuf, dstSize, ...)			sprintf(dstBuf, __VA_ARGS__)
	#define VSNPRINTF(dstBuffer, dstSize, ...)	vsnprintf(dstBuffer, dstSize - 1, __VA_ARGS__)
	#define STRLWR(str, length)						strlwr(str, length)
	#define STRUPR(str, length)						_strupr(str, length)



#endif


//////////////////////////////////////////////////////////////////////////
//@ memory
#define SC_SAFE_DELETE(pointer)				{ delete pointer; pointer = 0; }
#define SC_SAFE_DELETE_ARRAY(array)			{ delete[] array; array = 0; }

#define SC_DEPRECATED						__declspec(deprecated("deprecated function"))
