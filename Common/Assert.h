/*
* filename:			Assert.h
*
* author:			Kral Jozef
* date:				02/24/2008		22:21
* version:			1.00
* brief:				asserts
*/

#pragma once

#include "PlatformDef.h"

#ifdef PLATFORM_WIN32
   #include <crtdbg.h>
   #include <boost/static_assert.hpp>

#define SC_CTASSERT(expr) BOOST_STATIC_ASSERT(expr); 

#elif defined PLATFORM_MACOSX
   #include <assert.h>
   #define SC_CTASSERT(expr) SC_ASSERT(expr) //Runtime assert
#endif


//////////////////////////////////////////////////////////////////////////
//@ assert
#if defined (_DEBUG)|| (USE_ASSERT) || (DEBUG)
	#ifdef MANAGED_CODE
		void _trace(char *fmt, ...);
		#ifndef SC_ASSERT
			#define SC_ASSERT(x)
		#endif
		#ifndef SC_VERIFY
			#define SC_VERIFY(x)
		#endif

	#else
		void _trace(char *fmt, ...);
		#ifndef SC_ASSERT
         #ifdef PLATFORM_WIN32
            #define SC_ASSERT(x) {if(!(x)) __asm{int 0x03}}
         #elif defined PLATFORM_MACOSX
            #define SC_ASSERT(x) assert(x)
         #endif
		#endif
		#ifndef SC_VERIFY
			#define SC_VERIFY(x) {if(!(x)) _asm{int 0x03}}
		#endif
	#endif
#else
	#ifndef SC_ASSERT
		#define SC_ASSERT(x)
	#endif
	#ifndef SC_VERIFY
		#define SC_VERIFY(x) x
	#endif
#endif