/*
* filename:			I_NonCopyAble.h
*
* author:			Kral Jozef
* date:				03/24/2009		20:33
* version:			1.00
* brief:				interface for objects which can not copyable
*/

#pragma once

//////////////////////////////////////////////////////////////////////////
//@ C_NonCopyAble
class I_NonCopyAble
{
public:
	//@ c-tor
	I_NonCopyAble() {};
	//@ d-tor
	virtual ~I_NonCopyAble() {};

private:
	//@ copy c-tor
	I_NonCopyAble(const I_NonCopyAble&);	//not implemented!
	//@ assignment operator
	const I_NonCopyAble & operator=(const I_NonCopyAble&);	//not implemented
};
