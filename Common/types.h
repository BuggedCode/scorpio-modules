/*
* filename:			Types.h
*
* author:			Kral Jozef
* date:				Januar 2007
* version:			1.00
* description:		common types
*/

#pragma once

#include "PlatformDef.h"

#ifdef USE_SDL
	
   #include <SDL_stdinc.h>

	typedef Uint8		u8;
	typedef Uint16		u16;
	typedef Uint32		u32;
	typedef Uint64		u64;

	typedef Sint8		s8;
	typedef Sint16		s16;
	typedef Sint32		s32;
	typedef Sint64		s64;

#elif defined PLATFORM_MACOSX

   #include <stdint.h>

   typedef uint8_t   u8;
   typedef uint16_t  u16;
   typedef uint32_t  u32;
   typedef uint64_t  u64;

   typedef int8_t    s8;
   typedef int16_t   s16;
   typedef int32_t   s32;
   typedef int64_t   s64;

#include <stdio.h>   //size_t / NULL

#elif defined PLATFORM_WIN32

	typedef unsigned __int8		u8;
	typedef unsigned __int16	u16;
	typedef unsigned __int32	u32;
	typedef unsigned __int64	u64;

	typedef signed __int8	s8;
	typedef signed __int16	s16;
	typedef signed __int32	s32;
	typedef signed __int64	s64;

   #ifdef NULL
      #undef NULL
      #define NULL 0L
   #else
		#define NULL 0L
	#endif

#endif


typedef u16			T_Id16;
typedef u32			T_Id32;
typedef u32			T_Hash32;
typedef u64			T_Hash64;
typedef u32			T_RenderLayer;
