/*
* filename:			I_BaseObject.cpp
*
* author:			Kral Jozef
* date:				02/11/2008		19:25
* version:			1.00
* brief:				base class for RTTI objects - define base of rtti
*/

#include "Pragmas.h"
#include "I_BaseObject.h"
#include "C_RTTI.h"

DEFINE_ROOT_RTTI( I_BaseObject )