/*
* filename:			PointerTraits.h
*
* author:			Kral Jozef thnx to MBlazek
* date:				06/04/2008		23:36
* version:			1.00
* brief:				http://www.boost.org/doc/libs/1_38_0/libs/type_traits/doc/html/index.html
*/

#pragma once

namespace typetraits{

	//////////////////////////////////////////////////////////////////////////
	//
	// is_pointer
	//
	//////////////////////////////////////////////////////////////////////////
	template<typename T>
	struct is_pointer
	{
		enum{ value = false };
	};

	//partial specialisation for pointer types
	template<typename T>
	struct is_pointer<T*>
	{
		enum{ value = true };
	};

	//////////////////////////////////////////////////////////////////////////
	//
	// remove_pointer
	//
	//////////////////////////////////////////////////////////////////////////
	template<typename T>
	struct remove_pointer
	{
		typedef T	type;
	};

	//partial specialisation for pointer types
	template<typename T>
	struct remove_pointer<T*>
	{
		typedef T	type;
	};

}
