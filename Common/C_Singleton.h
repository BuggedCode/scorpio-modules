/*
* filename:			C_Singleton.h
*
* author:			Kral Jozef
* date:				02/03/2008		13:36
* version:			1.00
* brief:			Just copy/modify form scorpio engine
*/

#pragma once

#include <Common/I_NonCopyAble.h>
#include <Common/Macros.h>
#include <Common/Assert.h>


template<class classType>
class C_Singleton : public I_NonCopyAble
{
public:
	//@ CreateInstance
	//@ return false if something fail
	static bool				CreateInstance()
	{
		SC_ASSERT(!m_instance);//, "Singleton already created!");

		m_instance = new classType();
		if (!m_instance)
			return false;

		return true;
	}

	//@ GetInstance
	static classType	&	GetInstance()
	{
		SC_ASSERT(m_instance);//, "Singleton not created");
		return *m_instance;
	}

	//@ DestroyInstance
	static void				DestroyInstance()
	{
		SC_SAFE_DELETE(m_instance);
	}

protected:
	//@ c-tor
	C_Singleton() {};
	//@ d-tor
	virtual ~C_Singleton() {};

	static classType	*	m_instance;

};

template <class T> T * C_Singleton<T>::m_instance = NULL;