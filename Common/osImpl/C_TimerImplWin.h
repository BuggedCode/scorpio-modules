/*
* filename:			C_TimerImplWin.h
*
* author:			Kral Jozef
* date:				02/26/2009		21:40
* version:			1.00
* brief:				High-resolution timer implementation Win32
*/

#include "types.h"
#include <windows.h>

#pragma once

//@ nobody cannot acces to this class not exported
class C_TimerImpl
{
public:
	//@ c-tor
	C_TimerImpl()
	{
		LARGE_INTEGER	qpf;
		QueryPerformanceFrequency(&qpf);

		m_freq = qpf.QuadPart;
		m_freqDiv1e6 = (double)m_freq / 1000000.0;
		m_freqDiv1e3 = (double)m_freq / 1000.0;

		LARGE_INTEGER	qpc;
		QueryPerformanceCounter(&qpc);
		m_startTime = qpc.QuadPart;
	}

	//@ d-tor
	~C_TimerImpl() {};


	//@ GetTimeInMicroSeconds
	u64	GetTimeInMicroSeconds()
	{
		LARGE_INTEGER	qpcActual;
		QueryPerformanceCounter(&qpcActual);
		u64 deltaT = qpcActual.QuadPart - m_startTime;
		return (u64)((double)deltaT / m_freqDiv1e6);
	}

	//@ GetTimeInMiliSeconds
	u64	GetTimeInMiliSeconds()
	{
		LARGE_INTEGER	qpcActual;
		QueryPerformanceCounter(&qpcActual);
		u64 deltaT = qpcActual.QuadPart - m_startTime;
		return (u64)((double)deltaT / m_freqDiv1e3);
	}


private:

	u64				m_startTime;		//start time when initialized C_TimerImpl
	u64				m_freq;				//tick per second
	double			m_freqDiv1e6;		//freq for counting in microSeconds
	double			m_freqDiv1e3;		//freq for counting in miliSeconds


};