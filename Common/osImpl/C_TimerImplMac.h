//
//  C_TimerImplMac.h
//  
//
//  Created by jozef pd on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef _C_TimerImplMac_h
#define _C_TimerImplMac_h

#pragma once


//@ nobody cannot acces to this class not exported
class C_TimerImpl
{
public:
	//@ c-tor
	C_TimerImpl()
	{
		/*LARGE_INTEGER	qpf;
		QueryPerformanceFrequency(&qpf);
      
		m_freq = qpf.QuadPart;
		m_freqDiv1e6 = (double)m_freq / 1000000.0;
		m_freqDiv1e3 = (double)m_freq / 1000.0;
      
		LARGE_INTEGER	qpc;
		QueryPerformanceCounter(&qpc);
		m_startTime = qpc.QuadPart;*/
	}
   
	//@ d-tor
	~C_TimerImpl() {};
   
   
	//@ GetTimeInMicroSeconds
	u64	GetTimeInMicroSeconds()
	{
      return 1;
		/*LARGE_INTEGER	qpcActual;
		QueryPerformanceCounter(&qpcActual);
		u64 deltaT = qpcActual.QuadPart - m_startTime;
		return (u64)((double)deltaT / m_freqDiv1e6);*/
	}
   
	//@ GetTimeInMiliSeconds
	u64	GetTimeInMiliSeconds()
	{
      return 1;
		/*LARGE_INTEGER	qpcActual;
		QueryPerformanceCounter(&qpcActual);
		u64 deltaT = qpcActual.QuadPart - m_startTime;
		return (u64)((double)deltaT / m_freqDiv1e3);*/
	}
   
   
private:
   
	u64				m_startTime;		//start time when initialized C_TimerImpl
	u64				m_freq;				//tick per second
	double			m_freqDiv1e6;		//freq for counting in microSeconds
	double			m_freqDiv1e3;		//freq for counting in miliSeconds
};



#endif
