/*
* filename:			STLTypes.h
*
* author:			Kral Jozef aka BuggedCode
* date:				22/2/2014		18:07
* version:			1.00
* brief:
*/

#pragma once

#include <string>
#include <vector>
#include <map>
#include "Types.h"

typedef std::vector<std::string>	T_StringVector;
typedef std::map<T_Hash32, std::string>	T_HashStringMap;
