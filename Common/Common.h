/*
* filename:			Common.h
*
* author:			Kral Jozef
* date:				Januar 2007
* version:			1.00
* description:		common
*/

#pragma once

#include "GlobalDefines.h"
#include "Pragmas.h"
#include "Assert.h"
#include "macros.h"
#include "types.h"
#include "PlatformDef.h"