/*
* filename:			C_Timer.cpp
*
* author:			Kral Jozef
* date:				02/26/2009		21:21
* version:			1.00
* brief:				High resolution timer
*/

#include "C_Timer.h"

#ifdef PLATFORM_WIN32
	#include "osImpl/C_TimerImplWin.h"
#elif defined PLATFORM_MACOSX
   #include "osImpl/C_TimerImplMac.h"
#endif

//@ global hidden object for timer implementation
static C_TimerImpl	g_Timer;

u64 C_Timer::GetTimeInMicroSeconds()
{
	return g_Timer.GetTimeInMicroSeconds();
}

u64 C_Timer::GetTimeInMiliSeconds()
{
	return g_Timer.GetTimeInMiliSeconds();
}
