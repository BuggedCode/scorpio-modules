/*
* filename:			Limits.h
*
* author:			Kral Jozef
* date:				1/17/2012		14:18
* version:			1.00
* brief:
*/

#pragma once

#include "PlatformDef.h"

#ifdef PLATFORM_WIN32

   #define SC_MAX_PATH		_MAX_PATH

#elif defined PLATFORM_MACOSX

   #include <sys/syslimits.h>
	#define SC_MAX_PATH     PATH_MAX

#endif
