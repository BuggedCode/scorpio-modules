/*
* filename:			C_Timer.h
*
* author:			Kral Jozef
* date:				02/26/2009		20:55
* version:			1.00
* brief:				High resolution timer
*/

#pragma once

#include "CommonApi.h"
#include "types.h"

//@ static class
class COMMON_API C_Timer
{
public:

	//@ returns time in platform native type (is the fastest way), do not expect any concrete type
	//static T_TimerVal		GetTime();

	//@ returns time in microseconds
	static u64				GetTimeInMicroSeconds();

	//@ returns time in microseconds
	static u64				GetTimeInMiliSeconds();


	//@ Be sure to not call ConvertToMicroSeconds(GetTime())! Use it just on intervals ( ConvertToMicroSeconds(begin-GetTime()) otherwise you can loose precision!
	//static u32				ConvertToMicroSeconds(const T_TimerFastVal &time);

private:
	//@ c-tor - not implemented
	C_Timer();
	//@ copy - ctor - not implemented
	C_Timer(const C_Timer & copy);
	//@ assignment operator - not implemented
	C_Timer & operator=(const C_Timer & copy);

};