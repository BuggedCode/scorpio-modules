/*
* filename:			PlatformDef.h
*
* author:			Kral Jozef
* date:				02/24/2008		21:39
* version:			1.00
* brief:				Platform definitions
*/

#pragma once

/*
platfoems Win, Linux, Mac
PLATFORM_WIN32
*/

#ifdef WIN32
	#define PLATFORM_WIN32
	//#define RENDER_WIN_GLES_20
	//#define RENDER_WIN_GLES_11
#endif

#ifdef _PLATFORM_MACOSX
   #define PLATFORM_MACOSX
   //#define USE_SDL
#endif
