/*
* filename:			C_RTTI.h
*
* author:			Kral Jozef
* date:				02/07/2008		22:53
* version:			1.00
* brief:				base rtti class in use of manual rtti, base on Medved idea
*/

#pragma once

#include "PointerTraits.h"
#include "Assert.h"
#include "types.h"

class C_RTTI
{
public:
	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_RTTI(const char * className, const C_RTTI * baseClass) : m_className(className), m_baseClass(baseClass) {};

	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	virtual ~C_RTTI() {};

	//////////////////////////////////////////////////////////////////////////
	//@ equal operator
	bool operator==(const C_RTTI & rVal) const
	{
		return this == &rVal;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ GetBaseClass
	const C_RTTI	*	GetBaseClass() const { return m_baseClass; }

	//////////////////////////////////////////////////////////////////////////
	//@ GetClassName
	const char		*	GetClassName() const { return m_className; }

private:
	//////////////////////////////////////////////////////////////////////////
	//@ copy c-tor
	C_RTTI(const C_RTTI & rtti);	//not implemented - disable

	//////////////////////////////////////////////////////////////////////////
	//@ assignment operator
	void	operator=(const C_RTTI & rtti);	//not implemented - disable

	const char		*	m_className;
	const C_RTTI	*	m_baseClass;
};


//////////////////////////////////////////////////////////////////////////
//@ define a declare makro pre odvodene triedy,
//@ define makro je nutne vlozit do cpp, a declare do triedy, ktora ma podporovat manualne rtti

#define DECLARE_RTTI																\
	public:																			\
		virtual const C_RTTI	&	GetRTTI() const { return m_rtti; }	\
		static const C_RTTI		m_rtti;										\
	private:

#define DEFINE_RTTI(className, baseClass)									\
	const C_RTTI className::m_rtti(#className, &baseClass::m_rtti);


//////////////////////////////////////////////////////////////////////////
//@ define a declare makro pre bazove triedy,
//@ define makro je nutne vlozit do cpp, a declare do triedy, ktora ma podporovat manualne rtti
#define DECLARE_ROOT_RTTI														\
	public:																			\
		virtual const C_RTTI	&	GetRTTI() const { return m_rtti; }		\
		static const C_RTTI		m_rtti;											\
	private:

#define DEFINE_ROOT_RTTI(className)											\
	const C_RTTI className::m_rtti(#className, NULL);

//tohle by snad nemel pouzivat nikdo
namespace internal{
	inline bool IsKindOf(const C_RTTI & typeTo, const C_RTTI * typeFrom)
	{
		while (typeFrom)
		{
			if (typeTo == *typeFrom)
				return true;

			typeFrom = typeFrom->GetBaseClass();
		}

		return false;
	}
}

//@ test zhodnosti typov
template<class TypeTo, class TypeFrom>
bool IsKindOf(TypeFrom * obj)
{
	if (!obj)
		return false;

	return internal::IsKindOf(TypeTo::m_rtti, &obj->GetRTTI() );
}

//@ test zhodnosti typov
template<class TypeFrom>
bool IsKindOf(const C_RTTI & typeTo, TypeFrom * obj)
{
	if (!obj)
		return false;

	return internal::IsKindOf(typeTo, &obj->GetRTTI() );
}


//@ pretypovani 
template<class TypeTo, class TypeFrom>
TypeTo DynamicCast(TypeFrom obj)
{
	SC_CTASSERT(typetraits::is_pointer<TypeFrom>::value);
	if (!obj)
		return 0;

	return internal::IsKindOf( typetraits::remove_pointer<TypeTo>::type::m_rtti, &obj->GetRTTI() ) ? static_cast<TypeTo>(obj) : 0;

}