/*
* filename:			I_BaseObject.h
*
* author:			Kral Jozef
* date:				02/11/2008		19:18
* version:			1.00
* brief:				base class for RTTI objects - define base of rtti
*/

#pragma once

#include "C_RTTI.h"
#include "CommonApi.h"

class COMMON_API I_BaseObject
{
public:
	DECLARE_ROOT_RTTI

protected:
	//////////////////////////////////////////////////////////////////////////
	//c-tor protected, child can override
	I_BaseObject() {};

	//////////////////////////////////////////////////////////////////////////
	//@ copy c-tor
	I_BaseObject(const I_BaseObject & baseObject) {};

	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	virtual ~I_BaseObject() {};

	//////////////////////////////////////////////////////////////////////////
	//@ assignment operator
	void operator=(const I_BaseObject & baseObject) {};
};