/*
* filename:			CommonApi.h
*
* author:			Kral Jozef
* date:				02/11/2008		19:48
* version:			1.00
* brief:				common api for exporting classes functions
*/

#pragma once

#ifdef _USRDLL
	#ifdef COMMON_EXPORTS
		#define COMMON_API __declspec(dllexport)
	#else
		#define COMMON_API __declspec(dllimport)
	#endif
#else
	#define COMMON_API
#endif