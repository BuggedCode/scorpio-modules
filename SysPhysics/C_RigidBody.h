/*
* filename:			C_RigidBody.h
*
* author:			Kral Jozef
* date:				04/05/2010		21:07
* version:			1.00
* brief:
*/

#pragma once

#include <SysPhysics/SysPhysicsApi.h>
#include "PhysicsDefines.h"
#include "C_PhysicsWorld.h"
#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace sysphysics{
      

	//////////////////////////////////////////////////////////////////////////
	//@ C_RigidBody
	class SYSPHYSICS_API C_RigidBody
	{
	public:
		//@ c-tor
		C_RigidBody(T_PhysicsWorld world, const T_BodyDef & bodyDef);
		//@ c-tor
		//C_RigidBody(T_PhysicsWorld world, const T_BodyDef & bodyDef, const T_FixtureDef & fixDef, const scmath::C_Rect & shape);
		//@ d-tor
		~C_RigidBody();


		//@ CreateFixture
		void			CreateFixture(const T_FixtureDef & fixDef);
		//@ CreateFixture - local center is in Zero, in position of rigidBody
		void			CreateFixture(const T_FixtureDef & fixDef, float fWidth, float fHeight);
		//@ CreateFixture - local center is in vctOffset, shape is transformed vctOffset from rigidBody position
		void			CreateFixture(const T_FixtureDef & fixDef, const scmath::C_Vector2 & vctOffset, float fWidth, float fHeight);
		//@ CreateFixture - vctOffset is offset from rigidBody position
		void			CreateFixture(const T_FixtureDef & fixDef, const scmath::C_Vector2 & vctOffset, float fRadius);
		//@ DestroyAllFixtures
		void			DestroyAllFixtures();

		//@ GetPosition
		void			GetPosition(scmath::C_Vector2 & vctOutPos) const;
		//@ SetPosition
		void			SetPosition(const scmath::C_Vector2 & vctInPos);
		
		//@ ApplyForce - apply force to center of RigidBody
		void			ApplyForce(const scmath::C_Vector2 & vctForce);
		//@ ApplyForce
		void			ApplyForce(const scmath::C_Vector2 & vctForce, const scmath::C_Vector2 & vctPos);


		bool			IsActive() const { return m_body->IsActive(); }
		bool			IsAwake() const { return m_body->IsAwake(); }

	private:


		T_Body				*	m_body;	//box2d body
		T_PhysicsWorld			m_world;
	};

	typedef boost::shared_ptr<C_RigidBody>		T_RigidBody;

}}