/*
* filename:			C_PhysicsWorld.h
*
* author:			Kral Jozef
* date:				04/04/2010		21:23
* version:			1.00
* brief:
*/

#pragma once

#include "SysPhysicsApi.h"
#include "PhysicsDefines.h"
#include <SysMath/C_Rect.h>
#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace sysphysics{

	namespace scmath = scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
   class SYSPHYSICS_API C_PhysicsWorld
	{
	public:
		//@ c-tor
		C_PhysicsWorld(const sysmath::C_Vector2 & gravity);
		//@ d-tor
		~C_PhysicsWorld();


		//@ Simulate
		void				Simulate(u32 fDeltaTick, u16 velocityIterations, u16 positionIterations);

		//@ CreateBody
		T_Body		*	CreateBody(const T_BodyDef & bodyDef);
		//@ DestroyBody
		void				DestroyBody(T_Body * body);

	private:
		b2World		*	m_physWorld;
	};


	typedef boost::shared_ptr<C_PhysicsWorld>		T_PhysicsWorld;

}}