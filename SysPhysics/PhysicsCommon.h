/*
* filename:			PhysicsCommon.h
*
* author:			Kral Jozef
* date:				10/5/2011		18:46
* version:			1.00
* brief:				Defines physics structs
*/

#pragma once

#include <SysMath/C_Vector2.h>

namespace scorpio{ namespace sysphysics{

	//////////////////////////////////////////////////////////////////////////
	//@ S_RayCastResult
	struct S_RayCastResult
	{
		float						m_Fraction;	//intersect distance from ray start
		sysmath::C_Vector2	m_Normal;
		//@ Clear
		void						Clear() { m_Fraction = FLT_MAX; m_Normal = sysmath::C_Vector2::C_ZERO; }
	};

	typedef u32	T_CollisionLayer;

#define D_CL_ALL	0xFFFFFFFF

}}
