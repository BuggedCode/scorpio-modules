/*
* filename:			C_RigidBody.cpp
*
* author:			Kral Jozef
* date:				04/05/2010		21:08
* version:			1.00
* brief:
*/

#include "C_RigidBody.h"
#include "C_PhysicsWorld.h"
#include <Common/Assert.h>

namespace scorpio{ namespace sysphysics{

	using namespace scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_RigidBody::C_RigidBody(T_PhysicsWorld world, const T_BodyDef & bodyDef)
		: m_world(world)
	{
		m_body = world->CreateBody(bodyDef);
		SC_ASSERT(m_body);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	/*C_RigidBody::C_RigidBody(T_PhysicsWorld world, const T_BodyDef & bodyDef, const T_FixtureDef & fixDef, const scmath::C_Rect & shape)
		: m_world(world)
	{
		m_body = world->CreateBody(bodyDef);
		SC_ASSERT(m_body);

		b2PolygonShape polyShape;
		polyShape.SetAsBox(shape.GetCenter().m_x, shape.GetCenter().m_y);

		SC_ASSERT(!fixDef.shape);
		T_FixtureDef def = fixDef;
		def.shape = &polyShape;

		T_Fixture * fixture = m_body->CreateFixture(&def);
		SC_ASSERT(fixture);
	}*/



	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_RigidBody::~C_RigidBody()
	{
		m_world->DestroyBody(m_body);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateFixture
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RigidBody::CreateFixture(const T_FixtureDef & fixDef)
	{
		T_Fixture * fixture = m_body->CreateFixture(&fixDef);
		SC_ASSERT(fixture);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateFixture
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RigidBody::CreateFixture(const T_FixtureDef & fixDef, float fWidth, float fHeight)
	{
		b2PolygonShape polyShape;
		polyShape.SetAsBox(fWidth / 2.f, fHeight / 2.f);

		SC_ASSERT(!fixDef.shape);
		T_FixtureDef def = fixDef;
		def.shape = &polyShape;

		T_Fixture * fixture = m_body->CreateFixture(&def);
		SC_ASSERT(fixture);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateFixture
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RigidBody::CreateFixture(const T_FixtureDef & fixDef, const C_Vector2 & vctOffset, float fWidth, float fHeight)
	{
		b2PolygonShape polyShape;
		float fAngle = 0.f;
		T_Vector2 vctCenterPos(vctOffset.m_x, vctOffset.m_y);	//center of the box in local coord, offset from rigidBody position
		polyShape.SetAsBox(fWidth / 2.f, fHeight / 2.f, vctCenterPos, fAngle);

		SC_ASSERT(!fixDef.shape);
		T_FixtureDef def = fixDef;
		def.shape = &polyShape;

		T_Fixture * fixture = m_body->CreateFixture(&def);
		SC_ASSERT(fixture);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateFixture
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RigidBody::CreateFixture(const T_FixtureDef & fixDef, const C_Vector2 & vctOffset, float fRadius)
	{
		b2CircleShape circleShape;
		circleShape.m_p = T_Vector2(vctOffset.m_x, vctOffset.m_y);	//vctOffset is offset from rigidBody position
		circleShape.m_radius = fRadius;

		SC_ASSERT(!fixDef.shape);
		T_FixtureDef def = fixDef;
		def.shape = &circleShape;

		T_Fixture * fixture = m_body->CreateFixture(&def);
		SC_ASSERT(fixture);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DestroyAllFixtures
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RigidBody::DestroyAllFixtures()
	{
		T_Fixture * fixture = m_body->GetFixtureList();
		while (fixture)
		{
			m_body->DestroyFixture(fixture);
			fixture = m_body->GetFixtureList();
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetPosition
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RigidBody::GetPosition(C_Vector2 & vctOutPos) const
	{
		T_Vector2 pos = m_body->GetPosition();
		vctOutPos.m_x = pos.x;
		vctOutPos.m_y = pos.y;
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetPosition
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RigidBody::SetPosition(const scmath::C_Vector2 & vctInPos)
	{
		m_body->SetTransform(T_Vector2(vctInPos.m_x, vctInPos.m_y), 0.f);	//zero angle !!
		m_body->SetLinearVelocity(T_Vector2(0.f, 0.f));
		m_body->SetAwake(true);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ApplyForce - apply force to center of RigidBody
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RigidBody::ApplyForce(const C_Vector2 & vctForce)
	{
		T_Vector2 vForce(vctForce.m_x, vctForce.m_y);
		m_body->ApplyForce(vForce, m_body->GetPosition());
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ApplyForce
	//
	//////////////////////////////////////////////////////////////////////////	
	void C_RigidBody::ApplyForce(const C_Vector2 & vctForce, const C_Vector2 & vctPos)
	{
		T_Vector2 vForce(vctForce.m_x, vctForce.m_y);
		T_Vector2 vPoint(vctPos.m_x, vctPos.m_y);

		m_body->ApplyForce(vForce, vPoint);
	}



}}