/*
* filename:			C_PhysicsWorld.cpp
*
* author:			Kral Jozef
* date:				04/04/2010		21:23
* version:			1.00
* brief:
*/

#include "C_PhysicsWorld.h"
#include <SysMemManager/globalNewDelete.h>
#include <Common/macros.h>

namespace scorpio{ namespace sysphysics{

	using namespace scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_PhysicsWorld::C_PhysicsWorld(const C_Vector2 & gravity)
	{
		// Do we want to let bodies sleep?
		bool doSleep = true;

		m_physWorld = new b2World(b2Vec2(gravity.m_x, gravity.m_y), doSleep);
		SC_ASSERT(m_physWorld);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	//////////////////////////////////////////////////////////////////////////
	C_PhysicsWorld::~C_PhysicsWorld()
	{
		SC_SAFE_DELETE(m_physWorld);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Simulate
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PhysicsWorld::Simulate(u32 fDeltaTick, u16 velocityIterations, u16 positionIterations)
	{
		SC_ASSERT(m_physWorld);
		float fDeltaInSec = fDeltaTick * 0.001f;

		m_physWorld->SetWarmStarting(true);
		m_physWorld->SetContinuousPhysics(true);

		m_physWorld->Step(fDeltaInSec, velocityIterations, positionIterations);

		m_physWorld->ClearForces();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateBody
	//
	//////////////////////////////////////////////////////////////////////////
	T_Body * C_PhysicsWorld::CreateBody(const T_BodyDef & bodyDef)
	{
		return m_physWorld->CreateBody(&bodyDef);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DestroyBody
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PhysicsWorld::DestroyBody(T_Body * body)
	{
		SC_ASSERT(body);
		return m_physWorld->DestroyBody(body);
	}
}}