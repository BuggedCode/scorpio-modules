/*
* filename:			C_CollisionUtils.cpp
*
* author:			Kral Jozef
* date:				10/23/2011		11:15
* version:			1.00
* brief:
*/

#include "C_CollisionUtils.h"
#include <SysMath/C_Matrix2d.h>
#include <SysMath/MathUtils.h>

namespace scorpio{ namespace sysphysics{

	using namespace scorpio::sysphysics;
	using namespace scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CheckOverlap
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_CollisionUtils::CheckOverlap(const C_AABB2 & aabb, const T_B2PolyShape polyShape, const T_Transformation & transform)
	{
		T_Vector2 vctLT = T_Vector2(aabb.GetMin().m_x, aabb.GetMin().m_y);
		bool bRes = polyShape->TestPoint(transform, vctLT);
		if(bRes)
			return true;

		T_Vector2 vctLB = T_Vector2(aabb.GetMin().m_x, aabb.GetMax().m_y);
		bRes = polyShape->TestPoint(transform, vctLB);
		if(bRes)
			return true;

		T_Vector2 vctRB = T_Vector2(aabb.GetMax().m_x, aabb.GetMax().m_y);
		bRes = polyShape->TestPoint(transform, vctRB);
		if(bRes)
			return true;

		T_Vector2 vctRT = T_Vector2(aabb.GetMax().m_x, aabb.GetMin().m_y);
		bRes = polyShape->TestPoint(transform, vctRT);
		if(bRes)
			return true;

		C_Matrix3x3 trMatrix;
		trMatrix.SetTranslation(C_Vector2(transform.position.x, transform.position.y));
		trMatrix.SetRotation(RAD2DEG(transform.R.GetAngle()));

		u32 nCount = polyShape->GetVertexCount();
		for (u32 i = 0; i < nCount; ++i)
		{
			const T_Vector2 & vct = polyShape->GetVertex(i);
			C_Vector2 vct2(vct.x, vct.y);

			//@ rotate + offset
			vct2 *= trMatrix;	//trMatrix;

			if (aabb.IsIntersect(vct2))
				return true;
		}

		//@ check edge intersect

		return false;
	}
}}
