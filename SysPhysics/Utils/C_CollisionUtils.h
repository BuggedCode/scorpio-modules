/*
* filename:			C_CollisionUtils.h
*
* author:			Kral Jozef
* date:				10/23/2011		11:12
* version:			1.00
* brief:
*/

#pragma once

#include <SysPhysics/SysPhysicsApi.h>
#include <SysMath/C_AABB2.h>
#include <SysPhysics/PhysicsDefines.h>

namespace scorpio{ namespace sysphysics{

	//////////////////////////////////////////////////////////////////////////
	//@ C_CollisionUtils
	class SYSPHYSICS_API C_CollisionUtils
	{
	public:
		//@ CheckOverlap
		static bool				CheckOverlap(const sysmath::C_AABB2 & aabb, const T_B2PolyShape polyShape, const T_Transformation & transform);
	};
}}
