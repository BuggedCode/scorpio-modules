/*
* filename:			PhysicsDefines.h
*
* author:			Kral Jozef
* date:				04/06/2010		23:01
* version:			1.00
* brief:
*/

#pragma once

#include <Box2d/Box2D.h>
#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace sysphysics{

	typedef b2Body				T_Body;
	typedef b2Fixture			T_Fixture;
	typedef b2BodyDef			T_BodyDef;
	typedef b2FixtureDef		T_FixtureDef;
	typedef b2Vec2				T_Vector2;
	typedef b2Shape			T_Shape;
	typedef b2AABB				T_AABB2;
	typedef b2Transform		T_Transformation;
	
	typedef b2PolygonShape	C_B2PolyShape;
	typedef boost::shared_ptr<C_B2PolyShape>	T_B2PolyShape;

}}