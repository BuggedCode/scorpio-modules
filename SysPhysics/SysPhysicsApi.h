#pragma once

#ifdef _USRDLL
	#ifdef SYSPHYSICS_EXPORTS
		#define SYSPHYSICS_API __declspec(dllexport)
		#define SYSPHYSICS_EXP_TEMP_INST
	#else
		#define SYSPHYSICS_API __declspec(dllimport)
		#define SYSPHYSICS_EXP_TEMP_INST extern
	#endif
#else
	#define SYSPHYSICS_API
	#define SYSPHYSICS_EXP_TEMP_INST
#endif
