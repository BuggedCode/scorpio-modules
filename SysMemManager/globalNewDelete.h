/*
* filename:			globalNewDelete.h
*
* author:			Kral Jozef
* date:				03/19/2009		21:42
* version:			1.00
* brief:				override of global new and delete
*/

#pragma once

#include <memory>
#ifdef PLATFORM_WIN32
#include <malloc.h>
#elif defined PLATFORM_MACOSX
#include <stdlib.h>
#endif
#include <Common/Assert.h>
#include <Common/types.h>
#include <Common/GlobalDefines.h>
#include "SysMemManagerApi.h"

//my allocation routines exported from dll
SYSMEMMANAGER_API void	*	my_new(size_t size);
SYSMEMMANAGER_API void		my_delete(void * ptr);

SYSMEMMANAGER_API extern char const * g_file;
SYSMEMMANAGER_API extern u32 g_line;


#ifdef SC_USE_NEW_ALLOCATOR
	//////////////////////////////////////////////////////////////////////////
	//
	//@ LogStackInfo
	//
	//////////////////////////////////////////////////////////////////////////
	inline void LogStackInfo(const char * file, u32 line)
	{
	#ifdef SC_LOG_STATISTICS_FULL
		g_file = file;
		g_line = line;
	#endif
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ overrided global new
	//
	//////////////////////////////////////////////////////////////////////////
	inline void * operator new(size_t size)
	{
		return my_new(size);
	}
	//////////////////////////////////////////////////////////////////////////
	//
	//@ overrided global new[]
	//
	//////////////////////////////////////////////////////////////////////////
	inline void * operator new[] (size_t size)
	{
		return my_new(size);
	}
	//////////////////////////////////////////////////////////////////////////
	//
	//@ overrided global new
	//
	//////////////////////////////////////////////////////////////////////////
	inline void * operator new( std::size_t size, std::nothrow_t const& ) throw ()
	{
		return my_new(size);
	}
	//////////////////////////////////////////////////////////////////////////
	//
	//@ overrided global new[]
	//
	//////////////////////////////////////////////////////////////////////////
	inline void * operator new[]( std::size_t size, std::nothrow_t const& ) throw ()
	{
		return my_new(size);
	}

	//standard Microsoft's mem tracker'
	inline void * operator new(size_t reportedSize, const char *sourceFile, int sourceLine)
	{
		LogStackInfo(sourceFile, sourceLine);
		return my_new(reportedSize);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ overrided global delete
	//
	//////////////////////////////////////////////////////////////////////////
	inline void operator delete(void * ptr)
	{
		return my_delete(ptr);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ overrided global delete[]
	//
	//////////////////////////////////////////////////////////////////////////
	inline void operator delete[](void * ptr)
	{
		return my_delete(ptr);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ overrided global delete
	//
	//////////////////////////////////////////////////////////////////////////
	inline void operator delete(void * ptr, std::nothrow_t const& ) throw ()
	{
		return my_delete(ptr);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ overrided global delete[]
	//
	//////////////////////////////////////////////////////////////////////////
	inline void operator delete[](void * ptr, std::nothrow_t const& ) throw ()
	{
		return my_delete(ptr);
	}


	#undef new

	#define new (LogStackInfo(__FILE__, __LINE__), false) ? NULL : new
	//#define new LogStackInfo(__FILE__, __LINE__), new

	#undef delete
#endif