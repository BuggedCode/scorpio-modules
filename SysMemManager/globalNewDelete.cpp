/*
* filename:			globalNewDelete.cpp
*
* author:			Kral Jozef
* date:				12/28/2009		15:33
* version:			1.00
* brief:
*/

#include "globalNewDelete.h"

#if (defined SC_LOG_STATISTICS_EASY) || (defined SC_LOG_STATISTICS_FULL)
	#include "C_GlobalMemoryStatistics.h"
#endif

#define D_UNKNOWN_LINE_ID	0xFFFFFFFF

char const * g_file = NULL;
u32 g_line = D_UNKNOWN_LINE_ID;

//////////////////////////////////////////////////////////////////////////
//
//@ my_new
//
//////////////////////////////////////////////////////////////////////////
void * my_new(size_t size)
{
	void * ptr = malloc(size);
	SC_ASSERT(ptr);

#if (defined SC_LOG_STATISTICS_EASY) && (!defined SC_LOG_STATISTICS_FULL)
	scorpio::sysmemmanager::C_GlobalMemoryStatistics::GetInstance().LogNewEasy();
#elif (defined SC_LOG_STATISTICS_FULL)
	scorpio::sysmemmanager::C_GlobalMemoryStatistics::GetInstance().LogNewFull(ptr, size, g_file, g_line);
	g_file = NULL;	//clear stack info, in case next allocation will be from poor new without info
	g_line = D_UNKNOWN_LINE_ID;
#endif

	return ptr;
}


//////////////////////////////////////////////////////////////////////////
//
//@ my_delete
//
//////////////////////////////////////////////////////////////////////////
void my_delete(void * ptr)
{
	if (ptr == NULL)
		return;

#if (defined SC_LOG_STATISTICS_EASY) && (!defined SC_LOG_STATISTICS_FULL)
	scorpio::sysmemmanager::C_GlobalMemoryStatistics::GetInstance().LogDeleteEasy();
#elif (defined SC_LOG_STATISTICS_FULL)
	scorpio::sysmemmanager::C_GlobalMemoryStatistics::GetInstance().LogDeleteFull(ptr);
#endif

	free(ptr);
	return;
}