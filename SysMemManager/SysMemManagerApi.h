#pragma once

#ifndef SYSMEMMANAGER_H__
#define SYSMEMMANAGER_H__

	#ifdef _USRDLL
		#ifdef SYSMEMMANAGER_EXPORTS
			#define SYSMEMMANAGER_API __declspec(dllexport)
		#else
			#define SYSMEMMANAGER_API __declspec(dllimport)
		#endif
	#else
		#define SYSMEMMANAGER_API
	#endif

#endif
