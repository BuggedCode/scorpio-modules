/*
* filename:			C_GlobalMemoryStatistics.h
*
* author:			Kral Jozef
* date:				03/19/2009		21:55
* version:			1.00
* brief:				class for global statistics of allocated memory
*/

#pragma once

#include <Common/types.h>
#include <Common/Assert.h>
#include <Common/macros.h>
#include <Common/Pragmas.h>
#include "SysMemManagerApi.h"
#include <map>
#include <Common/GlobalDefines.h>
#include <string.h>

namespace scorpio{ namespace sysmemmanager{

#define D_MEM_BUFFER_SIZE	128

	//////////////////////////////////////////////////////////////////////////
	//@ C_GlobalMemoryStatisctics
	class SYSMEMMANAGER_API C_GlobalMemoryStatistics
	{
	public:
		struct S_MemLeapInfo
		{
			S_MemLeapInfo(size_t size, u32 line, const char * file)
				: m_size(size), m_line(line)
			{
				if (file)
				{
					STR_CPY(m_file, file, D_MEM_BUFFER_SIZE);
				}
				else
				{
					m_file[0] = 0x00;
					m_file[1] = 0x0F;
				}
			}

			size_t	m_size;
			u32		m_line;
			char		m_file[D_MEM_BUFFER_SIZE];
		};

		//////////////////////////////////////////////////////////////////////////
		//@ interface for dump leaks
		class I_DumpLeakOutputInfo
		{
		public:
			virtual void operator()(const S_MemLeapInfo & info) const = 0;
		};

		typedef std::map<size_t, u32>		T_SizeCounterMap;

		class I_MemSizeStatisticsVisitor
		{
		public:
			virtual void operator()(size_t size, u32 nCount) = 0;
		};

		//@ GetInstance
		static C_GlobalMemoryStatistics	&	GetInstance()
		{
			return m_instance;
		}

		//@ GetUsedMemory
		u64				GetUsedMemory() const { return m_usedMem; }
		//@ GetAllocationsCount
		u32				GetAllocationsCount() const { return m_newCounter; }
		//@ GetDeallocationsCount
		u32				GetDeallocationsCount() const { return m_deleteCounter; }
		//@ GetUnknownDeallocationsCount
		u32				GetUnknownDeallocationsCount() const { return m_unknownAllocCounter; }
		//@ ClearStatistics - NEBEZPECNA FUNKCIA
		void				ClearStatistics()
		{ 
			m_usedMem = 0; 
			m_newCounter = 0;
			m_deleteCounter = 0;
			m_unknownAllocCounter = 0;
			m_bCreated = false;
			m_memInfo.clear();
			m_bCreated = true;
		}
		
		//////////////////////////////////////////////////////////////////////////
		//
		//@ LogNewEasy
		//
		//////////////////////////////////////////////////////////////////////////
		inline void		LogNewEasy()
		{
			++m_newCounter;
		}

		
		//////////////////////////////////////////////////////////////////////////
		//
		//@ LogDeleteEasy
		//
		//////////////////////////////////////////////////////////////////////////
		inline void		LogDeleteEasy()
		{
			++m_deleteCounter;
		}
		

		//////////////////////////////////////////////////////////////////////////
		//
		//@ LogNewFull
		//
		//////////////////////////////////////////////////////////////////////////
		void				LogNewFull(void * ptr, size_t size, const char * file, u32 line)
		{
			//@ TODO toto je hack jak svina, v c-tore C_GlobalMemStat sa stl::mapa alokuje uz cez moj new a xce sa do nej 
			//@ zapisovat este ked nie je vytvorena, neviem to ojebat aby sa pouzival defaultny new
			if (!m_bCreated)
				return;

			
			++m_newCounter;
			m_bCreated = false;	//hack aby sa necyklilo logovanie pri dyn alokacii prvkov v mape
			m_memInfo.insert(T_MemInfoContainer::value_type(ptr, S_MemLeapInfo(size, line, file)));

#ifdef SC_COLLECT_SIZE_STATISTICS
			T_SizeCounterMap::iterator iter = m_SizeCounter.find(size);
			if (iter != m_SizeCounter.end())
				++iter->second;
			else
				m_SizeCounter.insert(T_SizeCounterMap::value_type(size, 1));
#endif
			m_bCreated = true;
		}


		//////////////////////////////////////////////////////////////////////////
		//
		//@ LogDeleteFull
		//
		//////////////////////////////////////////////////////////////////////////
		void				LogDeleteFull(void * ptr)
		{
			if (!m_bCreated)
				return;


			++m_deleteCounter;
			T_MemInfoContainer::iterator iter = m_memInfo.find(ptr);
			if (iter == m_memInfo.end())
			{
				++m_unknownAllocCounter;
				return;
			}

			m_bCreated = false;
			m_memInfo.erase(iter);
			m_bCreated = true;
		}


		//////////////////////////////////////////////////////////////////////////
		//
		//@ DumpMemLeaksInfo
		void				DumpMemLeaksInfo(const I_DumpLeakOutputInfo & outputSolver) const
		{
			for (T_MemInfoContainer::const_iterator iter = m_memInfo.begin(); iter != m_memInfo.end(); ++iter)
			{
				outputSolver(iter->second);
			}
		}

#ifdef SC_COLLECT_SIZE_STATISTICS
		//////////////////////////////////////////////////////////////////////////
		//
		//@ DumpMemSizeStatistics
		void				DumpMemSizeStatistics(I_MemSizeStatisticsVisitor & visitor) const
		{
			for (T_SizeCounterMap::const_iterator iter = m_SizeCounter.begin(); iter != m_SizeCounter.end(); ++iter)
			{
				visitor(iter->first, iter->second);
			}
		}
#endif
		
	private:
		//@ c-tor
		C_GlobalMemoryStatistics();

		//@ copy c-tor
		C_GlobalMemoryStatistics(const C_GlobalMemoryStatistics & copy);	//not implemented
		//@ asisgnment operator
		const C_GlobalMemoryStatistics & operator=(const C_GlobalMemoryStatistics & copy);	//not implemented
		//@ d-tor
		~C_GlobalMemoryStatistics();


		static C_GlobalMemoryStatistics m_instance;

		u32		m_newCounter;			//count of called global new
		u32		m_deleteCounter;		//count of called global delete

		u32		m_unknownAllocCounter;

		u64		m_usedMem;				//actually used memory (allocated by our manager)

		typedef std::map<void *, S_MemLeapInfo>	T_MemInfoContainer;
		T_MemInfoContainer	m_memInfo;

#ifdef SC_COLLECT_SIZE_STATISTICS
		T_SizeCounterMap		m_SizeCounter;	//count of allocations per size
#endif

		//static premenna pre logovanie statistik, loguju sa len ak je uz C_GlobalMem... inicializovany
		// v jeho c-tore sa pouziva nas overridnuty new, ale este objekt nieje zkonstruovany tak nemoze byt pouzity na logovanie!
		static bool	m_bCreated;
	};

}}