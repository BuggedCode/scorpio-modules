/*
* filename:			SysMemoryManager.h
*
* author:			Kral Jozef
* date:				03/19/2009		22:46
* version:			1.00
* brief:				Global module initialisation part
*/

#include "SysMemManagerApi.h"

namespace scorpio{ namespace sysmemmanager{

	//@ Initialize
	//bool SYSMEMMANAGER_API	Initialize();
	//@ Deinitialize
	//void SYSMEMMANAGER_API	Deinitialize();

}}