/*
* filename:			C_GlobalMemoryStatistics.cpp
*
* author:			Kral Jozef
* date:				03/19/2009		22:10
* version:			1.00
* brief:
*/

#include "C_GlobalMemoryStatistics.h"

bool scorpio::sysmemmanager::C_GlobalMemoryStatistics::m_bCreated = false;

namespace scorpio{ namespace sysmemmanager{

	C_GlobalMemoryStatistics C_GlobalMemoryStatistics::m_instance;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_GlobalMemoryStatistics::C_GlobalMemoryStatistics()
		: m_newCounter(0), m_deleteCounter(0), m_usedMem(0), m_unknownAllocCounter(0)
	{
		m_memInfo.clear();
		m_bCreated = true;	//povolim logovanie statistik, objekt uz "existuje"
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_GlobalMemoryStatistics::~C_GlobalMemoryStatistics()
	{
		m_bCreated = false;
		m_memInfo.clear();
	}

				

}}