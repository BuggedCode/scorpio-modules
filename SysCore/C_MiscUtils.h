/*
* filename:			C_MiscUtils.h
*
* author:			Kral Jozef
* date:				02/16/2011		23:07
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysUtils/C_String.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	// C_MiscUtils
	class SYSCORE_API C_MiscUtils
	{
	public:
		//@ Initialize = must call before use
		static void				Initialize();

		//@ GetTicks 
		//@ retVal - milliseconds from start of initialization SDLWrapper/KMiscUtils
		static u32				GetMilliseconds();
	};

}}