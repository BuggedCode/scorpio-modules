/*
* filename:			C_AppWindow.cpp
*
* author:			Kral Jozef
* date:				02/17/2011		17:35
* version:			1.00
* brief:
*/

#include "C_AppWindow.h"
#include <Common/macros.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_AppWindow::~C_AppWindow()
	{
		SC_SAFE_DELETE(m_renderWnd);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateAppWindow
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AppWindow::CreateAppWindow(u16 nWidth, u16 nHeight, u16 depth, bool bWindowed, const char * title)
	{
		m_Width = nWidth;
		m_Height = nHeight;
		m_Depth = depth;

#ifdef USE_PTK
		m_renderWnd = new KWindowW(KWindowW::E_DIRECTX9);
		if (!m_renderWnd)
			return false;

		m_renderWnd->setPTKCallBack(&C_ScreenManager::wndProc);
		//@ disable VSync
		m_renderWnd->setMaxFrameRate(-1);
#elif defined RENDER_WIN_GLES_20
		m_renderWnd = new C_WinGLESWindow();
#else
		m_renderWnd = new C_SDLWindow();
#endif
		if (!m_renderWnd)
			return false;

		return m_renderWnd->createGameWindow(nWidth, nHeight, depth, bWindowed, title);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ProcessEvents
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AppWindow::ProcessEvents()
	{
#ifdef USE_PTK
		SC_CTASSERT(false);
		//SC_ASSERT(false);
		m_renderWnd->processEvents();
#else
		m_renderWnd->processEvents(this);
#endif
	}

}}