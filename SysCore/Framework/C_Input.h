/*
* filename:			C_Input.h
*
* author:			Kral Jozef
* date:				02/17/2011		20:37
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysMath/C_Point.h>
#include "FrameworkTypes.h"

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Input
	class SYSCORE_API C_Input
	{
	public:
		//@ GrabInputWM
		static void			GrabInputWM(bool bGrabMouse);
		//@ GetMousePos
		static void			GetMousePos(sysmath::C_Point & outMousePos);
		//@ IsPressed
		static bool			IsPressed(E_KeyboardLayout key);
		static bool			IsPressed(E_KeyboardModifier keyMod);
		//@ GetRelMousePos
		static void			GetRelMousePos(sysmath::C_Point & outMousePos);
		//@ WrapMouse - SetMousePos
		static void			WarpMouse(sysmath::C_Point & inMousePos);
		//@ SetVisible
		static void			ShowMouseCursor(bool bVisible);
		//@ GetMouseButtonState
		static void			GetMouseButtonState(u8 & mouseButtonMask);
	};

}}