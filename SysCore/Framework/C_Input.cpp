/*
* filename:			C_Input.cpp
*
* author:			Kral Jozef
* date:				02/17/2011		20:37
* version:			1.00
* brief:
*/

#include "C_Input.h"
#include <Common/PlatformDef.h>

#ifdef PLATFORM_WIN32
	#if defined RENDER_WIN_GLES_20 || RENDER_WIN_GLES_11
		#include <SysCore/PlatformImpl/C_Input_WinImpl.h>
		#define C_InputPlatformImpl	C_Input_WinImpl
	#else
		#include <SDL/include/SDL_mouse.h>
		#include <SysCore/PlatformImpl/C_Input_SDLImpl.h>
		#define C_InputPlatformImpl	C_Input_SDLImpl
	#endif
#elif defined PLATFORM_MACOSX
	#include <SDL/SDL_mouse.h>
    #include <SysCore/PlatformImpl/C_Input_SDLImpl.h>
    #define C_InputPlatformImpl	C_Input_SDLImpl
#endif


#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace sysmath;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetMousePos
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Input::GetMousePos(C_Point & outMousePos)
	{
		//@ in ifdef PTK use KInputW
		SDL_GetMouseState(&outMousePos.x, &outMousePos.y);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsPressed
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Input::IsPressed(E_KeyboardModifier keyMod)
	{
		return C_InputPlatformImpl::IsPressed(keyMod);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsPressed
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Input::IsPressed(E_KeyboardLayout key)
	{
		Uint8 * keys = SDL_GetKeyState(NULL);
		if (keys[key])
			return true;

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ WarpMouse
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Input::WarpMouse(C_Point & inMousePos)
	{
		SDL_WarpMouse(inMousePos.x, inMousePos.y);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ShowMouseCursor
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Input::ShowMouseCursor(bool bVisible)
	{
		SDL_ShowCursor(bVisible);
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetRelMousePos
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Input::GetRelMousePos(sysmath::C_Point & outMousePos)
	{
		SDL_GetRelativeMouseState(&outMousePos.x, &outMousePos.y);
	}


	//////////////////////////////////////////////////////////////////////////
	void C_Input::GrabInputWM(bool bGrabMouse)
	{
		if (bGrabMouse)
		{
			SDL_WM_GrabInput(SDL_GRAB_ON);
		}
		else
		{
			SDL_WM_GrabInput(SDL_GRAB_OFF);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetMouseButtonState
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Input::GetMouseButtonState(u8 & mouseButtonMask)
	{
		mouseButtonMask = 0;
		Uint8 buttonMask = SDL_GetMouseState(NULL, NULL);
		if (SDL_BUTTON(SDL_BUTTON_LEFT) & buttonMask)
			mouseButtonMask |= LEFT_MOUSE_BUTTON_MASK;
		if (SDL_BUTTON(SDL_BUTTON_RIGHT) & buttonMask)
			mouseButtonMask |= RIGHT_MOUSE_BUTTON_MASK;
		if (SDL_BUTTON(SDL_BUTTON_MIDDLE) & buttonMask)
			mouseButtonMask |= MIDDLE_MOUSE_BUTTON_MASK;
	}
}}