/*
* filename:			FrameworkTypes.h
*
* author:			Kral Jozef
* date:				03/21/2009		19:40
* version:			1.00
* brief:				Types bridge for framework, for now only support for PTK engine
*/

#pragma once

#include <Common/PlatformDef.h>
#include <Common/types.h>

#ifdef PLATFORM_WIN32
	#include <SDL/include/SDL_keysym.h>
	#include <SDL/include/sdl_events.h>
#elif defined PLATFORM_MACOSX
	#include <SDL/SDL_keysym.h>
	#include <SDL/sdl_events.h>
#endif


typedef SDL_Event T_WindowEvent;
typedef SDLKey	E_KeyboardLayout;

typedef u16		E_KeyboardModifier;
enum E_KeyboardModifierFlags
{
	E_KM_NONE	= KMOD_NONE,
   
	E_KM_LSHIFT = KMOD_LSHIFT,
	E_KM_RSHIFT = KMOD_RSHIFT,
   
#ifdef PLATFORM_WIN32
	E_KM_LCTRL	= KMOD_LCTRL,
	E_KM_RCTRL	= KMOD_RCTRL,
#elif defined PLATFORM_MACOSX
	E_KM_LCTRL	= KMOD_LMETA,
	E_KM_RCTRL	= KMOD_RMETA,   
#endif
	E_KM_LALT   = KMOD_LALT,
	E_KM_RALT   = KMOD_RALT,

	E_KM_LCMD   = KMOD_LMETA,  //mac left comand
   E_KM_RCMD   = KMOD_RMETA,  //mac right comand

	E_KM_SHIFT	= (E_KM_LSHIFT | E_KM_RSHIFT),
	E_KM_CTRL	= (E_KM_LCTRL | E_KM_RCTRL),	
   E_KM_ALT		= (E_KM_LALT | E_KM_RALT),
   E_KM_CMD		= (E_KM_LCMD | E_KM_RCMD),
};


//@ use in events
#define LEFT_MOUSE_BUTTON			0x00000001
#define MIDDLE_MOUSE_BUTTON		0x00000002
#define RIGHT_MOUSE_BUTTON			0x00000003
#define MOUSE_WHEEL_UP				0x00000004
#define MOUSE_WHEEL_DOWN			0x00000005

//@ use for states
#define LEFT_MOUSE_BUTTON_MASK	0x00000001
#define MIDDLE_MOUSE_BUTTON_MASK	0x00000002
#define RIGHT_MOUSE_BUTTON_MASK	0x00000004
#define MOUSE_WHEEL_UP_MASK		0x00000008
#define MOUSE_WHEEL_DOWN_MASK		0x00000010

typedef SDL_MouseMotionEvent		T_MouseMotionEvent;
typedef SDL_MouseButtonEvent		T_MouseButtonEvent;

//@ events
#define MOUSE_MOTION_EVENT			SDL_MOUSEMOTION
#define MOUSE_BUTTON_DOWN_EVENT	SDL_MOUSEBUTTONDOWN
#define MOUSE_BUTTON_UP_EVENT		SDL_MOUSEBUTTONUP
#define KEY_DOWN_EVENT				SDL_KEYDOWN
#define KEY_UP_EVENT					SDL_KEYUP

