/*
* filename:			C_AppWindow.h
*
* author:			Kral Jozef
* date:				02/17/2011		17:34
* version:			1.00
* brief:				Application main window
*/

#include <SysCore/SysCoreApi.h>
#ifdef USE_PTK
	#include <SysCore/PTKWrapper/KWindowW.h>
#elif defined RENDER_WIN_GLES_20
	#include <SysCore/PlatformImpl/C_WinGLESWindow.h>
#else
	#include <SysCore/SDLWrapper/C_SDLWindow.h>
#endif

#include "FrameworkTypes.h"

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	// C_AppWindow
	class SYSCORE_API C_AppWindow
	{
	public:
		//@ d-tor
		virtual ~C_AppWindow();

		//@ CreateWindow
		bool					CreateAppWindow(u16 nWidth, u16 nHeight, u16 depth, bool bWindowed, const char * title);
		//@ ProcessEvents
		void					ProcessEvents();
		//@ GetFullScreenState
		bool					GetFullScreenState() const { return m_renderWnd->getFullScreenState(); }
		//@ ToggleFullScreen
		void					ToggleFullScreen(bool bFullScreen) { m_renderWnd->toggleFullScreen(bFullScreen); }
		//@ HandleEvents
		virtual bool		HandleEvent(T_WindowEvent & event) = 0;
		//@ Delay
		//@ time - delay time in ms
		void					Delay(u32 time) { m_renderWnd->Delay(time); }

	protected:
#ifdef USE_PTK
		ptkwrapper::KWindowW		*	m_renderWnd;	// engine render window
#elif defined RENDER_WIN_GLES_20
		C_WinGLESWindow			*	m_renderWnd;
#else
		C_SDLWindow					*	m_renderWnd;	// engine render window
#endif


		u16		m_Width;
		u16		m_Height;
		u16		m_Depth;
	};

}}