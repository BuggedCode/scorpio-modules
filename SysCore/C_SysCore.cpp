/*
* filename:			C_SysCore.cpp
*
* author:			Kral Jozef
* date:				01/23/2010		16:53
* version:			1.00
* brief:
*/

#include "C_SysCore.h"
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_TextureManager.h>
#include <SysCore/Core/C_AnimTextureManager.h>
#include <SysCore/Core/C_RenderManager.h>
#include <SysCore/Core/C_FontManager.h>
#include <SysCore/Core/C_BitmapFontManager.h>
#include <SysCore/Core/C_AnimationManager.h>
#include <SysCore/Geometry/C_MeshManager.h>
#include <SysCore/C_MiscUtils.h>

namespace scorpio{ namespace syscore{

	using namespace sysutils;
	using namespace sysrender;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_SysCore::Initialize()
	{
		//@ initialize misceleanous tools
		C_MiscUtils::Initialize();

		//@ init animation manager
		bool bRes = T_AnimationManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create AnimationManager singleton instance!");
			return false;
		}

		//@ init texture manager
		bRes = T_TextureManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create TextureManager singleton instance!");
			return false;
		}

		//@ init font manager
		bRes = T_FontManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create FontManager singleton instance!");
			return false;
		}

		//@ init anim texture manager
		bRes = T_AnimTextureManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create AnimTextureManager singleton instance!");
			return false;
		}

		bRes = T_RenderManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create RenderManager singleton instance!");
			return false;
		}

		bRes = T_BitmapFontManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create BitmapFontManager singleton instance!");
			return false;
		}

		bRes = T_MeshManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create MeshManager singleton instance!");
			return false;
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Deinitialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_SysCore::Deinitialize()
	{
		T_MeshManager::DestroyInstance();
		T_BitmapFontManager::DestroyInstance();
		T_AnimTextureManager::DestroyInstance();
		T_FontManager::DestroyInstance();
		T_TextureManager::DestroyInstance();
		T_RenderManager::DestroyInstance();
		T_AnimationManager::DestroyInstance();
		return true;
	}
}}