/*
* filename:			I_RenderObject.h
*
* author:			Kral Jozef
* date:				01/10/2010		20:47
* version:			1.00
* brief:				Interface of renderable object
*/

#pragma once

#include <boost/weak_ptr.hpp>
#include <vector>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ I_RenderObject
	class I_RenderObject
	{
	public:
		//@ c-tor
		I_RenderObject()
			: m_bVisible(true) {};
		//@ d-tor
		~I_RenderObject() {};

		//@ Render
		virtual void	Render() const = 0;

#ifndef MASTER
		//@ DebugDraw
		virtual void	DebugDraw() const {};
#endif


		//@ Set/Get
		void				SetVisible(bool bVisible) { m_bVisible = bVisible; }
		bool				IsVisible() const { return m_bVisible; }

		virtual float	GetZCoord() const = 0;


	protected:
		bool		m_bVisible;		//object is visible for render
		//pos / color / KGraphics?
	};
}}

typedef boost::shared_ptr<scorpio::syscore::I_RenderObject>	T_RenderObject;
typedef boost::weak_ptr<scorpio::syscore::I_RenderObject>	T_WeakRenderObject;
typedef std::vector<T_RenderObject>									T_RenderObjectList;

inline bool operator==(const T_RenderObject & ro, const T_WeakRenderObject roWeak)
{
	return (ro.get() == roWeak.lock().get());
}