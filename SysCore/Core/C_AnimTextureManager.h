/*
* filename:			C_AnimTextureManager.h
*
* author:			Kral Jozef
* date:				06/15/2009		19:54
* version:			1.00
* brief:				Manager of animated textures
*/

#pragma once

#include <Common/C_Singleton.h>
#include <SysCore/SysCoreApi.h>
#include "C_AnimTexture.h"
#include <map>
#include <SysUtils/C_String.h>
#include <SysUtils/C_HashName.h>
#include <SysUtils/FileSystem/C_Stream.h>


namespace scorpio{ namespace syscore{

	typedef std::map<sysutils::C_HashName, T_AnimImage>	T_AnimTextureMap;

	//////////////////////////////////////////////////////////////////////////
	//@ C_AnimTextureManager
	class SYSCORE_API C_AnimTextureManager
	{
		friend class C_Singleton<C_AnimTextureManager>;
	public:
		//@ InitFromDefXml
		//@ initialize - fill manager with anim textures from xml file
		bool				InitFromDefXml(const sysutils::T_Stream inputStream);

		//@ GetAnimTexture
		//@ animTexUID - unique id of animatedTexture
		//@ check if image is in container if not try to load from disk and add to container
		T_AnimImage		GetAnimTexture(const sysutils::C_HashName & animTexUID);


	private:
		//@ c-tor
		C_AnimTextureManager();
		//@ copy c-tor
		C_AnimTextureManager(const C_AnimTextureManager & copy);	//not implemented
		//@ assignment operator
		C_AnimTextureManager & operator=(const C_AnimTextureManager & copy);	//not implemented
		//@ d-tor
		~C_AnimTextureManager();

		T_AnimTextureMap						m_container;	//kontainer pre vsetky animovane textury
	};

	typedef C_Singleton<C_AnimTextureManager>	T_AnimTextureManager;
}}

//template <class T> T C_Singleton<T>::m_instance;
SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::syscore::C_AnimTextureManager>;