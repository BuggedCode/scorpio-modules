/*
* filename:			C_FontManager.h
*
* author:			Kral Jozef
* date:				09/17/2010		23:12
* version:			1.00
* brief:				Singleton manager for font
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <Common/C_Singleton.h>
#include <SysCore/Render/C_TrueText.h>
#include <map>
#include <vector>
#include <SysUtils/C_String.h>
#include <SysUtils/C_HashName.h>

namespace scorpio{ namespace syscore{

	//typedef sysutils::C_HashName	T_TextureKey;	//hashed texture filePath
	//typedef std::map<T_TextureKey, sysrender::T_GraphicRes>	T_TextureMap;

	//typedef std::map<sysutils::C_HashName, T_TextureKey>	T_UniqueIDToKeyTable;

	//////////////////////////////////////////////////////////////////////////
	//@ C_FontManager
	class SYSCORE_API C_FontManager
	{
		friend class C_Singleton<C_FontManager>;
	public:

		//@ AddFont
		//@ fontFileName - relative path from data directory in native Win32 format
		//@ adds fonts to manager/ but not load from disk(just request - need call after PreloadFonts)
		//@ check if image is in container if not try to load from disk and add to container
		bool				AddFont(sysutils::C_HashName & fontUID, sysutils::C_String fontFileName, u32 fontSize = C_DEFAULT_FONT_SIZE);

		//@ GetTexture
		//@ hashID - uniqueID of font - manager will find correspondend path
		T_TrueText		GetFont(sysutils::C_HashName & fontUID);

		//@ PreloadTexture TODO predelat na GetTexture uniqueId
		bool				PreloadFonts();

		//@ ReleaseInternalResources - release from VRAM
		bool				ReleaseInternalResources();
		//@ RestoreInternalResources
		bool				RestoreInternalResources();
		//@ Clear
		void				Clear();

	private:
		//@ c-tor
		C_FontManager() {};
		//@ d-tor
		~C_FontManager() {};

		static const u32 C_DEFAULT_FONT_SIZE = 12;

		struct S_FontInfo
		{
			S_FontInfo(sysutils::C_HashName & hashName, sysutils::C_String fileName, u32 fontSize)
				: m_HashName(hashName), m_FileName(fileName), m_FontSize(fontSize) {};

			sysutils::C_HashName	m_HashName;
			sysutils::C_String	m_FileName;
			u32						m_FontSize;
		};

		typedef std::vector<S_FontInfo>	T_RequestQueue;
		T_RequestQueue						m_requestQueue;


		typedef std::map<sysutils::C_HashName, T_TrueText>	T_FontContainer;
		T_FontContainer					m_container;	//kontainer pre vsetky textury


		//T_UniqueIDToKeyTable				m_conversionTable;	//table of conversion uniqueID to key into textureMap
	};

	typedef C_Singleton<C_FontManager>	T_FontManager;
}}

SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::syscore::C_FontManager>;
