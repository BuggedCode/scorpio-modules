/*
* filename:			C_Animation.cpp
*
* author:			Kral Jozef
* date:				11/07/2010		17:48
* version:			1.00
* brief:
*/

#include "C_Animation.h"
#include <fstream>
#include <SysUtils/C_TraceClient.h>
#include <Common/macros.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;

	static const C_String C_ANIM_TRACK_TYPE_TAG = "AnimTrackType";
	static const u8 C_ANIM_TRACK_TYPE_TAG_LEN = 13;	//strlen("AnimTrackType");

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Animation::C_Animation(C_String animFileName)
	{
		std::ifstream ifStream;
		ifStream.open(animFileName);
		if (ifStream.is_open())
		{
			const u32 C_BUFFER_SIZE = 1024;
			char buffer[C_BUFFER_SIZE];
			ZERO_ARRAY(buffer);

			bool bFindSamples = false;
			std::vector<C_String> floatStrList;
			while (!ifStream.eof())
			{
				ifStream.getline(buffer, C_BUFFER_SIZE);
				C_String strTmp(buffer);

				//@ anim track type in first line
				C_String strTag = strTmp.Left(C_ANIM_TRACK_TYPE_TAG_LEN - 1);	//<= strlen(C_ANIM_TRACK_TYPE_TAG);
				if (strTag == C_ANIM_TRACK_TYPE_TAG)
				{
					m_trackType = E_ATT_POS;	//home proprietary exporter will be exported just animated tracks not all
					//Get type
					continue;
				}

				//@ second line
				if (!bFindSamples)
				{
					bFindSamples = true;
					//@ TODO PARSE
					continue;
				}


				//@ parse samples
				strTmp.Split(' ', floatStrList);
			}

			if (floatStrList.size() % 10 != 0)
			{
				TRACE_FE("Animation track: %s does not contain correct information!", animFileName.c_str());
			}
			else
			{
				for (std::vector<C_String>::iterator iter = floatStrList.begin(); iter != floatStrList.end(); /*++iter*/)
				{
					//////////////////////////////////////////////////////////////////////////
					float posX = iter->ToFloat(); ++iter;
					float posY = iter->ToFloat(); ++iter;
					//float posZ = iter->ToFloat(); ++iter;

					//float rotX = iter->ToFloat(); ++iter;
					//float rotY = iter->ToFloat(); ++iter;
					float rotZ = iter->ToFloat(); ++iter;

					float fScale = iter->ToFloat(); ++iter;
					++iter;++iter;++iter;

					m_trackData.push_back(S_SampleData(posX, -posY, rotZ, fScale));
				}
			}
		}

		ifStream.close();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddController
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Animation::AddController(E_ControllerType type, T_AnimController controller)
	{
		//SC_ASSERT(m_flags & type != true);	//controller daneho typu uz je v animacke
		//m_flags != type;

		m_controllers.insert(std::map<int, T_AnimController>::value_type(1, controller));


	}

}}