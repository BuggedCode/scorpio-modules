/*
* filename:			C_AnimControllerFactory.cpp
*
* author:			Kral Jozef
* date:				11/17/2010		22:56
* version:			1.00
* brief:
*/

#include "C_AnimControllerFactory.h"
#include <SysUtils/C_TraceClient.h>

#include "C_BezierAnimController.h"
#include "C_LinearFloatController.h"
#include "C_LinearPosController.h"

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateController
	//
	//////////////////////////////////////////////////////////////////////////
	T_AnimController C_AnimControllerFactory::CreateController(C_String controllerType)
	{
		//PHUJ THIS IS NOT FACTORY:)
		if (controllerType == "Bezier")
		{
			return T_AnimController();
		}
		else if (controllerType == "TCB")
		{
			SC_ASSERT(false);
			return T_AnimController();
		}
		else if (controllerType == C_LinearFloatController::C_CONTROLLER_TYPENAME)
		{
			return T_AnimController(new C_LinearFloatController());
		}
		else if (controllerType == C_LinearPosController::C_CONTROLLER_TYPENAME)
		{
			return T_AnimController(new C_LinearPosController());
		}

		TRACE_FE("Unsupported Controller Interpolator type: %s", controllerType.c_str());
		return T_AnimController();
	}

}}