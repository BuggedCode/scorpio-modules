/*
* filename:			C_LinearFloatController.h
*
* author:			Kral Jozef
* date:				11/14/2010		23:14
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include "I_AnimController.h"
#include <vector>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_LinearFloatController
	class SYSCORE_API C_LinearFloatController : public I_AnimController
	{
		struct S_Key
		{
			S_Key(u32 frameID, float fVal)
				: m_frameID(frameID), m_fVal(fVal) {};

			u32		m_frameID;
			float		m_fVal;
		};
	public:
		//@ c-tor
		C_LinearFloatController() {};
		//@ d-tor
		~C_LinearFloatController() {};
		
		//@ AddKey
		void								AddKey(u32 frameId, float fVal)
		{
			m_keysContainer.push_back(S_Key(frameId, fVal));
		}

		//@ Save
		virtual void					Save(sysutils::C_XmlNode & parentNode) const;
		//@ InitializeFromNode
		virtual bool					InitializeFromNode(const sysutils::C_XmlNode & ctrlInterpNode);
		//@ GetValue
		virtual void					GetValue(u32 frameId, float & outValue) const;
		//@ Clone
		virtual T_AnimController	Clone() const;
		//@ CreateReversed
		virtual T_AnimController	CreateReversed() const;

	private:
		void								SampleSameValues(u32 frameID);

	public:
		static const sysutils::C_String	C_CONTROLLER_TYPENAME;

	private:
		typedef std::vector<S_Key>	T_KeyContainer;
		T_KeyContainer					m_keysContainer;
	};

}}