/*
* filename:			C_SampleData.h
*
* author:			Kral Jozef
* date:				11/21/2010		23:26
* version:			1.00
* brief:
*/

#pragma once

#include <SysMath/C_Vector2.h>
#include <syscore/Core/Animation/C_NoteTrackKey.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_SampleData
	class C_SampleData
	{
	public:
		enum E_ParameterType
		{
			E_PT_POSITION = (1 << 1),
			E_PT_ROTATION = (1 << 2),
			E_PT_SCALE = (1 << 3),
			E_PT_ALPHA = (1 << 5),
			E_PT_TRACK_NOTE = (1 << 6),
			E_PT_VELOCITY = (1 << 7),
		};

		//@ c-tor
		C_SampleData()
			: m_fAngle(0.f), m_fScale(1.f), m_fAlpha(1.f), m_flags(0), m_fVelocity(0.f) {}
		//@ c-tor
		/*C_SampleData(const sysmath::C_Vector2 & m_vctPos, float fAngle, float fScale, float fAlpha)
			: m_vctPos(m_vctPos), m_fAngle(fAngle), m_fScale(fScale), m_fAlpha(fAlpha) {};
		//@ c-tor
		C_SampleData(float fPosX, float fPosY, float fAngle, float fScale, float fAlpha)
			: m_vctPos(fPosX, fPosY), m_fAngle(fAngle), m_fScale(fScale), m_fAlpha(fAlpha) {};*/

		//void						SetPosX(float posX) { m_vctPos.m_x = posX, m_flags += E_DT_POS_X; }
		//void						SetPosY(float posY) { m_vctPos.m_y = posY, m_flags += E_DT_POS_Y; }
		//void						SetAngle(float fAngle) { m_fAngle = fAngle; m_flags += E_DT_ANGLE; }
		//void						SetScale(float fScale) { m_fScale = fScale; m_flags += E_DT_SCALE; }
		//void						SetAlpha(float fAlpha) { m_fAlpha = fAlpha; m_flags += E_DT_ALPHA; }

		bool							HasSample(E_ParameterType paramType) { return ((m_flags & paramType) == paramType); }

	//private:
		sysmath::C_Vector2	m_vctPos;
		float						m_fAngle;
		float						m_fScale;
		float						m_fAlpha;
		float						m_fVelocity;

		u32						m_flags;	//'E_AnimControllerType' sum of all anim params - to identify which param is animated

		typedef std::vector<C_NoteTrackKey>		T_NoteTrackKeys;
		T_NoteTrackKeys		m_NoteTrackKeys;
	};


}}