/*
* filename:			I_AnimController.h
*
* author:			Kral Jozef
* date:				11/14/2010		21:48
* version:			1.00
* brief:
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <SysUtils/C_String.h>

namespace scorpio{
	namespace sysutils{
		class C_XmlNode;
	}
}

namespace scorpio{ namespace syscore{

	class I_AnimController;

	typedef boost::shared_ptr<I_AnimController>	T_AnimController;

	//////////////////////////////////////////////////////////////////////////
	//@ I_AnimController
	class I_AnimController
	{
	public:
		//@ c-tor
		I_AnimController() {};
		//@ d-tor
		virtual ~I_AnimController() {};
		//@ Save
		virtual void					Save(sysutils::C_XmlNode & parentNode) const = 0;
		//@ InitializeFromNode
		virtual bool					InitializeFromNode(const sysutils::C_XmlNode & ctrlInterpNode) = 0;
		//@ GetValue
		virtual void					GetValue(u32 frameId, float & outValue) const = 0;
		//@ Clone
		virtual T_AnimController	Clone() const = 0;
		//@ CreateReversed
		virtual T_AnimController	CreateReversed() const = 0;
		//@ SetSampled
		void								SetSampled(bool bSampled) { m_Sampled = bSampled; }
		//@ IsSampled
		bool								IsSampled() const { return m_Sampled; }

	private:
		bool								m_Sampled;
	};

}}