/*
* filename:			C_NoteTrackKey.h
*
* author:			Kral Jozef
* date:				05/04/2011		20:29
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/Xml/C_XmlNode.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_NoteTrackKey
	class C_NoteTrackKey
	{
	public:
		T_Hash32		m_EventId;
		T_Hash32		m_Param1;
		T_Hash32		m_Param2;

		//////////////////////////////////////////////////////////////////////////
		//@ Save
		bool			Save(u32 frameId, sysutils::C_XmlNode & node) const
		{
			sysutils::C_XmlNode * frame = node.CreateNode("Frame");
			sysutils::C_XmlNode * eventId = node.CreateNode("EventId");
			sysutils::C_XmlNode * param1 = node.CreateNode("Param1");
			sysutils::C_XmlNode * param2 = node.CreateNode("Param2");
			frame->Write(frameId);
			eventId->Write(m_EventId);
			param1->Write(m_Param1);
			param2->Write(m_Param2);

			return true;
		}


		//////////////////////////////////////////////////////////////////////////
		//@ InitializeFormNode
		u32		InitializeFromNode(const sysutils::C_XmlNode & keyNode)
		{
			const sysutils::C_XmlNode * tmpNode = keyNode.GetNode("Frame");
			u32 frmId;
			tmpNode->Read(frmId);
			tmpNode = keyNode.GetNode("EventId");
			tmpNode->Read(m_EventId);
			tmpNode = keyNode.GetNode("Param1");
			tmpNode->Read(m_Param1);
			tmpNode = keyNode.GetNode("Param2");
			tmpNode->Read(m_Param2);

			return frmId;
		}
	};
}}
