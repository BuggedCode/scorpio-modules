/*
* filename:			C_LinearFloatController.cpp
*
* author:			Kral Jozef
* date:				11/20/2010		15:45
* version:			1.00
* brief:
*/

#include "C_LinearFloatController.h"
#include <SysUtils/C_String.h>
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace sysutils;

	const C_String C_LinearFloatController::C_CONTROLLER_TYPENAME = "LinearFloat";

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Save
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LinearFloatController::Save(C_XmlNode & parentNode) const
	{
		C_XmlNode * nodeCtrl = parentNode.CreateNode("ControllerInterpolator");
		nodeCtrl->AddAttribute("Type", C_CONTROLLER_TYPENAME);

		for (T_KeyContainer::const_iterator iter = m_keysContainer.begin(); iter != m_keysContainer.end(); ++iter)
		{
			const S_Key & key = *iter;
			C_XmlNode * keyNode = nodeCtrl->CreateNode("Key");
			C_XmlNode * node = keyNode->CreateNode("Frame");
			node->Write(key.m_frameID);
			node = keyNode->CreateNode("Value");
			node->Write(key.m_fVal);
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitializeFromNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_LinearFloatController::InitializeFromNode(const C_XmlNode & ctrlInterpNode)
	{
		//@ KEY
		C_XmlNode::const_child_iterator it_begin = ctrlInterpNode.BeginChild("Key");
		C_XmlNode::const_child_iterator it_end	= ctrlInterpNode.EndChild("Key");


		//@ spracuju vsechny templaty v subore
		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & keyNode = *it_begin;
			if (!keyNode.IsValid())
			{
				TRACE_E("Invalid 'Key' element in LinearAnimController");
				continue;
			}

		
			//@ FRAME
			const C_XmlNode * node = keyNode.GetNode("Frame");
			if (!node || !node->IsValid())
			{
				TRACE_E("Invalid 'Frame' element in LinearAnimController");
				return false;
			}
			u32 frameId;
			if (!node->Read(frameId))
			{
				TRACE_E("Failed to read 'Frame' element in KeyNode");
				return false;
			}

			//@ VALUE
			node = keyNode.GetNode("Value");
			if (!node || !node->IsValid())
			{
				TRACE_E("Invalid 'Value' element in LinearAnimController");
				return false;
			}		
			float value;
			if (!node->Read(value))
			{
				TRACE_E("Failed to read 'Value' element in KeyNode");
				return false;
			}

			//@ pokud zjistim ze frameId neodpoveda indexu do vektora!musim nasamplovat vsetky rovnake hodnoty!
			if (IsSampled() && frameId != m_keysContainer.size())
			{
				SampleSameValues(frameId);
			}
			AddKey(frameId, value);
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SampleSameValues
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LinearFloatController::SampleSameValues(u32 frameID)
	{
		S_Key key = m_keysContainer.back();
		u32 keyCount = (u32)m_keysContainer.size();
		for (u32 i = 0; i < (frameID - keyCount); ++i)
		{
			u32 newFramId = keyCount + i;
			AddKey(newFramId, key.m_fVal);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetValue
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LinearFloatController::GetValue(u32 frameId, float &outValue) const
	{
		if (!IsSampled())
		{
			//@ Start Anim frame is before controller StartKey FrameId!
			SC_ASSERT(frameId >= m_keysContainer[0].m_frameID);
			if (frameId <= m_keysContainer[0].m_frameID)
			{
				frameId = m_keysContainer[0].m_frameID;
				outValue = m_keysContainer[0].m_fVal;
				return;
			}

			//@ Anim is behind controller controller LastKey FrameId!
			SC_ASSERT(frameId <= m_keysContainer[m_keysContainer.size()-1].m_frameID);
			if (frameId >= m_keysContainer[m_keysContainer.size()-1].m_frameID)
			{
				frameId = m_keysContainer[m_keysContainer.size()-1].m_frameID;
				outValue = m_keysContainer[m_keysContainer.size()-1].m_fVal;
				return;
			}


			//@ find Key & NextKey for interpolation
			u32 NextFrameId = -1;	//index to keyContainer
			u32 PrewFrameId = -1;
			int index = 0;
			for (T_KeyContainer::const_iterator iter = m_keysContainer.begin(); iter != m_keysContainer.end(); ++iter, ++index)
			{
				if (iter->m_frameID >= frameId)
				{
					NextFrameId = index;
					break;
				}
				PrewFrameId = index;
			}

			SC_ASSERT(PrewFrameId != -1);	//Impossible frameID is before First Key.FrameId, this case is catched above
			SC_ASSERT(NextFrameId != -1);	//Impossible frameID is behind Last Key.FrameId, this case is catched above

			//@ LERP
			u32 frameForInterp = m_keysContainer[NextFrameId].m_frameID - m_keysContainer[PrewFrameId].m_frameID;
			SC_ASSERT(frameForInterp);

			float fValRatio = (frameId - m_keysContainer[PrewFrameId].m_frameID ) / (float)frameForInterp;
			float fDiffVal = m_keysContainer[NextFrameId].m_fVal - m_keysContainer[PrewFrameId].m_fVal;
			fDiffVal *= fValRatio;

			outValue = m_keysContainer[PrewFrameId].m_fVal + fDiffVal;
			return;
		}
		else
		{
			if (frameId >= m_keysContainer.size())
			{
				frameId = (u32)m_keysContainer.size() - 1;
			}

			outValue = m_keysContainer[frameId].m_fVal;
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clone
	//
	//////////////////////////////////////////////////////////////////////////
	T_AnimController C_LinearFloatController::Clone() const
	{
		C_LinearFloatController * controller = new C_LinearFloatController();

		for (T_KeyContainer::const_iterator iter = m_keysContainer.begin(); iter != m_keysContainer.end(); ++iter)
		{
			controller->AddKey(iter->m_frameID, iter->m_fVal);
		}

		controller->SetSampled(IsSampled());

		return T_AnimController(controller);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateReversed
	//
	//////////////////////////////////////////////////////////////////////////
	T_AnimController C_LinearFloatController::CreateReversed() const
	{
		C_LinearFloatController * controller = new C_LinearFloatController();

		int nSize = (int)m_keysContainer.size();
		const S_Key & lastKey = m_keysContainer[nSize-1];
		
		for (int i= (nSize-1); i >= 0; --i)
		{
			controller->AddKey(lastKey.m_frameID - m_keysContainer[i].m_frameID, m_keysContainer[i].m_fVal);
		}

		return T_AnimController(controller);
	}

}}