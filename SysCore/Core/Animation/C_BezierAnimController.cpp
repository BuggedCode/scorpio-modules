/*
* filename:			C_BezierAnimController.cpp
*
* author:			Kral Jozef
* date:				11/16/2010		23:05
* version:			1.00
* brief:
*/

#include "C_BezierAnimController.h"
#include <SysUtils/Xml/C_XmlNode.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Save
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BezierAnimController::Save(C_XmlNode & parentNode) const
	{
		C_XmlNode * nodeCtrl = parentNode.CreateNode("ControllerInterpolator");
		nodeCtrl->AddAttribute("Type", "Bezier");

		for (std::vector<S_Key>::const_iterator iter = m_keysContainer.begin(); iter != m_keysContainer.end(); ++iter)
		{
			const S_Key & key = *iter;
			C_XmlNode * keyNode = nodeCtrl->CreateNode("Key");
			C_XmlNode * node = keyNode->CreateNode("Frame");
			node->Write(key.m_frame);
			node = keyNode->CreateNode("Value");
			node->Write(key.m_fVal);
			node = keyNode->CreateNode("InTanLen");
			node->Write(key.m_inLen);
			node = keyNode->CreateNode("InTanVal");
			node->Write(key.m_inTan);
			node = keyNode->CreateNode("OutTanLen");
			node->Write(key.m_outLen);
			node = keyNode->CreateNode("OutTanVal");
			node->Write(key.m_outTan);
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitializeFromNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_BezierAnimController::InitializeFromNode(const sysutils::C_XmlNode & ctrlInterpNode)
	{
		return true;
	}

}}