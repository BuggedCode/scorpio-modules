/*
* filename:			C_AnimationSet.h
*
* author:			Kral Jozef
* date:				11/17/2010		20:50
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysUtils/C_String.h>
#include <vector>
#include "C_AnimationTrack.h"
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_AnimationSet
	class SYSCORE_API C_AnimationSet
	{
	public:
		typedef std::vector<T_AnimationTrack>	T_AnimationTrackList;
		
	public:
		//@ c-tor
		C_AnimationSet()
			: m_startFrameID(-1), m_endFrameID(-1), m_FPS(-1) {};
		//@ d-tor
		~C_AnimationSet() {};

		//@ Load
		bool									Load(sysutils::T_Stream stream);
		//@ GetAnimationTrackList
		const T_AnimationTrackList &	GetAnimationTrackList() const { return m_animSetContainer; }


	private:
		T_AnimationTrackList	m_animSetContainer;
		u32						m_startFrameID;
		u32						m_endFrameID;
		u32						m_FPS;
	};

}};