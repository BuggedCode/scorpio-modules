/*
* filename:			C_BezierAnimController.h
*
* author:			Kral Jozef
* date:				11/14/2010		21:50
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include "I_AnimController.h"
#include <vector>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_BezierAnimCOntroller
	class SYSCORE_API C_BezierAnimController : public I_AnimController
	{
		struct S_Key
		{
			S_Key(u32 frame, float fVal, float inLen, float inTan, float outLen, float outTan)
				: m_frame(frame), m_fVal(fVal), m_inLen(inLen), m_inTan(inTan), m_outLen(outLen), m_outTan(outTan) {};

			u32		m_frame;
			float		m_fVal;
			float		m_inLen;
			float		m_inTan;
			float		m_outLen;
			float		m_outTan;
		};

	public:
		//@ c-tor
		C_BezierAnimController() {};
		//@ d-tor
		~C_BezierAnimController() {};
		
		//@ AddKey
		void						AddKey(u32 frameId, float fVal, float inLen, float inTan, float outLen, float outTan)
		{
			m_keysContainer.push_back(S_Key(frameId, fVal, inLen, inTan, outLen, outTan));
		}
		
		//@ Save
		virtual void			Save(sysutils::C_XmlNode & parentNode) const;
		//@ InitializeFromNode
		virtual bool			InitializeFromNode(const sysutils::C_XmlNode & ctrlInterpNode);
		//@ GetValue
		virtual void			GetValue(u32 frameId, float & outValue) const {};
		//@ Clone
		virtual T_AnimController	Clone() const { SC_ASSERT(false); return T_AnimController(); }
		//@ CreateReversed
		virtual T_AnimController	CreateReversed() const { SC_ASSERT(false); return T_AnimController(); }

	private:
		std::vector<S_Key>	m_keysContainer;
	};


}}