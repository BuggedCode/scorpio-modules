/*
* filename:			C_LinearPosController.h
*
* author:			Kral Jozef
* date:				01/14/2011		21:27
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include "I_AnimController.h"
#include <vector>
#include <SysMath/C_Vector2.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_LinearPosController
	class SYSCORE_API C_LinearPosController : public I_AnimController
	{
		struct S_Key
		{
			S_Key(u32 frame, const sysmath::C_Vector2 & value)
				: m_frameID(frame), m_Value(value) {};

			u32						m_frameID;
			sysmath::C_Vector2	m_Value;
		};
	public:
		//@ c-tor
		C_LinearPosController() {};
		//@ d-tor
		~C_LinearPosController() {};

		//@ AddKey
		void								AddKey(u32 frameId, const sysmath::C_Vector2 & value)
		{
			m_keysContainer.push_back(S_Key(frameId, value));
		}

		//@ Save
		virtual void					Save(sysutils::C_XmlNode & parentNode) const;
		//@ InitializeFromNode
		virtual bool					InitializeFromNode(const sysutils::C_XmlNode & ctrlInterpNode);
		//@ GetValue
		virtual void					GetValue(u32 frameId, float & outValue) const { SC_ASSERT(false); }
		//@ GetValue
		void								GetValue(u32 frameId, sysmath::C_Vector2 & outValue) const;
		//@ Clone
		virtual T_AnimController	Clone() const;
		//@ CreateReversed
		virtual T_AnimController	CreateReversed() const;
		//@ Transform - transform animation Values (not Keys) - translation & rotoation affect just Position values!
		void								TransformValues(const sysmath::C_Vector2 & translVct, float fAngle, float fScale);

	private:
		void								SampleSameValues(u32 frameID);

	public:
		static const sysutils::C_String	C_CONTROLLER_TYPENAME;

	private:
		typedef std::vector<S_Key>	T_KeyContainer;
		T_KeyContainer			m_keysContainer;
	};


	typedef boost::shared_ptr<C_LinearPosController>	T_LinearPosController;

}}