/*
* filename:			C_AnimationTrack.cpp
*
* author:			Kral Jozef
* date:				11/15/2010		21:10
* version:			1.00
* brief:
*/

#include "C_AnimationTrack.h"
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>
#include "C_BezierAnimController.h"
#include "C_AnimControllerFactory.h"
#include "C_LinearPosController.h"
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_AnimationTrack::C_AnimationTrack(bool bSampled)
		: m_flags(0), m_startFrameID(-1), m_endFrameID(-1), m_FPS(30), m_Looped(false), m_Sampled(bSampled)
	{
		//////////////////////////////////////////////////////////////////////////
	}

	C_AnimationTrack::~C_AnimationTrack()
	{

		//////////////////////////////////////////////////////////////////////////
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddController
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AnimationTrack::AddController(E_AnimControllerType type, T_AnimController controller)
	{
		SC_ASSERT(controller);
		SC_ASSERT((m_flags & (u32)type) == false);	//controller is not in track
		
		m_flags |= (u32)type;

		m_controllers.insert(T_ControllerMap::value_type(type, controller));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TypeToString
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_AnimationTrack::TypeToString(E_AnimControllerType controllerType) const
	{
		switch(controllerType)
		{
		case E_ACT_POSITION:
			return "POSITION";
			break;
		case E_ACT_ROT_X:
			return "ROT_X";
			break;
		case E_ACT_ROT_Y:
			return "ROT_Y";
			break;
		case E_ACT_ROT_Z:
			return "ROT_Z";
			break;
		case E_ACT_SCA_X:
			return "SCALE_X";
			break;
		case E_ACT_SCA_Y:
			return "SCALE_Y";
			break;
		case E_ACT_SCA_Z:
			return "SCALE_Z";
			break;
		case E_ACT_ALPHA:
			return "ALPHA";
			break;
		case E_ACT_VELOCITY:
			return "VELOCITY";
		default:
			SC_ASSERT(false);
			TRACE_E("Unsupported controller type!");
		};	//switch

		return "UNKNOW_CONTROLLER";
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ StringToType
	//
	//////////////////////////////////////////////////////////////////////////
	E_AnimControllerType C_AnimationTrack::StringToType(sysutils::C_String controllerType) const
	{
		if (controllerType == "POSITION")
			return E_ACT_POSITION;
		else if (controllerType == "ROT_X")
			return E_ACT_ROT_X;
		else if (controllerType == "ROT_Y")
			return E_ACT_ROT_Y;
		else if (controllerType == "ROT_Z")
			return E_ACT_ROT_Z;
		else if (controllerType == "SCALE_X")
			return E_ACT_SCA_X;
		else if (controllerType == "SCALE_Y")
			return E_ACT_SCA_Y;
		else if (controllerType == "SCALE_Z")
			return E_ACT_SCA_Z;
		else if (controllerType == "ALPHA")
			return E_ACT_ALPHA;
		else if (controllerType == "VELOCITY")
			return E_ACT_VELOCITY;

		SC_ASSERT(false);
		TRACE_FE("Unsupported controller type: %s!", controllerType.c_str());

		return E_ACT_UNKNOWN;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Save
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AnimationTrack::Save(C_XmlNode & parentNode) const
	{
		C_XmlNode * tmpNode  = parentNode.CreateNode("Sampled");
		tmpNode->Write(m_Sampled);

		tmpNode  = parentNode.CreateNode("Looped");
		tmpNode->Write(m_Looped);

		for (T_ControllerMap::const_iterator iter = m_controllers.begin(); iter != m_controllers.end(); ++iter)
		{
			C_XmlNode * ctrlNode = parentNode.CreateNode("Controller");
			ctrlNode->AddAttribute("Type", TypeToString(iter->first));
			iter->second->Save(*ctrlNode);
		}

		//@ write NoteTrack
		C_XmlNode * ntNode = parentNode.CreateNode("NoteTrack");
		for (T_NoteTrack::const_iterator iter = m_NoteTrack.begin(); iter != m_NoteTrack.end(); ++iter)
		{
			const C_NoteTrackKey & ntKey = iter->second;
			C_XmlNode * key = ntNode->CreateNode("Key");
			ntKey.Save(iter->first, *key);
		}
		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Load
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimationTrack::InitializeFromNode(const sysutils::C_XmlNode & trackNode)
	{
		const C_XmlAttribute * att = trackNode.GetAttribute("TrackName");
		if (!att)
		{
			TRACE_E("Animation track without ObjectName attribute");
			return false;
		}

		m_trackName = att->GetValue();


		const C_XmlNode * tmpNode = trackNode.GetNode("Sampled");
		if (tmpNode && tmpNode->IsValid())
		{
			tmpNode->Read(m_Sampled);
		}
		else
		{
			TRACE_FE("Animation Track: %s has no Sampled node!", m_trackName.c_str());
		}


		tmpNode = trackNode.GetNode("Looped");
		if (tmpNode && tmpNode->IsValid())
		{
			tmpNode->Read(m_Looped);
		}

		//@ Load NoteTrack
		T_XmlNodeList ntKeys = trackNode.SelectNodes("NoteTrack/Key");
		for (T_ConstXmlNodePtrVector::iterator iter = ntKeys->begin(); iter != ntKeys->end(); ++iter)
		{
			C_NoteTrackKey ntKey;
			u32 frmId = ntKey.InitializeFromNode(**iter);
			m_NoteTrack.insert(T_NoteTrack::value_type(frmId, ntKey));
		}


		C_XmlNode::const_child_iterator it_begin = trackNode.BeginChild("Controller");
		C_XmlNode::const_child_iterator it_end	= trackNode.EndChild("Controller");

		//@ spracuju vsechny templaty v subore
		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			att = tempNode.GetAttribute("Type");
			if (!att)
			{
				TRACE_FE("Invalid attribute 'Type' in 'Controller' node. TrackName: %s", m_trackName.c_str());
				continue;
			}
			C_String ctrlType = att->GetValue();
			E_AnimControllerType acType = StringToType(ctrlType);
			m_flags |= acType;

			const C_XmlNode * tmpNode = tempNode.GetNode("ControllerInterpolator");
			if (!tmpNode || !tmpNode->IsValid())
			{
				TRACE_FE("Invalid 'ControllerInterpolator' node in trackNode: %s", m_trackName.c_str());
				continue;
			}

			att = tmpNode->GetAttribute("Type");
			if (!att)
			{
				TRACE_FE("Invalid attribute 'Type' in 'Controller' node. TrackName: %s", m_trackName.c_str());
				continue;
			}

			C_String ctrlInterpType = att->GetValue();

			T_AnimController controller = C_AnimControllerFactory::CreateController(ctrlInterpType);
			if (!controller)
			{
				TRACE_E("Null Controller!");
				continue;
			}

			controller->SetSampled(m_Sampled);
			bool bRes = controller->InitializeFromNode(*tmpNode);
			if (!bRes)
			{
				TRACE_E("Failed To Initialize Controller!");
				continue;
			}


#ifdef NO_FINAL
			SC_ASSERT(m_controllers.find(acType) == m_controllers.end());
#endif
			m_controllers[acType] = controller;
		}

		
		return true;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetSampleData
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AnimationTrack::GetSampleData(u32 frameID, C_SampleData & outSample) const
	{
		SC_ASSERT(frameID >= 0);
		SC_ASSERT(frameID <= m_endFrameID);

		if (frameID > m_endFrameID)
			frameID = m_endFrameID;

		//@ calculate pos/rot/scale/alpha

		if (m_flags & E_ACT_POSITION)
		{
			T_ControllerMap::const_iterator iter = m_controllers.find(E_ACT_POSITION);
			T_LinearPosController linPosCtrl = boost::dynamic_pointer_cast<C_LinearPosController>(iter->second);
			linPosCtrl->GetValue(frameID, outSample.m_vctPos);
			outSample.m_flags |= (C_SampleData::E_PT_POSITION);
		}

		if (m_flags & E_ACT_ROT_Z)
		{
			T_ControllerMap::const_iterator iter = m_controllers.find(E_ACT_ROT_Z);
			(iter->second)->GetValue(frameID, outSample.m_fAngle);
			outSample.m_flags |= (C_SampleData::E_PT_ROTATION);
		}


		if (m_flags & E_ACT_SCA_X)
		{
			T_ControllerMap::const_iterator iter = m_controllers.find(E_ACT_SCA_X);
			(iter->second)->GetValue(frameID, outSample.m_fScale);
			outSample.m_flags |= C_SampleData::E_PT_SCALE;
		}

		if (m_flags & E_ACT_ALPHA)
		{
			T_ControllerMap::const_iterator iter = m_controllers.find(E_ACT_ALPHA);
			(iter->second)->GetValue(frameID, outSample.m_fAlpha);
			outSample.m_flags |= C_SampleData::E_PT_ALPHA;
		}

		if (m_flags & E_ACT_VELOCITY)
		{
			T_ControllerMap::const_iterator iter = m_controllers.find(E_ACT_VELOCITY);
			(iter->second)->GetValue(frameID, outSample.m_fVelocity);
			outSample.m_flags |= C_SampleData::E_PT_VELOCITY;
		}

		if (!m_NoteTrack.empty())
		{
			std::pair<T_NoteTrack::const_iterator, T_NoteTrack::const_iterator> pair = m_NoteTrack.equal_range(frameID);
			for (T_NoteTrack::const_iterator iter = pair.first; iter != pair.second; ++iter)
			{
				outSample.m_NoteTrackKeys.push_back(iter->second);
			}
			outSample.m_flags |= C_SampleData::E_PT_TRACK_NOTE;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clone
	//
	//////////////////////////////////////////////////////////////////////////
	T_AnimationTrack C_AnimationTrack::Clone() const
	{
		C_AnimationTrack * animTrack = new C_AnimationTrack(m_Sampled);
		animTrack->m_startFrameID = m_startFrameID;
		animTrack->m_endFrameID = m_endFrameID;
		animTrack->m_trackName = m_trackName;
		C_String str = C_String::Format("_Cloned_%x", animTrack);
		animTrack->m_trackName += m_trackName;
		animTrack->m_trackName += str;
		animTrack->m_FPS = m_FPS;

		for (T_ControllerMap::const_iterator iter = m_controllers.begin(); iter != m_controllers.end(); ++iter)
		{
			T_AnimController ctrl = iter->second->Clone();
			animTrack->AddController(iter->first, ctrl);
		}

		return T_AnimationTrack(animTrack);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateReversed
	//
	//////////////////////////////////////////////////////////////////////////
	T_AnimationTrack C_AnimationTrack::CreateReversed() const
	{
		C_AnimationTrack * animTrack = new C_AnimationTrack(m_Sampled);
		animTrack->m_startFrameID = m_startFrameID;
		animTrack->m_endFrameID = m_endFrameID;
		animTrack->m_trackName = "Reversed_";
		animTrack->m_trackName += m_trackName;
		animTrack->m_FPS = m_FPS;

		for (T_ControllerMap::const_iterator iter = m_controllers.begin(); iter != m_controllers.end(); ++iter)
		{
			T_AnimController ctrl = iter->second->CreateReversed();
			animTrack->AddController(iter->first, ctrl);
		}

		return T_AnimationTrack(animTrack);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TransformValues
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AnimationTrack::TransformValues(const sysmath::C_Vector2 & translVct, float fAngle, float fScale)
	{
		if (m_flags & E_ACT_POSITION)
		{
			T_ControllerMap::const_iterator iter = m_controllers.find(E_ACT_POSITION);
			T_LinearPosController posCtrl = boost::dynamic_pointer_cast<C_LinearPosController>(iter->second);
			posCtrl->TransformValues(translVct, fAngle, fScale);
		}


		/*if (m_flags & E_ACT_POS_Z)
		{
			T_ControllerMap::const_iterator iter = m_controllers.find(E_ACT_POS_Z);
			iter->second->TransformValues();
		}*/
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddNoteTrackKey
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AnimationTrack::AddNoteTrackKey(u32 frameId, T_Hash32 eventId, T_Hash32 param1, T_Hash32 param2)
	{
		C_NoteTrackKey ntKey;
		ntKey.m_EventId = eventId;
		ntKey.m_Param1 = param1;
		ntKey.m_Param2 = param2;
		m_NoteTrack.insert(T_NoteTrack::value_type(frameId, ntKey));
	}


}}