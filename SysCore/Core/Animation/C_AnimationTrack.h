/*
* filename:			C_AnimationTrack.h
*
* author:			Kral Jozef
* date:				11/15/2010		21:07
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <boost/shared_ptr.hpp>
#include <SysUtils/C_String.h>
#include <SysMath/C_Vector2.h>
#include "I_AnimController.h"
#include <map>
#include <vector>
#include "C_SampleData.h"
#include <SysUtils/C_HashName.h>
#include "C_NoteTrackKey.h"

namespace scorpio{
	namespace sysutils{
		class C_XmlNode;
	}
}

namespace scorpio{ namespace syscore{

	enum E_AnimControllerType
	{
		E_ACT_UNKNOWN = (1 << 0),

		E_ACT_POSITION = (1 << 1),	//XYZ

		E_ACT_ROT_X	= (1 << 4),
		E_ACT_ROT_Y	= (1 << 5),
		E_ACT_ROT_Z	= (1 << 6),

		//@ for PTK render is used only uniform scale
		E_ACT_SCA_X	= (1 << 7),	//non-uniform scale
		E_ACT_SCA_Y	= (1 << 8),	//non-uniform scale
		E_ACT_SCA_Z	= (1 << 9),	//non-uniform scale

		E_ACT_ALPHA	= (1 << 10),	//not used DON'T USE!
		E_ACT_VELOCITY = (1 << 11),	//animated custom att Velocity
	};

	class C_AnimationTrack;

	typedef boost::shared_ptr<C_AnimationTrack>	T_AnimationTrack;

	//////////////////////////////////////////////////////////////////////////
	//@ C_AnimationTrack
	class SYSCORE_API C_AnimationTrack
	{
	public:
		//@ c-tor
		C_AnimationTrack(bool bSampled);
		//@ d-tor
		~C_AnimationTrack();

		//@ Load
		bool							InitializeFromNode(const sysutils::C_XmlNode & trackNode);

		//@ SetStartFrame
		void							SetStartFrame(u32 startFrameID) { m_startFrameID = startFrameID; }
		//@ GetStartFrame
		u32							GetStartFrame() const { return m_startFrameID; }
		//@ SetEndFrame
		void							SetEndFrame(u32 endFrameID) { m_endFrameID = endFrameID; }
		//@ GetEndFrame
		u32							GetEndFrame() const { return m_endFrameID; }
		//@ GetSample
		void							GetSampleData(u32 frameID, C_SampleData & outSample) const;

		//@ AddController
		void							AddController(E_AnimControllerType type, T_AnimController controller);
		//@ Save
		void							Save(sysutils::C_XmlNode & parentNode) const;

		//@ SetTrackName
		void							SetTrackName(const sysutils::C_String trackName) { m_trackName = trackName; }
		//@ GetTrackName
		sysutils::C_String		GetTrackName() const { return m_trackName; }
		//@ SetFPS
		void							SetFPS(u32 pFPS) { m_FPS = pFPS; }
		//@ GetFPS
		u32							GetFPS() const { return m_FPS; }
		//@ IsEmpty
		bool							empty() const { return m_controllers.empty(); }
		//@ SetLooped
		void							SetLooped(bool bLooped) { m_Looped = bLooped; }
		//@ IsLooped
		bool							IsLooped() const { return m_Looped; }
		//@ IsSampled
		bool							IsSampled() const { return m_Sampled; }
		//@ AddNoteTrackKey
		void							AddNoteTrackKey(u32 frameId, T_Hash32 eventId, T_Hash32 param1, T_Hash32 param2);


		//@ Clone - create new copy of animTrack
		T_AnimationTrack			Clone() const;
		//@ CreateReversed
		//@ retVal - new reversed animation track
		T_AnimationTrack			CreateReversed() const;
		//@ Transform - transform animation Values (not Keys) - affect just Position values!
		void							TransformValues(const sysmath::C_Vector2 & translVct, float fAngle, float fScale);

	private:
		//@ TypeTostring
		sysutils::C_String		TypeToString(E_AnimControllerType controllerType) const;
		//@ StringToType
		E_AnimControllerType		StringToType(sysutils::C_String controllerType) const;


	private:
		typedef std::map<E_AnimControllerType, T_AnimController>	T_ControllerMap;
		T_ControllerMap		m_controllers;
		u32						m_flags;	//flags designed which controllers are in AnimTrack
		sysutils::C_String	m_trackName;
		u32						m_startFrameID;
		u32						m_endFrameID;
		u32						m_FPS;		//frames per second, frame change in 1/m_FPS*1000 ms
		bool						m_Looped;	//Animation is Looped
		bool						m_Sampled;	//animation is Sampled/ FALSE = Keyable animation
		typedef std::multimap<u32,  C_NoteTrackKey>	T_NoteTrack;
		T_NoteTrack				m_NoteTrack;
	};

}}