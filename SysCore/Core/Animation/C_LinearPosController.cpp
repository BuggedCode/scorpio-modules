/*
* filename:			C_LinearPosController.cpp
*
* author:			Kral Jozef
* date:				01/14/2011		21:31
* version:			1.00
* brief:
*/

#include "C_LinearPosController.h"
#include <SysUtils/C_String.h>
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{


	using namespace sysutils;
	using namespace sysmath;

	const C_String C_LinearPosController::C_CONTROLLER_TYPENAME = "LinearPos";

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Save
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LinearPosController::Save(C_XmlNode & parentNode) const
	{
		C_XmlNode * nodeCtrl = parentNode.CreateNode("ControllerInterpolator");
		nodeCtrl->AddAttribute("Type", C_CONTROLLER_TYPENAME);

		for (T_KeyContainer::const_iterator iter = m_keysContainer.begin(); iter != m_keysContainer.end(); ++iter)
		{
			const S_Key & key = *iter;
			C_XmlNode * keyNode = nodeCtrl->CreateNode("Key");
			C_XmlNode * node = keyNode->CreateNode("Frame");
			node->Write(key.m_frameID);
			node = keyNode->CreateNode("Value");
			node->Write(key.m_Value);
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitializeFromNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_LinearPosController::InitializeFromNode(const C_XmlNode & ctrlInterpNode)
	{
		//@ KEY
		C_XmlNode::const_child_iterator it_begin = ctrlInterpNode.BeginChild("Key");
		C_XmlNode::const_child_iterator it_end	= ctrlInterpNode.EndChild("Key");


		//@ spracuju vsechny templaty v subore
		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & keyNode = *it_begin;
			if (!keyNode.IsValid())
			{
				TRACE_E("Invalid 'Key' element in LinearPosController");
				continue;
			}


			//@ FRAME
			const C_XmlNode * node = keyNode.GetNode("Frame");
			if (!node || !node->IsValid())
			{
				TRACE_E("Invalid 'Frame' element in LinearPosController");
				return false;
			}
			u32 frameId;
			if (!node->Read(frameId))
			{
				TRACE_E("Failed to read 'Frame' element in KeyNode");
				return false;
			}

			//@ VALUE
			node = keyNode.GetNode("Value");
			if (!node || !node->IsValid())
			{
				TRACE_E("Invalid 'Value' element in LinearPosController");
				return false;
			}		
			
			C_Vector2 vctPos;
			if (!node->Read(vctPos))
			{
				TRACE_E("Failed to read 'Value' element in KeyNode");
				return false;
			}

			//@ sample only if controller is exported in Sampled animTrack, pokud zjistim ze frameId neodpoveda indexu do vektora!musim nasamplovat vsetky rovnake hodnoty!
			if (IsSampled() && frameId != m_keysContainer.size())
			{
				SampleSameValues(frameId);
			}

			AddKey(frameId, vctPos);
		}

		return true;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ SampleSameValues
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LinearPosController::SampleSameValues(u32 frameID)
	{
		S_Key key = m_keysContainer.back();
		u32 keyCount = (u32)m_keysContainer.size();
		for (u32 i = 0; i < (frameID - keyCount); ++i)
		{
			u32 newFramId = keyCount + i;
			AddKey(newFramId, key.m_Value);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetValue
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LinearPosController::GetValue(u32 frameId, C_Vector2 & outValue) const
	{
		if (!IsSampled())
		{
			//@ Start Anim frame is before controller StartKey FrameId!
			SC_ASSERT(frameId >= m_keysContainer[0].m_frameID);
			if (frameId <= m_keysContainer[0].m_frameID)
			{
				frameId = m_keysContainer[0].m_frameID;
				outValue = m_keysContainer[0].m_Value;
				return;
			}

			//@ Anim is behind controller controller LastKey FrameId!
			SC_ASSERT(frameId <= m_keysContainer[m_keysContainer.size()-1].m_frameID);
			if (frameId >= m_keysContainer[m_keysContainer.size()-1].m_frameID)
			{
				frameId = m_keysContainer[m_keysContainer.size()-1].m_frameID;
				outValue = m_keysContainer[m_keysContainer.size()-1].m_Value;
				return;
			}


			//@ find Key & NextKey for interpolation
			u32 NextFrameId = -1;	//index to keyContainer
			u32 PrewFrameId = -1;
			int index = 0;
			for (T_KeyContainer::const_iterator iter = m_keysContainer.begin(); iter != m_keysContainer.end(); ++iter, ++index)
			{
				if (iter->m_frameID >= frameId)
				{
					NextFrameId = index;
					break;
				}
				PrewFrameId = index;
			}

			SC_ASSERT(PrewFrameId != -1);	//Impossible frameID is before First Key.FrameId, this case is catched above
			SC_ASSERT(NextFrameId != -1);	//Impossible frameID is behind Last Key.FrameId, this case is catched above

			//@ LERP
			u32 frameForInterp = m_keysContainer[NextFrameId].m_frameID - m_keysContainer[PrewFrameId].m_frameID;
			SC_ASSERT(frameForInterp);

			float fValRatio = (frameId - m_keysContainer[PrewFrameId].m_frameID ) / (float)frameForInterp;
			C_Vector2 fDiffVal = m_keysContainer[NextFrameId].m_Value - m_keysContainer[PrewFrameId].m_Value;
			fDiffVal *= fValRatio;

			outValue = m_keysContainer[PrewFrameId].m_Value + fDiffVal;
			return;
		}
		else
		{
			if (frameId >= m_keysContainer.size())
			{
				frameId = (u32)m_keysContainer.size() - 1;
			}

			outValue = m_keysContainer[frameId].m_Value;
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clone
	//
	//////////////////////////////////////////////////////////////////////////
	T_AnimController C_LinearPosController::Clone() const
	{
		C_LinearPosController * controller = new C_LinearPosController();

		for (T_KeyContainer::const_iterator iter = m_keysContainer.begin(); iter != m_keysContainer.end(); ++iter)
		{
			controller->AddKey(iter->m_frameID, iter->m_Value);
		}

		controller->SetSampled(IsSampled());

		return T_AnimController(controller);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateReversed
	//
	//////////////////////////////////////////////////////////////////////////
	T_AnimController C_LinearPosController::CreateReversed() const
	{
		C_LinearPosController * controller = new C_LinearPosController();

		int nSize = (int)m_keysContainer.size();
		const S_Key & lastKey = m_keysContainer[nSize-1];

		for (int i= (nSize-1); i >= 0; --i)
		{
			controller->AddKey(lastKey.m_frameID - m_keysContainer[i].m_frameID, m_keysContainer[i].m_Value);
		}

		return T_AnimController(controller);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TransformValues
	//
	//////////////////////////////////////////////////////////////////////////
	void C_LinearPosController::TransformValues(const sysmath::C_Vector2 & translVct, float fAngle, float fScale)
	{
		for (T_KeyContainer::iterator iter = m_keysContainer.begin(); iter != m_keysContainer.end(); ++iter)
		{
			iter->m_Value.rotateBy(fAngle);
			iter->m_Value *= fScale;
			iter->m_Value += translVct;
		}
	}

}}