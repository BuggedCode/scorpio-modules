/*
* filename:			C_AnimControllerFactory.h
*
* author:			Kral Jozef
* date:				11/17/2010		22:54
* version:			1.00
* brief:
*/

#pragma once

#include <SysUtils/C_String.h>
#include "I_AnimController.h"

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_AnimControllerfactory
	class C_AnimControllerFactory
	{
	public:
		static T_AnimController		CreateController(sysutils::C_String controllerType);
	};

}}

