/*
* filename:			C_AnimationSet.cpp
*
* author:			Kral Jozef
* date:				11/17/2010		20:51
* version:			1.00
* brief:
*/

#include "C_AnimationSet.h"
#include <SysUtils/Xml/C_XmlFile.h>
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;

	static const C_String C_STR_ANIMATION_SET_FILE = "Scorpio_AnimationSet_file";


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Load
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimationSet::Load(sysutils::T_Stream stream)
	{
		C_XmlFile file;
		bool bRes = file.Load(stream);
		if (!bRes)
			return false;

		//parse xml
		if (!file.IsValid())
			return false;

		const C_XmlNode * root = file.GetRoot();
		if (!root || !root->IsValid())
			return false;

		if (root->GetName() != C_STR_ANIMATION_SET_FILE)
			return false;

		const C_XmlNode * tmpNode = root->GetNode("StartFrame");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Missing 'StartFrame' element in Animation Set File: %s ", stream->GetFileName());
			return false;
		}

		if (!tmpNode->Read(m_startFrameID))
		{
			TRACE_FE("Error in read 'StartFrame' element in Animation Set File: %s ", stream->GetFileName());
			return false;
		}

		tmpNode = root->GetNode("EndFrame");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Missing 'EndFrame' element in Animation Set File: %s ", stream->GetFileName());
			return false;
		}

		if (!tmpNode->Read(m_endFrameID))
		{
			TRACE_FE("Error in read 'EndFrame' element in Animation Set File: %s ", stream->GetFileName());
			return false;
		}

		tmpNode = root->GetNode("FPS");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Missing 'FPS' element in Animation Set File: %s ", stream->GetFileName());
			return false;
		}

		if (!tmpNode->Read(m_FPS))
		{
			TRACE_FE("Error in read 'FPS' element in Animation Set File: %s ", stream->GetFileName());
			return false;
		}


		C_XmlNode::const_child_iterator it_begin = root->BeginChild("Track");
		C_XmlNode::const_child_iterator it_end	= root->EndChild("Track");

		//@ spracuju vsechny templaty v subore
		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			T_AnimationTrack animtrack = T_AnimationTrack(new C_AnimationTrack(true));	//sampled animation
			animtrack->SetStartFrame(m_startFrameID);
			animtrack->SetEndFrame(m_endFrameID);
			animtrack->SetFPS(m_FPS);
			bool bRes = animtrack->InitializeFromNode(tempNode);
			if (bRes)
			{
				m_animSetContainer.push_back(animtrack);
			}
		}

		return true;
	}


}}