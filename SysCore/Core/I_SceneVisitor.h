/*
* filename:			I_SceneVisitor.h
*
* author:			Kral Jozef
* date:				01/18/2011		1:41
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/Frames/C_Frame.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ I_SceneVisitor
	class I_SceneVisitor
	{
	public:
		//@ Visit
		virtual void			Visit(T_Frame frame) = 0;	//JK TODO retVal SKIP_ALL, CONTINUE, ...
	};

}}