/*
* filename:			C_BitmapFont.cpp
*
* author:			Kral Jozef
* date:				11/04/2010		21:34
* version:			1.00
* brief:
*/

#include "C_BitmapFont.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysUtils/Utils/C_StringUtils.h>
#include <sstream>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;

	static const u32 C_CHAR_MAX_COUNT = 0xFF + 1;
	static const C_String C_CHAR_TAG = "Char=";
	static const u8 C_CHAR_TAG_LEN = 4;	//strlen(C_CHAR_TAG);

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_BitmapFont::C_BitmapFont(const T_String & fontFileName, sysrender::T_GraphicRes fontTexture)
		: m_fontTexture(fontTexture), m_FontFileName(fontFileName), m_CharHeight(1)
	{
		T_String fntName = C_StringUtils::TrimExtension(fontFileName);
		fntName += ".fnt";
		
      T_Stream stream = T_FileSystemManager::GetInstance().Open(fntName);

		m_characterMap.resize(C_CHAR_MAX_COUNT, S_CharInfo());

		if (stream->IsOpen())
		{
			const u32 C_BUFFER_SIZE = 1024;
			char buffer[C_BUFFER_SIZE];
			ZERO_ARRAY(buffer);

			u16 index = 0;
			while(!stream->EndOfStream())
			{
				stream->ReadLine(buffer, C_BUFFER_SIZE);
				C_String strTmp(buffer);

				C_String strTag = strTmp.Left(C_CHAR_TAG_LEN);	//<= strlen(C_CHAR_TAG);

				if (strTag != C_CHAR_TAG)
					continue;

				if (index >= C_CHAR_MAX_COUNT)
				{
					//SC_ASSERT(false);
					TRACE_FE("IGNORING: Too many characters defined in file: %s", fntName.c_str());
					break;
				}

				strTmp = C_String::SubString(strTmp, C_CHAR_TAG_LEN + 1, strTmp.length());
				
				std::vector<C_String> strList;
				u32 charCode = -1;

				char n = strTmp[(u32)0];
				if ( n == '\"')
				{
					C_String code = C_String::SubString(strTmp, 1, 1);
					charCode = *code.c_str();
					strTmp = C_String::SubString(strTmp, 4, strTmp.length());
					strTmp.Split(',', strList);
				}
				else
				{
					C_String code = C_String::SubString(strTmp, 0, 1);
					std::stringstream ss;
					ss << std::hex << code.c_str();
					ss >> charCode;

					strTmp = C_String::SubString(strTmp, 3, strTmp.length());
					strTmp.Split(',', strList);
				}

				SC_ASSERT(charCode != -1 && strList.size() >= 4);
				
				m_characterMap[charCode].m_coordX = strList[0].ToInt();
				m_characterMap[charCode].m_coordY = strList[1].ToInt();
				m_characterMap[charCode].m_width = strList[2].ToInt();
				m_characterMap[charCode].m_height = strList[3].ToInt();

				m_CharHeight = m_characterMap[charCode].m_height;

				++index;
			}
		}
		else
		{
			TRACE_FE("Can not open Font file: %s", fntName.c_str());
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BitmapFont::Render(const sysmath::C_Vector2 & dstPos, const sysmath::C_Point & size, char character, float fAngle, const C_Vector4 & vctColor) const
	{
		C_Rect srcRect = GetSrcRectangle(character);
		C_Vector2 texCoordX(srcRect.m_left / m_fontTexture->GetWidth(), srcRect.m_right / m_fontTexture->GetWidth());
		C_Vector2 texCoordY(srcRect.m_top / m_fontTexture->GetHeight(), srcRect.m_bottom / m_fontTexture->GetHeight());
		if (m_fontTexture)
		{
			m_fontTexture->Render(dstPos, size, texCoordX, texCoordY, vctColor, fAngle, 1.f);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetSrcRectangle
	//
	//////////////////////////////////////////////////////////////////////////
	C_Rect C_BitmapFont::GetSrcRectangle(char character) const
	{
		const S_CharInfo & charInfo = m_characterMap[(u8)character];
		if (charInfo.m_coordX == -1 || charInfo.m_coordY == -1 || charInfo.m_width == -1 || charInfo.m_height == -1)
		{
			TRACE_FE("Character %c is not in font texture: %s", character, m_FontFileName.c_str());
			return C_Rect();
		}

		C_Rect rect;
		rect.m_left = charInfo.m_coordX;// + 1;	//HACK jak prase pretoze sa zobrazoval cast znaku pred, asi koli lin filteringu
		rect.m_right = rect.m_left + charInfo.m_width;
		rect.m_top = charInfo.m_coordY;
		rect.m_bottom = rect.m_top + charInfo.m_height;
		return rect;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetCharacterSize
	//
	//////////////////////////////////////////////////////////////////////////
	C_Point C_BitmapFont::GetCharacterSize(char character) const
	{
		const S_CharInfo & charInfo = m_characterMap[(u8)character];
		if (charInfo.m_coordX == -1 || charInfo.m_coordY == -1 || charInfo.m_width == -1 || charInfo.m_height == -1)
		{
			TRACE_FE("Chaarcter %c is not in font texture: %s", character, m_FontFileName.c_str());
			return C_Point();
		}

		//@ HACK - //because ' ' space character has tool litle pixel for width (aprox. 3pix), manualy increase it's width
		if (character == ' ')
			return C_Point(charInfo.m_width * 4, charInfo.m_height);

		return C_Point(charInfo.m_width, charInfo.m_height);
	}

}}