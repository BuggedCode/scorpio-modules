/*
* filename:			C_BitmapFont.h
*
* author:			Kral Jozef
* date:				11/04/2010		21:34
* version:			1.00
* brief:
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <SysCore/Render/C_Graphic.h>
#include <vector>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Rect.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_BitmapFont
	class SYSCORE_API C_BitmapFont
	{
		struct S_CharInfo
		{
			//@ c-tor
			S_CharInfo() 
				: m_coordX(-1), m_coordY(-1), m_width(-1), m_height(-1) {};
			u32	m_coordX;	//X-coord into bitmap
			u32	m_coordY;	//Y-coord into bitmap
			u8		m_width;		//width of char
			u8		m_height;	//height of char
		};

	public:
		//@ c-tor
		C_BitmapFont(const T_String & fontFileName, sysrender::T_GraphicRes fontTexture);
		//@ d-tor
		~C_BitmapFont() {};

		//@ Load
		//bool						Load(sysutils::C_String fileName);

		//@ Render
		void							Render(const sysmath::C_Vector2 & dstPos, const sysmath::C_Point & size, char character, float fAngle, const sysmath::C_Vector4 & vctColor) const;
		//@ GetCharacterSize
		sysmath::C_Point			GetCharacterSize(char character) const;	//TODO instead of ret copy - pass into reference(on stack) as parameter into

		//@ GetHeight
		u16							GetHeight() const { return m_CharHeight; }

	private:
		sysmath::C_Rect			GetSrcRectangle(char character) const;	//TODO instead of ret copy - pass into reference(on stack) as parameter into
		//@ CheckForSpaceCharacterWidth
		void							CheckForSpaceCharacterWidth(u8 & outWidth);


	private:
		sysrender::T_GraphicRes	m_fontTexture;	//graphics for font
		std::vector<S_CharInfo>	m_characterMap;

		T_String				m_FontFileName;
		u8						m_CharHeight;
	};

	typedef boost::shared_ptr<C_BitmapFont>	T_BitmapFont;

}}