/*
* filename:			C_BitmapFontManager.h
*
* author:			Kral Jozef
* date:				11/04/2010		21:49
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <Common/C_Singleton.h>
#include "C_BitmapFont.h"
#include <map>
#include <vector>
#include <SysUtils/C_String.h>
#include <SysUtils/C_HashName.h>
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_BitmapFontManager
	class SYSCORE_API C_BitmapFontManager
	{
		friend class C_Singleton<C_BitmapFontManager>;

	public:
		//@ Clear
		void				Clear();

		//@ LoadFonts - physically load fonts and insert into manager
		bool				LoadFonts(const sysutils::T_Stream inputStream);
		//@ AddFont
		//@ fontFileName - relative path from data directory in native Win32 format
		//@ adds fonts to manager/ but not load from disk(just request - need call after PreloadFonts)
		//@ check if image is in container if not try to load from disk and add to container
		bool				AddFont(sysutils::C_HashName fontUID, sysutils::C_String fontFileName);

		//@ GetFont
		//@ hashID - uniqueID of font - manager will find correspondend path
		T_BitmapFont	GetFont(sysutils::C_HashName fontUID);

		//@ PreloadTexture TODFO predelat na GetTexture uniqueId
		bool				PreloadFonts();

	private:
		//@ c-tor
		C_BitmapFontManager() {};
		//@ d-tor
		~C_BitmapFontManager() {};

		typedef std::vector<std::pair<sysutils::C_HashName, sysutils::C_String> >	T_RequestQueue;
		T_RequestQueue						m_requestQueue;


		typedef std::map<sysutils::C_HashName, T_BitmapFont>	T_FontContainer;
		T_FontContainer					m_container;	//kontainer pre vsetky textury
	};


	typedef C_Singleton<C_BitmapFontManager>	T_BitmapFontManager;

}}

SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::syscore::C_BitmapFontManager>;