/*
* filename:			C_Scene.cpp
*
* author:			Kral Jozef
* date:				01/09/2011		16:13
* version:			1.00
* brief:
*/


#include "C_Scene.h"
#include <SysCore/Core/C_RenderManager.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetVisible
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Scene::SetVisible(bool bVisible)
	{
		T_RenderManager::GetInstance().SetVisible(bVisible, m_RenderLayer);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Add
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Scene::Add(T_Frame frame)
	{
		T_FrameList childList;
		frame->GetAllChildren(childList);

		m_frames.insert(T_Frames::value_type(frame->GetHashName(), frame));
		frame->OnAddToScene(this);
		T_RenderManager::GetInstance().Add(frame, m_RenderLayer);
		for (T_FrameList::iterator iter = childList.begin(); iter != childList.end(); ++iter)
		{
			T_RenderManager::GetInstance().Add(*iter, m_RenderLayer);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Scene::Remove(T_Frame frame)
	{
		T_Frames::iterator iter = m_frames.find(frame->GetHashName());
		if (iter != m_frames.end())
		{
			bool bRes = T_RenderManager::GetInstance().Remove(iter->second);
			

			m_frames.erase(iter);

			//@ remove all children from render manager!
			T_FrameList children;
			frame->GetAllChildren(children);
			for (T_FrameList::iterator iterChild = children.begin(); iterChild != children.end(); ++iterChild)
				bRes &= T_RenderManager::GetInstance().Remove(*iterChild);

			SC_ASSERT(bRes);

			frame->OnRemoveFromScene(this);
			return true;
		}
		
		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Scene::Remove(T_Hash32 frameId)
	{
		T_Frames::iterator iter = m_frames.find(frameId);
		if (iter != m_frames.end())
		{
			bool bRes = T_RenderManager::GetInstance().Remove(iter->second);
			SC_ASSERT(bRes);

			iter->second->OnRemoveFromScene(this);
			m_frames.erase(iter);
			return true;
		}

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Scene::Clear()
	{
		for (T_Frames::iterator iter = m_frames.begin(); iter != m_frames.end(); ++iter)
		{
			bool bRes = T_RenderManager::GetInstance().Remove(iter->second);
			SC_ASSERT(bRes);
			iter->second->OnRemoveFromScene(this);
		}
		m_frames.clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Find
	//
	//////////////////////////////////////////////////////////////////////////
	T_Frame C_Scene::FindFrame(T_Hash32 frameHash)
	{
		T_Frames::iterator iter = m_frames.find(frameHash);
		if (iter != m_frames.end())
		{
			return iter->second;
		}

		return T_Frame();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ EnumFrames
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Scene::EnumFrames(I_SceneVisitor & visitor)
	{
		for (T_Frames::iterator iter = m_frames.begin(); iter != m_frames.end(); ++iter)
		{
			T_Frame frame = iter->second;
			visitor.Visit(frame);
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ FindFrames
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Scene::FindFrames(T_Hash32 frameHash, T_FrameList & outFrameList)
	{
		T_Frames::iterator iter = m_frames.find(frameHash);
		if (iter != m_frames.end())
		{
			T_Frames::iterator iterEnd = m_frames.upper_bound(frameHash);
			for (T_Frames::iterator iter__ = iter; iter__ != iterEnd; ++iter__)
			{
				outFrameList.push_back(iter__->second);
			}
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SortFrames
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Scene::SortFrames()
	{
		T_RenderManager::GetInstance().SortLayer(m_RenderLayer);
	}

}}