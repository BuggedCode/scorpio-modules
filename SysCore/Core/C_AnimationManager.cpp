/*
* filename:			C_AnimationManager.cpp
*
* author:			Kral Jozef
* date:				11/07/2010		17:42
* version:			1.00
* brief:
*/

#include "C_AnimationManager.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/Xml/C_XmlFile.h>
#include <SysCore/Core/Animation/C_AnimationSet.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;

	static const C_String C_STR_ANIMATION_SET_LIST = "AnimationSet_list";

	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadAnimationSet
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimationManager::LoadAnimationFileList(const T_Stream inputStream)
	{
		//parse xml and load/insert fonts into manager
		C_XmlFile file;
		bool bRes = file.Load(inputStream);
		if (!bRes)
			return false;

		//parse xml
		if (!file.IsValid())
			return false;

		const C_XmlNode * root = file.GetRoot();
		if (!root || !root->IsValid())
			return false;

		if (root->GetName() != C_STR_ANIMATION_SET_LIST)
			return false;

		C_XmlNode::const_child_iterator it_begin = root->BeginChild("AnimationSet");
		C_XmlNode::const_child_iterator it_end	= root->EndChild("AnimationSet");

		//@ spracuju vsechny templaty v subore
		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			const C_XmlAttribute * fileAtt = tempNode.GetAttribute("file");
			if (!fileAtt)
			{
				TRACE_FE("Some animation has not set file attribute in file '%s'!", inputStream->GetFileName());
				continue;
			}

			C_String fileName = fileAtt->GetValue();

			AddSetIntoQueue(fileName);
		}

		LoadAllAnimationSetFromQueue();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddSetIntoQueue
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AnimationManager::AddSetIntoQueue(C_String animationSetFileName)
	{
		m_requestQueue.push_back(animationSetFileName);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadAnimation
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimationManager::LoadAnimation(const sysutils::T_Stream inputStream)
	{
		if (!inputStream)
			return false;

		C_AnimationSet animSet;
		bool bRes = animSet.Load(inputStream);
		if (!bRes)
		{
			TRACE_FE("Can not load AnimationSet file: '%s'", inputStream->GetFileName());
			return false;
		}


		//@ from animation set we move animation track to manager
		const C_AnimationSet::T_AnimationTrackList & trackList = animSet.GetAnimationTrackList();
		for (C_AnimationSet::T_AnimationTrackList::const_iterator iterTrack = trackList.begin(); iterTrack != trackList.end(); ++iterTrack)
		{
			C_HashName hashtrackName((*iterTrack)->GetTrackName());
			T_AnimationContainer::iterator iter__ = m_container.find(hashtrackName);
			if (iter__ == m_container.end())
			{
				m_container.insert(T_AnimationContainer::value_type(hashtrackName, *iterTrack));
				TRACE_FI("Inserting AnimationTrack '%s' into AnimationManager", (*iterTrack)->GetTrackName().c_str());
			}
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ PreloadAnimations
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimationManager::LoadAllAnimationSetFromQueue()
	{
		bool bRes = true;
		for (T_RequestQueue::iterator iter = m_requestQueue.begin(); iter != m_requestQueue.end(); ++iter)
		{
			T_Stream stream = T_FileSystemManager::GetInstance().Open(*iter);
			bRes &= LoadAnimation(stream);
		}

		m_requestQueue.clear();
		return bRes;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetAnimation
	//
	//////////////////////////////////////////////////////////////////////////
	T_AnimationTrack C_AnimationManager::GetAnimation(C_HashName animUID)
	{
		T_AnimationContainer::iterator iter = m_container.find(animUID);
		if (iter != m_container.end())
		{
			return iter->second;
		}

		//TRACE_FE("Can not find AnimationTrack with TrackName: %s", animUID.GetName());
		return T_AnimationTrack();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadAnimations
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimationManager::LoadAnimations(const sysutils::T_XmlNodeList animNodeList)
	{
		SC_ASSERT(animNodeList);
		for (T_ConstXmlNodePtrVector::const_iterator iter = animNodeList->begin(); iter != animNodeList->end(); ++iter)
		{
			const C_XmlNode * animNode = *iter;
			SC_ASSERT(animNode);
			if (!animNode || !animNode->IsValid())
			{
				TRACE_E("INVALID AnimationSet node in animNodeList.");
				continue;
			}

			C_String fileName = (*animNode)["file"];

			AddSetIntoQueue(fileName);
		}

		LoadAllAnimationSetFromQueue();

		return true;
	}

}}