/*
* filename:			C_RenderManager.h
*
* author:			Kral Jozef
* date:				01/10/2010		21:06
* version:			1.00
* brief:				Singleton for Render Object
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include "I_RenderObject.h"
#include <vector>
#include <map>
#include <Common/C_Singleton.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_RenderManager
	class SYSCORE_API C_RenderManager
	{
		friend class C_Singleton<C_RenderManager>;
	public:
		//@ Render
		void		Render();

		//@ Add - add object to render manager
		//@ layerID - id of layer, render from lowest to highest layer, sense like priority
		void		Add(T_RenderObject renderObject, T_RenderLayer layerId);
		void		Add(T_RenderObjectList & roList, T_RenderLayer layerId);

		//@ Remove
		//@ slow operation!
		bool		Remove(T_RenderObject renderObject);
		//@ Remove
		bool		Remove(T_RenderObject renderObject, T_RenderLayer layerId);


		//@ ClearExpiredObjects - remove expired objects
		void		ClearExpiredObjects();

		//@ SetVisible
		void		SetVisible(bool bVisible, T_RenderLayer layerId);

		//@ SortLayer
		void		SortLayer(T_RenderLayer layerId);
		//@ ClearLayer
		void		ClearLayer(T_RenderLayer layerId);

#ifndef MASTER
		void		EnableDebugDraw(bool bEnable) { m_bDDEnabled = bEnable; }
		bool		IsDebugDrawEnabled() { return m_bDDEnabled; }
#endif


	private:
		//@ c-tor
		C_RenderManager();
		//@ copy c-tor
		C_RenderManager(const C_RenderManager & copy);	//not implemented
		//@ assignment operator
		C_RenderManager & operator=(const C_RenderManager & copy);	//not implemented
		//@ d-tor
		~C_RenderManager();

		//@ attributes
		typedef std::vector<T_WeakRenderObject>	T_WeakRenderObjectList;
		struct S_LayerInfo
		{
			//@ c-tor
			S_LayerInfo() : m_Visible(true) {};
			bool							m_Visible;		//visibility of layer
			T_WeakRenderObjectList	m_roContainer;	//render objects
		};

		typedef std::map<T_RenderLayer, S_LayerInfo>	T_LayerContainer;
		T_LayerContainer				m_Layers;

#ifndef MASTER
		bool								m_bDDEnabled;
#endif
	};

	typedef C_Singleton<C_RenderManager>	T_RenderManager;
}}

SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::syscore::C_RenderManager>;