/*
* filename:			C_RenderManager.cpp
*
* author:			Kral Jozef
* date:				01/10/2010		21:10
* version:			1.00
* brief:
*/

#include "C_RenderManager.h"
#include <algorithm>
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Helpers/RenderUtils.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_RenderManager::C_RenderManager()
	{
#ifndef MASTER
		m_bDDEnabled = false;
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	C_RenderManager::~C_RenderManager()
	{
		ClearExpiredObjects();
#ifdef _DEBUG
		bool bEmpty = true;
		for (T_LayerContainer::iterator iter = m_Layers.begin(); iter != m_Layers.end(); ++iter)
		{
			T_WeakRenderObjectList & roList = iter->second.m_roContainer;
			bEmpty &= roList.empty();
		}
		SC_ASSERT(bEmpty);
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetVisible
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RenderManager::SetVisible(bool bVisible, T_RenderLayer layerId)
	{
		T_LayerContainer::iterator iter = m_Layers.find(layerId);
		if (iter != m_Layers.end())
		{
			S_LayerInfo & layerInfo = iter->second;
			layerInfo.m_Visible = bVisible;
			return;
		}

		TRACE_FW("Unknown Render LayerId: %d", layerId);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RenderManager::Render()
	{
		ClearExpiredObjects();

		for (T_LayerContainer::iterator iter = m_Layers.begin(); iter != m_Layers.end(); ++iter)
		{
			S_LayerInfo & layerInfo = iter->second;
			if (!layerInfo.m_Visible)
				continue;

			T_WeakRenderObjectList & roList = layerInfo.m_roContainer;
			for (T_WeakRenderObjectList::iterator iter_ = roList.begin(); iter_ != roList.end(); ++iter_)
			{
				T_WeakRenderObject roWeak = *iter_;
				SC_ASSERT(roWeak.expired() == false);
				T_RenderObject ro(roWeak.lock());
				if (ro->IsVisible())
					ro->Render();
			}
		}


#ifndef MASTER
		if (m_bDDEnabled)
		{
			for (T_LayerContainer::iterator iter = m_Layers.begin(); iter != m_Layers.end(); ++iter)
			{
				S_LayerInfo & layerInfo = iter->second;
				if (!layerInfo.m_Visible)
					continue;

				T_WeakRenderObjectList & roList = layerInfo.m_roContainer;
				for (T_WeakRenderObjectList::iterator iter_ = roList.begin(); iter_ != roList.end(); ++iter_)
				{
					T_WeakRenderObject roWeak = *iter_;
					SC_ASSERT(roWeak.expired() == false);
					T_RenderObject ro(roWeak.lock());
					if (ro->IsVisible())
						ro->DebugDraw();
				}
			}
		}
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ClearExpiredObjects
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RenderManager::ClearExpiredObjects()
	{
		for (T_LayerContainer::iterator iter = m_Layers.begin(); iter != m_Layers.end(); ++iter)
		{
			T_WeakRenderObjectList & roList = iter->second.m_roContainer;
			size_t size = roList.size();
			size_t actId = 0;
			for (size_t i = 0; i < size; ++i)
			{
				T_WeakRenderObject	ro = roList[i];
				if (!ro.expired())
				{
					roList[actId] = ro;
					++actId;
				}
			}
			roList.resize(actId);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Add
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RenderManager::Add(T_RenderObject renderObject, T_RenderLayer layerId)
	{
		S_LayerInfo & layerInfo = m_Layers[layerId];
		layerInfo.m_roContainer.push_back(T_WeakRenderObject(renderObject));
	}

	void C_RenderManager::Add(T_RenderObjectList & roList, T_RenderLayer layerId)
	{
		if (roList.empty())
			return;

		S_LayerInfo & layerInfo = m_Layers[layerId];
		for (T_RenderObjectList::iterator iter = roList.begin(); iter != roList.end(); ++iter)
		{
			layerInfo.m_roContainer.push_back(T_WeakRenderObject(*iter));
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_RenderManager::Remove(T_RenderObject renderObject)
	{
		//////////////////////////////////////////////////////////////////////////
		class C_Deleter
		{
		public:
			//@ c-tor
			C_Deleter(const T_RenderObject & renderObject)
				: m_renderObject(renderObject) {};

			//@ operator func
			bool operator()(const T_WeakRenderObject & weakRenderObject)
			{
				return (m_renderObject.get() == weakRenderObject.lock().get());
			}

		private:
			const T_RenderObject & m_renderObject;
		};
		//////////////////////////////////////////////////////////////////////////

		C_Deleter	deleter(renderObject);
		typedef T_LayerContainer::iterator T_Iter;
		for (T_Iter iter = m_Layers.begin(); iter != m_Layers.end(); ++iter)
		{
			T_WeakRenderObjectList & roList = iter->second.m_roContainer;
			T_WeakRenderObjectList::iterator iter__ = std::find_if(roList.begin(), roList.end(), deleter);
			if (iter__ != roList.end())
			{
				roList.erase(iter__);
				return true;			//predpokladany vyskyt max 1x
			}
		}

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_RenderManager::Remove(T_RenderObject renderObject, T_RenderLayer layerId)
	{
		//////////////////////////////////////////////////////////////////////////
		class C_Deleter
		{
		public:
			//@ c-tor
			C_Deleter(const T_RenderObject & renderObject)
				: m_renderObject(renderObject) {};

			//@ operator func
			bool operator()(const T_WeakRenderObject & weakRenderObject)
			{
				return (m_renderObject.get() == weakRenderObject.lock().get());
			}

		private:
			const T_RenderObject & m_renderObject;
		};
		//////////////////////////////////////////////////////////////////////////

		C_Deleter	deleter(renderObject);
		T_LayerContainer::iterator iter = m_Layers.find(layerId);
		if (iter == m_Layers.end())
		{
			TRACE_FE("Can not find Render layer: %d", layerId);
			return false;
		}

		T_WeakRenderObjectList & roList = iter->second.m_roContainer;
		T_WeakRenderObjectList::iterator iter__ = std::find_if(roList.begin(), roList.end(), deleter);
		if (iter__ != roList.end())
		{
			roList.erase(iter__);
			return true;			//predpokladany vyskyt max 1x
		}

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SortLayer
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RenderManager::SortLayer(T_RenderLayer layerId)
	{
		ClearExpiredObjects();

		T_LayerContainer::iterator iter = m_Layers.find(layerId);
		if (iter != m_Layers.end())
		{
			T_WeakRenderObjectList & roList = iter->second.m_roContainer;

			C_RenderObjectSorter roSorter;
			std::sort(roList.begin(), roList.end(), roSorter);
			return;
		}

		//TRACE_FW("Unknown Render LayerId: %d", layerId);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ClearLayer
	//
	//////////////////////////////////////////////////////////////////////////
	void C_RenderManager::ClearLayer(T_RenderLayer layerId)
	{
		T_LayerContainer::iterator iter = m_Layers.find(layerId);
		if (iter != m_Layers.end())
		{
			T_WeakRenderObjectList & roList = iter->second.m_roContainer;
			roList.clear();
			return;
		}

		TRACE_FW("Unknown Render LayerId: %d", layerId);
	}
}}
