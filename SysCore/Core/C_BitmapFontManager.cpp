/*
* filename:			C_BitmapFontManager.cpp
*
* author:			Kral Jozef
* date:				11/04/2010		21:52
* version:			1.00
* brief:
*/

#include "C_BitmapFontManager.h"
#include <SysUtils/C_TraceClient.h>
#include "C_TextureManager.h"
#include <SysUtils/Xml/C_XmlFile.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BitmapFontManager::Clear()
	{
		m_requestQueue.clear();
		m_container.clear();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadFonts
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_BitmapFontManager::LoadFonts(const T_Stream inputStream)
	{
		//parse xml and load/insert fonts into manager
		C_XmlFile file;
		bool bRes = file.Load(inputStream);
		if (!bRes)
			return false;

		//parse xml
		if (!file.IsValid())
			return false;

		const C_XmlNode * root = file.GetRoot();
		if (!root || !root->IsValid())
			return false;

		if (root->GetName() != "font_list")
			return false;

		C_XmlNode::const_child_iterator it_begin = root->BeginChild("font");
		C_XmlNode::const_child_iterator it_end	= root->EndChild("font");

		//@ spracuju vsechny templaty v subore
		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			const C_XmlAttribute * uidAtt = tempNode.GetAttribute("uid");
			if (!uidAtt)
			{
				TRACE_FE("Some font has not set UniqueID in file '%s'!", inputStream->GetFileName());
				continue;
			}

			C_String fontUID = uidAtt->GetValue();
			C_HashName fontUIDHash(fontUID);

			const C_XmlAttribute * fileAtt = tempNode.GetAttribute("file");
			if (!fileAtt)
			{
				TRACE_FE("Some font has not set file attribute in file '%s'!", inputStream->GetFileName());
				continue;
			}

			C_String fileName = fileAtt->GetValue();

			AddFont(fontUIDHash, fileName);			
		}

		PreloadFonts();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddFont
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_BitmapFontManager::AddFont(C_HashName fontUID, C_String fontFileName)
	{
		m_requestQueue.push_back(std::make_pair(fontUID, fontFileName));
		//@ ifdef debug check if same UID / same filepath!
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ PreloadFonts
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_BitmapFontManager::PreloadFonts()
	{
		bool bRes = true;
		for (T_RequestQueue::iterator iter = m_requestQueue.begin(); iter != m_requestQueue.end(); ++iter)
		{
			//@ E_TFM_NEAREST - nemusis se upravovat font, ale pri zmene pozicie artefakty - scroll
			sysrender::T_GraphicRes fontTexture = T_TextureManager::GetInstance().GetTexture(iter->second, sysrender::E_TWM_CLAMP, sysrender::E_TFM_LINEAR);
			T_BitmapFont font = T_BitmapFont(new C_BitmapFont(iter->second, fontTexture));
			if (!font)
			{
				TRACE_FE("Can not load Bitmap font : '%s' from disk", iter->second.c_str());
				bRes = false;
				continue;
			}

			m_container.insert(T_FontContainer::value_type(iter->first, font));
		}

		m_requestQueue.clear();

		return bRes;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetFont
	//
	//////////////////////////////////////////////////////////////////////////
	T_BitmapFont C_BitmapFontManager::GetFont(sysutils::C_HashName fontUID)
	{
		T_FontContainer::iterator iter = m_container.find(fontUID);
		if (iter != m_container.end())
		{
			return iter->second;
		}

		TRACE_FE("Can not find Bitmap font withID: %s", fontUID.GetName());
		return T_BitmapFont();
	}

}}