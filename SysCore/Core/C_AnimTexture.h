/*
* filename:			C_AnimTexture.h
*
* author:			Kral Jozef
* date:				06/14/2009		23:00
* version:			1.00
* brief:				Animated texture
*/

#pragma once

#include <SysUtils/C_HashName.h>
#include <SysUtils/Xml/C_XmlNode.h>
#include <boost/shared_ptr.hpp>
#include <SysCore/Render/C_Graphic.h>
#include <SysCore/Render/C_VertexBufferObject.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_AnimTexture
	class SYSCORE_API C_AnimTexture
	{
	public:
		//@ c-tor
		C_AnimTexture(const sysutils::C_HashName & animTexUID, sysrender::T_GraphicRes image);
		//@ d-tor
		~C_AnimTexture() {};

		//@ InitFromXmlNode
		bool					InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ Update
		//@ param inDeltaTime - difference between last called update
		void					Update(u32 inDeltaTime);
		//@ Draw
		void					Draw(sysrender::T_VertexBufferObject vertexBuffer, const sysmath::C_Vector2 & vctPos, const sysmath::C_Vector4 & color = sysmath::C_Vector4(), float fScale = 1.f);


		C_AnimTexture	&	operator=(const C_AnimTexture & srcTexture);

		//////////////////////////////////////////////////////////////////////////
		//@ SET/GET methodes
		const sysutils::C_HashName		&	GetUID() const { return m_uniqueID; }

		//@ GetWidth
		s32					GetWidth() const { return m_width; }
		//@ GetHeight
		s32					GetHeight() const { return m_height; }
		//@ GetFramesCountX
		s32					GetFramesCountX() const { return (s32)m_image->GetWidth() / m_width; }
		//@ GetFramesCountY
		s32					GetFramesCountY() const { return (s32)m_image->GetHeight() / m_height; }


	private:
		sysutils::C_HashName	m_uniqueID;		//uniqueID
		sysrender::T_GraphicRes				m_image;
		u32						m_interval;		//time interval between changing of frame in ms
		u32						m_width;			//X-size of 1 frame
		u32						m_height;		//Y-size of 1 frame

		u32						m_accDelta;			//delta t from last change of frame
		sysmath::C_Point		m_actFramePos;		//current frame coordinate to texture
		u16						m_FrameID;
		u16						m_AllParts;			//count of all frames in textture
	};


	typedef boost::shared_ptr<C_AnimTexture>	T_AnimImage;

}}