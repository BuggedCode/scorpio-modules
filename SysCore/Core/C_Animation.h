/*
* filename:			C_Animation.h
*
* author:			Kral Jozef
* date:				11/07/2010		17:48
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <boost/shared_ptr.hpp>
#include <SysUtils/C_String.h>
#include <SysMath/C_Vector2.h>
#include <SysCore/Core/Animation/I_AnimController.h>
#include <map>

namespace scorpio{ namespace syscore{

	enum E_ControllerType
	{
		E_ATT_UNKNOWN = 0,
		E_ATT_POS,
		E_ATT_ROT,
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_Animation
	class SYSCORE_API C_Animation
	{
	public:
		struct S_SampleData	//2D
		{
			//@ c-tor
			S_SampleData(const sysmath::C_Vector2 & m_vctPos, float fAngle, float fScale)
				: m_vctPos(m_vctPos), m_fAngle(fAngle), m_fScale(fScale) {};
			//@ c-tor
			S_SampleData(float fPosX, float fPosY, float fAngle, float fScale)
				: m_vctPos(fPosX, fPosY), m_fAngle(fAngle), m_fScale(fScale) {};

			sysmath::C_Vector2	m_vctPos;
			float						m_fAngle;
			float						m_fScale;
		};

	public:
		//@ c-tor
		C_Animation() {};
		//@ c-tor
		C_Animation(sysutils::C_String animFileName);
		//@ d-tor
		~C_Animation() {};

		//@ GetAnimationEndFrame
		u32							GetAnimationEndFrame() const { return (u32)m_trackData.size(); }
		//@ GetSample
		const S_SampleData	&	GetSampleData(u32 frameID) const { SC_ASSERT(frameID < m_trackData.size()); return m_trackData[frameID]; }

		//@ AddController
		void							AddController(E_ControllerType type, T_AnimController controller);


	private:
		typedef std::vector<S_SampleData>	T_TrackData;
		T_TrackData			m_trackData;
		E_ControllerType	m_trackType;
		u32					m_flags;	//flags which controllers are active
		std::map<int, T_AnimController>	m_controllers;
	};

	typedef boost::shared_ptr<C_Animation>		T_Animation;
}}