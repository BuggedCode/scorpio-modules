/*
* filename:			C_AnimTextureManager.cpp
*
* author:			Kral Jozef
* date:				06/15/2009		19:54
* version:			1.00
* brief:				Manager of animated textures
*/

#include "C_AnimTextureManager.h"
#include <SysUtils/Xml/C_XmlFile.h>
#include <SysUtils/C_TraceClient.h>
#include "C_TextureManager.h"
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace sysutils;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_AnimTextureManager::C_AnimTextureManager()
	{
	}


	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	//////////////////////////////////////////////////////////////////////////
	C_AnimTextureManager::~C_AnimTextureManager()
	{
		m_container.clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//@ InitFromDefXml
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimTextureManager::InitFromDefXml(const T_Stream inputStream)
	{
		C_XmlFile file;
		bool bRes = file.Load(inputStream);
		if (!bRes)
			return false;

		//parse xml
		if (!file.IsValid())
			return false;

		const C_XmlNode * root = file.GetRoot();
		if (!root || !root->IsValid())
			return false;

		if (root->GetName() != "anim_textures_definition")
			return false;

		C_XmlNode::const_child_iterator it_begin = root->BeginChild("anim_texture");
		C_XmlNode::const_child_iterator it_end	= root->EndChild("anim_texture");

		//@ spracuju vsechny dialog infa v souboru
		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			const C_XmlAttribute * imgAtt = tempNode.GetAttribute("img");
			if (!imgAtt)
				continue;

			const C_String tmpImgFilePath = imgAtt->GetValue();

			sysrender::T_GraphicRes animTexture = T_TextureManager::GetInstance().GetTexture(tmpImgFilePath);
			if (!animTexture)
			{
				TRACE_FE("Can not create animated texture '%s'", tmpImgFilePath.c_str());
				continue;
			}

			const C_XmlAttribute * animTexUIDAtt = tempNode.GetAttribute("uid");
			if (!animTexUIDAtt)
			{
				TRACE_FE("Animtexture: '%s' has not defined uniqueID!", tmpImgFilePath.c_str());
				continue;
			}

			C_String animTexUID = animTexUIDAtt->GetValue();
			C_HashName animTexUIDhash(animTexUID);
			T_AnimImage animImage = T_AnimImage(new C_AnimTexture(animTexUIDhash, animTexture));

			//load anim texture properties
			bool bResInit = animImage->InitFromXmlNode(tempNode);
			if (bResInit)
			{
				typedef T_AnimTextureMap::iterator T_AnimTexIterator;
				std::pair<T_AnimTexIterator, bool> result = m_container.insert(T_AnimTextureMap::value_type(animImage->GetUID(), animImage));
				if (!result.second)
				{
					TRACE_FE("Can not add AnimTexture with UID: '%s' from xml to container, probably other AnimTexture with same UID is in already!", animTexUID.c_str());
				}
			}
			else
			{
				TRACE_FE("Can not initialize animTexture with UID: '%s'", animTexUID.c_str());
			}
		}
		return bRes;
	}



	//////////////////////////////////////////////////////////////////////////
	//@ GetTexture
	//////////////////////////////////////////////////////////////////////////
	T_AnimImage C_AnimTextureManager::GetAnimTexture(const sysutils::C_HashName & animTexUID)
	{
		//@ hladam uz v naladovanych texturach
		T_AnimTextureMap::iterator iter = m_container.find(animTexUID);
		if (iter != m_container.end())
			return iter->second;

		return T_AnimImage();
	}

}}