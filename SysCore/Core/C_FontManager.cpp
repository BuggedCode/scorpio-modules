/*
* filename:			C_FontManager.cpp
*
* author:			Kral Jozef
* date:				09/17/2010		23:12
* version:			1.00
* brief:
*/

#include "C_FontManager.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddFont
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FontManager::AddFont(sysutils::C_HashName & fontUID, sysutils::C_String fontFileName, u32 fontSize)
	{
		m_requestQueue.push_back(S_FontInfo(fontUID, fontFileName, fontSize));
		//@ ifdef debug check if same UID / same filepath!
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ PreloadFonts
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FontManager::PreloadFonts()
	{
		bool bRes = true;
		
		for (T_RequestQueue::iterator iter = m_requestQueue.begin(); iter != m_requestQueue.end(); ++iter)
		{
			T_Stream stream = T_FileSystemManager::GetInstance().Open(iter->m_FileName);

			T_TrueText font;
			if (stream)
				font = T_TrueText(new sysrender::C_TrueText(stream, iter->m_FontSize));
			if (!font)
			{
				TRACE_FE("Can not open font : '%s'", iter->m_FileName.c_str());
				bRes = false;
				continue;
			}

			m_container.insert(T_FontContainer::value_type(iter->m_HashName, font));
		}

		m_requestQueue.clear();

		return bRes;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetFont
	//
	//////////////////////////////////////////////////////////////////////////
	T_TrueText C_FontManager::GetFont(sysutils::C_HashName & fontUID)
	{
		T_FontContainer::iterator iter = m_container.find(fontUID);
		if (iter != m_container.end())
		{
			return iter->second;
		}

		TRACE_FE("Can not find font withID: %s", fontUID.GetName());
		return T_TrueText();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FontManager::Clear()
	{
		m_requestQueue.clear();
		m_container.clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReloadFonts
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FontManager::ReleaseInternalResources()
	{
		bool bRes = true;
		for (T_FontContainer::iterator iter = m_container.begin(); iter != m_container.end(); ++iter)
		{
			bRes &= iter->second->ReleaseReources();
		}
		return bRes;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FontManager::RestoreInternalResources()
	{
		bool bRes = true;
		for (T_FontContainer::iterator iter = m_container.begin(); iter != m_container.end(); ++iter)
		{
			bRes &= iter->second->RestoreReources();
		}
		return bRes;
	}

}}