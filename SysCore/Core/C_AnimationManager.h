/*
* filename:			C_AnimationManager.h
*
* author:			Kral Jozef
* date:				11/07/2010		17:41
* version:			1.00
* brief:				Simple manager of animations
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <Common/C_Singleton.h>
#include <SysCore/Core/Animation/C_AnimationTrack.h>
#include <SysUtils/C_HashName.h>
#include <SysUtils/FileSystem/C_Stream.h>
#include <vector>
#include <map>


namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Animationmanager
	class SYSCORE_API C_AnimationManager
	{
		friend class C_Singleton<C_AnimationManager>;

	public:
		//@ LoadFonts - physically load AnimationSets and insert AnimationTracks from all sets into manager
		bool					LoadAnimationFileList(const sysutils::T_Stream inputStream);

		//@ LoadAnimation - load animation file into manager
		bool					LoadAnimation(const sysutils::T_Stream inputStream);
		//@ LoadAnimations
		bool					LoadAnimations(const sysutils::T_XmlNodeList animNodeList);

		//@ AddAnimation
		//@ animationFileName - relative path from data directory in native Win32 format
		//@ adds fonts to manager/ but not load from disk(just request - need call after PreloadFonts)
		//@ check if image is in container if not try to load from disk and add to container
		//bool					AddAnimationSet(sysutils::C_HashName animationUID, sysutils::C_String animationFileName);
		
		//@ AddSetIntoQueue
		//@ animationSetFileName - relative Path from Data dir
		void						AddSetIntoQueue(sysutils::C_String animationSetFileName);

		//@ LoadAllAnimationSetFromQueue - preload of all sets in queue, 
		bool						LoadAllAnimationSetFromQueue();


		//@ GetAnimation
		//@ hashID - uniqueID of animation - manager will find correspondend path
		T_AnimationTrack		GetAnimation(sysutils::C_HashName animUID);

	private:
		//@ c-tor
		C_AnimationManager() {};
		//@ d-tor
		~C_AnimationManager() {};

		typedef std::vector<sysutils::C_String>	T_RequestQueue;
		T_RequestQueue						m_requestQueue;


		typedef std::map<sysutils::C_HashName, T_AnimationTrack>	T_AnimationContainer;
		T_AnimationContainer				m_container;	//container for all animations
	};


	typedef C_Singleton<C_AnimationManager>	T_AnimationManager;

}}

SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::syscore::C_AnimationManager>;