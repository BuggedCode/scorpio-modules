/*
* filename:			C_TextureManager.h
*
* author:			Kral Jozef
* date:				05/28/2009		20:44
* version:			1.00
* brief:				Singleton manager for textures
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <Common/C_Singleton.h>
#include <map>
#include <SysUtils/C_String.h>
#include <SysCore/Render/C_Graphic.h>
#include <SysUtils/C_HashName.h>

namespace scorpio{ namespace syscore{

	typedef sysutils::C_HashName	T_TextureKey;	//hashed texture filePath
	typedef std::map<T_TextureKey, sysrender::T_GraphicRes>	T_TextureMap;

	typedef std::map<sysutils::C_HashName, T_TextureKey>	T_UniqueIDToKeyTable;

	//////////////////////////////////////////////////////////////////////////
	//@ C_TextureManager
	class SYSCORE_API C_TextureManager
	{
		friend class C_Singleton<C_TextureManager>;
	public:
		//@ Clear
		void								Clear();

		//@ GetTexture
		//@ texFileName - relative path from data directory in native Win32 format
		//@ texture manager convert name to actual native platform fileName
		//@ check if image is in container if not try to load from disk and add to container
		sysrender::T_GraphicRes		GetTexture(sysutils::C_String texFileName, sysrender::E_TextureWrapMode texWrapMode = sysrender::E_TWM_CLAMP, sysrender::E_TextureFilteringMode texFilteringMode = sysrender::E_TFM_LINEAR);

		//@ GetTexture
		//@ hashID - uniqueID of texture - manager will find correspondend path
		sysrender::T_GraphicRes		GetTexture(sysutils::C_HashName texUID);

		//@ PreloadTexture
		bool				Preloadtexture(sysutils::C_HashName texUID, sysutils::C_String texFileName, sysrender::E_TextureWrapMode texWrapMode = sysrender::E_TWM_CLAMP);

		//@ ReleaseInternalResources
		bool				ReleaseInternalResources();
		//@ RestoreInternalResources
		bool				RestoreInternalResources();

	private:
		//@ c-tor
		C_TextureManager() {};
		//@ c-tor
		//C_TextureManager(sysutils::C_String dataDir);
		//@ d-tor
		~C_TextureManager();


		T_TextureMap						m_container;	//kontainer pre vsetky textury
		T_UniqueIDToKeyTable				m_conversionTable;	//table of conversion uniqueID to key into textureMap
	};

	typedef C_Singleton<C_TextureManager>	T_TextureManager;
}}

SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::syscore::C_TextureManager>;
