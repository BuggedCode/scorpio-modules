/*
* filename:			C_AnimTexture.cpp
*
* author:			Kral Jozef
* date:				06/14/2009		23:00
* version:			1.00
* brief:				Animated texture
*/

#include "C_AnimTexture.h"
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>

namespace scorpio{ namespace syscore{

	using namespace sysutils;
	using namespace sysmath;
	using namespace sysrender;


	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_AnimTexture::C_AnimTexture(const sysutils::C_HashName & animTexUID, sysrender::T_GraphicRes image)
		: m_uniqueID(animTexUID), m_image(image), m_width(0), m_height(0), m_interval(1), m_accDelta(0), m_FrameID(0), m_AllParts(0)
	{
		//
	}


	//////////////////////////////////////////////////////////////////////////
	//@ InitFromXml
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimTexture::InitFromXmlNode(const sysutils::C_XmlNode & node)
	{
		if (!node.IsValid())
			return false;

		//@ WIDTH & HEIGHT
		const C_XmlNode * tempNode = node.GetNode("width");
		if (!tempNode || !tempNode->IsValid())
		{
			TRACE_E("Missing or corrupted 'width' node");
			return false;
		}
		if (!tempNode->Read(m_width))
		{
			TRACE_E("Can not read value of 'width' node");
			return false;
		}

		tempNode = node.GetNode("height");
		if (!tempNode || !tempNode->IsValid())
		{
			TRACE_E("Missing or corrupted 'height' node");
			return false;
		}
		if (!tempNode->Read(m_height))
		{
			TRACE_E("Can not read value of 'height' node");
			return false;
		}

		//@ INTERVAL
		tempNode = node.GetNode("interval");
		if (!tempNode || !tempNode->IsValid())
		{
			TRACE_E("Missing or corrupted 'interval' node");
			return false;
		}
		if (!tempNode->Read(m_interval))
		{
			TRACE_E("Can not read value of 'interval' node");
			return false;
		}

		//check for compact of frames in texture
		SC_ASSERT(((u32)m_image->GetWidth() % m_width) == 0);
		SC_ASSERT(((u32)m_image->GetHeight() % m_height) == 0);

		m_AllParts = (m_image->GetWidth() / m_width) * (m_image->GetHeight() / m_height);
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Update
	//////////////////////////////////////////////////////////////////////////
	void C_AnimTexture::Update(u32 inDeltaTime)
	{
		if ((m_accDelta + inDeltaTime) > m_interval)
		{
			m_actFramePos.x += m_width;	//posuvam sa po X dalej
			if (m_actFramePos.x == m_image->GetWidth())
			{
				//posuvam sa nizsie
				m_actFramePos.x = 0;
				m_actFramePos.y += m_height;
				if (m_actFramePos.y == m_image->GetHeight())
					m_actFramePos.y = 0;
			}
			m_accDelta = (m_accDelta + inDeltaTime) - m_interval;
			//@ JK pokud raz prislo deltaTime velmi velke tak by akumulator bol stale velky i pod odpocitani intervalu 
			//@ pre 1 snimok, dochadzalo by ku zmene framu kazdy tick az pokial by akumulator neklesom pod hodnotu intervalu
			//@ vynulujeme akumulator a setneme mu biasnuty/quantizovany zvysok 
			m_accDelta = m_accDelta % m_interval;

			++m_FrameID;
			if (m_FrameID == m_AllParts)
				m_FrameID = 0;
		}
		else
			m_accDelta += inDeltaTime;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Draw
	//////////////////////////////////////////////////////////////////////////
	void C_AnimTexture::Draw(T_VertexBufferObject vertexBuffer, const C_Vector2 & vctPos, const C_Vector4 & color, float fScale)
	{
		SC_ASSERT(m_image);
		if (m_image)
		{
			C_Vector2 texCoordX;
			texCoordX.m_x = RemapValue((float)m_actFramePos.x, 0.f, m_image->GetWidth(), 0.f, 1.f);
			texCoordX.m_y = RemapValue((float)(m_actFramePos.x+m_width), 0.f, m_image->GetWidth(), 0.f, 1.f);

			C_Vector2 texCoordY;
			texCoordY.m_x = RemapValue((float)m_actFramePos.y, 0.f, m_image->GetHeight(), 0.f, 1.f);
			texCoordY.m_y = RemapValue((float)(m_actFramePos.y+m_height), 0.f, m_image->GetHeight(), 0.f, 1.f);

			m_image->Render(vctPos, C_Point(m_width, m_height), texCoordX, texCoordY, color, 0.f, fScale);
			//m_image->RenderVBOElements(vertexBuffer, m_FrameID, vctPos, 0.f, fScale, color);
		}
	}

}}