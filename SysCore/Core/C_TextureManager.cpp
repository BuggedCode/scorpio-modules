/*
* filename:			C_TextureManager.cpp
*
* author:			Kral Jozef
* date:				05/28/2009		20:44
* version:			1.00
* brief:				Singleton manager for textures
*/

#include "C_TextureManager.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysCore/Render/C_Graphic.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	//////////////////////////////////////////////////////////////////////////
	C_TextureManager::~C_TextureManager()
	{
		m_container.clear();
		m_conversionTable.clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_TextureManager::Clear()
	{
		m_container.clear();
		m_conversionTable.clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetTexture
	//
	//////////////////////////////////////////////////////////////////////////
	sysrender::T_GraphicRes C_TextureManager::GetTexture(C_String texFileName, sysrender::E_TextureWrapMode texWrapMode, sysrender::E_TextureFilteringMode texFilteringMode)
	{
		C_HashName hashName(texFileName);

		//@ hladam uz v naladovanych texturach
		T_TextureMap::iterator iter = m_container.find(hashName);
		if (iter != m_container.end())
			return iter->second;



		//@ vytvaram image
		sysrender::T_GraphicRes newTexture = sysrender::T_GraphicRes(new sysrender::C_Graphic());
		if (!newTexture)
		{
			TRACE_FE("Can not create image: '%s'", texFileName.c_str());
			return newTexture;	//vracim null ImageRef
		}

		//@ loadujem image
		T_Stream stream = T_FileSystemManager::GetInstance().Open(texFileName);
		bool bRes = newTexture->LoadTexture(stream, texWrapMode, texFilteringMode, texFileName);

		if (!bRes)
		{
			TRACE_FE("Can not load image : '%s' from disk", texFileName.c_str());
			return sysrender::T_GraphicRes();	//return null image;
		}

		m_container.insert(T_TextureMap::value_type(hashName, newTexture));
		return newTexture;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetTexture
	//
	//////////////////////////////////////////////////////////////////////////
	sysrender::T_GraphicRes C_TextureManager::GetTexture(sysutils::C_HashName texUID)
	{
		T_UniqueIDToKeyTable::iterator iter = m_conversionTable.find(texUID);
		if (iter != m_conversionTable.end())
		{
			//@ hladam uz v naladovanych texturach
			T_TextureMap::iterator iter_ = m_container.find(iter->second);
			if (iter_ != m_container.end())
				return iter_->second;

			SC_ASSERT(false);	//id je v konverzni tabulce ale nie v zozname textur "velmi zle"
		}

		TRACE_FE("Can not find texture withID: %s", texUID.GetName());
		return sysrender::T_GraphicRes();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ PreloadTexture
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_TextureManager::Preloadtexture(sysutils::C_HashName texUID, sysutils::C_String texFileName, sysrender::E_TextureWrapMode texWrapMode /*= sysrender::E_TWM_CLAMP*/)
	{
		sysrender::T_GraphicRes img = GetTexture(texFileName, texWrapMode);
		if (img)
		{
			typedef std::pair<T_UniqueIDToKeyTable::iterator, bool>	T_ResType;
			T_ResType res = m_conversionTable.insert(T_UniqueIDToKeyTable::value_type(texUID, C_HashName(texFileName)));
			if (!res.second)
			{
				TRACE_FW("Unique texture ID conflict! Overwrite! UniqueID: %s with new filePath:", texUID.GetName(), texFileName.c_str());
				return true;
			}
			return true;
		}

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_TextureManager::ReleaseInternalResources()
	{
		bool bRes = true;
		for (T_TextureMap::iterator iter = m_container.begin(); iter != m_container.end(); ++iter)
			bRes &= iter->second->ReleaseResources();

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_TextureManager::RestoreInternalResources()
	{
		bool bRes = true;
		for (T_TextureMap::iterator iter = m_container.begin(); iter != m_container.end(); ++iter)
		{
			C_String relFileName = iter->second->GetFileName().GetName();
			SC_ASSERT(!relFileName.empty());

			T_Stream stream = T_FileSystemManager::GetInstance().Open(relFileName);
			if (!stream)
			{
				bRes = false;
				TRACE_FE("Invalid texture stream: %s when Reloading textures!", relFileName.c_str());
				continue;
			}

			bRes &= iter->second->Reload(stream);
		}
		
		return bRes;
	}

}}