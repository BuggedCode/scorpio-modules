/*
* filename:			C_Scene.h
*
* author:			Kral Jozef
* date:				01/09/2011		16:08
* version:			1.00
* brief:				Scene of frames- visual/texts/...
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <map>
#include <SysCore/Frames/C_Frame.h>
#include "I_SceneVisitor.h"

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Scene
	class SYSCORE_API C_Scene
	{
	public:
		//@ c-tor
		//@ renderLayer - every scene works on 1 render layer - for better hiding/showing objects in render
		//@ renderLayer should be defined in GameFramework or higher (GlobalId.h)
		C_Scene(u32 renderLayer)
			: m_RenderLayer(renderLayer) {};
		//@ d-tor
		virtual ~C_Scene() {};


		//@ Add
		void			Add(T_Frame frame);
		//@ Remove
		//@ retVal = TRUE - if frame was removed / FALSE when frame was not in container
		bool			Remove(T_Frame frame);
		//@ Remove
		bool			Remove(T_Hash32 frameId);
		//@ Clear
		void			Clear();
		//@ Find
		//@ search just topmost frames, from container, do not search through children!
		T_Frame		FindFrame(T_Hash32 frameHash);
		//@ EnumFrames
		void			EnumFrames(I_SceneVisitor & visitor);
		//@ FindFrames
		//@ search just topmost frames, from container, do not search through children!
		void			FindFrames(T_Hash32 frameHash, T_FrameList & outFrameList);
		//@ SetVisible
		void			SetVisible(bool bVisible);
		//@ SortFrames
		//@ sort frames by Z in render layer!
		void			SortFrames();
		//@ GetRenderlayer
		u32			GetRenderLayer() const { return m_RenderLayer; }


	private:
		//@ TODO - hashTable
		u32			m_RenderLayer;
		typedef std::multimap<T_Hash32, T_Frame>	T_Frames;
		T_Frames		m_frames;
	};
	

}}
