/*
* filename:			C_SysCore.h
*
* author:			Kral Jozef
* date:				01/23/2010		16:53
* version:			1.00
* brief:				Initialisation part for module SysCore
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysUtils/C_String.h>


namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_SysCore
	class SYSCORE_API C_SysCore
	{
	public:
		//@ Initialize module
		static bool			Initialize();

		//@ Deinitialize module
		static bool			Deinitialize();
	};

}}