/*
* filename:			C_Mesh2d.h
*
* author:			Kral Jozef
* date:				01/04/2011		1:31
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Vector.h>
#include <SysUtils/C_String.h>
#include <SysMath/C_AABB2.h>
#include <boost/shared_ptr.hpp>
#include <SysMath/C_Vector4.h>
#include <SysUtils/C_TraceClient.h>

namespace scorpio{
	namespace sysutils{
		class C_XmlNode;
	};
};

namespace scorpio{ namespace syscore{ 

	//////////////////////////////////////////////////////////////////////////
	//@ C_Mesh2d
	class SYSCORE_API C_Mesh2d
	{
	public:
		//@ c-tor
		C_Mesh2d(sysutils::C_String nodeName)
			: m_nodeName(nodeName), m_vertsCount(0), m_verticies(NULL), m_fCoordZ(0.f) {};

		//@ d-tor
		virtual ~C_Mesh2d();

		//@ GetName
		sysutils::C_String				GetName() const { return m_nodeName; }
		//@ SetPivot
		void									SetPivot(const sysmath::C_Vector2 & vctPivot) { m_pivot = vctPivot; }
		//@ GetPivot
		const sysmath::C_Vector2	&	GetPivot() const { return m_pivot; }
		//@ SetRotation
		void									SetRotation(float fAngleZ) { m_AngleZ = fAngleZ; }
		//@ GetRotation
		float									GetRotation() const { return m_AngleZ; }

		//@ SetFaces
		//void								SetFaces(u32 numFaces, Face * faces);
		//@ SetVerticiess
		void									SetVerticies(u32 numVerts, sysmath::C_Vector2 * verts);
		//@ SetVertexColors
		void									SetVertexColors(u32 numVerts, sysmath::C_Vector3 * vertCols);
		//@ SetTexCoords
		void									SetTexCoords(u32 numTexCoords, sysmath::C_Vector2 * texCoords);
		//@ SetDiffuseTextureName
		void									SetDiffuseTextureName(sysutils::C_String texName) { m_diffuseTexName0 = texName; }
		//@ SetDiffuseTextureName
		//@ 
		void									SetDiffuseTextureName(u8 textureId, sysutils::C_String texName) 
		{
			if (textureId == 1)
			{
				m_diffuseTexName1 = texName;
				return;
			}

			if (textureId != 0)
				TRACE_FW("Not supported textureId: %d", textureId);

			m_diffuseTexName0 = texName;
		}
		//@ GetDiffuseTextureName
		sysutils::C_String				GetDiffuseTextureName() const { return m_diffuseTexName0; }
		//@ GetDiffuseTextureName
		sysutils::C_String				GetDiffuseTextureName(u8 textureId) const 
		{ 
			if (textureId == 1)
				return m_diffuseTexName1; 

			if (textureId != 0)
				TRACE_FW("Not supported textureId: %d", textureId);

			return m_diffuseTexName0;
		}
		//@ SetColor
		void									SetColor(const sysmath::C_Vector4 & vctColor);
		//@ GetColor
		const sysmath::C_Vector4	&	GetColor() const { return m_color; }

		//@ Save
		void									Save(sysutils::C_XmlNode & parentNode) const;
		//@ InitializeFromNode
		virtual bool						InitializeFromNode(const sysutils::C_XmlNode & ctrlInterpNode);
		
		//@ GetAABB
		const sysmath::C_AABB2	&		GetAABB() const { return m_aabox; }
		//@ SetZCoord
		void									SetZCoord(float fCoordZ) { m_fCoordZ = fCoordZ; }
		//@ GetZCoord
		float									GetZCoord() const { return m_fCoordZ; }
		//@ GetTexCoords
		const sysmath::C_AABB2	&		GetTexCoords() const { return m_TexCoord; }

	private:
		sysutils::C_String		m_nodeName;
		sysmath::C_Vector2		m_pivot;
		float							m_AngleZ;		//rotation around Z
		sysmath::C_Vector4		m_color;
		sysutils::C_String		m_diffuseTexName0;	//first diff map
		sysutils::C_String		m_diffuseTexName1;	//second diff map
		sysmath::C_AABB2			m_aabox;	//for now just AABB!

		u32							m_vertsCount;
		sysmath::C_Vector2	*	m_verticies;
		float							m_fCoordZ;

		sysmath::C_AABB2			m_TexCoord;	//rectangle texture coordinates
	};

	DECLARE_SHARED_PTR(C_Mesh2d, T_Mesh2d);
	typedef std::vector<T_Mesh2d>				T_MeshList;

}}