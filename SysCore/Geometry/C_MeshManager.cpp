/*
* filename:			C_MeshManager.cpp
*
* author:			Kral Jozef
* date:				01/04/2011		1:43
* version:			1.00
* brief:
*/

#include "C_MeshManager.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/Xml/C_XmlFile.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace sysutils;
	using namespace sysmath;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadMeshSet
	//
	//////////////////////////////////////////////////////////////////////////
	/*bool C_MeshManager::LoadMeshSet(sysutils::C_String meshsetFile, std::vector<T_Mesh2d> * outMeshList)
	{
		//parse xml and load/insert fonts into manager
		C_XmlFile file;
		bool bRes = file.LoadXml(meshsetFile);
		if (!bRes)
			return false;

		//parse xml
		if (!file.IsValid())
			return false;

		const C_XmlNode * root = file.GetRoot();
		if (!root || !root->IsValid())
			return false;

		if (root->GetName() != "Scorpio_Geometry_Data")
			return false;


		const C_XmlNode * planesNode = root->GetNode("Planes");
		
		if (!planesNode || !planesNode->IsValid())
		{
			TRACE_FE("missing 'Planes' node in mes definition file: %s", meshsetFile);
			return false;
		}

		C_XmlNode::const_child_iterator it_begin = planesNode->BeginChild("Mesh2d");
		C_XmlNode::const_child_iterator it_end	= planesNode->EndChild("Mesh2d");

		for (it_begin; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			const C_XmlAttribute * uNameAtt = tempNode.GetAttribute("Name");
			if (!uNameAtt)
			{
				TRACE_FE("Some Mesh has not set UniqueID Name in file '%s'!", meshsetFile);
				continue;
			}

			C_String meshName = uNameAtt->GetValue();
			C_HashName meshUIDHash(meshName);

			//if mesh is not in manager  - laod into manager
			if (!ContainsMesh(meshUIDHash))
			{
				T_Mesh2d mesh = T_Mesh2d(new C_Mesh2d(meshName));
				bool bRes = mesh->InitializeFromNode(tempNode);
				if (!bRes)
				{
					TRACE_FE("Failed to initialize mesh %s", meshName);
					continue;
				}

				AddMesh(meshUIDHash, mesh);

				//@ Insert Mesh into output MeshList
				if (outMeshList)
					outMeshList->push_back(mesh);
			}
			else if (outMeshList)
			{
				//@ Insert Mesh into output MeshList
				T_Mesh2d tmpMesh = GetMesh(meshUIDHash);
				outMeshList->push_back(tmpMesh);
			}
		}

		return true;
	}*/


	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddMesh
	//
	//////////////////////////////////////////////////////////////////////////
	void C_MeshManager::AddMesh(C_HashName meshUID, T_Mesh2d mesh)
	{
		std::pair<T_MeshContainer::iterator, bool> bRes = m_container.insert(T_MeshContainer::value_type(meshUID.GetHash(), mesh));
		SC_ASSERT(bRes.second);

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ContainsMesh
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_MeshManager::ContainsMesh(C_HashName meshUID) const
	{
		T_MeshContainer::const_iterator iter = m_container.find(meshUID);
		return (iter != m_container.end());
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetMesh
	//
	//////////////////////////////////////////////////////////////////////////
	T_Mesh2d C_MeshManager::GetMesh(C_HashName meshUID)
	{
		T_MeshContainer::iterator iter = m_container.find(meshUID);
		if (iter != m_container.end())
		{
			return iter->second;
		}

		TRACE_FE("Can not find mesh: %s", meshUID.GetName());
		return T_Mesh2d();
	}

	
	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_MeshManager::Clear()
	{
		m_container.clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	void C_MeshManager::Remove(const std::vector<T_Mesh2d> & meshList)
	{
		for (std::vector<T_Mesh2d>::const_iterator iter = meshList.begin(); iter != meshList.end(); ++iter)
		{
			C_HashName meshUIDHash((*iter)->GetName());
			size_t ret =  m_container.erase(meshUIDHash.GetHash());
			SC_ASSERT(ret != 0);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_MeshManager::Remove(sysutils::C_HashName meshUID)
	{
		size_t ret = m_container.erase(meshUID.GetHash());
		return (ret != 0);
	}

}}