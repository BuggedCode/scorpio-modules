/*
* filename:			C_Mesh2d.cpp
*
* author:			Kral Jozef
* date:				01/04/2011		1:37
* version:			1.00
* brief:
*/

#include "C_Mesh2d.h"
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/C_MountConfig.h>
#include <Common/macros.h>
//#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{ 

	using namespace sysutils;
	using namespace sysmath;
	using namespace syscore;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Mesh2d::~C_Mesh2d()
	{
		delete[] m_verticies;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetColor
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Mesh2d::SetColor(const sysmath::C_Vector4 & vctColor)
	{
		//@ clamp va;lue to 0-1 color, coz later remaping to u8 (clamping)
		m_color.m_x = ClampValue(vctColor.m_x, 0.f, 1.f);
		m_color.m_y = ClampValue(vctColor.m_y, 0.f, 1.f);
		m_color.m_z = ClampValue(vctColor.m_z, 0.f, 1.f);
		m_color.m_w = ClampValue(vctColor.m_w, 0.f, 1.f);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetVerticies
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Mesh2d::SetVerticies(u32 numVerts, sysmath::C_Vector2 * verts)
	{
		m_verticies = new C_Vector2[numVerts];
		MEM_CPY(m_verticies, sizeof(C_Vector2) * numVerts, verts, sizeof(C_Vector2) * numVerts);
		for (u32 u = 0; u < numVerts; ++u, ++verts)
		{
			//@ JK dont transform LOCAL coord! C_MathHelper::TransformToEngine(*verts);
			m_aabox.Include(*verts);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetVertexColors
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Mesh2d::SetVertexColors(u32 numVerts, sysmath::C_Vector3 * vertCols)
	{
		if (numVerts % 2 != 0)
			return;

	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetTexCoords
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Mesh2d::SetTexCoords(u32 numTexCoords, sysmath::C_Vector2 * texCoords)
	{
		m_TexCoord.Invalidate();

		if (numTexCoords != 4)
		{
			TRACE_FE("Mesh: %s has invalid Tex Coordinates", m_nodeName.c_str());
		}

		//@ find xMin/xMax
		for (u32 i = 0; i < numTexCoords; ++i)
			m_TexCoord.Include(texCoords[i]);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Save
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Mesh2d::Save(C_XmlNode & parentNode) const
	{
		C_XmlNode * node = parentNode.CreateNode("DiffuseColor");
		node->Write(m_color);

		//////////////////////////////////////////////////////////////////////////
		node = parentNode.CreateNode("DiffuseMap");
		//@ use relative path to texName
		T_String relPath = m_diffuseTexName0;
#ifdef USE_MOUNT_POINT
		relPath = T_MountConfig::GetInstance().RemapToRelative(m_diffuseTexName0);
#endif
		node->Write(relPath);

		//////////////////////////////////////////////////////////////////////////
		if (!m_diffuseTexName1.empty())
		{
			node = parentNode.CreateNode("DiffuseMap1");
			relPath = m_diffuseTexName1;
#ifdef USE_MOUNT_POINT
			relPath = T_MountConfig::GetInstance().RemapToRelative(m_diffuseTexName1);
#endif
			node->Write(relPath);
		}

		node = parentNode.CreateNode("Pivot");
		node->Write(m_pivot);
		node = parentNode.CreateNode("Rot");
		node->Write(m_AngleZ);

		node = parentNode.CreateNode("AABBMin");
		node->Write(m_aabox.GetMin());
		node = parentNode.CreateNode("AABBMax");
		node->Write(m_aabox.GetMax());

		if (!m_TexCoord.IsInvalid())
		{
			node = parentNode.CreateNode("TexCoordLT");
			node->Write(m_TexCoord.GetMin());
			node = parentNode.CreateNode("TexCoordRB");
			node->Write(m_TexCoord.GetMax());
		}

		node = parentNode.CreateNode("CoordZ");
		node->Write(m_fCoordZ);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitializeFromNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Mesh2d::InitializeFromNode(const C_XmlNode & meshNode)
	{
		if (!meshNode.IsValid())
			return false;

		//@ DIFFUSE COLOR
		const C_XmlNode * tmpNode = meshNode.GetNode("DiffuseColor");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Failed to load 'DiffuseColor' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}

		if (!tmpNode->Read(m_color))
		{
			TRACE_FE("Can not read from 'DiffuseColor' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}


		//@ DIFFUSE MAP
		tmpNode = meshNode.GetNode("DiffuseMap");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Failed to load 'DiffuseMap' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}

		if (!tmpNode->Read(m_diffuseTexName0))
		{
			TRACE_FE("Can not read from 'DiffuseMap' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}

		//@ DIFFUSE_MAP_1
		tmpNode = meshNode.GetNode("DiffuseMap1");
		if (tmpNode && tmpNode->IsValid())
		{
			if (!tmpNode->Read(m_diffuseTexName1))
			{
				TRACE_FE("Can not read from 'DiffuseMap1' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			}
		}


		//@ PIVOT
		tmpNode = meshNode.GetNode("Pivot");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Failed to load 'Pivot' in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}
		
		if (!tmpNode->Read(m_pivot))
		{
			TRACE_FE("Can not read from 'Pivot' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}

		//@ ROT
		tmpNode = meshNode.GetNode("Rot");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Failed to load 'Rot' in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}

		if (!tmpNode->Read(m_AngleZ))
		{
			TRACE_FE("Can not read from 'Rot' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}

		//@ AABB MIN
		tmpNode = meshNode.GetNode("AABBMin");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Failed to load 'AABBMin' in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}

		C_Vector2 vctMin;
		if (!tmpNode->Read(vctMin))
		{
			TRACE_FE("Can not read from 'AABBMin' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}
		m_aabox.Include(vctMin);

		//@ AABB MAX
		tmpNode = meshNode.GetNode("AABBMax");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Failed to load 'AABBMax' in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}

		C_Vector2 vctMax;
		if (!tmpNode->Read(vctMax))
		{
			TRACE_FE("Can not read from 'AABBMax' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}
		m_aabox.Include(vctMax);

		//@ TEX_COORD_LT
		tmpNode = meshNode.GetNode("TexCoordLT");
		C_Vector2 vctTexCoord;
		if (tmpNode)
		{
			if (!tmpNode->Read(vctTexCoord))
			{
				TRACE_FE("Can not read from 'TexCoordLT' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
				return false;
			}
			m_TexCoord.Include(vctTexCoord);
		}
		else
			m_TexCoord.Include(sysmath::C_Vector2::C_ONE);


		//@ TEX_COORD_RB
		tmpNode = meshNode.GetNode("TexCoordRB");
		if (tmpNode)
		{
			if (!tmpNode->Read(vctTexCoord))
			{
				TRACE_FE("Can not read from 'TexCoordRB' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
				return false;
			}
			m_TexCoord.Include(vctTexCoord);
		}
		else
			m_TexCoord.Include(sysmath::C_Vector2::C_ZERO);


		//@ COORDZ
		tmpNode = meshNode.GetNode("CoordZ");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Failed to load 'CoordZ' in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}


		if (!tmpNode->Read(m_fCoordZ))
		{
			TRACE_FE("Can not read from 'CoordZ' node in 'Mesh2d' %s node", meshNode.GetName().c_str());
			return false;
		}

		return true;
	}

}}