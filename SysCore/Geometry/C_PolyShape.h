/*
* filename:			C_PolyShape.h
*
* author:			Kral Jozef
* date:				12/22/2010		22:26
* version:			1.00
* brief:				2d shape - polygon
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysMath/C_Polygon.h>
#include <SysUtils/C_HashName.h>

namespace scorpio{
	namespace sysutils{
		class C_XmlNode;
	}
}

namespace scorpio{ namespace syscore{

	//# forward
	class C_PolyShape;

	DECLARE_SHARED_PTR(C_PolyShape,	T_PolyShape);

	//////////////////////////////////////////////////////////////////////////
	//@ C_PolyShape
	class SYSCORE_API C_PolyShape
	{
	public:
		//@ c-tor
		C_PolyShape(sysutils::C_String name)
			: m_Angle(0.f), m_Scale(0.f), m_name(name), m_HashName(name)
		{ 
			m_poly2d = sysmath::T_Polygon(new sysmath::C_Polygon()); 
		}

		//@ c-tor
		C_PolyShape(sysutils::C_String name, sysmath::T_Polygon polygon)
			: m_Angle(0.f), m_Scale(0.f), m_name(name), m_HashName(name), m_poly2d(polygon) {};
		
		//@ d-tor
		virtual ~C_PolyShape() {};
		//@ SetPosition
		void									SetPosition(const sysmath::C_Vector2 & vctPos) { m_pos = vctPos; }
		//@ just set pivot information - DO NOT TRANSFROM VETICIES OF POLYGON!
		void									SetTransformation(const sysmath::C_Vector2 & vctPos, float fAngle, float fScale);
		//@ Initialize
		bool									InitializeFromNode(const sysutils::C_XmlNode & parentNode);
		//@ Save
		void									Save(sysutils::C_XmlNode & parentNode) const;
		//@ GetName
		sysutils::C_String				GetName() const { return m_name; }
		//@ GetHashName
		const sysutils::C_HashName	&	GetHashName() const { return m_HashName; }
		//@ IsInside
		bool									IsInside(const sysmath::C_Vector2 & vctPos) const;
		//@ IsClosed
		bool									IsClosed() const { return m_poly2d->IsClosed(); }
		//@ GetPolygon
		const sysmath::T_Polygon		GetPolygon() const { return m_poly2d; }
		//@ GetPosition
		const sysmath::C_Vector2	&	GetPosition() const { return m_pos; }
		//@ GetRotation
		float									GetRotation() const { return m_Angle; }
		//@ GetScale
		float									GetScale() const { return m_Scale; }
		//@ Clone
		T_PolyShape							Clone() const;



	private:
		sysmath::C_Vector2	m_pos;	//TODO JK -> Replace by Transformation Matrix4
		float						m_Angle;	//TODO JK -> Replace by Transformation Matrix4
		float						m_Scale;	//TODO JK -> Replace by Transformation Matrix4
		sysutils::C_String	m_name;
		sysutils::C_HashName	m_HashName;
		sysmath::T_Polygon	m_poly2d;	//polygon is in Local transfromation!
	};


	typedef std::vector<T_PolyShape>		T_ShapeList;

}}