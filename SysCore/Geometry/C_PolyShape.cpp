/*
* filename:			C_PolyShape.cpp
*
* author:			Kral Jozef
* date:				12/22/2010		22:31
* version:			1.00
* brief:
*/

#include "C_PolyShape.h"
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetTransformation
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PolyShape::SetTransformation(const sysmath::C_Vector2 & vctPos, float fAngle, float fScale)
	{
		m_pos = vctPos;
		m_Angle = fAngle;
		m_Scale = fScale;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PolyShape::InitializeFromNode(const sysutils::C_XmlNode & parentNode)
	{
		const C_XmlNode * transformNode = parentNode.GetNode("Transform");
		if (!transformNode)
		{
			TRACE_FE("Can not load 'Transform' node form shape: %s", m_name.c_str());
			return false;
		}

		const C_XmlNode * tmpNode = transformNode->GetNode("Pos");
		tmpNode->Read(m_pos);
		tmpNode = transformNode->GetNode("Rot");
		tmpNode->Read(m_Angle);
		tmpNode = transformNode->GetNode("Scale");
		tmpNode->Read(m_Scale);
		tmpNode = transformNode->GetNode("Closed");
		bool bClosed;
		tmpNode->Read(bClosed);
		m_poly2d->SetClosed(bClosed);

		T_XmlNodeList vertexList = parentNode.SelectNodes("Vertex");
		if (vertexList)
		{
			for (T_ConstXmlNodePtrVector::iterator iter_ = vertexList->begin(); iter_ != vertexList->end(); ++iter_)
			{
				const C_XmlNode * vertexNode = *iter_;
				C_Vector2 vertexPos;
				vertexNode->Read(vertexPos);
				
				m_poly2d->Add(vertexPos);
			}
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Save
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PolyShape::Save(sysutils::C_XmlNode & parentNode) const
	{
		C_XmlNode * node = parentNode.CreateNode("Transform");
		C_XmlNode * tmpNode = node->CreateNode("Pos");
		tmpNode->Write(m_pos);
		tmpNode = node->CreateNode("Rot");
		tmpNode->Write(m_Angle);
		tmpNode = node->CreateNode("Scale");
		tmpNode->Write(m_Scale);
		tmpNode = node->CreateNode("Closed");
		tmpNode->Write(m_poly2d->IsClosed() ? true : false);

		for (u32 i = 0; i < m_poly2d->GetSize(); ++i)
		{
			node = parentNode.CreateNode("Vertex");
			node->Write((*m_poly2d)[i]);
		}
		
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clone
	//
	//////////////////////////////////////////////////////////////////////////
	T_PolyShape C_PolyShape::Clone() const
	{
		T_Polygon poly = sysmath::T_Polygon(new sysmath::C_Polygon());
		for (u32 i = 0; i < m_poly2d->GetSize(); ++i)
		{
			poly->Add((*m_poly2d)[i]);
		}
		poly->SetClosed(m_poly2d->IsClosed());

		C_String newName(m_name) ;
		newName += "_cloned";
		T_PolyShape newShape = T_PolyShape(new C_PolyShape(newName, poly));
		newShape->m_pos = m_pos;
		newShape->m_Angle = m_Angle;
		newShape->m_Scale = m_Scale;

		return newShape;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsInside
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PolyShape::IsInside(const C_Vector2 & vctPos) const
	{ 
		//transform input vctPos
		C_Vector2 vctLocalInPos(vctPos);
		vctLocalInPos -= m_pos;
		SC_ASSERT(isZero(m_Angle) && isZero(m_Scale - 1.f));
		return (m_poly2d->WhichSide(vctLocalInPos) == sysmath::E_SP_INSIDE); 
	}


}}