/*
* filename:			C_MeshManager.h
*
* author:			Kral Jozef
* date:				01/04/2011		1:43
* version:			1.00
* brief:
*/

#pragma once

#include <Common/C_Singleton.h>
#include "C_Mesh2d.h"
#include <SysUtils/C_HashName.h>
#include <map>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_MeshManager
	class SYSCORE_API C_MeshManager
	{
		friend class C_Singleton<C_MeshManager>;
	public:
		//@ LoadMeshSet
		//@ meshsetFile - full platform dependent path
		//@ outMeshList - if no NULL, output will contain all meshes from meshsetFile
		//bool						LoadMeshSet(sysutils::C_String meshsetFile, std::vector<T_Mesh2d> * outMeshList);
		//@ AddMesh
		void						AddMesh(sysutils::C_HashName meshUID, T_Mesh2d mesh);
		//@ GetMesh
		T_Mesh2d					GetMesh(sysutils::C_HashName meshUID);
		//@ ContainsMesh - check if mesh is in manager
		bool						ContainsMesh(sysutils::C_HashName meshUID) const;
		//@ Clear
		void						Clear();
		//@ Remove
		void						Remove(const std::vector<T_Mesh2d> & meshList);
		//@ Remove
		bool						Remove(sysutils::C_HashName meshUID);


	private:
		//@ c-tor
		C_MeshManager() {};
		//@ d-tor
		~C_MeshManager() {};


		typedef std::map<T_Hash32, T_Mesh2d>	T_MeshContainer;
		T_MeshContainer	m_container;
	};

	typedef C_Singleton<C_MeshManager>	T_MeshManager;
}}

SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::syscore::C_MeshManager>;