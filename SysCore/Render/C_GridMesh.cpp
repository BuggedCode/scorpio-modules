/*
* filename:			C_GridMesh.cpp
*
* author:			Kral Jozef
* date:				10/25/2011		0:28
* version:			1.00
* brief:
*/

#include "C_GridMesh.h"

#ifdef PLATFORM_WIN32
   #include <Windows.h>
   #include <gl/GL.h>
#elif defined PLATFORM_MACOSX
   #include <OpenGL/gl.h>
#endif

namespace scorpio{ namespace sysrender{

	using namespace scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_GridMesh::C_GridMesh(u32 nWidth, u32 nHeight, u8 xCells, u8 yCells)
		: m_bDebugDraw(false)
	{
		m_XCells = xCells;
		m_YCells = yCells;

		u8 xVecrticesNum = xCells + 1;
		u8 yVecrticesNum = yCells + 1;

		m_Size = xVecrticesNum * yVecrticesNum;

		m_Width = nWidth;
		m_Height = nHeight;

		m_Vertices = new C_Vector2[m_Size];
		m_TexCoords = new C_Vector2[m_Size];

		float xPos = 0, yPos = 0;
		float xCellSize = nWidth / (float)xCells;
		float yCellSize = nHeight / (float)yCells;

		for (u32 j = 0; j < xVecrticesNum; ++j)
		{
			for (u32 i = 0; i < yVecrticesNum; ++i)
			{
				m_Vertices[j * yVecrticesNum + i] = C_Vector2(xPos, yPos);
				m_TexCoords[j * yVecrticesNum + i] = C_Vector2(xPos / (float)m_Width, 1 - (yPos / (float)m_Height));

				xPos += xCellSize;
			}

			yPos += yCellSize;
			xPos = 0;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_GridMesh::~C_GridMesh()
	{
		delete[] m_Vertices;
		delete[] m_TexCoords;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_GridMesh::Render(T_Texture texture)
	{
		/*glPushAttrib(GL_COLOR_BUFFER_BIT | GL_ENABLE_BIT);
		glPushMatrix();
		glLoadIdentity();

		if (texture)
			texture->BindTexture();
		else
			glDisable(GL_TEXTURE_2D);

		glColor4f(1.f, 1.f, 1.f, 1.f);


		glBegin(GL_QUADS);

		for (u32 j = 0; j < m_YCells; ++j)
		{
			for (u32 i = 0; i < m_XCells; ++i)
			{
				//@ (m_YCells+1) -> pocet rohov a nie buniek!
				C_Vector2 & vctPosLT = m_Vertices[j * (m_YCells+1) + i];
				C_Vector2 & vctPosRT = m_Vertices[j * (m_YCells+1) + (i+1)];
				C_Vector2 & vctPosRB = m_Vertices[(j+1) * (m_YCells+1) + (i+1)];
				C_Vector2 & vctPosLB = m_Vertices[(j+1) * (m_YCells+1) + i];

				C_Vector2 & texCoordLT = m_TexCoords[j * (m_YCells+1) + i];
				C_Vector2 & texCoordRT = m_TexCoords[j * (m_YCells+1) + (i+1)];
				C_Vector2 & texCoordRB = m_TexCoords[(j+1) * (m_YCells+1) + (i+1)];
				C_Vector2 & texCoordLB = m_TexCoords[(j+1) * (m_YCells+1) + i];

				glTexCoord2f(texCoordLT.m_x, texCoordLT.m_y);
				glVertex2f(vctPosLT.m_x, vctPosLT.m_y);

				glTexCoord2f(texCoordLB.m_x, texCoordLB.m_y);
				glVertex2f(vctPosLB.m_x, vctPosLB.m_y);

				glTexCoord2f(texCoordRB.m_x, texCoordRB.m_y);
				glVertex2f(vctPosRB.m_x, vctPosRB.m_y);
		
				glTexCoord2f(texCoordRT.m_x, texCoordRT.m_y);
				glVertex2f(vctPosRT.m_x, vctPosRT.m_y);
			}
		}

		glEnd();

		if (m_bDebugDraw)
			DebugDraw();

		glPopMatrix();
		glPopAttrib();*/
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_GridMesh::DebugDraw()
	{
		glDisable(GL_TEXTURE_2D);
		glColor4f(1.f, 1.f, 1.f, .5f);

		glBegin(GL_LINES);
		for (u32 j = 0; j < m_YCells; ++j)
		{
			for (u32 i = 0; i < m_XCells; ++i)
			{
				//@ (m_YCells+1) -> pocet rohov a nie buniek!
				C_Vector2 & vctPosLT = m_Vertices[j * (m_YCells+1) + i];
				C_Vector2 & vctPosRT = m_Vertices[j * (m_YCells+1) + (i+1)];
				C_Vector2 & vctPosRB = m_Vertices[(j+1) * (m_YCells+1) + (i+1)];
				C_Vector2 & vctPosLB = m_Vertices[(j+1) * (m_YCells+1) + i];
				
				glVertex2f(vctPosLT.m_x, vctPosLT.m_y);
				glVertex2f(vctPosLB.m_x, vctPosLB.m_y);

				glVertex2f(vctPosLB.m_x, vctPosLB.m_y);
				glVertex2f(vctPosRB.m_x, vctPosRB.m_y);

				glVertex2f(vctPosRB.m_x, vctPosRB.m_y);
				glVertex2f(vctPosRT.m_x, vctPosRT.m_y);

				glVertex2f(vctPosRT.m_x, vctPosRT.m_y);
				glVertex2f(vctPosLT.m_x, vctPosLT.m_y);
			}
		}
		glEnd();

		glEnable(GL_TEXTURE_2D);
	}



}}
