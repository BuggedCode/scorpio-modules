/*
* filename:			C_Texture.h
*
* author:			Kral Jozef
* date:				02/21/2011		11:00
* version:			1.00
* brief:				LowLevel class for hodlding OpenGL Texture
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <boost/shared_ptr.hpp>
#include <SysUtils/C_HashName.h>
#include "RenderTypes.h"
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace sysrender{

	class C_Texture;

	typedef boost::shared_ptr<C_Texture>	T_Texture;

	//////////////////////////////////////////////////////////////////////////
	//@ C_Texture
	class SYSCORE_API C_Texture
	{
	public:
		//@ c-tor
		C_Texture();
		//@ d-tor
		~C_Texture();
		
		//@ Load
		bool					Load(const sysutils::T_Stream inputStream, E_TextureWrapMode texWrapMode, E_TextureFilteringMode texFilteringMode, sysutils::C_String relativeFileName);
		//@ Create
		static T_Texture	Create(u32 nWidth, u32 nHeight, u8 * pTextureData, E_TextureWrapMode texWrapMode, E_TextureFilteringMode texFilteringMode, E_TextureInternalFormat texInternalFormat);
		//@ ReleaseResources
		bool					ReleaseResources();
		//@ GetWidth
		u32					GetWidth() const { return m_Width; }
		//@ GetHeight
		u32					GetHeight() const { return m_Height; }
		//@ GetFileName
		const sysutils::C_HashName	&	GetFileName() const { return m_FileName; }
		//@ BindTexture
		void					BindTexture();
		//@ GetID
		u32					GetTextureID() const { return m_TextureID; }	//DON'T USE!! obnly in frame buffer
		//@ Reload - into videomemory
		bool					Reload(const sysutils::T_Stream inputStream);

	public:
		static sysutils::C_HashName	C_EMPTY_FILE_NAME;

	private:
		u32		m_Width;
		u32		m_Height;
		u32		m_Bpp;	//bytes per pixel
		u32		m_TextureID;
		//@ m_FileName - coz button need it for generating tex name for its variant (disabled/mouse over)
		sysutils::C_HashName		m_FileName;	//relative
		E_TextureWrapMode			m_WrapMode:8;			//need to store for reload
		E_TextureFilteringMode	m_FilteringMode:8;	//need to store for reload
	};

}}