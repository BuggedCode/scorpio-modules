/*
* filename:			C_Shader.cpp
*
* author:			Kral Jozef
* date:				11/6/2011		17:29
* version:			1.00
* brief:
*/

#include "C_Shader.h"

#ifdef PLATFORM_WIN32
	#ifdef RENDER_WIN_GLES_20
		#include <GLES2/gl2.h>
	#else
		#include <Windows.h>
		#include <gl/gl.h>
		#include <gl/glext.h>
		#include <SDL/include/SDL_video.h>
	#endif

	#define GL_HANDLE GLuint

#elif defined (PLATFORM_MACOSX)
   //#include <OpenGL/gl.h>
   //#include <OpenGL/glext.h>
   #include <SDL/SDL_opengl.h>
   #include <SDL/SDL_video.h>
   #define GL_HANDLE void */*GLhandleARB*/
#endif

#include <SysUtils/C_TraceClient.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace sysrender{

	static const u32 C_STR_BUFFER_SIZE = 4096;
	static const u32 C_SHADER_FILE_SIZE = 5120;	//5 KB buffer for shader

	using namespace scorpio::sysutils;

#ifdef RENDER_WIN_GLES_20
	
#else
	#ifndef GL_VERTEX_SHADER
		#define GL_VERTEX_SHADER GL_VERTEX_SHADER_ARB
	#endif
	#ifndef GL_FRAGMENT_SHADE
		#define GL_FRAGMENT_SHADE	GL_FRAGMENT_SHADE_ARB
	#endif
    
	PFNGLCREATESHADEROBJECTARBPROC glCreateShader = NULL;
	PFNGLCREATEPROGRAMOBJECTARBPROC glCreateProgram = NULL;
	PFNGLDELETEOBJECTARBPROC glDeleteObjectARB = NULL;
	PFNGLSHADERSOURCEARBPROC glShaderSource = NULL;
	PFNGLCOMPILESHADERARBPROC glCompileShaderARB = NULL;
	PFNGLATTACHOBJECTARBPROC glAttachObjectARB = NULL;
	PFNGLGETINFOLOGARBPROC glGetInfoLogARB = NULL;
	PFNGLLINKPROGRAMARBPROC glLinkProgram = NULL;
	PFNGLGETOBJECTPARAMETERIVARBPROC glGetObjectParameterivARB = NULL;
	PFNGLUSEPROGRAMOBJECTARBPROC glUseProgram = NULL;
	PFNGLGETUNIFORMLOCATIONARBPROC glGetUniformLocation = NULL;
	
	PFNGLUNIFORM1FARBPROC glUniform1f = NULL;
	PFNGLUNIFORM2FARBPROC glUniform2f = NULL;
	PFNGLUNIFORM3FARBPROC glUniform3f = NULL;

#define glDeleteShader	glDeleteObjectARB
#define glDeleteProgram	glDeleteObjectARB
#define glCompileShader	glCompileShaderARB
#define glGetShaderiv	glGetObjectParameterivARB
#define glGetProgramiv	glGetObjectParameterivARB
#define glGetShaderInfoLog	glGetInfoLogARB
#define glGetProgramInfoLog	glGetInfoLogARB
#define glAttachShader	glAttachObjectARB
#ifndef GL_COMPILE_STATUS
	#define GL_COMPILE_STATUS GL_OBJECT_COMPILE_STATUS_ARB
#endif
#ifndef GL_LINK_STATUS
	#define GL_LINK_STATUS	GL_OBJECT_LINK_STATUS_ARB
#endif
#endif


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Shader::InitializeExtensionFunctionPointers()
	{
#ifdef RENDER_WIN_GLES_20
#else
		glCreateShader = (PFNGLCREATESHADEROBJECTARBPROC)SDL_GL_GetProcAddress("glCreateShaderObjectARB");
		glCreateProgram = (PFNGLCREATEPROGRAMOBJECTARBPROC)SDL_GL_GetProcAddress("glCreateProgramObjectARB");
		glDeleteObjectARB = (PFNGLDELETEOBJECTARBPROC)SDL_GL_GetProcAddress("glDeleteObjectARB");
		glShaderSource = (PFNGLSHADERSOURCEARBPROC)SDL_GL_GetProcAddress("glShaderSourceARB");
		glCompileShaderARB = (PFNGLCOMPILESHADERARBPROC)SDL_GL_GetProcAddress("glCompileShaderARB");
		glAttachObjectARB = (PFNGLATTACHOBJECTARBPROC)SDL_GL_GetProcAddress("glAttachObjectARB");
		glGetInfoLogARB = (PFNGLGETINFOLOGARBPROC)SDL_GL_GetProcAddress("glGetInfoLogARB");
		glLinkProgram = (PFNGLLINKPROGRAMARBPROC)SDL_GL_GetProcAddress("glLinkProgramARB");
		glGetObjectParameterivARB = (PFNGLGETOBJECTPARAMETERIVARBPROC)SDL_GL_GetProcAddress("glGetObjectParameterivARB");
		glUseProgram = (PFNGLUSEPROGRAMOBJECTARBPROC)SDL_GL_GetProcAddress("glUseProgramObjectARB");
		glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONARBPROC)SDL_GL_GetProcAddress("glGetUniformLocationARB");
		
		glUniform1f = (PFNGLUNIFORM1FARBPROC)SDL_GL_GetProcAddress("glUniform1fARB");
		glUniform2f = (PFNGLUNIFORM2FARBPROC)SDL_GL_GetProcAddress("glUniform2fARB");
		glUniform3f = (PFNGLUNIFORM3FARBPROC)SDL_GL_GetProcAddress("glUniform3fARB");

		if (!glCreateShader || !glCreateProgram || !glDeleteObjectARB || !glShaderSource || !glCompileShaderARB || !glAttachObjectARB ||
			 !glLinkProgram || !glGetInfoLogARB || !glGetObjectParameterivARB || !glUseProgram || !glGetUniformLocation ||
			 !glUniform1f || !glUniform2f || !glUniform3f)
			 return false;
#endif

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Shader::C_Shader()
		: m_VertexShaderString(C_SHADER_FILE_SIZE), m_FragmentShaderString(C_SHADER_FILE_SIZE)
	{
		m_VertexShaderObj = glCreateShader(GL_VERTEX_SHADER);
		m_FragmentShaderObj = glCreateShader(GL_FRAGMENT_SHADER);
		m_ProgramObject = glCreateProgram();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Shader::~C_Shader()
	{
		if (m_ProgramObject)
			glDeleteProgram((GL_HANDLE)m_ProgramObject);
		if (m_VertexShaderObj)
			glDeleteShader((GL_HANDLE)m_VertexShaderObj);
		if (m_FragmentShaderObj)
			glDeleteShader((GL_HANDLE)m_FragmentShaderObj);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadShaderFile
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Shader::ParseShaderFile(T_Stream inputStream, C_String & vertexShaderStringOut, C_String & fragmentShaderStringOut)
	{
		const u32 C_BUFFER_SIZE = 1024;
		char buffer[C_BUFFER_SIZE];
		ZERO_ARRAY(buffer);

		C_String shaderText(10240);	//preallocated str to prevent allocations

		while(!inputStream->EndOfStream())
		{
			inputStream->ReadLine(buffer, C_BUFFER_SIZE);
			
			C_String buffStr(buffer);
			buffStr = buffStr.TrimLeft(' ');
			buffStr = buffStr.TrimLeft('\t');

			if (buffStr.length() > 1 && buffStr[(u32)0] == '/' && buffStr[(u32)1] == '/')
				continue;	//skip comments

			shaderText += buffer;
		}

		u32 vsId = shaderText.Find("[Vertex shader]");
		u32 fsId = shaderText.Find("[Fragment shader]");
		if (vsId == -1 || fsId == -1)
		{
			TRACE_E("Can not find [Vertex shader] / [Fragment shader] marker string");
			return false;
		}

		C_String declaration;
		if (vsId != 0)
			declaration = C_String::SubString(shaderText, 0, vsId-1);
		
		C_String vertexShaderSrc = C_String::SubString(shaderText, vsId + 15, fsId-1);	//15 - sizeof [Vertex shader]
		C_String fragmentShaderSrc = C_String::SubString(shaderText, fsId + 17, shaderText.length()-1);	//17 - sizeof [Fragment shader]	//-EOF

		vertexShaderStringOut = declaration;
		vertexShaderStringOut += vertexShaderSrc;
		fragmentShaderStringOut = declaration;
		fragmentShaderStringOut += fragmentShaderSrc;

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ BuildShader
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Shader::BuildShader(const C_String vertexShaderSrcString, const C_String fragmentShaderSrcString)
	{
		bool bRes = true;

		const char * vsChar = vertexShaderSrcString.c_str();
		const char * fsChar = fragmentShaderSrcString.c_str();

		glShaderSource(m_VertexShaderObj, 1, (const GLchar**)&vsChar, NULL);
		glShaderSource(m_FragmentShaderObj, 1, (const GLchar**)&fsChar, NULL);

		glCompileShader(m_VertexShaderObj);
		GLint bCompiled;
		glGetShaderiv(m_VertexShaderObj, GL_COMPILE_STATUS, &bCompiled);
		if (!bCompiled)
		{
			TRACE_E("Compile Error in VertexShader");
			char buffer[C_STR_BUFFER_SIZE];
			glGetShaderInfoLog(m_VertexShaderObj, C_STR_BUFFER_SIZE, NULL, buffer);
			TRACE_FE("%s", buffer);
			bRes = false;
		}

		glCompileShader(m_FragmentShaderObj);
		glGetShaderiv(m_FragmentShaderObj, GL_COMPILE_STATUS, &bCompiled);
		if (!bCompiled)
		{
			TRACE_E("Compile Error in FragmentShader");
			char buffer[C_STR_BUFFER_SIZE];
			glGetShaderInfoLog(m_FragmentShaderObj, C_STR_BUFFER_SIZE, NULL, buffer);
			TRACE_FE("%s", buffer);
			bRes = false;
		}

		//////////////////////////////////////////////////////////////////////////

		glAttachShader(m_ProgramObject, m_VertexShaderObj);
		glAttachShader(m_ProgramObject, m_FragmentShaderObj);

		glLinkProgram(m_ProgramObject);
		GLint bLinked = false;
		glGetProgramiv(m_ProgramObject, GL_LINK_STATUS, &bLinked);
		if (!bLinked)
		{
			TRACE_E("Linking Error in ShaderObject");
			char buffer[C_STR_BUFFER_SIZE];
			glGetProgramInfoLog(m_ProgramObject, C_STR_BUFFER_SIZE, NULL, buffer);
			TRACE_FE("%s", buffer);
			bRes = false;
		}

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Load
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Shader::Load(sysutils::T_Stream inputStream)
	{
		if (!inputStream)
			return false;

		bool bLoadRes = ParseShaderFile(inputStream, m_VertexShaderString, m_FragmentShaderString);
		if (!m_VertexShaderString || !m_FragmentShaderString || !bLoadRes)
			return false;

		bool bRes = BuildShader(m_VertexShaderString, m_FragmentShaderString);
		if (!bRes)
			TRACE_FE("Error in Shader file: %s", inputStream->GetFileName());

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ResleaseResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Shader::ReleaseResources()
	{
		glUseProgram(NULL);
		if (m_ProgramObject)
			glDeleteProgram(m_ProgramObject);
		if (m_VertexShaderObj)
			glDeleteShader(m_VertexShaderObj);
		if (m_FragmentShaderObj)
			glDeleteShader(m_FragmentShaderObj);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Shader::RestoreResources()
	{
		C_Shader::InitializeExtensionFunctionPointers();

		m_VertexShaderObj = glCreateShader(GL_VERTEX_SHADER);
		m_FragmentShaderObj = glCreateShader(GL_FRAGMENT_SHADER);
		m_ProgramObject = glCreateProgram();

		bool bRes = BuildShader(m_VertexShaderString, m_FragmentShaderString);
		if (!bRes)
			TRACE_E("Error in ShaderBuilding after Restore");

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Bind
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Shader::Bind()
	{
		glUseProgram(m_ProgramObject);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Unbind
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Shader::Unbind()
	{
		glUseProgram(NULL);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateVariable
	//
	//////////////////////////////////////////////////////////////////////////
	s32 C_Shader::CreateUniformVariable(const char * variableName)
	{
		s32 varHandle = glGetUniformLocation(m_ProgramObject, variableName);
		return varHandle;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ BindUniform
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Shader::BindUniform(s32 variableID, float value)
	{
		glUniform1f(variableID, value);
	}
	void C_Shader::BindUniform(s32 variableID, const sysmath::C_Vector2 & vct)
	{
		glUniform2f(variableID, vct.m_x, vct.m_y);
	}
	void C_Shader::BindUniform(s32 variableID, const sysmath::C_Vector & vct)
	{
		glUniform3f(variableID, vct.m_x, vct.m_y, vct.m_z);
	}
}}
