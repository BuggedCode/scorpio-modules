/*
* filename:			I_VertexBuffer.h
*
* author:			Kral Jozef
* date:				03/23/2011		14:55
* version:			1.00
* brief:
*/

#pragma once

#include <Common/types.h>

namespace scorpio{ namespace sysrender{

	enum E_VertexFormat
	{
		VF_UNDEFINED   = 0x00000000,
		VF_POSITION 	= 0x00000001,
		VF_RHW 			= 0x00000004,	//Because of Pyro
		VF_DIFFUSE 		= 0x00000008,
		VF_NORMAL 		= 0x00000010,
		VF_TEXTURE0  	= 0x00010000,
		//VF_TEXTURE1  	= 0x00020000,
		//VF_TEXTURE2 	= 0x00040000,
		//VF_TEXTURE3  	= 0x00080000
	};

	//////////////////////////////////////////////////////////////////////////
	//@ I_VertexBuffer
	class I_VertexBuffer
	{
	public:
		//I_VertexBuffer
		//@ GetVertexFormat
		virtual u32			GetVertexFormat() const = 0;
		//@ GetVertexSize - Stride
		virtual u32			GetVertexSize() const = 0;
		//@ GetData
		virtual u8		*	GetData() const = 0;
		//@ GetCount
		virtual u32			GetCount() const = 0;
	};
}}