/*
* filename:			C_Renderer.cpp
*
* author:			Kral Jozef
* date:				1/4/2012		18:25
* version:			1.00
* brief:
*/

#include "C_Renderer.h"
#include <SysCore/Managers/C_ShaderManager.h>
#include <SysCore/Managers/C_FrameBufferManager.h>
#include <SysCore/Managers/C_VertexBufferManager.h>
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Render/C_GlExtensions.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace sysrender{

	using namespace scorpio::sysutils;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Renderer::Initialize(u16 winWidth, u16 winHeight)
	{
		C_String ver = C_GlExtensions::GetVersion();
		TRACE_FI("#FF5599 OpenGL version: %s", ver.c_str());

		m_ViewportSize.x = winWidth;
		m_ViewportSize.y = winHeight;


		//@ VERTEX BUFFER MANAGER
		bool bRes = T_VertexBufferManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create VertexBufferManager Instance!");
			return false;
		}

		//@ InitializeExtensions
		bRes = T_VertexBufferManager::GetInstance().Initialize();
		if (!bRes)
		{
			TRACE_E("Couldn't initialize VertexBuferManager Instance!");
			return false;
		}


		//@ FRAME BUFFER INITIALIZATION
		bRes = T_FrameBufferManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Couldn't create FrameBufferManager Instance!");
			return false;
		}

		bRes = T_FrameBufferManager::GetInstance().Initialize(winWidth, winHeight);
		if (!bRes)
		{
			TRACE_E("Couldn't create FrameBufferManager Instance!");
			return false;
		}


		//@ SHADER MANAGER INITIALIZATION
		bRes = T_ShaderManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Couldn't create ShaderManager Instance!");
			return false;
		}

		bRes = T_ShaderManager::GetInstance().Initialize();
		if (!bRes)
		{
			TRACE_E("Couldn't initialize ShaderManager Instance!");
			return false;
		}


		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Deinitialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Renderer::Deinitialize()
	{
		T_VertexBufferManager::DestroyInstance();
		T_ShaderManager::DestroyInstance();
		T_FrameBufferManager::DestroyInstance();
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseInternalResource
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Renderer::ReleaseInternalResources()
	{
		T_ShaderManager::GetInstance().ReleaseInternalResources();
		T_FrameBufferManager::GetInstance().ReleaseInternalResources();
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Renderer::RestoreInternalResources()
	{
		T_ShaderManager::GetInstance().RestoreInternalResources();
		T_FrameBufferManager::GetInstance().RestoreInternalResources();
		return;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ StartCaptureFrameBuffer
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Renderer::StartCaptureFrameBuffer()
	{
		T_FrameBuffer fb = T_FrameBufferManager::GetInstance().GetRenderTarget(E_RT_MAIN);
		SC_ASSERT(fb);
		fb->StartCapture();
	}
	
	//////////////////////////////////////////////////////////////////////////
	//
	//@ EndCaptureFrameBuffer
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Renderer::EndCaptureFrameBuffer()
	{
		T_FrameBuffer fb = T_FrameBufferManager::GetInstance().GetRenderTarget(E_RT_MAIN);
		SC_ASSERT(fb);
		fb->EndCapture();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderFrameBuffer
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Renderer::RenderFrameBuffer()
	{
		T_FrameBuffer fb = T_FrameBufferManager::GetInstance().GetRenderTarget(E_RT_MAIN);
		SC_ASSERT(fb);
		fb->Render();
	}


}}