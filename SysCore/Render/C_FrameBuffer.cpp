/*
* filename:			C_FrameBuffer.cpp
*
* author:			Kral Jozef
* date:				10/17/2011		13:17
* version:			1.00
* brief:
*/

#include "C_FrameBuffer.h"

#ifdef PLATFORM_WIN32
	#ifdef RENDER_WIN_GLES_20
		#include <GLES2/gl2.h>
		//#include <GLES2/gl2ext.h>
	#else
		#include <Windows.h>
		#include <gl/gl.h>
		#include <gl/glext.h>
		#include <SDL/include/SDL_video.h>
	#endif
#elif defined (PLATFORM_MACOSX)
   //#include <OpenGL/gl.h>
   //#include <OpenGL/glext.h>
   #include <SDL/SDL_opengl.h>
	#include <SDL/SDL_video.h>
#endif

#include <SysMath/MathUtils.h>
#include <SysCore/Managers//C_ShaderManager.h>
#include <SysUtils/C_HashName.h>

namespace scorpio{ namespace sysrender{

	using namespace scorpio::sysmath;
	using namespace scorpio::sysutils;


#if !defined RENDER_WIN_GLES_20
	
#define GL_RENDERBUFFER					GL_RENDERBUFFER_EXT
#define GL_FRAMEBUFFER					GL_FRAMEBUFFER_EXT
#define GL_FRAMEBUFFER_COMPLETE		GL_FRAMEBUFFER_COMPLETE_EXT
#define GL_COLOR_ATTACHMENT0			GL_COLOR_ATTACHMENT0_EXT
#define GL_DEPTH_ATTACHMENT			GL_DEPTH_ATTACHMENT_EXT

	//PFNGLISRENDERBUFFEREXTPROC glIsRenderbuffer = NULL;
	PFNGLBINDRENDERBUFFEREXTPROC glBindRenderbuffer = NULL;
	PFNGLDELETERENDERBUFFERSEXTPROC glDeleteRenderbuffers = NULL;
	PFNGLGENRENDERBUFFERSEXTPROC glGenRenderbuffers = NULL;
	PFNGLRENDERBUFFERSTORAGEEXTPROC glRenderbufferStorage = NULL;
	PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC glGetRenderbufferParameteriv = NULL;
	PFNGLGENFRAMEBUFFERSEXTPROC glGenFramebuffers = NULL;
	PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC glCheckFramebufferStatus = NULL;
	PFNGLDELETEFRAMEBUFFERSEXTPROC glDeleteFramebuffers = NULL;
	PFNGLBINDFRAMEBUFFEREXTPROC glBindFramebuffer = NULL;
	PFNGLFRAMEBUFFERTEXTURE2DEXTPROC glFramebufferTexture2D = NULL;
	PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbuffer = NULL;
#endif

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FrameBuffer::InitializeExtensionFunctionPointers()
	{
#if !defined RENDER_WIN_GLES_20
		//glIsRenderbuffer = (PFNGLISRENDERBUFFEREXTPROC)SDL_GL_GetProcAddress("glIsRenderbufferEXT");
		glBindRenderbuffer = (PFNGLBINDRENDERBUFFEREXTPROC)SDL_GL_GetProcAddress("glBindRenderbufferEXT");
		glDeleteRenderbuffers = (PFNGLDELETERENDERBUFFERSEXTPROC)SDL_GL_GetProcAddress("glDeleteRenderbuffersEXT");
		glGenRenderbuffers = (PFNGLGENRENDERBUFFERSEXTPROC)SDL_GL_GetProcAddress("glGenRenderbuffersEXT");
		glRenderbufferStorage = (PFNGLRENDERBUFFERSTORAGEEXTPROC)SDL_GL_GetProcAddress("glRenderbufferStorageEXT");
		//glGetRenderbufferParameteriv = (PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC)SDL_GL_GetProcAddress("glGetRenderbufferParameterivEXT");
		//glIsFramebuffer = (PFNGLISFRAMEBUFFEREXTPROC)SDL_GL_GetProcAddress("glIsFramebufferEXT");
		glBindFramebuffer = (PFNGLBINDFRAMEBUFFEREXTPROC)SDL_GL_GetProcAddress("glBindFramebufferEXT");
		glDeleteFramebuffers = (PFNGLDELETEFRAMEBUFFERSEXTPROC)SDL_GL_GetProcAddress("glDeleteFramebuffersEXT");
		glGenFramebuffers = (PFNGLGENFRAMEBUFFERSEXTPROC)SDL_GL_GetProcAddress("glGenFramebuffersEXT");
		glCheckFramebufferStatus = (PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC)SDL_GL_GetProcAddress("glCheckFramebufferStatusEXT");
		//glFramebufferTexture1D = (PFNGLFRAMEBUFFERTEXTURE1DEXTPROC)SDL_GL_GetProcAddress("glFramebufferTexture1DEXT");
		glFramebufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DEXTPROC)SDL_GL_GetProcAddress("glFramebufferTexture2DEXT");
		//glFramebufferTexture3D = (PFNGLFRAMEBUFFERTEXTURE3DEXTPROC)SDL_GL_GetProcAddress("glFramebufferTexture3DEXT");
		glFramebufferRenderbuffer = (PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC)SDL_GL_GetProcAddress("glFramebufferRenderbufferEXT");
		//glGetFramebufferAttachmentParameteriv = (PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC)SDL_GL_GetProcAddress("glGetFramebufferAttachmentParameterivEXT");
		//glGenerateMipmap = (PFNGLGENERATEMIPMAPEXTPROC)SDL_GL_GetProcAddress("glGenerateMipmapEXT");

		if (/*!glIsRenderbuffer ||*/ !glBindRenderbuffer || !glDeleteRenderbuffers || !glGenRenderbuffers || !glRenderbufferStorage || 
			 !glBindFramebuffer || !glDeleteFramebuffers || !glGenFramebuffers || !glCheckFramebufferStatus || !glFramebufferTexture2D ||
			 !glFramebufferRenderbuffer)
			 return false;
#endif

		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FrameBuffer::Create(u32 width, u32 height)
	{
		m_WndWidth = width;
		m_WndHeight = height;
		m_Width = (float)next_p2(width);
		m_Height = (float)next_p2(height);

		glGenFramebuffers(1, &m_FrameBufferID);
		
		glGenRenderbuffers(1, &m_DepthRenderBufferID);
		glBindRenderbuffer(GL_RENDERBUFFER, m_DepthRenderBufferID);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, (u32)m_Width, (u32)m_Height);
		
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE)
		{
			return false;
		}
		
		m_FBOTexture = C_Texture::Create((u32)m_Width, (u32)m_Height, NULL, E_TWM_CLAMP, E_TFM_LINEAR, E_TIF_RGB);	// pixel format !! GL_BGR

		m_VertexBuffer = T_VertexBufferObject(new C_VertexBufferObject());

		C_Vector2 texCoordLT(0.f, 0.f);
		C_Vector2 texCoordRB(m_WndWidth / (float)m_Width, m_WndHeight / (float)m_Height);

		m_VertexBuffer->CreateFromRect(C_Vector2(m_WndWidth / 2.f, m_WndHeight / 2.f), C_Vector2((float)m_WndWidth, (float)m_WndHeight), 0.f, texCoordLT, texCoordRB);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Deinitialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FrameBuffer::Destroy()
	{
		if (m_FrameBufferID)
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glDeleteFramebuffers(1, &m_FrameBufferID);
			m_FrameBufferID = 0;			
		}

		if (m_DepthRenderBufferID)
		{
			glBindRenderbuffer(GL_RENDERBUFFER, 0);
			glDeleteRenderbuffers(1, &m_DepthRenderBufferID);
			m_DepthRenderBufferID = 0;
		}

		m_FBOTexture.reset();
		m_VertexBuffer.reset();

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FrameBuffer::ReleaseResources()
	{
		bool bRes = this->Destroy();
		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FrameBuffer::RestoreResources()
	{
		C_FrameBuffer::InitializeExtensionFunctionPointers();

		glGenFramebuffers(1, &m_FrameBufferID);

		glGenRenderbuffers(1, &m_DepthRenderBufferID);
		glBindRenderbuffer(GL_RENDERBUFFER, m_DepthRenderBufferID);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, (u32)m_Width, (u32)m_Height);

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE)
		{
			return false;
		}

		m_FBOTexture = C_Texture::Create((u32)m_Width, (u32)m_Height, NULL, E_TWM_CLAMP, E_TFM_LINEAR, E_TIF_RGB);	// pixel format !! GL_BGR

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitializeShaders
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FrameBuffer::InitializeShaders()
	{
		//@ called from InitDoneModule in SysInit coz not until now is shaders loaded!
		m_BasicShader = T_ShaderManager::GetInstance().GetShader(C_HashName("DEFAULT"));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ StartCapture
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FrameBuffer::StartCapture()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_FrameBufferID);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_FBOTexture->GetTextureID(), 0);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_DepthRenderBufferID);
		glClearColor(0.f, 0.f ,0.f, 1.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ EndCapture
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FrameBuffer::EndCapture()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//@ quad is rendered in		1------------2		1: uv(0,0)  xyz (0, Height)
	//									|            |		2: uv(1,0)  xyz (Width, Height)
	//									|            |
	//									|            |
	//									4------------3
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FrameBuffer::Render()
	{
		if (m_Effect && m_Effect->IsActive())
		{
			//m_Effect->Render(m_FBOTexture);
			//return;
			m_Effect->Bind();
		}
		else if (m_BasicShader)
			m_BasicShader->Bind();

		m_FBOTexture->BindTexture();
		m_VertexBuffer->Render();

		//////////////////////////////////////////////////////////////////////////
			/*static C_Vector2 wndTexCoord(m_WndWidth / (float)m_Width, m_WndHeight / (float)m_Height);
			glBegin(GL_QUADS);
			glColor3f(1.f, 0.f, 0.f);
			glTexCoord2f(0.f, 0.f);
			glVertex2f( 0, (float)m_WndHeight);

			glColor3f(0.f, 1.f, 0.f);
			glTexCoord2f(wndTexCoord.m_x, 0.f);
			glVertex2f( (float)m_WndWidth, (float)m_WndHeight);

			glColor3f(0.f, 0.f, 1.f);
			glTexCoord2f(wndTexCoord.m_x, wndTexCoord.m_y);
			glVertex2f( (float)m_WndWidth, 0);	//TODO cache substraction

			glColor3f(1.f, 1.f, 1.f);
			glTexCoord2f(0.f, wndTexCoord.m_y);
			glVertex2f( 0, 0);	//TODO cache substraction
			glEnd();*/
		//////////////////////////////////////////////////////////////////////////

		if (m_Effect && m_Effect->IsActive())
			m_Effect->Unbind();
		else if (m_BasicShader)
			m_BasicShader->Unbind();
	}
}}