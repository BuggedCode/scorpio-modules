/*
* filename:			C_Renderer.h
*
* author:			Kral Jozef
* date:				1/4/2012		18:25
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <Common/C_Singleton.h>
#include <SysCore/Managers/C_FrameBufferManager.h>
#include <SysMath/C_Point.h>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//@ C_REnderer
	class SYSCORE_API C_Renderer
	{
		friend class C_Singleton<C_Renderer>;
	public:

		//@ Initialize
		bool								Initialize(u16 winWidth, u16 winHeight);
		//@ Deinitialize
		bool								Deinitialize();
		//@ ReleaseInternalResource
		//@ call after recreting render context (fullscreen switch)
		void								ReleaseInternalResources();
		//@ RestoreInternalResources
		void								RestoreInternalResources();

		//@ StartCaptureFrameBuffer
		void								StartCaptureFrameBuffer();
		//@ EndCaptureFrameBuffer
		void								EndCaptureFrameBuffer();
		//@ RenderFrameBuffer
		void								RenderFrameBuffer();

		const sysmath::C_Point	&	GetViewportSize() const { return m_ViewportSize; }

	private:
		//@ c-tor
		C_Renderer() {};
		//@ d-tor
		~C_Renderer() {};


		sysmath::C_Point					m_ViewportSize;
	};


	typedef C_Singleton<C_Renderer>	T_Renderer;
}}


SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::sysrender::C_Renderer>;
