/*
* filename:			C_Vertex.h
*
* author:			Kral Jozef
* date:				1/10/2012		22:18
* version:			1.00
* brief:
*/

#pragma once

#include <SysMath/C_Vector.h>
#include <SysMath/C_Vector2.h>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Vertex
	class /*SYSCORE_API*/ C_Vertex
	{
	public:
		sysmath::C_Vector		m_Position;
		sysmath::C_Vector2	m_TexCoord;
		u8							m_Color[4];
		//@ normal
	};

}}
