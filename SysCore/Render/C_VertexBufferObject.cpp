/*
* filename:			C_VertexBufferObject.cpp
*
* author:			Kral Jozef
* date:				1/30/2012		18:35
* version:			1.00
* brief:
*/

#include "C_VertexBufferObject.h"
#include <Common/PlatformDef.h>

#ifdef PLATFORM_WIN32
	#ifdef RENDER_WIN_GLES_20
		#include <GLES2/gl2.h>
		#include <GLES2/gl2ext.h>
		#include <EGL/egl.h>
	#else
		#include <Windows.h>
		#include <gl/gl.h>
		#include <gl/glext.h>
		#include <SDL/include/SDL_video.h>
	#endif
#elif defined (PLATFORM_MACOSX)
	#include <SDL/SDL_opengl.h>
	#include <SDL/SDL_video.h>
#endif

#include <SysMath/C_Vector.h>
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Render/C_GlErrorCheck.h>
#include <SysMemManager/globalNewDelete.h>


namespace scorpio{ namespace sysrender{

	using namespace scorpio::sysmath;

	static const u8 C_SIZE_OF_FLOAT = sizeof(float);

#ifdef RENDER_WIN_GLES_20
	#define GL_WRITE_ONLY	GL_WRITE_ONLY_OES
	PFNGLMAPBUFFEROESPROC glMapBuffer = NULL;
	PFNGLUNMAPBUFFEROESPROC glUnmapBuffer = NULL;

#else
	#ifndef GL_WRITE_ONLY
		#define GL_WRITE_ONLY	GL_WRITE_ONLY_ARB
	#endif
	PFNGLGENBUFFERSARBPROC glGenBuffers = NULL;
	PFNGLBINDBUFFERARBPROC glBindBuffer = NULL;
	PFNGLBUFFERDATAARBPROC glBufferData = NULL;
	PFNGLDELETEBUFFERSARBPROC glDeleteBuffers = NULL;
	PFNGLMAPBUFFERARBPROC glMapBuffer = NULL;
	PFNGLUNMAPBUFFERARBPROC glUnmapBuffer = NULL;
	//PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = NULL;
#endif

	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitializeExtensionFunctionPointers
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_VertexBufferObject::InitializeExtensionFunctionPointers()
	{
#ifdef RENDER_WIN_GLES_20
		glMapBuffer = (PFNGLMAPBUFFEROESPROC) eglGetProcAddress("glMapBufferOES");
		glUnmapBuffer = (PFNGLUNMAPBUFFEROESPROC) eglGetProcAddress("glUnmapBufferOES");
#else
		glGenBuffers = (PFNGLGENBUFFERSARBPROC) SDL_GL_GetProcAddress("glGenBuffersARB");
		glBindBuffer = (PFNGLBINDBUFFERARBPROC) SDL_GL_GetProcAddress("glBindBufferARB");
		glBufferData = (PFNGLBUFFERDATAARBPROC) SDL_GL_GetProcAddress("glBufferDataARB");
		glDeleteBuffers = (PFNGLDELETEBUFFERSARBPROC) SDL_GL_GetProcAddress("glDeleteBuffersARB");
		glMapBuffer = (PFNGLMAPBUFFERARBPROC) SDL_GL_GetProcAddress("glMapBufferARB");
		glUnmapBuffer = (PFNGLUNMAPBUFFERARBPROC) SDL_GL_GetProcAddress("glUnmapBufferARB");
		//glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC) SDL_GL_GetProcAddress("glVertexAttribPointerARB");
		// Load Vertex Data Into The Graphics Card Memory

		if (!glGenBuffers || !glBindBuffer || !glBufferData || !glDeleteBuffers)
			return false;
#endif

		if (!glMapBuffer || !glUnmapBuffer)
			return false;

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_VertexBufferObject::C_VertexBufferObject()
		: m_Stride(0), m_NumVertices(0)
	{
		glGenBuffers(1, &m_VertexBufferId);
		m_UniqueID = (u32)this;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_VertexBufferObject::~C_VertexBufferObject()
	{
		glDeleteBuffers(1, &m_VertexBufferId);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Bind
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBufferObject::Bind()
	{
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Unbind
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBufferObject::Unbind()
	{
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_VertexBufferObject::ReleaseResources()
	{
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_VertexBufferObject::RestoreResources()
	{
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateFromRect
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBufferObject::CreateFromRect(const C_Vector2 & size, float fZpos, const C_Vector2 & texCoordLT, const C_Vector2 & texCoordRB)
	{
		m_Stride = 3*C_SIZE_OF_FLOAT + 2*C_SIZE_OF_FLOAT;

		glBindBuffer(GL_ARRAY_BUFFER, m_VertexBufferId);
		glBufferData(GL_ARRAY_BUFFER, 4 * m_Stride, 0, GL_STATIC_DRAW);

		//@ glMapBufferOES
		u8 * data = (u8*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		//@ 3*POS + 2*TEX_COORD
		u32 startId = 0;

		//GL_TRIANGLE_STRIP Draws a series of triangles using vertices v0, v1, v2, then v2, v1, v3 (note the order), then v2, v3, v4, and so on

		//@ VERTEX 0
		(*(C_Vector3*)&data[startId]) = C_Vector(-size.m_x / 2.f, +size.m_y / 2.f, fZpos);	//LB
		(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2(texCoordLT.m_x, texCoordRB.m_y);
		startId += m_Stride;
		//@ VERTEX 1
		
		(*(C_Vector3*)&data[startId]) = C_Vector(size.m_x / 2.f, +size.m_y / 2.f, fZpos);		//@ POSITION
		(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2(texCoordRB.m_x, texCoordRB.m_y);			//@ RB
		startId += m_Stride;
		
		//@ VERTEX 2
		(*(C_Vector3*)&data[startId]) = C_Vector(-size.m_x / 2.f, -size.m_y / 2.f, fZpos);	//@ POSITION
		(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2(texCoordLT.m_x, texCoordLT.m_y);			//@ LT
		startId += m_Stride;
		
		//@ VERTEX 3
		(*(C_Vector3*)&data[startId]) = C_Vector(size.m_x / 2.f, -size.m_y / 2.f, fZpos);		//@ POSITION
		(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2(texCoordRB.m_x, texCoordLT.m_y);			//@ RT
		

		glUnmapBuffer(GL_ARRAY_BUFFER);
		m_NumVertices = 4;
		//m_VertexFormat = VF_POSITION | VF_TEXTURE0;

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateFromRect
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBufferObject::CreateFromRect(const sysmath::C_Vector2 & worldPos, const sysmath::C_Vector2 & size, float fZpos, const C_Vector2 & texCoordLT, const C_Vector2 & texCoordRB)
	{
		m_Stride = 3*C_SIZE_OF_FLOAT + 2*C_SIZE_OF_FLOAT;

		glBindBuffer(GL_ARRAY_BUFFER, m_VertexBufferId);
		glBufferData(GL_ARRAY_BUFFER, 4 * m_Stride, 0, GL_STATIC_DRAW);

		//@ glMapBufferOES
		u8 * data = (u8*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		//@ 3*POS + 2*TEX_COORD
		u32 startId = 0;
		
		//@ VERTEX 0
		(*(C_Vector3*)&data[startId]) = C_Vector(worldPos.m_x - size.m_x / 2.f, worldPos.m_y + size.m_y / 2.f, fZpos);	//LT
		(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = texCoordLT;
		startId += m_Stride;
		
		//@ VERTEX 1
		(*(C_Vector3*)&data[startId]) = C_Vector(worldPos.m_x + size.m_x / 2.f, worldPos.m_y + size.m_y / 2.f, fZpos);		//@ POSITION
		(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2(texCoordRB.m_x, texCoordLT.m_y);			//@ RT
		startId += m_Stride;
		
		//@ VERTEX 2
		(*(C_Vector3*)&data[startId]) = C_Vector(worldPos.m_x - size.m_x / 2.f, worldPos.m_y - size.m_y / 2.f, fZpos);	//@ POSITION
		(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2(texCoordLT.m_x, texCoordRB.m_y);			//@ LB
		startId += m_Stride;
		
		//@ VERTEX 3
		(*(C_Vector3*)&data[startId]) = C_Vector(worldPos.m_x + size.m_x / 2.f, worldPos.m_y - size.m_y / 2.f, fZpos);		//@ POSITION
		(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = texCoordRB;			//@ RB


		glUnmapBuffer(GL_ARRAY_BUFFER);
		m_NumVertices = 4;
		//m_VertexFormat = VF_POSITION | VF_TEXTURE0;

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateFromAnimTexture
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBufferObject::CreateFromAnimTexture(const sysmath::C_Vector2 & size, const sysmath::C_Point & framesCount, float fZpos)
	{
		m_NumVertices = framesCount.x * framesCount.y * 4;
		m_Stride = 3*C_SIZE_OF_FLOAT + 2*C_SIZE_OF_FLOAT;

		glBindBuffer(GL_ARRAY_BUFFER, m_VertexBufferId);
		glBufferData(GL_ARRAY_BUFFER, m_NumVertices * m_Stride, 0, GL_STATIC_DRAW);

		//@ glMapBufferOES
		u8 * data = (u8*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		u32 startId = 0;

		float xCoordSize = 1.f / framesCount.x;
		float yCoordSize = 1.f / framesCount.y;

		for (s32 j = 0; j < framesCount.y; ++j)
		{
			for (s32 i = 0; i < framesCount.x; ++i)
			{
				//@ VERTEX 0
				(*(C_Vector3*)&data[startId]) = C_Vector(-size.m_x / 2.f, +size.m_y / 2.f, fZpos);	//LB
				(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2(i*xCoordSize, (j+1)*yCoordSize);
				startId += m_Stride;
				//@ VERTEX 1

				(*(C_Vector3*)&data[startId]) = C_Vector(size.m_x / 2.f, +size.m_y / 2.f, fZpos);		//@ POSITION
				(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2((i+1)*xCoordSize, (j+1)*yCoordSize);			//@ RB
				startId += m_Stride;

				//@ VERTEX 2
				(*(C_Vector3*)&data[startId]) = C_Vector(-size.m_x / 2.f, -size.m_y / 2.f, fZpos);	//@ POSITION
				(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2(i*xCoordSize, j*yCoordSize);			//@ LT
				startId += m_Stride;

				//@ VERTEX 3
				(*(C_Vector3*)&data[startId]) = C_Vector(size.m_x / 2.f, -size.m_y / 2.f, fZpos);		//@ POSITION
				(*(C_Vector2*)&data[startId + 3*C_SIZE_OF_FLOAT]) = C_Vector2((i+1)*xCoordSize, j*yCoordSize);			//@ RT
				startId += m_Stride;
			}
		}

		glUnmapBuffer(GL_ARRAY_BUFFER);
		//m_VertexFormat = VF_POSITION | VF_TEXTURE0;

		return;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBufferObject::Render() const
	{
#ifdef RENDER_WIN_GLES_20
		Render_GLES_20();
#else
		Render_StandardGL();
#endif
	}


#ifdef RENDER_WIN_GLES_20
	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render_GLES_20
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBufferObject::Render_GLES_20() const
	{
		return;
	}

#else
	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render_StandardGL
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBufferObject::Render_StandardGL() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_VertexBufferId);

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, m_Stride, 0);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, m_Stride, (char*)NULL + 12);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, m_NumVertices);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
#endif


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderQuadElement
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBufferObject::RenderQuadElement(u32 startElementID, u32 elementCount) const
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_VertexBufferId);

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, m_Stride, (char*)NULL + startElementID);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, m_Stride, (char*)NULL + startElementID + 12);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, elementCount * 4);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

}}