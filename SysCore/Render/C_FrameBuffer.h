/*
* filename:			C_FrameBuffer.h
*
* author:			Kral Jozef
* date:				10/17/2011		13:16
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysCore/Render/I_FrameBufferEffect.h>
#include <SysCore/Render/C_Texture.h>
#include <SysCore/Render/C_Shader.h>
#include <SysCore/Render/C_VertexBufferObject.h>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//@ C_FrameBuffer
	class SYSCORE_API C_FrameBuffer
	{
	public:
		//@ c-tor
		C_FrameBuffer()
			: m_Width(0), m_Height(0), m_FrameBufferID(0), m_DepthRenderBufferID(0) {};

		//@ Initialize
		static bool				InitializeExtensionFunctionPointers();

		// Initialize
		bool						Create(u32 width, u32 height);
		//@ Deinitialize
		bool						Destroy();
		//@ Release - diff against Destroy -> releae freed only resources in VRAM, not texture instance!
		bool						ReleaseResources();
		//@ Restore - only restore VRAM resource
		bool						RestoreResources();
		//@ InitializeShaders
		void						InitializeShaders();
		//@ StartCapture
		void						StartCapture();
		//@ EndCapture
		void						EndCapture();
		//@ Render
		void						Render();
		//@ SetEffect
		void						SetEffect(T_FrameBufferEffect effect) { m_Effect = effect; }
		//@ ClearEffect
		void						ClearEffect() { m_Effect.reset(); }

	private:
		float						m_Width;
		float						m_Height;
		u32						m_WndWidth;
		u32						m_WndHeight;
		u32						m_FrameBufferID;
		u32						m_DepthRenderBufferID;
		u32						m_FBOTextureID;
		T_FrameBufferEffect	m_Effect;
		T_Texture				m_FBOTexture;

		T_Shader					m_BasicShader;
		T_VertexBufferObject	m_VertexBuffer;
	};


	typedef boost::shared_ptr<C_FrameBuffer>	T_FrameBuffer;

}}
