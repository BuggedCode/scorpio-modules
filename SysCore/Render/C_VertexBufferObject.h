/*
* filename:			C_VertexBufferObject.h
*
* author:			Kral Jozef
* date:				1/30/2012		18:34
* version:			1.00
* brief:				THis VBO are specific for scorpio 2D engine (has just verticies and UVs)
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <boost/shared_ptr.hpp>
#include <Common/types.h>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Point.h>

namespace scorpio{ namespace sysrender{

	class C_VertexBufferManager;
	class C_FrameBuffer;

	//////////////////////////////////////////////////////////////////////////
	//@ C_VertexBufferObject
	class SYSCORE_API C_VertexBufferObject
	{
		friend class C_VertexBufferManager;
		friend class C_FrameBuffer;
	public:
		//@ d-tor
		~C_VertexBufferObject();

		//@ InitializeExtensionFunctionPointers
		static bool				InitializeExtensionFunctionPointers();
		//@ ResleaseResources
		bool						ReleaseResources();
		//@ RestoreResources
		bool						RestoreResources();
		//@ Bind
		void						Bind();
		//@ Unbind
		void						Unbind();

		//@ CreateFrom
		void						CreateFromRect(const sysmath::C_Vector2 & size, float fZpos, const sysmath::C_Vector2 & texCoordLT, const sysmath::C_Vector2 & texCoordRB);
		void						CreateFromRect(const sysmath::C_Vector2 & worldPos, const sysmath::C_Vector2 & size, float fZpos, const sysmath::C_Vector2 & texCoordLT, const sysmath::C_Vector2 & texCoordRB);
		//@ CreateFromAnimTexture
		void						CreateFromAnimTexture(const sysmath::C_Vector2 & size, const sysmath::C_Point & framesCount, float fZpos);

		//@ GetUID
		T_Hash32					GetUID() const { return m_UniqueID; }

		//@ Render
		void						Render() const;
		//@ RenderElement
		void						RenderQuadElement(u32 startElementID, u32 elementCount) const;

	private:
#ifdef RENDER_WIN_GLES_20
		//@ Render_GLES_20
		void						Render_GLES_20() const;
#else
		//@ Render_StandardGL
		void						Render_StandardGL() const;
#endif

	private:
		//@ c-tor
		C_VertexBufferObject();


	private:
		u32						m_VertexBufferId;
		T_Hash32					m_UniqueID;
		u32						m_NumVertices;
		u16						m_Stride;
	};


	typedef boost::shared_ptr<C_VertexBufferObject>	T_VertexBufferObject;

}}