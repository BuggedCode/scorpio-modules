/*
* filename:			C_Quad.h
*
* author:			Kral Jozef
* date:				09/14/2010		20:50
* version:			1.00
* brief:
*/

#pragma once

#include <SysMath/C_Vector2.h>
#include <vector>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	class C_Quad
	{
	public:
		sysmath::C_Vector2	m_verticies[4];
	};

	typedef std::vector<C_Quad>	T_Quads;

}}