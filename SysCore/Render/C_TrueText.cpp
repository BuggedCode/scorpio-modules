/*
* filename:			C_TrueText.cpp
*
* author:			Kral Jozef
* date:				02/20/2011		14:01
* version:			1.00
* brief:
*/

#include "C_TrueText.h"
#include <SysUtils/C_TraceClient.h>
#include <freetype/ftglyph.h>
#include <SysMath/MathUtils.h>
#include <vector>
#include <string>
#include <ft2build.h>
#include <freetype/freetype.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include "C_GlErrorCheck.h"
#include <SysMemManager/globalNewDelete.h>


namespace scorpio{ namespace sysrender{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;

	static const u32 C_DISPLAY_LIST_COUNT = 128;

	static FT_Face g_st_Face = NULL;	//phuj but not need to include freetype into header

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_TrueText::C_TrueText(const sysutils::T_Stream inputStream, u32 nSize)
		: m_Textures(NULL)
	{
		m_Height = nSize;
		if (!inputStream)
		{
			TRACE_E("Invalid input stream");
			return;
		}
		m_FileName = inputStream->GetFileName();
		Create(inputStream);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_TrueText::~C_TrueText()
	{
		ReleaseReources();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Create
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_TrueText::Create(const sysutils::T_Stream inputStream)
	{
		FT_Library library;
		FT_Error res = FT_Init_FreeType(&library);
		if (res != 0)
		{
			TRACE_E("Error in Init FreeType Library");
			return false;
		}


		FT_Face face;
		C_String fileName = inputStream->GetFileName();
		if (inputStream->GetBuffer())
		{
			//@ load from memoryStream
			res = FT_New_Memory_Face(library, inputStream->GetBuffer(), inputStream->GetBufferLength(), 0, &face);
			if (res != 0)
			{
				TRACE_FE("Error in loading font memory file %s", fileName.c_str());
				return false;
			}
		}
		else
		{
			//@ load form file
			res = FT_New_Face(library, fileName.c_str(), 0, &face);
			if (res != 0)
			{
				TRACE_FE("Error in loading font file %s", fileName.c_str());
				return false;
			}
		}
		

		FT_Set_Char_Size(face, m_Height << 6, m_Height << 6, 96, 96);	//2 << 6 = 64, freeType accept seize in 1/64

		m_ListBase = glGenLists(C_DISPLAY_LIST_COUNT);	//for english (Czech need more)
		m_Textures = new GLuint[C_DISPLAY_LIST_COUNT];
		glGenTextures(C_DISPLAY_LIST_COUNT, m_Textures);

		//////////////////////////////////////////////////////////////////////////
		bool bRes = true;
		for (u32 i = 0; i < C_DISPLAY_LIST_COUNT; ++i)
		{
			g_st_Face = face;
			bRes &= GenerateDisplayList(i, m_ListBase, m_Textures);
		}
		if (!bRes)
		{
			TRACE_E("Error in GenerateDisplayList");
			return false;
		}

		FT_Done_Face(face);
		FT_Done_FreeType(library);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GenerateDisplayList
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_TrueText::GenerateDisplayList(u32 charIndex, GLuint listBase, GLuint * textures)
	{
		//Load the Glyph for our character.
		if (FT_Load_Glyph(g_st_Face, FT_Get_Char_Index(g_st_Face, charIndex), FT_LOAD_DEFAULT) != 0)
			return false;

		//Move the face's glyph into a Glyph object.
		FT_Glyph glyph;
		if (FT_Get_Glyph(g_st_Face->glyph, &glyph) != 0)
			return false;

		//Convert the glyph to a bitmap.
		if (FT_Glyph_To_Bitmap(&glyph, ft_render_mode_normal, 0, 1) != 0)
			return false;
		
		FT_BitmapGlyph bitmapGlyph = (FT_BitmapGlyph)glyph;
		
		//This reference will make accessing the bitmap easier
		FT_Bitmap bitmap = bitmapGlyph->bitmap;

		//Use our helper function to get the widths of
		//the bitmap data that we will need in order to create
		//our texture.
		s32 width = next_p2(bitmap.width);
		s32 height = next_p2(bitmap.rows);

		//Allocate memory for the texture data.
		GLubyte* expanded_data = new GLubyte[ 2 * width * height];
		//Here we fill in the data for the expanded bitmap. Notice that we are using two channel bitmap (one for
		//luminocity and one for alpha), but we assign both luminocity and alpha to the value that we
		//find in the FreeType bitmap. We use the ?: operator so that value which we use
		//will be 0 if we are in the padding zone, and whatever is the the Freetype bitmap otherwise.
		for(s32 j=0; j < height; j++)
		{
			for(s32 i = 0; i < width; i++)
			{
				expanded_data[2*(i+j*width)]= expanded_data[2*(i+j*width)+1] = (i>=bitmap.width || j>=bitmap.rows) ? 0 : bitmap.buffer[i + bitmap.width*j];
			}
		}

		//Now we just setup some texture paramaters.
		glBindTexture(GL_TEXTURE_2D, textures[charIndex]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		C_GlErrorCheck::CheckError();

		//Here we actually create the texture itself, notice
		//that we are using GL_LUMINANCE_ALPHA to indicate that
		//we are using 2 channel data.
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data );
		C_GlErrorCheck::CheckError();
		

		//With the texture created, we don't need to expanded data anymore
		delete [] expanded_data;

		//So now we can create the display list
		glNewList(listBase + charIndex, GL_COMPILE);

		glBindTexture(GL_TEXTURE_2D, textures[charIndex]);
		glPushMatrix();

		//first we need to move over a little so that
		//the character has the right amount of space
		//between it and the one before it.
		glTranslatef((GLfloat)bitmapGlyph->left, 0.f, 0.f);

		//Now we move down a little in the case that the
		//bitmap extends past the bottom of the line 
		//(this is only true for characters like 'g' or 'y'.
		glTranslatef(0, (GLfloat)-(bitmapGlyph->top - bitmap.rows), 0);

		//Now we need to account for the fact that many of
		//our textures are filled with empty padding space.
		//We figure what portion of the texture is used by 
		//the actual character and store that information in 
		//the x and y variables, then when we draw the
		//quad, we will only reference the parts of the texture
		//that we contain the character itself.
		float	x = (float)bitmap.width / (float)width;
		float y = (float)bitmap.rows / (float)height;

		//Here we draw the texturemaped quads.
		//The bitmap that we got from FreeType was not 
		//oriented quite like we would like it to be,
		//so we need to link the texture to the quad
		//so that the result will be properly aligned.
		glTranslatef(0.f, (GLfloat)m_Height, 0.f);
		glBegin(GL_TRIANGLE_STRIP);
			glTexCoord2d(0, 0); 
			glVertex2f(0, (GLfloat)-bitmap.rows);

			glTexCoord2d(0, y); 
			glVertex2f(0, 0);

			glTexCoord2d(x, 0); 
			glVertex2f((GLfloat)bitmap.width, -(GLfloat)bitmap.rows);

			glTexCoord2d(x, y); 
			glVertex2f((GLfloat)bitmap.width, 0);
			
		glEnd();
		glPopMatrix();
		glTranslatef((GLfloat)(g_st_Face->glyph->advance.x >> 6), 0, 0);


		//increment the raster position as if we were a bitmap font.
		//(only needed if you want to calculate text length)
		glBitmap(0, 0, 0, 0, (GLfloat)(g_st_Face->glyph->advance.x >> 6), 0, NULL);

		//Finnish the display list
		glEndList();

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DrawStringFromLeft
	//
	//////////////////////////////////////////////////////////////////////////
	void C_TrueText::DrawStringFromLeft(const char *text, const sysmath::C_Vector2 & vctPos)
	{
		glColor4f(m_Color.m_x, m_Color.m_y, m_Color.m_z, m_Color.m_w);
		// We want a coordinate system where things coresponding to window pixels.
		//pushScreenCoordinateMatrix();					

		//char text[256];								// Holds Our String
		
		//Here is some code to split the text that we have been
		//given into a set of lines.  
		//This could be made much neater by using
		//a regular expression library such as the one avliable from
		//boost.org (I've only done it out by hand to avoid complicating
		//this tutorial with unnecessary library dependencies).
		/*const char *start_line=text;
		std::vector<std::string> lines;
		for(const char *c = text; *c; c++) 
		{
			if(*c=='\n') 
			{
				std::string line;
				for(const char *n = start_line; n < c; n++) 
					line.append(1,*n);
				lines.push_back(line);
				start_line = c+1;
			}
		}*/
		
		/*if(start_line) 
		{
			std::string line;
			for(const char * n = start_line; n < c; n++) 
				line.append(1,*n);
			lines.push_back(line);
		}*/

		glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT | GL_TRANSFORM_BIT);	
		glMatrixMode(GL_MODELVIEW);
		glDisable(GL_LIGHTING);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	

		glListBase(m_ListBase);

		//float modelview_matrix[16];	
		//glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix);

		//This is where the text display actually happens.
		//For each line of text we reset the modelview matrix
		//so that the line's text will start in the correct position.
		//Notice that we need to reset the matrix, rather than just translating
		//down by h. This is because when each character is
		//draw it modifies the current matrix so that the next character
		//will be drawn immediatly after it.  
		//for(u32 i = 0; i < lines.size(); i++) 
		{
			glPushMatrix();
			//glLoadIdentity();
			glTranslatef(vctPos.m_x, vctPos.m_y /*- m_Height * i*/, 0.f);
			//glMultMatrixf(modelview_matrix);

			//  The commented out raster position stuff can be useful if you need to
			//  know the length of the text that you are creating.
			//  If you decide to use it make sure to also uncomment the glBitmap command
			//  in make_dlist().
			//	glRasterPos2f(0,0);
			glCallLists((GLsizei)strlen(text)/*(GLsizei)lines[i].length()*/, GL_UNSIGNED_BYTE, /*lines[i].c_str()*/text);
			//	float rpos[4];
			//	glGetFloatv(GL_CURRENT_RASTER_POSITION ,rpos);
			//	float len=x-rpos[0];

			glPopMatrix();
		}


		glPopAttrib();
		//pop_projection_matrix();
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetStringWidth
	//
	//////////////////////////////////////////////////////////////////////////
	float C_TrueText::GetStringWidth(const char * text)
	{
		// We want a coordinate system where things coresponding to window pixels.
		//pushScreenCoordinateMatrix();					

		glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT | GL_TRANSFORM_BIT);	
		glMatrixMode(GL_MODELVIEW);
		glDisable(GL_LIGHTING);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	

		glListBase(m_ListBase);

		float modelview_matrix[16];	
		glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix);

		//This is where the text display actually happens.
		//For each line of text we reset the modelview matrix
		//so that the line's text will start in the correct position.
		//Notice that we need to reset the matrix, rather than just translating
		//down by h. This is because when each character is
		//draw it modifies the current matrix so that the next character
		//will be drawn immediatly after it.  
		float retLength = 0.f;
		{
			glPushMatrix();
			glLoadIdentity();
			glTranslatef(0.f, 0.f /*- m_Height * i*/, 0.f);
			glMultMatrixf(modelview_matrix);

			//  The commented out raster position stuff can be useful if you need to
			//  know the length of the text that you are creating.
			//  If you decide to use it make sure to also uncomment the glBitmap command
			//  in make_dlist().
			glRasterPos2f(0.f, 0.f);
			glCallLists((GLsizei)strlen(text), GL_UNSIGNED_BYTE, text);
			
			float rpos[4];
			glGetFloatv(GL_CURRENT_RASTER_POSITION, rpos);
			retLength = 0 - rpos[0];
			glPopMatrix();
		}


		glPopAttrib();
		//pop_projection_matrix();

		return retLength;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DrawStringFromRight
	//
	//////////////////////////////////////////////////////////////////////////
	void C_TrueText::DrawStringFromRight(const char * text, const sysmath::C_Vector2 & vctPos)
	{
		SC_ASSERT(false);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseReources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_TrueText::ReleaseReources()
	{
		glDeleteLists(m_ListBase, C_DISPLAY_LIST_COUNT);
		glDeleteTextures(C_DISPLAY_LIST_COUNT, m_Textures);
		SC_SAFE_DELETE_ARRAY(m_Textures)

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Reload
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_TrueText::RestoreReources()
	{
		//@ no need to delete, assumption delete RC was called prior
		T_Stream stream = T_FileSystemManager::GetInstance().Open(m_FileName);
		return Create(stream);
	}

}}