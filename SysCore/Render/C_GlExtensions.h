/*
* filename:			C_GlExtensions.h
*
* author:			Kral Jozef
* date:				02/24/2011		13:28
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>

namespace scorpio{ namespace sysrender{


	//////////////////////////////////////////////////////////////////////////
	//@ C_GLExtensions
	class SYSCORE_API C_GlExtensions
	{
	public:
		enum E_GlExtension
		{
			GL_EXT_RECTANGLE_TEXTURE = 0,
			GL_EXT_FRAME_BUFFER_OBJECT,
			GL_ARB_VERTEX_BUFFER_OBJECT,
			GL_GLSL_100,
		};

		//@ IsExtSupported
		static bool				IsExtSupported(E_GlExtension ext);
		//@ GetVersion
		static const char	*	GetVersion();
	};

}}