
#pragma once

#define STBI_VERSION 1

enum
{
	STBI_default = 0, // only used for req_comp

	STBI_grey       = 1,
	STBI_grey_alpha = 2,
	STBI_rgb        = 3,
	STBI_rgb_alpha  = 4
};

typedef unsigned char stbi_uc;

unsigned char *stbi_load(char const *filename, int *x, int *y, int *comp, int req_comp);
void				stbi_image_free(void *retval_from_stbi_load);
unsigned char *stbi_load_from_memory(stbi_uc const *buffer, int len, int *x, int *y, int *comp, int req_comp);
