/*
* filename:			I_FrameBufferEffect.h
*
* author:			Kral Jozef
* date:				10/24/2011		23:27
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <boost/shared_ptr.hpp>
#include <SysCore/Render/C_Texture.h>
#include <SysMath/C_Vector2.h>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	// I_FrameBufferEffect
	class SYSCORE_API I_FrameBufferEffect
	{
	public:
		//@ c-tor
		I_FrameBufferEffect() : m_bActive(true) {};

		//@ Bind
		virtual void			Bind() = 0;
		//@ Unbind
		virtual void			Unbind() = 0;
		//@ Render
		virtual void			Render(T_Texture fboTexture) = 0;
		//@ IsActive
		virtual bool			IsActive() const { return m_bActive; }
		//@ Update
		virtual void			Update(u32 inDeltaTime) = 0;
		//@ Activate
		virtual void			Activate(const sysmath::C_Vector2 & vctPos) = 0;	//JK HARD CODED NOW! use init struct

	protected:
		bool						m_bActive;
	};

	typedef boost::shared_ptr<I_FrameBufferEffect>		T_FrameBufferEffect;
}}
