/*
* filename:			C_GlErrorCheck.h
*
* author:			Kral Jozef
* date:				04/09/2011		11:56
* version:			1.00
* brief:
*/

#pragma once

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//@ C_GlErrorCheck
	class C_GlErrorCheck
	{
	public:
		//@ CheckError
		static void CheckError();
	};
}}
