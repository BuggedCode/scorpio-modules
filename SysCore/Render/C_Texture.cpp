/*
* filename:			C_Texture.cpp
*
* author:			Kral Jozef
* date:				02/21/2011		11:00
* version:			1.00
* brief:
*/


#include "C_Texture.h"
#include <SysCore/Render/stb_image.h>
#include <SysUtils/C_TraceClient.h>
#ifdef PLATFORM_WIN32
   #include <Windows.h>
   #include <gl/GL.h>
#elif defined PLATFORM_MACOSX
   #include <OpenGL/gl.h>
#endif
#include <SysUtils/FileSystem/C_FileSystemManager.h>


namespace scorpio{ namespace sysrender{

	using namespace sysutils;

	C_HashName C_Texture::C_EMPTY_FILE_NAME = C_HashName("Empty Name");
	C_HashName C_GENERATED_PARTICLE_TEXTURE = C_HashName("GPT");

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Texture::C_Texture()
		: m_Width(0), m_Height(0), m_Bpp(0), m_TextureID(0)
	{
		//////////////////////////////////////////////////////////////////////////
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Texture::~C_Texture()
	{
		if (m_TextureID)
			ReleaseResources();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Texture::ReleaseResources()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
		glDeleteTextures(1, &m_TextureID);

		GLenum err = glGetError();
		if (err != GL_NO_ERROR)
		{
			TRACE_FE("Error in deleting Texture: %d", m_TextureID);
			m_TextureID = 0;
			return false;
		}

		m_TextureID = 0;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Create
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Texture::Load(const T_Stream inputStream, E_TextureWrapMode texWrapMode, E_TextureFilteringMode texFilteringMode, sysutils::C_String relativeFileName)
	{
		//@ if is empty - Load is caled from reload:()
		if (!relativeFileName.empty())
			m_FileName = relativeFileName;

		m_WrapMode = texWrapMode;
		m_FilteringMode = texFilteringMode;

		if (!inputStream)
		{
			//TRACE_E("Invalid input stream");
			return false;
		}

		u8 * pData = NULL;	
		if (inputStream->GetBuffer())
		{
			//@ load from memoryStream we have a buffer of data
			pData = stbi_load_from_memory(inputStream->GetBuffer(), inputStream->GetBufferLength(), (s32*)&m_Width, (s32*)&m_Height, (s32*)&m_Bpp, STBI_default);
		}
		else
		{
			pData = stbi_load(inputStream->GetFileName(), (s32*)&m_Width, (s32*)&m_Height, (s32*)&m_Bpp, STBI_default);
		}

		if (!pData)
		{
			TRACE_FE("Failed to load %s", inputStream->GetFileName());
			return false;
		}

		//@ if swap inverted Y
		/*int i, j;
		for( j = 0; j*2 < m_Height; ++j )
		{
			int index1 = j * m_Width * m_Bpp;
			int index2 = (m_Height - 1 - j) * m_Width * m_Bpp;
			for( i = m_Width * m_Bpp; i > 0; --i )
			{
				unsigned char temp = pData[index1];
				pData[index1] = pData[index2];
				pData[index2] = temp;
				++index1;
				++index2;
			}
		}*/

		glGenTextures(1, &m_TextureID);
		glBindTexture(GL_TEXTURE_2D, m_TextureID);
		GLenum err = glGetError();
		if (err != GL_NO_ERROR)
		{
			TRACE_FE("Error in creating Texture %s, TextureID: %d, ErrorID: %d", inputStream->GetFileName(), m_TextureID, err);
			return false;
		}

		GLenum texFormat;
		switch(m_Bpp)
		{
		case 4:
			texFormat = GL_RGBA;//GL_BGRA_EXT;
			break;
		case 3:
			texFormat = GL_RGB;
			break;
		default:
			TRACE_FE("Unsupported Image Format with %d Bytes per pixel! FileName: %s", m_Bpp, inputStream->GetFileName());
			texFormat = m_Bpp;
		};	//switch
		
		glTexImage2D(GL_TEXTURE_2D, 0, m_Bpp, m_Width, m_Height, 0, texFormat, GL_UNSIGNED_BYTE, pData);

		GLint glFiltering = (texFilteringMode == E_TFM_LINEAR) ? GL_LINEAR : GL_NEAREST;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glFiltering);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glFiltering);
		
		GLint glTexWrapMode = (texWrapMode == E_TWM_CLAMP) ? GL_CLAMP : GL_REPEAT;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glTexWrapMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glTexWrapMode);

		stbi_image_free(pData);
		
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Create
	//
	//////////////////////////////////////////////////////////////////////////
	T_Texture C_Texture::Create(u32 nWidth, u32 nHeight, u8 * pTextureData, E_TextureWrapMode texWrapMode, E_TextureFilteringMode texFilteringMode, E_TextureInternalFormat texInternalFormat)
	{
		T_Texture retTexture = T_Texture(new C_Texture());
		retTexture->m_FileName = C_GENERATED_PARTICLE_TEXTURE;
		retTexture->m_WrapMode = texWrapMode;
		retTexture->m_FilteringMode = texFilteringMode;

		glGenTextures(1, &retTexture->m_TextureID);
		glBindTexture(GL_TEXTURE_2D, retTexture->m_TextureID);

		retTexture->m_Width = nWidth;
		retTexture->m_Height = nHeight;
		retTexture->m_Bpp = GL_RGBA;

		GLint glInternalFormat = (texInternalFormat == E_TIF_RGB) ? GL_RGB : GL_RGBA;
		glTexImage2D(GL_TEXTURE_2D, 0, glInternalFormat, retTexture->m_Width, retTexture->m_Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pTextureData);
		GLenum err = glGetError();
		if (err != GL_NO_ERROR)
		{
			TRACE_E("Error in creating particle Texture");
			return T_Texture();
		}

		GLint glFiltering = (retTexture->m_FilteringMode == E_TFM_LINEAR) ? GL_LINEAR : GL_NEAREST;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glFiltering/*GL_LINEAR*/);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glFiltering/*GL_LINEAR*/);

		GLint glTexWrapMode = (retTexture->m_WrapMode == E_TWM_CLAMP) ? GL_CLAMP : GL_REPEAT;
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glTexWrapMode/*GL_REPEAT*/);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glTexWrapMode/*GL_REPEAT*/);

		return retTexture;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ BindTexture
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Texture::BindTexture()
	{
		if (m_TextureID == -1)
		{
			TRACE_E("Binding texture ID is -1!");
			return;
		}
		glBindTexture(GL_TEXTURE_2D, m_TextureID);
		/*glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		glScalef((float)m_Width, (float)m_Height, 1.f);
		glMatrixMode(GL_MODELVIEW);*/
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Reload
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Texture::Reload(const T_Stream inputStream)
	{
		//@ after fullscreen switch this textures doesn't exists so don't delete un-existing textures
		if (m_TextureID)
			ReleaseResources();

		SC_ASSERT(m_FileName != C_GENERATED_PARTICLE_TEXTURE);

		//T_Stream stream = T_FileSystemManager::GetInstance().Open(m_FileName.GetName());
		bool bRes = Load(inputStream, m_WrapMode, m_FilteringMode, C_String());
		return bRes;
	}

}}