/*
* filename:			C_GlErrorCheck.cpp
*
* author:			Kral Jozef
* date:				04/09/2011		12:01
* version:			1.00
* brief:
*/

#include "C_GlErrorCheck.h"
#include <Common/PlatformDef.h>

#ifdef PLATFORM_WIN32
   #include <Windows.h>
   #include <gl/gl.h>
#elif defined (PLATFORM_MACOSX)
   #include <OpenGL/gl.h>
#endif

#include <SysUtils/C_String.h>
#include <SysUtils/C_TraceClient.h>

namespace scorpio{ namespace sysrender{

	using namespace sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CheckError
	//
	//////////////////////////////////////////////////////////////////////////
	void C_GlErrorCheck::CheckError()
	{
		GLenum err = glGetError();

		if (err != GL_NO_ERROR)
		{
			C_String strErr = "UNKNOWN";
			switch(err)
			{
			case GL_INVALID_ENUM:
				strErr = "GL_INVALID_ENUM";
				break;
			case GL_INVALID_VALUE:
				strErr = "GL_INVALID_VALUE";
				break;
			case GL_INVALID_OPERATION:
				strErr = "GL_INVALID_OPERATION";
				break;
			case GL_STACK_OVERFLOW:
				strErr = "GL_STACK_OVERFLOW";
				break;
			case GL_STACK_UNDERFLOW:
				strErr = "GL_STACK_UNDERFLOW";
				break;
			case GL_OUT_OF_MEMORY:
				strErr = "GL_OUT_OF_MEMORY";
				break;
			};	//switch

			TRACE_FE("OpenGL Erorr: %s", strErr.c_str());
		}
	}


}}
