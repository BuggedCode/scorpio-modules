/*
* filename:			C_Graphic.h
*
* author:			Kral Jozef
* date:				02/21/2011		16:52
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <boost/shared_ptr.hpp>
#include "C_Texture.h"
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Point.h>
#include <SysMath/C_Rect.h>
#include <SysCore/Render/RenderTypes.h>
#include <SysCore/Render/I_VertexBuffer.h>
#include <SysCore/Render/C_VertexBufferObject.h>
#include <SysCore/Render/C_Quad.h>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Graphic
	class SYSCORE_API C_Graphic
	{
	public:
		//@ c-tor
		C_Graphic()
			: m_BlendingMode(E_BM_SRC_ALPHA__DST_ONE_MINUS_ALPHA) {};

		//@ d-tor
		~C_Graphic() {};
		//@ loadPicture
		bool			LoadTexture(const	sysutils::T_Stream inputStream, E_TextureWrapMode texWrapMode, E_TextureFilteringMode texFilterMode, sysutils::C_String relativeFileName);
		//@ ReleaseResources
		bool			ReleaseResources();
		//@ Reload - reload texture resource
		bool			Reload(const sysutils::T_Stream inputStream);
		//@ RenderLine
		void			RenderLine(const sysmath::C_Vector2 & vctStart, const sysmath::C_Vector2 & vctEnd, const sysmath::C_Vector4 & color);
		//@ RenderLines
		void			RenderLines(const std::vector<sysmath::C_Vector2> & points, const sysmath::C_Vector4 & color);

		//@ GetFileName
		const sysutils::C_HashName	&	GetFileName() const
		{
			if (m_Texture)
				return m_Texture->GetFileName();
			return C_Texture::C_EMPTY_FILE_NAME;
		}

		//@ getWidth
		float			GetWidth() 
		{ 
			if (m_Texture)
				return (float)m_Texture->GetWidth();
			return 0.f;
		}

		//@ getheight
		float			GetHeight()
		{ 
			if (m_Texture)
				return (float)m_Texture->GetHeight();
			return 0.f; 
		}

		//@ setBlendingMode
		void			SetBlendingMode(E_BlendingMode blendMode) { m_BlendingMode = blendMode; }

		//////////////////////////////////////////////////////////////////////////
		// OpenGL Specific Interface

		//@ Render
		void			Render(const sysmath::C_Vector2 & vctPivot, const sysmath::C_Point & size, const sysmath::C_Vector4 & color, float fRotAngle, float fScale);
		//@ Render - with texture coord
		void			Render(const sysmath::C_Vector2 & vctPivot, const sysmath::C_Point & size, const sysmath::C_Vector2 & texCoordX, const sysmath::C_Vector2 & texCoordY, const sysmath::C_Vector4 & color, float fRotAngle, float fScale);
		//@ RenderRect
		void			RenderRect(const sysmath::C_Vector2 & vctPivot, const sysmath::C_Point & size, const sysmath::C_Vector4 & color, bool bFilled, float fRotAngle, float fScale);
		//@ RenderRect
		void			RenderRect(const sysmath::C_Vector2 & vctLTVertex, const sysmath::C_Vector2 & vctRBVertex, const sysmath::C_Vector4 & color, bool bFilled);
		//@ CreateTextureFromArray
		bool			CreateTextureFromArray(u8 * pTextureData, u32 nWidth, u32 nHeight);
		//@ RenderQuad
		void			RenderQuad(const sysmath::C_Vector2 & vertex0, const sysmath::C_Vector2 & vertex1, const sysmath::C_Vector2 & vertex2, const sysmath::C_Vector2 & vertex3, const sysmath::C_Vector4 & color);
		//@ RenderBatchQuad
		void			RenderBatchQuad(const T_Quads & quadBatch, const sysmath::C_Vector4 & color);
		//@ RenderVertexBuffer
		void			RenderVertexBuffer(const I_VertexBuffer & vertexBuffer, u32 firstVertex, u32 verticiesCount);
		//@ RenderVBO
		void			RenderVBO(const T_VertexBufferObject vbo, const sysmath::C_Vector2 & worldPos, float fAngle, float fScale, const sysmath::C_Vector4 & color);
		//@ RenderVBOClipped
		void			RenderVBOClipped(const T_VertexBufferObject vbo, const sysmath::C_Vector2 & worldPos, float fAngle, float fScale, const sysmath::C_Vector4 & color, const sysmath::C_Point & clipLBCoord, const sysmath::C_Point & clipAreaSize);
		//@ RenderVBOElements
		void			RenderVBOElements(const T_VertexBufferObject vbo, u32 startIndex, const sysmath::C_Vector2 & worldPos, float fAngle, float fScale, const sysmath::C_Vector4 & color);

	private:
		//@ set/turn on blening mode
		void			SetupBlendMode();


	private:
		T_Texture		m_Texture;
		E_BlendingMode	m_BlendingMode;
	};


	typedef boost::shared_ptr<C_Graphic>		T_GraphicRes;	//graphics resource

}}

typedef scorpio::sysrender::T_GraphicRes		T_Graphics;