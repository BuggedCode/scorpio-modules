/*
* filename:			C_TrueText.h
*
* author:			Kral Jozef
* date:				02/20/2011		14:01
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Vector4.h>
#include <SysUtils/C_String.h>
#include <SysUtils/FileSystem/C_Stream.h>
#ifdef PLATFORM_WIN32
   #include <Windows.h>
   #include <gl/GL.h>
#elif defined PLATFORM_MACOSX
   #include <OpenGL/gl.h>
#endif
#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace sysrender{

	//# forward

	//////////////////////////////////////////////////////////////////////////
	//@ C_TrueText
	class SYSCORE_API C_TrueText
	{
	public:
		//@ c-tor
		C_TrueText(const sysutils::T_Stream inputStream, u32 nSize);
		//@ d-tor
		~C_TrueText();


		void					DrawStringFromLeft(const char * text, const sysmath::C_Vector2 & vctPos);
		void					DrawStringFromRight(const char * text, const sysmath::C_Vector2 & vctPos);
		//void					setHeightPix(u32 nHeightPix);
		void					SetColor(const sysmath::C_Vector4 & vctColor) { m_Color = vctColor; }
		float					GetStringWidth(const char * text);
		//@ ReleaseReources - from VRAM
		bool					ReleaseReources();
		//@ RestoreReources();
		bool					RestoreReources();

	private:
		//@ Create
		bool					Create(const sysutils::T_Stream inputStream);
		//@ GenerateDisplayList
		bool					GenerateDisplayList(u32 charIndex, GLuint listBase, GLuint * textures);


	private:
		sysutils::C_String	m_FileName;
		u32						m_Height;
		GLuint*					m_Textures;	//pointer to texture
		GLuint					m_ListBase;
		sysmath::C_Vector4	m_Color;
	};

}}

	typedef boost::shared_ptr<scorpio::sysrender::C_TrueText>	T_TrueText;