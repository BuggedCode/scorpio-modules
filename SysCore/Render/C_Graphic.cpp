/*
* filename:			C_Graphic.cpp
*
* author:			Kral Jozef
* date:				02/21/2011		16:53
* version:			1.00
* brief:
*/

#include "C_Graphic.h"
#ifdef PLATFORM_WIN32
	#include <Windows.h>
   #include <gl/GL.h>
#elif defined PLATFORM_MACOSX
   #include <OpenGL/gl.h>
#endif

namespace scorpio{ namespace sysrender{


	using namespace sysmath;
	using namespace sysutils;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ loadPicture
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Graphic::LoadTexture(const T_Stream inputStream, E_TextureWrapMode texWrapMode, E_TextureFilteringMode texFilterMode, sysutils::C_String relativeFileName)
	{
		m_Texture = T_Texture(new C_Texture());
		if (!m_Texture->Load(inputStream, texWrapMode, texFilterMode, relativeFileName))
		{
			return false;
		}

		//@ generate texture
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderLine
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::RenderLine(const sysmath::C_Vector2 & vctStart, const sysmath::C_Vector2 & vctEnd, const sysmath::C_Vector4 & color)
	{
		C_Vector2 vertices[2] = { vctStart, vctEnd };
		glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
		glDisable(GL_TEXTURE_2D);

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);
		
		glVertexPointer(2, GL_FLOAT, 0, vertices);
		glDrawArrays(GL_LINES, 0, 2);

		glPopAttrib();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderLines
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::RenderLines(const std::vector<C_Vector2> & points, const C_Vector4 & color)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
		glDisable(GL_TEXTURE_2D);

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);
		
		u32 size = (u32)points.size();
		glVertexPointer(2, GL_FLOAT, 0, &points[0]);
		glDrawArrays(GL_LINES, 0, size);

		glPopAttrib();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::Render(const C_Vector2 & vctPivot, const C_Point & size, const C_Vector4 & color, float fRotAngle, float fScale)
	{
		glPushAttrib(GL_CURRENT_BIT | GL_COLOR_BUFFER_BIT);
		SetupBlendMode();

		glPushMatrix();

		glTranslatef(vctPivot.m_x, vctPivot.m_y, 0.f);
		glScalef(fScale, fScale, 1.f);
		glRotatef(fRotAngle, 0.f, 0.f, 1.f);

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);
		if (m_Texture)
			m_Texture->BindTexture();

		glBegin(GL_TRIANGLE_STRIP);
		{
			glTexCoord2f(0.f, 1.f);
			glVertex2f( -size.x*.5f, size.y*.5f);
			glTexCoord2f(1.f, 1.f);
			glVertex2f( size.x*.5f, size.y*.5f);
			glTexCoord2f(0.f, 0.f);
			glVertex2f( -size.x*.5f, -size.y*.5f);
			glTexCoord2f(1.f, 0.f);
			glVertex2f( size.x*.5f, -size.y*.5f);
		}
		glEnd();

		glPopMatrix();
		glPopAttrib();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::Render(const C_Vector2 & vctPivot, const C_Point & size, const C_Vector2 & texCoordX, const C_Vector2 & texCoordY, const C_Vector4 & color, float fRotAngle, float fScale)
	{
		glPushAttrib(GL_CURRENT_BIT | GL_COLOR_BUFFER_BIT);
		SetupBlendMode();

		glPushMatrix();

		glTranslatef(vctPivot.m_x, vctPivot.m_y, 0.f);
		glScalef(fScale, fScale, 1.f);
		glRotatef(fRotAngle, 0.f, 0.f, 1.f);

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);
		if (m_Texture)
			m_Texture->BindTexture();

		glBegin(GL_TRIANGLE_STRIP);
		{
			glTexCoord2f(texCoordX.m_x, texCoordY.m_y);
			glVertex2f( -size.x*.5f, size.y*.5f);
			glTexCoord2f(texCoordX.m_y, texCoordY.m_y);
			glVertex2f( size.x*.5f, size.y*.5f);
			glTexCoord2f(texCoordX.m_x, texCoordY.m_x);
			glVertex2f( -size.x*.5f, -size.y*.5f);
			glTexCoord2f(texCoordX.m_y, texCoordY.m_x);
			glVertex2f( size.x*.5f, -size.y*.5f);
		}
		glEnd();

		glPopMatrix();
		glPopAttrib();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderRect
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::RenderRect(const C_Vector2 & vctPivot, const C_Point & size, const C_Vector4 & color, bool bFilled, float fRotAngle, float fScale)
	{
		glPushMatrix();
		glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);

		glDisable(GL_TEXTURE_2D);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);

		glTranslatef(vctPivot.m_x, vctPivot.m_y, 0.f);
		glScalef(fScale, fScale, 1.f);
		glRotatef(fRotAngle, 0.f, 0.f, 1.f);

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);

		if (bFilled)
		{
			C_Vector2 vertices[4] = { C_Vector2(-size.x*.5f, size.y*.5f), C_Vector2( size.x*.5f, size.y*.5f),
											  C_Vector2(-size.x*.5f, -size.y*.5f), C_Vector2(size.x*.5f, -size.y*.5f) };

			glVertexPointer(2, GL_FLOAT, 0, vertices);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
		else
		{
			/*C_Vector2 vertices[4] = { C_Vector2(-size.x*.5f, size.y*.5f), C_Vector2(size.x*.5f, size.y*.5f), 
											  C_Vector2(size.x*.5f, -size.y*.5f), C_Vector2(-size.x*.5f, -size.y*.5f) };
			glVertexPointer(2, GL_FLOAT, 0, vertices);
			glDrawArrays(GL_LINE_LOOP, 0, 4);*/

			glBegin(GL_LINE_LOOP);
			{
				glVertex2f( -size.x*.5f, size.y*.5f);
				glVertex2f( size.x*.5f, size.y*.5f);
				glVertex2f( size.x*.5f, -size.y*.5f);
				glVertex2f( -size.x*.5f, -size.y*.5f);
			}
			glEnd();
		}

		glPopAttrib();
		glPopMatrix();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderRect
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::RenderRect(const C_Vector2 & vctLTVertex, const C_Vector2 & vctRBVertex, const C_Vector4 & color, bool bFilled)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
		
		glDisable(GL_TEXTURE_2D);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);

		if (bFilled)
		{
			C_Vector2 vertices[4] = { C_Vector2(vctLTVertex.m_x, vctLTVertex.m_y), C_Vector2(vctRBVertex.m_x, vctLTVertex.m_y),
											  C_Vector2(vctLTVertex.m_x, vctRBVertex.m_y), C_Vector2(vctRBVertex.m_x, vctRBVertex.m_y) };

			glVertexPointer(2, GL_FLOAT, 0, vertices);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
		else
		{
			C_Vector2 vertices[4] = { C_Vector2(vctLTVertex.m_x, vctLTVertex.m_y), C_Vector2(vctRBVertex.m_x, vctLTVertex.m_y), 
											  C_Vector2(vctRBVertex.m_x, vctRBVertex.m_y), C_Vector2(vctLTVertex.m_x, vctRBVertex.m_y) };
			glVertexPointer(2, GL_FLOAT, 0, vertices);
			glDrawArrays(GL_LINE_LOOP, 0, 4);	
		}
		
		glPopAttrib();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateTextureFromArray
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Graphic::CreateTextureFromArray(u8 * pTextureData, u32 nWidth, u32 nHeight)
	{
		SC_ASSERT(!m_Texture);
		m_Texture = C_Texture::Create(nWidth, nHeight, pTextureData, E_TWM_REPEAT, E_TFM_LINEAR, E_TIF_RGBA);
		return (m_Texture != NULL);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderQuad
	//
	//////////////////////////////////////////////////////////////////////////
	/*void C_Graphic::RenderQuad(const sysmath::C_Vector2 & vertex0, const sysmath::C_Vector2 & vertex1, const sysmath::C_Vector2 & vertex2, const sysmath::C_Vector2 & vertex3, const sysmath::C_Vector4 & color)
	{
		glPushAttrib(GL_COLOR_BUFFER_BIT);
		SetupBlendMode();
		
		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);
		glEnable(GL_TEXTURE_2D);
		if (m_Texture)
			m_Texture->BindTexture();

		glBegin(GL_TRIANGLE_STRIP);
		{
			glTexCoord2f(0.f, 0.f);
			glVertex2f( vertex0.m_x, vertex0.m_y);
			glTexCoord2f(1.f, 0.f);
			glVertex2f( vertex1.m_x, vertex1.m_y);
			glTexCoord2f(0.f, 1.f);
			glVertex2f( vertex3.m_x, vertex3.m_y);
			glTexCoord2f(1.f, 1.f);
			glVertex2f( vertex2.m_x, vertex2.m_y);
		}
		glEnd();

		glPopAttrib();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderBatchQuad
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::RenderBatchQuad(const T_Quads & quadBatch, const sysmath::C_Vector4 & color)
	{
		glPushAttrib(GL_CURRENT_BIT | GL_COLOR_BUFFER_BIT);
		SetupBlendMode();

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);
		glEnable(GL_TEXTURE_2D);
		if (m_Texture)
			m_Texture->BindTexture();

		glBegin(GL_TRIANGLE_STRIP);
		{
			for (T_Quads::const_iterator iter = quadBatch.begin(); iter != quadBatch.end(); ++iter)
			{
				const C_Quad & quad = (*iter);
				glTexCoord2f(0.f, 0.f);
				glVertex2f( quad.m_verticies[0].m_x, quad.m_verticies[0].m_y);
				glTexCoord2f(1.f, 0.f);
				glVertex2f( quad.m_verticies[1].m_x, quad.m_verticies[1].m_y);
				glTexCoord2f(0.f, 1.f);
				glVertex2f( quad.m_verticies[3].m_x, quad.m_verticies[3].m_y);
				glTexCoord2f(1.f, 1.f);
				glVertex2f( quad.m_verticies[2].m_x, quad.m_verticies[2].m_y);
			}
		}
		glEnd();

		glPopAttrib();
	}*/


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetupBlendMode
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::SetupBlendMode()
	{
		if (m_BlendingMode == E_BM_SRC_ALPHA__DST_ONE_MINUS_ALPHA)	//default alpha mask real transparency
			return;

      if (m_BlendingMode == E_BM_SRC_ONE__DST_ONE)
         glBlendFunc(GL_ONE, GL_ONE);

      else if (m_BlendingMode == E_BM_SRC_ALPHA__DST_ONE)
         glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Graphic::ReleaseResources()
	{
		if (!m_Texture)
			return true;

		return m_Texture->ReleaseResources();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Reload
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Graphic::Reload(const T_Stream inputStream)
	{
		if (!m_Texture)
			return true;
		
		return m_Texture->Reload(inputStream);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderVertexBuffer
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::RenderVertexBuffer(const I_VertexBuffer & vertexBuffer, u32 firstVertex, u32 verticiesCount)
	{
		glPushAttrib(GL_CURRENT_BIT | GL_COLOR_BUFFER_BIT);
		SetupBlendMode();

		glEnable(GL_TEXTURE_2D);
		if (m_Texture)
			m_Texture->BindTexture();

		u32 vertexFormat = vertexBuffer.GetVertexFormat();

		u8 * pData = vertexBuffer.GetData();
		u32 vertexSize = vertexBuffer.GetVertexSize();	//stride
		glEnableClientState(GL_VERTEX_ARRAY);
		
		if (vertexFormat & VF_POSITION)
		{
			glVertexPointer(3, GL_FLOAT, vertexSize, pData);
			pData += sizeof(float) * 3;
		}

		if (vertexFormat & VF_RHW)
			pData += sizeof(float);


		if (vertexFormat & VF_NORMAL)
		{
			glEnableClientState(GL_NORMAL_ARRAY);
			glNormalPointer(GL_FLOAT, vertexSize, pData);
			pData += sizeof(float) * 3;
		}

		if (vertexFormat & VF_DIFFUSE)
		{
			glEnableClientState(GL_COLOR_ARRAY);
			glColorPointer(4, GL_UNSIGNED_BYTE, vertexSize, pData);
			pData += 4;
		}

		if (vertexFormat & VF_TEXTURE0)
		{
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glTexCoordPointer(2, GL_FLOAT, vertexSize, pData);
			pData += sizeof(float) * 2;
		}

		glDrawArrays(GL_QUADS, firstVertex, verticiesCount);
		
		if (vertexFormat & VF_POSITION)
			glDisableClientState(GL_VERTEX_ARRAY);

		if (vertexFormat & VF_NORMAL)
			glDisableClientState(GL_NORMAL_ARRAY);
		
		if (vertexFormat & VF_DIFFUSE)
			glDisableClientState(GL_COLOR_ARRAY);

		if (vertexFormat & VF_TEXTURE0)
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glPopAttrib();
		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderVBO
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::RenderVBO(const T_VertexBufferObject vbo, const C_Vector2 & worldPos, float fAngle, float fScale, const C_Vector4 & color)
	{
		glPushMatrix();

		glTranslatef(worldPos.m_x, worldPos.m_y, 0.f);
		glScalef(fScale, fScale, 1.f);
		glRotatef(fAngle, 0.f, 0.f, 1.f);

		//////////////////////////////////////////////////////////////////////////
		glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);
		SetupBlendMode();

		glEnable(GL_TEXTURE_2D);
		if (m_Texture)
			m_Texture->BindTexture();

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);

		vbo->Render();


		glPopAttrib();

		glPopMatrix();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderVBOClipped
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::RenderVBOClipped(const T_VertexBufferObject vbo, const C_Vector2 & worldPos, float fAngle, float fScale, const C_Vector4 & color, const C_Point & clipLBCoord, const C_Point & clipAreaSize)
	{
		glPushMatrix();
		glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);

		glScissor(clipLBCoord.x, clipLBCoord.y, clipAreaSize.x, clipAreaSize.y);	//world coords
		glEnable(GL_SCISSOR_TEST);

		glTranslatef(worldPos.m_x, worldPos.m_y, 0.f);
		glScalef(fScale, fScale, 1.f);
		glRotatef(fAngle, 0.f, 0.f, 1.f);

		SetupBlendMode();

		glEnable(GL_TEXTURE_2D);
		if (m_Texture)
			m_Texture->BindTexture();

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);

		vbo->Render();

		glPopAttrib();
		glPopMatrix();

		/*C_Vector2 vctLB(clipLBCoord.x, 600 - clipLBCoord.y);
		C_Vector2 vctRT(clipLBCoord.x + clipAreaSize.x, 600 - (clipLBCoord.y + clipAreaSize.y));
		C_Vector4 color (1.f, 1.f, 1.f, 1.f);
		
		RenderRect(vctLB, vctRT, color, false);*/
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RenderVBO
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Graphic::RenderVBOElements(const T_VertexBufferObject vbo, u32 startIndex, const sysmath::C_Vector2 & worldPos, float fAngle, float fScale, const sysmath::C_Vector4 & color)
	{
		glPushMatrix();
		glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);

		glTranslatef(worldPos.m_x, worldPos.m_y, 0.f);
		glScalef(fScale, fScale, 1.f);
		glRotatef(fAngle, 0.f, 0.f, 1.f);

		SetupBlendMode();

		glEnable(GL_TEXTURE_2D);
		if (m_Texture)
			m_Texture->BindTexture();

		glColor4f(color.m_x, color.m_y, color.m_z, color.m_w);

		vbo->RenderQuadElement(startIndex, 2);

		glPopAttrib();
		glPopMatrix();
	}


}}