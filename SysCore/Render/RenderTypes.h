/*
* filename:			RenderTypes.h
*
* author:			Kral Jozef
* date:				03/10/2011		14:49
* version:			1.00
* brief:
*/

#pragma once

namespace scorpio{ namespace sysrender{

	enum E_BlendingMode
	{
		//E_BM_NONE = 0,					//Disabled blending
		E_BM_SRC_ALPHA__DST_ONE_MINUS_ALPHA = 0,		//Real transparency (SrcAlpha, 1-SrcAlpha)	//Y-Intense Y-Premultiply - Pyro
		E_BM_SRC_ONE__DST_ONE,			//Additive blending without considering alpha (1, 1)			//Y-Intense N-Premultiply - Pyro
		E_BM_SRC_ALPHA__DST_ONE,		//Additive blending (SrcAlpha, 1)									//N-Intense ????????????? - Pyro
	};

	enum E_TextureWrapMode
	{
		E_TWM_CLAMP = 0,
		E_TWM_REPEAT,
	};

	enum E_TextureFilteringMode
	{
		E_TFM_NEAREST = 0,
		E_TFM_LINEAR,
	};

	enum E_TextureInternalFormat
	{
		E_TIF_RGB = 0,
		E_TIF_RGBA,
	};


}}