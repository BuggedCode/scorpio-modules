/*
* filename:			C_Shader.h
*
* author:			Kral Jozef
* date:				11/6/2011		17:28
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysUtils/FileSystem/C_Stream.h>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Vector.h>

namespace scorpio{ namespace sysrender{

	class C_ShaderManager;

	//////////////////////////////////////////////////////////////////////////
	//@ C_Shader
	class SYSCORE_API C_Shader
	{
		friend class C_ShaderManager;
	public:
		//@ d-tor - public coz voost deleter
		~C_Shader();
		//@ Initialize
		static bool				InitializeExtensionFunctionPointers();
		//@ Load
		bool						Load(sysutils::T_Stream inputStream);
		//@ ResleaseResources
		bool						ReleaseResources();
		//@ RestoreResources
		bool						RestoreResources();
		//@ Bind
		void						Bind();
		//@ Unbind
		void						Unbind();
		//@ BindVariable
		s32						CreateUniformVariable(const char * variableName);
		//@ BindUniform
		void						BindUniform(s32 variableID, float value);
		void						BindUniform(s32 variableID, const sysmath::C_Vector2 & vct);
		void						BindUniform(s32 variableID, const sysmath::C_Vector & vct);

	private:
		//@ ParseShaderFile
		bool						ParseShaderFile(sysutils::T_Stream inputStream, sysutils::C_String & vertexShaderStringOut, sysutils::C_String & fragmentShaderStringOut);
		//@ BuildShader
		bool						BuildShader(const sysutils::C_String vertexShaderSrcString, const sysutils::C_String fragmentShaderSrcString);

	private:
		//@ c-tor
		C_Shader();


	private:
#ifdef PLATFORM_MACOSX
		void *		m_VertexShaderObj;	//GhandleARB GLuint
		void *		m_FragmentShaderObj;	//GhandleARB GLuint
		void *		m_ProgramObject;		//GhandleARB GLuint
#elif defined PLATFORM_WIN32
		u32			m_VertexShaderObj;	//GhandleARB GLuint
		u32			m_FragmentShaderObj;	//GhandleARB GLuint
		u32			m_ProgramObject;		//GhandleARB GLuint
#endif

		sysutils::C_String		m_VertexShaderString;	//store buffer for shader src string (need when switching RC -> rebuild of shaders)
		sysutils::C_String		m_FragmentShaderString;	//store buffer for shader src string (need when switching RC -> rebuild of shaders)
	};

	DECLARE_SHARED_PTR(C_Shader, T_Shader);
}}
