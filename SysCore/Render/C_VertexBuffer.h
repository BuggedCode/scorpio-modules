/*
* filename:			C_VertexBuffer.h
*
* author:			Kral Jozef
* date:				1/10/2012		19:50
* version:			1.00
* brief:
*/

#pragma once

#include "I_VertexBuffer.h"
#include <SysCore/SysCoreApi.h>
#include <boost/shared_ptr.hpp>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Vector4.h>
#include "C_Vertex.h"

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//@ C_VertexBuffer
	class SYSCORE_API C_VertexBuffer : public I_VertexBuffer
	{
		enum E_AccesFlag
		{
			E_AF_READONLY = 0,
			E_AF_REAWRITE,
		};


	public:
		//@ c-tor
		C_VertexBuffer();
		//@ d-tor
		~C_VertexBuffer();
		//@ CreateFrom
		void					CreateFromRect(const sysmath::C_Vector2 & size, sysmath::C_Vector4 & color, float fZpos);
		//@ GetVertexFormat
		virtual u32			GetVertexFormat() const { return m_VertexFormat; }
		//@ GetVerteSize
		virtual u32			GetVertexSize() const { return m_Stride; }
		//@ GetData
		virtual u8		*	GetData() const { return m_Data; }
		//@ GetCount
		virtual u32			GetCount() const { return m_NumVertices; }

		//@ GetVertexData
		void					GetVertexData(C_Vertex & outVertex, u32 vertexIndex);
		//@ SetVertexData
		void					SetVertexData(const C_Vertex & inVertex, u32 vertexIndex);

	private:
		//@ SetCOlor to VB data array
		void					SetColorData(u8 * dstData, const sysmath::C_Vector4 & color);

	private:
		E_AccesFlag			m_Flag;	//acces flag
		u32					m_NumVertices;
		u32					m_VertexFormat;
		u8						m_Stride;
		u8					*	m_Data;

	};

	typedef boost::shared_ptr<C_VertexBuffer>	T_VertexBuffer;
}}
