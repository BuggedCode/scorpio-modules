/*
* filename:			C_GridMesh.h
*
* author:			Kral Jozef
* date:				10/25/2011		0:28
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <SysMath/C_Vector2.h>
#include <SysCore/Render/C_Texture.h>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//@ C_GridMesh
	class SYSCORE_API C_GridMesh
	{
	public:
		//@ c-tor
		C_GridMesh(u32 nWidth, u32 nHeight, u8 xCells, u8 yCells);
		//@ d-tor
		~C_GridMesh();
		//@ get operator
		sysmath::C_Vector2	&	operator[](u32 index) { SC_ASSERT(index < m_Size); return m_Vertices[index]; }
		//@ Render - texture has to be bind already
		void							Render(T_Texture texture);

		//@ GetXCells
		u8								GetXCells() const { return m_XCells; }
		//@ GetYCells
		u8								GetYCells() const { return m_YCells; }
		//@ ToggleDebugDraw
		void							SetDebugDraw(bool bEnable) { m_bDebugDraw = bEnable; }

	private:
		//@ DebugDraw
		void							DebugDraw();

	private:
		u32							m_Width;
		u32							m_Height;
		u32							m_Size;	//CellsCount
		u8								m_XCells;
		u8								m_YCells;
		sysmath::C_Vector2	*	m_Vertices;
		sysmath::C_Vector2	*	m_TexCoords;
		bool							m_bDebugDraw;
	};


}}
