/*
* filename:			C_VertexBuffer.cpp
*
* author:			Kral Jozef
* date:				1/10/2012		19:51
* version:			1.00
* brief:
*/

#include "C_VertexBuffer.h"
#include <SysMath/C_Vector.h>
#include <Common/macros.h>

namespace scorpio{ namespace sysrender{


	using namespace sysmath;

	static const u8 C_SIZE_OF_FLOAT = sizeof(float);

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_VertexBuffer::C_VertexBuffer()
	{
		m_VertexFormat = VF_UNDEFINED;
		m_Stride = 0;
		m_Data = NULL;
		m_NumVertices = 0;
		m_Flag = E_AF_READONLY;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_VertexBuffer::~C_VertexBuffer()
	{
		delete[] m_Data;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetVertexData
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBuffer::GetVertexData(C_Vertex & outVertex, u32 vertexIndex)
	{
		if (vertexIndex > m_NumVertices)
		{
			SC_ASSERT(false);
			return;
		}

		u32 pos = m_Stride * vertexIndex;
		outVertex.m_Position = (*(C_Vector3*)&m_Data[pos]);

		outVertex.m_Color[0] = m_Data[pos + 3*C_SIZE_OF_FLOAT + 0];
		outVertex.m_Color[1] = m_Data[pos + 3*C_SIZE_OF_FLOAT + 1];
		outVertex.m_Color[2] = m_Data[pos + 3*C_SIZE_OF_FLOAT + 2];
		outVertex.m_Color[3] = m_Data[pos + 3*C_SIZE_OF_FLOAT + 3];

		outVertex.m_TexCoord = (*(C_Vector2*)&m_Data[pos + 3*C_SIZE_OF_FLOAT + 4]);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetVertexData
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBuffer::SetVertexData(const C_Vertex & inVertex, u32 vertexIndex)
	{
		if (vertexIndex > m_NumVertices)
		{
			SC_ASSERT(false);
			return;
		}

		u32 pos = m_Stride * vertexIndex;
		(*(C_Vector3*)&m_Data[pos]) = inVertex.m_Position;
		//@ TODO
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetColorData
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBuffer::SetColorData(u8 * dstData, const sysmath::C_Vector4 & color)
	{
		dstData[0] = (u8)RemapValue((float)color.m_x, 0.f, 1.f, 0.f, 255.f);
		dstData[1] = (u8)RemapValue((float)color.m_y, 0.f, 1.f, 0.f, 255.f);
		dstData[2] = (u8)RemapValue((float)color.m_z, 0.f, 1.f, 0.f, 255.f);
		dstData[3] = (u8)RemapValue((float)color.m_w, 0.f, 1.f, 0.f, 255.f);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateFrom2DRect
	//
	//////////////////////////////////////////////////////////////////////////
	void C_VertexBuffer::CreateFromRect(const C_Vector2 & size, C_Vector4 & color, float fZpos)
	{
		//@ 3*POS + !NORMAL + RGBA in 1 float + 2*TEX_COORD
		SC_ASSERT(sizeof(float) == 4);
		m_Stride = 3*C_SIZE_OF_FLOAT + 4 + 2*C_SIZE_OF_FLOAT;
		m_Data = new u8[4 * m_Stride];

		u32 startId = 0;
		//-------------------------------------------------------------------------
		//@ VERTEX 0
		(*(C_Vector3*)&m_Data[startId]) = C_Vector(-size.m_x / 2.f, +size.m_y / 2.f, fZpos);	//@ POSITION
		SetColorData(&m_Data[startId + 3*C_SIZE_OF_FLOAT], color);										//@ DIFFUSE COLOR
		(*(C_Vector2*)&m_Data[startId + 3*C_SIZE_OF_FLOAT + 4]) = C_Vector2(0.f, 1.f);			//@ TEXTURE COORDINATE
		startId += m_Stride;

		//-------------------------------------------------------------------------
		//@ VERTEX 1
		(*(C_Vector3*)&m_Data[startId]) = C_Vector(size.m_x / 2.f, +size.m_y / 2.f, fZpos);		//@ POSITION
		SetColorData(&m_Data[startId + 3*C_SIZE_OF_FLOAT], color);										//@ DIFFUSE COLOR
		(*(C_Vector2*)&m_Data[startId + 3*C_SIZE_OF_FLOAT + 4]) = C_Vector2(1.f, 1.f);			//@ TEXTURE COORDINATE
		startId += m_Stride;

		//-------------------------------------------------------------------------
		//@ VERTEX 2
		(*(C_Vector3*)&m_Data[startId]) = C_Vector(size.m_x / 2.f, -size.m_y / 2.f, fZpos);		//@ POSITION
		SetColorData(&m_Data[startId + 3*C_SIZE_OF_FLOAT], color);										//@ DIFFUSE COLOR
		(*(C_Vector2*)&m_Data[startId + 3*C_SIZE_OF_FLOAT + 4]) = C_Vector2(1.f, 0.f);			//@ TEXTURE COORDINATE
		startId += m_Stride;

		//-------------------------------------------------------------------------
		//@ VERTEX 3
		(*(C_Vector3*)&m_Data[startId]) = C_Vector(-size.m_x / 2.f, -size.m_y / 2.f, fZpos);	//@ POSITION
		SetColorData(&m_Data[startId + 3*C_SIZE_OF_FLOAT], color);										//@ DIFFUSE COLOR
		(*(C_Vector2*)&m_Data[startId + 3*C_SIZE_OF_FLOAT + 4]) = C_Vector2(0.f, 0.f);			//@ TEXTURE COORDINATE

		m_NumVertices = 4;
		m_VertexFormat = VF_POSITION | VF_DIFFUSE | VF_TEXTURE0;
		return;
	}

}}