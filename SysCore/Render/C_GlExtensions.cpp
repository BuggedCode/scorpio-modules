/*
* filename:			C_GlExtensions.cpp
*
* author:			Kral Jozef
* date:				02/24/2011		13:30
* version:			1.00
* brief:
*/

#include "C_GlExtensions.h"
#include <SysUtils/C_String.h>
#include <boost/tokenizer.hpp>
#include <string>
#ifdef PLATFORM_WIN32
   #include <Windows.h>
   #include <gl/GL.h>
   #include <gl/glext.h>
#elif defined PLATFORM_MACOSX
   #include <OpenGL/gl.h>
#endif

namespace scorpio{ namespace sysrender{

	using namespace sysutils;
   
  	static bool CheckExtension(const char * extString);


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CheckExtension
	//
	//////////////////////////////////////////////////////////////////////////
	bool CheckExtension(const char * extString)
	{
		const char * extList = ( const char *) glGetString( GL_EXTENSIONS );

		if (!extString || !extList)
			return false;

		typedef boost::tokenizer<boost::char_separator<char> > T_Tokenizer;
		boost::char_separator<char> separ(" ");
		std::string tmpStr(extList);
		T_Tokenizer tokenizer(tmpStr, separ);

		for (T_Tokenizer::iterator iter = tokenizer.begin(); iter != tokenizer.end(); ++iter)
		{
			T_Tokenizer::iterator tmpIter = iter;
			C_String str = iter->c_str();
			if (str == extString)
				return true;
		}

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsExtSupported
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_GlExtensions::IsExtSupported(E_GlExtension ext)
	{
		switch (ext)
		{
			case GL_EXT_RECTANGLE_TEXTURE:
				if (CheckExtension("GL_ARB_texture_rectangle"))
					return true;

				if (CheckExtension("GL_NV_texture_rectangle"))
					return true;
				return true;
			break;

			case GL_EXT_FRAME_BUFFER_OBJECT:
				if (CheckExtension("GL_EXT_framebuffer_object"))
					return true;
				break;

			case GL_GLSL_100:
				{
					bool bRes = CheckExtension("GL_ARB_shader_objects");
					bRes &= CheckExtension("GL_ARB_shading_language_100");
					bRes &= CheckExtension("GL_ARB_vertex_shader");
					bRes &= CheckExtension("GL_ARB_fragment_shader");
					return bRes;
				}
				break;

			case GL_ARB_VERTEX_BUFFER_OBJECT:
				{
					if (CheckExtension("GL_ARB_vertex_buffer_object"))
						return true;
				}
			default:
				return false;
		}

		return false;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetVersion
	//
	//////////////////////////////////////////////////////////////////////////
	const char * C_GlExtensions::GetVersion()
	{
		const char * extList = ( const char *) glGetString( GL_VERSION );

		return extList;
	}



}}