/*
* filename:			C_VertexBufferManager.h
*
* author:			Kral Jozef
* date:				1/30/2012		18:32
* version:			1.00
* brief:
*/

#pragma once

#include <Common/C_Singleton.h>
#include <SysCore/SysCoreApi.h>
#include <SysCore/Render/C_VertexBufferObject.h>
#include <map>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//@ C_VertexBufferManager
	class SYSCORE_API C_VertexBufferManager
	{
		friend class C_Singleton<C_VertexBufferManager>;
	public:
		//@ Initialize
		bool							Initialize();

		//@ GenerateVertexBuffer
		T_VertexBufferObject		GenerateVertexBufferObject();
		//@ Remove
		bool							Remove(const T_VertexBufferObject vbo);

	private:
		//@ c-tor
		C_VertexBufferManager() {};
		//@ d-tor
		~C_VertexBufferManager();

		typedef std::map<T_Hash32, T_VertexBufferObject>	T_VertexBufferMap;
		T_VertexBufferMap			m_Container;

	};


	typedef C_Singleton<C_VertexBufferManager>	T_VertexBufferManager;

}}

SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::sysrender::C_VertexBufferManager>;