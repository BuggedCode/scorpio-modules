/*
* filename:			C_FrameBufferManager.cpp
*
* author:			Kral Jozef
* date:				1/4/2012		18:03
* version:			1.00
* brief:
*/

#include "C_FrameBufferManager.h"
#include <SysCore/Render/C_GlExtensions.h>
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_FrameBufferManager::~C_FrameBufferManager()
	{
		for (u32 i = 0; i < E_RT_LAST; ++i)
		{
			SC_ASSERT(m_RenderTargetList[i].unique());
			m_RenderTargetList[i]->Destroy();
			m_RenderTargetList[i] = T_FrameBuffer();
		}	
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FrameBufferManager::Initialize(u32 width, u32 height)
	{
		bool bRes = C_GlExtensions::IsExtSupported(C_GlExtensions::GL_EXT_FRAME_BUFFER_OBJECT);
		if (!bRes)
		{
			TRACE_E("Unsupported extension: GL_EXT_framebuffer_object");
			return false;
		}

		bRes = C_FrameBuffer::InitializeExtensionFunctionPointers();

		T_FrameBuffer fbMain = T_FrameBuffer(new C_FrameBuffer());
		fbMain->Create(width, height);
		m_RenderTargetList[E_RT_MAIN] = fbMain;

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FrameBufferManager::ReleaseInternalResources()
	{
		bool bRes = true;
		for (u32 i = 0; i < E_RT_LAST; ++i)
			bRes &= m_RenderTargetList[i]->ReleaseResources();

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_FrameBufferManager::RestoreInternalResources()
	{
		bool bRes = true;
		for (u32 i = 0; i < E_RT_LAST; ++i)
			bRes &= m_RenderTargetList[i]->RestoreResources();

		return bRes;
	}

}}
