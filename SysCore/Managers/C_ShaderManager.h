/*
* filename:			C_ShaderManager.h
*
* author:			Kral Jozef
* date:				11/7/2011		14:11
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <Common/C_Singleton.h>
#include <SysUtils/C_HashName.h>
#include <SysUtils/FileSystem/C_Stream.h>
#include <map>
#include <SysCore/Render/C_Shader.h>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	// C_ShaderManager
	class SYSCORE_API C_ShaderManager
	{
		friend class C_Singleton<C_ShaderManager>;
	public:
		//@ Initialize
		bool						Initialize();
		//@ LoadShaders
		bool						LoadShaders(const sysutils::T_Stream inputStream);
		//@ GetShader
		T_Shader					GetShader(const sysutils::C_HashName & shaderUID);
		//@ ReleaseInternalResources
		bool						ReleaseInternalResources();
		//@ RestoreInternalResources - after fullscreen change
		bool						RestoreInternalResources();
	private:
		//@ c-tor
		C_ShaderManager() {};
		//@ d-tor
		~C_ShaderManager();


		typedef std::map<sysutils::C_HashName, T_Shader>	T_ShaderMap;
		T_ShaderMap				m_Container;

	};


	typedef C_Singleton<C_ShaderManager>	T_ShaderManager;
}}

SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::sysrender::C_ShaderManager>;
