/*
* filename:			C_VertexBufferManager.cpp
*
* author:			Kral Jozef
* date:				1/30/2012		21:07
* version:			1.00
* brief:
*/

#include "C_VertexBufferManager.h"
#include <SysCore/Render/C_VertexBufferObject.h>
#include <SysCore/Render/C_GlExtensions.h>
#include <SysUtils/C_TraceClient.h>

namespace scorpio{ namespace sysrender{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_VertexBufferManager::~C_VertexBufferManager()
	{
		m_Container.clear();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_VertexBufferManager::Initialize()
	{
		//@ check extensions
		bool bRes = C_GlExtensions::IsExtSupported(C_GlExtensions::GL_ARB_VERTEX_BUFFER_OBJECT);
		if (!bRes)
		{
			TRACE_E("Unsupported extension: VertexBufferObjects");
			return false;
		}

		bRes = C_VertexBufferObject::InitializeExtensionFunctionPointers();
		if (!bRes)
		{
			TRACE_E("Couldn't initialize VBO Extensions")
				return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GenerateVertexBufferObject
	//
	//////////////////////////////////////////////////////////////////////////
	T_VertexBufferObject C_VertexBufferManager::GenerateVertexBufferObject()
	{
		T_VertexBufferObject vbo = T_VertexBufferObject(new C_VertexBufferObject());
		T_Hash32 hash = vbo->GetUID();
		std::pair<T_VertexBufferMap::iterator, bool> res = m_Container.insert(T_VertexBufferMap::value_type(hash, vbo));
		SC_ASSERT(res.second);

		return vbo;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_VertexBufferManager::Remove(const T_VertexBufferObject vbo)
	{
		size_t nCount = m_Container.erase(vbo->GetUID());
		return nCount == 1;
	}


}}
