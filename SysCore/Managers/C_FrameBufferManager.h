/*
* filename:			C_FrameBufferManager.h
*
* author:			Kral Jozef
* date:				1/4/2012		18:01
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <Common/C_Singleton.h>
#include <SysCore/Render/C_FrameBuffer.h>

namespace scorpio{ namespace sysrender{

	enum E_RenderTargetType
	{
		E_RT_MAIN = 0,
		E_RT_LAST,	//must be LAST!! as a array len def
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_FrameBufferManager
	class SYSCORE_API C_FrameBufferManager
	{
		friend class C_Singleton<C_FrameBufferManager>;
	public:
		//@ Initialize
		//@ width, height -> window resolution 
		bool						Initialize(u32 width, u32 height);
		//@ ReleaseInternalResources
		bool						ReleaseInternalResources();
		//@ RestoreInternalResources - after fullscreen change
		bool						RestoreInternalResources();
		//@ GetRenderTarget
		T_FrameBuffer			GetRenderTarget(E_RenderTargetType rtType)
		{
			T_FrameBuffer fb = m_RenderTargetList[rtType];
			SC_ASSERT(fb);
			return fb;
		}

	private:
		//@ c-tor
		C_FrameBufferManager() {};
		//@ d-tor
		~C_FrameBufferManager();

		T_FrameBuffer	m_RenderTargetList[E_RT_LAST];
	};


	typedef C_Singleton<C_FrameBufferManager>	T_FrameBufferManager;
}}


SYSCORE_EXP_TEMP_INST template class SYSCORE_API C_Singleton<scorpio::sysrender::C_FrameBufferManager>;
