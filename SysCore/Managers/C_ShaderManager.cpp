/*
* filename:			C_ShaderManager.cpp
*
* author:			Kral Jozef
* date:				11/7/2011		14:12
* version:			1.00
* brief:
*/

#include <SysCore/Managers/C_ShaderManager.h>
#include <SysCore/Render/C_Shader.h>
#include <SysUtils/Xml/C_XmlDocument.h>
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysCore/Render/C_GlExtensions.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace sysrender{

	using namespace scorpio::sysutils;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_ShaderManager::~C_ShaderManager()
	{
		m_Container.clear();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ShaderManager::Initialize()
	{
		//@ check extensions
		bool bRes = C_GlExtensions::IsExtSupported(C_GlExtensions::GL_GLSL_100);
		if (!bRes)
		{
			TRACE_E("Unsupported extension: GL_GLSL_100");
			return false;
		}

		bRes = C_Shader::InitializeExtensionFunctionPointers();
		if (!bRes)
		{
			TRACE_E("Couldn't initialize SHADER SYSTEM")
			return false;
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadShaders
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ShaderManager::LoadShaders(const T_Stream inputStream)
	{
		//naparsujem xml a povytvaram templaty aj zinicializujem
		C_XmlDocument xmlDoc;
		bool bRes = xmlDoc.Load(inputStream);
		if (!bRes)
			return false;


		T_XmlNodeList nodeList = xmlDoc.SelectNodes("shader_list/shader");
		for (T_ConstXmlNodePtrVector::iterator iter = nodeList->begin(); iter != nodeList->end(); ++iter)
		{
			const C_XmlNode * node = *iter;
			C_String strUID = (*node)["uid"];
			C_String strFile = (*node)["file"];

			T_Shader shader = T_Shader(new C_Shader());
			T_Stream stream = T_FileSystemManager::GetInstance().Open(strFile);
			if (!stream)
			{
				TRACE_FE("Unable to open shader file: %s", strFile.c_str());
				continue;
			}
			bool bLocRes = shader->Load(stream);
			if (!bLocRes)
			{
				TRACE_FE("Unable to load shader: %s", strFile.c_str());
				continue;
			}
			
			//////////////////////////////////////////////////////////////////////////
			C_HashName hash(strUID);
			std::pair<T_ShaderMap::iterator, bool> bRes = m_Container.insert(T_ShaderMap::value_type(hash, shader));
			if (!bRes.second)
			{
				TRACE_FE("Duplicated shader: %s", strFile.c_str());
				continue;
			}

			TRACE_FI("Shader: %s CREATED", strUID.c_str());
		}


		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetShader
	//
	//////////////////////////////////////////////////////////////////////////
	T_Shader C_ShaderManager::GetShader(const C_HashName & shaderUID)
	{
		T_ShaderMap::iterator iter = m_Container.find(shaderUID);
		if (iter == m_Container.end())
		{
			TRACE_FE("Unknown shader: %s", shaderUID.GetName());
			return T_Shader();
		}

		return iter->second;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ShaderManager::ReleaseInternalResources()
	{
		bool bRes = true;
		for (T_ShaderMap::iterator iter = m_Container.begin(); iter != m_Container.end(); ++iter)
			bRes &= iter->second->ReleaseResources();
		
		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ShaderManager::RestoreInternalResources()
	{
		bool bRes = true;
		for (T_ShaderMap::iterator iter = m_Container.begin(); iter != m_Container.end(); ++iter)
			bRes &= iter->second->RestoreResources();

		return bRes;
	}
}}
