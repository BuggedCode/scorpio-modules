/*
* filename:			C_SDLWindow.h
*
* author:			Kral Jozef
* date:				02/16/2011		16:40
* version:			1.00
* brief:				SDL Window wrapper
*/

#pragma once

#include <Common/PlatformDef.h>

#ifdef PLATFORM_WIN32
	#include <SDL/include/SDL_video.h>
#elif defined PLATFORM_MACOSX
	#include <SDL/SDL_video.h>
#endif

#include <SysCore/SysCoreApi.h>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Vector2.h>
#include <SysUtils/C_String.h>


namespace scorpio{ namespace syscore{

	//# forward
	class C_AppWindow;

	//////////////////////////////////////////////////////////////////////////
	//@ C_SDLWindow
	class SYSCORE_API C_SDLWindow
	{
		typedef u8	T_RendererType;

	public:
		//@ c-tor
		C_SDLWindow() {};
		//@ d-tor
		~C_SDLWindow() {};


		bool		createGameWindow(short width, short height, short depth, bool windowed, const char *windowTitle);
		void		setDefaultWorldView( void );
		void		setWorldView( float translateX, float translateY, float rotation , float zoom, bool clearworld );
		void		setClearColor(const sysmath::C_Vector4 & vctColor) { m_ClearColor = vctColor; }

		bool		getFullScreenState( void );
		bool		toggleFullScreen( bool fullscreen );
		//bool		hasFocus();
		void		flipBackBuffer( bool	waitForBackWindow, bool restoreview);
		void		processEvents(C_AppWindow * appWindow);
		/*bool		isQuit( void ) { return false; }
		bool		setQuit( bool quitstate );
		//JKHWND		getWindowHandle(void );
		void		setMaxFrameRate( long desiredFrameRate );
		bool		getRectangleTexCap( void );		//returns true if rectangle is possible
		bool		getAccelerationCap( void );		//returns true if accelerated
		void		setRectangleTexCap( bool	forceRectangleCap );
		void		setGamma( float	gammai );
		long		getWindowWidth( void );
		long		getWindowHeight( void );
		void		restore( void );
		void		minimize( void );
		void		displayMouse( bool mousestate );
		bool		saveBackBuffer( const char * cpcFileName , long	imageFormat = 0 , long resizeW=0 , long resizeH=0 );
		void		setClipping( long x1, long y1, long x2 , long y2 );

		//callback  for the window  ( differs on  mac )
		//JKvoid		setCallBack( ptkWindowCallBack  userProc );

		//JKvoid		enumerateDisplays( ptkEnumDisplayCallBack  functionPtr );
		void		showWindow( bool show );
		bool		isWindowVisible( void );*/

		//@ Delay
		//@ time - delay time in ms
		void		Delay(u32 time);
		//@ Push/Pop Transform matrix pair functions
		void		PushAndTransform(const sysmath::C_Vector2 & vctTransl, float fRotAngle, float fScale);
		void		PopTransform();

	private:
		SDL_Surface			*	m_Screen;
		sysmath::C_Vector4	m_ClearColor;
		sysutils::C_String	m_WndCaption;
	};

}}
