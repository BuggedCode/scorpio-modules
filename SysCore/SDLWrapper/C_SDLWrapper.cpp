/*
* filename:			C_SDLWrapper.cpp
*
* author:			Kral Jozef
* date:				02/16/2011		16:23
* version:			1.00
* brief:
*/

#include "C_SDLWrapper.h"
#include <Common/PlatformDef.h>

#ifdef PLATFORM_WIN32
	#include <SDL/include/SDL.h>
#elif defined PLATFORM_MACOSX
	#include <SDL/SDL.h>
#endif


#include <SysUtils/C_TraceClient.h>
#include <SysUtils/C_String.h>
//#include <SDL_ttf.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{


	using namespace sysutils;


	//////////////////////////////////////////////////////////////////////////
	bool C_SDLWrapper::Initialize()
	{
		SDL_version version;
		SDL_VERSION(&version);

		TRACE_FI("#FF5599 SDL version: %d.%d.%d", version.major, version.minor, version.patch);

		if (SDL_Init(/*SDL_INIT_EVERYTHING*/SDL_INIT_VIDEO | SDL_INIT_TIMER /*SDL_INIT_EVENTTHREAD*/) == -1)
		{
			C_String msg = SDL_GetError();
			TRACE_FE("SDL Initialization Error: %s", msg.c_str());
			return false;
		}

		/*if (TTF_Init() == -1)
		{
			C_String msg = SDL_GetError();
			TRACE_FE("SDL Initialization Error: %s", msg);
			return false;
		}*/

		return true;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Deinitialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_SDLWrapper::Deinitialize()
	{
		//TTF_Quit();

		SDL_Quit();
		return true;
	}
}}