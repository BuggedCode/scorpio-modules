/*
* filename:			C_SDLWindow.cpp
*
* author:			Kral Jozef
* date:				02/16/2011		16:47
* version:			1.00
* brief:
*/

#include "C_SDLWindow.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/C_String.h>
#include "Globals.h"
#include <SysCore/Framework/C_AppWindow.h>

#ifdef PLATFORM_WIN32
	#include <SDL/include/SDL.h>
	#include <SDL/include/SDL_opengl.h>
#elif defined PLATFORM_MACOSX
	#include <SDL/SDL.h>
	#include <SDL/SDL_opengl.h>
#endif

#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ createGameWindow
	bool C_SDLWindow::createGameWindow(short width, short height, short depth, bool windowed, const char *windowTitle)
	{
		TRACE_I("CreateSDLGameWindow")
		m_WndCaption = windowTitle;
		
		SDL_WM_SetCaption(m_WndCaption, NULL);
		/*u32 res = */SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 0);

		// nepouzitelne delay pri close application a vsetko ostatne!! SDL_EventState(SDL_SYSWMEVENT, SDL_ENABLE);
		m_Screen = SDL_SetVideoMode(width, height, depth, (windowed ? 0 : SDL_FULLSCREEN ) | SDL_OPENGL);

		char name[256]; 

		SDL_VideoDriverName((char*)name, 256);
		TRACE_FI("Video Driver Name [%s]", name);

		SDL_PumpEvents();

		//@ Init OpenGl
		glClearColor(m_ClearColor.m_x, m_ClearColor.m_y, m_ClearColor.m_z, m_ClearColor.m_w);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, m_Screen->w, m_Screen->h, 0, -1000, 1);	//Z is from the scree positive (but in glOrtho negative is what is behinf camera)
		glViewport(0, 0, m_Screen->w, m_Screen->h);

		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_CULL_FACE);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glFinish();

		TRACE_I("CreateSDLGameWindow - Done")
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ Delay
	void C_SDLWindow::Delay(u32 time)
	{
		SDL_Delay(time);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ setDefaultWorldView
	void C_SDLWindow::setDefaultWorldView( void )
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}

	//////////////////////////////////////////////////////////////////////////
	//@ setWorldView
	void C_SDLWindow::setWorldView( float translateX, float translateY, float rotation , float zoom, bool clearworld )
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glScalef(zoom, zoom, 1.f);
		glRotatef(rotation, 0.f, 0.f, 1.f);
		glTranslatef(translateX, translateY, 0.f);

		if (clearworld)
		{
			glClearColor(m_ClearColor.m_x, m_ClearColor.m_y, m_ClearColor.m_z, m_ClearColor.m_w);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
	}

	bool C_SDLWindow::toggleFullScreen(bool fullscreen)
	{ 
		if (SDL_WM_ToggleFullScreen(m_Screen) == 0)
		{
			//TRACE_E("Can not toggle to Fullscreen and back");
			//m_Screen = SDL_SetVideoMode(m_Screen->w, m_Screen->h, m_Screen->format->BitsPerPixel, (fullscreen ? SDL_FULLSCREEN : 0) | SDL_OPENGL);
			createGameWindow(m_Screen->w, m_Screen->h, m_Screen->format->BitsPerPixel, !fullscreen, m_WndCaption.c_str());
			return true;
		}
		return true; 
	}
	
	bool C_SDLWindow::getFullScreenState( void ) { return ((m_Screen->flags & SDL_FULLSCREEN) == SDL_FULLSCREEN); }

	//////////////////////////////////////////////////////////////////////////
	//
	//@ flipBackBuffer
	//
	//////////////////////////////////////////////////////////////////////////
	void C_SDLWindow::flipBackBuffer(bool waitForBackWindow, bool restoreview)
	{
		SDL_GL_SwapBuffers();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ PushAndTransform
	//
	//////////////////////////////////////////////////////////////////////////
	void C_SDLWindow::PushAndTransform(const sysmath::C_Vector2 & vctTransl, float fRotAngle, float fScale)
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();

		glTranslatef(vctTransl.m_x, vctTransl.m_y, 0.f);
		glRotatef(fRotAngle, 0.f, 0.f, 1.f);
		glScalef(fScale, fScale, 1.f);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ PopTransform
	void C_SDLWindow::PopTransform()
	{
		glPopMatrix();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ processEvents
	//
	//////////////////////////////////////////////////////////////////////////
	void C_SDLWindow::processEvents(C_AppWindow * appWindow)
	{
		SDL_Event event;
		while(SDL_PollEvent(&event))
		{
			/*bool bRes = */appWindow->HandleEvent(event);
			//if (!bRes)
			//	return;
		}
	}

}}