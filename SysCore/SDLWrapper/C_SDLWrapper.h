/*
* filename:			C_SDLWrapper.h
*
* author:			Kral Jozef
* date:				02/16/2011		16:21
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	// C_SDLWrapper
	class SYSCORE_API C_SDLWrapper
	{
	public:

		//@ Initialize
		static bool			Initialize();
		//@ Deinitialize
		static bool			Deinitialize();
	};

}}