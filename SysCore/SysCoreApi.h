#pragma once

#include <Common/Pragmas.h>

#ifdef _USRDLL
	#ifdef SYSCORE_EXPORTS
		#define SYSCORE_API __declspec(dllexport)
		#define SYSCORE_EXP_TEMP_INST
	#else
		#define SYSCORE_API __declspec(dllimport)
		#define SYSCORE_EXP_TEMP_INST extern
	#endif
#else
	#define SYSCORE_API
	#define SYSCORE_EXP_TEMP_INST
#endif
	

#ifdef USE_BOOST_SMARTPTR
	#undef DECLARE_SHARED_PTR
	#define	DECLARE_SHARED_PTR(ClassType, ClassName) \
		typedef boost::shared_ptr<ClassType>	ClassName;
#else
	#undef DECLARE_SHARED_PTR
	#define	DECLARE_SHARED_PTR(ClassType, ClassName) \
		typedef C_SharedPtr<ClassType>			ClassName;
#endif