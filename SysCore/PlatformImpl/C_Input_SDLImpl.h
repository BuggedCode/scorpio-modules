/*
* filename:			C_Input_SDLImpl.h
*
* author:			Kral Jozef
* date:				2/6/2012		16:26
* version:			1.00
* brief:
*/


#pragma once

#include <SysMath/C_Point.h>
#include <SysCore/Framework/FrameworkTypes.h>

namespace scorpio{ namespace syscore{


	//////////////////////////////////////////////////////////////////////////
	//@ C_Input_SDLImpl
	class C_Input_SDLImpl
	{
	public:
		//@ IsPressed
		//static bool			IsPressed(E_KeyboardLayout key);
		static bool			IsPressed(E_KeyboardModifier keyMod);
	};


}}