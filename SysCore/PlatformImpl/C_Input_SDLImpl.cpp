/*
* filename:			C_Input_SDLImpl.cpp
*
* author:			Kral Jozef
* date:				2/6/2012		16:24
* version:			1.00
* brief:
*/

#include <Common/PlatformDef.h>
#include "C_Input_SDLImpl.h"

#ifdef PLATFORM_WIN32
	#include <SDL/include/SDL_mouse.h>
#elif defined PLATFORM_MACOSX
	#include <SDL/SDL_mouse.h>
#endif

#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsPressed
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Input_SDLImpl::IsPressed(E_KeyboardModifier keyMod)
	{
		SDLMod mod = SDL_GetModState();
		if ((mod & keyMod) == keyMod)
			return true;

		if (keyMod == E_KM_SHIFT)
		{
			if ((mod & keyMod) == E_KM_LSHIFT || (mod & keyMod) == E_KM_RSHIFT)
				return true;
		}
		else if (keyMod == E_KM_CTRL)
		{
			if ((mod & keyMod) == E_KM_LCTRL || (mod & keyMod) == E_KM_RCTRL)
				return true;
		}
		else if (keyMod == E_KM_ALT)
		{
			if ((mod & keyMod) == E_KM_LALT || (mod & keyMod) == E_KM_RALT)
				return true;
		}

		return false;
	}

}}
