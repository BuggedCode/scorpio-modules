/*
* filename:			C_WinGLESWindow.cpp
*
* author:			Kral Jozef
* date:				2/2/2012		14:44
* version:			1.00
* brief:
*/

#include "C_WinGLESWindow.h"

#ifdef RENDER_WIN_GLES_20

#include <SysUtils/C_TraceClient.h>
#include <SysCore/Framework/C_AppWindow.h>
#include <WindowsX.h>

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#pragma comment(lib, "libEGL")
#pragma comment(lib, "libGLESv2")

#elif defined RENDER_WIN_GLES_11

#include <EGL/egl.h>
#include <GLES1/gl1.h>
#pragma comment(lib, "libEGL")
#pragma comment(lib, "libGLESv1")

#endif


#if defined RENDER_WIN_GLES_20 || defined RENDER_WIN_GLES_11

namespace scorpio{ namespace syscore{

	static const char * C_SCORPIO_WND_CLASSNAME = "Scorpio Window ClassName";

	static C_WinGLESWindow * gWndInstance = NULL;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_WinGLESWindow::C_WinGLESWindow()
		: m_hWnd(NULL), m_hInstance(NULL), m_hDC(NULL), m_EventFired(false)
	{
		(/*(C_WinGLESWindow *)*/gWndInstance) = this;
	};

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_WinGLESWindow::~C_WinGLESWindow()
	{
		eglMakeCurrent( m_EglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT );
		eglTerminate( m_EglDisplay );

		// Release the device context.
		if(m_hDC)
			ReleaseDC(m_hWnd, m_hDC);

		DestroyWindow(m_hWnd);
		UnregisterClass( C_SCORPIO_WND_CLASSNAME, m_hInstance );
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ createGameWindow
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_WinGLESWindow::createGameWindow(short width, short height, short depth, bool windowed, const char *windowTitle)
	{
		m_hInstance = GetModuleHandle(0);
		// Register the windows class
		WNDCLASS winClass;
		winClass.style = CS_HREDRAW | CS_VREDRAW;
		winClass.lpfnWndProc = C_WinGLESWindow::WindowProc;
		winClass.cbClsExtra = 0;
		winClass.cbWndExtra = 0;
		winClass.hInstance = m_hInstance;
		winClass.hIcon = 0;
		winClass.hCursor = 0;
		winClass.lpszMenuName = 0;
		winClass.hbrBackground = ( HBRUSH ) GetStockObject( WHITE_BRUSH );
		winClass.lpszClassName = C_SCORPIO_WND_CLASSNAME;

		if( !RegisterClass(&winClass) )
		{
			TRACE_E("Failed to register the window class");
			return false;
		}

		// Create the g_eglWindow.
		RECT sRect;
		SetRect( &sRect, 0, 0, width, height );
		AdjustWindowRectEx( &sRect, WS_CAPTION | WS_SYSMENU, false, 0 );

		m_hWnd = CreateWindow( C_SCORPIO_WND_CLASSNAME, windowTitle, WS_VISIBLE | WS_SYSMENU, 0, 0, width, height, NULL, NULL, m_hInstance, NULL);
		if (!m_hWnd)
			return false;

		//@ InitGL
		if (!InitOpenGL())
			return false;

		::SetCursor(::LoadCursor(NULL, IDC_ARROW));

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitOpenGL
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_WinGLESWindow::InitOpenGL()
	{
		m_hDC = ::GetDC(m_hWnd);

		m_EglDisplay = eglGetDisplay(m_hDC);
		if( m_EglDisplay == EGL_NO_DISPLAY )
			m_EglDisplay = eglGetDisplay( ( EGLNativeDisplayType ) EGL_DEFAULT_DISPLAY );

		// Initialize EGL.
		EGLint iMajorVersion, iMinorVersion;
		if (!eglInitialize(m_EglDisplay, &iMajorVersion, &iMinorVersion))
		{
			TRACE_E("eglInitialize FAILED");
			return false;
		}

		// Specify the required configuration attributes.
		const EGLint attList[] = {	EGL_LEVEL, 0, EGL_SURFACE_TYPE, EGL_WINDOW_BIT, EGL_RENDERABLE_TYPE,	EGL_OPENGL_ES2_BIT, 
											EGL_NATIVE_RENDERABLE, EGL_FALSE, EGL_DEPTH_SIZE, EGL_DONT_CARE, EGL_NONE };

		EGLConfig config;
		s32 iConfigs;
		if (!eglChooseConfig(m_EglDisplay, attList, &config, 1, &iConfigs) || iConfigs != 1)
		{
			TRACE_E("eglChooseConfig FAILED");
			return false;
		}

		m_Surface = eglCreateWindowSurface(m_EglDisplay, config, m_hWnd, NULL);
		if( m_Surface == EGL_NO_SURFACE )
		{
			eglGetError(); // Clear error and try again.
			m_Surface = eglCreateWindowSurface( m_EglDisplay, config, NULL, NULL );
		}


		// Create a context.

		// Bind the API ( It could be OpenGLES or OpenVG )
		eglBindAPI( EGL_OPENGL_ES_API );

		EGLint ai32ContextAttribs[] =
		{
			EGL_CONTEXT_CLIENT_VERSION, 2,
			EGL_NONE
		};

		EGLContext eglContext = eglCreateContext( m_EglDisplay, config, NULL, ai32ContextAttribs );

		eglMakeCurrent(m_EglDisplay, m_Surface, m_Surface, eglContext);


		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ setDefaultWorldView
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WinGLESWindow::setDefaultWorldView( void )
	{
#ifdef RENDER_WIN_GLES_11
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
#endif
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ setWorldView
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WinGLESWindow::setWorldView( float translateX, float translateY, float rotation , float zoom, bool clearworld )
	{
#ifdef RENDER_WIN_GLES_11
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glScalef(zoom, zoom, 1.f);
		glRotatef(rotation, 0.f, 0.f, 1.f);
		glTranslatef(translateX, translateY, 0.f);

		if (clearworld)
		{
			glClearColor(m_ClearColor.m_x, m_ClearColor.m_y, m_ClearColor.m_z, m_ClearColor.m_w);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
#endif
	}
	
	//////////////////////////////////////////////////////////////////////////
	//
	//@ flipBackBuffer
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WinGLESWindow::flipBackBuffer( bool	waitForBackWindow, bool restoreview)
	{
		eglSwapBuffers(m_EglDisplay, m_Surface);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Delay
	//@ time - delay time in ms
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WinGLESWindow::Delay(u32 time)
	{
		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Push/Pop Transform matrix pair functions
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WinGLESWindow::PushAndTransform(const sysmath::C_Vector2 & vctTransl, float fRotAngle, float fScale)
	{
#ifdef RENDER_WIN_GLES_11
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();

		glTranslatef(vctTransl.m_x, vctTransl.m_y, 0.f);
		glRotatef(fRotAngle, 0.f, 0.f, 1.f);
		glScalef(fScale, fScale, 1.f);
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ PopTransform
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WinGLESWindow::PopTransform()
	{
#ifdef RENDER_WIN_GLES_11
		glPopMatrix();
#endif
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ WindowProc
	//
	//////////////////////////////////////////////////////////////////////////
	LRESULT CALLBACK C_WinGLESWindow::WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		bool bRes = gWndInstance->HandleWinMessages(hWnd, uMsg, wParam, lParam);
		if (bRes)
			return DefWindowProc( hWnd, uMsg, wParam, lParam );

		return 0;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ HandleWinMessages
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_WinGLESWindow::HandleWinMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		m_EventFired = true;
		switch( uMsg )
		{
			// Here we are handling 2 system messages: screen saving and monitor power.
			// They are especially relevant on mobile devices.
		case WM_SYSCOMMAND:
			{
				switch( wParam )
				{
				case SC_SCREENSAVE: // Screensaver trying to start ?
				case SC_MONITORPOWER: // Monitor trying to enter powersave ?
					return false; // Prevent this from happening
				}
				break;
			}

		case WM_CLOSE:
			{
				::PostQuitMessage(0);
				m_Event.type = SDL_QUIT;
				return false;
			}

		case WM_DESTROY:
			{
				::PostQuitMessage(0);
				m_Event.type = SDL_QUIT;
				return false;
			}
			break;

		case WM_MOUSEMOVE:
			m_Event.type = MOUSE_MOTION_EVENT;
			m_Event.motion.type = SDL_MOUSEMOTION;
			m_Event.motion.x = GET_X_LPARAM(lParam);
			m_Event.motion.y = GET_Y_LPARAM(lParam);
			return false;

		case WM_KEYDOWN:
			m_Event.type = KEY_DOWN_EVENT;
			m_Event.key.keysym.sym = TranslateToSDLCode(wParam);
			return false;

		case WM_KEYUP:
			m_Event.type = KEY_UP_EVENT;
			m_Event.key.keysym.sym = TranslateToSDLCode(wParam);
			return false;

		default:
			m_EventFired = false;
			return true;
			break;
		}

		m_EventFired = false;
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ProcessEvents
	//
	//////////////////////////////////////////////////////////////////////////
	void C_WinGLESWindow::processEvents(C_AppWindow * appWindow)
	{
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			DispatchMessage(&msg);
			if (m_EventFired)
				appWindow->HandleEvent(m_Event);
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TranslateToSDLCode
	//
	//////////////////////////////////////////////////////////////////////////
	SDLKey C_WinGLESWindow::TranslateToSDLCode(WPARAM keyCode)
	{
		u32 code = (u32)keyCode;
		//@ convert F1 - F12
		if (code >= 112 && code <= 123)
			code += 170;

		return (SDLKey)code;
	}


}}

#endif