/*
* filename:			C_WinGLESWindow.h
*
* author:			Kral Jozef
* date:				2/2/2012		14:41
* version:			1.00
* brief:
*/

#pragma once

#include <Common/PlatformDef.h>
#include <Common/macros.h>

#ifdef RENDER_WIN_GLES_20

#include <windows.h>
#include <SysCore/SysCoreApi.h>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Vector2.h>
#include <SysUtils/C_String.h>
#include <SysCore/Framework/FrameworkTypes.h>


namespace scorpio{ namespace syscore{

	//# forward
	class C_AppWindow;

	//////////////////////////////////////////////////////////////////////////
	//@ C_WinGLESWindow
	class SYSCORE_API C_WinGLESWindow
	{
	public:
		//@ c-tor
		C_WinGLESWindow();
		//@ d-tor
		~C_WinGLESWindow();


		bool		createGameWindow(short width, short height, short depth, bool windowed, const char *windowTitle);
		void		setDefaultWorldView( void );
		void		setWorldView( float translateX, float translateY, float rotation , float zoom, bool clearworld );
		void		setClearColor(const sysmath::C_Vector4 & vctColor) { m_ClearColor = vctColor; }

		bool		getFullScreenState( void ) { return false; }
		bool		toggleFullScreen( bool fullscreen ) { SC_ASSERT(false); }
		//bool		hasFocus();
		void		flipBackBuffer( bool	waitForBackWindow, bool restoreview);
		void		processEvents(C_AppWindow * appWindow);
		//@ Delay
		//@ time - delay time in ms
		void		Delay(u32 time);
		//@ Push/Pop Transform matrix pair functions
		void		PushAndTransform(const sysmath::C_Vector2 & vctTransl, float fRotAngle, float fScale);
		void		PopTransform();

		static LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

		bool		HandleWinMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	private:
		//@ InitOpenGL
		bool		InitOpenGL();
		//@ TranslateToSDLCode
		SDLKey	TranslateToSDLCode(WPARAM keyCode);

		typedef void *EGLSurface;
		typedef void *EGLDisplay;	//just coz don't want include gles in header
	private:
		HWND						m_hWnd;
		HINSTANCE				m_hInstance;
		HDC						m_hDC;
		EGLDisplay				m_EglDisplay;
		EGLSurface				m_Surface;
		sysmath::C_Vector4	m_ClearColor;
		sysutils::C_String	m_WndCaption;
		bool						m_EventFired;
		T_WindowEvent			m_Event;
	};
}}

#endif