/*
* filename:			C_Input_WinImpl.h
*
* author:			Kral Jozef
* date:				2/6/2012		16:30
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/Framework/FrameworkTypes.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	// C_Input_WinImpl
	class C_Input_WinImpl
	{
	public:
		//static bool			IsPressed(E_KeyboardLayout key);
		static bool			IsPressed(E_KeyboardModifier keyMod);
	};
}}
