/*
* filename:			C_Input_WinImpl.cpp
*
* author:			Kral Jozef
* date:				2/6/2012		16:30
* version:			1.00
* brief:
*/


#include "C_Input_WinImpl.h"
#include <Windows.h>

namespace scorpio{ namespace syscore{


	//////////////////////////////////////////////////////////////////////////
	//
	//@ IsPressed
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Input_WinImpl::IsPressed(E_KeyboardModifier keyMod)
	{
		u32 winKeyMod = 0;

		switch (keyMod)
		{
		case E_KM_SHIFT:
			winKeyMod = VK_SHIFT;
			break;
		case E_KM_LSHIFT:
			winKeyMod = VK_LSHIFT;
			break;
		case E_KM_RSHIFT:
			winKeyMod = VK_RSHIFT;
			break;

		case E_KM_CTRL:
			winKeyMod = VK_CONTROL;
			break;
		case E_KM_LCTRL:
			winKeyMod = VK_LCONTROL;
			break;
		case E_KM_RCTRL:
			winKeyMod = VK_RCONTROL;
			break;

		case E_KM_ALT:
			winKeyMod = VK_MENU;
			break;
		case E_KM_LALT:
			winKeyMod = VK_LMENU;
			break;
		case E_KM_RALT:
			winKeyMod = VK_RMENU;
			break;
		default:
			SC_ASSERT(false);
			return false;
		}

		u16 ret = ::GetAsyncKeyState(winKeyMod);
		if (ret & 0x8000)
			return true;

		return false;
	}
}}
