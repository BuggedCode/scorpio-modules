/*
* filename:			C_Frame.cpp
*
* author:			Kral Jozef
* date:				01/22/2011		22:24
* version:			1.00
* brief:
*/


#include "C_Frame.h"
#include <SysCore/Render/C_Graphic.h>

namespace scorpio{ namespace syscore{

	DEFINE_ROOT_RTTI(C_Frame)

	using namespace scorpio::sysmath;
	using namespace scorpio::sysutils;
	using namespace scorpio::sysrender;

	const C_Point C_Frame::C_ZERO_SIZE = C_Point(0, 0);


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Frame::C_Frame()
		: m_Flags(0), m_fAngle(0.f), m_fScale(1.f), m_color(1.f, 1.f, 1.f, 1.f), m_Scene(NULL), m_Parent(NULL)
	{
		//////////////////////////////////////////////////////////////////////////
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ copy c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Frame::C_Frame(const C_Frame & copy)
	{
		m_Flags = copy.m_Flags;

		m_WorldPos = copy.m_WorldPos;
		m_WorldPosZ = copy.m_WorldPosZ;
		m_fAngle = copy.m_fAngle;
		m_fScale = copy.m_fScale;
		m_AABB = copy.m_AABB;

		m_localPos = copy.m_localPos;
		m_localPosZ = copy.m_localPosZ;

		m_color = copy.m_color;
		/*C_String str = copy.m_HashName.GetName();
		str += "_copy";
		m_HashName = C_HashName(str);*/
		m_HashName = copy.m_HashName;	//do not change name of child!! because we will not be able to find frame by name, (maybe to children set orig name)

		//m_Parent = copy.m_Parent;	//parent frame

		m_Properties = copy.m_Properties;

		for (T_ChildrenMap::const_iterator iter = copy.m_children.begin(); iter != copy.m_children.end(); ++iter)
		{
			T_Frame frmChild = iter->second->Clone();
			LinkToParent(frmChild);
		}
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetPosition
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::SetPosition(const C_Vector2 & vctPos)
	{
		m_WorldPos = vctPos;	//world
		if (m_Parent == NULL)
		{
			m_localPos = vctPos;
		}

		for (T_ChildrenMap::iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			iter->second->SetPosition(iter->second->GetLocalPosition() + vctPos);
		}

		m_Flags = C_AABB_CHANGED;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetZCoord
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::SetZCoord(float zCoord)
	{
		m_WorldPosZ = zCoord;	//world
		if (m_Parent == NULL)
		{
			m_localPosZ = zCoord;
		}

		for (T_ChildrenMap::iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			iter->second->SetZCoord(iter->second->GetZCoord() + zCoord);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ FindFrame
	//
	//////////////////////////////////////////////////////////////////////////
	T_Frame C_Frame::FindFrame(T_Hash32 frameHash)
	{
		T_ChildrenMap::iterator iter = m_children.find(frameHash);
		if (iter != m_children.end())
			return iter->second;

		//@ TODO RECURSIVE searching
		for (T_ChildrenMap::iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			T_Frame frm = iter->second->FindFrame(frameHash);
			if (frm)
				return frm;	//VARY - return first with same hash!
		}

		return T_Frame();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LinkTo
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::LinkToParent(T_Frame child)
	{
		//@ musia mat stejnou scenu nebo child musi mit NULL
		SC_ASSERT(child->m_Scene == this->m_Scene || child->m_Scene == NULL);
		
		child->m_Scene = this->m_Scene;
		child->m_Parent = this;
		child->SetPosition(this->m_WorldPos + child->m_localPos);
		child->SetZCoord(this->m_WorldPosZ + child->m_WorldPosZ);

		m_children.insert(T_ChildrenMap::value_type(child->GetHashName().GetHash(), child));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Unlink
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::Unlink()
	{
		if (!m_Parent)
			return;

		u32 nCount = (u32)m_Parent->m_children.erase(m_HashName.GetHash());
		SC_ASSERT(nCount == 1);

		m_localPos = C_Vector2::C_ZERO;
		m_localPosZ = 0.f;
		
		m_Parent->m_Flags = C_AABB_CHANGED;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ UnlinkChildren
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::UnlinkChildren()
	{
		m_children.clear();
		m_Flags = C_AABB_CHANGED;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ EnumFrames
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::EnumFrames(I_FrameVisitor & visitor)
	{
		for (T_ChildrenMap::iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			iter->second->EnumFrames(visitor);
		}

		visitor.Visit(*this);
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetAllChildren
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::GetAllChildren(T_FrameList & outChildren)
	{
		for (T_ChildrenMap::iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			iter->second->GetAllChildren(outChildren);
			outChildren.push_back(iter->second);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetChildren - on one top level
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::GetChildren(T_FrameList & outChildren)
	{
		for (T_ChildrenMap::iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			iter->second->GetAllChildren(outChildren);
			outChildren.push_back(iter->second);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::DebugDraw() const
	{
		//@ dbeug draw of links
		C_Vector4 dbgColor(.5f, .0f, 1.f, 1.f);	//purple links!
		C_Graphic graph;

		for (T_ChildrenMap::const_iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			const C_Vector2 & vctPos = iter->second->GetPosition();
			graph.RenderLine(m_WorldPos, vctPos, dbgColor);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ OnAddToScene
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::OnAddToScene(C_Scene * scene)
	{
		for (T_ChildrenMap::iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			iter->second->OnAddToScene(scene);
		}
		m_Scene = scene;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ OnRemoveFromScene
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::OnRemoveFromScene(C_Scene * scene)
	{
		for (T_ChildrenMap::iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			iter->second->OnRemoveFromScene(scene);
		}
		m_Scene = NULL;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ComputeAABB
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Frame::ComputeAABB() const
	{
		GetNonHrAABB(m_AABB);
		for (T_ChildrenMap::const_iterator iter = m_children.begin(); iter != m_children.end(); ++iter)
		{
			iter->second->ComputeAABB();
			const C_AABB2 & aabb = iter->second->GetAABB();
			if (aabb.IsValid())
				m_AABB.Include(aabb);
		}
	}

}}