/*
* filename:			C_TextVisual.h
*
* author:			Kral Jozef
* date:				09/17/2010		19:13
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include "C_Visual.h"
#include <SysCore/Render/C_TrueText.h>


namespace scmath = scorpio::sysmath;
namespace scutils = scorpio::sysutils;

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_TextVisual
	class SYSCORE_API C_TextVisual : public C_Visual
	{
		typedef C_Visual T_Super;
	public:
		//@ c-tor
		C_TextVisual(const sysmath::C_Vector2 & vctPos, u32 height, T_TrueText font, scutils::C_String text, bool bDrawFromLeft = true);
		//@ d-tor
		virtual ~C_TextVisual() {};

		//@ I_RenderObject::Render
		virtual void					Render() const;
		//@ DebugDraw
		virtual void					DebugDraw() const;
		
		//@ SetColor
		virtual void					SetColor(const sysmath::C_Vector4 & color);
		
		//@ SetText
		void								SetText(scutils::C_String text);
		
		//@ DrawStringFormLeft
		bool								IsDrawingFormLeft() const { return m_bDrawFromLeft; }

	private:
		//@ SetSize
		void								SetSize(u32 inWidth, u32 inHeight) { m_size.x = inWidth; m_size.y = inHeight; }

	private:
		scutils::C_String		m_text;
		T_TrueText				m_ttFont;
		bool						m_bDrawFromLeft;	//text se vykresluje z lavo do prava

		DECLARE_RTTI
	};

	typedef boost::shared_ptr<scorpio::syscore::C_TextVisual>	T_TextVisual;

}}