/*
* filename:			C_Frame.h
*
* author:			Kral Jozef
* date:				01/22/2011		22:24
* version:			1.00
* brief:				Base abstract class of frames
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <Common/C_RTTI.h>
#include <SysCore/Core/I_RenderObject.h>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Point.h>
#include <SysUtils/C_HashName.h>
#include <map>
#include <SysUtils/Spatial/I_SpatialObject.h>

namespace scorpio{ namespace syscore{

	namespace scutils = scorpio::sysutils;


	//# forward
	class C_Frame;
	class C_Scene;

	typedef boost::shared_ptr<scorpio::syscore::C_Frame>	T_Frame;
	typedef std::vector<T_Frame>		T_FrameList;

	//////////////////////////////////////////////////////////////////////////
	//@ I_FrameVisitor
	class I_FrameVisitor
	{
	public:
		virtual void			Visit(C_Frame & frame) = 0;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_Frame
	class SYSCORE_API C_Frame : public I_RenderObject, public sysutils::I_SpatialObject
	{
		static const u32	C_AABB_CHANGED = (1 << 0);
	protected:
		//@ copy c-tor
		C_Frame(const C_Frame & copy);

	public:
		//@ c-tor
		C_Frame();
		//@ d-tor
		virtual ~C_Frame() {};

		//@ I_RenderObject::Render
		virtual void					Render() const = 0;
#ifndef MASTER
		//@ DebugDraw
		virtual void					DebugDraw() const;
#endif
		//@ WorldAABB sysutils::I_SpatialObject::GetAABB
		virtual const sysmath::C_AABB2	&	GetAABB() const
		{ 
			if (m_Flags & C_AABB_CHANGED)
			{
				m_AABB.Invalidate();
				ComputeAABB();
				m_Flags &= ~C_AABB_CHANGED	;//rem flag
			}
			return m_AABB; 
		}

		//@ GetHashName
		const scutils::C_HashName	&	GetHashName() const { return m_HashName; }

		//@ GetZCoord
		virtual float					GetZCoord() const { return m_WorldPosZ; }
		//@ SetZCoord
		void								SetZCoord(float zCoord);


		//@ SetPosition - pivot is in top/left corner
		virtual void					SetPosition(const sysmath::C_Vector2 & vctPos);
		const sysmath::C_Vector2	&	GetPosition() const { return m_WorldPos; }
		void								SetRotation(float fAngleZ) { m_fAngle = fAngleZ; m_Flags = C_AABB_CHANGED; }
		float								GetRotation() const { return m_fAngle; }
		virtual void					SetScale(float fScale) { m_fScale = fScale; m_Flags = C_AABB_CHANGED; }
		float								GetScale() const { return m_fScale; }
		//@ LOCAL TRANSFORMATION
		const sysmath::C_Vector2	&	GetLocalPosition() const { return m_localPos; }
		//@ Set correct scale depend on internal textureSize
		virtual void					SetSize(u32 inWidth, u32 inHeight) { SC_ASSERT(false); }

		virtual const sysmath::C_Point	&	GetSize() const { SC_ASSERT(false); return C_ZERO_SIZE; }
		virtual u32						GetWidth() const { SC_ASSERT(false); return 0; }
		virtual u32						GetHeight() const { SC_ASSERT(false); return 0; }

		virtual void					SetColor(const sysmath::C_Vector4 & color) { m_color = color; }	//fe C_TextVisual override and set color for font!
		sysmath::C_Vector4		&	GetColor() { return m_color; }
		const sysmath::C_Vector4&	GetColor() const { return m_color; }
		virtual void					SetAlpha(float fAlpha) { m_color.m_w = fAlpha; }
		//@ SetProperties
		void								SetProperties(sysutils::C_String propString) { m_Properties = propString; }
		//@ GetProperties
		sysutils::C_String			GetProperties() const { return m_Properties; }

		//@ Contains position
		virtual bool					Contains(const sysmath::C_Vector2 & vctPos) const = 0;

		//@ LinkTo
		//@ child - child parameter, this == parent
		void								LinkToParent(T_Frame child);
		//@ Unlink - from parent
		void								Unlink();
		//@ UnlinkChildren
		void								UnlinkChildren();
		//@ FindFrame
		T_Frame							FindFrame(T_Hash32 frameHash);
		//@ FindFrame
		T_Frame							FindFrame(sysutils::C_String frmName) { return FindFrame(sysutils::C_HashName(frmName).GetHash()); }
		//@ GetScene
		C_Scene						*	GetScene() const { return m_Scene; }
		//@ EnumFrames
		void								EnumFrames(I_FrameVisitor & visitor);
		//@ GetAllChildren
		void								GetAllChildren(T_FrameList & outChildren);
		//@ GetChildren - on one top level
		void								GetChildren(T_FrameList & outChildren);
		//@ Clone - retrun hierarchical copyi of self
		virtual T_Frame				Clone() const = 0;


		//@ callbacks form scene
		void								OnAddToScene(C_Scene * scene);
		void								OnRemoveFromScene(C_Scene * scene);

	protected:
		//@ ComputeAABB
		void								ComputeAABB() const;	//mutable const:(
		//@ GetNonHrAABB
		virtual void					GetNonHrAABB(sysmath::C_AABB2 & aabbOut) const = 0;

	public:
		//@ tmp for removing
		static const sysmath::C_Point		C_ZERO_SIZE;
	protected:
		mutable u32				m_Flags;

		//@ WORLD TRANSFORMATION
		sysmath::C_Vector2	m_WorldPos;		//pivot
		float						m_WorldPosZ;		//depth on the screen
		float						m_fAngle;	//uhel rotace kolem Z
		float						m_fScale;	//scale

		mutable sysmath::C_AABB2		m_AABB;		//worl hierarchical AABB
		
		//@ LOCAL TRANSFORMATION
		sysmath::C_Vector2	m_localPos;
		float						m_localPosZ;		//depth on the screen


		sysmath::C_Vector4	m_color;	//diffuse color
		sysutils::C_HashName	m_HashName;
		sysutils::C_String	m_Properties;	//object properties defined in 3DS max

		C_Scene				*	m_Scene;		//owner scene in which this frame is

		C_Frame				*	m_Parent;	//parent frame
		typedef std::map<T_Hash32, T_Frame>	T_ChildrenMap;
		T_ChildrenMap			m_children;	//childrens are not render now TODO


		DECLARE_ROOT_RTTI
	};

}}
