/*
* filename:			C_TextVisual.cpp
*
* author:			Kral Jozef
* date:				09/17/2010		19:15
* version:			1.00
* brief:
*/

#include "C_TextVisual.h"
#include <SysCore/Render/C_Graphic.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	DEFINE_RTTI(C_TextVisual, C_Visual)

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::sysrender;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_TextVisual::C_TextVisual(const C_Vector2 & vctPos, u32 height, T_TrueText font, C_String text, bool bDrawFromLeft)
		: C_Visual(vctPos, 0, height)
	{
		m_bDrawFromLeft = bDrawFromLeft;
		//@ JK create bitmap image from font & text?
		m_ttFont = font;
		m_text = text;
		SC_ASSERT(false);
		//m_ttFont->setHeightPix(height);
		float fWidth = m_ttFont->GetStringWidth(text);	//kerning 0
		SetSize((u32)fWidth, height);
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_TextVisual::Render() const
	{
		m_ttFont->SetColor(m_color);
		if (m_bDrawFromLeft)
		{
			m_ttFont->DrawStringFromLeft(m_text, m_WorldPos);	//pre vykreslenie ttfontu sa udava Top jako Y, on ho kresli pod!
		}
		else
		{
			m_ttFont->DrawStringFromRight(m_text, m_WorldPos);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_TextVisual::DebugDraw() const
	{
		C_Graphic image;
		if (m_bDrawFromLeft)
		{
			image.RenderRect(m_WorldPos, m_size, m_color, false, 0.f, 1.f);
		}
		else
		{
			image.RenderRect(C_Vector2(m_WorldPos - C_Vector2((float)m_size.x, 0.f)), m_size, m_color, false, 0.f, 1.f);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetColor
	//
	//////////////////////////////////////////////////////////////////////////
	void C_TextVisual::SetColor(const sysmath::C_Vector4 & color)
	{
		 m_color = color;
		 m_ttFont->SetColor(m_color);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetText
	//
	//////////////////////////////////////////////////////////////////////////
	void C_TextVisual::SetText(C_String text)
	{
		m_text = text;
		float fWidth = m_ttFont->GetStringWidth(text);	//kerning 0
		SetSize((u32)fWidth, m_size.y);
	}


}}