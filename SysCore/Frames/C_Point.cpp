/*
* filename:			C_Point.cpp
*
* author:			Kral Jozef
* date:				01/24/2011		3:13
* version:			1.00
* brief:
*/

#include "C_Point.h"
#include <SysCore/Render/C_Graphic.h>

namespace scorpio{ namespace syscore{

	DEFINE_RTTI(C_Point, C_Frame)

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::sysrender;

	static const C_Vector2 C_DBG_DRAW_OFFSET_X = C_Vector2(10.f, .0f);
	static const C_Vector2 C_DBG_DRAW_OFFSET_Y = C_Vector2(.0f, 10.f);


	//////////////////////////////////////////////////////////////////////////
	//
	//@ copy c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Point::C_Point(const C_Point & copy)
		: C_Frame(copy)
	{
		//////////////////////////////////////////////////////////////////////////
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	//@ DebugDraw
	void C_Point::DebugDraw() const
	{
		C_Vector4 dbgColor(.8f, .8f, .0f, 1.f);	//Yellow Point!
		C_Graphic graph;

		graph.RenderLine(m_WorldPos - C_DBG_DRAW_OFFSET_X, m_WorldPos + C_DBG_DRAW_OFFSET_X, dbgColor);
		graph.RenderLine(m_WorldPos - C_DBG_DRAW_OFFSET_Y, m_WorldPos + C_DBG_DRAW_OFFSET_Y, dbgColor);

		T_Super::DebugDraw();
	}
#endif


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clone
	//
	//////////////////////////////////////////////////////////////////////////
	T_Frame C_Point::Clone() const
	{
		T_Point point = T_Point(new C_Point(*this));
		return point;
	}


}}