/*
* filename:			C_Visual.cpp
*
* author:			Kral Jozef
* date:				01/25/2010		21:50
* version:			1.00
* brief:
*/

#include "C_Visual.h"
#include <SysMath/C_Rect.h>
#include <SysUtils/Utils/C_StringUtils.h>
#include <SysCore/Core/C_TextureManager.h>
#include <SysCore/Render/C_Renderer.h>
#include <SysCore/Managers/C_VertexBufferManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	DEFINE_RTTI(C_Visual, C_Frame)

	using namespace sysmath;
	using namespace sysutils;
	using namespace sysrender;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Visual::C_Visual(const T_Mesh2d mesh)
		: m_BlendingMode(sysrender::E_BM_SRC_ALPHA__DST_ONE_MINUS_ALPHA), m_clipCoordX(C_Vector2(0.f, 1.f)), m_clipCoordY(C_Vector2(0.f, 1.f)), m_bClipped(false),
		  m_RenderImage1(false), m_TexCoordX(C_Vector2(0.f, 1.f)), m_TexCoordY(C_Vector2(0.f, 1.f))
	{
		m_WorldPos = mesh->GetPivot();
		m_fAngle = mesh->GetRotation();
		m_WorldPosZ = mesh->GetZCoord();
		m_color = mesh->GetColor();

		const C_AABB2 & texCoords = mesh->GetTexCoords();
		m_TexCoordX.m_x = texCoords.GetMin().m_x;
		m_TexCoordX.m_y = texCoords.GetMax().m_x;
		m_TexCoordY.m_x = texCoords.GetMin().m_y;
		m_TexCoordY.m_y = texCoords.GetMax().m_y;


		//@ SET LOCAL TRANSFORMATION
		m_localPos = mesh->GetPivot();
		m_localPosZ = mesh->GetZCoord();
		
		m_GraphicResource = T_TextureManager::GetInstance().GetTexture(mesh->GetDiffuseTextureName());
		m_size.x = (s32)mesh->GetAABB().GetWidth();
		m_size.y = (s32)mesh->GetAABB().GetHeight();

		C_String strDiffMap1 = mesh->GetDiffuseTextureName(1);
		if (!strDiffMap1.empty())
		{
			m_GraphicResource1 = T_TextureManager::GetInstance().GetTexture(strDiffMap1);
		}

		ComputeAABB();

		m_HashName = mesh->GetName();

		GenerateVertexBuffer();	//from Mesh
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	/*C_Visual::C_Visual(const sysrender::T_GraphicRes & imgRef)
		: m_GraphicResource(imgRef), m_BlendingMode(E_BM_NONE), m_clipCoordX(C_Vector2(0.f, 1.f)), m_clipCoordY(C_Vector2(0.f, 1.f)), m_bClipped(false)
	{
		m_fPosZ = m_localPosZ = 0.f;
		if (m_GraphicResource)
		{
			m_size.x = (s32)m_GraphicResource->getWidth();
			m_size.y = (s32)m_GraphicResource->getHeight();
			
			RecalcSrcRenderRect();
			//RecalcDstRenderRect();

			
#ifdef NO_FINAL
			m_name = C_StringUtils::ExtractFileNameFromPath(imgRef->GetFileName());
#else
			m_name = imgRef->GetFileName();
#endif
		}
	}*/


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Visual::C_Visual(const sysmath::C_Vector2 & vctPos, u32 pWidth, u32 pHeight)
		: m_BlendingMode(sysrender::E_BM_SRC_ALPHA__DST_ONE_MINUS_ALPHA), m_clipCoordX(C_Vector2(0.f, 1.f)), m_clipCoordY(C_Vector2(0.f, 1.f)), m_bClipped(false),
		  m_RenderImage1(false), m_TexCoordX(C_Vector2(0.f, 1.f)), m_TexCoordY(C_Vector2(0.f, 1.f))
	{
		m_WorldPos = m_localPos = vctPos;
		m_WorldPosZ = m_localPosZ = 0.f;
		m_size = C_Point(pWidth, pHeight);
		m_HashName = C_HashName("");

		m_AABB.Include(vctPos);	//TODO

		GenerateVertexBuffer();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ copy c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Visual::C_Visual(const C_Visual & copy)
		: C_Frame(copy)
	{
		m_size = copy.m_size;
		m_GraphicResource = copy.m_GraphicResource;
		m_GraphicResource1 = copy.m_GraphicResource1;
		m_BlendingMode = copy.m_BlendingMode;

		m_clipCoordX = copy.m_clipCoordX;
		m_clipCoordY = copy.m_clipCoordY;
		m_TexCoordX = copy.m_TexCoordX;
		m_TexCoordY = copy.m_TexCoordY;

		m_bClipped = copy.m_bClipped;

		m_RenderImage1 = copy.m_RenderImage1;

		m_VertexBuffer = copy.m_VertexBuffer;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Visual::~C_Visual()
	{
		T_VertexBufferManager::GetInstance().Remove(m_VertexBuffer);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Visual::Render() const
	{
		if (!m_bVisible)
			return;

		if (m_GraphicResource)
		{
			m_GraphicResource->SetBlendingMode(m_BlendingMode);

			SC_ASSERT(m_VertexBuffer);
			if (!IsClipped())
				m_GraphicResource->RenderVBO(m_VertexBuffer, m_WorldPos, m_fAngle, m_fScale, m_color);
			else
				m_GraphicResource->RenderVBOClipped(m_VertexBuffer, m_WorldPos, m_fAngle, m_fScale, m_color, m_ClipAreaLBCorner, m_ClipAreaSize);
		}
		else
		{
			C_Graphic graph;
			graph.RenderRect(m_WorldPos, m_size, m_color, true, m_fAngle, m_fScale);
		}

		if (m_GraphicResource1 && m_RenderImage1)
		{
			//@ ?? JK m_GraphicResource1->SetBlendingMode(m_BlendingMode);			
			if (!IsClipped())
				m_GraphicResource1->RenderVBO(m_VertexBuffer, m_WorldPos, m_fAngle, m_fScale, m_color);
			else
				m_GraphicResource1->RenderVBOClipped(m_VertexBuffer, m_WorldPos, m_fAngle, m_fScale, m_color, m_ClipAreaLBCorner, m_ClipAreaSize);
		}
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	//@ DebugDraw
	void C_Visual::DebugDraw() const
	{
		C_Vector4 dbgColor(.0f, .8f, .0f, 1.f);	//Green visual!
		C_Graphic graph;
		graph.RenderRect(m_WorldPos, m_size, dbgColor, false, m_fAngle, m_fScale);
		T_Super::DebugDraw();

		dbgColor = C_Vector4(.8f, .0f, .4f, 1.f);	//pink AABB!
		//const C_AABB2 & aabb = GetAABB();
		//graph.RenderRect(aabb.GetMin(), aabb.GetMax(), dbgColor, false);
		graph.RenderRect(m_AABB.GetMin(), m_AABB.GetMax(), dbgColor, false);
	}
#endif



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Contains
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Visual::Contains(const sysmath::C_Vector2 & vctPos) const
	{
		//@ Recompute AABB if need
		const C_AABB2 & aabb = T_Super::GetAABB();
		if (vctPos.m_x >= aabb.GetMin().m_x && vctPos.m_x <= aabb.GetMax().m_x && vctPos.m_y >= aabb.GetMin().m_y && vctPos.m_y <= aabb.GetMax().m_y)
			return true;

		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clone
	//
	//////////////////////////////////////////////////////////////////////////
	T_Frame C_Visual::Clone() const
	{
		T_Visual vis = T_Visual(new C_Visual(*this));
		return vis;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetNonHrAABB
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Visual::GetNonHrAABB(C_AABB2 & aabbOut) const
	{
		C_Vector2 vct(m_size.x * 0.5f, m_size.y * 0.5f);
		vct.rotateBy(m_fAngle);
		vct += m_WorldPos;
		aabbOut.Include(vct);

		vct = C_Vector2(-m_size.x * 0.5f, -m_size.y * 0.5f);
		vct.rotateBy(m_fAngle);
		vct += m_WorldPos;
		aabbOut.Include(vct);

		vct = C_Vector2(m_size.x * 0.5f, -m_size.y * 0.5f);
		vct.rotateBy(m_fAngle);
		vct += m_WorldPos;
		aabbOut.Include(vct);

		vct = C_Vector2(-m_size.x * 0.5f, m_size.y * 0.5f);
		vct.rotateBy(m_fAngle);
		vct += m_WorldPos;
		aabbOut.Include(vct);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetClipCoord
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Visual::SetClipCoord(const sysmath::C_Vector2 & widthCoord, const sysmath::C_Vector2 & heightCoord)
	{
		m_clipCoordX = widthCoord;
		m_clipCoordY = heightCoord;

		CheckClipping();
		RecalcClipCoords();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RecalcClipCoords
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Visual::RecalcClipCoords()
	{
		if (!m_bClipped)
			return;

		const C_Point & viewportSize = T_Renderer::GetInstance().GetViewportSize();

		m_ClipAreaLBCorner.x = (u32)(m_WorldPos.m_x - (m_size.x * m_fScale * 0.5f) + (m_clipCoordX.m_x * m_size.x * m_fScale));	//left
		m_ClipAreaLBCorner.y = (u32)(m_WorldPos.m_y - (m_size.y * m_fScale * 0.5f) + (m_clipCoordY.m_y * m_size.y * m_fScale));	//bottom
		m_ClipAreaLBCorner.y = viewportSize.y - m_ClipAreaLBCorner.y;

		m_ClipAreaSize.x = (u32)((m_clipCoordX.m_y - m_clipCoordX.m_x) * m_fScale * m_size.x);	//width
		m_ClipAreaSize.y = (u32)((m_clipCoordY.m_y - m_clipCoordY.m_x) * m_fScale * m_size.y);	//width
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ CheckClipping
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Visual::CheckClipping()
	{
		m_bClipped = false;
		
		if (!isZero(m_clipCoordX.m_x))
		{
			m_bClipped = true;
			return;
		}

		if (!isZero(m_clipCoordX.m_y - 1.f))
		{
			m_bClipped = true;
			return;
		}

		if (!isZero(m_clipCoordY.m_x))
		{
			m_bClipped = true;
			return;
		}

		if (!isZero(m_clipCoordY.m_y - 1.f))
		{
			m_bClipped = true;
			return;
		}

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GenerateVertexBuffer
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Visual::GenerateVertexBuffer()
	{
		m_VertexBuffer = T_VertexBufferManager::GetInstance().GenerateVertexBufferObject();
		C_Vector2 vctLT(m_TexCoordX.m_x, m_TexCoordY.m_x);
		C_Vector2 vctRB(m_TexCoordX.m_y, m_TexCoordY.m_y);
		m_VertexBuffer->CreateFromRect(C_Vector2((float)m_size.x, (float)m_size.y), m_WorldPosZ, vctLT, vctRB);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetSize
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Visual::SetSize(u32 inWidth, u32 inHeight)
	{
		//@ UNIFORM SCALE!
		m_fScale = inWidth / (float) m_size.x;
		RecalcClipCoords();
		 //m_size.x = inWidth; 
		 //m_size.y = inHeight;
	}
}}