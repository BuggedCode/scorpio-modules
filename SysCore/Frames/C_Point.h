/*
* filename:			C_Point.h
*
* author:			Kral Jozef
* date:				01/24/2011		2:59
* version:			1.00
* brief:
*/

#pragma once

#include "C_Frame.h"

namespace scorpio{ namespace syscore{

	namespace scutils = scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//@ C_Frame
	class SYSCORE_API C_Point : public C_Frame
	{
		typedef C_Frame	T_Super;

	private:
		//@ copy c-tor
		C_Point(const C_Point & copy);

	public:
		//@ c-tor
		C_Point(sysutils::C_String name, sysmath::C_Vector2 & vctPos)
		{
			m_HashName = sysutils::C_HashName(name);
			m_WorldPos = vctPos;
			m_localPos = vctPos;
		}

		//@ I_RenderObject::Render
		virtual void					Render() const {};
#ifndef MASTER
		//@ DebugDraw
		virtual void					DebugDraw() const;
#endif

		virtual bool					Contains(const sysmath::C_Vector2 & vctPos) const { return false; }
		//@ Clone
		virtual T_Frame				Clone() const;
		//@ GetNonHrAABB
		virtual void					GetNonHrAABB(sysmath::C_AABB2 & aabbOut) const { aabbOut.Invalidate(); }
		//@ GetSize
		//@ Widgets/Labels can be inited from points defined in Max(just for position)
		virtual const sysmath::C_Point	&	GetSize() const { return C_Frame::C_ZERO_SIZE; }

		DECLARE_RTTI
	};


	typedef boost::shared_ptr<scorpio::syscore::C_Point>	T_Point;
	typedef std::vector<T_Point>			T_PointList;


}}