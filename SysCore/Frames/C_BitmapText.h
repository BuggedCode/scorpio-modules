/*
* filename:			C_BitmapText.h
*
* author:			Kral Jozef
* date:				11/07/2010		22:38
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/Core/I_RenderObject.h>
#include <SysCore/Core/C_BitmapFont.h>
#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace syscore{

	enum E_TranslationTableId
	{
		E_TT_NONE = 0,
		E_TT_CZSK = 1,
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_BitmapText
	class SYSCORE_API C_BitmapText : public I_RenderObject
	{
	public:
		//@ c-tor
		C_BitmapText(sysutils::C_String strText, T_BitmapFont font, E_TranslationTableId trTableId);
		//@ c-tor
		C_BitmapText(const sysmath::C_Vector2 & vctPos, sysutils::C_String strText, T_BitmapFont font, E_TranslationTableId trTableId);
		//@ d-tor
		virtual ~C_BitmapText() {};
		
		//@ Render
		virtual void					Render() const;
#ifndef MASTER
		//@ DebugDraw
		virtual void					DebugDraw() const;
#endif

		virtual void					SetZCoord(float zCoord) { m_zCoord = zCoord; }
		virtual float					GetZCoord() const { return m_zCoord; }
		//@ SetText
		void								SetText(sysutils::C_String text) 
		{ 
			m_text = text; 
			if(m_trTableId != E_TT_NONE)
				m_text = ConvertUTF8ToAscii(m_text, m_trTableId);
		}
		//@ SetPosition
		void								SetPosition(const sysmath::C_Vector2 & vctPos) { m_pos = vctPos; /*RecalcDstRenderRect();*/ }
		const sysmath::C_Vector2	&	GetPosition() const { return m_pos; }
		void								SetScale(float fScale) { m_fScale = fScale; /*RecalcDstRenderRect();*/ }
		float								GetScale() const { return m_fScale; }

		sysmath::C_Point				GetSize() const;
		u32								GetWidth() const { return GetSize().x; }
		u32								GetHeight() const { return GetSize().y; }

		virtual void					SetColor(const sysmath::C_Vector4 & color) { m_color = color; }	//fe C_TextVisual override and set color for font!
		sysmath::C_Vector4		&	GetColor() { return m_color; }
		const sysmath::C_Vector4&	GetColor() const { return m_color; }
		virtual void					SetAlpha(float fAlpha) { m_color.m_w = fAlpha; }

	private:
		static sysutils::C_String	ConvertUTF8ToAscii(sysutils::C_String inText, E_TranslationTableId tableId);
		static char						TranslateCharacter(char character);
	
	private:
		T_BitmapFont			m_font;		//font which is used in this text
		sysutils::C_String	m_text;
		
		sysmath::C_Vector2	m_pos;		//left top corner
		float						m_fScale;	//scale
		//float						m_fAngle;	//uhel rotace kolem Z
		sysmath::C_Vector4	m_color;
		float						m_zCoord;

		E_TranslationTableId	m_trTableId;
	};

	typedef boost::shared_ptr<C_BitmapText>	T_BitmapText;
}}