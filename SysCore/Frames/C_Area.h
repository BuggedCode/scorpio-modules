/*
* filename:			C_Area.h
*
* author:			Kral Jozef
* date:				01/22/2011		22:23
* version:			1.00
* brief:				2d area
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include "C_Frame.h"
#include <SysCore/Geometry/C_PolyShape.h>

namespace scorpio{ namespace syscore{


	//////////////////////////////////////////////////////////////////////////
	//@ C_Frame
	class SYSCORE_API C_Area : public C_Frame
	{
		typedef C_Frame		T_Super;

	private:
		//@ copy c-tor
		C_Area(const C_Area & copy);

	public:
		//@ c-tor
		C_Area(T_PolyShape shape);
		//@ d-tor
		virtual ~C_Area() {};


		//@ I_RenderObject::Render
		virtual void					Render() const {};
#ifndef MASTER
		//@ DebugDraw
		virtual void					DebugDraw() const;
#endif


		virtual void					SetPosition(const sysmath::C_Vector2 & vctPos) { T_Super::SetPosition(vctPos); m_shape->SetPosition(m_WorldPos); }
		virtual void					SetScale(float fScale) { m_fScale = fScale; SC_ASSERT(false); }

		//@ Contains position
		virtual bool					Contains(const sysmath::C_Vector2 & vctPos) const;
		//@ Clone
		T_Frame							Clone() const;

		const T_PolyShape				GetShape() const { return m_shape; }
		//@ GetNonHrAABB
		virtual void					GetNonHrAABB(sysmath::C_AABB2 & aabbOut) const { aabbOut.Invalidate(); }

	protected:
		T_PolyShape			m_shape;	//should be positioned in World, polygon has defined by vertex with local coord

		DECLARE_RTTI
	};

	typedef boost::shared_ptr<C_Area>	T_Area;


}}