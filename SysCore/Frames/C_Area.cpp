/*
* filename:			C_Area.cpp
*
* author:			Kral Jozef
* date:				01/22/2011		22:23
* version:			1.00
* brief:
*/

#include "C_Area.h"
#include <SysCore/Render/C_Graphic.h>

namespace scorpio{ namespace syscore{

	DEFINE_RTTI(C_Area, C_Frame)

	using namespace scorpio::sysmath;
	using namespace scorpio::sysutils;
	using namespace scorpio::sysrender;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ copy c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Area::C_Area(const C_Area & copy)
		: C_Frame(copy)
	{
		m_shape = copy.m_shape->Clone();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Area::C_Area(T_PolyShape shape)
		: m_shape(shape)
	{
		m_HashName = shape->GetHashName();
		//@ DO NOT NEED - shape was transfomed into world after loading
		//store pivot information local
		m_WorldPos = shape->GetPosition();
		m_localPos = shape->GetPosition();	//local position is same as a global because area has no parent in construct time
		m_fAngle = shape->GetRotation();
		m_fScale = shape->GetScale();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	void C_Area::DebugDraw() const
	{
		C_Vector4 dbgColor(.0f, .0f, .8f, 1.f);	//Blue Area!
		C_Graphic graph;
		
		T_Polygon poly = m_shape->GetPolygon();
		const C_Vector2 & shapePos = m_shape->GetPosition();

		u32 size = (u32)poly->GetSize();
		for (u32 i = 0; i < size; ++i)
		{
			const C_Vector2 & vctPos = (*poly)[i];
			const C_Vector2 & vctNext = (i+1 == (size)) ? (*poly)[0] : (*poly)[i+1];

			graph.RenderLine(vctPos + shapePos, vctNext + shapePos, dbgColor);
		}

		T_Super::DebugDraw();
	}
#endif


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Contains
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Area::Contains(const sysmath::C_Vector2 & vctPos) const
	{
		return m_shape->IsInside(vctPos);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clone
	//
	//////////////////////////////////////////////////////////////////////////
	T_Frame C_Area::Clone() const
	{
		T_Area area = T_Area(new C_Area(*this));
		return area;
	}


}}