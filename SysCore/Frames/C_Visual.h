/*
* filename:			C_Visual.h
*
* author:			Kral Jozef
* date:				01/25/2010		21:37
* version:			1.00
* brief:
*/

#pragma once

#include <Common/macros.h>
#include <SysCore/SysCoreApi.h>
#include <SysCore/Frames/C_Frame.h>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Point.h>
#include <SysUtils/C_HashName.h>
#include <SysMath/MathUtils.h>
#include <SysCore/Geometry/C_Mesh2d.h>
#include <SysCore/Render/RenderTypes.h>	//because of blending
#include <SysCore/Render/C_VertexBufferObject.h>
#include <SysCore/Render/C_Graphic.h>

namespace scorpio{ namespace syscore{
	
	namespace scutils = scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//@ C_Visual
	//template<class T> z templatovat podle animtextury alebo obyc textury


	//////////////////////////////////////////////////////////////////////////
	//@ C_Visual
	class SYSCORE_API C_Visual : public C_Frame
	{
		typedef C_Frame	T_Super;
	public:
		//@ c-tor
		C_Visual(const T_Mesh2d mesh);
		//@ c-tor
		//C_Visual(const sysrender::T_GraphicRes & imgRef);
		//@ c-tor
		C_Visual(const sysmath::C_Vector2 & vctPos, u32 pWidth, u32 pHeight);
		//@ copy c-tor
		C_Visual(const C_Visual & copy);
		//@ d-tor
		virtual ~C_Visual();

		//@ I_RenderObject::Render
		virtual void					Render() const;
#ifndef MASTER
		//@ DebugDraw
		virtual void					DebugDraw() const;
#endif

		//@ SetPosition - pivot is in top/left corner
		virtual void					SetPosition(const sysmath::C_Vector2 & vctPos) { T_Super::SetPosition(vctPos); RecalcClipCoords(); }

		virtual void					SetScale(float fScale) { m_fScale = fScale; RecalcClipCoords(); }
		virtual SC_DEPRECATED void	SetSize(u32 inWidth, u32 inHeight);

		virtual const sysmath::C_Point	&	GetSize() const { return m_size; }
		virtual u32						GetWidth() const { return (u32)(m_size.x * m_fScale); }
		virtual u32						GetHeight() const { return (u32)(m_size.y * m_fScale); }


		void								SetBlendingMode(sysrender::E_BlendingMode blendingMode) { m_BlendingMode = blendingMode; }
		u32								GetBlendingMode() const { return m_BlendingMode; }

		//@ Contains position
		virtual bool					Contains(const sysmath::C_Vector2 & vctPos) const;
		//@ Clone
		virtual T_Frame				Clone() const;

		//@ SetTexture - diffuse
		void								SetTexture(sysrender::T_GraphicRes graphicRes) { m_GraphicResource = graphicRes; }
		//@ GetTexture - diffuse
		sysrender::T_GraphicRes		GetTexture() { return m_GraphicResource; }
		sysutils::C_String			GetTextureName() { return m_GraphicResource->GetFileName().GetName(); }
		//@ Clipping - depend on viewport Size
		void								SetClipCoord(const sysmath::C_Vector2 & widthCoord, const sysmath::C_Vector2 & heightCoord);
		const sysmath::C_Vector2&	GetClipCoordX() const { return m_clipCoordX; }
		const sysmath::C_Vector2&	GetClipCoordY() const { return m_clipCoordY; }
		void								SetTexCoordX(const sysmath::C_Vector2 & widthCoord) { m_TexCoordX = widthCoord; }
		void								SetTexCoordY(const sysmath::C_Vector2 & heightCoord) { m_TexCoordY = heightCoord; }
		const sysmath::C_Vector2&	GetTexCoordX() const { return m_TexCoordX; }
		const sysmath::C_Vector2&	GetTexCoordY() const { return m_TexCoordY; }

		//@ EnableDiffuseTexture1
		void								EnableDiffuseTexture1(bool bEnable) { m_RenderImage1 = bEnable; }
		//@ GenerateVertexBuffer
		void								GenerateVertexBuffer();

	protected:
		//@ GetNonHrAABB
		virtual void					GetNonHrAABB(sysmath::C_AABB2 & aabbOut) const;

	private:
		//@ RecalcClipCoords - compute viewport scisor rectangle
		void								RecalcClipCoords();
		//@ IsClipped - return if image is clipped
		bool								IsClipped() const { return m_bClipped; }
		//@ CheckClipping - set m_bClipped flag based on clip Coordinates
		void								CheckClipping();

	protected:
		sysmath::C_Point				m_size;
		sysrender::T_GraphicRes		m_GraphicResource;
		sysrender::T_GraphicRes		m_GraphicResource1;	//internal second texture over first one
		sysrender::E_BlendingMode	m_BlendingMode;

		//@ Clipping Coordinates
		sysmath::C_Vector2	m_clipCoordX;	//cliping coords for Width <0,1>		(x = LEFT = 0, y = RIGHT = 1)
		sysmath::C_Vector2	m_clipCoordY;	//clipping coord for Height <0,1>	(x = TOP = 0, y = BOTTOM = 1)
		bool						m_bClipped;

		//@ Texture Coordinates
		sysmath::C_Vector2	m_TexCoordX;	//texture coords for Width <0,1>		(x = LEFT = 0, y = RIGHT = 1)
		sysmath::C_Vector2	m_TexCoordY;	//texture coord for Height <0,1>	(x = TOP = 0, y = BOTTOM = 1)

		sysmath::C_Point		m_ClipAreaLBCorner;	//for scisors test in viewport space
		sysmath::C_Point		m_ClipAreaSize;		//for scisors test in viewport space	//2 * 16 improove!

		bool						m_RenderImage1 : 1;

		sysrender::T_VertexBufferObject	m_VertexBuffer;


		DECLARE_RTTI
	};

	typedef boost::shared_ptr<scorpio::syscore::C_Visual>	T_Visual;
}}