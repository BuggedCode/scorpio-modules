/*
* filename:			C_BitmapText.cpp
*
* author:			Kral Jozef
* date:				11/07/2010		22:48
* version:			1.00
* brief:
*/

#include "C_BitmapText.h"
#include <SysMath/C_AABB2.h>
#include <SysUtils/C_CharacterTable.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio::sysmath;
	using namespace scorpio::sysutils;
	using namespace scorpio::sysrender;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_BitmapText::C_BitmapText(sysutils::C_String strText, T_BitmapFont font, E_TranslationTableId trTableId)
		: m_font(font), m_text(strText), m_fScale(1.f), m_color(C_Vector4(1.0f, 1.0f, 1.0f, 1.0f)), m_zCoord(0.f), m_trTableId(trTableId)
	{
		if(m_trTableId != E_TT_NONE)
			m_text = ConvertUTF8ToAscii(m_text, m_trTableId);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_BitmapText::C_BitmapText(const sysmath::C_Vector2 & vctPos, sysutils::C_String strText, T_BitmapFont font, E_TranslationTableId trTableId)
		: m_font(font), m_text(strText), m_pos(vctPos), m_fScale(1.f), m_color(C_Vector4(1.0f, 1.0f, 1.0f, 1.0f)), m_zCoord(0.f), m_trTableId(trTableId)
	{
		if(m_trTableId != E_TT_NONE)
			m_text = ConvertUTF8ToAscii(m_text, m_trTableId);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BitmapText::Render() const
	{
		//podla scalu/rotace/pozice vypocitam dest rectangle a tam vyrendrujem fontom pismeno
		u32 nCount = m_text.length();
		C_Vector2 vctpos = m_pos;
		for (u32 i = 0; i < nCount; ++i)
		{
			C_Point size = m_font->GetCharacterSize(m_text[i]);
			C_Vector2 vctSize((float)size.x, (float)size.y);
			vctSize *= m_fScale;
			
			C_Vector2 dstPos(vctpos.m_x + vctSize.m_x / 2.f, vctpos.m_y - vctSize.m_y / 2.f);
			vctpos.m_x += vctSize.m_x;

			float fAngle = 0;	//TODO IGNORE ANGLE FOR NOW
			m_font->Render(dstPos, C_Point((s32)vctSize.m_x, (s32)vctSize.m_y), m_text[i], fAngle, m_color);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BitmapText::DebugDraw() const
	{
		//podla scalu/rotace/pozice vypocitam dest rectangle a tam vyrendrujem fontom pismeno
		u32 nCount = m_text.length();
		C_Vector2 vctSize;
		C_Point size;
		for (u32 i = 0; i < nCount; ++i)
		{
			size = m_font->GetCharacterSize(m_text[i]);
			vctSize.m_x += size.x;
		}
		vctSize.m_y = (float)size.y;

		vctSize *= m_fScale;
		C_Graphic graph;
		C_Vector4 dbgColor(1.f, .5f, 0.25f, 0.8f);	//orange text
		graph.RenderRect(m_pos + C_Vector2(vctSize.m_x / 2.f, -vctSize.m_y / 2.f), C_Point((s32)vctSize.m_x, (s32)vctSize.m_y), dbgColor, false, 0.f, 1.f);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetSize
	//
	//////////////////////////////////////////////////////////////////////////
	C_Point C_BitmapText::GetSize() const
	{
		u32 nCount = m_text.length();
		C_Vector2 vctpos;
		C_AABB2 bbox(vctpos);
		for (u32 i = 0; i < nCount; ++i)
		{
			C_Point size = m_font->GetCharacterSize(m_text[i]);
			C_Vector2 vctSize((float)size.x, (float)size.y);
			vctSize *= m_fScale;

			//C_FloatRect dstRect(vctpos.m_x, vctpos.m_y, (s32)vctSize.m_x, (s32)vctSize.m_y);

			vctpos.m_x += vctSize.m_x;
			vctpos.m_y = vctSize.m_y;

			bbox.Include(vctpos);
		}


		return C_Point((s32)bbox.GetWidth(), (s32)bbox.GetHeight());
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ConvertText	//JK FOR NOW HARDCODED
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_BitmapText::ConvertUTF8ToAscii(C_String inText, E_TranslationTableId tableId)
	{
		SC_ASSERT(tableId == E_TT_CZSK);
		const char * text = inText.c_str();

		char * buffer = new char[inText.length() + 1];
		char * buffTextPtr = buffer;
		u8 utf8Code = 0;

		while (*text != 0)
		{
			char c = *text;
			++text;
			if ((u8)c == 0xC3 || (u8)c == 0xC4 || (u8)c == 0xC5 || (u8)c == 0xC6 || (u8)c == 0xC7)
			{
				utf8Code = (u8)c;
				continue;
			}

			if (utf8Code)
			{
				c = T_CharacterTable::GetInstance().ConvertCharacter(utf8Code, c);
				utf8Code = 0;
			}
			
			*buffer = c;
			++buffer;
		}

		*buffer = 0;

		C_String strTmp = C_String(buffTextPtr);

		delete[] buffTextPtr;

		return strTmp;
	}

}}