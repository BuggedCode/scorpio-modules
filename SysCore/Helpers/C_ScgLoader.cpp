/*
* filename:			C_ScgLoader.cpp
*
* author:			Kral Jozef
* date:				01/22/2011		23:29
* version:			1.00
* brief:
*/

#include "C_ScgLoader.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/Xml/C_XmlDocument.h>
//#include <SysCore/Geometry/C_MeshManager.h>
#include <SysCore/Frames/C_Visual.h>
#include <SysCore/Frames/C_Area.h>
#include <SysCore/Frames/C_Point.h>

namespace scorpio{ namespace syscore{

	using namespace scorpio;
	using namespace scorpio::sysmath;
	using namespace scorpio::sysutils;

	static const T_Hash32	C_MESH_TYPE_NAME = C_HashName("Mesh2d").GetHash();
	static const T_Hash32	C_SHAPE_TYPE_NAME = C_HashName("Shape").GetHash();
	static const T_Hash32	C_HELPER_TYPE_NAME = C_HashName("Helper").GetHash();


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateFrame
	//
	//////////////////////////////////////////////////////////////////////////
	T_Frame C_ScgLoader::CreateFrame(const C_XmlNode & node) const
	{
		C_String strTypeName = node["TypeName"];
		C_String objectName = node["Name"];

		T_Frame retFrame;
		if (C_HashName(strTypeName).GetHash() == C_MESH_TYPE_NAME)
		{
			T_Mesh2d mesh;
			C_HashName meshUID(objectName);
			//JK do not store meshes into manager, because of same named meshes
			//if (!T_MeshManager::GetInstance().ContainsMesh(meshUID))
			{
				mesh = T_Mesh2d(new C_Mesh2d(objectName));
				bool bRes = mesh->InitializeFromNode(node);
				if (!bRes)
				{
					TRACE_FE("Failed to initialize mesh %s", objectName.c_str());
				}

				//T_MeshManager::GetInstance().AddMesh(meshUID, mesh);
			}
			/*else
			{
				mesh = T_MeshManager::GetInstance().GetMesh(meshUID);
			}*/
			retFrame = T_Frame(new C_Visual(mesh));
		}

		//////////////////////////////////////////////////////////////////////////
		//@ C_SHAPE_TYPE_NAME
		else if (C_HashName(strTypeName).GetHash() == C_SHAPE_TYPE_NAME)
		{
			T_PolyShape shape = T_PolyShape(new C_PolyShape(objectName));
			bool bRes = shape->InitializeFromNode(node);
			if (!bRes)
			{
				TRACE_FE("Failed to initialize mesh %s", objectName.c_str());
			}

			retFrame = T_Frame(new C_Area(shape));
		}

		//////////////////////////////////////////////////////////////////////////
		//@ C_HELPER_TYPE_NAME
		else if (C_HashName(strTypeName).GetHash() == C_HELPER_TYPE_NAME)
		{
			const C_XmlNode * helpPos = node.GetNode("Pos");
			if (!helpPos || !helpPos->IsValid())
			{
				TRACE_FE("Pos node in helper: %s is missing", objectName.c_str());
			}

			C_Vector2 vctHelpPos;
			helpPos->Read(vctHelpPos);

			const C_XmlNode * zPos = node.GetNode("CoordZ");
			float fZCoord = 0.f;
			if (!zPos || !zPos->IsValid())
			{
				TRACE_FE("CoordZ node in helper: %s is missing", objectName.c_str());
			}
			else
			{
				zPos->Read(fZCoord);
			}


			retFrame = T_Point(new syscore::C_Point(objectName, vctHelpPos));
			retFrame->SetZCoord(fZCoord);
		}
		
		//////////////////////////////////////////////////////////////////////////
		else
		{

			SC_ASSERT(false);
		}

		//@ load object properties
		if (retFrame)
		{
			const C_XmlNode * propNode = node.SelectNode("Properties");
			if (propNode && propNode->IsValid())
			{
				C_String objectProp;
				bool bResRead = propNode->Read(objectProp);
				if (bResRead)
				{
					retFrame->SetProperties(objectProp);
				}
				else
				{
					TRACE_FE("Unable to read object properties for node: %s !", objectName.c_str());
				}
			}
		}

		if (retFrame)
		{
			//get children and link them to frame
			T_XmlNodeList children = node.SelectNodes("Object");
			for (T_ConstXmlNodePtrVector::iterator iter = children->begin(); iter != children->end(); ++iter)
			{
				const C_XmlNode * child = *iter;
				SC_ASSERT(child);

				T_Frame childFrm = CreateFrame(*child);
				if (!childFrm)
				{
					TRACE_FE("Can not create frame from node: %s", child->GetName().c_str());
					continue;
				}

				retFrame->LinkToParent(childFrm);
			}
		}

		return retFrame;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Load
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ScgLoader::Load(const T_Stream inputStream)
	{
		TRACE_FI("Loading SCG file: %s", inputStream->GetFileName());

		C_XmlDocument xmlDoc;
		if (!xmlDoc.Load(inputStream))
		{
			TRACE_FE("Can not load file: %s", inputStream->GetFileName());
			return false;
		}

		//////////////////////////////////////////////////////////////////////////
		const C_XmlNode * root = xmlDoc.SelectNode("Scorpio_Geometry_Data");
		if (!root && !root->IsValid())
		{
			TRACE_FE("Invalid file: %s", inputStream->GetFileName());
			return false;
		}

		T_XmlNodeList objList = root->SelectNodes("Object");
		for (T_ConstXmlNodePtrVector::iterator iter = objList->begin(); iter != objList->end(); ++iter)
		{
			const C_XmlNode * objNode = *iter;
			SC_ASSERT(objNode);

			T_Frame frm = CreateFrame(*objNode);
			if (!frm)
			{
				TRACE_FE("Can not create frame from node: %s", objNode->GetName().c_str());
				continue;
			}

			m_Frames.push_back(frm);
		}

		return true;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetFrame
	//
	//////////////////////////////////////////////////////////////////////////
	T_Frame C_ScgLoader::GetFrame(T_Hash32 frameHash)
	{
		for (T_FrameList::iterator iter = m_Frames.begin(); iter != m_Frames.end(); ++iter)
		{
			if ((*iter)->GetHashName().GetHash() == frameHash)
				return *iter;
		}

		return T_Frame();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetFramesByName
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_ScgLoader::GetFramesByName(C_String namePrefix, T_FrameList & outList)
	{
		u32 prefNameLen = namePrefix.length();
		C_HashName prefHash(namePrefix);

		for (T_FrameList::iterator iter = m_Frames.begin(); iter != m_Frames.end(); ++iter)
		{
			T_Frame frame = (*iter);
			C_String frameName = frame->GetHashName().GetName();
			if (frameName.length() < prefNameLen)
				continue;

			C_String str = C_String::SubString(frameName, 0, prefNameLen - 1);
			C_HashName hashStr(str);
			if (hashStr == prefHash)
				outList.push_back(frame);
		}

		return (u32)outList.size();
	}




}}