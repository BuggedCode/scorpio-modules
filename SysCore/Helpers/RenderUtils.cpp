/*
* filename:			RenderUtils.cpp
*
* author:			Kral Jozef
* date:				01/25/2011		16:00
* version:			1.00
* brief:
*/

#include "RenderUtils.h"
#include <SysCore/Core/C_RenderManager.h>

namespace scorpio{ namespace syscore{


	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddFrameToRender
	//
	//////////////////////////////////////////////////////////////////////////
	void AddFrameToRender(T_Frame frame, u32 renderLayer)
	{
		T_RenderManager::GetInstance().Add(frame, renderLayer);

		T_FrameList outChildren;
		frame->GetAllChildren(outChildren);
		for (T_FrameList::iterator iter = outChildren.begin(); iter != outChildren.end(); ++iter)
		{
			AddFrameToRender(*iter, renderLayer);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RemoveFrameFromRender
	//
	//////////////////////////////////////////////////////////////////////////
	void RemoveFrameFromRender(T_Frame frame, u32 renderLayer)
	{
		T_RenderManager::GetInstance().Remove(frame, renderLayer);

		T_FrameList outChildren;
		frame->GetAllChildren(outChildren);
		for (T_FrameList::iterator iter = outChildren.begin(); iter != outChildren.end(); ++iter)
		{
			RemoveFrameFromRender(*iter, renderLayer);
		}
	}
}}