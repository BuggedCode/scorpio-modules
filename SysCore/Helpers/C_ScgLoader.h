/*
* filename:			C_ScgLoader.h
*
* author:			Kral Jozef
* date:				01/22/2011		23:28
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/SysCoreApi.h>
#include <vector>
//#include <SysCore/Geometry/C_Mesh2d.h>
//#include <SysCore/Geometry/C_PolyShape.h>
//#include <SysCore/Frames/C_Point.h>
#include <SysCore/Frames/C_Frame.h>
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ScgLoader
	class SYSCORE_API C_ScgLoader
	{
	public:
		//@ c-tor
		C_ScgLoader() {};


		//@ Load
		//@ relative fileName - platform independent relative path from data directory
		bool									Load(const sysutils::T_Stream inputStream);
		//@ GetFrameList
		T_FrameList						&	GetFrameList() { return m_Frames; }
		//@ GetFrame
		T_Frame								GetFrame(T_Hash32 frameHash);
		//@ GetFramesByName - SLOW function!
		u32									GetFramesByName(sysutils::C_String namePrefix, T_FrameList & outList);

	private:
		T_Frame								CreateFrame(const sysutils::C_XmlNode & node) const;

	private:
		//std::vector<T_Mesh2d>		m_MeshContainer;
		//std::vector<T_PolyShape>	m_ShapeContainer;
		//T_PointList						m_PointContainer;	//free points - linked under scene root

		//std::map<T_PolyShape, T_PointList>	m_helpers;
		T_FrameList						m_Frames;
	};

}}