/*
* filename:			SceneNodeVisitors.h
*
* author:			Kral Jozef
* date:				01/18/2011		1:45
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/Core/I_SceneVisitor.h>

namespace scorpio{ namespace syscore{

	//////////////////////////////////////////////////////////////////////////
	//@ C_HideFramesVisitor
	class C_HideFramesVisitor : public I_SceneVisitor
	{
	public:
		//@ c-tor
		C_HideFramesVisitor(bool bHide)
			: m_Hide(bHide) {};


		//@ Visit
		void				Visit(T_Frame frame)
		{
			SC_ASSERT(frame);
			frame->SetVisible(!m_Hide);
		}

	private:
		bool			m_Hide;	//Hide/Unhide flag
	};


}}