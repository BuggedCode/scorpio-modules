/*
* filename:			RenderUtils.h
*
* author:			Kral Jozef
* date:				01/25/2011		16:00
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/Core/I_RenderObject.h>
#include <SysCore/Frames/C_Frame.h>
#include <SysCore/SysCoreApi.h>

namespace scorpio{ namespace syscore{


	//////////////////////////////////////////////////////////////////////////
	//@ C_RenderObjectSorter
	class C_RenderObjectSorter
	{
		public:
			bool operator ()(const T_WeakRenderObject roLeft, const T_WeakRenderObject roRight) const
			{
				SC_ASSERT(roLeft.expired() == false || roRight.expired() == false);
				T_RenderObject roLeftInst(roLeft.lock());
				T_RenderObject roRightInst(roRight.lock());

				if (roLeftInst->GetZCoord() < roRightInst->GetZCoord())
				{
					return true;
				}
				return false;
			}
	};



	//@ AddFrameToRender - recursive
	void SYSCORE_API AddFrameToRender(T_Frame frame, u32 renderLayer);
	//@ RemoveFrameFromRender
	void SYSCORE_API RemoveFrameFromRender(T_Frame frame, u32 renderLayer);

}}