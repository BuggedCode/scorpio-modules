/*
* filename:			C_MiscUtils.cpp
*
* author:			Kral Jozef
* date:				02/16/2011		23:09
* version:			1.00
* brief:
*/


#include "C_MiscUtils.h"
#include <SysUtils/C_String.h>

#ifdef PLATFORM_WIN32
	#include <stdlib.h>
	#include <Windows.h>
	#include <SDL/include/SDL_timer.h>
#elif defined PLATFORM_MACOSX
	#include <SDL/SDL_timer.h>
#endif


namespace scorpio{ namespace syscore{

	using namespace sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	void C_MiscUtils::Initialize()
	{
#ifdef USE_PTK
		KMiscToolsW::initMiscTools();
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetTicks
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_MiscUtils::GetMilliseconds()
	{
#ifdef USE_PTK
		return KMiscToolsW::getMilliseconds();
#else
		return (u32)SDL_GetTicks();
#endif
	}

}}