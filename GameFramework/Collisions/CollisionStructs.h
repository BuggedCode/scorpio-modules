/*
* filename:			CollisionStructs.h
*
* author:			Kral Jozef
* date:				10/5/2011		18:53
* version:			1.00
* brief:				vary collision structs
*/

#pragma once

#include <GameFramework/Entities/C_Actor.h>
#include <SysPhysics/PhysicsCommon.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ S_RayCastGameResult
	struct S_RayCastGameResult
	{
		T_Actor								m_Actor;
		sysmath::C_Vector2				m_Intersect;
		sysmath::C_Vector2				m_RayStart;
		sysphysics::S_RayCastResult	m_Result;

		//@ Clear
		void Clear()
		{
			m_Intersect = sysmath::C_Vector2::C_ZERO;
			m_RayStart = sysmath::C_Vector2::C_ZERO;
			m_Actor.reset(); 
			m_Result.Clear(); 
		}
	};



	//////////////////////////////////////////////////////////////////////////
	//@ S_QueryGameResult
	struct S_QueryAABBGameResult
	{
		//@ c-tor
		S_QueryAABBGameResult() : m_InputAABB(NULL) {};
		const sysmath::C_AABB2 *	m_InputAABB;
		T_ActorList						m_ActorList;

		//@ Clear
		void Clear()
		{
			m_ActorList.clear();
		}
	};
}}