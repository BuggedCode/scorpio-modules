#pragma once

#include <Common/Pragmas.h>

#ifdef _USRDLL
	#ifdef GAMEFRAMEWORK_EXPORTS
		#define GAMEFRAMEWORK_API __declspec(dllexport)
		#define GAMEFRAMEWORK_EXP_TEMP_INST
	#else
		#define GAMEFRAMEWORK_API __declspec(dllimport)
		#define GAMEFRAMEWORK_EXP_TEMP_INST extern
	#endif
#else
	#define GAMEFRAMEWORK_API
	#define GAMEFRAMEWORK_EXP_TEMP_INST
#endif