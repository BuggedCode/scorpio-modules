/*
* filename:			C_StringTable.h
*
* author:			Kral Jozef
* date:				04/29/2011		15:39
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Common/C_Singleton.h>
#include <SysUtils/C_String.h>
#include <SysUtils/FileSystem/C_Stream.h>
#include <map>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_StringTable
	class GAMEFRAMEWORK_API C_StringTable
	{
		friend class C_Singleton<C_StringTable>;

	public:
		//@ Load
		//@ fileName - abs filePath
		bool						Load(const sysutils::T_Stream inputStream);
		//@ GetText
		sysutils::C_String	GetText(T_Id32 textId) const;

	private:
		//@ c-tor
		C_StringTable() {};

		//@ TODO unorderedmap
		typedef std::map<T_Id32, sysutils::C_String>	T_StringContainer;
		T_StringContainer		m_Strings;
	};


	typedef C_Singleton<C_StringTable>	T_StringTable;

}}

GAMEFRAMEWORK_EXP_TEMP_INST template class GAMEFRAMEWORK_API C_Singleton<scorpio::framework::C_StringTable>;