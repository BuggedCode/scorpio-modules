/*
* filename:			C_StringTable.cpp
*
* author:			Kral Jozef
* date:				04/29/2011		15:42
* version:			1.00
* brief:
*/

#include "C_StringTable.h"
#include <SysUtils/C_TraceClient.h>
#include <fstream>
#include <SysUtils/C_HashName.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Load
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_StringTable::Load(const sysutils::T_Stream inputStream)
	{
		if (!inputStream)
			return false;

		const u32 C_BUFFER_SIZE = 1024;
		char buffer[C_BUFFER_SIZE];
		ZERO_ARRAY(buffer);

		while (!inputStream->EndOfStream())
		{
			inputStream->ReadLine(buffer, C_BUFFER_SIZE);
			C_String str(buffer);
			std::vector<C_String> outVector;
			str.Split(',', outVector);
			if (outVector.empty())
			{
				//try to use comma separator
				str.Split('.', outVector);
			}

			if (outVector.empty() || outVector.size() > 2)
			{
				TRACE_FE("Wrong line in StringTableFile: %s", inputStream->GetFileName());
				continue;
			}


			C_String strText;
			if (outVector.size() == 2)
			{
				strText = outVector[1];
				//erase free spaces at the start/end & dblQuotas
				strText = strText.TrimLeft(' ');
				strText = strText.TrimRight(' ');
				strText = strText.TrimLeft('\"');
				strText = strText.TrimRight('\"');
			}

			C_String trextId = outVector[0];
			C_HashName hash(trextId);

			m_Strings.insert(T_StringContainer::value_type(hash.GetHash(), strText));
		}
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetText
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_StringTable::GetText(T_Id32 textId) const
	{
		T_StringContainer::const_iterator iter = m_Strings.find(textId);
		if (iter != m_Strings.end())
			return iter->second;
		
		TRACE_FE("Can not find StringId: %d", textId);
		return C_String();
	}

}}