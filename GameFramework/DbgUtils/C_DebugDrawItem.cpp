/*
* filename:			C_DebugWindow.h
*
* author:			Kral Jozef
* date:				03/22/2009		15:18
* version:			1.00
* brief:				DebugDrawItem for DebugWindow
*/

#include "C_DebugDrawItem.h"

namespace scorpio{ namespace framework{

	using namespace sysmath;


	//////////////////////////////////////////////////////////////////////////
	//@ C_DebugDrawItem::Draw
	void C_DebugDrawItem::Draw(const C_Vector2 & vctPos, T_TrueText pFont) const
	{
		pFont->SetColor(m_color);
		pFont->DrawStringFromLeft(m_text, vctPos);
	}


}}