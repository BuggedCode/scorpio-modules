/*
* filename:			C_DebugDrawTool.cpp
*
* author:			Kral Jozef
* date:				02/28/2010		10:59
* version:			1.00
* brief:				Tool for drawing simple primitives
*/

#include "C_DebugDrawTool.h"
#include <SysCore/Render/C_Graphic.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace sysmath;

	sysmath::C_Vector4 C_DebugDrawTool::m_color = sysmath::C_Vector4(1.f, 1.f, 1.f, 1.f);

	const u16 C_MAX_PRIMITIVES = 12048;


	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_DebugDrawTool::C_DebugDrawTool()
		: m_bEnabled(false)
	{
		m_primitives.reserve(C_MAX_PRIMITIVES);
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_DebugDrawTool::Render() const
	{
		if (!m_bEnabled)
			return;

		sysrender::C_Graphic dbgGraph;
		u32 nSize = (u32)m_primitives.size();
		u32 nIndex = 0;
		for (u32 i = 0; i < nSize; ++i)
		{
			const S_DDPrimitive & sdp = m_primitives[i];
			dbgGraph.RenderLine(sdp.m_pos0, sdp.m_pos1, sdp.m_color);
			if (sdp.m_LifeTime > 0)
			{
				sdp.m_LifeTime -= 16;	//60FPS; hardcoded
				m_primitives[nIndex] = sdp;
				++nIndex;
			}
		}

		m_primitives.resize(nIndex);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DrawRect
	//
	//////////////////////////////////////////////////////////////////////////
	void C_DebugDrawTool::DrawRect(const scmath::C_Vector2 & pos0, const scmath::C_Vector2 & pos1, const scmath::C_Vector2 & pos2, const scmath::C_Vector2 & pos3, const scmath::C_Vector4 & color /* = m_color */)
	{
		if (!m_bEnabled)
			return;

		m_primitives.push_back(S_DDPrimitive(color, pos0, pos1));
		m_primitives.push_back(S_DDPrimitive(color, pos1, pos2));
		m_primitives.push_back(S_DDPrimitive(color, pos2, pos3));
		m_primitives.push_back(S_DDPrimitive(color, pos3, pos0));

		SC_ASSERT(m_primitives.size() <= C_MAX_PRIMITIVES);	//perf problem
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DrawRect
	//
	//////////////////////////////////////////////////////////////////////////
	void C_DebugDrawTool::DrawRect(const scmath::C_Vector2 & pivot, float fWidth, float fHeight, const scmath::C_Vector4 & color /*= m_color*/)
	{
		if (!m_bEnabled)
			return;


		float fHalfWidth = fWidth/2.f;
		float fHalfHeight = fHeight/2.f;

		C_Vector2 vct1(pivot.m_x - fHalfWidth, pivot.m_y - fHalfHeight);
		C_Vector2 vct2(pivot.m_x + fHalfWidth, pivot.m_y - fHalfHeight);
		C_Vector2 vct3(pivot.m_x + fHalfWidth, pivot.m_y + fHalfHeight);
		C_Vector2 vct4(pivot.m_x - fHalfWidth, pivot.m_y + fHalfHeight);
		
		m_primitives.push_back(S_DDPrimitive(color, vct1, vct2));
		m_primitives.push_back(S_DDPrimitive(color, vct2, vct3));
		m_primitives.push_back(S_DDPrimitive(color, vct3, vct4));
		m_primitives.push_back(S_DDPrimitive(color, vct4, vct1));

		SC_ASSERT(m_primitives.size() <= C_MAX_PRIMITIVES);	//perf problem
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DrawLine
	//
	//////////////////////////////////////////////////////////////////////////
	void C_DebugDrawTool::DrawLine(const scmath::C_Vector2 & pos0, const scmath::C_Vector2 & pos1, const scmath::C_Vector4 & color /* = m_color */)
	{
		if (!m_bEnabled)
			return;

		m_primitives.push_back(S_DDPrimitive(color, pos0, pos1));

		SC_ASSERT(m_primitives.size() <= C_MAX_PRIMITIVES);	//perf problem
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DrawPoint
	//
	//////////////////////////////////////////////////////////////////////////
	void C_DebugDrawTool::DrawPoint(const scmath::C_Vector2 & vctPos, float fHalfSize, const scmath::C_Vector4 & color /* = m_color */)
	{
		if (!m_bEnabled)
			return;

		m_primitives.push_back(S_DDPrimitive(color, vctPos, vctPos - C_Vector2(fHalfSize, 0.f)));
		m_primitives.push_back(S_DDPrimitive(color, vctPos, vctPos + C_Vector2(fHalfSize, 0.f)));
		m_primitives.push_back(S_DDPrimitive(color, vctPos, vctPos - C_Vector2(0.f, fHalfSize)));
		m_primitives.push_back(S_DDPrimitive(color, vctPos, vctPos + C_Vector2(0.f, fHalfSize)));

		SC_ASSERT(m_primitives.size() <= C_MAX_PRIMITIVES);	//perf problem
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DrawCircle
	//
	//////////////////////////////////////////////////////////////////////////
	void C_DebugDrawTool::DrawCircle(const scmath::C_Vector2 & vctPos, float fRadius, const scmath::C_Vector4 & color /* = m_color */, u32 lifeTime)
	{
		if (!m_bEnabled)
			return;

		float fAngle = 0;
		for (u32 i = 0; i < 12; ++i)
		{
			C_Vector2 vctDir(fRadius, 0.f);
			vctDir.rotateBy(fAngle);
			C_Vector2 vctStart = vctPos + vctDir;

			vctDir.rotateBy(30);	//another 30 degree
			C_Vector2 vctEnd = vctPos + vctDir;

			m_primitives.push_back(S_DDPrimitive(color, vctStart, vctEnd, lifeTime));

			fAngle += 30;
		}

		SC_ASSERT(m_primitives.size() <= C_MAX_PRIMITIVES);	//perf problem
	}

}}