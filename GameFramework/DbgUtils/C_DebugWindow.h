/*
* filename:			C_DebugWindow.h
*
* author:			Kral Jozef
* date:				03/22/2009		15:18
* version:			1.00
* brief:				simple window with debug information
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <GameFramework/Core/C_Window.h>
#include "C_DebugDrawItem.h"
#include <SysCore/Render/C_TrueText.h>
#include <vector>

namespace scorpio{ namespace framework{


	//////////////////////////////////////////////////////////////////////////
	//@ C_DebugWindow
	class GAMEFRAMEWORK_API C_DebugWindow : public C_Window
	{
		typedef C_Window	T_Super;
	public:
		//@ c-tor
		C_DebugWindow(const sysutils::C_String strWndName);
		//@ d-tor
		~C_DebugWindow();

		//@ HandleEvent
		bool			HandleEvent() { return true; };
		//@
		void			Draw();

		//@ AddItem
		T_DebugDrawItem		AddItem(s16 priority = -1);
		//@ GetItem
		//C_DebugDrawItem	*	GetItem(const sysutils::C_String & itemName) const;
		//@ RemoveItem
		bool						RemoveItem(const T_DebugDrawItem ddItem);
		//@ AddText
		void						AddText(const sysutils::C_String strText);
		//@ Clear
		void						Clear() { m_ddItems.clear(); }

	private:
		typedef std::vector<T_DebugDrawItem>	T_DebugDrawContainer;
		T_DebugDrawContainer		m_ddItems;
		T_TrueText					m_Font;
	};

	typedef boost::shared_ptr<C_DebugWindow>	T_DebugWindow;


}}