/*
* filename:			C_DebugDrawTool.h
*
* author:			Kral Jozef
* date:				02/28/2010		10:59
* version:			1.00
* brief:				Tool for drawing simple primitives
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Common/C_Singleton.h>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Vector2.h>
#include <vector>

namespace scorpio{ namespace framework{

	namespace scmath = scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ C_DebugDrawTool
	class GAMEFRAMEWORK_API C_DebugDrawTool
	{
		friend class C_Singleton<C_DebugDrawTool>;

	public:
		//@ OnEnableDebugDraw
		void		OnEnableDebugDraw() { m_bEnabled = !m_bEnabled; }
		//@ SetEnabled
		void		SetEnabled(bool bEnable) { m_bEnabled = bEnable; }
		//@ IsEnabled
		bool		IsEnabled() const { return m_bEnabled; }
		//@ Render
		void		Render() const;

		//@ DrawLine
		void		DrawLine(const scmath::C_Vector2 & pos0, const scmath::C_Vector2 & pos1, const scmath::C_Vector4 & color = m_color);
		//@ DrawRect
		void		DrawRect(const scmath::C_Vector2 & pos0, const scmath::C_Vector2 & pos1, const scmath::C_Vector2 & pos2, const scmath::C_Vector2 & pos3, const scmath::C_Vector4 & color = m_color);
		//@ DrawRect
		void		DrawRect(const scmath::C_Vector2 & pivot, float fWidth, float fHeight, const scmath::C_Vector4 & color = m_color);
		//@ DrawPoint
		void		DrawPoint(const scmath::C_Vector2 & vctPos, float fHalfSize, const scmath::C_Vector4 & color = m_color);
		//@ DrawCircle
		//@ lifeTime - in ms
		void		DrawCircle(const scmath::C_Vector2 & vctPos, float fRadius, const scmath::C_Vector4 & color = m_color, u32 lifeTime = 0);


	private:
		//@ c-tor
		C_DebugDrawTool();

		static scmath::C_Vector4	m_color;
		bool								m_bEnabled;

		//@ DebugDrawPrimitive
		struct S_DDPrimitive
		{
			//@ c-tor - this c-tor is just for resize (but will never be used)
			S_DDPrimitive() : m_LifeTime(0) {;}
			//@ c-tor
			S_DDPrimitive(const scmath::C_Vector4 & color, const scmath::C_Vector2 & pos0, const scmath::C_Vector2 & pos1)
				: m_color(color), m_pos0(pos0), m_pos1(pos1), m_LifeTime(0) {};
			//@ c-tor
			S_DDPrimitive(const scmath::C_Vector4 & color, const scmath::C_Vector2 & pos0, const scmath::C_Vector2 & pos1, u32 lifeTime)
				: m_color(color), m_pos0(pos0), m_pos1(pos1), m_LifeTime(lifeTime) {};
		

			scmath::C_Vector4	m_color;
			scmath::C_Vector2	m_pos0;
			scmath::C_Vector2	m_pos1;
			mutable u32			m_LifeTime;	//in ms
		};

		typedef std::vector<S_DDPrimitive>	T_DDPrimitiveContainer;
		mutable T_DDPrimitiveContainer	m_primitives;	//v render sa clearuje
	};



	typedef C_Singleton<C_DebugDrawTool>	T_DebugDrawTool;

}}

GAMEFRAMEWORK_EXP_TEMP_INST template class GAMEFRAMEWORK_API C_Singleton<scorpio::framework::C_DebugDrawTool>;