/*
* filename:			C_DebugWindow.h
*
* author:			Kral Jozef
* date:				03/22/2009		15:18
* version:			1.00
* brief:				DebugDrawItem for DebugWindow
*/


#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Point.h>
#include <SysUtils/C_String.h>
#include <SysCore/Render/C_TrueText.h>
#include <boost/shared_ptr.hpp>


namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_DebugDrawItem
	class GAMEFRAMEWORK_API C_DebugDrawItem
	{
	public:
		//@ c-tor
		C_DebugDrawItem() : m_color(1.f,1.f,1.f,1.f) {};
		sysutils::C_String		m_text;
		sysmath::C_Vector4		m_color;

	private:

		friend class C_DebugWindow;

		//@ Draw
		//@ param pFont could be changed
		void						Draw(const sysmath::C_Vector2 & vctPos, T_TrueText pFont) const;
	};

	typedef boost::shared_ptr<C_DebugDrawItem>	T_DebugDrawItem;

}}