/*
* filename:			C_DebugWindow.cpp
*
* author:			Kral Jozef
* date:				03/22/2009		15:18
* version:			1.00
* brief:				simple window with debug information
*/

#include "C_DebugWindow.h"
#include <SysCore/Render/C_Graphic.h>
#include <SysCore/Core/C_FontManager.h>
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysUtils/Utils/C_FileSystemUtils.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace sysmath;
	using namespace sysutils;
	using namespace syscore;


	static const u16	C_DEBUG_WND_POS_X		= 1;
	static const u16	C_DEBUG_WND_POS_Y		= 1;
	static const u16	C_DEBUG_WND_SIZE_X	= 250;
	static const u16	C_DEBUG_WND_SIZE_Y	= 80;

	static const u16	C_DEBUG_FONT_HEIGHT	= 12;
	static const C_Vector4 C_DEBUG_FONT_COLOR = C_Vector4(1.f, 1.f, 1.f, 1.f);


	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_DebugWindow::C_DebugWindow(const C_String strWndName)
		: C_Window(C_DEBUG_WND_POS_X, C_DEBUG_WND_POS_Y, C_DEBUG_WND_SIZE_X, C_DEBUG_WND_SIZE_Y, strWndName, NULL, false)
	{
		m_bEnabled = false;

		C_HashName fontHashUID(C_String::Format("DEBUG_FONT_%d", this));
		bool bRes = T_FontManager::GetInstance().AddFont(fontHashUID, "Data/fonts/arial.ttf", C_DEBUG_FONT_HEIGHT);

		const char * arch_path = C_FileSystemUtils::MakeFilePath("\\Data\\Packages\\Engine.zip");
		bRes = T_FileSystemManager::GetInstance().OpenArchive(arch_path);
		TRACE_FU(bRes, "Open archive %s", arch_path, bRes ? "OK" : "FAILED");

		bRes = T_FontManager::GetInstance().PreloadFonts();
		SC_ASSERT(bRes);
		m_Font = T_FontManager::GetInstance().GetFont(fontHashUID);
		SC_ASSERT(m_Font && "Couldn't create font!");
		m_Font->SetColor(C_DEBUG_FONT_COLOR);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	C_DebugWindow::~C_DebugWindow()
	{
		m_ddItems.clear();
	}

	//////////////////////////////////////////////////////////////////////////
	// Draw
	void C_DebugWindow::Draw()
	{
		SC_ASSERT(m_Font);
		C_Vector2 vctPos(3.f, 3.f);
		for (T_DebugDrawContainer::const_iterator iter = m_ddItems.begin(); iter != m_ddItems.end(); ++iter)
		{
			//ClientToScreen(pos);
			(*iter)->Draw(vctPos, m_Font);
			vctPos.m_y += (C_DEBUG_FONT_HEIGHT + 1);
		}

		sysrender::C_Graphic graph;
		C_Vector4 colorBkg(.5f, .5f, .5f, .3f);

		C_Vector2 wndPivot(m_rect.GetWidth() / 2.f, m_rect.GetHeight() / 2.f);
		graph.RenderRect(wndPivot, C_Point(m_rect.GetWidth(), m_rect.GetHeight()), colorBkg, false, 0.f, 1.f);
		T_Super::DbgDraw();
	}

	//////////////////////////////////////////////////////////////////////////
	//@ AddItem
	//////////////////////////////////////////////////////////////////////////
	T_DebugDrawItem C_DebugWindow::AddItem(s16 priority)
	{
		T_DebugDrawItem newDDItem = T_DebugDrawItem(new C_DebugDrawItem());
		if (priority == -1 || priority >= (s32)m_ddItems.size())
		{
			m_ddItems.push_back(newDDItem);
		}
		else
		{
			SC_ASSERT(false);
		}

		return newDDItem;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddText
	//
	//////////////////////////////////////////////////////////////////////////
	void C_DebugWindow::AddText(const C_String strText)
	{
		T_DebugDrawItem newDDItem = T_DebugDrawItem(new C_DebugDrawItem());
		m_ddItems.push_back(newDDItem);
		newDDItem->m_text = strText;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ RemoveItem
	//////////////////////////////////////////////////////////////////////////
	bool C_DebugWindow::RemoveItem(const T_DebugDrawItem ddItem)
	{
		for (T_DebugDrawContainer::iterator iter = m_ddItems.begin(); iter != m_ddItems.end(); ++iter)
		{
			if (iter->get() == ddItem.get())
			{
				m_ddItems.erase(iter);
				return true;
			}
		}
		return false;
	}


}}