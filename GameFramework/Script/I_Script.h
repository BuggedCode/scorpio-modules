/*
* filename:			I_Script.h
*
* author:			Kral Jozef
* date:				10/28/2010		20:46
* version:			1.00
* brief:				Base Interface for Script
*/

#pragma once

#include <boost/shared_ptr.hpp>

namespace scorpio{
	namespace syscore{
		class C_Scene;
	}
}

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ I_Script
	class I_Script
	{
	public:
		//@ c-tor
		I_Script() : m_Finished(false), m_Paused(false) {};
		//@ d-tor
		virtual ~I_Script() {};

		//@ Initialize - in this time script can request for usefull info
		virtual bool	Initialize(syscore::C_Scene & scene) { m_Scene = &scene; return true; }
		//@ Update
		virtual void	Update(u32 inDeltaTime) = 0;
		//@ IsFinished
		bool				IsFinished() const { return m_Finished; }
		//@ Pause
		virtual void	Pause(bool bPause) { m_Paused = bPause; }
		//@ IsPaused
		bool				IsPaused() const { return m_Paused; }

	protected:
		syscore::C_Scene	*	m_Scene;
		bool						m_Finished;		//@ script is finished
		bool						m_Paused;		//@ sxript is paused
	};


	typedef boost::shared_ptr<I_Script>	T_Script;

}}