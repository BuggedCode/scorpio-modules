/*
* filename:			Types.h
*
* author:			Kral Jozef
* date:				10/12/2010		21:30
* version:			1.00
* brief:
*/

#pragma once

#include <vector>
#include <SysUtils/C_HashName.h>
#include <SysCore/Frames/C_Visual.h>

	typedef std::vector<scorpio::sysutils::C_HashName>	T_HashNameVector;	//seznam hashNamu
	typedef std::vector<scorpio::sysutils::C_HashName>	T_HashNameList;	//seznam hashNamu
	typedef std::vector<scorpio::syscore::T_Visual>	T_VisualContainer;	//seznam visualu
	typedef std::vector<scorpio::syscore::T_Frame>	T_FrameContainer;	//seznam framu
