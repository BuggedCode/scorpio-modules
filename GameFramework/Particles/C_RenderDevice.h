/*
* filename:			C_RenderDevice.h
*
* author:			Kral Jozef
* date:				06/17/2009		21:21
* version:			1.00
* brief:				render device for particles inherited from pyro::IDevice
*/

#pragma once

#include <Pyro/Include/PyroGraphics.h>
#include "C_VertexBuffer.h"
#include <SysCore/Render/RenderTypes.h>
#include <Common/Assert.h>

namespace scorpio{ namespace effects{

	namespace pyrographics = PyroParticles::PyroGraphics;

	//////////////////////////////////////////////////////////////////////////
	//@ C_RenderDevice
	class C_RenderDevice : public PyroParticles::PyroGraphics::IDevice
	{
	public:
		//@ c-tor
		C_RenderDevice()
			: m_blendMode(sysrender::E_BM_SRC_ALPHA__DST_ONE_MINUS_ALPHA), m_texture(NULL) {}
		//@ d-tor
		~C_RenderDevice() { m_texture = NULL; }

		//@ SetRenderState
		virtual PyroResult SetRenderState(u32 State, u32 Value)
		{
			/*u32 glState = 0;
			switch (State)
			{
			case RS_ALPHABLENDENABLE:
				glState = GL_BLEND;
				break;
			default:
				return PyroOK;
			};	//switch

			if (Value)
				glEnable(glState);
			else
				glDisable(glState);*/

			return PyroOK;
		}
		
		//@ SetBlendFunc
		virtual PyroResult SetBlendFunc(EBlend SrcBlend, EBlend DestBlend);
		
		//@ CreateVertexBuffer
		virtual PyroResult CreateVertexBuffer(pyrographics::IVertexBuffer **ppVertexBuffer, u32 Vertices, u32 VertexSize, u32 VertexFormat, u32 Flags)
		{
			//@ vertex buffer vytvaram ja ale rusi ho pyro::particleLibrary ktora poziadala o jeho vytvorenie
			*ppVertexBuffer = new C_VertexBuffer(Vertices, VertexSize, VertexFormat);
			return PyroOK;
		}
		
		//@ CreateIndexBuffer
		virtual PyroResult CreateIndexBuffer(pyrographics::IIndexBuffer **ppIndexBuffer, u32 Indexes, u32 IndexFormat, u32 Flags)
		{
			return PyroOK;
		}
		
		//@ CreateTexture
		virtual PyroResult CreateTexture(pyrographics::ITexture **ppTexture, pyrographics::CBitmap *pBitmap, u32 Flags);
		
		//@ CreateBitmap
		virtual PyroResult CreateBitmap(pyrographics::CBitmap **ppBitmap, u8 *pBuffer, u32 Width, u32 Height, u32 Flags);

		
		//@ SetWorldMatrix
		//@ Modifies the current world transformation
		virtual PyroResult SetWorldMatrix(float WorldMatrix[3][4])
		{
			return PyroOK;
		}

		//@ SetTexture
		//@ Assigns a texture to a stage for a device
		virtual PyroResult SetTexture(u32 Stage, pyrographics::ITexture *pTexture)
		{
			m_texture = pTexture;
			SC_ASSERT(m_texture);
			return PyroOK;
		}

		//@ RenderQuads
		virtual PyroResult RenderQuads(pyrographics::IVertexBuffer *pVertexBuffer, u32 nFirstQuad, u32 nQuads, u32 Flags);

		//@ IsRGBA
		virtual PyroBool IsRGBA() const { return PyroTrue; }


	private:
		pyrographics::ITexture	*	m_texture;	//pointer to current pyro texture
		sysrender::E_BlendingMode	m_blendMode;	//blending mode value for PTK engine

	};

}}
