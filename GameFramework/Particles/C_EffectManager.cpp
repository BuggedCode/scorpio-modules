/*
* filename:			C_EffectManager.cpp
*
* author:			Kral Jozef
* date:				06/18/2009		20:33
* version:			1.00
* brief:
*/

#include "C_EffectManager.h"
#include <SysUtils/Xml/C_XmlFile.h>
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace effects{

	using namespace sysutils;

	static const C_String C_EFFECT_FILE_EXTENSION = ".pyro";

	static const char * C_EFF_ATT_UID = "uid";


	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_EffectManager::~C_EffectManager()
	{
		for (T_TemplateContainer::iterator iter = m_templContainer.begin(); iter != m_templContainer.end(); ++iter)
			iter->second->Deinitialize();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EffectManager::Clear()
	{
		m_templContainer.clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadTemplates
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_EffectManager::LoadTemplates(const T_Stream inputStream)
	{
		//naparsujem xml a povytvaram templaty aj zinicializujem
		C_XmlFile file;
		bool bRes = file.Load(inputStream);
		if (!bRes)
			return false;

		//parse xml
		if (!file.IsValid())
			return false;

		const C_XmlNode * root = file.GetRoot();
		if (!root || !root->IsValid())
			return false;

		if (root->GetName() != "effect_templates_definition")
			return false;

		C_XmlNode::const_child_iterator it_begin = root->BeginChild("template");
		C_XmlNode::const_child_iterator it_end	= root->EndChild("template");

		//@ spracuju vsechny templaty v subore

		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
			{
				bRes = false;
				continue;
			}

			bRes &= CreateTemplateFromNode(tempNode);
		}

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ LoadTemplates
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_EffectManager::LoadTemplates(const sysutils::T_XmlNodeList effectNodeList)
	{
		SC_ASSERT(effectNodeList);
		bool bRes = true;
		for (T_ConstXmlNodePtrVector::const_iterator iter = effectNodeList->begin(); iter != effectNodeList->end(); ++iter)
		{
			const C_XmlNode * effNode = *iter;
			SC_ASSERT(effNode);
			if (!effNode || !effNode->IsValid())
			{
				TRACE_E("INVALID effect node in effectNodeList.");
				bRes = false;
				continue;
			}

			bRes &= CreateTemplateFromNode(*effNode);
		}

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateTemplateFromNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_EffectManager::CreateTemplateFromNode(const C_XmlNode & templNode)
	{
		const C_XmlAttribute * uidAtt = templNode.GetAttribute(C_EFF_ATT_UID);
		if (!uidAtt)
		{
			TRACE_FE("Template node: '%s' has not set UniqueID!", templNode.GetName().c_str());
			return false;
		}

		C_String templateUID = uidAtt->GetValue();
		C_HashName templateUIDhash(templateUID);

		const C_XmlAttribute * fileRefAtt = templNode.GetAttribute("file");
		if (!fileRefAtt)
		{
			TRACE_FE("Template UID: '%s'has not set effect file-reference!", templateUID.c_str());
			return false;
		}

		C_String fileRef = fileRefAtt->GetValue();
		fileRef += C_EFFECT_FILE_EXTENSION;


		T_EffectTemplate effectTempl = T_EffectTemplate(new C_EffectTemplate(templateUIDhash, fileRef));
		SC_ASSERT(effectTempl);

		std::pair<T_TemplateContainer::iterator, bool> result = m_templContainer.insert(T_TemplateContainer::value_type(templateUIDhash, effectTempl));
		if (!result.second)
			TRACE_FE("EffectTemplate with ID: '%s' is already in manager!", templateUID.c_str());

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CreateEffect
	//
	//////////////////////////////////////////////////////////////////////////
	T_Effect C_EffectManager::CreateEffect(const sysutils::C_HashName & effectTemplateUID) const
	{
		T_TemplateContainer::const_iterator iter = m_templContainer.find(effectTemplateUID);
		if (iter == m_templContainer.end())
		{
			TRACE_FE("EffectManager: Can not find template: '%s'", (const char*)effectTemplateUID.GetName());
			return T_Effect();
		}

		T_Effect effect = iter->second->CreateEffect();
		return effect;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ClearTemplates
	//
	//////////////////////////////////////////////////////////////////////////
	u32 C_EffectManager::ClearTemplates(const sysutils::T_HashNameList & hashList)
	{
		u32 nCounter = 0;
		for (T_HashNameList::const_iterator iter = hashList.begin(); iter != hashList.end(); ++iter)
		{
			T_Hash32 hash = iter->GetHash();
			T_TemplateContainer::iterator it = m_templContainer.find(hash);
			if (it == m_templContainer.end())
				continue;
			
			it->second->Deinitialize();
			++nCounter;
			m_templContainer.erase(it);
		}

		return nCounter;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_EffectManager::ReleaseInternalResources()
	{
		bool bRes = true;
		for (T_TemplateContainer::iterator iter = m_templContainer.begin(); iter != m_templContainer.end(); ++iter)
			bRes &= iter->second->ReleaseResources();

		return bRes;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreInternalResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_EffectManager::RestoreInternalResources()
	{
		bool bRes = true;
		for (T_TemplateContainer::iterator iter = m_templContainer.begin(); iter != m_templContainer.end(); ++iter)
			bRes &= iter->second->RestoreResources();
		
		return bRes;
	}

}}