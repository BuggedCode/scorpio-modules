/*
* filename:			C_VertexBuffer.h
*
* author:			Kral Jozef
* date:				06/17/2009		22:56
* version:			1.00
* brief:				Vertex buffer for particles inherited from pyro::IVertexBuffer
*/

#pragma once

#include <Pyro/Include/PyroGraphics.h>
#include <SysCore/Render/I_VertexBuffer.h>
#include <Common/macros.h>

namespace scorpio{ namespace effects{

	//////////////////////////////////////////////////////////////////////////
	//@ C_VertexBuffer
	class C_VertexBuffer : public PyroParticles::PyroGraphics::IVertexBuffer, public sysrender::I_VertexBuffer
	{
	public:
		//@ c-tor
		C_VertexBuffer(u32 numVerticies, u32 vertexSize, u32 vertexFormat)
			: m_numVerticies(numVerticies), m_vertexSize(vertexSize), m_vertexFormat(vertexFormat)
		{
			m_data = new u8[m_numVerticies * vertexSize];
		}

		//@ d-tor
		virtual ~C_VertexBuffer()
		{
			SC_SAFE_DELETE(m_data);
		}

		//@ Lock
		//@ Locks a range of vertex data and obtains a pointer to the vertex buffer memory
		virtual PyroResult Lock()
		{
			return PyroOK;
		}

		//@ Unlock
		//@ Unlocks vertex data
		virtual PyroResult Unlock()
		{
			return PyroOK;
		}

		//@ Returns address of a pointer to vertex buffer memory
		virtual void * GetBuffer() { return m_data; }
		virtual u8 * GetData() const { return m_data; }

		//@ My interface
		u32		GetVertexSize() const { return m_vertexSize; }
		u32		GetVertexFormat() const { return m_vertexFormat; }
		//@ GetCount
		u32		GetCount() const { return m_numVerticies; }


	private:
		u32		m_numVerticies;	//pocet vertexov
		u32		m_vertexSize;		//velikost jednoho vertexu (28 4 floaty pro pos, 2 floaty prop UV, 1 u32 pro color)
		u32		m_vertexFormat;	//pyro vertex format
		u8		*	m_data;
	};

}}