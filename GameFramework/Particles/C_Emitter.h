/*
* filename:			C_Emitter.h
*
* author:			Kral Jozef
* date:				06/18/2009		22:22
* version:			1.00
* brief:				Emitter wrapper onto pyro emitter
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Pyro/Include/Pyro.h>
#include <Pyro/Include/PyroInterfaces.h>
#include <boost/shared_ptr.hpp>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Vector2.h>

namespace scorpio{ namespace effects{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Emitter
	class GAMEFRAMEWORK_API C_Emitter
	{
	public:
		//@ c-tor
		C_Emitter(PyroParticles::IPyroParticleEmitter * pyroEmitter)
			: m_pyroEmitter(pyroEmitter)
		{
			SC_ASSERT(pyroEmitter);
		}
		//@ d-tor
		~C_Emitter() {};

		//@ Update - tick to parcticle emitter - prepare particles
		//@ param inDeltaTime - different time between last and actual called update in seconds
		//@ param inDeltaFromStart - different time(delta) from Activating effect in seconds
		//@ param bEmitNew - emit new particles
		void					Update(float inDeltaTime, float inDeltaFromStart, const sysmath::C_Vector2 & emitterPos, bool bEmitNew = true);
		//@ Render
		void					Render() const;
		//@ HasParticle
		bool					HasParticles() const { return m_pyroEmitter->HasParticles() ? true : false; }
		//@ Reset
		void					Reset() { m_pyroEmitter->Reset(); }
		//@ SetTintColor
		void					SetTintColor(const sysmath::C_Vector4 & color) { m_pyroEmitter->SetTintColor(color.m_x, color.m_y, color.m_z, color.m_w); }
		//@ SetTransformation
		void					SetTransformation(float matrix[3][4]);
		//@ SetScale
		void					SetScale(float fScale);

	private:
		PyroParticles::IPyroParticleEmitter	*	m_pyroEmitter;	//pyro emitter
	};


	typedef boost::shared_ptr<C_Emitter>	T_Emitter;

}}