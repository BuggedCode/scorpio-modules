/*
* filename:			C_Effect.h
*
* author:			Kral Jozef
* date:				06/17/2009		20:34
* version:			1.00
* brief:				Effect class holds emitters
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include "C_Emitter.h"
#include <SysMath/C_Vector2.h>
#include <vector>
#include <SysCore/Core/I_RenderObject.h>
//#include <SysMath/C_Matrix.h>

namespace scorpio{ namespace effects{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Effect
	class GAMEFRAMEWORK_API C_Effect : public syscore::I_RenderObject
	{
		typedef std::vector<PyroParticles::IPyroParticleEmitter*>	T_vPyroEmitters;
		typedef std::vector<T_Emitter>					T_vEmitters;
	public:
		//@ c-tor
		C_Effect(const T_vPyroEmitters & pyroEmitters);
		//@ d-tor
		~C_Effect();

		//@ Update - tick to parcticle emitter - prepare particles
		//@ param inDeltaTime - different time between last and actual called update in ms
		void					Update(u32 inDeltaTime);
		//@ Render
		void					Render() const;

#ifndef MASTER
		//@ DebugDraw
		virtual void		DebugDraw() const;
#endif

		//@ Activate
		//@ param offsetFromAct - if you need activate effect not in initiali time you can use offset in seconds!
		void					Activate(float offsetFromAct = 0.f);
		//@ Deactivate - zasle pozadavek na deaktivaci - uz se nevyrabaju nove particli len 'dozivaju' uz vytvorene
		//@ param bStrong - pozadavek na tvrdu deaktivaciu - zresetujem effect a nastavim na neaktivny - partikle nedozivaju - moze sa pouzivat pre resetInputState vo widgetoch
		void					Deactivate(bool bStrong = false);
		//@ IsActive
		bool					IsActive()
		{ 
			if (m_active && !m_requestDeact) 
				return true;
			return false;
		}
		//@ Hasparticles
		bool					HasParticles() const;

		void					SetZCoord(float zCoord) { m_zCoord = zCoord; }
		//@ GetZCoord
		virtual float		GetZCoord() const { return m_zCoord; }

		//@ SetEmitterPosition
		void					SetPosition(const sysmath::C_Vector2 & vctPos) { m_EmitterPosition = vctPos; }
		void					SetPosition(float fPosX, float fPosY) { m_EmitterPosition.m_x = fPosX, m_EmitterPosition.m_y = fPosY; }
		//@ GetEmitterPosition
		const sysmath::C_Vector2 & GetPosition() const { return m_EmitterPosition; }

		//////////////////////////////////////////////////////////////////////////
		//@ TRANSFORMATION ROUTUNES FOR WHOLE EFFECT -> not just emitter, transform even particles already 
		//@ SetRotation
		void					SetEffectRotation(u32 nAngle);
		//@ SetScale
		void					SetEffectScale(float fScale);
		//@ SetPosition
		void					SetEffectPosition(const sysmath::C_Vector2 & vctPos);
		void					SetEffectPosition(float fPosX, float fPosY);

		//@ SetTintColor
		void					SetTintColor(const sysmath::C_Vector4 & color);


	private:
		T_vEmitters				m_emitters;
		sysmath::C_Vector2	m_EmitterPosition;
		//u32						m_Angle;	//rotation angle
		float						m_zCoord;
		float						m_deltaFromActivation;	//pyro needs to know how long it takes from activation(start) of emitter in seconds
		bool						m_active;			//is effect active
		bool						m_requestDeact;	//poziadavok na deaktivaciu effektu - uz se nevyrabaju nove particli len 'dozivaju' uz vytvorene

		//u32					m_maxParticles;	//TODO mozme urcit max pocet partiklov na efekt potom sa budu v rendru uniformne rozdistribuovavat
		mutable bool			m_EmiterStartEmitting;	//false until emiter will start emits particles, prevent return false before emiter starts firing particles
	};


	typedef boost::shared_ptr<C_Effect>	T_Effect;
	typedef std::vector<T_Effect>			T_EffectList;

}}