/*
* filename:			C_ParticleSystem.cpp
*
* author:			Kral Jozef
* date:				06/16/2009		22:39
* version:			1.00
* brief:				Particle System - wrapper onto Pyro
*/

#include "C_ParticleSystem.h"
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace effects{

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_ParticleSystem::C_ParticleSystem()
		: m_particleLibrary(NULL)
	{
		//
	}


	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	//////////////////////////////////////////////////////////////////////////
	C_ParticleSystem::~C_ParticleSystem()
	{
		//
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Initialize
	//////////////////////////////////////////////////////////////////////////
	bool C_ParticleSystem::Initialize()
	{
#ifdef PLATFORM_MACOSX_DISABLE
		return true;	//JK TODO now not support pyro on MAC:( need to buy src and build lib
#endif
		SC_ASSERT(!m_particleLibrary);
		
		//@ nealokuje sa cez new - nedeletovat!
      try 
      {
         m_particleLibrary = CreateParticleLibrary(PYRO_SDK_VERSION, 0);
      } 
      catch (PyroParticles::CPyroException * except)
      {
         TRACE_E(except->GetExceptionMessage());
			TRACE_FE("Can not create ParticleLibrary for pyro SDK version %d", PYRO_SDK_VERSION);
			return false;         
      }

      if (!m_particleLibrary)
		{
			TRACE_FE("Can not create ParticleLibrary for pyro SDK version %d", PYRO_SDK_VERSION);
			return false;                  
		}

		u8 minorLo = (PYRO_SDK_VERSION & 0x000000FF);
		u8 minorHi = (PYRO_SDK_VERSION & 0x0000FF00) >> 8;
		u8 major   = (PYRO_SDK_VERSION & 0x00FF0000) >> 16;

		TRACE_FI("#FF5599 Pyro version: %d.%d.%d", major, minorHi, minorLo);

		//@ poziada device o vytvorenie vertex buffra, ale o zrusenie sa uz postara library v metode Done!
		m_particleLibrary->Init(&m_renderDevice);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Deinitialize
	//////////////////////////////////////////////////////////////////////////
	bool C_ParticleSystem::Deinitialize()
	{
#ifdef PLATFORM_MACOSX_DISABLE
		return true;	//JK TODO now not support pyro on MAC:( need to buy src and build lib
#endif
		SC_ASSERT(m_particleLibrary);
		m_particleLibrary->Done();
		DestroyParticleLibrary(m_particleLibrary);
		m_particleLibrary = NULL;
		return true;
	}

}}