/*
* filename:			C_ParticleSystem.h
*
* author:			Kral Jozef
* date:				06/16/2009		22:38
* version:			1.00
* brief:				Particle System - wrapper onto Pyro
*/

#pragma once

#include <Common/C_Singleton.h>
#include <GameFramework/GameFrameworkApi.h>
#include <Pyro/Include/Pyro.h>
#include "C_RenderDevice.h"
#include "C_EffectManager.h"

namespace scorpio{ namespace effects{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ParticleSystem
	class GAMEFRAMEWORK_API C_ParticleSystem
	{
		friend class C_Singleton<C_ParticleSystem>;
	public:		
		//@ Initialize
		bool				Initialize();
		//@ Deinitialize
		bool				Deinitialize();

		//////////////////////////////////////////////////////////////////////////
		// GET/SET METHODS
		C_EffectManager	&	GetManager() { return m_manager; }

		//@ GetLibrary
		PyroParticles::IPyroParticleLibrary	*	GetLibrary() { return m_particleLibrary; }


	private:
		//@ c-tor
		C_ParticleSystem();
		//@ copy c-tor
		C_ParticleSystem(const C_ParticleSystem & copy);	//not implemented
		//@ assignment operator
		C_ParticleSystem & operator=(const C_ParticleSystem & copy);	//not implemented
		//@ d-tor
		~C_ParticleSystem();

		C_RenderDevice						m_renderDevice;	//render device for particles
		//@ sluzi na load pyro fajlov, inituje sa na devicom - vytvara si vlastny VB pre particle
		PyroParticles::IPyroParticleLibrary	*	m_particleLibrary;	//zatial podpora len 1 library

		C_EffectManager					m_manager;
	};

	typedef C_Singleton<C_ParticleSystem>	T_ParticleSystem;

}}

//@ explicit instatiation
//template <class C_ParticleSystem> C_ParticleSystem * C_Singleton<C_ParticleSystem>::m_instance = NULL;
//template <class T> T * C_Singleton<T>::m_instance = NULL;
GAMEFRAMEWORK_EXP_TEMP_INST template class GAMEFRAMEWORK_API C_Singleton<scorpio::effects::C_ParticleSystem>;
