/*
* filename:			C_RenderDevice.cpp
*
* author:			Kral Jozef
* date:				06/17/2009		22:06
* version:			1.00
* brief:				render device for particles inherited from pyro::IDevice
*/

#include <Pyro/Include/Pyro.h>	//koli PYROAPI
#include "C_RenderDevice.h"
#include "C_ParticleTexture.h"
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Vector2.h>
#include <SysCore/Framework/FrameworkTypes.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace effects{

#pragma pack(1)	//zarovnanie na 1 byte
	//@ struktura pre vertexy pouzivane Pyrom!
	struct S_PyroVertex
	{
		float		m_x;
		float		m_y;
		float		m_z;
		float		m_w;
		u32		m_color;
		float		m_tx;		//tex coordinat?
		float		m_ty;		//tex coordinat?
	};
#pragma pack()		//stejne pragma pack(pop)


	//////////////////////////////////////////////////////////////////////////
	//@ CreateBitmap
	//////////////////////////////////////////////////////////////////////////
	PyroResult C_RenderDevice::CreateBitmap(pyrographics::CBitmap **ppBitmap, u8 *pBuffer, u32 Width, u32 Height, u32 Flags)
	{
		//alokujem pyroBitmapu - pyro ju pouzije pro callbackovy dotaz na vytvorenie textury - CreateTexture 
		//@ ak ju pyro sam dealokuje! - este v inicializacii textur pre pyroFile - effect templata
		*ppBitmap = new pyrographics::CBitmap(pBuffer, Width, Height, Flags);
		return PyroOK;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ CreateTextures
	//////////////////////////////////////////////////////////////////////////
	PyroResult C_RenderDevice::CreateTexture(pyrographics::ITexture **ppTexture, pyrographics::CBitmap *pBitmap, u32 Flags)
	{
		*ppTexture = new C_ParticleTexture(*pBitmap, Flags);
		return PyroOK;
	}



	//////////////////////////////////////////////////////////////////////////
	//@ RenderQuads
	//////////////////////////////////////////////////////////////////////////
	PyroResult C_RenderDevice::RenderQuads(pyrographics::IVertexBuffer *pVertexBuffer, u32 nFirstQuad, u32 nQuads, u32 Flags)
	{
		//castovacky
		C_VertexBuffer * vertexBuffer = dynamic_cast<C_VertexBuffer*>(pVertexBuffer);	//toto je mozne castnut c preprocesu
		SC_ASSERT(vertexBuffer);

		C_ParticleTexture * pTexture = dynamic_cast<C_ParticleTexture*>(m_texture);		
		SC_ASSERT(pTexture);		

		T_Graphics image = pTexture->GetImage();
		image->SetBlendingMode(m_blendMode);

		image->RenderVertexBuffer(*vertexBuffer, nFirstQuad * 4, nQuads * 4);

		return PyroOK;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetBlendFunc
	//
	//////////////////////////////////////////////////////////////////////////
	PyroResult C_RenderDevice::SetBlendFunc(EBlend SrcBlend, EBlend DestBlend)
	{
		if (SrcBlend == PyroParticles::PyroGraphics::IDevice::BLEND_SRC_ALPHA &&
			DestBlend == PyroParticles::PyroGraphics::IDevice::BLEND_ONE)
		{
			m_blendMode = sysrender::E_BM_SRC_ALPHA__DST_ONE;
		}
		else if (SrcBlend == PyroParticles::PyroGraphics::IDevice::BLEND_SRC_ALPHA &&
			      DestBlend == PyroParticles::PyroGraphics::IDevice::BLEND_ONE_MINUS_SRC_ALPHA)
		{
			m_blendMode = sysrender::E_BM_SRC_ALPHA__DST_ONE_MINUS_ALPHA;
		}
		else
		{
			m_blendMode = sysrender::E_BM_SRC_ONE__DST_ONE;
		}

		return PyroOK;
	}


}}