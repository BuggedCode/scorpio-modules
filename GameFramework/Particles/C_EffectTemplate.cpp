/*
* filename:			C_EffectTemplate.cpp
*
* author:			Kral Jozef
* date:				06/17/2009		20:40
* version:			1.00
* brief:
*/

#include "C_EffectTemplate.h"
#include "C_ParticleSystem.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <GameFramework/C_GameFramework.h>
#include <SysCore/Framework/FrameworkTypes.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace effects{

	using namespace sysutils;
	using namespace framework;
	using namespace PyroParticles;

	//////////////////////////////////////////////////////////////////////////
	//@ Initialize
	//////////////////////////////////////////////////////////////////////////
	bool C_EffectTemplate::Initialize()
	{
#if defined PLATFORM_MACOSX_DISABLE
	return true;	//JK TODO now not support pyro on MAC:( need to buy src and build lib
#endif
		try
		{
			PyroParticles::IPyroParticleLibrary * lib = T_ParticleSystem::GetInstance().GetLibrary();
			SC_ASSERT(lib);

         T_Stream stream = T_FileSystemManager::GetInstance().Open(m_fileName);
			if (!stream)
				return false;

			m_pyroFile = lib->LoadPyroFile(stream->GetFileName());
			if (!m_pyroFile)
			{
				TRACE_FE("Can not initialize pyro effect: '%s'", m_fileName.c_str());
				return false;
			}

			m_pyroFile->CreateTextures();
			return true;
		}
		catch (CPyroException & exception)
		{
			TRACE_FE("Error %s! Template: %s", exception.GetExceptionMessage(), m_fileName.c_str());
			return false;
		}

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Deinitialize
	//////////////////////////////////////////////////////////////////////////
	void C_EffectTemplate::Deinitialize()
	{
		if (m_pyroFile)
			m_pyroFile->DestroyTextures();
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ CreateEmitter
	//////////////////////////////////////////////////////////////////////////
	T_Effect C_EffectTemplate::CreateEffect() const
	{
#ifdef PLATFORM_MACOSX_DISABLE
		return T_Effect();	//JK TODO now not support pyro on MAC:( need to buy src and build lib
#endif
		if (!m_pyroFile)
		{
			TRACE_FI("Template '%s' is not initialize, initializing...", (const char*)m_uniqueID);
			if (!const_cast<C_EffectTemplate*>(this)->Initialize())
				return T_Effect();
		}

		u32 nCount = m_pyroFile->GetFileEmitters();
		std::vector<IPyroParticleEmitter*>	pyroEmitters;
		pyroEmitters.reserve(nCount);

		for (u32 i = 0; i < nCount; ++i)
		{
			const IPyroParticleEmitter * baseEmitter = m_pyroFile->GetFileEmitter(i);
			if (!baseEmitter)
			{
				TRACE_FE("Effect: '%s' has invalid emitter id: %d!", m_fileName.c_str(), i);
				continue;
			}

			IPyroParticleEmitter * newEmitter = m_pyroFile->CreateEmitter(baseEmitter->GetName());
			SC_ASSERT(newEmitter);
			pyroEmitters.push_back(newEmitter);
		}

		T_Effect effect = T_Effect(new C_Effect(pyroEmitters));
		return effect;
      return T_Effect();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ ReleaseResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_EffectTemplate::ReleaseResources()
	{
		//@ effectTemplate is not initialized yet
		if (!m_pyroFile)
			return true;

		m_pyroFile->DestroyTextures();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ RestoreResources
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_EffectTemplate::RestoreResources()
	{
		if (!m_pyroFile)
			return true;

		m_pyroFile->CreateTextures();
		return true;
	}

}}