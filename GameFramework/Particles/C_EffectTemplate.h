/*
* filename:			C_EffectTemplate.h
*
* author:			Kral Jozef
* date:				06/17/2009		20:37
* version:			1.00
* brief:				Templata pro effect, mohli by byt grupnute este podla nejakeho ID, pro levely/screeny a pro ne sa invalidovat...
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Pyro/Include/pyro.h>
#include <Pyro/Include/PyroInterfaces.h>
#include <SysUtils/C_HashName.h>
#include <boost/shared_ptr.hpp>
#include "C_Effect.h"

namespace scorpio{ namespace effects{

	//////////////////////////////////////////////////////////////////////////
	//@ C_EffectTemplate
	class GAMEFRAMEWORK_API C_EffectTemplate
	{
	public:
		//@ c-tor
		C_EffectTemplate(const sysutils::C_HashName & templateUID, const sysutils::C_String fileName)
			: m_uniqueID(templateUID), m_fileName(fileName), m_pyroFile(NULL) {};

		//@ d-tor
		~C_EffectTemplate() {};

		//@ Initialize - vytvori interne textury pre effekt
		bool			Initialize();
		//@ Deinitialize - znici interne textury pre effekt
		void			Deinitialize();

		//@ GetUID
		T_Id32		GetUID() const { return m_uniqueID; }

		//@ CreateEmitter
		T_Effect		CreateEffect() const;

		//@ ReleaseResources
		bool			ReleaseResources();
		//@ RestoreResources
		bool			RestoreResources();


	
	private:
		sysutils::C_HashName			m_uniqueID;		//unique ID of effect template
		sysutils::C_String			m_fileName;		//file name particle effectu
		PyroParticles::IPyroFile*	m_pyroFile;		//interface fro work with pyro
	};

	typedef boost::shared_ptr<C_EffectTemplate>	T_EffectTemplate;

}}