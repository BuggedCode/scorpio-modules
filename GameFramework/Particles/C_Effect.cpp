/*
* filename:			C_Effect.cpp
*
* author:			Kral Jozef
* date:				06/19/2009		19:38
* version:			1.00
* brief:
*/

#include "C_Effect.h"
#include <SysUtils/C_TraceClient.h>
#include <SysMath/C_Vector4.h>
#include <boost/bind.hpp>
#include <SysMath/C_Matrix.h>
#ifndef MASTER
#include <SysCore/Render/C_Graphic.h>
#endif
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace effects{

	using namespace scorpio::sysmath;
	using namespace syscore;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_Effect::C_Effect(const T_vPyroEmitters & pyroEmitters)
		: m_zCoord(0.f), m_active(true), m_deltaFromActivation(0.f), m_requestDeact(false), m_EmiterStartEmitting(false)
	{
		for (T_vPyroEmitters::const_iterator iter = pyroEmitters.begin(); iter != pyroEmitters.end(); ++iter)
		{
			SC_ASSERT(*iter);	//paranoid check
			T_Emitter emit = T_Emitter(new C_Emitter(*iter));
			m_emitters.push_back(emit);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	//////////////////////////////////////////////////////////////////////////
	C_Effect::~C_Effect()
	{
		m_emitters.clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Update
	//////////////////////////////////////////////////////////////////////////
	void C_Effect::Update(u32 inDeltaTime)
	{
		if (!m_active)
			return;

		m_deltaFromActivation += (inDeltaTime * 0.001f);	//seconds
		float deltaTimeSec =  inDeltaTime * 0.001f;					//convert to seconds
		for (T_vEmitters::iterator iter = m_emitters.begin(); iter != m_emitters.end(); ++iter)
			(*iter)->Update(deltaTimeSec, m_deltaFromActivation, m_position, !m_requestDeact);

		bool bAtLeastOneHasParticle = false;
		for (T_vEmitters::iterator iter = m_emitters.begin(); iter != m_emitters.end(); ++iter)
		{
			bool bHasParticles = (*iter)->HasParticles();
			if (bHasParticles)
			{
				m_EmiterStartEmitting = true;
				bAtLeastOneHasParticle = true;
				break;
			}
		}

		//ak nema ziadny emitter particle a je pozadavok na deaktivaciu => deaktivujem effect
		if (!bAtLeastOneHasParticle && m_requestDeact)
		{
			m_active = false;
			m_requestDeact = false;
		}
	}



	//////////////////////////////////////////////////////////////////////////
	//@ Render
	//////////////////////////////////////////////////////////////////////////
	void C_Effect::Render() const
	{
		if (!m_active)
			return;

		for (T_vEmitters::const_iterator iter = m_emitters.begin(); iter != m_emitters.end(); ++iter)
			(*iter)->Render();
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Activate
	//////////////////////////////////////////////////////////////////////////
	void C_Effect::Activate(float offsetFromAct /* = 0 */)
	{
		m_active = true;
		m_deltaFromActivation = offsetFromAct;
		if (m_requestDeact)
		{
			//ak prisiel poziadavok na aktivaciu a partikel je pozadovany deaktivovat tak resetujem vsetky emittery
			for (T_vEmitters::const_iterator iter = m_emitters.begin(); iter != m_emitters.end(); ++iter)
				(*iter)->Reset();
		}
		m_requestDeact = false;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Deactivate
	//////////////////////////////////////////////////////////////////////////
	void C_Effect::Deactivate(bool bStrong /* = false */)
	{
		if (bStrong)
		{
			m_active = false;
			for (T_vEmitters::const_iterator iter = m_emitters.begin(); iter != m_emitters.end(); ++iter)
				(*iter)->Reset();
		}
		else
			m_requestDeact = true;
	}



	//////////////////////////////////////////////////////////////////////////
	//@ HasParticles
	//////////////////////////////////////////////////////////////////////////
	bool C_Effect::HasParticles() const
	{
		bool bHasParticles = false;
		for (T_vEmitters::const_iterator iter = m_emitters.begin(); iter != m_emitters.end(); ++iter)
		{
			bHasParticles |= (*iter)->HasParticles();
			if (bHasParticles)
			{
				m_EmiterStartEmitting = true;
				break;
			}
		}

		//@ emitter still doesn't start emit particles
		if (!m_EmiterStartEmitting)
			return true;

		return bHasParticles;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetTintColor
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Effect::SetTintColor(const C_Vector4 & color)
	{
		std::for_each(m_emitters.begin(), m_emitters.end(), boost::bind(&C_Emitter::SetTintColor, _1, color));
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetEffectPosition
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Effect::SetEffectPosition(const sysmath::C_Vector2 & vctPos)
	{
		C_Matrix mat;
		mat.SetPosition(vctPos.m_x, vctPos.m_y, 0.f);
		float matrix[3][4];
		mat.GetMatrix(matrix);

		std::for_each(m_emitters.begin(), m_emitters.end(), boost::bind(&C_Emitter::SetTransformation, _1, matrix));
	}
	void C_Effect::SetEffectPosition(float fPosX, float fPosY)
	{
		C_Matrix mat;
		mat.SetPosition(fPosX, fPosY, 0.f);
		float matrix[3][4];
		mat.GetMatrix(matrix);

		std::for_each(m_emitters.begin(), m_emitters.end(), boost::bind(&C_Emitter::SetTransformation, _1, matrix));
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetEffectScale
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Effect::SetEffectScale(float fScale)
	{
		std::for_each(m_emitters.begin(), m_emitters.end(), boost::bind(&C_Emitter::SetScale, _1, fScale));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetEffectRotation
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Effect::SetEffectRotation(u32 nAngle)
	{
		//@ getEmitter position
		C_Matrix mat;
		mat.SetPosition(m_EmitterPosition.m_x, m_EmitterPosition.m_y, 0.f);
		mat.invert();
		C_Matrix rotM;
		rotM.SetRotationZ((float)nAngle);
		mat = mat * rotM;
		float matrix[3][4];
		mat.GetMatrix(matrix);

		std::for_each(m_emitters.begin(), m_emitters.end(), boost::bind(&C_Emitter::SetTransformation, _1, matrix));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	void C_Effect::DebugDraw() const
	{
		C_Vector4 dbgColor(0.f, 0.5f, 1.f, 1.f);	//Light blue effect
		sysrender::C_Graphic graph;
		graph.RenderRect(m_position, C_Point(15, 15), dbgColor, false, 0.f, 1.f);
	}
#endif

}}