/*
* filename:			C_ParticleTexture.h
*
* author:			Kral Jozef
* date:				06/19/2009		22:44
* version:			1.00
* brief:				implementation of IPyroTexture interface
*/

#pragma once

#include <Pyro/Include/Pyro.h>
#include <Pyro/Include/PyroInterfaces.h>
#include <SysCore/Render/C_Graphic.h>
#include <SysUtils/C_TraceClient.h>

namespace scorpio{ namespace effects{

	namespace pyrographics = PyroParticles::PyroGraphics;

	//////////////////////////////////////////////////////////////////////////
	//@ C_ParticleTexture
	class C_ParticleTexture : public pyrographics::ITexture
	{
	public:
		//@ c-tor
		C_ParticleTexture(const pyrographics::CBitmap & inBitmap, u32 flags)
			: m_flags(flags)
		{
			m_image = T_Graphics(new sysrender::C_Graphic());
			if (m_image)
			{
				bool bRes = m_image->CreateTextureFromArray(inBitmap.GetBuffer(), inBitmap.GetWidth(), inBitmap.GetHeight());
				if (!bRes)
					TRACE_E("Can not create particle texture from buffer!");
			}
			else
				TRACE_E("Can not create particle texture");
		}

		//@ d-tor
		virtual ~C_ParticleTexture() {};	//shared ptr reset -> destroy graphics(ogl texture) resource

		//@ GetImage
		T_Graphics		GetImage() const { return m_image; }


		//@ Restore
		virtual PyroResult Restore()
		{ 
			return 0; 
		}

		//@ Invalidate
		virtual PyroResult Invalidate()
		{
			return 0;
		}


	private:
		T_Graphics			m_image;	//texture created from buffer for particle
		u32					m_flags;	//unknown flags:-(
	};

}}