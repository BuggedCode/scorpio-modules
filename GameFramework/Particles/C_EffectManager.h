/*
* filename:			C_EffectManager.h
*
* author:			Kral Jozef
* date:				06/18/2009		20:31
* version:			1.00
* brief:				manager of effect templates & effects
*/

#pragma once

#include <Common/I_NonCopyAble.h>
#include <GameFramework/GameFrameworkApi.h>
#include <map>
#include "C_EffectTemplate.h"
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/FileSystem/C_Stream.h>

namespace scorpio{ namespace effects{

	//////////////////////////////////////////////////////////////////////////
	//@ C_EffectManager
	class GAMEFRAMEWORK_API C_EffectManager : public I_NonCopyAble
	{
		typedef std::map<T_Id32, T_EffectTemplate>	T_TemplateContainer;
	public:
		//@ c-tor
		C_EffectManager() {};
		//@ d-tor
		~C_EffectManager();

		//@ Clear
		void				Clear();

		//@ LoadTemplates
		bool				LoadTemplates(const sysutils::T_Stream inputStream);
		//@ LoadTemplates
		bool				LoadTemplates(const sysutils::T_XmlNodeList effectNodeList);
		//@ CreateEffect
		T_Effect			CreateEffect(const sysutils::C_HashName & effectTemplateUID) const;
		//@ ClearTemplates
		//@ retVal - count of cleared templates
		u32				ClearTemplates(const sysutils::T_HashNameList & hashList);
		//@ ReleaseInternalResources
		bool				ReleaseInternalResources();
		//@ RestoreInternalResources - fullscreen change
		bool				RestoreInternalResources();

	private:
		//@ CreateTemplateFromNode
		bool				CreateTemplateFromNode(const sysutils::C_XmlNode & templNode);

		T_TemplateContainer		m_templContainer;
	};

}}