/*
* filename:			C_Emitter.cpp
*
* author:			Kral Jozef
* date:				06/21/2009		17:52
* version:			1.00
* brief:				Emitter wrapper onto pyro emitter
*/

#include "C_Emitter.h"
#include <SysMath/C_Matrix.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace effects{

	static const float C_MAX_DELTA = 0.02f;	//20ms
	using namespace scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ Update
	//////////////////////////////////////////////////////////////////////////
	void C_Emitter::Update(float inDeltaTime, float inDeltaFromStart, const sysmath::C_Vector2 & emitterPos, bool bEmitNew)
	{
		C_Vector2 emitterPos1 = C_Vector2(-100.f, 200.f);
		while ((inDeltaTime - C_MAX_DELTA) > 0)
		{
			//@ update po malych casovych intervaloch
			m_pyroEmitter->Move(inDeltaFromStart, (float)C_MAX_DELTA);
			m_pyroEmitter->Prepare(inDeltaFromStart, (float)C_MAX_DELTA, emitterPos1.m_x, emitterPos1.m_y, 0.f, bEmitNew);
			inDeltaTime -= C_MAX_DELTA;
		}

		m_pyroEmitter->Move(inDeltaFromStart, (float)inDeltaTime);
		m_pyroEmitter->Prepare(inDeltaFromStart, (float)inDeltaTime, emitterPos1.m_x, emitterPos1.m_y, 0.f, bEmitNew);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Render
	//////////////////////////////////////////////////////////////////////////
	void C_Emitter::Render() const
	{
		m_pyroEmitter->Render();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetTransformation
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Emitter::SetTransformation(float matrix[3][4])
	{
		m_pyroEmitter->SetLocalMatrix(matrix);
		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetScale
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Emitter::SetScale(float fScale)
	{
		m_pyroEmitter->SetScale(fScale);
	}

}}