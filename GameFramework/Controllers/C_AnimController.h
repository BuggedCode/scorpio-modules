/*
* filename:			C_AnimController.h
*
* author:			Kral Jozef
* date:				11/21/2010		22:24
* version:			1.00
* brief:				Generic frame based anim controller
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <GameFramework/gui/C_Widget.h>
#include <GameFramework/Entities/I_Entity.h>
#include <GameFramework/Operators/I_BaseOperator.h>
#include <SysCore/Core/Animation/C_AnimationTrack.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_AnimController

	//@ Mostly anim controller is used inside of animated object and we have no shared_ptr onto this object, 
	//@ so that's the reason we use in constructor reference to raw instance
	template <typename T>
	class GAMEFRAMEWORK_API C_AnimController : public I_BaseOperator
	{
	public:
		//@ c-tor
		C_AnimController(const syscore::T_AnimationTrack animationTrack, T & animObject, I_OperatorListener * listener);
		//@ d-tor
		~C_AnimController() {}


		//@ Update
		//@ param inDeltaTime - difference between last called update
		virtual void			Update(u32 inDeltaTime);

		//@ Reset
		virtual void			Reset() 
		{ 
			if (m_Reversed)
				m_currFrameID = m_animTrack->GetEndFrame();
			else
				m_currFrameID = m_animTrack->GetStartFrame(); 
		}
		//@ RevertAnim
		void						RevertAnim()
		{
			m_Reversed = !m_Reversed;
		}
		//@ IsReverted
		bool						IsReveresed() const { return m_Reversed; }

		//@ T_Animationtrack
		const syscore::T_AnimationTrack	GetAnimTrack() const { return m_animTrack; }

	private:
		//@ CalculateNextFrame
		void						CalculateNextFrame();
		//@ CalculatePrewFrame
		void						CalculatePrewFrame();


	private:
		const syscore::T_AnimationTrack		m_animTrack;
		T						&	m_AnimObject;
		u32						m_currFrameID;			//actual frameID for Sampled animation
		u32						m_elapsedT;		//accumulated delta T from last change of frame, in ms
		u32						m_deltaForOneFrame;
		bool						m_Reversed;	//animation is reversed
	};


	typedef C_AnimController<C_Widget>	C_WidgetAnimController;
	typedef C_AnimController<I_Entity>	C_EntityAnimController;

	typedef boost::shared_ptr<C_WidgetAnimController>	T_WidgetAnimController;
	typedef boost::shared_ptr<C_EntityAnimController>	T_EntityAnimController;

}}