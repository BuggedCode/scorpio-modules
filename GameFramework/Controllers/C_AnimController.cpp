/*
* filename:			C_AnimController.cpp
*
* author:			Kral Jozef
* date:				11/21/2010		22:24
* version:			1.00
* brief:
*/

#include "C_AnimController.h"
#include <SysMath/C_Point.h>
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace sysmath;
	using namespace syscore;
	using namespace sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	template<typename T>
	C_AnimController<T>::C_AnimController(const syscore::T_AnimationTrack animationTrack, T & animObject, I_OperatorListener * listener)
		: I_BaseOperator(listener), m_animTrack(animationTrack), m_AnimObject(animObject), m_Reversed(false)
	{
		m_currFrameID = animationTrack->GetStartFrame();
		m_elapsedT = 0;
		m_deltaForOneFrame = (u32)((1.f / (float)animationTrack->GetFPS()) * 1000);	//ms
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	template<typename T>
	void C_AnimController<T>::Update(u32 inDeltaTime)
	{
		if (!m_active)
			return;

		m_elapsedT += inDeltaTime;

		if (m_elapsedT < m_deltaForOneFrame)
			return;

		m_elapsedT = m_elapsedT - m_deltaForOneFrame;

		C_SampleData data;
		//@ Retrieve data from Smapled Animation Track
		m_animTrack->GetSampleData(m_currFrameID, data);

		if(data.HasSample(C_SampleData::E_PT_POSITION))
		{
			m_AnimObject.SetPosition(data.m_vctPos);
		}

		if (data.HasSample(C_SampleData::E_PT_ROTATION))
		{
			m_AnimObject.SetRotation(data.m_fAngle);
		}

		if (data.HasSample(C_SampleData::E_PT_ALPHA))
		{
			C_Vector4 color = m_AnimObject.GetColor();
			color.m_w = data.m_fAlpha;
			m_AnimObject.SetColor(color);
		}

		if (data.HasSample(C_SampleData::E_PT_SCALE))
		{
			m_AnimObject.SetScale(data.m_fScale);
		}

		if (data.HasSample(C_SampleData::E_PT_TRACK_NOTE))
		{
			for (C_SampleData::T_NoteTrackKeys::iterator iter = data.m_NoteTrackKeys.begin(); iter != data.m_NoteTrackKeys.end(); ++iter)
			{
				const C_NoteTrackKey & ntKey = *iter;
				m_AnimObject.NoteTrackNotify(ntKey.m_EventId, ntKey.m_Param1, ntKey.m_Param2);
			}
		}


		//@ Calculate Next FrameId
		if (m_Reversed)
			CalculatePrewFrame();
		else 
			CalculateNextFrame();

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ClaculateNextFrame
	//
	//////////////////////////////////////////////////////////////////////////
	template<typename T>
	void C_AnimController<T>::CalculateNextFrame()
	{
		if (m_currFrameID == m_animTrack->GetEndFrame())
		{
			if (m_animTrack->IsLooped())
			{
				Reset();
			}
			else
			{
				m_active = false;
				if (m_listener)
					m_listener->NotifyOperatorFinish(this);
			}
		}
		else
		{
			++m_currFrameID;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CalculatePrewFrame
	//
	//////////////////////////////////////////////////////////////////////////
	template<typename T>
	void C_AnimController<T>::CalculatePrewFrame()
	{
		if (m_currFrameID == m_animTrack->GetStartFrame())
		{
			if (m_animTrack->IsLooped())
			{
				Reset();
			}
			else
			{
				m_active = false;
				if (m_listener)
					m_listener->NotifyOperatorFinish(this);
			}
		}
		else
		{
			--m_currFrameID;
		}
	}

	template class C_AnimController<C_Widget>;
	template class C_AnimController<I_Entity>;

}}