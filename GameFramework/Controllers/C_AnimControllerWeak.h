/*
* filename:			C_AnimControllerWeak.cpp
*
* author:			Kral Jozef
* date:				9/19/2011		18:51
* version:			1.00
* brief:				Same as classic C_AnimCOntroller but uses I_Animatable interface and different method (the reason is to use 1 anim track with more controllers - relative ofsetted)
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <GameFramework/Operators/I_BaseOperator.h>
#include <SysCore/Core/Animation/C_AnimationTrack.h>
#include <GameFramework/Core/I_Animatable.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_AnimCOntrollerWeak
	class GAMEFRAMEWORK_API C_AnimControllerWeak : public I_BaseOperator
	{
	public:
		//@ c-tor
		C_AnimControllerWeak(const syscore::T_AnimationTrack animationTrack, I_Animatable * animatable, I_OperatorListener * listener);
		//@ d-tor
		~C_AnimControllerWeak() {};


		//@ Update
		//@ param inDeltaTime - difference between last called update
		virtual void			Update(u32 inDeltaTime);

		//@ Reset
		virtual void			Reset() 
		{ 
			if (m_Reversed)
				m_currFrameID = m_animTrack->GetEndFrame();
			else
				m_currFrameID = m_animTrack->GetStartFrame(); 
		}
		//@ RevertAnim
		void						RevertAnim()
		{
			m_Reversed = !m_Reversed;
		}
		//@ IsReverted
		bool						IsReveresed() const { return m_Reversed; }

	private:
		//@ CalculateNextFrame
		void						CalculateNextFrame();
		//@ CalculatePrewFrame
		void						CalculatePrewFrame();


	private:
		const syscore::T_AnimationTrack		m_animTrack;
		u32						m_currFrameID;			//actual frameID for Sampled animation
		u32						m_elapsedT;		//accumulated delta T from last change of frame, in ms
		u32						m_deltaForOneFrame;
		bool						m_Reversed;	//animation is reversed
		I_Animatable		*	m_Animatable;
	};


	typedef boost::shared_ptr<C_AnimControllerWeak>	T_AnimControllerWeak;
}}



