/*
* filename:			C_AnimControllerWeak.cpp
*
* author:			Kral Jozef
* date:				9/19/2011		19:15
* version:			1.00
* brief:
*/

#include "C_AnimControllerWeak.h"
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>


namespace scorpio{ namespace framework{

	using namespace sysmath;
	using namespace syscore;
	using namespace sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_AnimControllerWeak::C_AnimControllerWeak(const syscore::T_AnimationTrack animationTrack, I_Animatable * animatable, I_OperatorListener * listener)
		: I_BaseOperator(listener), m_animTrack(animationTrack), m_Animatable(animatable), m_Reversed(false)
	{
		SC_ASSERT(m_Animatable);
		m_currFrameID = animationTrack->GetStartFrame();
		m_elapsedT = 0;
		m_deltaForOneFrame = (u32)((1.f / (float)animationTrack->GetFPS()) * 1000);	//ms
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AnimControllerWeak::Update(u32 inDeltaTime)
	{
		if (!m_active)
			return;

		m_elapsedT += inDeltaTime;

		if (m_elapsedT < m_deltaForOneFrame)
			return;

		m_elapsedT = m_elapsedT - m_deltaForOneFrame;

		C_SampleData data;
		//@ Retrieve data from Smapled Animation Track
		m_animTrack->GetSampleData(m_currFrameID, data);

		if(data.HasSample(C_SampleData::E_PT_POSITION))
		{
			m_Animatable->SetPositionRel(data.m_vctPos);
		}

		if (data.HasSample(C_SampleData::E_PT_ROTATION))
		{
			m_Animatable->SetRotation(data.m_fAngle);
		}

		if (data.HasSample(C_SampleData::E_PT_ALPHA))
		{
			m_Animatable->SetAlpha(data.m_fAlpha);
		}

		if (data.HasSample(C_SampleData::E_PT_SCALE))
		{
			m_Animatable->SetScale(data.m_fScale);
		}

		if (data.HasSample(C_SampleData::E_PT_VELOCITY))
		{
			m_Animatable->SetVelocity(data.m_fVelocity);
		}

		if (data.HasSample(C_SampleData::E_PT_TRACK_NOTE))
		{
			for (C_SampleData::T_NoteTrackKeys::iterator iter = data.m_NoteTrackKeys.begin(); iter != data.m_NoteTrackKeys.end(); ++iter)
			{
				const C_NoteTrackKey & ntKey = *iter;
				m_Animatable->NoteTrackNotify(ntKey.m_EventId, ntKey.m_Param1, ntKey.m_Param2);
			}
		}


		//@ Calculate Next FrameId
		if (m_Reversed)
			CalculatePrewFrame();
		else 
			CalculateNextFrame();

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ClaculateNextFrame
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AnimControllerWeak::CalculateNextFrame()
	{
		if (m_currFrameID == m_animTrack->GetEndFrame())
		{
			if (m_animTrack->IsLooped())
			{
				Reset();
			}
			else
			{
				m_active = false;
				if (m_listener)
					m_listener->NotifyOperatorFinish(this);
			}
		}
		else
		{
			++m_currFrameID;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CalculatePrewFrame
	//
	//////////////////////////////////////////////////////////////////////////
	void C_AnimControllerWeak::CalculatePrewFrame()
	{
		if (m_currFrameID == m_animTrack->GetStartFrame())
		{
			if (m_animTrack->IsLooped())
			{
				Reset();
			}
			else
			{
				m_active = false;
				if (m_listener)
					m_listener->NotifyOperatorFinish(this);
			}
		}
		else
		{
			--m_currFrameID;
		}
	}



}}
