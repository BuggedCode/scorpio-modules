/*
* filename:			C_OptionsDialog.h
*
* author:			Kral Jozef
* date:				06/02/2009		21:18
* version:			1.00
* brief:				Base options dialog
*/

#pragma once

#include "C_Dialog.h"
#include "I_DialogListeners.h"
#include <GameFramework/Core/C_OptionsSettings.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_OptionsDialog
	class GAMEFRAMEWORK_API C_OptionsDialog : public C_Dialog
	{
		typedef C_Dialog	T_Super;
	public:
		//@ c-tor
		C_OptionsDialog(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener);

		//@ AssignSettings
		//@ param settings - valid in/out settings
		void					AssignSettings(C_OptionsSettings & settings);

		//@ SetOptionsDialogListener
		//@ param listener - valid listener
		void					SetOptionsDialogListener(I_OptionsDialogListener * listener)
		{
			SC_ASSERT(listener);	//if someone call setting listener, it has to be valid
			m_listener = listener;
		}

		//////////////////////////////////////////////////////////////////////////
		//@ WIDGET LISTENER INTERFACE
		virtual void		OnMouseUp(const sysutils::C_HashName & widgetUID);

		DECLARE_RTTI

	protected:
		C_OptionsSettings			*	m_settings;		//current settings from application
		C_OptionsSettings				m_bkpSettings;	//backup setting when cancel will be canceled
		I_OptionsDialogListener	*	m_listener;		//listener for optionsDialog changes

		static const sysutils::C_HashName	C_CHB_FULLSCREEN_UID;
		static const sysutils::C_HashName	C_SLD_MUSICVOLUME_UID;
		static const sysutils::C_HashName	C_SLD_SOUNDVOLUME_UID;
		static const sysutils::C_HashName	C_BTN_OK_UID;
		static const sysutils::C_HashName	C_BTN_CANCEL_UID;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_OptionsDialogCreator
	class C_OptionsDialogCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_OptionsDialog(widgetUID, NULL/*listener*/));
		}
	};


}}