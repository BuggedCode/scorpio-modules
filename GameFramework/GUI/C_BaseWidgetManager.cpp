/*
* filename:			C_BaseWidgetManager.cpp
*
* author:			Kral Jozef
* date:				06/06/2009		14:02
* version:			1.00
* brief:				Base widget manager for dialogs, not intended for export
*/

#include "C_BaseWidgetManager.h"
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ GetWidget
	//////////////////////////////////////////////////////////////////////////
	const T_Widget C_BaseWidgetManager::GetWidget(T_Id32 widgetID) const
	{
		T_WidgetContainer::const_iterator iter = m_widgets.find(widgetID);
		if (iter == m_widgets.end())
			return T_Widget();

		return iter->second;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Add
	//////////////////////////////////////////////////////////////////////////
	bool C_BaseWidgetManager::Add(T_Widget widget)
	{
		SC_ASSERT(widget);
		std::pair<T_WidgetContainer::iterator, bool> bRes = m_widgets.insert(T_WidgetContainer::value_type(widget->GetUID(),widget));
		if (bRes.second)
		{
			if (widget->HasFocus())
				m_FocusedWidget = widget;
		}
		return bRes.second;
	}



	//////////////////////////////////////////////////////////////////////////
	//@ Update
	//////////////////////////////////////////////////////////////////////////
	void C_BaseWidgetManager::Update(u32 inDeltaTime)
	{
		for (T_WidgetContainer::iterator iter = m_widgets.begin(); iter != m_widgets.end(); ++iter)
		{
			T_Widget widget = iter->second;
			SC_ASSERT(widget);
			widget->Update(inDeltaTime);
		}
	}

	
	
	//////////////////////////////////////////////////////////////////////////
	//@ Draw
	//////////////////////////////////////////////////////////////////////////
	void C_BaseWidgetManager::Draw(I_TransformationCallback & callback) const
	{
		//@ Render widgets
		for (T_WidgetContainer::const_iterator iter = m_widgets.begin(); iter != m_widgets.end(); ++iter)
		{
			T_Widget widget = iter->second;
			SC_ASSERT(widget);
			if (!widget->IsVisible())
				continue;
			widget->Draw(callback);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	void C_BaseWidgetManager::DebugDraw(I_TransformationCallback & callback) const
	{
		//@ Render widgets
		for (T_WidgetContainer::const_iterator iter = m_widgets.begin(); iter != m_widgets.end(); ++iter)
		{
			T_Widget widget = iter->second;
			SC_ASSERT(widget);
			//if (!widget->IsVisible())
			//	continue;
			widget->DebugDraw(callback);
		}
	}
#endif


	//////////////////////////////////////////////////////////////////////////
	//@ Reset
	//////////////////////////////////////////////////////////////////////////
	void C_BaseWidgetManager::ResetInputStates()
	{
		//posledny aktivny widget zneplatnim aby sa mu neposielal v Mousemove MouseLeave!
		m_lastWidget = T_Widget();
		//@ Reset widgets
		for (T_WidgetContainer::const_iterator iter = m_widgets.begin(); iter != m_widgets.end(); ++iter)
		{
			T_Widget widget = iter->second;
			SC_ASSERT(widget);
			widget->ResetInputStates();
		}
	}




	//////////////////////////////////////////////////////////////////////////
	//
	// EVENTS
	//
	//////////////////////////////////////////////////////////////////////////

	T_Widget C_BaseWidgetManager::GetWidgetAt(s32 posX, s32 posY) const
	{
		for (T_WidgetContainer::const_iterator iter = m_widgets.begin(); iter != m_widgets.end(); ++iter)
		{
			T_Widget widgetRef = iter->second;
			SC_ASSERT(widgetRef);

			//obsahuje widget mysku
			if (widgetRef->Contains(posX, posY))
				return widgetRef;
		}

		return T_Widget();
	}


	//////////////////////////////////////////////////////////////////////////
	//@ MouseMove
	//////////////////////////////////////////////////////////////////////////
	void C_BaseWidgetManager::MouseMove(s32 x, s32 y)
	{
		//@ if some widget is captured, send only to that widget MoveMsg
		if (m_CapturedWidget)
		{
			m_CapturedWidget->MouseMove(x, y);
			return;
		}

		//pokial je stlacena mys tak sa pokusam dragovat aktualny widget
		//if (m_draggedWidget)
		//	DragWidget(m_draggedWidget, x, y);

		T_Widget actWidget = GetWidgetAt(x, y);
		//pokud widget pod cursorem je jiny jako naposledy => MouseHover
		if (actWidget == m_lastWidget)
		{
			if (m_lastWidget)
				m_lastWidget->MouseMove(x, y);
			return;
		}

		if (actWidget != m_lastWidget)
		{
			if (m_lastWidget)
			{
				m_lastWidget->MouseLeave();
				m_lastWidget = T_Widget();
			}

			if (actWidget)
			{
				actWidget->MouseEnter();
				m_lastWidget = actWidget;
			}
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//@ MouseDown
	//////////////////////////////////////////////////////////////////////////
	void C_BaseWidgetManager::MouseDown(s32 x, s32 y, u32 buttonIndex)
	{
		if (m_CapturedWidget)
		{
			m_CapturedWidget->MouseDown(x, y, (C_Widget::E_MouseButton)buttonIndex);	//TODO !PREPISAT
			return;
		}

		T_Widget actWidget = GetWidgetAt(x, y);
		if (actWidget)
		{
			actWidget->MouseDown(x, y, (C_Widget::E_MouseButton)buttonIndex);	//TODO !PREPISAT
			if (actWidget->IsCaptured())
				m_CapturedWidget = actWidget;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//@ MouseUp
	//////////////////////////////////////////////////////////////////////////
	void C_BaseWidgetManager::MouseUp(s32 x, s32 y, u32 buttonIndex)
	{
		if (m_CapturedWidget)
		{
			m_CapturedWidget->MouseUp(x, y, (C_Widget::E_MouseButton)buttonIndex);
			if (!m_CapturedWidget->IsCaptured())
				m_CapturedWidget.reset();
			return;
		}

		T_Widget actWidget = GetWidgetAt(x, y);
		if (actWidget)
		{
			actWidget->MouseUp(x, y, (C_Widget::E_MouseButton)buttonIndex);	//TODO !PREPISAT
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//@ OnKeyDown
	//////////////////////////////////////////////////////////////////////////
	void C_BaseWidgetManager::OnKeyDown(E_KeyboardLayout keyCode)
	{
		if (keyCode == SDLK_TAB)
		{
			//ChageFocus;
			return;
		}
		if (m_FocusedWidget)
			m_FocusedWidget->OnKeyDown(keyCode);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ OnKeyUp
	//////////////////////////////////////////////////////////////////////////
	void C_BaseWidgetManager::OnKeyUp(E_KeyboardLayout keyCode)
	{
		if (m_FocusedWidget)
			m_FocusedWidget->OnKeyUp(keyCode);
	}

}}