/*
* filename:			C_CheckBox.cpp
*
* author:			Kral Jozef
* date:				06/07/2009		20:12
* version:			1.00
* brief:				CheckBox widget
*/

#include "C_CheckBox.h"
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_TextureManager.h>
#include <SysUtils/Utils/C_StringUtils.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_CheckBox, C_Button)

	using namespace sysutils;
	using namespace sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_CheckBox::C_CheckBox(const C_HashName & widgetUID, I_WidgetListener * listener)
		: C_Button(widgetUID, listener), m_bChecked(NULL)
	{


	}


	//////////////////////////////////////////////////////////////////////////
	//@ InitFromXmlNode
	//////////////////////////////////////////////////////////////////////////
	bool C_CheckBox::InitFromXmlNode(const C_XmlNode & node)
	{
		if (!T_Super::InitFromXmlNode(node))
			return false;

		//@ CHECKED UNFOCUSED IMAGE
		const C_XmlNode * tempNode = node.GetNode("checked_image");
		if (!tempNode || !tempNode->IsValid())
		{
			TRACE_E("Missing or corrupted 'checked_image' node");
			return false;
		}

		const C_XmlAttribute * imgAtt = tempNode->GetAttribute("img");
		if (!imgAtt)
		{
			TRACE_E("Missing or corrupted 'img' attribute in 'checked_image' node");
			return false;
		}

		C_String imgPath = imgAtt->GetValue();
		m_checkedImg = syscore::T_TextureManager::GetInstance().GetTexture(imgPath);
		if (!m_checkedImg)
			return false;


		//@ CHECKED FOCUSED IMAGE
		imgPath = C_StringUtils::TrimExtension(imgPath);
		imgPath += "_o.png";
		m_checkedFocusImg = syscore::T_TextureManager::GetInstance().GetTexture(imgPath);
		if (!m_checkedFocusImg)
			return false;

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Draw
	//////////////////////////////////////////////////////////////////////////
	void C_CheckBox::Draw(I_TransformationCallback & callback)
	{
		if (!m_bChecked || !(*m_bChecked))
			return T_Super::Draw(callback);

		//@ ak som checknuty vlastny draw
		//@ enablaciu zatial neberem do uvahy!

		if (m_bMouseOver)
		{
			if (m_checkedFocusImg)
				__asm nop;//m_checkedFocusImg->Render(m_pivot, C_Point((s32)m_checkedFocusImg->GetWidth(), (s32)m_checkedFocusImg->GetHeight()), m_color, 0.f, m_fScale);
			else
				SC_ASSERT(false);//m_image->Render(m_pivot, m_size, m_color, 0.f, m_fScale);
			//DbgDraw();
			return;
		}

		SC_ASSERT(false);//m_checkedImg->Render(m_pivot, C_Point((s32)m_checkedImg->GetWidth(), (s32)m_checkedImg->GetHeight()), m_color, 0.f, m_fScale);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ MOUSE EVENTS
	//@ MouseUp
	void C_CheckBox::MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		if (!m_bChecked)
		{
			SC_ASSERT(false);
			return;
		}
		(*m_bChecked) = !(*m_bChecked);
		T_Super::MouseUp(inPosX, inPosY, eButtonID);	//spracovanie predka a zavolanie listenera
	}


}}