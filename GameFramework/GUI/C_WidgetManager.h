/*
* filename:			C_WidgetManager.h
*
* author:			Kral Jozef
* date:				04/01/2009		22:09
* version:			1.00
* brief:				extended widget manager for screens
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include "C_Dialog.h"
#include <vector>
#include "C_BaseWidgetManager.h"

namespace scorpio{ namespace framework{


	//////////////////////////////////////////////////////////////////////////
	//@ C_WidgetManager
	class GAMEFRAMEWORK_API C_WidgetManager : public C_BaseWidgetManager
	{
		typedef C_BaseWidgetManager	T_Super;
	public:
		//@ c-tor
		C_WidgetManager() {};
		//@ d-tor
		~C_WidgetManager() {};

		//@ Update
		//@ param inDeltaTime - difference between last called update
		void					Update(u32 inDeltaTime);

		//@ Draw - all enabled windows
		//@ param - callback - which we use for local wnd transformations
		void					Draw(I_TransformationCallback & callback) const;
#ifndef MASTER
		//@ DebugDraw
		void					DebugDraw(I_TransformationCallback & callback) const;
#endif

		//@ PushModal
		void					PushModal(T_Dialog dialog)
		{
			dialog->InitDialog();
			m_modalDlgStack.push_back(dialog);
		}

		//@ PopModal
		T_Dialog				PopModal()
		{
			if (m_modalDlgStack.empty())
				return T_Dialog();

			T_Dialog retDlg = m_modalDlgStack.back();
			m_modalDlgStack.pop_back();
			return retDlg;
		}

		//@ GetTopModal
		T_Dialog				GetTopModal()
		{
			if (m_modalDlgStack.empty())
				return T_Dialog();

			return m_modalDlgStack[m_modalDlgStack.size() - 1];
		}


		//@ GetDialog
		const T_Dialog GetDialog(T_Id32 dialogID) const;

		
		//////////////////////////////////////////////////////////////////////////
		//@ EVENTS
		virtual void		MouseMove(s32 x, s32 y);
		virtual void		MouseDown(s32 x, s32 y, u32 buttonIndex);
		virtual void		MouseUp(s32 x, s32 y, u32 buttonIndex);
		virtual void		OnKeyDown(E_KeyboardLayout keyCode);
		virtual void		OnKeyUp(E_KeyboardLayout keyCode);

	private:
		typedef std::vector<T_Dialog>		T_DlgStack;
		T_DlgStack					m_modalDlgStack;	//stack modalnich dialogu
	};
}}