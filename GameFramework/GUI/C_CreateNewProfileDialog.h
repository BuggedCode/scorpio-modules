/*
* filename:			C_CreateNewProfileDialog.h
*
* author:			Kral Jozef
* date:				04/20/2011		10:27
* version:			1.00
* brief:
*/

#pragma once

#include "C_Dialog.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	// C_CreateNewProfileDialog
	class GAMEFRAMEWORK_API C_CreateNewProfileDialog : public C_Dialog
	{
		typedef C_Dialog	T_Super;
	public:
		//@ c-tor
		C_CreateNewProfileDialog(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener)
			: C_Dialog(widgetUID, listener) {};

		//@ InitDialog
		virtual void				InitDialog();

		//@ GetProfileName
		sysutils::C_String		GetProfileName() const;

		//@ OnMouseUp
		virtual void				OnMouseUp(const sysutils::C_HashName & widgetUID);

		//////////////////////////////////////////////////////////////////////////
		//@ INPUT C_WIDGET INTERFACE
		//////////////////////////////////////////////////////////////////////////
		virtual void				OnKeyDown(E_KeyboardLayout keyCode);


		DECLARE_RTTI

	private:
		static const sysutils::C_HashName	C_BTN_CREATE_UID;
		static const sysutils::C_HashName	C_BTN_CANCEL_UID;
		static const sysutils::C_HashName	C_EDB_NEWPROFILE_UID;
	};

	typedef boost::shared_ptr<C_CreateNewProfileDialog>	T_CreateNewProfileDialog;

	//////////////////////////////////////////////////////////////////////////
	//@ C_CreateNewProfileDialogCreator
	class C_CreateNewProfileDialogCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_CreateNewProfileDialog(widgetUID, NULL/*listener*/));
		}
	};

}}
