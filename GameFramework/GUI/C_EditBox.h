/*
* filename:			C_EditBox.h
*
* author:			Kral Jozef
* date:				09/14/2010		20:50
* version:			1.00
* brief:
*/

#pragma once

#include "C_Picture.h"
#include <SysCore/Core/C_BitmapFont.h>
#include <SysCore/Frames/C_BitmapText.h>
#include "C_Caret.h"

namespace scorpio{ namespace framework{


	//////////////////////////////////////////////////////////////////////////
	//@ C_EditBox
	class GAMEFRAMEWORK_API C_EditBox : public C_Picture
	{
		typedef C_Picture		T_Super;
	public:
		//@ c-tor
		C_EditBox(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener);
		//@ d-tor
		~C_EditBox() {};

		//@ InitFromXmlNode
		virtual bool		InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ InitFromFrame
		virtual bool		InitFromFrame(const syscore::T_Frame frame);
		//@ Update
		virtual void		Update(u32 inDeltaTime);
		//@ Draw
		virtual void		Draw(I_TransformationCallback & callback);
		//@ Reset - reset states from input (mouse over/...)
		virtual void		ResetInputStates();
		//@ KeyDown
		virtual void		OnKeyDown(E_KeyboardLayout keyCode);
		//@ SetColor
		virtual void		SetColor(const sysmath::C_Vector4 & color);
		//@ SetFocus
		virtual void		SetFocus(bool bFocus);

		//@ GetText
		sysutils::C_String	GetText() const { return m_Text; }


		DECLARE_RTTI

	private:
		//@ RemoveLastChar
		void					RemoveLastChar();

	protected:
		sysutils::C_String		m_Text;
		syscore::T_BitmapText	m_BitmapText;
		syscore::T_BitmapFont	m_Font;	//TODO optimize store it into singleton WidgetInfoStorage
		sysmath::C_Vector2		m_TextPos;	//Left/Bottom of text position
		float							m_Scale;	//font scale
		float							m_CaretHeight;
		u8								m_MaxChars;	//maximum characters in EditBox
		T_Caret						m_Caret;
	};


	typedef boost::shared_ptr<C_EditBox>		T_EditBox;


	//////////////////////////////////////////////////////////////////////////
	//@ C_EditBoxCreator
	class C_EditBoxCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_EditBox(widgetUID, listener));
		}
	};

}}