/*
* filename:			C_ListBox.cpp
*
* author:			Kral Jozef
* date:				04/10/2011		2:04
* version:			1.00
* brief:
*/

#include "C_ListBox.h"
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_BitmapFontManager.h>
#include <SysAudio/C_AudioManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;
	using namespace scorpio::sysaudio;

	const T_Hash32 C_ListBox::C_SND_LISTBOXITEM_CLICK = C_HashName("LISTBOXITEM_CLICK").GetHash();

	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromXmlNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ListBox::InitFromXmlNode(const C_XmlNode & node)
	{
		bool bRes = T_Super::InitFromXmlNode(node);
		if (!bRes)
			return false;

		//@ InitFont
		m_Font = T_BitmapFontManager::GetInstance().GetFont(C_HashName("GUI_DEFAULT"));

		//@ calc scale
		if (!isZero(m_ItemInfo.m_Height))
			m_Scale =  m_ItemInfo.m_Height / (float)(m_Font->GetHeight() + 5.f);	//+ 5pix smaller font

		return bRes && m_Font;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromFrame
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ListBox::InitFromFrame(const syscore::T_Frame frame)
	{
		bool bRes = T_Super::InitFromFrame(frame);
		if (!bRes)
			return false;

		T_Frame lbItem = frame->FindFrame("Item");
		if (!lbItem)
		{
			TRACE_FE("Can not find 'Item' frame under ListBox widget: %s", frame->GetHashName().GetName());
			return false;
		}

		//init offset & item info

		C_AABB2 aabb = lbItem->GetAABB();
		C_Vector2 pos = lbItem->GetLocalPosition() + m_pivot;
		aabb.Translate(pos);
		m_TopItemPos = C_Vector2(aabb.GetMin().m_x, aabb.GetMax().m_y);
		m_ItemInfo.m_Width = (u32)aabb.GetWidth();
		m_ItemInfo.m_Height = (u32)aabb.GetHeight();


		//@ Selector is copy of ListBoxItem
		m_Selector = boost::dynamic_pointer_cast<C_Visual>(lbItem->Clone());
		m_Selector->SetVisible(false);
		SC_ASSERT(m_Selector);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Draw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ListBox::Draw(I_TransformationCallback & callback)
	{
		T_Super::Draw(callback);
		
		m_Selector->Render();

		for (T_TextItemContainer::iterator iter = m_TextContainer.begin(); iter != m_TextContainer.end(); ++iter)
		{
			iter->first->Render();
			iter->second->Render();
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ListBox::DebugDraw(I_TransformationCallback & callback)
	{
		T_Super::DebugDraw(callback);
		for (T_TextItemContainer::iterator iter = m_TextContainer.begin(); iter != m_TextContainer.end(); ++iter)
		{
			iter->first->DebugDraw();
			iter->second->DebugDraw();
		}

		m_Selector->DebugDraw();

		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetColor
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ListBox::SetColor(const C_Vector4 & color)
	{
		T_Super::SetColor(color);
		for (T_TextItemContainer::iterator iter = m_TextContainer.begin(); iter != m_TextContainer.end(); ++iter)
		{
			iter->first->SetColor(color);
			iter->second->SetColor(color);
		}

		if (m_Selector)
			m_Selector->SetColor(color);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ClearAll
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ListBox::ClearAll()
	{
		m_Container.clear();
		m_TextContainer.clear();
		m_Selector->SetVisible(false);
		m_SelectedId = -1;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddItem
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ListBox::AddItem(const C_ListBoxItem & item)
	{
		C_Vector2 vctPos = m_TopItemPos + C_Vector2(0.f, float(m_Container.size() * m_ItemInfo.m_Height));

		m_Container.push_back(item);

		T_BitmapText text = T_BitmapText(new C_BitmapText(vctPos, item.m_Name, m_Font, E_TT_CZSK));
		T_BitmapText value = T_BitmapText(new C_BitmapText(vctPos, item.m_Value, m_Font, E_TT_CZSK));

		u32 width = value->GetWidth();
		vctPos.m_x = m_TopItemPos.m_x + m_ItemInfo.m_Width - width;
		value->SetPosition(vctPos);
		text->SetScale(m_Scale);
		value->SetScale(m_Scale);

		m_TextContainer.push_back(std::make_pair(text, value));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SelectItem
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ListBox::SelectItem(u16 index)
	{
		SC_ASSERT(index < (u16)m_TextContainer.size());
		
		m_Selector->SetVisible(true);
		C_Vector2 selPos = m_TopItemPos;
		selPos.m_y += (index * m_ItemInfo.m_Height);
		selPos.m_y -= m_ItemInfo.m_Height / 2.f;
		selPos.m_x += m_ItemInfo.m_Width / 2.f;

		m_Selector->SetPosition(selPos);
		m_SelectedId = index;

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ MouseDown
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ListBox::MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		T_Super::MouseDown(inPosX, inPosY, eButtonID);
		if (eButtonID == C_Widget::E_MB_LEFT)
		{
			if (m_TextContainer.empty())
			{
				m_Selector->SetVisible(false);
				return;
			}
			if (inPosX < m_TopItemPos.m_x || inPosX >  (m_TopItemPos.m_x + m_ItemInfo.m_Width))
				return;

			if (inPosY < (m_TopItemPos.m_y - m_ItemInfo.m_Height) ||  inPosY > ((m_TopItemPos.m_y - m_ItemInfo.m_Height) + m_TextContainer.size() * m_ItemInfo.m_Height))
				return;

			s32 ItemIndex = (s32)((inPosY - (m_TopItemPos.m_y - m_ItemInfo.m_Height)) / m_ItemInfo.m_Height);
			m_SelectedId = ItemIndex;


			m_Selector->SetVisible(true);

			C_Vector2 selPos = m_TopItemPos;
			selPos.m_y += (m_SelectedId * m_ItemInfo.m_Height);
			selPos.m_y -= m_ItemInfo.m_Height / 2.f;
			selPos.m_x += m_ItemInfo.m_Width / 2.f;
			
			m_Selector->SetPosition(selPos);

			T_AudioManager::GetInstance().PlaySoundEx(C_SND_LISTBOXITEM_CLICK);
		}
	}

}}
