/*
* filename:			C_ProfilesDialog.cpp
*
* author:			Kral Jozef
* date:				04/12/2011		22:11
* version:			1.00
* brief:
*/

#include "C_ProfilesDialog.h"
#include "C_DialogManager.h"
#include <SysUtils/C_TraceClient.h>
#include <GameFramework/GUI/C_ListBox.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_ProfilesDialog, C_Dialog)

	using namespace scorpio::sysutils;

	const C_HashName	C_ProfilesDialog::C_BTN_OK_UID = C_HashName("IDCBTN_ProfilesOk");
	const C_HashName	C_ProfilesDialog::C_BTN_CREATE_UID	= C_HashName("IDCBTN_ProfilesCreate");
	const C_HashName	C_ProfilesDialog::C_BTN_DELETE_UID	= C_HashName("IDCBTN_ProfilesDelete");
	const C_HashName	C_ProfilesDialog::C_LB_PROFILES_UID	= C_HashName("IDCLB_Profiles");


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitDialog
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilesDialog::InitDialog()
	{
		//////////////////////////////////////////////////////////////////////////
		class C_IndexFinder
		{
		public:
			C_IndexFinder(const C_GUID & profileGuid) : m_ProfileGuid(profileGuid), m_Index(0) {};
			bool operator()(const T_PlayerProfile playerProfile)
			{
				if (playerProfile->GetGuid() == m_ProfileGuid)
					return false;

				++m_Index;
				return true;
			}

		public:
			u16					m_Index;
		private:
			const C_GUID	&	m_ProfileGuid;
		};


		T_Super::InitDialog();
		FillListBox();
		UpdateControls();

		//@ Select Current Profile, find index in profiles vector, and select reverted index(listbox contain profiles in reverted order)
		const C_GUID & currGuid = m_Profiles->GetCurrent();
		C_IndexFinder indexFinder(currGuid);
		m_Profiles->DoForAllProfiles(indexFinder);
		
		T_Widget widget = m_widgetManager.GetWidget(C_LB_PROFILES_UID.GetHash());
		T_ListBox lbWidget = boost::dynamic_pointer_cast<C_ListBox>(widget);

		u16 index = 0;
		if (m_Profiles->GetCount() != indexFinder.m_Index)	//if in some strange case can not find current profile in LB we select firstItem
		{
			index = m_Profiles->GetCount() - indexFinder.m_Index - 1;
		}

		lbWidget->SelectItem(index);
	}


	//////////////////////////////////////////////////////////////////////////
	// WIDGET LISTENER IMPLEMENTATION
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilesDialog::OnMouseUp(const C_HashName & widgetUID)
	{
		if (widgetUID == C_HashName(C_BTN_OK_UID))
		{
			StartFadeOut(E_DEV_OK);
		}
		else if ((widgetUID == C_BTN_CREATE_UID) || (widgetUID == C_BTN_DELETE_UID))
		{
			SC_ASSERT(m_listener);
			m_listener->OnMouseUp(widgetUID);
		}
		else if (widgetUID == C_LB_PROFILES_UID)
		{
			//@ click into ProfileListBox
			T_Widget widget = m_widgetManager.GetWidget(C_LB_PROFILES_UID.GetHash());
			T_ListBox lbWidget = boost::dynamic_pointer_cast<C_ListBox>(widget);
			if (lbWidget->GetSelectedId() != -1)
			{
				C_ListBoxItem lbItem = lbWidget->GetSelectedItem();
				m_Profiles->SetCurrent(lbItem.m_Guid);
				//m_listener->OnMouseUp(widgetUID);
			}
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddNewProfile
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilesDialog::AddNewProfile(C_String profileName)
	{
		T_PlayerProfile profile = T_PlayerProfile(new C_PlayerProfile(profileName));
		SC_ASSERT(m_Profiles);
		m_Profiles->Add(profile, true);
		
		FillListBox();

		T_Widget widget = m_widgetManager.GetWidget(C_HashName("IDCLB_Profiles").GetHash());
		T_ListBox listBox = boost::dynamic_pointer_cast<C_ListBox>(widget);

		listBox->SelectItem(0);	//first item last added we select

		UpdateControls();
		return;
	}


	class C_GetProfileInfoFunc
	{
	public:
		C_GetProfileInfoFunc(std::vector<C_ListBoxItem> & valScoreList)
			: m_ValScoreList(valScoreList) {};

		//@ operator()
		bool operator()(const T_PlayerProfile profile)
		{
			m_ValScoreList.push_back(C_ListBoxItem());
			C_ListBoxItem & item = m_ValScoreList.back();
			item.m_Name = profile->GetName();
			item.m_Value = C_String::Format("%d", profile->GetScore());
			item.m_Guid = profile->GetGuid();
			return true;
		}

	private:
		std::vector<C_ListBoxItem> & m_ValScoreList;
	};

	//////////////////////////////////////////////////////////////////////////
	//
	//@ FillListBox
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilesDialog::FillListBox()
	{
		T_Widget widget = m_widgetManager.GetWidget(C_HashName("IDCLB_Profiles").GetHash());
		if (!widget)
		{
			TRACE_E("Can not find ListBox widget with uid: 'IDCLB_Profiles'");
			return;
		}

		T_ListBox listBox = boost::dynamic_pointer_cast<C_ListBox>(widget);
		//////////////////////////////////////////////////////////////////////////
		listBox->ClearAll();

		std::vector<C_ListBoxItem>	lbItems;
		C_GetProfileInfoFunc func(lbItems);
		m_Profiles->DoForAllProfiles(func);

		for(std::vector<C_ListBoxItem>::reverse_iterator iter = lbItems.rbegin(); iter != lbItems.rend(); ++iter)
		{
			listBox->AddItem(*iter);
		}
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ DeleteProfile
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilesDialog::DeleteSelectedProfile()
	{
		T_Widget widget = m_widgetManager.GetWidget(C_HashName("IDCLB_Profiles").GetHash());
		if (!widget)
		{
			TRACE_E("Can not find ListBox widget with uid: 'IDCLB_Profiles'");
			return;
		}

		T_ListBox listBox = boost::dynamic_pointer_cast<C_ListBox>(widget);
		C_ListBoxItem selItem = listBox->GetSelectedItem();
		s16 selId = listBox->GetSelectedId();

		SC_ASSERT(selId != -1);

		if (selId != 0)
			selId -= 1;	//if higher than top select prew, pothet still sleect new 0

		m_Profiles->Remove(selItem.m_Guid);
		FillListBox();

		if (!listBox->IsEmpty())
		{
			listBox->SelectItem(selId);
			C_ListBoxItem lbItem = listBox->GetSelectedItem();
			m_Profiles->SetCurrent(lbItem.m_Guid);
		}

		UpdateControls();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ UpdateControls
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ProfilesDialog::UpdateControls()
	{
		T_Widget widget = m_widgetManager.GetWidget(C_HashName("IDCLB_Profiles").GetHash());
		T_ListBox listBox = boost::dynamic_pointer_cast<C_ListBox>(widget);

		widget = m_widgetManager.GetWidget(C_BTN_DELETE_UID);
		widget->ResetInputStates();
		widget->SetEnable(!listBox->IsEmpty());
		widget = m_widgetManager.GetWidget(C_BTN_CREATE_UID);
		widget->ResetInputStates();
		widget->SetEnable(!listBox->IsFull());
	}

}}
