/*
* filename:			C_Dialog.h
*
* author:			Kral Jozef
* date:				04/06/2009		19:21
* version:			1.00
* brief:				Modal dialog
*/

#pragma once

#include "C_Picture.h"
#include "C_BaseWidgetManager.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Dialog
	class GAMEFRAMEWORK_API C_Dialog : public C_Picture, public I_WidgetListener
	{
		typedef C_Picture	T_Super;
	public:
		//@ c-tor
		C_Dialog(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener);
		//@ d-tor
		~C_Dialog();

		//@ InitFromXmlNode
		virtual bool				InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ Update
		//@ param inDeltaTime - difference between last called update
		virtual void				Update(u32 inDeltaTime);
		//@ Draw
		virtual void				Draw(I_TransformationCallback & callback);
#ifndef MASTER
		//@ DebugDraw
		virtual void				DebugDraw(I_TransformationCallback & callback);
#endif

		//@ SetDialogListener - ability to set listener into dialog after obtainig from DialogManager and pushing onto ScreenStack
		virtual void				SetDialogListener(I_WidgetListener * dlgListener)
		{ 
			//SC_ASSERT(m_listener == NULL); ASSERT can be here just for first time, after first time dialog can has already set listener
			m_listener = dlgListener; 
		}
		//@ Reset - resetuje hodnoty dialogu
		virtual void				Reset();
		//@ StartFadeOut - force dialog to disapear with exitValue
		virtual void				StartFadeOut(E_DialogExitValue exitValue);
		//@ InitDialog - called when dialog is pushed into dialog stack
		virtual void				InitDialog() {};


		//@ Setposition
		virtual void				SetPosition(const sysmath::C_Vector2 & vctPos);
		//@ SetColor
		virtual void				SetColor(const sysmath::C_Vector4 & color);
		//@ SetScale
		virtual void				SetScale(float fScale);
		//@ SetEnable
		virtual void				SetEnable(bool bEnable);
		//@ SetActive
		virtual void				SetActive(bool bActive);

		//@ AddWidget
		//@ add widgets to dialog
		bool							AddWidget(T_Widget widget)
		{ 
			return m_widgetManager.Add(widget);
		}

		//////////////////////////////////////////////////////////////////////////
		//@ INPUT C_WIDGET INTERFACE
		//////////////////////////////////////////////////////////////////////////
		virtual void		MouseMove(s32 inPosX, s32 inPosY);
		virtual void		MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);
		virtual void		MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);
		virtual void		OnKeyDown(E_KeyboardLayout keyCode);
		virtual void		OnKeyUp(E_KeyboardLayout keyCode);

		
		//////////////////////////////////////////////////////////////////////////
		// I_WIDGET_LISTENER INTERFACE - haas to send notification to dlgListener
		//////////////////////////////////////////////////////////////////////////
		//@ MouseDown
		virtual void		OnMouseDown(const sysutils::C_HashName & widgetUID)
		{ 
			if (m_listener)
				m_listener->OnMouseDown(widgetUID);
		}
		//@ OnMouseUp
		virtual void		OnMouseUp(const sysutils::C_HashName & widgetUID)
		{
			if (m_listener)
				m_listener->OnMouseUp(widgetUID);
		}
		//@ OnDriverPosChanged
		virtual void		OnDriverPosChanged(const sysutils::C_HashName & widgetUID, float fCurrValue)
		{
			if (m_listener)
				m_listener->OnDriverPosChanged(widgetUID, fCurrValue);
		}

		//////////////////////////////////////////////////////////////////////////
		//@ OPERATOR LISTENER INTERFACE
		virtual void		NotifyOperatorFinish(I_BaseOperator * callerOp);
		//////////////////////////////////////////////////////////////////////////


		DECLARE_RTTI

	private:
		//@ Transform input pos to dialog local
		//@ inputPos - transformed output
		void					TransfromToDialog(sysmath::C_Point & inputPos);
		//@ TryLoadDialogAnimation
		bool					TryLoadDialogAnimation(const sysutils::C_String animFileName);


	protected:
		sysutils::C_String	m_strHeader;		//header
		C_BaseWidgetManager	m_widgetManager;	//container for widgets

		T_Operator				m_animFadeInOp;	//animovany operator ktory zoscroluje menu dole
		T_Operator				m_animFadeOutOp;	//animovany operator ktory odscroluje menu hore

		E_DialogExitValue		m_retValue;		//return value from dialog
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_DialogCreator
	class C_DialogCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_Dialog(widgetUID, NULL/*listener*/));
		}
	};

	typedef boost::shared_ptr<C_Dialog>		T_Dialog;
}}
