/*
* filename:			C_Label.cpp
*
* author:			Kral Jozef
* date:				09/14/2010		20:50
* version:			1.00
* brief:
*/

#include "C_Label.h"
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_BitmapFontManager.h>
#include <GameFramework/StringTable/C_StringTable.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_Label, C_Widget)

	using namespace sysutils;
	using namespace syscore;
	using namespace sysmath;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Label::C_Label(const C_HashName & widgetUID)
		: C_Widget(widgetUID, NULL)
	{
		//SC_ASSERT(m_listener);	//listener pro label neexistuje
		m_FontScale = 1.f;
		m_Alignment = E_LA_LEFT;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromFrame
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Label::InitFromFrame(const syscore::T_Frame frame)
	{
		return T_Super::InitFromFrame(frame);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromXmlNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Label::InitFromXmlNode(const C_XmlNode & node)
	{
		bool bRes = T_Super::InitFromXmlNode(node);
		if (!bRes)
			return false;

		//@ FONT
		const C_XmlNode * tempNode = node.GetNode("font");
		if (!tempNode || !tempNode->IsValid())
		{
			TRACE_E("Missing or corrupted 'font' node");
			return false;
		}

		const C_XmlAttribute * fontUIDAtt = tempNode->GetAttribute("uid");
		if (!fontUIDAtt)
		{
			TRACE_E("Missing or corrupted 'uid' attribute in 'font' node");
			return false;
		}

		C_String fontUID = fontUIDAtt->GetValue();
		m_Font = T_BitmapFontManager::GetInstance().GetFont(C_HashName(fontUID));
		if (!m_Font)
		{
			TRACE_FE("Can not find fontUID: %s for widgetUID: %s", fontUID.c_str(), m_uniqueID.GetName());
			return false;
		}


		//@ FONT_SCALE
		tempNode = node.GetNode("font_scale");
		if (tempNode && tempNode->IsValid())
		{
			if (!tempNode->Read(m_FontScale))
			{
				TRACE_FE("Could not read value from 'font_scale' node in widgetUID: %s", m_uniqueID.GetName());
			}
		}

		//@ ALIGNMENT
		tempNode = node.GetNode("alignment");
		if (tempNode && tempNode->IsValid())
		{
			C_String alignment = (*tempNode)["type"];
			if (alignment == "CENTER")
				m_Alignment = E_LA_CENTER;
		}

		//@ TEXT
		//CreateText based on UniqueID
		m_RawText = T_StringTable::GetInstance().GetText(m_uniqueID.GetHash());
		if (!m_RawText.empty())
		{
			m_BitmapText = T_BitmapText(new C_BitmapText(GetPosition(), m_RawText, m_Font, E_TT_CZSK));
			m_BitmapText->SetScale(m_FontScale);
			m_BitmapText->SetColor(m_color);
			C_Point size = m_BitmapText->GetSize();
			
			if (m_Alignment == E_LA_CENTER)
			{
				m_BitmapText->SetPosition(GetPosition() - C_Vector2(size.x / 2.f, 0.f));
			}
			
			this->SetSize(size.x, size.y);
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Draw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Label::Draw(I_TransformationCallback & callback)
	{
		SC_ASSERT(m_bVisible);
		if (m_BitmapText)
			m_BitmapText->Render();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	void C_Label::DebugDraw(I_TransformationCallback & callback)
	{
		C_Vector4 dbgColor(.8f, .0f, .0f, 1.f);	//Red widgets!
		sysrender::C_Graphic graph;
		graph.RenderRect(C_Vector2((float)m_rect.m_left, (float)m_rect.m_top), C_Vector2((float)m_rect.m_right, (float)m_rect.m_bottom), dbgColor, false);
	}
#endif

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetText
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Label::SetText(sysutils::C_String strText)
	{
		if (!m_Font)
		{
			TRACE_FE("No font defined for Label: %s", m_uniqueID.GetName());
			return;
		}
		m_RawText = strText;
		//@ create bitmapText
		m_BitmapText = T_BitmapText(new C_BitmapText(GetPosition(), m_RawText, m_Font, E_TT_CZSK));
		m_BitmapText->SetScale(m_FontScale);
		m_BitmapText->SetColor(m_color);

		C_Point size = m_BitmapText->GetSize();

		if (m_Alignment == E_LA_CENTER)
		{
			m_BitmapText->SetPosition(GetPosition() - C_Vector2(size.x / 2.f, 0.f));
		}

		this->SetSize(size.x, size.y);
	}

}}