/*
* filename:			C_Button.h
*
* author:			Kral Jozef
* date:				04/01/2009		23:24
* version:			1.00
* brief:				Button widget
*/

#pragma once

#include "C_Picture.h"
#include <SysCore/Core/C_BitmapFont.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Button
	class GAMEFRAMEWORK_API C_Button : public C_Picture
	{
		typedef C_Picture T_Super;
	public:
		//@ c-tor
		C_Button(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener);
		//@ d-tor
		~C_Button() {};

		//@ InitFromXmlNode
		virtual bool		InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ InitFromFrame
		virtual bool		InitFromFrame(const syscore::T_Frame frame);
		//@ Draw
		virtual void		Draw(I_TransformationCallback & callback);


		//////////////////////////////////////////////////////////////////////////
		// SET/GET Methods
		//@ SetFocusImage
		void					SetFocusImage(T_Graphics image) { m_focusImg = image; }


		//////////////////////////////////////////////////////////////////////////
		//@ MOUSE EVENTS

		//@ MouseEnter
		void					MouseEnter();
		//@ MouseLeave
		void					MouseLeave();
		//@ MouseDown
		void					MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);
		//@ MouseUp
		void					MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);

		DECLARE_RTTI

	protected:
		T_Graphics					m_focusImg;		//image when mouse is over btn
		T_Graphics					m_disableImg;	//image when button is disabled

		//@ text related fields
		syscore::T_BitmapFont	m_font;	//
		float							m_fFontScale;
		sysutils::C_String		m_text;
		
		static const T_Hash32	C_SND_MOUSE_ENTER;	//sound fx when mouse come enter onto button
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_ButtonCreator
	class C_ButtonCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_Button(widgetUID, listener));
		}
	};
}}