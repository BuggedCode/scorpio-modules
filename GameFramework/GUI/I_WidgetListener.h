/*
* filename:			I_WidgetListener.h
*
* author:			Kral Jozef
* date:				04/15/2009		23:18
* version:			1.00
* brief:				Button listener
*/

#pragma once

#include <SysUtils/C_HashName.h>

namespace scorpio{ namespace framework{

	//@ JK 21-01-11 for support of dialogs
	enum E_DialogExitValue
	{
		E_DEV_UNDEF = -1,
		E_DEV_OK = 1,
		E_DEV_CANCEL,
		E_DEV_YES,
		E_DEV_NO,
	};

	//////////////////////////////////////////////////////////////////////////
	//@ I_ButtonListener
	class I_WidgetListener
	{
	public:
		//@ MouseEnter
		virtual void		OnMouseEnter(const sysutils::C_HashName & widgetUID) {};
		//@ MouseLeave
		virtual void		OnMouseLeave(const sysutils::C_HashName & widgetUID) {};
		//@ MouseMove
		virtual void		OnMouseMove(s32 inPosX, s32 inPosY) {};
		//@ MouseDown
		virtual void		OnMouseDown(const sysutils::C_HashName & widgetUID) {};
		//@ MouseDown
		virtual void		OnMouseUp(const sysutils::C_HashName & widgetUID) {};
		//@ OnExitDialog
		//@ widgetUID - dialog UID, only dialog should use this!
		virtual void		OnExitDialog(E_DialogExitValue retValue, const sysutils::C_HashName & widgetUID) {};
		//@ OnValueChanged/OnDriverPositionChanged - SliderListener
		//@ widgetUID - id of widget which send this notification
		//@ fCurrValue - actual relative value <0, 1>
		virtual void		OnDriverPosChanged(const sysutils::C_HashName & widgetUID, float fCurrValue) {};
	};

}}
