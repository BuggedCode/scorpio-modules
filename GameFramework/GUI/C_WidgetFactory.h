/*
* filename:			C_WidgetFactory.h
*
* author:			Kral Jozef
* date:				05/23/2009		16:59
* version:			1.00
* brief:				singleton widget factory
*/

#pragma once

#include <SysUtils/C_String.h>
#include "C_Widget.h"
#include <map>

namespace scorpio{ namespace framework{

	struct S_WidgetDescriptor
	{
		//c-tor
		S_WidgetDescriptor(I_WidgetListener * listener)
			: m_listener(listener) {};

		//@ GetListener
		I_WidgetListener	*	GetListener() { return m_listener; }

		sysutils::C_String	m_typeName;
		
		//id konkretneho widgetu kcez ktore je mozne k widgetom pristupovat cez widget manager,
		//nepovinny param - factory prideli vlastne id ?? TODO
		sysutils::C_HashName	m_hashNameUID;

	private:
		I_WidgetListener	*	m_listener;
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_WidgetFactory
	class GAMEFRAMEWORK_API C_WidgetFactory
	{
	public:
		//@ CreateInstance
		//@ return false if something fail
		static bool		CreateInstance()
		{
			SC_ASSERT(!m_instance && "WidgetFactory already created!");

			m_instance = new C_WidgetFactory();
			if (!m_instance)
				return false;

			return true;
		}

		//@ GetInstance
		static C_WidgetFactory &	GetInstance()
		{
			SC_ASSERT(m_instance && "WidgetFactory not created");
			return *m_instance;
		}

		//@ DestroyInstance
		static void		DestroyInstance()
		{
			SC_SAFE_DELETE(m_instance);
		}

		//@ RegisterCreator
		bool				RegisterCreator(sysutils::C_String widgetTypeName, T_WidgetCreator creator);

		//@ CreateWidget
		T_Widget			CreateWidget(S_WidgetDescriptor & desc);

	private:
		T_Id32			GenerateUID() 
		{ 
			SC_ASSERT(m_freeID < UINT_MAX);	//dosiahli sme strop
			return ++m_freeID; 
		}



	private:
		//@ c-tor
		C_WidgetFactory();
		//@ copy c-tor
		C_WidgetFactory(const C_WidgetFactory & copy);	//not implemented
		//@ assignment operator
		C_WidgetFactory & operator=(const C_WidgetFactory & copy);	//not implemented
		//@ d-tor
		~C_WidgetFactory();

		//@ attributes
		static C_WidgetFactory	*	m_instance;

		T_Id32							m_freeID;

		typedef std::map<T_Hash32, T_WidgetCreator>	T_CreatorContainer;
		T_CreatorContainer			m_container;	//zoznam znamych typov widgetow a ich creatorov
	};

}}