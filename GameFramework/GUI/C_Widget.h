/*
* filename:			C_Widget.h
*
* author:			Kral Jozef
* date:				04/01/2009		21:13
* version:			1.00
* brief:				Base widget class
*/

#pragma once

#include <Common/Pragmas.h>
#include <Common/C_RTTI.h>
#include <Common/I_NonCopyAble.h>
#include <Common/types.h>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Rect.h>
#include <SysUtils/C_HashName.h>
#include <SysUtils/xml/C_XmlNode.h>
#include <boost/shared_ptr.hpp>
#include "I_WidgetListener.h"
#include <GameFramework/GameFrameworkApi.h>
#include <GameFramework/Core/I_TransformationCallback.h>
#include <SysCore/Framework/FrameworkTypes.h>
#include <SysCore/Frames/C_Frame.h>
#include <SysCore/Core/Animation/C_AnimationTrack.h>
#include <GameFramework/Operators/I_BaseOperator.h>
#include <SysCore/Geometry/C_PolyShape.h>


namespace scorpio{
	namespace sysutils{
		class C_XmlNode;
	}
}

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Widget
	class GAMEFRAMEWORK_API C_Widget : public I_OperatorListener
	{
	public:
		enum E_MouseButton
		{
			E_MB_LEFT = 1,
			E_MB_RIGHT,
			E_MB_MIDDLE
		};

		//@ c-tor
		C_Widget(const sysutils::C_HashName & widgetUID, I_WidgetListener * widgetListener);
		//@ d-tor
		virtual ~C_Widget();

		//@ 1. InitFromFrame - called before TnitFormXmlNode
		virtual bool		InitFromFrame(const syscore::T_Frame frame);
		//@ 2. InitFromXmlNode
		virtual bool		InitFromXmlNode(const sysutils::C_XmlNode & node);

		//@ Update
		//@ param inDeltaTime - different time between last and actual called update in ms
		virtual void		Update(u32 inDeltaTime);
		//@ Draw
		virtual void		Draw(I_TransformationCallback & callback) = 0;
#ifndef MASTER
		//@ DebugDraw
		virtual void		DebugDraw(I_TransformationCallback & callback) = 0;
#endif

		//@ Reset - reset states from input (mouse over/...)
		virtual void		ResetInputStates();
		//@ Resize
		void					Resize(const sysmath::C_Rect & rect) { m_rect = rect; }


		//////////////////////////////////////////////////////////////////////////
		//@ @ OPERATOR NOTIFICATION
		void					NotifyOperatorFinish(I_BaseOperator * callerOp) {};

		//////////////////////////////////////////////////////////////////////////
		//@ INPUT

		//@ MouseEnter
		virtual void		MouseEnter() { if (m_listener && m_bEnabled) m_listener->OnMouseEnter(m_uniqueID); }
		//@ MouseLeave
		virtual void		MouseLeave() { if (m_listener && m_bEnabled) m_listener->OnMouseLeave(m_uniqueID); }
		//@ MouseMove
		virtual void		MouseMove(s32 inPosX, s32 inPosY) { if (m_listener && m_bEnabled) m_listener->OnMouseMove(inPosX, inPosY); }
		//@ MouseDown
		virtual void		MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID) { if (m_listener && m_bEnabled) m_listener->OnMouseDown(m_uniqueID); }
		//@ MouseDown
		virtual void		MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID) { if (m_listener && m_bEnabled) m_listener->OnMouseUp(m_uniqueID); }
		//@ KeyDown
		virtual void		OnKeyDown(E_KeyboardLayout keyCode) {};
		//@ KeyUp
		virtual void		OnKeyUp(E_KeyboardLayout keyCode) {};



		//////////////////////////////////////////////////////////////////////////
		//@ SET/GET methodes
		sysutils::C_HashName	GetUID() const { return m_uniqueID; }

		//@ SetPosition
		virtual void		SetPosition(const sysmath::C_Vector2 & vctPos)
		{
			m_pivot = vctPos;
			m_rect.MoveCenterTo(m_pivot);
			//translate polyshape !!
		}
		//@ GetPosition
		const sysmath::C_Vector2	&	GetPosition() const { return m_pivot; }

		//@ SetSize
		virtual void		SetSize(u32 inWidth, u32 inHeight) 
		{ 
			m_fScale = 1.f; 
			m_size.x = inWidth; 
			m_size.y = inHeight; 
			m_rect.m_left = (s32)(m_pivot.m_x - inWidth/2.f);
			m_rect.m_right = (s32)(m_pivot.m_x + inWidth/2.f);
			m_rect.m_top = (s32)(m_pivot.m_y - inHeight/2.f);
			m_rect.m_bottom = (s32)(m_pivot.m_y + inHeight/2.f);
		}

		//@ SetScale - uniform scale
		virtual void		SetScale(float fScale) { m_fScale = fScale; /*RecalcDstRect();*/ }
		virtual void		SetRotation(float fAngle) {};	//widgets doesn't support rotation - just for templated anim function
		
		//@ SetVisible
		void					SetVisible(bool bVisible) { m_bVisible = bVisible; }
		bool					IsVisible() const { return m_bVisible; }
		//@ SetEnable
		virtual void		SetEnable(bool bEnable) { m_bEnabled = bEnable; }
		virtual bool		IsEnable() const { return m_bEnabled; }

		//@ SetActive
		virtual void		SetActive(bool bActive) { m_bActive = bActive; }
		virtual bool		GetActive(bool bActive) const { return m_bActive; }

		//@ SetFocus
		virtual void		SetFocus(bool bFocus) { m_HasFocus = bFocus; }
		bool					HasFocus() const { return m_HasFocus; }

		//@ SetColor
		virtual void		SetColor(const sysmath::C_Vector4 & color) { m_color = color; }
		const sysmath::C_Vector4	&	GetColor() const { return m_color; }

		bool					IsMouseOver() const { return m_bMouseOver; }

		//@ Contains - je mys nad widgetom - moze byt pretazene a pocitane z bound polygonu/mapy
		virtual bool		Contains(s32 posX, s32 posY) const;
		//@ IsCaptured
		virtual bool		IsCaptured() const { return false; }
		//@ NoteTrackNotify
		virtual void		NoteTrackNotify(T_Hash32 eventId, T_Hash32 param1, T_Hash32 param2) {};


		DECLARE_ROOT_RTTI

	//private:
	protected:
		//@ RecalcRect - recalculate render destination rect
		//void					RecalcDstRect();
		//@ DbgDraw
		void					DbgDraw();	//TODO not for MASTER

	protected:
		I_WidgetListener	*	m_listener;
		sysutils::C_HashName	m_uniqueID;		//uniqueID
		
		sysmath::C_Vector2	m_pivot;			//position center of widget!
		sysmath::C_Rect		m_rect;
		sysmath::C_Point		m_size;			//orig size pro scale
		float						m_fScale;
		bool						m_bVisible;
		bool						m_bActive;	//active for logic!
		bool						m_bEnabled;			//listen/react on input
		bool						m_HasFocus;
		bool						m_bMouseDown;		//mouse is down
		bool						m_bMouseOver;		//mouse is over widget
		sysmath::C_Vector4	m_color;
		u16						m_TabOrder;	// 0 == Ignore Tab

		syscore::T_AnimationTrack	m_animTrack;	//animation track
		T_Operator				m_animOperator;
		syscore::T_PolyShape	m_collShape;	//if !NULL then collision for input
	};


	typedef boost::shared_ptr<C_Widget>		T_Widget;


	//////////////////////////////////////////////////////////////////////////
	//@ Interface for widget creator
	class I_WidgetCreator : public I_NonCopyAble
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const = 0;
	};

	typedef boost::shared_ptr<I_WidgetCreator>	T_WidgetCreator;
}}
