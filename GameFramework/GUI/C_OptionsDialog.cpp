/*
* filename:			C_OptionsDialog.cpp
*
* author:			Kral Jozef
* date:				06/02/2009		21:20
* version:			1.00
* brief:				Base options dialog
*/

#include "C_OptionsDialog.h"
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_TextureManager.h>
#include "C_CheckBox.h"
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_OptionsDialog, C_Dialog)

	using namespace scorpio::sysutils;

	//@ tohle su idcka z xmlka!
	const C_HashName	C_OptionsDialog::C_CHB_FULLSCREEN_UID = C_HashName("IDCCHB_Fullscreen");
	const C_HashName	C_OptionsDialog::C_SLD_MUSICVOLUME_UID = C_HashName("IDCSLD_MusicVolume");
	const C_HashName	C_OptionsDialog::C_SLD_SOUNDVOLUME_UID = C_HashName("IDCSLD_SoundVolume");
	const C_HashName	C_OptionsDialog::C_BTN_OK_UID = C_HashName("IDCBTN_Ok");
	const C_HashName	C_OptionsDialog::C_BTN_CANCEL_UID = C_HashName("IDDLGBTN_OptionsCancel");
	

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_OptionsDialog::C_OptionsDialog(const C_HashName & widgetUID, I_WidgetListener * listener)
		: C_Dialog(widgetUID, listener)
	{
		//
	}


	//////////////////////////////////////////////////////////////////////////
	//@ AssignSettings
	//////////////////////////////////////////////////////////////////////////
	void C_OptionsDialog::AssignSettings(C_OptionsSettings & settings)
	{
		SC_ASSERT(&settings);	//settings must be valid!
		m_settings = &settings;
		m_bkpSettings = settings;

		//este priradit widgetom hodnoty
		//FULLSCREEN CHECKBOX
		T_Widget widget = m_widgetManager.GetWidget(C_CHB_FULLSCREEN_UID);
		SC_ASSERT(widget);
		C_CheckBox * checkbox = DynamicCast<C_CheckBox*>(widget.get());
		SC_ASSERT(checkbox);
		checkbox->AssignValue(settings.m_bFullscreen);
	}


	//////////////////////////////////////////////////////////////////////////
	// WIDGET LISTENER IMPLEMENTATION
	//////////////////////////////////////////////////////////////////////////
	void C_OptionsDialog::OnMouseUp(const sysutils::C_HashName & widgetUID)
	{
		if (!m_listener)
			return;

		if (widgetUID == C_HashName(C_CHB_FULLSCREEN_UID))
			m_listener->OnValueChanged(I_OptionsDialogListener::E_OVCH_FULLSCREEN, m_uniqueID);
		else if (widgetUID == C_HashName(C_SLD_MUSICVOLUME_UID))
			m_listener->OnValueChanged(I_OptionsDialogListener::E_OVCH_MUSIC_VOLUME, m_uniqueID);
		else if (widgetUID == C_HashName(C_SLD_SOUNDVOLUME_UID))
			m_listener->OnValueChanged(I_OptionsDialogListener::E_OVCH_SOUNDFX_VOLUME, m_uniqueID);
		else if (widgetUID == C_HashName(C_BTN_OK_UID))
			m_listener->OnExitDialog(E_DEV_OK, m_uniqueID);
		else if (widgetUID == C_HashName(C_BTN_CANCEL_UID))
		{
			//restore original settings
			*m_settings = m_bkpSettings;
			m_listener->OnExitDialog(E_DEV_CANCEL, m_uniqueID);
		}
		else
			SC_ASSERT(false);	//sem to nemoze prist
	}


}}