/*
* filename:			C_PushButton.h
*
* author:			Kral Jozef
* date:				04/21/2009		22:09
* version:			1.00
* brief:				PushButton widget interface
*/

#include "C_Button.h"
#include <GameFramework/GameFrameworkApi.h>

namespace scorpio{ namespace framework{

	class GAMEFRAMEWORK_API C_PushButton : public C_Button
	{
		typedef C_Button	T_Super;

	public:
		//@ c-tor
		C_PushButton(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) : C_Button(widgetUID, listener) {};
		//@ d-tor
		~C_PushButton() {};

		//@ Draw
		void					Draw(I_TransformationCallback & callback);


		//////////////////////////////////////////////////////////////////////////
		// SET/GET Methods
		void					SetPushImage(T_Graphics image) { m_pushImg = image; }


		//////////////////////////////////////////////////////////////////////////
		//@ MOUSE EVENTS

		//@ MouseEnter
		void					MouseEnter();
		//@ MouseLeave
		void					MouseLeave();
		//@ MouseMove
		void					MouseMove(s32 inPosX, s32 inPosY);
		//@ MouseDown
		void					MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);
		//@ MouseDown
		void					MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);


	private:				
		T_Graphics				m_pushImg;		//image when button is pushed
		bool						m_bPushed;		//button is pushed
	};

}}