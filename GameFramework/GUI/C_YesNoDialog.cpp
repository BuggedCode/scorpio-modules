/*
* filename:			C_YesNoDialog.cpp
*
* author:			Kral Jozef
* date:				06/09/2009		21:50
* version:			1.00
* brief:				YesNo dialog
*/

#include "C_YesNoDialog.h"
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_YesNoDialog, C_Dialog)

	using namespace scorpio::sysutils;

	//@ tohle su idcka z xmlka!
	static const C_HashName	C_BTN_YES_UID = C_HashName("IDCBTN_Yes");
	static const C_HashName	C_BTN_NO_UID = C_HashName("IDCBTN_No");


	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_YesNoDialog::C_YesNoDialog(const C_HashName & widgetUID, I_WidgetListener * listener)
		: C_Dialog(widgetUID, listener)
	{
		//
	}

	//////////////////////////////////////////////////////////////////////////
	// WIDGET LISTENER IMPLEMENTATION
	//////////////////////////////////////////////////////////////////////////
	void C_YesNoDialog::OnMouseUp(const sysutils::C_HashName & widgetUID)
	{
		T_Widget widget = m_widgetManager.GetWidget(widgetUID);
		widget->ResetInputStates();

		if (!m_listener)
			return;

		if (widgetUID == C_HashName(C_BTN_YES_UID))
		{
			m_retValue = E_DEV_YES;
		}
		else if (widgetUID == C_HashName(C_BTN_NO_UID))
		{
			m_retValue = E_DEV_NO;
		}
		else
			SC_ASSERT(false);	//sem to nemoze prist
		
		StartFadeOut(m_retValue);
	}


}}