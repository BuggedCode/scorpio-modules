/*
* filename:			C_Picture.cpp
*
* author:			Kral Jozef
* date:				05/27/2009		21:17
* version:			1.00
* brief:				picture widget
*/

#include "C_Picture.h"
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_TextureManager.h>
#include <SysCore/Frames/C_Visual.h>
#include<SysCore/Managers/C_VertexBufferManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_Picture, C_Widget)

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;
	using namespace scorpio::sysrender;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_Picture::C_Picture(const C_HashName & widgetUID, I_WidgetListener * listener)
		: T_Super(widgetUID, listener)
	{
		//SC_ASSERT(listener);	//listener pro picture nemusi existovat
	}


	//////////////////////////////////////////////////////////////////////////
	//@ InitFromXmlNode
	//////////////////////////////////////////////////////////////////////////
	bool C_Picture::InitFromXmlNode(const C_XmlNode & node)
	{
		bool bRes = T_Super::InitFromXmlNode(node);
		if (!bRes)
			return false;

		//@ IMAGE
		const C_XmlNode * tempNode = node.GetNode("image");
		if (tempNode && tempNode->IsValid())
		{
			const C_XmlAttribute * imgAtt = tempNode->GetAttribute("img");
			if (!imgAtt)
			{
				TRACE_E("Missing or corrupted 'img' attribute in 'image' node");
				return false;
			}

			C_String imgPath = imgAtt->GetValue();
			m_image = syscore::T_TextureManager::GetInstance().GetTexture(imgPath);

			if (m_image)
				SetSize((u32)m_image->GetWidth(), (u32)m_image->GetHeight());
		}

		m_VertexBuffer = T_VertexBufferManager::GetInstance().GenerateVertexBufferObject();
		m_VertexBuffer->CreateFromRect(C_Vector2(), C_Vector2((float)m_size.x, (float)m_size.y), 0.f, C_Vector2(0.f, 0.f), C_Vector2(1.f, 1.f));


		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromFrame
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Picture::InitFromFrame(const T_Frame frame)
	{
		bool bRes = T_Super::InitFromFrame(frame);
		if (!bRes)
			return false;

		T_Visual vis = boost::dynamic_pointer_cast<C_Visual>(frame);
		m_image = vis->GetTexture();

		m_VertexBuffer = T_VertexBufferManager::GetInstance().GenerateVertexBufferObject();
		m_VertexBuffer->CreateFromRect(C_Vector2(), C_Vector2((float)m_size.x, (float)m_size.y), 0.f, C_Vector2(0.f, 0.f), C_Vector2(1.f, 1.f));
		
		if (m_image && m_VertexBuffer)
			return true;

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Draw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Picture::Draw(I_TransformationCallback & callback)
	{
		SC_ASSERT(m_bVisible);
		if (m_image)
		{
			//m_image->Render(m_pivot, m_size, m_color, 0.f, m_fScale);
			m_image->RenderVBO(m_VertexBuffer, m_pivot, 0.f, m_fScale, m_color);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	void C_Picture::DebugDraw(I_TransformationCallback & callback)
	{
		C_Vector4 dbgColor(.8f, .0f, .0f, 1.f);	//Red widgets!
		sysrender::C_Graphic graph;
		graph.RenderRect(m_pivot, m_size, dbgColor, false, 0.f, m_fScale);
	}
#endif


}}