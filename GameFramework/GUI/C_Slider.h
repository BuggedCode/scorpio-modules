/*
* filename:			C_Slider.h
*
* author:			Kral Jozef
* date:				03/09/2011		11:22
* version:			1.00
* brief:				Simple slider implementation, for now just for horizontal type
*/

#pragma once

#include "C_Button.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Slider
	class GAMEFRAMEWORK_API C_Slider : public C_Picture
	{
	public:
		typedef C_Picture	T_Super;
	public:
		//@ c-tor
		C_Slider(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener)
			: C_Picture(widgetUID, listener), m_Dragging(false) {};
		//@ d-tor
		~C_Slider() {};

		//@ InitFromXmlNode
		virtual bool		InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ InitFromFrame
		virtual bool		InitFromFrame(const syscore::T_Frame frame);
		//@ Draw
		virtual void		Draw(I_TransformationCallback & callback);


		//////////////////////////////////////////////////////////////////////////
		//@ MOUSE EVENTS
		//@ MouseDown
		void					MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);
		//@ MouseUp
		void					MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);
		//@ MouseMove
		void					MouseMove(s32 inPosX, s32 inPosY);
		//@ MouseLeave
		void					MouseLeave();

		//@ IsCaptured
		virtual bool		IsCaptured() const { return m_Dragging; }

		//////////////////////////////////////////////////////////////////////////
		//@ SetDriverPosition
		void					SetDriverPosition(float fPos);



		DECLARE_RTTI


	private:
		T_Widget					m_Driver;	//picture widget
		bool						m_Dragging;
		sysmath::C_Vector2	m_MinPos;	//min limit for driver
		sysmath::C_Vector2	m_MaxPos;	//max limit for driver
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_SliderCreator
	class C_SliderCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_Slider(widgetUID, listener));
		}
	};

}}