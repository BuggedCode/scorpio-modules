/*
* filename:			C_Widget.cpp
*
* author:			Kral Jozef
* date:				04/01/2009		21:20
* version:			1.00
* brief:				Base widget class
*/

#include "C_Widget.h"
#include <SysUtils/xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_AnimationManager.h>
#include <GameFramework/Controllers/C_AnimController.h>
#include <SysCore/Frames/C_Area.h>
#include <SysCore/Render/C_Graphic.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_ROOT_RTTI(C_Widget)

	using namespace scorpio::sysmath;
	using namespace scorpio::sysutils;
	using namespace scorpio::syscore;

	static const C_Vector4 C_DBG_COLOR		=	C_Vector4(0.5f, 0.5f, 0.5f, 0.5f); 

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_Widget::C_Widget(const C_HashName & widgetUID, I_WidgetListener * widgetListener)
		: m_listener(widgetListener), m_uniqueID(widgetUID), m_rect(0, 0, 0, 0), m_fScale(1.f), m_color(1.f,1.f,1.f,1.f)
	{
		m_bVisible = true;
		//m_mouseVisible;
		m_bEnabled = true;
		m_bActive = true;
		m_bMouseDown = false;
		m_bMouseOver = false;
		m_TabOrder = 0;	//ignore tab
		m_HasFocus = false;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	C_Widget::~C_Widget()
	{
		//
	}


	//////////////////////////////////////////////////////////////////////////
	//@ InitFromXmlNode
	//////////////////////////////////////////////////////////////////////////
	bool C_Widget::InitFromXmlNode(const C_XmlNode & node)
	{
		if (!node.IsValid())
			return false;

		//@ POSITION X,Y
		const C_XmlNode * tempNodeX = node.GetNode("positionX");
		const C_XmlNode * tempNodeY = node.GetNode("positionY");
		if (tempNodeX && tempNodeX->IsValid() && tempNodeY && tempNodeY->IsValid())
		{
			s32 posX = 0;
			if (!tempNodeX->Read(posX))
			{
				TRACE_E("Can not read value of positionX node");
				return false;
			}

			s32 posY = 0;
			if (!tempNodeY->Read(posY))
			{
				TRACE_E("Can not read value of positionY node");
				return false;
			}

			this->SetPosition(C_Vector2((float)posX, (float)posY));
		}


		//@ COLOR
		const C_XmlNode * tempNode = node.GetNode("color");
		if (tempNode && tempNode->IsValid())
		{
			if (!tempNode->Read(m_color))
			{
				TRACE_E("Can not read value of color node for widget");
			}
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromFrame
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Widget::InitFromFrame(const T_Frame frame)
	{
		//////////////////////////////////////////////////////////////////////////
		const C_Point & size = frame->GetSize();
		//@ Need transform to world
		SetPosition(frame->GetLocalPosition());	//for widgets linked inder dialogs
		SetSize(size.x, size.y);
		SetColor(frame->GetColor());
		//@ SetCollision Shape even if is NULL
		T_Frame collFrm = frame->FindFrame(C_FNVHash::computeHash32("Coll"));
		T_Area area = boost::dynamic_pointer_cast<C_Area>(collFrm);
		if (collFrm && area)
		{
			m_collShape = area->GetShape()->Clone();
			//TODO m_collShape->Transform(m_)
		}

		//@ TODO JK if (mesh->IsAnimated())
		m_animTrack = T_AnimationManager::GetInstance().GetAnimation(m_uniqueID);
		if (m_animTrack)
		{
			m_animOperator = T_Operator(new C_WidgetAnimController(m_animTrack, *this, this));
		}
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Widget::Update(u32 inDeltaTime)
	{
		if (m_animOperator)
			m_animOperator->Update(inDeltaTime);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Reset
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Widget::ResetInputStates()
	{
		m_bMouseDown = false;
		m_bMouseOver = false;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ DbgDraw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Widget::DbgDraw()
	{
		//ccw draw bounding of window
		sysrender::C_Graphic dbgDrawSurface;
		dbgDrawSurface.RenderLine(C_Vector2((float)m_rect.m_left, (float)m_rect.m_top),			C_Vector2((float)m_rect.m_left,	(float)m_rect.m_bottom),	C_DBG_COLOR);
		dbgDrawSurface.RenderLine(C_Vector2((float)m_rect.m_left, (float)m_rect.m_bottom),		C_Vector2((float)m_rect.m_right,	(float)m_rect.m_bottom),	C_DBG_COLOR);
		dbgDrawSurface.RenderLine(C_Vector2((float)m_rect.m_right, (float)m_rect.m_bottom),	C_Vector2((float)m_rect.m_right,	(float)m_rect.m_top),		C_DBG_COLOR);
		dbgDrawSurface.RenderLine(C_Vector2((float)m_rect.m_right, (float)m_rect.m_top),		C_Vector2((float)m_rect.m_left,	(float)m_rect.m_top),		C_DBG_COLOR);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Contains
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Widget::Contains(s32 posX, s32 posY) const
	{
		if (m_collShape)
		{
			return m_collShape->IsInside(C_Vector2((float)posX, float(posY)));
		}

		if (posX >= m_rect.m_left && posX <= m_rect.m_left + m_rect.GetWidth() && posY >= m_rect.m_top && posY <= m_rect.m_top + m_rect.GetHeight())
			return true;

		return false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ RecalcRect
	//
	//////////////////////////////////////////////////////////////////////////
	//@ Has sense only for collision detection - but need fix this function -> add transformation by center!!
	/*void C_Widget::RecalcDstRect()
	{
		float fWidth = m_size.x * m_fScale;
		m_rect.m_right = m_rect.m_left + (s32)fWidth;

		float fHeight = m_size.y * m_fScale;
		m_rect.m_bottom = m_rect.m_top + (s32)fHeight;
		return;
	}*/
}}