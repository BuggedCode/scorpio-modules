/*
* filename:			C_FloatingButton.h
*
* author:			Kral Jozef
* date:				06/13/2009		14:25
* version:			1.00
* brief:				Floating button widget
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include "C_Button.h"
#include <GameFramework/Operators/I_BaseOperator.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_FloatingButton
	class GAMEFRAMEWORK_API C_FloatingButton : public C_Button
	{
		typedef C_Button	T_Super;
	public:
		//@ c-tor
		C_FloatingButton(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener);
		//@ d-tor
		~C_FloatingButton() {};

		//@ InitFromXmlNode - store orig rectangles for reset!
		virtual bool		InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ Update
		//@ param inDeltaTime - difference between last called update
		virtual void		Update(u32 inDeltaTime);
		//@ ResetInputStates - reset states from input (mouse over/...)
		virtual void		ResetInputStates();
		//@ SetPosition
		virtual void		SetPosition(const sysmath::C_Vector2 & vctPos);
		//@ SetActive
		//virtual void		SetActive(bool bActive);



		//////////////////////////////////////////////////////////////////////////
		//@ MOUSE EVENTS

		//@ MouseEnter
		virtual void		MouseEnter();
		//@ MouseLeave
		virtual void		MouseLeave();




		//////////////////////////////////////////////////////////////////////////
		// OPERATOR_LISTENER INTERFACE
		void					NotifyOperatorFinish(I_BaseOperator * callerOp);

	protected:
		T_Operator			m_extendOp;
		T_Operator			m_shrinkOp;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_FloatingButtonCreator
	class C_FloatingButtonCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_FloatingButton(widgetUID, listener));
		}
	};


}}