/*
* filename:			C_EditBox.cpp
*
* author:			Kral Jozef
* date:				09/14/2010		20:50
* version:			1.00
* brief:
*/

#include "C_EditBox.h"
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_BitmapFontManager.h>
#include <SysCore/Framework/C_Input.h>
#include <SysAudio/C_AudioManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_EditBox, C_Picture)

	using namespace syscore; 
	using namespace sysutils;
	using namespace sysmath; 
	using namespace sysaudio; 

	static const u8 C_TEXT_OFFSET_FROM_LEFT = 13;
	static const u8 C_TEXT_OFFSET_FROM_BOTTOM = 7;
	static const u8 C_DEFAULT_MAX_CHARACTERS = 14;

	static const T_Hash32 C_SND_KEY_DOWN = C_HashName("KEYBOARD_TYPING").GetHash();


	//////////////////////////////////////////////////////////////////////////
	//
	//@ C_EditBox
	//
	//////////////////////////////////////////////////////////////////////////
	C_EditBox::C_EditBox(const C_HashName & widgetUID, I_WidgetListener * listener)
		: C_Picture(widgetUID, listener)
	{
		m_Scale = 1.f;
		m_CaretHeight = 0.f;
		m_MaxChars = C_DEFAULT_MAX_CHARACTERS;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromXmlNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_EditBox::InitFromXmlNode(const sysutils::C_XmlNode & node)
	{
		bool bRes = T_Super::InitFromXmlNode(node);
		if (!bRes)
			return false;

		//@ InitFont
		m_Font = T_BitmapFontManager::GetInstance().GetFont(C_HashName("GUI_DEFAULT"));

		//@ calc scale
		if (!isZero(m_CaretHeight))
			m_Scale =  m_CaretHeight / (float)m_Font->GetHeight();

		return bRes && m_Font;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromFrame
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_EditBox::InitFromFrame(const T_Frame frame)
	{
		bool bRes = T_Super::InitFromFrame(frame);

		T_Frame caret = frame->FindFrame("Caret");
		if (!caret)
		{
			TRACE_W("EditBox has no underlinked 'Caret' frame")
			return bRes;
		}

		C_AABB2 aabb = caret->GetAABB();

		C_Vector2 pos = caret->GetLocalPosition() + m_pivot;
		aabb.Translate(pos);

		m_TextPos = C_Vector2(aabb.GetMax());
		m_CaretHeight = aabb.GetHeight();

		T_Visual visCaret = boost::dynamic_pointer_cast<C_Visual>(caret->Clone());
		m_Caret = T_Caret(new C_Caret(500, visCaret));
		C_Vector2 caretPos = caret->GetLocalPosition() + m_pivot;
		m_Caret->SetPosition(caretPos);

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EditBox::Update(u32 inDeltaTime)
	{
		T_Super::Update(inDeltaTime);
		if (m_HasFocus && m_Caret)
		{
			m_Caret->Update(inDeltaTime);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Draw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EditBox::Draw(I_TransformationCallback & callback)
	{
		T_Super::Draw(callback);
		
		//@ drawText
		if (m_BitmapText)
			m_BitmapText->Render();
		if (m_Caret)
			m_Caret->Render();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ResetInputStates
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EditBox::ResetInputStates()
	{
		m_Text = "";
		m_BitmapText.reset();
		if (m_Caret)
			m_Caret->SetPosition(m_TextPos - C_Vector2(0.f, m_CaretHeight/2.f));
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetFocus
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EditBox::SetFocus(bool bFocus)
	{
		T_Super::SetFocus(bFocus);
		if (m_Caret && !bFocus)	//lost focus -> hide caret
			m_Caret->SetVisible(false);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ OnKeyDown
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EditBox::OnKeyDown(E_KeyboardLayout keyCode)
	{
		T_AudioManager::GetInstance().PlaySoundEx(C_SND_KEY_DOWN);

		if (keyCode == SDLK_BACKSPACE)
		{
			u32 len = m_Text.length();
			if (!len)
				return;
			if (len == 1)
			{
				m_Text = "";
				m_BitmapText = T_BitmapText(new C_BitmapText(m_TextPos, m_Text, m_Font, E_TT_CZSK));
				m_BitmapText->SetScale(m_Scale);
				if (m_Caret)
					m_Caret->SetPosition(m_TextPos - C_Vector2(0.f, m_CaretHeight/2.f));
				return;
			}

			m_Text = m_Text.Left(m_Text.length() - 2);
			m_BitmapText = T_BitmapText(new C_BitmapText(m_TextPos, m_Text, m_Font, E_TT_CZSK));
			m_BitmapText->SetScale(m_Scale);
			
			//@ move carret left
			if (m_Caret)
			{
				C_Vector2 caretPos = m_TextPos;
				caretPos.m_x += m_BitmapText->GetWidth();
				m_Caret->SetPosition(caretPos - C_Vector2(0.f, m_CaretHeight/2.f));
			}
		}
		else if ((keyCode >= SDLK_a && keyCode <= SDLK_z) || (keyCode >= SDLK_0 && keyCode <= SDLK_9))
		{
			if (m_Text.length() == m_MaxChars)
				return;

			char buffer[2];
			buffer[0] = keyCode;
			buffer[1] = 0;
			C_String character(buffer);
			if (C_Input::IsPressed(E_KM_SHIFT))
				character = character.ToUpper();

			m_Text += character;
			m_BitmapText = T_BitmapText(new C_BitmapText(m_TextPos, m_Text, m_Font, E_TT_CZSK));
			m_BitmapText->SetScale(m_Scale);
			
			//@ move carret right
			if (m_Caret)
			{
				C_Vector2 caretPos = m_TextPos - C_Vector2(0.f, m_CaretHeight/2.f);
				caretPos.m_x += m_BitmapText->GetWidth();
				m_Caret->SetPosition(caretPos);
			}
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetColor
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EditBox::SetColor(const C_Vector4 & color)
	{
		T_Super::SetColor(color);
		if (m_BitmapText)
			m_BitmapText->SetColor(color);

		if (m_Caret)
			m_Caret->SetColor(color);
	}




}}