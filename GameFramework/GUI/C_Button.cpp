/*
* filename:			C_Button.cpp
*
* author:			Kral Jozef
* date:				04/01/2009		23:33
* version:			1.00
* brief:				Button widget
*/

#include "C_Button.h"
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_TextureManager.h>
#include <SysCore/Core/C_BitmapFontManager.h>
#include <SysUtils/Utils/C_StringUtils.h>
#include <SysCore/Frames/C_Visual.h>
#include <SysCore/C_MiscUtils.h>
#include <SysAudio/C_AudioManager.h>
#include <SysUtils/Utils/C_FileSystemUtils.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_Button, C_Picture)

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;
	using namespace scorpio::sysaudio;

	const T_Hash32 C_Button::C_SND_MOUSE_ENTER = C_HashName("BTN_MOUSE_ENTER").GetHash();

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_Button::C_Button(const C_HashName & widgetUID, I_WidgetListener * listener)
		: C_Picture(widgetUID, listener), m_fFontScale(1.f)
	{
		//m_disableImg = T_Graphics(new C_Graphics());

		SC_ASSERT(m_listener);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ InitFromXmlNode
	bool C_Button::InitFromXmlNode(const C_XmlNode & node)
	{
		bool bRes = T_Super::InitFromXmlNode(node);
		if (!bRes)
			return false;

		//@ FONT
		const C_XmlNode * tempNode = node.GetNode("font");
		if (tempNode && tempNode->IsValid())
		{
			const C_XmlAttribute * fontUIDAtt = tempNode->GetAttribute("uid");
			if (!fontUIDAtt)
			{
				TRACE_E("Missing or corrupted 'uid' attribute in 'font' node");
				return false;
			}

			C_String fontUID = fontUIDAtt->GetValue();
			m_font = T_BitmapFontManager::GetInstance().GetFont(C_HashName(fontUID));
			if (!m_font)
			{
				TRACE_FE("Can not find fontUID: %s for widgetUID: %s", fontUID.c_str(), m_uniqueID.GetName());
			}
		}


		//@ FONT_SCALE
		tempNode = node.GetNode("font_scale");
		if (tempNode && tempNode->IsValid())
		{
			if (!tempNode->Read(m_fFontScale))
			{
				TRACE_FE("Could not read value from 'font_scale' node in widgetUID: %s", m_uniqueID.GetName());
			}
		}

		//@ TEXT
		tempNode = node.GetNode("text");
		if (tempNode && tempNode->IsValid())
		{
			const C_XmlAttribute * textAtt = tempNode->GetAttribute("text");
			if (!textAtt)
			{
				TRACE_E("Missing or corrupted 'text' attribute in 'text' node");
				return false;
			}

			m_text = textAtt->GetValue();
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromFrame
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Button::InitFromFrame(const T_Frame frame)
	{
		bool bRes = T_Super::InitFromFrame(frame);
		if (!bRes)
			return false;

		T_Visual vis = boost::dynamic_pointer_cast<C_Visual>(frame);
		C_String baseTexName = vis->GetTextureName();
		//@ for focused image simply add "_o.ext" to texture name - by RULE
		C_String ext = C_StringUtils::GetExtension(baseTexName);
		baseTexName = C_StringUtils::TrimExtension(baseTexName);

		//@ FOCUSED IMAGE
		C_String focusTex = baseTexName;
		focusTex += "_o.";
		focusTex += ext;
		//@ need convert focusTex to local path
		m_focusImg = T_TextureManager::GetInstance().GetTexture(focusTex);
		if (!m_focusImg)
			return false;

		//@ DISABLED IMAGE
		C_String disableTex = baseTexName;
		disableTex += "_d.";
		disableTex += ext;

		//@ need convert focusTex to local path
		m_disableImg = T_TextureManager::GetInstance().GetTexture(disableTex);

		SC_ASSERT(m_image->GetWidth() == m_focusImg->GetWidth());	//presume images of states has to be same size large
		SC_ASSERT(m_image->GetHeight() == m_focusImg->GetHeight());	//presume images of states has to be same size large

		if (!m_disableImg)
			return true;

		//SC_ASSERT(m_image->getWidth() == m_disableImg->getWidth());	//presume images of states has to be same size large
		//SC_ASSERT(m_image->getHeight() == m_disableImg->getHeight());	//presume images of states has to be same size large

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Draw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Button::Draw(I_TransformationCallback & callback)
	{
		SC_ASSERT(m_bVisible);

		if (!m_bEnabled)
		{
			if (m_disableImg)
			{
				m_disableImg->Render(m_pivot, m_size, m_color, 0.f, m_fScale);
			}
			else
			{
				m_image->Render(m_pivot, m_size, m_color, 0.f, m_fScale);
			}
			return;
		}

		if (m_bMouseOver)
		{
			if (m_focusImg)
			{
				m_focusImg->Render(m_pivot, m_size, m_color, 0.f, m_fScale);
			}
			else
			{
				m_image->Render(m_pivot, m_size, m_color, 0.f, m_fScale);
			}
			return;
		}

		m_image->Render(m_pivot, m_size, m_color, 0.f, m_fScale);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ EVENTS

	//////////////////////////////////////////////////////////////////////////
	//@ MouseEnter
	void C_Button::MouseEnter()
	{
		m_bMouseOver = true;
		T_Super::MouseEnter();
		T_AudioManager::GetInstance().PlaySoundEx(C_SND_MOUSE_ENTER);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ MouseLeave
	void C_Button::MouseLeave()
	{
		m_bMouseOver = false;
		T_Super::MouseLeave();
	}

	//////////////////////////////////////////////////////////////////////////
	//@ MouseDown
	void C_Button::MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		m_bMouseDown = true;
		T_Super::MouseDown(inPosX, inPosY, eButtonID);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ MouseUp
	void C_Button::MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		m_bMouseDown = false;
		T_Super::MouseUp(inPosX, inPosY, eButtonID);
	}
}}