/*
* filename:			C_DialogManager.h
*
* author:			Kral Jozef
* date:				06/01/2009		20:52
* version:			1.00
* brief:				Load & Store modal dialog-widgets for using in screen(widget managers)
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <SysUtils/C_String.h>
#include <SysUtils/FileSystem/C_Stream.h>
#include <map>
#include "C_Dialog.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_DialogManager
	class GAMEFRAMEWORK_API C_DialogManager
	{
		//@ kontainer dialog kde kluc je typeName - dialogy musia byt rozne podla typeName v def subore!
		typedef std::map<T_Id32, T_Dialog>	T_DialogContainer;

	public:
		//@ CreateInstance
		//@ return false if something fail
		static bool		CreateInstance()
		{
			SC_ASSERT(!m_instance && "DialogManager already created!");

			m_instance = new C_DialogManager();
			if (!m_instance)
				return false;

			return true;
		}

		//@ GetInstance
		static C_DialogManager &	GetInstance()
		{
			SC_ASSERT(m_instance && "DialogManager not created");
			return *m_instance;
		}

		//@ DestroyInstance
		static void		DestroyInstance()
		{
			SC_SAFE_DELETE(m_instance);
		}

		//@ InitFromDefXml
		//@ inicialize dialog definitions info from xml file
		bool				InitFromDefXml(const sysutils::T_Stream inputStream);


		//@ GetDialog
		T_Dialog			GetDialog(sysutils::C_HashName dlgID);
		//@ Clear
		void				Clear();

	private:
		//@ c-tor
		C_DialogManager() {};
		//@ copy c-tor
		C_DialogManager(const C_DialogManager & copy);	//not implemented
		//@ assignment operator
		C_DialogManager & operator=(const C_DialogManager & copy);	//not implemented
		//@ d-tor
		~C_DialogManager() {};

		//@ attributes
		static C_DialogManager		*	m_instance;

		T_DialogContainer					m_container;
	};

}}