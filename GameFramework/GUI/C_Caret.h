/*
* filename:			C_Caret.h
*
* author:			Kral Jozef
* date:				04/26/2011		9:48
* version:			1.00
* brief:
*/

#pragma once

#include <SysCore/Frames/C_Visual.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Caret
	class C_Caret
	{
	public:
		//@ c-tor
		//@ blinkInterval in ms, has to be less than 800ms(0.8 second)
		C_Caret(u16 blinkInterval, syscore::T_Visual caretFrame)
			: m_BlinkInterval(blinkInterval), m_Frame(caretFrame), m_Visible(false), m_AccTime(0) {};

		//@ Update
		void						Update(u32 deltaTime);
		//@ Render
		void						Render();
		//@ SetVisisble
		void						SetVisible(bool bVisible) { m_Visible = bVisible; }
		//@ IsVisible
		bool						IsVisible() const { return m_Visible; }
		//@ SetPosition
		void						SetPosition(const sysmath::C_Vector2 & vctPos) { m_Frame->SetPosition(vctPos); }
		//@ GetPosition
		const sysmath::C_Vector2	&	GetPosition() const { return m_Frame->GetPosition(); }
		//@ SetColor
		void						SetColor(const sysmath::C_Vector4 & color) { m_Frame->SetColor(color); }

	private:
		syscore::T_Visual		m_Frame;
		u16						m_AccTime;
		u16						m_BlinkInterval;
		bool						m_Visible;

	};


	typedef boost::shared_ptr<C_Caret>	T_Caret;

}};