/*
* filename:			I_OptionsDialogListener.h
*
* author:			Kral Jozef
* date:				06/08/2009		21:37
* version:			1.00
* brief:				Interface for dialog listeners
*/

#pragma once

#include <SysUtils/C_HashName.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ class I_OptionsDialogListener
	class I_OptionsDialogListener
	{
	public:
		enum E_OptionsValueChange
		{
			E_OVCH_MUSIC_VOLUME = 1,
			E_OVCH_SOUNDFX_VOLUME,
			E_OVCH_FULLSCREEN,
		};

		//@ OnValueChanged - settings changed
		virtual void		OnValueChanged(E_OptionsValueChange changeType, const sysutils::C_HashName & dialogUID) = 0;
		//@ OnExit
		virtual void		OnExitDialog(E_DialogExitValue exitValue, const sysutils::C_HashName & dialogUID) = 0;
	};

}}