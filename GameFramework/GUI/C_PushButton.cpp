/*
* filename:			C_PushButton.cpp
*
* author:			Kral Jozef
* date:				04/21/2009		22:15
* version:			1.00
* brief:
*/

#include "C_PushButton.h"
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace sysmath;


	//////////////////////////////////////////////////////////////////////////
	void C_PushButton::Draw(I_TransformationCallback & callback)
	{
		T_Super::Draw(callback);

		if (!m_bEnabled)
			return;

		if (!m_bMouseOver && m_bPushed)
		{
			if (m_pushImg)
				__asm nop//m_pushImg->Render(m_pivot, C_Point((s32)m_pushImg->GetWidth(), (s32)m_pushImg->GetHeight()), m_color, 0.f, m_fScale);
			else
				SC_ASSERT(false);//m_image->Render(m_pivot, m_size, m_color, 0.f, m_fScale);
			DbgDraw();
			return;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//@ EVENTS

	//////////////////////////////////////////////////////////////////////////
	//@ MouseEnter
	void C_PushButton::MouseEnter()
	{
		T_Super::MouseEnter();
	}

	//////////////////////////////////////////////////////////////////////////
	//@ MouseLeave
	void C_PushButton::MouseLeave()
	{
		T_Super::MouseLeave();
	}

	//////////////////////////////////////////////////////////////////////////
	//@ MouseMove
	void C_PushButton::MouseMove(s32 inPosX, s32 inPosY)
	{
		T_Super::MouseMove(inPosX, inPosY);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ MouseDown
	void C_PushButton::MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		T_Super::MouseDown(inPosX, inPosY, eButtonID);
		//TODO push!
	}

	//////////////////////////////////////////////////////////////////////////
	//@ MouseUp
	void C_PushButton::MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		T_Super::MouseUp(inPosX, inPosY, eButtonID);
		//push!!
	}
}}