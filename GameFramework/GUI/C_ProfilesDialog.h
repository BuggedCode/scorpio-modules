/*
* filename:			C_ProfilesDialog.h
*
* author:			Kral Jozef
* date:				04/12/2011		22:11
* version:			1.00
* brief:
*/

#pragma once

#include "C_Dialog.h"
#include "I_DialogListeners.h"
#include <GameFramework/Core/C_PlayerProfiles.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ProfilesDialog
	class GAMEFRAMEWORK_API C_ProfilesDialog : public C_Dialog
	{
		typedef C_Dialog	T_Super;
	public:
		//@ c-tor
		C_ProfilesDialog(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener)
			: C_Dialog(widgetUID, listener), m_Profiles(NULL) {};

		//@ InitDialog
		void					InitDialog();
		//@ SetProfiles - called before InitDialog (PushDialog)
		void					SetProfiles(C_PlayerProfiles & profiles/*, C_PlayerProfile & currProfile*/) { m_Profiles = &profiles; }
		//@ AddProfile
		void					AddNewProfile(sysutils::C_String profileName);
		//@ DeleteSelectedProfile
		void					DeleteSelectedProfile();
		//@ GetCurrentProfile
		T_PlayerProfile	GetCurrentProfile() const { return m_Profiles->GetCurrentProfile(); }

		//////////////////////////////////////////////////////////////////////////
		//@ WIDGET LISTENER INTERFACE
		virtual void		OnMouseUp(const sysutils::C_HashName & widgetUID);

		DECLARE_RTTI

	private:
		//@ FillListBox
		void					FillListBox();
		//@ UpdateControls
		void					UpdateControls();

	private:
		static const sysutils::C_HashName	C_BTN_OK_UID;
		static const sysutils::C_HashName	C_BTN_CREATE_UID;
		static const sysutils::C_HashName	C_BTN_DELETE_UID;
		static const sysutils::C_HashName	C_LB_PROFILES_UID;

		C_PlayerProfiles	*	m_Profiles;
	};


	typedef boost::shared_ptr<C_ProfilesDialog>	T_ProfilesDialog;


	//////////////////////////////////////////////////////////////////////////
	//@ C_ProfilesDialogCreator
	class C_ProfilesDialogCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_ProfilesDialog(widgetUID, NULL/*listener*/));
		}
	};

}}