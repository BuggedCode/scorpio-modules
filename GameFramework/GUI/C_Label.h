/*
* filename:			C_Label.h
*
* author:			Kral Jozef
* date:				09/14/2010		20:50
* version:			1.00
* brief:
*/

#pragma once

#include "C_Widget.h"
#include <SysCore/Frames/C_BitmapText.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Label
	class GAMEFRAMEWORK_API C_Label : public C_Widget
	{
		enum E_LabelAlignment
		{
			E_LA_LEFT = 0,
			E_LA_CENTER,
		};

		typedef C_Widget	T_Super;
	public:
		//@ c-tor
		C_Label(const sysutils::C_HashName & widgetUID);
		//@ d-tor
		~C_Label() {};

		//@ InitFromFrame
		virtual bool		InitFromFrame(const syscore::T_Frame frame);
		//@ InitFromXmlNode
		virtual bool		InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ Update
		//@ param inDeltaTime - different time between last and actual called update in ms
		virtual void		Update(u32 inDeltaTime) {};
		//@ Draw
		virtual void		Draw(I_TransformationCallback & callback);
#ifndef MASTER
		//@ DebugDraw
		virtual void		DebugDraw(I_TransformationCallback & callback);
#endif

		//@ SetColor
		virtual void		SetColor(const sysmath::C_Vector4 & color)
		{ 
			m_color = color; 
			if (m_BitmapText) 
				m_BitmapText->SetColor(m_color);
		}

		//@ SetSize
		virtual void		SetSize(u32 inWidth, u32 inHeight) 
		{
			m_fScale = 1.f; 
			m_size.x = inWidth; 
			m_size.y = inHeight; 
			if (m_Alignment == E_LA_CENTER)
			{
				m_rect.m_left = (s32)(m_pivot.m_x - inWidth/2.f);
				m_rect.m_right = (s32)(m_pivot.m_x + inWidth/2.f);
			}
			else 
			{
				m_rect.m_left = (s32)(m_pivot.m_x);
				m_rect.m_right = (s32)(m_pivot.m_x + inWidth);
			}
			m_rect.m_top = (s32)(m_pivot.m_y - inHeight);
			m_rect.m_bottom = (s32)(m_pivot.m_y);
		}

		//@ SetText
		void					SetText(sysutils::C_String strText);
		//@ GetText
		sysutils::C_String	GetText() const { return m_RawText; }


		DECLARE_RTTI

	protected:
		syscore::T_BitmapText	m_BitmapText;
		sysutils::C_String		m_RawText;
		float							m_FontScale;
		syscore::T_BitmapFont	m_Font;
		E_LabelAlignment			m_Alignment;
	};


	typedef boost::shared_ptr<C_Label>		T_Label;


	//////////////////////////////////////////////////////////////////////////
	//@ C_LabelCreator
	class C_LabelCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_Label(widgetUID));
		}
	};

}}
