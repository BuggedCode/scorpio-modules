/*
* filename:			C_AnimPicture.cpp
*
* author:			Kral Jozef
* date:				06/15/2009		21:52
* version:			1.00
* brief:				Animated widget Picture
*/

#include "C_AnimPicture.h"
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_AnimTextureManager.h>
#include <SysCore/Managers/C_VertexBufferManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_AnimPicture, C_Picture)

	using namespace sysmath;
	using namespace sysutils;
	using namespace sysrender;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_AnimPicture::C_AnimPicture(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener)
		: C_Picture(widgetUID, listener)
	{
		//
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Update
	//////////////////////////////////////////////////////////////////////////
	void C_AnimPicture::Update(u32 inDeltaTime)
	{
		m_animImage->Update(inDeltaTime);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ InitFromXmlNode
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimPicture::InitFromXmlNode(const sysutils::C_XmlNode & node)
	{
		bool bRes = C_Widget::InitFromXmlNode(node);
		if (!bRes)
			return false;

		//@ ANIM TEXTURE
		const C_XmlNode * tempNode = node.GetNode("anim_texture");
		if (!tempNode || !tempNode->IsValid())
		{
			TRACE_E("Missing or corrupted 'anim_texture' node");
			return false;
		}

		const C_XmlAttribute * uidAtt = tempNode->GetAttribute("uid");
		if (!uidAtt)
		{
			TRACE_E("Missing or corrupted 'uid' attribute in 'anim_texture' node");
			return false;
		}

		C_String animTexUID = uidAtt->GetValue();
		m_animImage = syscore::T_AnimTextureManager::GetInstance().GetAnimTexture(C_HashName(animTexUID));
		
		if (!m_animImage)
			return false;

		SetSize(m_animImage->GetWidth(), m_animImage->GetHeight());

		m_VertexBuffer = T_VertexBufferManager::GetInstance().GenerateVertexBufferObject();
		m_VertexBuffer->CreateFromAnimTexture(C_Vector2((float)m_animImage->GetWidth(), (float)m_animImage->GetHeight()), C_Point(m_animImage->GetFramesCountX(), m_animImage->GetFramesCountY()), 0.f);
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@'InitFromAnimTexture
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_AnimPicture::InitFromAnimTexture(const C_HashName & animTexUID)
	{
		m_animImage = syscore::T_AnimTextureManager::GetInstance().GetAnimTexture(animTexUID);

		if (!m_animImage)
			return false;

		SetSize(m_animImage->GetWidth(), m_animImage->GetHeight());
		
		m_VertexBuffer = T_VertexBufferManager::GetInstance().GenerateVertexBufferObject();
		m_VertexBuffer->CreateFromAnimTexture(C_Vector2((float)m_animImage->GetWidth(), (float)m_animImage->GetHeight()), C_Point(m_animImage->GetFramesCountX(), m_animImage->GetFramesCountY()), 0.f);
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ Draw
	//////////////////////////////////////////////////////////////////////////
	void C_AnimPicture::Draw(I_TransformationCallback & callback)
	{
		SC_ASSERT(m_bVisible && m_VertexBuffer);
		if (m_animImage)
			m_animImage->Draw(m_VertexBuffer, m_pivot, m_color, m_fScale);
	}

}}