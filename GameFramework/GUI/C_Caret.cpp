/*
* filename:			C_Caret.cpp
*
* author:			Kral Jozef
* date:				04/26/2011		9:52
* version:			1.00
* brief:
*/

#include "C_Caret.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Caret::Update(u32 deltaTime)
	{
		m_AccTime += deltaTime;
		if (!m_Visible)
		{
			//calculate 1000 - m_BlinkInterval
			if (m_AccTime >= (800 - m_BlinkInterval))
			{
				SetVisible(true);
				m_AccTime = 0;
			}
		}
		else
		{
			if (m_AccTime >= m_BlinkInterval)
			{
				SetVisible(false);
				m_AccTime = 0;
			}
		}
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Render
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Caret::Render()
	{
		if (!m_Visible)
			return;

		m_Frame->Render();
	}

}};