/*
* filename:			C_Dialog.cpp
*
* author:			Kral Jozef
* date:				04/06/2009		19:20
* version:			1.00
* brief:				Modal dialog
*/

#include "C_Dialog.h"
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_TextureManager.h>
#include <SysUtils/Xml/C_XmlNode.h>
#include "C_WidgetFactory.h"
#include <SysCore/Core/C_AnimationManager.h>
#include <GameFramework/Controllers/C_AnimController.h>
#include <SysCore/Geometry/C_MeshManager.h>
#include <SysCore/Helpers/C_ScgLoader.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_Dialog, C_Picture)

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_Dialog::C_Dialog(const C_HashName & widgetUID, I_WidgetListener * dlgListener)
		: C_Picture(widgetUID, dlgListener), m_retValue(E_DEV_UNDEF)
	{
		m_strHeader = "Pokus";
		//, C_String strHeader = vytiahnem dialog na zaklade iDcka z dialog manageru
	}

	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	//////////////////////////////////////////////////////////////////////////
	C_Dialog::~C_Dialog()
	{
		//
	}


	//////////////////////////////////////////////////////////////////////////
	//@ InitFromXmlNode
	bool C_Dialog::InitFromXmlNode(const C_XmlNode & node)
	{
		//ancestor cannot read position/image
		if (!node.IsValid())
			return false;

		const C_XmlNode * tmpNode = node.GetNode("Meshes");
		if (!tmpNode || !tmpNode->IsValid())
		{
			TRACE_FE("Dialog: %s has no Valid 'Meshes' node!", m_uniqueID.GetName());
			return false;
		}

		C_String meshFile = (*tmpNode)["file"];
		
		T_Stream stream = T_FileSystemManager::GetInstance().Open(meshFile);

		C_ScgLoader loader;
		bool bRes = loader.Load(stream);

		T_Frame dlgFrm = loader.GetFrame(m_uniqueID.GetHash());
		if (!bRes || !dlgFrm)
			return false;

		T_MeshManager::GetInstance().Remove(m_uniqueID);

		T_Super::InitFromFrame(dlgFrm);

		//@ FadeInAnimationTrackID
		tmpNode = node.GetNode("FadeInAnimationTrackID");
		T_AnimationTrack fadeInTrack;
		if (tmpNode && tmpNode->IsValid())
		{
			C_String fiTrackId;
			if (!tmpNode->Read(fiTrackId))
			{
				TRACE_E("Can not read value of 'FadeInAnimationTrackID' node");
			}
			else
			{
				fadeInTrack = T_AnimationManager::GetInstance().GetAnimation(C_HashName(fiTrackId));
				if (fadeInTrack)
				{
					m_animFadeInOp = T_Operator(new C_WidgetAnimController(fadeInTrack, *this, this));
				}
				else
				{
					//@ try load / parse anim file
					tmpNode = node.GetNode("Anim");
					C_String animFile = (*tmpNode)["file"];
					TryLoadDialogAnimation(animFile);
					fadeInTrack = T_AnimationManager::GetInstance().GetAnimation(C_HashName(fiTrackId));
					if (fadeInTrack)
						m_animFadeInOp = T_Operator(new C_WidgetAnimController(fadeInTrack, *this, this));
					else
						TRACE_FE("Can not find FadeIn animation: %s in Animation Manager", fiTrackId.c_str());
				}
			}
		}
		else
		{
			//coz sometimes exported dialog is from 1st frame, and animation for blending has 100%opacity
			//@ but if we dont want animation, in xml we will not define fadeInAnimOp, so we want NOT transparent color
			if (isZero(m_color.m_w))
				m_color.m_w = 1;
		}
		//@ FadeOutAnimationTrackID
		tmpNode = node.GetNode("FadeOutAnimationTrackID");
		if (tmpNode && tmpNode->IsValid())
		{
			C_String foTrackId;
			if (!tmpNode->Read(foTrackId))
			{
				TRACE_E("Can not read value of 'FadeOutAnimationTrackID' node");
			}
			else
			{
				T_AnimationTrack track = T_AnimationManager::GetInstance().GetAnimation(C_HashName(foTrackId));
				if (track)
				{
					m_animFadeOutOp = T_Operator(new C_WidgetAnimController(track, *this, this));
					m_animFadeOutOp->Deactivate();
				}
				else
				{
					TRACE_FE("Ca not find FadeOut animation: %s in Animation Manager", foTrackId.c_str());
				}
			}
		}
		else
		{
			//@ if not defined FadeOutAnimationTrackID, we try to create reversed track from FadeInAnimationTrackID
			if (m_animFadeInOp && fadeInTrack)
			{
				C_WidgetAnimController * waCtrl = new C_WidgetAnimController(fadeInTrack, *this, this);
				waCtrl->RevertAnim();
				m_animFadeOutOp = T_Operator(waCtrl);
				m_animFadeOutOp->Deactivate();
			}
		}


		//@ naladuj vsechny widgety
		C_XmlNode::const_child_iterator it_begin = node.BeginChild("widget");
		C_XmlNode::const_child_iterator it_end	= node.EndChild("widget");
		for (; it_begin != it_end; ++it_begin)
		{
			const C_XmlNode & widgetNode = *it_begin;
			if (!widgetNode.IsValid())
				continue;

			const C_XmlAttribute * wTypeAtt = widgetNode.GetAttribute("type");
			SC_ASSERT(wTypeAtt);
			if (!wTypeAtt)
			{
				TRACE_E("Missing attribute of widget 'type'");
				continue;
			}

			S_WidgetDescriptor desc(this);	//listener for widgets from dialog is this dialog!
			desc.m_typeName = wTypeAtt->GetValue();

			C_HashName widgetUIDhash;
			wTypeAtt = widgetNode.GetAttribute("uid");
			if (wTypeAtt)
			{
				C_String str = wTypeAtt->GetValue();
				widgetUIDhash = str;
			}
			
			desc.m_hashNameUID = widgetUIDhash;

			T_Widget widget = C_WidgetFactory::GetInstance().CreateWidget(desc);
			if (!widget)
			{
				TRACE_FE("Unknown type of widget: %s", desc.m_typeName.c_str());
				continue;
			}

			//@ Focus
			C_String strFocus = widgetNode["focus"];
			if (!strFocus.empty())
			{
				if (strFocus.ToBool())
					widget->SetFocus(true);
				else
					widget->SetFocus(false);
			}

			//@ check if is mesh in manager for this widget
			//T_Frame frmWidget = loader.GetFrame(desc.m_hashNameUID.GetHash());
			T_Frame frmWidget = dlgFrm->FindFrame(desc.m_hashNameUID.GetHash());
			if (frmWidget)
			{
				bool bRes = widget->InitFromFrame(frmWidget);
				T_MeshManager::GetInstance().Remove(desc.m_hashNameUID);
				if (!bRes)
				{
					TRACE_FE("Failed to init widget: %s form Mesh", desc.m_hashNameUID.GetName());
					continue;
				}
			}
			else
			{
				TRACE_FW("Can not find Reference frame for widget: %s ", desc.m_hashNameUID.GetName());
			}

			//load widget properties
			bool bResInit = widget->InitFromXmlNode(widgetNode);
			if (bResInit)
			{
				//Move widget to worldPos
				//const C_Vector2 & locPos = widget->GetPosition();
				//C_Vector2 dlgPos = GetPosition();
				//dlgPos += locPos;
				//widget->SetPosition(dlgPos);
				bool bResAdd = m_widgetManager.Add(widget);
				if (!bResAdd)
					TRACE_FE("Can not add widget of type: %s from xml", desc.m_typeName.c_str());
			}
			else
			{
				TRACE_FE("Can not initialize widget of type: %s from xml", desc.m_typeName.c_str());
			}
		}

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ TryLoadDialogAnimation
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Dialog::TryLoadDialogAnimation(const C_String animFileName)
	{
		T_Stream stream = T_FileSystemManager::GetInstance().Open(animFileName);
		if (!stream)
			return false;

		bool bRes = T_AnimationManager::GetInstance().LoadAnimation(stream);
		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Update
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::Update(u32 inDeltaTime)
	{
		if (m_animFadeInOp)
			m_animFadeInOp->Update(inDeltaTime);
		if (m_animFadeOutOp)
			m_animFadeOutOp->Update(inDeltaTime);

		if (!m_bEnabled)
			return;

		m_widgetManager.Update(inDeltaTime);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Draw
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::Draw(I_TransformationCallback & callback)
	{
		T_Super::Draw(callback);
		//@ render widgets
		callback.PushAndTransform(m_pivot, 0.f, m_fScale);
		m_widgetManager.Draw(callback);
		callback.PopTransform();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	void C_Dialog::DebugDraw(I_TransformationCallback & callback)
	{
		T_Super::DebugDraw(callback);
		//@ debugDraw widgets
		callback.PushAndTransform(m_pivot, 0.f, m_fScale);
		m_widgetManager.DebugDraw(callback);
		callback.PopTransform();
	}
#endif


	//////////////////////////////////////////////////////////////////////////
	//@ Reset
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::Reset()
	{
		//zresetujem input statusy vsech widgetu v dialogu
		if (m_animFadeInOp)
		{
			m_animFadeInOp->Reset();
			m_animFadeInOp->Activate();
		}
		if (m_animFadeOutOp)
		{
			m_animFadeOutOp->Reset();
			m_animFadeOutOp->Deactivate();
		}

		m_widgetManager.ResetInputStates();
	}

	//////////////////////////////////////////////////////////////////////////
	//@ WIDGET INPUT INTERFACE IMPLEMENTATION
	void C_Dialog::MouseMove(s32 inPosX, s32 inPosY)
	{
		if (!m_bEnabled || !m_bActive)
			return;

		//@ inv translate, rotate & scale
		C_Point mousePos(inPosX, inPosY);
		TransfromToDialog(mousePos);

		m_widgetManager.MouseMove(mousePos.x, mousePos.y);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		if (!m_bEnabled || !m_bActive)
			return;

		//@ inv translate, rotate & scale
		C_Point mousePos(inPosX, inPosY);
		TransfromToDialog(mousePos);

		m_widgetManager.MouseDown(mousePos.x, mousePos.y, eButtonID);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		if (!m_bEnabled || !m_bActive)
			return;

		//@ inv translate, rotate & scale
		C_Point mousePos(inPosX, inPosY);
		TransfromToDialog(mousePos);

		m_widgetManager.MouseUp(mousePos.x, mousePos.y, eButtonID);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ TransfromToDialog
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::TransfromToDialog(sysmath::C_Point & inputPos)
	{
		inputPos.x -= (s32)m_pivot.m_x;
		inputPos.y -= (s32)m_pivot.m_y;

		inputPos *= m_fScale;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ OnKeyDown
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::OnKeyDown(E_KeyboardLayout keyCode)
	{
		m_widgetManager.OnKeyDown(keyCode);
	}
	void C_Dialog::OnKeyUp(E_KeyboardLayout keyCode)
	{
		m_widgetManager.OnKeyUp(keyCode);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ NotifyOperatorFinish
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::NotifyOperatorFinish(I_BaseOperator * callerOp)
	{
		if (callerOp == m_animFadeInOp.get())
		{
			m_bEnabled = true;
			m_animFadeInOp->Deactivate();
		}
		else if (callerOp == m_animFadeOutOp.get())
		{
			m_animFadeOutOp->Deactivate();
			if (m_listener)
				m_listener->OnExitDialog(m_retValue, m_uniqueID);
		}

		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetColor
	//
	//////////////////////////////////////////////////////////////////////////
	class C_WidgetColorizer
	{
	public:
		//@ c-tor
		C_WidgetColorizer(const C_Vector4 & color)
			: m_color(color) {};
		
		//@ op function
		bool operator()(T_Widget widget) const
		{
			widget->SetColor(m_color);
			return true;
		}
	private:
		const C_Vector4	&	m_color;
	};

	void C_Dialog::SetColor(const C_Vector4 & color)
	{
		T_Super::SetColor(color);

		C_WidgetColorizer colorizer(color);
		m_widgetManager.EnumWidgets(colorizer);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetScale
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::SetScale(float fScale)
	{
		class C_WidgetScaler
		{
		public:
			//@ c-tor
			C_WidgetScaler(float fScale)
				: m_fScale(fScale) {};

			//@ op function
			bool operator()(T_Widget widget) const
			{
				widget->SetScale(m_fScale);
				return true;
			}
		private:
			float		m_fScale;
		};

		T_Super::SetScale(fScale);
		
		C_WidgetScaler scaler(fScale);
		//m_widgetManager.EnumWidgets(scaler);
	}



	//////////////////////////////////////////////////////////////////////////
	//@ C_WidgetPositioner
	//////////////////////////////////////////////////////////////////////////
	class C_WidgetPositioner
	{
	public:
		//@ c-tor
		C_WidgetPositioner(const C_Vector2 & offsetPos)
			: m_offsetPos(offsetPos) {};

		//@ op function

		bool operator()(T_Widget widget) const
		{
			const C_Vector2 & oldPos = widget->GetPosition();
			widget->SetPosition(oldPos + m_offsetPos);
			return true;
		}
	private:
		const C_Vector2	&	m_offsetPos;
	};

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Setposition
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::SetPosition(const C_Vector2 & vctPos)
	{
		C_Vector2 offset = vctPos - T_Super::GetPosition();
		T_Super::SetPosition(vctPos);

		C_WidgetPositioner positioner(offset);
		//m_widgetManager.EnumWidgets(positioner);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetEnable
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::SetEnable(bool bEnable)
	{
		class C_WidgetEnabler
		{
		public:
			//@ c-tor
			C_WidgetEnabler(bool bEnable)
				: m_bEnable(bEnable) {};

			//@ op function

			bool operator()(T_Widget widget) const
			{
				widget->SetEnable(m_bEnable);
				return true;
			}
		private:
			bool		m_bEnable;
		};

		C_WidgetEnabler enabler(bEnable);
		m_widgetManager.EnumWidgets(enabler);
		m_bEnabled = bEnable;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetActive
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::SetActive(bool bActive)
	{
		class C_WidgetActivator
		{
		public:
			//@ c-tor
			C_WidgetActivator(bool bActive)
				: m_bActive(bActive) {};

			//@ op function

			bool operator()(T_Widget widget) const
			{
				widget->SetActive(m_bActive);
				return true;
			}
		private:
			bool		m_bActive;
		};

		m_bActive = bActive;
		C_WidgetActivator activator(bActive);
		m_widgetManager.EnumWidgets(activator);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ StartFadeOut
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Dialog::StartFadeOut(E_DialogExitValue exitValue)
	{
		m_retValue = exitValue;

		if (m_animFadeOutOp)
		{
			m_animFadeOutOp->Activate();
			m_bEnabled = false;
		}
		else
		{
			m_bEnabled = true;
			if (m_listener)
				m_listener->OnExitDialog(m_retValue, m_uniqueID);
		}
	}

}}