/*
* filename:			C_AnimPicture.h
*
* author:			Kral Jozef
* date:				06/15/2009		21:49
* version:			1.00
* brief:				Animated widget Picture
*/

#pragma once

#include "C_Picture.h"
#include <SysCore/Core/C_AnimTexture.h>
#include <SysCore/Render/C_VertexBufferObject.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_AnimPicture
	class GAMEFRAMEWORK_API C_AnimPicture : public C_Picture
	{
		typedef C_Picture T_Super;
	public:
		//@ c-tor
		C_AnimPicture(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener);
		//@ d-tor
		~C_AnimPicture() {};

		//@ InitFromXmlNode
		bool					InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ Update
		//@ param inDeltaTime - difference between last called update
		virtual void		Update(u32 inDeltaTime);
		//@ Draw
		virtual void		Draw(I_TransformationCallback & callback);


		//////////////////////////////////////////////////////////////////////////
		bool					InitFromAnimTexture(const sysutils::C_HashName & animTexUID);
		//@ JK TEST

		DECLARE_RTTI

	protected:
		//m_image sa nepouziva!
		syscore::T_AnimImage		m_animImage;
		sysrender::T_VertexBufferObject	m_VertexBuffer;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_PictureCreator
	class C_AnimPictureCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_AnimPicture(widgetUID, listener));
		}
	};


}}