/*
* filename:			C_FloatingButton.cpp
*
* author:			Kral Jozef
* date:				06/13/2009		14:26
* version:			1.00
* brief:				Floating button widget
*/

#include "C_FloatingButton.h"
#include <SysMemManager/globalNewDelete.h>
#include <GameFramework/Operators/BaseOperators.h>

namespace scorpio{ namespace framework{

	using namespace sysmath;

	static const float C_SCALE_OFFSET = 0.12f;
	static const float C_SCALE_OP_STEP = 0.0016f;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_FloatingButton::C_FloatingButton(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener)
		: C_Button(widgetUID, listener)
	{
		//////////////////////////////////////////////////////////////////////////
	}


	//////////////////////////////////////////////////////////////////////////
	//@ InitFromXmlNode
	//////////////////////////////////////////////////////////////////////////
	bool C_FloatingButton::InitFromXmlNode(const sysutils::C_XmlNode & node)
	{
		bool bRes = T_Super::InitFromXmlNode(node);
		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Update
	//////////////////////////////////////////////////////////////////////////
	void C_FloatingButton::Update(u32 inDeltaTime)
	{
		T_Super::Update(inDeltaTime);
		if (m_extendOp)
			m_extendOp->Update(inDeltaTime);
		if (m_shrinkOp)
			m_shrinkOp->Update(inDeltaTime);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ ResetInputStates
	//////////////////////////////////////////////////////////////////////////
	void C_FloatingButton::ResetInputStates()
	{
		if (m_extendOp)
		{
			m_extendOp->Deactivate();
			m_extendOp->Reset();
		}
		if (m_shrinkOp)
		{
			m_shrinkOp->Deactivate();
			m_shrinkOp->Reset();
		}
		T_Super::ResetInputStates();

		m_fScale = 1.f;
	}



	//////////////////////////////////////////////////////////////////////////
	//@ MOUSE INPUT

	//////////////////////////////////////////////////////////////////////////
	//@ MouseEnter
	//////////////////////////////////////////////////////////////////////////
	void C_FloatingButton::MouseEnter()
	{
		if (!m_bEnabled)
			return;

		SC_ASSERT(m_bActive);

		T_Super::MouseEnter();
		
		if (m_shrinkOp && m_shrinkOp->IsActive())
		{
			m_shrinkOp->Deactivate();
		}
		m_extendOp = T_Operator(new C_AdditiveOperator<float>(m_fScale, this, 1.f + C_SCALE_OFFSET, C_SCALE_OP_STEP));
	}


	//////////////////////////////////////////////////////////////////////////
	//@ MouseLeave
	//////////////////////////////////////////////////////////////////////////
	void C_FloatingButton::MouseLeave()
	{
		if (!m_bEnabled)
			return;

		SC_ASSERT(m_bActive);

		T_Super::MouseLeave();
		
		if (m_extendOp && m_extendOp->IsActive())
		{
			m_extendOp->Deactivate();
		}
		m_shrinkOp = T_Operator(new C_AdditiveOperator<float>(m_fScale, this, 1.f, -C_SCALE_OP_STEP));
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ MouseDown
	//
	//////////////////////////////////////////////////////////////////////////
	/*void C_FloatingButton::SetActive(bool bActive)
	{
		T_Super::SetActive(bActive);
		if (bActive)
		{
			//
		}
	}*/


	//////////////////////////////////////////////////////////////////////////
	//@ NotifyOperatorFinish
	//////////////////////////////////////////////////////////////////////////
	void C_FloatingButton::NotifyOperatorFinish(I_BaseOperator * callerOp)
	{
		T_Super::NotifyOperatorFinish(callerOp);

		//////////////////////////////////////////////////////////////////////////
		/*if (callerOp == m_extendOp.get())
		{
			__asm nop;	//aktivuj partikli
		}
		else if (callerOp == m_shrinkOp.get())
		{
			__asm nop;	//deaktivuj particli
		}*/
		
		callerOp->Reset();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetPosition
	//
	//////////////////////////////////////////////////////////////////////////
	void C_FloatingButton::SetPosition(const sysmath::C_Vector2 & vctPos)
	{
		T_Super::SetPosition(vctPos);
	}

}}