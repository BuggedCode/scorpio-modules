/*
* filename:			C_DialogManager.cpp
*
* author:			Kral Jozef
* date:				06/01/2009		20:57
* version:			1.00
* brief:				Load & Store modal dialog-widgets for using in screen(widget managers)
*/

#include "C_DialogManager.h"
#include <SysUtils/Xml/C_XmlFile.h>
#include <SysUtils/C_TraceClient.h>
#include "C_WidgetFactory.h"

#include "C_OptionsDialog.h"
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	C_DialogManager * C_DialogManager::m_instance = NULL;

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//@ InitFromDefXml
	//////////////////////////////////////////////////////////////////////////
	bool C_DialogManager::InitFromDefXml(const T_Stream inputStream)
	{
		C_XmlFile file;
		bool bRes = file.Load(inputStream);
		if (!bRes)
			return false;

		//parse xml
		if (!file.IsValid())
			return false;

		const C_XmlNode * root = file.GetRoot();
		if (!root || !root->IsValid())
			return false;

		if (root->GetName() != "dialog_definition")
			return false;

		C_XmlNode::const_child_iterator it_begin = root->BeginChild("dialog");
		C_XmlNode::const_child_iterator it_end	= root->EndChild("dialog");

		//@ spracuju vsechny dialog infa v souboru
		for (; it_begin != it_end; ++it_begin)
		{
			//////////////////////////////////////////////////////////////////////////
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			const C_XmlAttribute * dlgTypeAtt = tempNode.GetAttribute("type");
			if (!dlgTypeAtt)
				continue;

			const C_String tmpDlgTypeName = dlgTypeAtt->GetValue();

			S_WidgetDescriptor desc(NULL);
			desc.m_typeName = tmpDlgTypeName;

			C_HashName widgetUIDhash;
			const C_XmlAttribute * dlgUIDAtt = tempNode.GetAttribute("uid");
			if (dlgUIDAtt)
			{
				C_String str = dlgUIDAtt->GetValue();
				widgetUIDhash = str;
			}

			desc.m_hashNameUID = widgetUIDhash;

			T_Widget dlgWidget = C_WidgetFactory::GetInstance().CreateWidget(desc);
			if (!dlgWidget)
			{
				TRACE_FE("Unknown type of dialog: %s", desc.m_typeName.c_str());
				continue;
			}

			//load widget properties
			bool bResInit = dlgWidget->InitFromXmlNode(tempNode);
			if (bResInit)
			{
				typedef T_DialogContainer::iterator T_DlgIterator;
				T_Dialog dlgRef = boost::dynamic_pointer_cast<C_Dialog>(dlgWidget);
				std::pair<T_DlgIterator, bool> result = m_container.insert(T_DialogContainer::value_type(dlgWidget->GetUID(), dlgRef));
				if (!result.second)
				{
					TRACE_FE("Can not add dialog of type: %s from xml to container, probably same dialog is in already!", desc.m_typeName.c_str());
				}
			}
			else
			{
				TRACE_FE("Can not initialize dialog of type: %s from xml", desc.m_typeName.c_str());
			}
		}
		return bRes;
	}



	//////////////////////////////////////////////////////////////////////////
	//@ GetDialog
	//////////////////////////////////////////////////////////////////////////
	T_Dialog C_DialogManager::GetDialog(C_HashName dlgID)
	{
		T_DialogContainer::iterator iter = m_container.find(dlgID);
		if (iter == m_container.end())
			return T_Dialog();

		return iter->second;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_DialogManager::Clear()
	{
		m_container.clear();
	}

}}