/*
* filename:			C_CreateNewProfileDialog.cpp
*
* author:			Kral Jozef
* date:				04/20/2011		10:27
* version:			1.00
* brief:
*/

#include "C_CreateNewProfileDialog.h"
#include <SysUtils/C_TraceClient.h>
#include <GameFramework/GUI/C_EditBox.h>

namespace scorpio{ namespace framework{

	DEFINE_RTTI(C_CreateNewProfileDialog, C_Dialog)

	using namespace scorpio::sysutils;

	const C_HashName	C_CreateNewProfileDialog::C_BTN_CREATE_UID	= C_HashName("IDCBTN_CreateNewProfile");
	const C_HashName	C_CreateNewProfileDialog::C_BTN_CANCEL_UID	= C_HashName("IDCBTN_CancelNewProfile");
	const C_HashName	C_CreateNewProfileDialog::C_EDB_NEWPROFILE_UID	= C_HashName("IDCEB_NewProfile");


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitDialog
	//
	//////////////////////////////////////////////////////////////////////////
	void C_CreateNewProfileDialog::InitDialog()
	{
		T_Super::InitDialog();
		T_Widget createBtn = m_widgetManager.GetWidget(C_BTN_CREATE_UID);
		createBtn->SetEnable(false);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetProfileName
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_CreateNewProfileDialog::GetProfileName() const
	{
		T_Widget widget = m_widgetManager.GetWidget(C_EDB_NEWPROFILE_UID);
		if (!widget)
		{
			TRACE_FE("Can not find widget name: %s on dialog: %s", C_EDB_NEWPROFILE_UID.GetName(), m_uniqueID.GetName());
			return C_String();
		}

		T_EditBox edBox = boost::dynamic_pointer_cast<C_EditBox>(widget);
		return edBox->GetText();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ OnKeyDown
	//
	//////////////////////////////////////////////////////////////////////////
	void C_CreateNewProfileDialog::OnKeyDown(E_KeyboardLayout keyCode)
	{
		T_Super::OnKeyDown(keyCode);
		//check if edit box is empty
		T_Widget widget = m_widgetManager.GetWidget(C_EDB_NEWPROFILE_UID);
		T_EditBox edBox = boost::dynamic_pointer_cast<C_EditBox>(widget);
		C_String strProfileName = edBox->GetText();
		T_Widget createBtn = m_widgetManager.GetWidget(C_BTN_CREATE_UID);
		if (strProfileName.empty())
			createBtn->ResetInputStates();
		createBtn->SetEnable(!strProfileName.empty());

		if (createBtn->IsEnable() && keyCode == SDLK_RETURN)
		{
			StartFadeOut(E_DEV_OK);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	// WIDGET LISTENER IMPLEMENTATION
	//////////////////////////////////////////////////////////////////////////
	void C_CreateNewProfileDialog::OnMouseUp(const sysutils::C_HashName & widgetUID)
	{
		T_Widget widget = m_widgetManager.GetWidget(widgetUID);
		widget->ResetInputStates();

		if (!m_listener)
			return;

		if (widgetUID == C_HashName(C_BTN_CREATE_UID))
		{
			m_retValue = E_DEV_OK;
		}
		else if (widgetUID == C_HashName(C_BTN_CANCEL_UID))
		{
			m_retValue = E_DEV_CANCEL;
		}
		else
		{
			return;
			//SC_ASSERT(false);	//here can not come
			//CAN:) from other widgets like editbox....
		}

		m_animFadeOutOp->Activate();
		m_bEnabled = false;

		//@ for notification into higher layer
		m_listener->OnMouseUp(widgetUID);
	}



}}

