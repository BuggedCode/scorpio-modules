/*
* filename:			C_ListBox.h
*
* author:			Kral Jozef
* date:				04/10/2011		2:00
* version:			1.00
* brief:				Simple ListBox holds contaner of pairs C_String/C_String
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include "C_Picture.h"
#include <SysCore/Core/C_BitmapFont.h>
#include <SysCore/Frames/C_BitmapText.h>
#include <SysCore/Frames/C_Visual.h>
#include <SysUtils/C_GUID.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C _ListBoxItem
	class GAMEFRAMEWORK_API C_ListBoxItem
	{
	public:
		sysutils::C_String	m_Name;
		sysutils::C_String	m_Value;
		sysutils::C_GUID		m_Guid;
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_ListBox
	class GAMEFRAMEWORK_API C_ListBox : public C_Picture
	{
		typedef C_Picture	T_Super;
		struct S_ItemInfo
		{
			u32	m_Height;
			u32	m_Width;
			//
		};

		typedef std::pair<syscore::T_BitmapText, syscore::T_BitmapText>	T_TextItem;

	public:
		//@ c-tor
		C_ListBox(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener)
			: C_Picture(widgetUID, listener), m_SelectedId(-1) {};
		//@ d-tor
		~C_ListBox() {};


		//@ InitFromXmlNode
		virtual bool		InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ InitFromFrame
		virtual bool		InitFromFrame(const syscore::T_Frame frame);
		//@ Draw
		virtual void		Draw(I_TransformationCallback & callback);
#ifndef MASTER
		//@ DebugDraw
		virtual void		DebugDraw(I_TransformationCallback & callback);
#endif
		//@ SetColor
		virtual void		SetColor(const sysmath::C_Vector4 & color);

		//@ INPUT
		//@ MouseDown
		virtual void		MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);

		//@ ClearAll
		void					ClearAll();
		//@ AddItem
		void					AddItem(const C_ListBoxItem & item);
		//@ SelectItem
		void					SelectItem(u16 index);
		//@ GetSelectedItem
		C_ListBoxItem		GetSelectedItem() const
		{
			if (m_SelectedId == -1 || m_SelectedId >= (s32)m_Container.size())
				return C_ListBoxItem();

			return m_Container[m_SelectedId];
		}
		//@ GetSelectedId
		s16						GetSelectedId() const { return m_SelectedId; }

		//@ IsEmpty
		bool					IsEmpty() const { return m_TextContainer.empty(); }
		//@ IsFull
		bool					IsFull() const { return m_TextContainer.size() == (size_t)C_MAX_ITEM_COUNT; }

	private:
		static const u8			C_MAX_ITEM_COUNT = 5;
		static const T_Hash32	C_SND_LISTBOXITEM_CLICK;	//sound fx when mouse come enter onto button

	private:
		typedef std::vector<C_ListBoxItem>	T_ItemContainer;
		T_ItemContainer			m_Container;
		typedef std::vector<T_TextItem>	T_TextItemContainer;
		T_TextItemContainer		m_TextContainer;

		S_ItemInfo					m_ItemInfo;
		sysmath::C_Vector2		m_TopItemPos;
		syscore::T_BitmapFont	m_Font;	//TODO optimize store it into singleton WidgetInfoStorage
		float							m_Scale;	//font scale
		syscore::T_Visual			m_Selector;	//selectionMarker
		s16							m_SelectedId;	//index into vector -1 => no selection
	};


	typedef boost::shared_ptr<C_ListBox>		T_ListBox;


	//////////////////////////////////////////////////////////////////////////
	//@ C_ListBoxCreator
	class C_ListBoxCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_ListBox(widgetUID, listener));
		}
	};
}}
