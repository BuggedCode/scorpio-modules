/*
* filename:			C_WidgetManager.cpp
*
* author:			Kral Jozef
* date:				04/01/2009		22:13
* version:			1.00
* brief:				Widget manager
*/

#include "C_WidgetManager.h"
#include <Common/Assert.h>
#include <GameFramework/Core/I_Screen.h>
#include <SysMath/C_Point.h>
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	// Update
	//////////////////////////////////////////////////////////////////////////
	void C_WidgetManager::Update(u32 inDeltaTime)
	{
		//@ if modal dialog is on stack update only dialog on top of stack, don't update else widgets & dialogs
		if (!m_modalDlgStack.empty())
		{
			m_modalDlgStack.back()->Update(inDeltaTime);
			return;
		}

		T_Super::Update(inDeltaTime);
	}

	//////////////////////////////////////////////////////////////////////////
	//@ Draw
	//////////////////////////////////////////////////////////////////////////
	void C_WidgetManager::Draw(I_TransformationCallback & callback) const
	{
		//@ Render widgets
		for (T_WidgetContainer::const_iterator iter = m_widgets.begin(); iter != m_widgets.end(); ++iter)
		{
			T_Widget widget = iter->second;
			SC_ASSERT(widget);
			if (!widget->IsVisible())
				continue;
			widget->Draw(callback);//@ JK
		}


		//@ Render modal dialogs from stack
		if (!m_modalDlgStack.empty())
		{
			for (T_DlgStack::const_iterator iter = m_modalDlgStack.begin(); iter != m_modalDlgStack.end(); ++iter)
				(*iter)->Draw(callback);	//dialogy neplavaju so screenom - plavanie screenov je len game zalezitost!
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	void C_WidgetManager::DebugDraw(I_TransformationCallback & callback) const
	{
		for (T_WidgetContainer::const_iterator iter = m_widgets.begin(); iter != m_widgets.end(); ++iter)
		{
			T_Widget widget = iter->second;
			SC_ASSERT(widget);
			if (!widget->IsVisible())
				continue;
			widget->DebugDraw(callback);//@ JK
		}


		//@ Render modal dialogs from stack
		if (!m_modalDlgStack.empty())
		{
			for (T_DlgStack::const_iterator iter = m_modalDlgStack.begin(); iter != m_modalDlgStack.end(); ++iter)
				(*iter)->DebugDraw(callback);	//dialogy neplavaju so screenom - plavanie screenov je len game zalezitost!
		}
	}
#endif




	//////////////////////////////////////////////////////////////////////////
	//
	// EVENTS
	//
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	//@ MouseMove
	//////////////////////////////////////////////////////////////////////////
	void C_WidgetManager::MouseMove(s32 x, s32 y)
	{
		if (!m_modalDlgStack.empty())
		{
			T_Dialog topDialog = m_modalDlgStack.back();
			if (topDialog->IsEnable())
				topDialog->MouseMove(x, y);
			return;
		}

		T_Super::MouseMove(x, y);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ MouseDown
	//////////////////////////////////////////////////////////////////////////
	void C_WidgetManager::MouseDown(s32 x, s32 y, u32 buttonIndex)
	{
		//@ pokud stack modalnych dialogov nie je prazdny
		if (!m_modalDlgStack.empty())
		{
			T_Dialog topDialog = m_modalDlgStack.back();
			if (topDialog->IsEnable())
				topDialog->MouseDown(x, y, (C_Widget::E_MouseButton)buttonIndex);
			return;
		}

		T_Super::MouseDown(x, y, buttonIndex);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ MouseUp
	//////////////////////////////////////////////////////////////////////////
	void C_WidgetManager::MouseUp(s32 x, s32 y, u32 buttonIndex)
	{
		//@ pokud stack modalnych dialogov nie je prazdny
		if (!m_modalDlgStack.empty())
		{
			T_Dialog topDialog = m_modalDlgStack.back();
			if (topDialog->IsEnable())
				topDialog->MouseUp(x, y, (C_Widget::E_MouseButton)buttonIndex);
			return;
		}

		T_Super::MouseUp(x, y, buttonIndex);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ OnKeyDown
	//////////////////////////////////////////////////////////////////////////
	void C_WidgetManager::OnKeyDown(E_KeyboardLayout keyCode)
	{
		//@ pokud stack modalnych dialogov nie je prazdny
		if (!m_modalDlgStack.empty())
		{
			m_modalDlgStack.back()->OnKeyDown(keyCode);
			return;
		}

		T_Super::OnKeyDown(keyCode);
	}


	//////////////////////////////////////////////////////////////////////////
	//@ OnKeyUp
	//////////////////////////////////////////////////////////////////////////
	void C_WidgetManager::OnKeyUp(E_KeyboardLayout keyCode)
	{
		//@ pokud stack modalnych dialogov nie je prazdny
		if (!m_modalDlgStack.empty())
		{
			m_modalDlgStack.back()->OnKeyUp(keyCode);
			return;
		}

		T_Super::OnKeyUp(keyCode);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetDialog
	//
	//////////////////////////////////////////////////////////////////////////
	const T_Dialog C_WidgetManager::GetDialog(T_Id32 dialogID) const
	{
		for (T_DlgStack::const_iterator iter = m_modalDlgStack.begin(); iter != m_modalDlgStack.end(); ++iter)
		{
			if ((*iter)->GetUID().GetHash() == dialogID)
				return *iter;
		}

		TRACE_FW("Can not find dialog: %d", dialogID);
		return T_Dialog();
	}





}}