/*
* filename:			C_Picture.h
*
* author:			Kral Jozef
* date:				05/27/2009		21:17
* version:			1.00
* brief:				picture widget
*/

#pragma once

#include "C_Widget.h"
#include <SysCore/Render/C_Graphic.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Picture
	class GAMEFRAMEWORK_API C_Picture : public C_Widget
	{
		typedef C_Widget	T_Super;
	public:
		//@ c-tor
		C_Picture(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener);
		//@ d-tor
		~C_Picture() {};

		//@ InitFromXmlNode
		virtual bool		InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ InitFromFrame
		virtual bool		InitFromFrame(const syscore::T_Frame frame);
		//@ Draw
		virtual void		Draw(I_TransformationCallback & callback);
#ifndef MASTER
		//@ DebugDraw
		virtual void		DebugDraw(I_TransformationCallback & callback);
#endif
		//@ SteTexture
		void					SetTexture(T_Graphics image) { m_image = image; }

		DECLARE_RTTI

	protected:
		T_Graphics			m_image;	//image of picture
		sysrender::T_VertexBufferObject	m_VertexBuffer;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_PictureCreator
	class C_PictureCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_Picture(widgetUID, listener));
		}
	};

}}