/*
* filename:			C_BaseWidgetManager.h
*
* author:			Kral Jozef
* date:				06/06/2009		14:02
* version:			1.00
* brief:				Base widget manager for dialogs, not intended for export
*/

#pragma once

#include <Common/I_NonCopyAble.h>
#include <GameFramework/Core/I_TransformationCallback.h>
#include <map>
#include "C_Widget.h"
#include <SysCore/Framework/FrameworkTypes.h>
#include <GameFramework/GameFrameworkApi.h>

namespace scorpio{ namespace framework{
	
	typedef std::map<T_Id32, T_Widget>	T_WidgetContainer;

	//////////////////////////////////////////////////////////////////////////
	//@ C_BaseWidgetmanager
	class C_BaseWidgetManager : public I_NonCopyAble
	{
	public:
		//@ c-tor
		C_BaseWidgetManager() {};
		//@ d-tor
		~C_BaseWidgetManager() {};

		
		//@ Add
		bool GAMEFRAMEWORK_API	Add(T_Widget widget);
		//@ Update
		//@ param inDeltaTime - difference between last called update
		void					Update(u32 inDeltaTime);
		//@ Draw - all enabled windows
		//@ param - callback - which we use for local wnd transformations
		void					Draw(I_TransformationCallback & callback) const;
#ifndef MASTER
		//@ DebugDraw
		//@ param - callback - which we use for local wnd transformations
		void					DebugDraw(I_TransformationCallback & callback) const;
#endif
		//@ Reset - reset input states of all widgets to default state
		void GAMEFRAMEWORK_API	ResetInputStates();

		//@ GetWidget
		const T_Widget	GAMEFRAMEWORK_API	GetWidget(T_Id32 widgetID) const;

		//@ EnumWidgets
		template<class Func>
		void					EnumWidgets(const Func & func)
		{
			for (T_WidgetContainer::iterator iter = m_widgets.begin(); iter != m_widgets.end(); ++iter)
			{
				func(iter->second);
			}
		}


		//////////////////////////////////////////////////////////////////////////
		//@ EVENTS
		virtual void		MouseMove(s32 x, s32 y);
		virtual void		MouseDown(s32 x, s32 y, u32 buttonIndex);
		virtual void		MouseUp(s32 x, s32 y, u32 buttonIndex);
		virtual void		OnKeyDown(E_KeyboardLayout keyCode);
		virtual void		OnKeyUp(E_KeyboardLayout keyCode);


	private:
		//@ GetWidgetAt position
		T_Widget				GetWidgetAt(s32 posX, s32 posY) const;


	protected:
		T_WidgetContainer			m_widgets;
		T_Widget						m_lastWidget;		//last widget with focus(muse cursor was above)
		T_Widget						m_FocusedWidget;	//Widget with focus
		T_Widget						m_CapturedWidget;	//currently captured widget
	};

}}