/*
* filename:			C_CheckBox.h
*
* author:			Kral Jozef
* date:				06/07/2009		20:11
* version:			1.00
* brief:				CheckBox widget
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include "C_Button.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_CheckBox
	class GAMEFRAMEWORK_API C_CheckBox : public C_Button
	{
		typedef C_Button	T_Super;
	public:
		//@ c-tor
		C_CheckBox(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener);
		//@ d-tor
		~C_CheckBox() {};

		//@ InitFromXmlNode
		virtual bool		InitFromXmlNode(const sysutils::C_XmlNode & node);
		//@ Draw
		virtual void		Draw(I_TransformationCallback & callback);


		//////////////////////////////////////////////////////////////////////////
		// SET/GET Methods
		//@ AssignValue - priradi checkboxu premennu nad ktorou bude operovat
		void					AssignValue(bool & bVariable) { m_bChecked = &bVariable; }
		//@ SetChecked
		void					SetChecked(bool bCheckStatus) 
		{
			if (!m_bChecked)
			{
				SC_ASSERT(false);
				return;
			}
			*m_bChecked = bCheckStatus;
		}

		//@ GetChecked
		bool					GetChecked() const 
		{ 
			if (!m_bChecked)
			{
				SC_ASSERT(false);
				return false;
			}
			return *m_bChecked;
		}


		//////////////////////////////////////////////////////////////////////////
		//@ MOUSE EVENTS

		//@ MouseUp
		void					MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID);



		DECLARE_RTTI

	private:
		bool				*	m_bChecked;				//variable bound with widget

		T_Graphics			m_checkedImg;			//image when checkbox is checked without focus
		T_Graphics			m_checkedFocusImg;	//image when checkbox is checked with focus

	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_ButtonCreator
	class C_CheckboxCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_CheckBox(widgetUID, listener));
		}
	};
}}