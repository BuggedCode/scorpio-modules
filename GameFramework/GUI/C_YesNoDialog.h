/*
* filename:			C_YesNoDialog.h
*
* author:			Kral Jozef
* date:				06/09/2009		21:37
* version:			1.00
* brief:				YesNo dialog
*/

#pragma once

#include "C_Dialog.h"
#include "I_DialogListeners.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_YesNoDialog
	class GAMEFRAMEWORK_API C_YesNoDialog : public C_Dialog
	{
		typedef C_Dialog	T_Super;
	public:
		//@ c-tor
		C_YesNoDialog(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener);

		//////////////////////////////////////////////////////////////////////////
		//@ WIDGET LISTENER INTERFACE
		virtual void		OnMouseUp(const sysutils::C_HashName & widgetUID);


		DECLARE_RTTI

	private:
		

	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_OptionsDialogCreator
	class C_YesNoDialogCreator : public I_WidgetCreator
	{
	public:
		//@ CreateWidget
		virtual T_Widget		CreateWidget(const sysutils::C_HashName & widgetUID, I_WidgetListener * listener) const
		{
			return T_Widget(new C_YesNoDialog(widgetUID, listener));
		}
	};

}}