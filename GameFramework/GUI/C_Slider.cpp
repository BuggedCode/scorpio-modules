/*
* filename:			C_Slider.cpp
*
* author:			Kral Jozef
* date:				03/09/2011		11:22
* version:			1.00
* brief:
*/

#include "C_Slider.h"
#include <SysUtils/C_TraceClient.h>
#include <SysMath/MathUtils.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;
	using namespace scorpio::syscore;
	using namespace scorpio::sysmath;

	DEFINE_RTTI(C_Slider, C_Picture)

	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromXmlNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Slider::InitFromXmlNode(const C_XmlNode & node)
	{
		//@ nothing
		return C_Picture::InitFromXmlNode(node);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromframe
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Slider::InitFromFrame(const T_Frame frame)
	{
		if (!C_Picture::InitFromFrame(frame))
			return false;

		//@ init driver
		T_Frame drvFrm = frame->FindFrame("IDCDRIVER");
		if (!drvFrm)
		{
			TRACE_FE("Can not find Driver Frame for Slider: %s", m_uniqueID.GetName());
			return false;
		}
		m_Driver = T_Widget(new C_Picture(C_HashName("IDCDRIVER"), NULL));
		bool bRes = m_Driver->InitFromFrame(drvFrm);
		//@ transform Dirver into Dialog coord from parent Slider
		m_Driver->SetPosition(m_Driver->GetPosition() + m_pivot);
		
		//@ init limits
		T_Frame minPosFrm = frame->FindFrame("IDCDRIVER_MINPOS");
		T_Frame maxPosFrm = frame->FindFrame("IDCDRIVER_MAXPOS");
		if (!minPosFrm || !maxPosFrm)
		{
			TRACE_FE("Can not find Limits Pos Frames for Slider-Driver: %s", m_uniqueID.GetName())
			return false;
		}
		
		m_MinPos = minPosFrm->GetLocalPosition() + m_pivot;
		m_MaxPos = maxPosFrm->GetLocalPosition() + m_pivot;
		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Draw
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Slider::Draw(I_TransformationCallback & callback)
	{
		T_Super::Draw(callback);

		m_Driver->Draw(callback);
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ MouseDown
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Slider::MouseDown(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		if (eButtonID != E_MB_LEFT)
			return;

		float newXpos = ClampValue((float)inPosX, m_MinPos.m_x, m_MaxPos.m_x);
		m_Driver->SetPosition(C_Vector2(newXpos, m_Driver->GetPosition().m_y));

		if (m_listener)
		{
			float fRelVal = RemapValue(newXpos, m_MinPos.m_x, m_MaxPos.m_x, 0.f, 1.f);
			m_listener->OnDriverPosChanged(m_uniqueID, fRelVal);
		}

		//if (m_Driver->Contains(inPosX, inPosY))
		{
			m_Dragging = true;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ MouseUp
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Slider::MouseUp(s32 inPosX, s32 inPosY, E_MouseButton eButtonID)
	{
		if (eButtonID != E_MB_LEFT)
			return;

		m_Dragging = false;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ MouseMove
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Slider::MouseMove(s32 inPosX, s32 inPosY)
	{
		if (!m_Dragging)
			return;

		float newXpos = ClampValue((float)inPosX, m_MinPos.m_x, m_MaxPos.m_x);
		m_Driver->SetPosition(C_Vector2(newXpos, m_Driver->GetPosition().m_y));

		if (m_listener)
		{
			float fRelVal = RemapValue(newXpos, m_MinPos.m_x, m_MaxPos.m_x, 0.f, 1.f);
			m_listener->OnDriverPosChanged(m_uniqueID, fRelVal);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ MouseLeave
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Slider::MouseLeave()
	{
		m_Dragging = false;
		T_Super::MouseLeave();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetDriverPosition
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Slider::SetDriverPosition(float fPos)
	{
		SC_ASSERT(m_Driver);
		float newXpos = RemapValue(fPos, 0.f, 1.f, m_MinPos.m_x, m_MaxPos.m_x);
		m_Driver->SetPosition(C_Vector2(newXpos, m_Driver->GetPosition().m_y));
	}

}}