/*
* filename:			C_WidgetFactory.cpp
*
* author:			Kral Jozef
* date:				05/23/2009		17:01
* version:			1.00
* brief:				singleton widget factory
*/

#include "C_WidgetFactory.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/C_FNVHash.h>
#include "C_Label.h"
#include "C_Button.h"
#include "C_FloatingButton.h"
#include "C_Picture.h"
#include "C_AnimPicture.h"
#include "C_CheckBox.h"
#include "C_Slider.h"
#include "C_ListBox.h"
#include "C_EditBox.h"
#include "C_OptionsDialog.h"
#include "C_YesNoDialog.h"
#include "C_ProfilesDialog.h"
#include "C_CreateNewProfileDialog.h"
#include <boost/lexical_cast.hpp>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	C_WidgetFactory * C_WidgetFactory::m_instance = NULL;

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	C_WidgetFactory::C_WidgetFactory()
		: m_freeID(0)
	{
		//base widget registration
		RegisterCreator("LABEL", T_WidgetCreator(new C_LabelCreator()));
		RegisterCreator("BUTTON", T_WidgetCreator(new C_ButtonCreator()));
		//RegisterCreator("PUSH_BUTTON", T_WidgetCreator(new C_PushButtonCreator()));
		RegisterCreator("FLOATING_BUTTON", T_WidgetCreator(new C_FloatingButtonCreator()));
		
		RegisterCreator("PICTURE", T_WidgetCreator(new C_PictureCreator()));
		RegisterCreator("ANIM_PICTURE", T_WidgetCreator(new C_AnimPictureCreator()));
		
		RegisterCreator("CHECKBOX", T_WidgetCreator(new C_CheckboxCreator()));
		RegisterCreator("SLIDER", T_WidgetCreator(new C_SliderCreator()));
		RegisterCreator("LIST_BOX", T_WidgetCreator(new C_ListBoxCreator()));
		RegisterCreator("EDIT_BOX", T_WidgetCreator(new C_EditBoxCreator()));

		RegisterCreator("DIALOG", T_WidgetCreator(new C_DialogCreator()));
		RegisterCreator("OPTIONS_DIALOG", T_WidgetCreator(new C_OptionsDialogCreator()));
		RegisterCreator("YESNO_DIALOG", T_WidgetCreator(new C_YesNoDialogCreator()));
		RegisterCreator("PROFILES_DIALOG", T_WidgetCreator(new C_ProfilesDialogCreator()));
		RegisterCreator("CREATE_NEW_PROFILE_DIALOG", T_WidgetCreator(new C_CreateNewProfileDialogCreator()));
	}


	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	//////////////////////////////////////////////////////////////////////////
	C_WidgetFactory::~C_WidgetFactory()
	{
		m_container.clear();
	}


	//////////////////////////////////////////////////////////////////////////
	//@ RegisterCreator
	//////////////////////////////////////////////////////////////////////////
	bool C_WidgetFactory::RegisterCreator(C_String widgetTypeName, T_WidgetCreator creator)
	{
		T_Hash32 widgetTypeID = C_FNVHash::computeHash32(widgetTypeName);

		std::pair<T_CreatorContainer::iterator, bool> res = m_container.insert(T_CreatorContainer::value_type(widgetTypeID, creator));
		if (!res.second)
		{
			//Creator pre widgetType uz vo factory je, overridneme ho
			m_container.erase(res.first);
			res = m_container.insert(T_CreatorContainer::value_type(widgetTypeID, creator));
			if (!res.second)
			{
				TRACE_FE("Can not register WidgetCreator for type: %s", widgetTypeName.c_str());//get string?
			}
			else
				TRACE_FI("Overrided WidgetCreator for type: %s", widgetTypeName.c_str());//get string?

		}
			
		return res.second;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ CreateWidget
	//////////////////////////////////////////////////////////////////////////
	T_Widget C_WidgetFactory::CreateWidget(S_WidgetDescriptor & desc)
	{
		T_Hash32 widgetTypeID = C_FNVHash::computeHash32(desc.m_typeName);
		T_CreatorContainer::iterator iter = m_container.find(widgetTypeID);

		if (iter == m_container.end())
		{
			TRACE_FE("Can not find proper WidgetCreator for type %s", desc.m_typeName.c_str());
			return T_Widget();
		}

		if (desc.m_hashNameUID.IsClear())
		{
			TRACE_FW("Widget type: '%s' has not set uniqueID!", desc.m_typeName.c_str());
			std::string str = boost::lexical_cast<std::string>(GenerateUID());
			desc.m_hashNameUID = C_HashName(str.c_str());
		}
		T_Widget widget = iter->second->CreateWidget(desc.m_hashNameUID, desc.GetListener());
		return widget;
	}

}}