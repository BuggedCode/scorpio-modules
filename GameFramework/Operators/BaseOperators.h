/*
* filename:			BaseOperators.h
*
* author:			Kral Jozef
* date:				03/24/2009		23:03
* version:			1.00
* brief:
*/

#pragma once

#include "I_BaseOperator.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ Additive operator
	template<typename T>
	class C_AdditiveOperator : public I_StateOperator<T>
	{
      typedef I_StateOperator<T> T_Super;
	public:
		//@ c-tor
		//@ step in 1 ms
		C_AdditiveOperator(T & inVariable, I_OperatorListener * inListener, const T & endState, float step)
			: I_StateOperator<T>(inVariable, inListener, endState), m_step(step)
		{
			//@ ak je step kladny zvacsujem, predpoklad ze endstate je > ako inVariable
			//@ nemusi vzdy platit ked sa niektory operator instancuje ale je neaktivny a zaktivni sa az neskor
			/*if (m_step > 0)
			{
				SC_ASSERT( endState >= inVariable );
			}
			else
			{
				SC_ASSERT( endState <= inVariable );
			}*/
		}
		//@ d-tor
		~C_AdditiveOperator() {};

		//@ GetStep
		float			GetStep() const { return m_step; }

		//@ Update - inDeltaTime in milliseconds
		void			Update(u32 inDeltaTime)
		{
			if (!T_Super::m_active)
				return;

			float offset = m_step * inDeltaTime;

			if (m_step > 0)
			{
				if ((T_Super::m_variable + offset) >= T_Super::m_endState)
				{
					T_Super::m_variable = T_Super::m_endState;
					T_Super::m_active = false;
					if (T_Super::m_listener)
						T_Super::m_listener->NotifyOperatorFinish(this);	
				}
				else
					IncreaseInternalVariable(offset);
			}
			else
			{
				if ((T_Super::m_variable + offset) <= T_Super::m_endState)
				{
					T_Super::m_variable = T_Super::m_endState;
					T_Super::m_active = false;
					if (T_Super::m_listener)
						T_Super::m_listener->NotifyOperatorFinish(this);
				}
				else
					IncreaseInternalVariable(offset);
			}
		}

	private:
		void			IncreaseInternalVariable(float fOffset) { T_Super::m_variable += (T)fOffset; }

	protected:
		float			m_step;
	};
   
}}