/*
* filename:			C_FadeOperator.h
*
* author:			Kral Jozef
* date:				06/28/2009		14:02
* version:			1.00
* brief:
*/

#pragma once

#include "BaseOperators.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_FadeOperator
	class C_FadeOperator : public C_AdditiveOperator<float>
	{
	public:
		//@ c-tor
		C_FadeOperator(float & colorAlpha, I_OperatorListener * listener, float fStep)
			: C_AdditiveOperator(colorAlpha, listener, (fStep < 0.f) ? 0.f : 1.f, fStep)
		{
			Deactivate();
		}

		//@ SetStep
		void				SetStep(float fStep)
		{
			m_step = fStep;
			m_endState = (fStep < 0.f) ? 0.f : 1.f;
		}
	private:
	};


	typedef boost::shared_ptr<C_FadeOperator>		T_FadeOperator;

}}