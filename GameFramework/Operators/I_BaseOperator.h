/*
* filename:			I_BaseOperator.h
*
* author:			Kral Jozef
* date:				03/24/2009		22:19
* version:			1.00
* brief:				Interface for base operators
*						operator operate above some variable in time until it reach end state, then notify listener
*						2 types of operators state operator and time operator
*/

#pragma once

#include "I_OperatorListener.h"
#include <Common/types.h>
#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace framework{

	class I_Operator;

	typedef boost::shared_ptr<I_BaseOperator>	T_Operator;

	//@ Interpolator!
	//////////////////////////////////////////////////////////////////////////
	//@ I_Operator
	class I_BaseOperator : public I_NonCopyAble
	{
	public:
		//@ c-tor
		I_BaseOperator(I_OperatorListener * inListener) : m_listener(inListener), m_active(true) {};
		//@ d-tor
		virtual ~I_BaseOperator() {};

		//@ Update
		//@ param inDeltaTime - difference between last called update
		virtual void			Update(u32 inDeltaTime) = 0;

		//@ Reset
		//@ pretazenim mozme umoznit reset internych stavov/nie vzdy je potreba (napr. potrebne pro reset accumulatora v offset operatore)
		virtual void			Reset() {};

		//@ Deactivate - vynutena deaktivacia
		virtual void			Deactivate() { m_active = false; }
		//@ Activate - forced Activation - posibilliyt of overriding fe. composed operators like C_ColorOperators
		virtual void			Activate() { m_active = true; }
		//@ IsActive
		bool						IsActive() const { return m_active; }

	protected:
		bool						m_active;
		I_OperatorListener	*	m_listener;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ I_StateOperator
	template<typename T>
	class I_StateOperator : public I_BaseOperator
	{
	public:
		//@ c-tor
		I_StateOperator(T & inVariable, I_OperatorListener * inListener, const T & endState)
			: I_BaseOperator(inListener), m_variable(inVariable), m_endState(endState) {};

		//@ SetEndState
		void						SetEndState(const T & endState) { m_endState = endState; }
		//@ GetEndState
		const T				&	GetEndState() const { return m_endState; }
	protected:
		T						&	m_variable;
		T							m_endState;		//@ final state when operator will stop
	};


	//////////////////////////////////////////////////////////////////////////
	//@ I_TimeOperator
	template<typename T>
	class I_TimeOperator : public I_BaseOperator
	{
	public:
		//@ c-tor
		I_TimeOperator(T & inVariable, I_OperatorListener * inListener, u64 inTime)
			: I_BaseOperator(inListener), m_variable(inVariable), m_time(inTime) {};
	protected:
		T						&	m_variable;
		u64						m_time;			//@ time in miliseconds how long operator will work until it stops
	};


	//////////////////////////////////////////////////////////////////////////
	//@ I_OffsetOperator
	//@ operator operuje nad premennou pokud nedosahne vstupniho offsetu(ten se zvysuje Updatem)
	template<typename T, class U>
	class I_OffsetOperator : public I_BaseOperator
	{
	public:
		//@ c-tor
		I_OffsetOperator(T & inVariable, I_OperatorListener * inListener, U inOffset)
			: I_BaseOperator(inListener), m_variable(inVariable), m_offset(inOffset) {};
	protected:
		T						&	m_variable;
		U							m_offset;
	};


}}
