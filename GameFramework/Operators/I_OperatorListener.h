/*
* filename:			I_OperatorListener.h
*
* author:			Kral Jozef
* date:				03/24/2009		22:40
* version:			1.00
* brief:				Interface for operator action listener
*/

#pragma once

#include <Common/I_NonCopyAble.h>

namespace scorpio{ namespace framework{

	class I_BaseOperator;

	//////////////////////////////////////////////////////////////////////////
	//@ I_Actionlistener
	class I_OperatorListener : public I_NonCopyAble
	{
	public:
		//@ d-tor
		virtual ~I_OperatorListener() {};

		//@ NotifyOperatorFinish
		//@ notify listener that operator finish work, callerOp - pointer to operator which send Notification
		//@ param - callerOp should be used as a UniqueID for looking for in list of many operators
		virtual void		NotifyOperatorFinish(I_BaseOperator * callerOp) = 0;
	};
}}
