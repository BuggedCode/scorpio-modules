/*
* filename:			RectangleOperators.h
*
* author:			Kral Jozef
* date:				06/13/2009		16:06
* version:			1.00
* brief:				operators working above rectangles
*/

#pragma once

#include "I_BaseOperator.h"
#include <SysMath/MathUtils.h>
#include <SysMath/C_Rect.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ExtendRectOperator
	class C_ExtendRectOperator : public I_OffsetOperator<sysmath::C_Rect, u16>
	{
	public:
		//@ c-tor
		C_ExtendRectOperator(sysmath::C_Rect & inVariable, I_OperatorListener * inListener, u16 inOffset, float positiveStep)
			: I_OffsetOperator(inVariable, inListener, inOffset), m_step(positiveStep), m_accOffset(0.f), m_origVariable(inVariable)
		{
			SC_ASSERT(m_step >= 0 && "Use C_ShrinkRectOperator");
		}

		//@ Update
		virtual void			Update(u32 inDeltaTime)
		{
			if (!m_active)
				return;

			float fPartOff = m_step * inDeltaTime;

			//@ ak novy offset bude vacsi vypocitam partialny o kolko zvacsit invariable
			if ((m_accOffset + fPartOff) >= m_offset)
			{
				m_variable.m_left = m_origVariable.m_left - m_offset;
				m_variable.m_right = m_origVariable.m_right + m_offset;
				m_variable.m_top = m_origVariable.m_top - m_offset;
				m_variable.m_bottom = m_origVariable.m_bottom + m_offset;

				m_active = false;
				if (m_listener)
					m_listener->NotifyOperatorFinish(this);
			}
			else
			{
				//@ zvysim m_variable a akumulujem offset
				m_accOffset += fPartOff;

				m_variable.m_left = m_origVariable.m_left - sysmath::round(m_accOffset);
				m_variable.m_right = m_origVariable.m_right + sysmath::round(m_accOffset);
				m_variable.m_top = m_origVariable.m_top - sysmath::round(m_accOffset);
				m_variable.m_bottom = m_origVariable.m_bottom + sysmath::round(m_accOffset);
			}
		}

		//@ Reset
		virtual void			Reset() { m_accOffset = 0.f; }
		//@ GetCurrentOffset
		u16						GetCurrentOffset() const { return sysmath::round(m_accOffset); }

		//@ d-tor
		~C_ExtendRectOperator() {};

	private:
		float			m_step;
		float			m_accOffset;	//akumulated offset
	protected:
		sysmath::C_Rect m_origVariable;	//originalna hodnota po inicializacii
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_ShrinkRectOpertaor
	class C_ShrinkRectOperator : public I_OffsetOperator<sysmath::C_Rect, u16>
	{
	public:
		//@ c-tor
		C_ShrinkRectOperator(sysmath::C_Rect & inVariable, I_OperatorListener * inListener, u16 inOffset, float negativeStep)
			: I_OffsetOperator(inVariable, inListener, inOffset), m_step(negativeStep), m_accOffset(0.f), m_origVariable(inVariable)
		{
			SC_ASSERT(m_step <= 0 && "Use C_ExtendRectOperator");
		}

		//@ Update
		virtual void			Update(u32 inDeltaTime)
		{
			if (!m_active)
				return;

			float fPartOff = -m_step * inDeltaTime;

			//@ ak novy offset bude vacsi vypocitam partialny o kolko zvacsit invariable
			if ((m_accOffset + fPartOff) >= m_offset)
			{
				m_variable.m_left = m_origVariable.m_left + m_offset;
				m_variable.m_right = m_origVariable.m_right - m_offset;
				m_variable.m_top = m_origVariable.m_top + m_offset;
				m_variable.m_bottom = m_origVariable.m_bottom - m_offset;

				m_active = false;
				if (m_listener)
					m_listener->NotifyOperatorFinish(this);
			}
			else
			{
				//@ zvysim m_variable a akumulujem offset
				m_accOffset += fPartOff;

				m_variable.m_left = m_origVariable.m_left + sysmath::round(m_accOffset);
				m_variable.m_right = m_origVariable.m_right - sysmath::round(m_accOffset);
				m_variable.m_top = m_origVariable.m_top + sysmath::round(m_accOffset);
				m_variable.m_bottom = m_origVariable.m_bottom - sysmath::round(m_accOffset);
			}
		}

		//@ Reset
		virtual void			Reset() { m_accOffset = 0.f; }
		//@ GetCurrentOffset
		u16						GetCurrentOffset() const { return sysmath::round(m_accOffset); }

		//@ d-tor
		~C_ShrinkRectOperator() {};

	private:
		float			m_step;
		float			m_accOffset;	//akumulated offset
		sysmath::C_Rect m_origVariable;	//originalna hodnota po inicializacii
	};


	//////////////////////////////////////////////////////////////////////////
	//
	template<class ObjType>
	class C_ExtendObjOperator : public C_ExtendRectOperator
	{
		typedef C_ExtendRectOperator	T_Super;
	public:
		//@ c-tor
		C_ExtendObjOperator(ObjType & object, I_OperatorListener * inListener, u16 inOffset, float positiveStep)
			: C_ExtendRectOperator(m_rect, inListener, inOffset, positiveStep), m_object(object)
		{
			m_rect.m_left = (s32)(object->GetPosition().m_x - object->GetWidth() / 2.f);
			m_rect.m_top = (s32)(object->GetPosition().m_y - object->GetHeight() / 2.f);
			m_rect.m_right = m_rect.m_left + object->GetWidth();
			m_rect.m_bottom = m_rect.m_top + object->GetHeight();

			//@ hack 
			m_origVariable = m_rect;
		}

		//@ Update
		virtual void			Update(u32 inDeltaTime)
		{
			bool bWasActive = m_active;
			T_Super::Update(inDeltaTime);
			if (bWasActive)
			{
				//prevalim zmeny na object, aj ked v poslednom kroku az po notifikacii
				//m_object->SetPosition(sysmath::C_Vector2((float)m_rect.m_left, (float)m_rect.m_top));
				m_object->SetSize(m_rect.GetWidth(), m_rect.GetHeight());
			}
		}



	private:
		sysmath::C_Rect	m_rect;
		ObjType			&	m_object;
	};



	typedef boost::shared_ptr<C_ExtendRectOperator>	T_ExtendRectOperator;
	typedef boost::shared_ptr<C_ShrinkRectOperator>	T_ShrinkRectOperator;

}}
