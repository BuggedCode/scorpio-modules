/*
* filename:			C_ColorInterpolator.h
*
* author:			Kral Jozef
* date:				03/17/2011		22:43
* version:			1.00
* brief:				Linear Interpolator between 2 colors
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <SysMAth/C_Vector4.h>
#include "BaseOperators.h"

namespace scorpio{ namespace framework{

#pragma warning(push)
#pragma warning(disable:4355)	//suppres warning for using this in initializer list		

	namespace scmath = scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ C_ColorInterpolator
	class C_ColorInterpolator : public I_BaseOperator, public I_OperatorListener
	{
		typedef I_BaseOperator	T_Super;
	public:
		//@ c-tor
		//@ transitionTime - interpolation time
		C_ColorInterpolator(scmath::C_Vector4 & vctColor, I_OperatorListener * inListener, const scmath::C_Vector4 & vctColorEnd, u32 transitionTime)
			: I_BaseOperator(inListener), m_Color(vctColor), m_FinishedOpCounter(0),
			m_operatorR(m_Color.m_x, this, vctColorEnd.m_x, (vctColorEnd.m_x - m_Color.m_x) / transitionTime),
			m_operatorG(m_Color.m_y, this, vctColorEnd.m_y, (vctColorEnd.m_y - m_Color.m_y) / transitionTime),
			m_operatorB(m_Color.m_z, this, vctColorEnd.m_z, (vctColorEnd.m_z - m_Color.m_z) / transitionTime)
		{
			m_active = false;
		};

		//@ NotifyOperatorFinish
		virtual void		NotifyOperatorFinish(I_BaseOperator * callerOp)
		{
			++m_FinishedOpCounter;
			if (m_FinishedOpCounter == 3 && m_listener)	//number of operators in class
			{
				m_listener->NotifyOperatorFinish(this);
			}
		}

		//@ Update
		virtual void		Update(u32 inDeltaTime)
		{
			if (!m_active)
				return;

			m_operatorR.Update(inDeltaTime);
			m_operatorG.Update(inDeltaTime);
			m_operatorB.Update(inDeltaTime);
		}


		//@ Activate
		virtual void			Activate()
		{
			T_Super::Activate();
			m_operatorR.Activate();
			m_operatorG.Activate();
			m_operatorB.Activate();
			m_FinishedOpCounter = 0;
		}


		//@ Deactivate
		virtual void			Deactivate()
		{
			T_Super::Deactivate();
			m_operatorR.Deactivate();
			m_operatorG.Deactivate();
			m_operatorB.Deactivate();
			m_FinishedOpCounter = 3;
		}

	private:
		scmath::C_Vector4	&	m_Color;
		//4 float operators
		C_AdditiveOperator<float>	m_operatorR;
		C_AdditiveOperator<float>	m_operatorG;
		C_AdditiveOperator<float>	m_operatorB;
		//C_AdditiveOperator<float>	m_operatorA;

		u8	m_FinishedOpCounter;
	};

#pragma warning(pop)
}}