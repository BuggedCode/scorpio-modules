/*
* filename:			C_VolumeFader.h
*
* author:			Kral Jozef
* date:				03/09/2011		0:12
* version:			1.00
* brief:				This fader is used only for MUSIC, has no sense to use it for sounds
*/

#pragma once

#include "C_FadeOperator.h"
#include <SysAudio/C_AudioManager.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_VolumeFader
	class C_VolumeFader : public C_FadeOperator
	{
	private:
		typedef C_FadeOperator	T_Super;
	public:
		//@ c-tor
		C_VolumeFader(float & fVolume, I_OperatorListener * listener, float fStep)
			: C_FadeOperator(fVolume, listener, fStep) {};


		//@ Update
		void			Update(u32 inDeltaTime)
		{
			if (!m_active)
				return;

			T_Super::Update(inDeltaTime);
			//@ set Volume
			sysaudio::T_AudioManager::GetInstance().SetMusicVolume(m_variable);
		}

	};

}}