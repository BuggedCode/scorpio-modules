/*
* filename:			C_GameFramework.cpp
*
* author:			Kral Jozef
* date:				05/23/2009		16:52
* version:			1.00
* brief:				Initialisation part for module GameFramework
*/

#include "C_GameFramework.h"
#include <SysUtils/C_TraceClient.h>
#include <GameFramework/gui/C_WidgetFactory.h>
#include <GameFramework/gui/C_DialogManager.h>
#include <GameFramework/Particles/C_ParticleSystem.h>
#include <GameFramework/Core/C_TickModulesManager.h>
#include <GameFramework/DbgUtils/C_DebugDrawTool.h>
#include <GameFramework/StringTable/C_StringTable.h>
#include <GameFramework/Core/C_EventManager.h>
#include <GameFramework/Core/C_ShortcutManager.h>
#include <GameFramework/Core/C_InputManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace sysutils;

	//////////////////////////////////////////////////////////////////////////
	//@ Initialize
	bool C_GameFramework::Initialize()
	{
		//@ create ShortcutManager
		bool bRes = T_InputManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create Input Manager!");
			return false;
		}

		//@ create ShortcutManager
		bRes = T_ShortcutManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create Shortcut Manager!");
			return false;
		}

		//@ create EventManager
		bRes = T_EventManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create Event Manager!");
			return false;
		}

		//@ init widget factory
		bRes = C_WidgetFactory::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create Widget Factory!");
			return false;
		}

		//@ init dialog manager
		bRes = C_DialogManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create DialogManager!");
			return false;
		}

		//////////////////////////////////////////////////////////////////////////
		//@ particles initialization
		bRes = effects::T_ParticleSystem::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create Particle system!");
			return false;
		}

		bRes = effects::T_ParticleSystem::GetInstance().Initialize();
		if (!bRes)
		{
			TRACE_E("Can not properly initilized Particle system!");
			return false;
		}
		else
			TRACE_I("Particle system succesfully initilized");

		bRes = T_TickModuleManager::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create TickModulesManager!");
			return false;
		}

#ifdef NO_FINAL
		bRes = T_DebugDrawTool::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create C_DebugDrawTool!");
			return false;
		}
		else
			TRACE_I("C_DebugDrawTool succesfully created");
#endif

		bRes = T_StringTable::CreateInstance();
		if (!bRes)
		{
			TRACE_E("Can not create StringTable!");
			return false;
		}


		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ Deinitialize
	bool C_GameFramework::Deinitialize()
	{
		T_StringTable::DestroyInstance();

#ifdef NO_FINAL
		T_DebugDrawTool::DestroyInstance();
#endif

		T_TickModuleManager::DestroyInstance();
		effects::T_ParticleSystem::GetInstance().Deinitialize();
		effects::T_ParticleSystem::DestroyInstance();
		C_DialogManager::DestroyInstance();
		C_WidgetFactory::DestroyInstance();
		T_EventManager::DestroyInstance();
		T_ShortcutManager::DestroyInstance();
		T_InputManager::DestroyInstance();
		return true;
	}

}}