/*
* filename:			CreditsUtils.h
*
* author:			Kral Jozef
* date:				04/03/2011		16:34
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/Credits/C_Credits.h>
#include <SysCore/Frames/C_BitmapText.h>
#include <SysMath/C_Point.h>
#include <SysMath/C_Vector2.h>
#include <SysCore/Core/C_RenderManager.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ScrollCreditsFunc
	class C_ScrollCreditsFunc
	{
	public:
		//@ c-tor
		C_ScrollCreditsFunc(float yOffsetMove)
			: m_OffsetY(yOffsetMove), m_Finished(true) {};

		//@ operator
		void operator()(C_Credits::S_LineInfo & lineInfo)
		{
			lineInfo.m_YPos -= m_OffsetY;
			//@ check if some line is on screen, if no one -> scroll finished
			if (lineInfo.m_YPos > 0 && m_Finished)
				m_Finished = false;

			if (lineInfo.m_BitmapText)
			{
				const sysmath::C_Vector2 & vctPos = lineInfo.m_BitmapText->GetPosition();
				lineInfo.m_BitmapText->SetPosition(sysmath::C_Vector2(vctPos.m_x, lineInfo.m_YPos));
			}

		}


		//@ IsFinished
		bool			IsFinished() const { return m_Finished; }

	private:
		float		m_OffsetY;
		bool		m_Finished;
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_TextVisualCreateFunc
	class C_TextVisualCreateFunc
	{
	public:
		//@ c-tor
		C_TextVisualCreateFunc(u16 screenWidth, u16 screenBottomLimit, u32 renderLayer)
			: m_ScreenWidth(screenWidth), m_ScreenBottomLimit(screenBottomLimit), m_RenderLayer(renderLayer) {};

		//@ operator
		void operator()(C_Credits::S_LineInfo & lineInfo)
		{
			if (lineInfo.m_YPos < m_ScreenBottomLimit && !lineInfo.m_BitmapText)
			{
				//@ create TextVisual
				syscore::T_BitmapText bitmapText = syscore::T_BitmapText(new syscore::C_BitmapText(lineInfo.m_Text, lineInfo.m_Font, syscore::E_TT_CZSK));
				bitmapText->SetScale(lineInfo.m_Scale);
				sysmath::C_Point size = bitmapText->GetSize();
				lineInfo.m_BitmapText = bitmapText;

				float xPos = (float)m_ScreenWidth - (float)size.x;
				xPos /= 2.f;


				lineInfo.m_BitmapText->SetPosition(scmath::C_Vector2(xPos, lineInfo.m_YPos));
				lineInfo.m_BitmapText->SetColor(lineInfo.m_Color);

				syscore::T_RenderManager::GetInstance().Add(lineInfo.m_BitmapText, m_RenderLayer);
			}
			else if (lineInfo.m_YPos < -10 && lineInfo.m_BitmapText)
			{
				//destroy credit
				lineInfo.m_BitmapText.reset();
			}
			return;
		}

	private:
		u16	m_ScreenWidth;
		u16	m_ScreenBottomLimit;
		u32	m_RenderLayer;
	};

}}