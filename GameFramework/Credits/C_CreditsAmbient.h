/*
* filename:			C_CreditsAmbient.h
*
* author:			Kral Jozef
* date:				04/08/2011		22:18
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <SysMath/C_Vector2.h>
#include <SysCore/Frames/C_Visual.h>
#include <SysCore/Render/C_Graphic.h>
#include <GameFramework/Operators/C_FadeOperator.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_CreditsAmbient
	class GAMEFRAMEWORK_API C_CreditsAmbient : public I_OperatorListener
	{
	public:
		//@ c-tor
		C_CreditsAmbient(u32 renderLayer)
			: m_RenderLayer(renderLayer), m_CurrGenTexId(0), m_CurrGenPosId(0) {};
		//@ d-tor
		~C_CreditsAmbient();


		//@ Update
		void					Update(u32 inDeltaTime);
		//@ Initialize
		bool					InitFromXml(const sysutils::T_Stream inpuStream);

		//@ ActivateFadeOut
		void					ActivateFadeOut();
		//@ StartProcessingAmbient
		void					StartProcessingAmbient();
		//@ NotifyOperatorFinish
		virtual void		NotifyOperatorFinish(I_BaseOperator * callerOp);

	private:
		//@ DecorateFrame
		void					DecorateFrame(syscore::T_Visual frame);

	private:
		u32					m_RenderLayer;
		typedef std::vector<sysmath::C_Vector2>	T_PosContainer;
		T_PosContainer		m_PosContainer;
		syscore::T_Visual	m_BaseFrame;
		float					m_FadeSpeed;
		u32					m_DisplayDelay;	//in Ms
		std::vector<T_Graphics>	m_Textures;
		T_FadeOperator		m_FadeOp;

		u32					m_CurrGenTexId;
		u32					m_CurrGenPosId;
	};

	typedef boost::shared_ptr<C_CreditsAmbient>	T_CreditsAmbient;

}}