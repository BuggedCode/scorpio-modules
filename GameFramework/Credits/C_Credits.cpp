/*
* filename:			C_Credits.cpp
*
* author:			Kral Jozef
* date:				04/02/2011		21:48
* version:			1.00
* brief:
*/

#include "C_Credits.h"
#include <SysUtils/Xml/C_XmlDocument.h>
#include <SysUtils/C_TraceClient.h>
#include "CreditsUtils.h"
#include <SysCore/Core/C_BitmapFontManager.h>
#include <SysUtils/C_Scheduler.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;
	using namespace scorpio::syscore;

	static const u32 C_CREDITS_DELAY = 1000;
	static const u16 C_CREDITS_Y_OFFSET_UNDER_BOTTOM = 50;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ initFromXmlNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Credits::InitFromXml(const T_Stream inputStream)
	{
		C_XmlDocument xmlDoc;
		if (!xmlDoc.Load(inputStream))
			return false;

		//@ SPEED
		const C_XmlNode * node = xmlDoc.SelectNode("/Credits/Speed");
		if (!node || !node->IsValid())
		{
			TRACE_FE("Missing node 'Speed' in %s file", inputStream->GetFileName());
			return false;
		}

		node->Read(m_Speed);

		//@ SPACING
		node = xmlDoc.SelectNode("/Credits/Spacing");
		if (!node || !node->IsValid())
		{
			TRACE_FE("Missing node 'Spacing' in %s file", inputStream->GetFileName());
			return false;
		}

		node->Read(m_Spacing);

		//@ TITLE_FONT
		node = xmlDoc.SelectNode("/Credits/TitleFont");
		if (!node || !node->IsValid())
		{
			TRACE_FE("Missing node 'TitleFont' in %s file", inputStream->GetFileName());
			return false;
		}

		//@ i'm lazy too drunk to check errors!!!! fuck saturday!!!!
		const C_XmlNode * tmpNode = node->GetNode("Size");
		tmpNode->Read(m_TitleFont.m_Size);
		tmpNode = node->GetNode("Font");
		C_String str = (*tmpNode)["uid"];
		m_TitleFont.m_Font = T_BitmapFontManager::GetInstance().GetFont(C_HashName(str));
		if (!m_TitleFont.m_Font)
			TRACE_FE("Credits TitleFont: %s not in manager!", str.c_str());
		tmpNode = node->GetNode("Color");
		tmpNode->Read(m_TitleFont.m_Color);


		//@ TEXT_FONT
		node = xmlDoc.SelectNode("/Credits/TextFont");
		if (!node || !node->IsValid())
		{
			TRACE_FE("Missing node 'TextFont' in %s file", inputStream->GetFileName());
			return false;
		}

		//@ I'm lazy too drunk to check errors!!!! fuck saturday!!!!
		tmpNode = node->GetNode("Size");
		tmpNode->Read(m_TextFont.m_Size);
		tmpNode = node->GetNode("Font");
		str = (*tmpNode)["uid"];
		m_TextFont.m_Font = T_BitmapFontManager::GetInstance().GetFont(C_HashName(str));
		if (!m_TitleFont.m_Font)
			TRACE_FE("Credits TextFont: %s not in manager!", str.c_str());
		tmpNode = node->GetNode("Color");
		tmpNode->Read(m_TextFont.m_Color);

		T_XmlNodeList lines = xmlDoc.SelectNodes("/Credits/Credits/Line");
		float fCurrYPos = (float)m_ScreenHeight + C_CREDITS_Y_OFFSET_UNDER_BOTTOM;
		for (T_ConstXmlNodePtrVector::iterator iter = lines->begin(); iter != lines->end(); ++iter)
		{
			const C_XmlNode * node = *iter;
			C_String str = (*node)["fontUID"];
			C_String text;
			node->Read(text);

			if (str.empty())	//textFont
			{
				u16 height = m_TextFont.m_Font->GetHeight();
				float fScale = m_TextFont.m_Size / (float)height;

				m_Lines.push_back(S_LineInfo(text, m_TextFont.m_Font, fCurrYPos, m_TextFont.m_Color, fScale, E_FT_TEXT));
				fCurrYPos += m_TextFont.m_Size;
			}
			else						//titleFont
			{
				u16 height = m_TitleFont.m_Font->GetHeight();
				float fScale = m_TitleFont.m_Size / (float)height;

				m_Lines.push_back(S_LineInfo(text, m_TitleFont.m_Font, fCurrYPos, m_TitleFont.m_Color, fScale, E_FT_TITLE));
				fCurrYPos += m_TitleFont.m_Size;
			}

			fCurrYPos += m_Spacing;
		}

		T_Scheduler::GetInstance().Schedule(this, &C_Credits::ActivateScrolling, 1);//1ms:)

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Credits::Update(u32 inDeltaTime)
	{
		if (!m_ScroolActive)
			return;

		C_ScrollCreditsFunc scrollFunc(inDeltaTime * m_Speed);
		//@ explicitly define to use reference for Func predicate
		std::for_each<T_LineContainer::iterator, C_ScrollCreditsFunc &>(m_Lines.begin(), m_Lines.end(), scrollFunc);

		C_TextVisualCreateFunc creteFunc(m_ScreenWidth, m_ScreenHeight + C_CREDITS_Y_OFFSET_UNDER_BOTTOM - 1, m_RenderLayer);
		std::for_each<T_LineContainer::iterator, C_TextVisualCreateFunc &>(m_Lines.begin(), m_Lines.end(), creteFunc);



		if (scrollFunc.IsFinished())
		{
			//reset positions & schedule new Scrolling
			ResetLines();
			m_ScroolActive = false;
			T_Scheduler::GetInstance().Schedule(this, &C_Credits::ActivateScrolling, C_CREDITS_DELAY);
		}

		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ResetLines
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Credits::ResetLines()
	{
		float fCurrYPos = (float)m_ScreenWidth + C_CREDITS_Y_OFFSET_UNDER_BOTTOM;
		for (T_LineContainer::iterator iter = m_Lines.begin(); iter != m_Lines.end(); ++iter)
		{
			iter->m_YPos = fCurrYPos;
			if (iter->m_FontType == E_FT_TEXT)
			{
				fCurrYPos += m_TextFont.m_Size;
			}
			else
			{
				fCurrYPos += m_TitleFont.m_Size;
			}

			fCurrYPos += m_Spacing;
		}

	}


}}