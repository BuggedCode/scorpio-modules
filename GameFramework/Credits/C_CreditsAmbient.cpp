/*
* filename:			C_CreditsAmbient.cpp
*
* author:			Kral Jozef
* date:				04/08/2011		22:19
* version:			1.00
* brief:
*/

#include "C_CreditsAmbient.h"
#include <SysCore/Helpers/C_ScgLoader.h>
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/Xml/C_XmlDocument.h>
#include <SysCore/Core/C_TextureManager.h>
#include <SysMath/C_Random.h>
#include <SysCore/Core/C_RenderManager.h>
#include <SysUtils/C_Scheduler.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;

	static const u32 C_ANGLE_OFFSET = 10;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_CreditsAmbient::~C_CreditsAmbient()
	{
		//@ remove scheduled actions for this context
		T_Scheduler::GetInstance().RemoveTasks(this);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_CreditsAmbient::InitFromXml(const T_Stream inpuStream)
	{
		C_XmlDocument xmlDoc;
		if (!xmlDoc.Load(inpuStream))
			return false;

		//@ FADE_SPEED
		const C_XmlNode * node = xmlDoc.SelectNode("/CreditsAmbient/FadeSpeed");
		if (!node || !node->IsValid())
		{
			TRACE_FE("Missing node 'FadeSpeed' in %s file", inpuStream->GetFileName());
			return false;
		}
		node->Read(m_FadeSpeed);

		//@ DISPLAY_DELAY
		node = xmlDoc.SelectNode("/CreditsAmbient/DisplayDelay");
		if (!node || !node->IsValid())
		{
			TRACE_FE("Missing node 'DisplayDelay' in %s file", inpuStream->GetFileName());
			return false;
		}
		node->Read(m_DisplayDelay);

		//@ MESH_SET
		node = xmlDoc.SelectNode("/CreditsAmbient/MeshSet");
		if (!node || !node->IsValid())
		{
			TRACE_FE("Missing node 'MeshSet' in %s file", inpuStream->GetFileName());
			return false;
		}

		C_String strMeshFileName = (*node)["file"];


		T_XmlNodeList list = xmlDoc.SelectNodes("/CreditsAmbient/Pictures/Texture");
		for (T_ConstXmlNodePtrVector::iterator iter = list->begin(); iter != list->end(); ++iter)
		{
			const C_XmlNode * node = *iter;
			SC_ASSERT(node);
			C_String texName = (*node)["file"];
			T_Graphics img = T_TextureManager::GetInstance().GetTexture(texName);
			if (!img)
			{
				TRACE_FE("Couldn't load texture: %s", texName.c_str());
				continue;
			}

			m_Textures.push_back(img);
		}


		//////////////////////////////////////////////////////////////////////////
		C_ScgLoader loader;
		T_Stream stream = T_FileSystemManager::GetInstance().Open(strMeshFileName);
		if (!loader.Load(stream))
			return false;

		T_FrameList frmList = loader.GetFrameList();
		for (T_FrameList::iterator iter = frmList.begin(); iter != frmList.end(); ++iter)
		{
			T_Frame frm = (*iter);
			C_HashName hashName = frm->GetHashName();
			C_String strName = hashName.GetName();
			strName = strName.Left(4);
			if (strName == "Point")
			{
				const C_Vector2 & vctWorld = frm->GetPosition();
				m_PosContainer.push_back(vctWorld);
			}
			else
			{
				strName = hashName.GetName();
				strName = strName.Left(9);
				if (strName == "BaseFrame")
				{
					m_BaseFrame = boost::dynamic_pointer_cast<C_Visual>(frm);
				}
				else
				{
					TRACE_FW("Unknown frame in Credits Ambient scene %s", hashName.GetName());
				}
			}
		}


		if (!m_BaseFrame)
		{
			TRACE_FE("Couldn't find 'BaseFrame' in %s", strMeshFileName.c_str());
			return false;
		}

		T_RenderManager::GetInstance().Add(m_BaseFrame, m_RenderLayer);


		T_Scheduler::GetInstance().Schedule(this, &C_CreditsAmbient::StartProcessingAmbient, 2000);

		DecorateFrame(m_BaseFrame);

		C_Vector4 & vctColor = m_BaseFrame->GetColor();
		vctColor.m_w = 0.f;
		m_FadeOp = T_FadeOperator(new C_FadeOperator(vctColor.m_w, this, m_FadeSpeed));

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_CreditsAmbient::Update(u32 inDeltaTime)
	{
		m_FadeOp->Update(inDeltaTime);
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ActivateFadeOut
	//
	//////////////////////////////////////////////////////////////////////////
	void C_CreditsAmbient::ActivateFadeOut()
	{
		m_FadeOp->SetStep(m_FadeOp->GetStep() * -1.f);
		m_FadeOp->Activate();
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ StartProcessingAmbient
	//
	//////////////////////////////////////////////////////////////////////////
	void C_CreditsAmbient::StartProcessingAmbient()
	{
		m_FadeOp->Activate();
		return;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ NotifyOperatorFinish
	//
	//////////////////////////////////////////////////////////////////////////
	void C_CreditsAmbient::NotifyOperatorFinish(I_BaseOperator * callerOp)
	{
		if (m_FadeOp->GetStep() > 0)	//fadeIn
		{
			T_Scheduler::GetInstance().Schedule(this, &C_CreditsAmbient::ActivateFadeOut, m_DisplayDelay);
		}
		else
		{
			//@ activate FadeIn & change Texture & position
			m_FadeOp->SetStep(m_FadeOp->GetStep() * -1.f);
			m_FadeOp->Activate();

			DecorateFrame(m_BaseFrame);
		}

		return;
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ DecorateFrame
	//
	//////////////////////////////////////////////////////////////////////////
	void C_CreditsAmbient::DecorateFrame(syscore::T_Visual frame)
	{
		//@ random texture
		u32 id = C_Random::Random((u32)m_Textures.size());
		if (id == m_CurrGenTexId)
		{
			if (id < (u32)m_Textures.size() - 1)
				++id;
			else
				--id;
		}
		SC_ASSERT(id >= 0 && id < (u32)m_Textures.size());
		m_CurrGenTexId = id;
		T_Graphics img = m_Textures[id];
		frame->SetTexture(img);

		//@ random position
		id = C_Random::Random((u32)m_PosContainer.size());
		if (id == m_CurrGenPosId)
		{
			if (id < (u32)m_PosContainer.size() - 1)
				++id;
			else
				--id;
		}
		SC_ASSERT(id >= 0 && id < (u32)m_PosContainer.size());
		m_CurrGenPosId = id;
		const C_Vector2 & vctPos = m_PosContainer[id];
		frame->SetPosition(vctPos);

		//random float angle
		u32 fAngle = C_Random::Random((u32)C_ANGLE_OFFSET);
		if (C_Random::Random((u32)2) == 1)
			fAngle = 360 - fAngle;
		if (fAngle == 0)
			fAngle = 8;

		frame->SetRotation((float)fAngle);
	}

}}