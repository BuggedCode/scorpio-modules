/*
* filename:			C_Credits.h
*
* author:			Kral Jozef
* date:				04/02/2011		21:47
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <SysUtils/C_HashName.h>
#include <SysMath/C_Vector4.h>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <SysCore/Frames/C_BitmapText.h>


namespace scorpio{ namespace framework{

	namespace scutils = scorpio::sysutils;
	namespace scmath = scorpio::sysmath;
	namespace sccore = scorpio::syscore;

	//////////////////////////////////////////////////////////////////////////
	// C_Credits
	class GAMEFRAMEWORK_API C_Credits
	{
		enum E_FontType
		{
			E_FT_TITLE = 0,
			E_FT_TEXT,
		};

		struct S_FontInfo
		{
			u16						m_Size;
			scmath::C_Vector4		m_Color;
			sccore::T_BitmapFont	m_Font;
		};

	public:
		//////////////////////////////////////////////////////////////////////////
		struct S_LineInfo
		{
			//@ c-tor
			S_LineInfo(scutils::C_String text, sccore::T_BitmapFont font, float fYPos, const scmath::C_Vector4 & vctColor, float fScale, E_FontType fontType)
				: m_Text(text), m_Font(font), m_YPos(fYPos), m_Color(vctColor), m_Scale(fScale), m_FontType(fontType) {};

			scutils::C_String				m_Text;
			sccore::T_BitmapText			m_BitmapText;
			sccore::T_BitmapFont			m_Font;
			float								m_Scale;
			float								m_YPos;
			scmath::C_Vector4				m_Color;
			E_FontType						m_FontType;	//just for reset to set correct yPos depend on fontSize
		};


	public:
		//@ c-tor
		C_Credits(u16 screenWidth, u16 screenHeight, u32 renderLayer)
			: m_ScreenWidth(screenWidth), m_ScreenHeight(screenHeight), m_RenderLayer(renderLayer), m_ScroolActive(false) {};
		//@ d-tor
		~C_Credits() {};

		//@ InitFromXmlNode
		bool			InitFromXml(const sysutils::T_Stream inputStream);
		//@ Update
		void			Update(u32 inDeltaTime);
		//@ ActivateScrolling
		void			ActivateScrolling() { m_ScroolActive = true; }

	private:
		//@ ResetLines
		void			ResetLines();


	private:
		u16			m_ScreenWidth;
		u16			m_ScreenHeight;
		u32			m_RenderLayer;
		u16			m_Spacing;
		float			m_Speed;
		S_FontInfo	m_TitleFont;
		S_FontInfo	m_TextFont;
		bool			m_ScroolActive;

		typedef std::vector<S_LineInfo>	T_LineContainer;
		T_LineContainer	m_Lines;
	};

	typedef boost::shared_ptr<C_Credits>	T_Credits;

}}
