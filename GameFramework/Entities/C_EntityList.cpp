/*
* filename:			C_EntityList.cpp
*
* author:			Kral Jozef
* date:				01/12/2011		1:00
* version:			1.00
* brief:
*/

#include "C_EntityList.h"

namespace scorpio{ namespace framework{

	using namespace sysmath;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Add
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EntityList::Add(T_Entity entity)
	{
		std::pair<T_EntityList::iterator, bool> insRes = m_container.insert(T_EntityList::value_type(entity->GetName(), entity));
		//SC_ASSERT(insRes.second);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EntityList::Remove(T_Entity entity)
	{
		size_t iSize = m_container.erase(entity->GetName());
		SC_ASSERT(iSize != 0);
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetEntity
	//
	//////////////////////////////////////////////////////////////////////////
	T_Entity C_EntityList::GetEntity(const C_Vector2 & inPos)
	{
		for (T_EntityList::iterator iter = m_container.begin(); iter != m_container.end(); ++iter)
		{
			if (iter->second->Contains(inPos))
				return iter->second;
		}

		return T_Entity();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EntityList::Update(u32 tickDelta)
	{
		for (T_EntityList::iterator iter = m_container.begin(); iter != m_container.end(); ++iter)
		{
			iter->second->Update(tickDelta);
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Clear
	//
	//////////////////////////////////////////////////////////////////////////
	void C_EntityList::Clear()
	{
		m_container.clear();
	}

}}