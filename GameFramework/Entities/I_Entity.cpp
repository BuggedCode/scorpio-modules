/*
* filename:			I_Entity.cpp
*
* author:			Kral Jozef
* date:				03/24/2009		21:10
* version:			1.00
* brief:				"Interface" for base Entity class
*/

#include "I_Entity.h"
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_ROOT_RTTI(I_Entity)


	//////////////////////////////////////////////////////////////////////////
	//@ copy c-tor
	//////////////////////////////////////////////////////////////////////////
	I_Entity::I_Entity(const I_Entity & copy)
	{
		m_enabled = copy.m_enabled;
		m_active = copy.m_active;
		m_hashName = copy.m_hashName;
		m_InGameSubdiv = copy.m_InGameSubdiv;
		m_PermanentIngameSubdiv = copy.m_PermanentIngameSubdiv;
	}
	
}}