/*
* filename:			I_Entity.h
*
* author:			Kral Jozef
* date:				03/24/2009		20:18
* version:			1.00
* brief:				"Interface" for base Entity class
*/

#pragma once

#include <Common/I_NonCopyAble.h>
#include <Common/C_RTTI.h>
#include <SysMath/C_Vector2.h>
#include <SysMath/C_Vector4.h>
#include <SysMath/C_Rect.h>
#include <SysUtils/C_HashName.h>
#include <SysUtils/C_String.h>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <GameFramework/GameFrameworkApi.h>
#include <SysCore/Frames/C_Visual.h> //:(
#include <SysCore/Core/Animation/C_AnimationTrack.h> //:(

namespace scorpio{ namespace framework{

	typedef std::vector<sysutils::C_HashName>	T_HashNameVector;	//seznam hashNamu

	//////////////////////////////////////////////////////////////////////////
	//@ I_Entity
	class GAMEFRAMEWORK_API I_Entity	: public I_NonCopyAble
	{
	public:
		//@ c-tor
		I_Entity(const sysutils::C_String & name) 
			: m_enabled(true), m_active(true), m_InGameSubdiv(false), m_hashName(name), m_PermanentIngameSubdiv(false)
		{
			//nop
		};

		//@ d-tor
		virtual ~I_Entity() {};

		//////////////////////////////////////////////////////////////////////////
		const sysutils::C_HashName	&	GetName() const { return m_hashName; }
		//@ SetHashName
		void									SetHashName(const sysutils::C_HashName & hashName) { m_hashName = hashName; }
		//@ SetVisible
		virtual void						SetVisible(bool bEnable) = 0;
		//@ IsVisible
		virtual bool						IsVisible() const = 0;
		//@ IsEnabled
		bool									IsEnabled() const { return m_enabled; }
		//@ SetEnabled
		void									SetEnabled(bool bEnabled) { m_enabled = bEnabled; }
		//@ IsActive
		bool									IsActive() const { return m_active; }
		//@ SetActive
		void									SetActive(bool bActive) { m_active = bActive; }
		//@ SetInGameSubdiv
		void									SetInGameSubdiv(bool bInGameSubdiv) { m_InGameSubdiv = bInGameSubdiv; }
		//@ IsInGameSubdiv
		bool									IsInGameSubdiv() const { return m_InGameSubdiv; }
		//@ IsPermanentInGameSubdiv
		bool									IsPermanentInGameSubdiv() const { return m_PermanentIngameSubdiv; }
		//@ SetPermanentInGameSubdiv
		void									SetPermanentInGameSubdiv(bool bEnable) { m_PermanentIngameSubdiv = bEnable; }
		//@ SetPosition
		virtual void						SetPosition(const sysmath::C_Vector2 & vctPos) = 0;
		virtual void						SetPosition(float inPosX, float inPosY) = 0;
		//@ GetPosition
		virtual const sysmath::C_Vector2	&	GetPosition() const = 0;
		//@ SetRotation
		//@ fAngle - is in degree
		virtual void						SetRotation(float fAngle) = 0;
		//@ GetRotation
		virtual float						GetRotation() const = 0;
		//@ SetScale
		virtual void						SetScale(float fScale) = 0;
		//@ GetScale
		virtual float						GetScale() const = 0;
		//@ SetSize
		//virtual void						SetSize(u32 inWidth, u32 inHeight) = 0;
		//@ GetSize
		//virtual const sysmath::C_Point& GetSize() const = 0;
		//@ GetWidth
		virtual u32							GetWidth() const = 0;
		//@ GetHeight
		virtual u32							GetHeight() const = 0;
		//@ SetColor
		virtual void						SetColor(const sysmath::C_Vector4 & color) = 0;
		//@ SetAlpha
		virtual void						SetAlpha(float fAlpha) = 0;
		//@ GetColor
		virtual const sysmath::C_Vector4		&	GetColor() const = 0;
		
		//@ Update - synchronize/update behaviours - must be called before draw
		//@ param inDeltaTime - different time between last and actual called update in ms
		virtual void						Update(u32 inDeltaTime) = 0;
		//@ "vlozi entitu" do rendru
		//virtual void						AddToRender(u32 renderLayer) = 0;

		//@ Contains - Collision function; if entity contain input position
		virtual bool						Contains(const sysmath::C_Vector2 & inPos) const = 0;
		//@ SetFrame - hm hm hm :(
		virtual void						SetFrame(syscore::T_Frame frame) {};
		//@ GetFrame
		virtual const syscore::T_Frame			GetFrame() const = 0;
		//@ AddAnimation
		virtual void						AddAnimation(syscore::T_AnimationTrack anim) {};
		//@ NoteTrackNotify
		virtual void						NoteTrackNotify(T_Hash32 eventId, T_Hash32 param1, T_Hash32 param2) {};

	protected:
		//@ copy c-tor
		I_Entity(const I_Entity & copy);

	protected:
		bool									m_enabled;		//entita je pristupna pre okolity svet (render/fyziku/...)
		bool									m_active;		//entita je aktivna pre game(playera)
		bool									m_InGameSubdiv;	//entita je v game subdiv
		bool									m_PermanentIngameSubdiv;	//entity sa netestuje na mazanie z gameSubdiv
		sysutils::C_HashName				m_hashName;		//jednoznacne meno

		DECLARE_ROOT_RTTI
	};

	
	typedef boost::shared_ptr<I_Entity>		T_Entity;

	//////////////////////////////////////////////////////////////////////////
	//@ C_EntityRefCounter
	/*class C_EntityRefCounter
	{
	public:
		//@ c-tor
		C_EntityRefCounter(T_Entity instance)
			: m_Instance(instance), m_RefCounter(1) {};

		//@ operator ==
		bool operator ==(const C_EntityRefCounter & instance) const
		{
			return (m_Instance == instance.m_Instance);
		}
		
		//@ operator ==
		bool operator ==(const T_Entity instance) const
		{
			return (m_Instance == instance);
		}

		//@ operator <
		bool operator <(const C_EntityRefCounter & instance) const
		{
			return this->m_Instance < instance.m_Instance;
		}

		//@ AddRef
		u16			AddRef() { return ++m_RefCounter; }
		//@ Release
		//! after every Release you should check ifret val is 0, if so u should free instance of C_EntityRefCounter, thats mean boost::shared_ptr will dec by 1
		u16			Release() { return --m_RefCounter; }

	private:
		T_Entity		m_Instance;
		u16			m_RefCounter;
	};*/
}}

//typedef scorpio::framework::C_EntityRefCounter	T_EntityRefCounter;