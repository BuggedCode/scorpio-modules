/*
* filename:			C_Actor.h
*
* author:			Kral Jozef
* date:				03/02/2011		17:27
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/Entities/C_BaseEntity.h>
#include <SysCore/Geometry/C_PolyShape.h>
#include <SysPhysics/PhysicsDefines.h>
#include <SysPhysics/PhysicsCommon.h>
#include <Box2d/Collision/b2Collision.h>
#include <Box2d/Collision/Shapes/b2PolygonShape.h>

namespace scorpio{ namespace framework{

	//# forward coz S_RayCastGameResult has member of C_Actor:( 
	struct S_RayCastGameResult;
	class C_Actor;

	typedef boost::shared_ptr<C_Actor>	T_Actor;


	//////////////////////////////////////////////////////////////////////////
	//@ C_Actor
	class GAMEFRAMEWORK_API C_Actor : public C_BaseEntity
	{
		typedef C_BaseEntity	T_Super;
	public:
		//@ c-tor
		C_Actor(const sysutils::C_String & name);

		//@ Update
		virtual void			Update(u32 deltaTime);
		//@ SetFrame
		virtual void			SetFrame(syscore::T_Frame frame);

		//@ InitFromXmlNode
		virtual bool			InitFromXmlNode(const sysutils::C_XmlNode & node);

		//@ SetVelocity
		void						SetVelocity(float fVelocity) { m_Velocity = fVelocity; }
		//@ GetVelocity
		float						GetVelocity() const { return m_Velocity; }
		//@ SetDir
		void						SetDir(const sysmath::C_Vector2 & vctDir) { m_Dir = vctDir; }
		//@ GetDir - return directikon of movement
		const sysmath::C_Vector2	&	GetDirection() const { return m_Dir; }
		//@ SetInitPosition
		void						SetInitPosition(const sysmath::C_Vector2 & vctInitPos);
		//@ SetSpin
		void						SetSpin(float fSpin) { m_Spin = fSpin; }

		//@ GetVelocityLimits
		const sysmath::C_Vector2	&	GetVelocityLimits() const { return m_VelocityLimits; }
		//@ GetSpinLimits
		const sysmath::C_Vector2	&	GetSpinLimits() const { return m_SpinLimits; }
		//@ SetProxyId
		void						SetProxyId(s32 proxyId) { m_ProxyId = proxyId; }
		//@ GetProxyId
		s32						GetProxyId() const { return m_ProxyId; }
		//@ GetCollisionLayer
		sysphysics::T_CollisionLayer	GetCollisionLayer() const { return m_CollisionLayer; }

		//////////////////////////////////////////////////////////////////////////
		//callback COLL DETECTION INTERFACE
		//@ ShapeCast
		bool						ShapeCast(const b2RayCastInput & rayInput, b2RayCastOutput & outResult);
		//@ QueryAABB
		bool						QueryAABB(const sysmath::C_AABB2 & inputAABB);
		//@ QueryShape
		bool						QueryShape(const sysphysics::T_B2PolyShape & shape, const sysphysics::T_Transformation & transform);
		//@ OnCollision
		virtual void			OnCollision(I_Entity * collWith, const S_RayCastGameResult & rayCastResult, T_Actor thisSharedPtr) {};

		//@ DebugDrawCollisions
		void						DebugDrawCollisions();

		//@ IsDone
		virtual bool			IsDead() const { return m_IsDead; }

		//@ OnDestruction
		//@ Same as explosion but public! - forced destruction from player or game play script! bonus (without deformation!)
		virtual void			OnDestruction(T_Actor thisSharedPtr/*, const sysmath::C_Vector2 & vctDir, const sysutils::C_HashName effectTemplate*/) { SC_ASSERT(false); };


		DECLARE_RTTI

	protected:
		//@ copy c-tor
		C_Actor(const C_Actor & copy);


	public:
		static s32				C_INVALID_PROXY_ID;


	protected:
		sysphysics::T_CollisionLayer	m_CollisionLayer;

		sysmath::C_Vector2			m_VelocityLimits;
		float								m_Velocity;
		sysmath::C_Vector2			m_Dir;
		u32								m_AccumTime;
		sysmath::C_Vector2			m_InitPos;
		//float							m_Scale;
		sysmath::C_Vector2			m_SpinLimits;
		float								m_Spin;
		s32								m_ProxyId;	//id to dynamic aabb tree of box2d	//game subdiv!

		sysmath::C_Vector2			m_PolyLocalPos;
		sysphysics::T_B2PolyShape	m_CollShape;	//can be NULL

		bool								m_IsDead;	//actor is dead - will be removed form subdivision && EntityList
	};
}}

typedef std::vector<scorpio::framework::T_Actor>			T_ActorList;
typedef std::map<T_Hash32, scorpio::framework::T_Actor>	T_ActorMap;
