/*
* filename:			C_Actor.cpp
*
* author:			Kral Jozef
* date:				03/02/2011		17:41
* version:			1.00
* brief:
*/

#include "C_Actor.h"
#include <SysUtils/xml/C_XmlNode.h>
#include <SysCore/Frames/C_Area.h>
#include <GameUtils/Others/C_ConvertUtils.h>
#include <GameFramework/DbgUtils/C_DebugDrawTool.h>
#include <SysMath/C_Matrix2d.h>
#include <SysPhysics/Utils/C_CollisionUtils.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;
	using namespace scorpio::sysutils;
	using namespace scorpio::sysphysics;
	using namespace scorpio::gameutils;
	using namespace scorpio::framework;

	DEFINE_RTTI(C_Actor, C_BaseEntity)

	static const T_Hash32 C_COLL_FRAME_NAME_HASH = C_FNVHash::computeHash32("Coll");

	s32 C_Actor::C_INVALID_PROXY_ID = -1;	//b2_nullNode

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Actor::C_Actor(const C_String & name)
		: C_BaseEntity(name), m_IsDead(false)
	{
		m_Velocity = 0.f;
		m_AccumTime = 0;
		m_Spin = 0.f;
		m_ProxyId = C_INVALID_PROXY_ID;
		m_CollisionLayer = D_CL_ALL;	//default
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ copy c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_Actor::C_Actor(const C_Actor & copy)
		: T_Super(copy)
	{
		m_VelocityLimits = copy.m_VelocityLimits;
		m_Velocity = copy.m_Velocity;
		m_Dir = copy.m_Dir;
		m_AccumTime = copy.m_AccumTime;
		m_InitPos = copy.m_InitPos;
		m_SpinLimits = copy.m_SpinLimits;
		m_Spin = copy.m_Spin;
		m_ProxyId = C_INVALID_PROXY_ID;
		m_PolyLocalPos = copy.m_PolyLocalPos;
		m_CollShape = copy.m_CollShape;
		m_IsDead = copy.m_IsDead;
		m_CollisionLayer = copy.m_CollisionLayer;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromXmlNode
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Actor::InitFromXmlNode(const C_XmlNode & node)
	{
		const C_XmlNode * tmpNode = node.GetNode("InitDir");
		if (tmpNode)
		{
			tmpNode->Read(m_Dir);
			m_Dir.normalize();
		}
		//const C_XmlNode * tmpNode = node.GetNode("InitScale");
		//if (tmpNode)
		//	tmpNode->Read(m_Scale);
		tmpNode = node.GetNode("Spin");
		if (tmpNode)
			tmpNode->Read(m_Spin);
		tmpNode = node.GetNode("Velocity");
		if (tmpNode)
			tmpNode->Read(m_Velocity);
		tmpNode = node.GetNode("VelocityLimits");
		if (tmpNode)
			tmpNode->Read(m_VelocityLimits);
		tmpNode = node.GetNode("SpinLimits");
		if (tmpNode)
			tmpNode->Read(m_SpinLimits);

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetInitPosition
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Actor::SetInitPosition(const scmath::C_Vector2 & vctInitPos)
	{
		this->SetPosition(vctInitPos);
		m_InitPos = vctInitPos;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetFrame
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Actor::SetFrame(T_Frame frame)
	{
		T_Super::SetFrame(frame);
		T_Frame collFrm = frame->FindFrame(C_COLL_FRAME_NAME_HASH);
		T_Area area = boost::dynamic_pointer_cast<C_Area>(collFrm);
		if (area)
		{
			T_PolyShape shape = area->GetShape();
			m_PolyLocalPos = area->GetLocalPosition();

			T_Polygon poly = shape->GetPolygon();
			u32 size = poly->GetSize();
			SC_ASSERT(size <= b2_maxPolygonVertices);

			T_Vector2 * vctArr = new T_Vector2[size];	//needless allocation!

			for (u8 i = 0; i < size; ++i)
			{
				const C_Vector2 & vctPos = (*poly)[i];
				vctArr[/*size - (i + 1)*/i].Set(vctPos.m_x, vctPos.m_y) ;
			}

			C_B2PolyShape * b2PolyShape = new C_B2PolyShape();
			b2PolyShape->Set(vctArr, size);

			m_CollShape = T_B2PolyShape(b2PolyShape);

			delete[] vctArr;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Actor::Update(u32 deltaTime)
	{
		//@ update anim ctrl
		T_Super::Update(deltaTime);
		
		m_AccumTime += deltaTime;

		//@ spin
		this->SetRotation(GetRotation() + deltaTime*m_Spin);
		//@ dir can change in time so we calculate next diff and addto current pos
		C_Vector2 vctS = (m_Dir * m_Velocity) * (float)deltaTime;
		this->SetPosition(GetPosition() + vctS);
		OnChangedPRS();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ShapeCast
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Actor::ShapeCast(const b2RayCastInput & rayInput, b2RayCastOutput & outResult)
	{
		if (!m_CollShape)
			return false;

		//@ check collision
		if (m_CollShape->GetVertexCount() == 0)
			return false;

		
		T_Transformation polyTransform;
		C_Vector2 vctPos = GetPosition() + m_PolyLocalPos;
		polyTransform.position.Set(vctPos.m_x, vctPos.m_y);
		float rot = DEG2RAD(GetRotation());
		polyTransform.R.Set(rot);
		bool bRes = m_CollShape->RayCast(&outResult, rayInput, polyTransform);

		if (!bRes)
		{
			//test if whole ray is inside shape
			bRes = m_CollShape->TestPoint(polyTransform, rayInput.p1);
			if (bRes)
				outResult.fraction = 0.f;
		}

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ QueryAABB
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Actor::QueryAABB(const C_AABB2 & inputAABB)
	{
		if (!m_CollShape)
		{
			//check aabb collision
			const C_AABB2 & aabb = m_Frame->GetAABB();
			return aabb.IsIntersect(inputAABB);
		}

		//@ check collision
		if (m_CollShape->GetVertexCount() == 0)
			return false;

		T_Transformation polyTransform;
		C_Vector2 vctPos = GetPosition() + m_PolyLocalPos;
		polyTransform.position.Set(vctPos.m_x, vctPos.m_y);
		float rot = DEG2RAD(GetRotation());
		polyTransform.R.Set(rot);
		
		bool bRes = C_CollisionUtils::CheckOverlap(inputAABB, m_CollShape, polyTransform);
		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ QueryShape
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_Actor::QueryShape(const T_B2PolyShape & shape, const T_Transformation & transform)
	{
		if (m_CollShape)
		{
			//@ calculate center of rotation
			float fAngle = GetRotation();
			C_Vector2 vctRotatedLocalPos = m_PolyLocalPos;
			vctRotatedLocalPos.rotateBy(fAngle);

			T_Transformation polyTransform;
			C_Vector2 vctPos = GetPosition() + vctRotatedLocalPos;
			polyTransform.position.Set(vctPos.m_x, vctPos.m_y);
			float rot = DEG2RAD(fAngle);
			polyTransform.R.Set(rot);

			bool bRes = b2TestOverlap(shape.get(), m_CollShape.get(), transform, polyTransform);
			return bRes;
		}

		bool bRes = C_CollisionUtils::CheckOverlap(m_Frame->GetAABB(), shape, transform);
		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDrawCollisions
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Actor::DebugDrawCollisions()
	{
		C_Vector4 dbgColor(.0f, .6f, .0f, 1.f);	//Green 

		C_Vector2 vctPolyPos = GetPosition();
		T_DebugDrawTool::GetInstance().DrawPoint(vctPolyPos, 3);

		if (!m_CollShape)
		{
			const C_AABB2 & aabb = m_Frame->GetAABB();

			const C_Vector2 & vctLT = aabb.GetMin();
			const C_Vector2 & vctRB = aabb.GetMax();
			C_Vector2 vctLB(vctLT.m_x, vctRB.m_y);
			C_Vector2 vctRT(vctRB.m_x, vctLT.m_y);

			T_DebugDrawTool::GetInstance().DrawLine(vctLT, vctLB, dbgColor);
			T_DebugDrawTool::GetInstance().DrawLine(vctLB, vctRB, dbgColor);
			T_DebugDrawTool::GetInstance().DrawLine(vctRB, vctRT, dbgColor);
			T_DebugDrawTool::GetInstance().DrawLine(vctRT, vctLT, dbgColor);

			return;
		}

		u32 nCount = m_CollShape->GetVertexCount();
		if (!nCount)
			return;


		C_Matrix3x3 trMatrixRotPivot;	//rotation matrix around ship pivot
		trMatrixRotPivot.SetRotation(GetRotation());
		trMatrixRotPivot.SetTranslation(vctPolyPos);

		C_Matrix3x3 trMatrixShapePivotOffset;	//translation offset to pivot of polyShape
		trMatrixShapePivotOffset.SetTranslation(m_PolyLocalPos);

		C_Matrix3x3 resTrMat = trMatrixRotPivot * trMatrixShapePivotOffset;


		for (u32 i = 0; i < nCount; ++i)
		{
			const T_Vector2 & vct1 = m_CollShape->GetVertex(i);
			const T_Vector2 & vct2 = (i != nCount-1) ? m_CollShape->GetVertex(i+1) : m_CollShape->GetVertex(0);

			C_Vector2 vctStart(vct1.x, vct1.y);		//	vctStart += m_PolyLocalPos;	now there is matrix mult (actor pivot rotation/trsnalsation * pivot of shape translation)
			C_Vector2 vctEnd(vct2.x, vct2.y);		//	vctEnd += m_PolyLocalPos;
			vctStart *= resTrMat;
			vctEnd *= resTrMat;

			T_DebugDrawTool::GetInstance().DrawLine(vctStart, vctEnd, dbgColor);
		}

		return;
	}
}}