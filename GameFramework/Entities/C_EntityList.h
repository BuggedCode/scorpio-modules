/*
* filename:			C_EntityList.h
*
* author:			Kral Jozef
* date:				01/12/2011		0:56
* version:			1.00
* brief:
*/

#pragma once

#include "I_Entity.h"
#include <map>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_EntityList
	class GAMEFRAMEWORK_API C_EntityList
	{
	public:
		//@ Add
		void							Add(T_Entity entity);
		//@ Remove
		virtual void				Remove(T_Entity entity);
		//@ GetEntity
		T_Entity						GetEntity(const sysmath::C_Vector2 & inPos);
		//@ Update
		virtual void				Update(u32 tickDelta);
		//@ Clear
		void							Clear();
		//@ GetCount
		u32							GetCount() const { return (u32)m_container.size(); }

	protected:
		//@ use spatial optimization
		typedef std::map<sysutils::C_HashName, T_Entity>	T_EntityList;
		T_EntityList	m_container;
	};

}}