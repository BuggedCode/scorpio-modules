/*
* filename:			C_BaseEntity.h
*
* author:			Kral Jozef
* date:				07/22/2009		18:50
* version:			1.00
* brief:				Common base entity class
*/

#pragma once

#include <GameFramework/Entities/I_Entity.h>
#include <GameFramework/Entities/I_EntityCreator.h>
#include <SysCore/Frames/C_Visual.h>
#include <SysMath/MathUtils.h>
#include <GameFramework/Operators/I_BaseOperator.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_BaseEntity
	class GAMEFRAMEWORK_API C_BaseEntity : public I_Entity
	{
		typedef I_Entity	T_Super;
	public:
		//@ c-tor
		C_BaseEntity(const sysutils::C_String & name)
			: I_Entity(name) {}
		//@ d-tor
		~C_BaseEntity() {};


		//@ SetFrame
		virtual void					SetFrame(syscore::T_Frame frame);
		//@ GetFrame
		virtual const syscore::T_Frame GetFrame() const { return m_Frame; }
		//@ Update
		virtual void					Update(u32 inDeltaTime);
		//@ AddToRender
		virtual void					AddToRender(u32 renderLayer) { SC_ASSERT(false); } 
		//@ Contains
		virtual bool					Contains(const sysmath::C_Vector2 & inPos) const { return m_Frame->Contains(inPos); }	//m_Frame - CAN NOT BE NULL!

		//////////////////////////////////////////////////////////////////////////
		//@ SET/GET FUNCTIONS
		virtual void					SetVisible(bool bEnable) { m_Frame->SetVisible(bEnable); }
		virtual bool					IsVisible() const { return m_Frame->IsVisible(); }
		virtual void					SetPosition(const sysmath::C_Vector2 & vctPos);
		virtual void					SetPosition(float inPosX, float inPosY);
		virtual const sysmath::C_Vector2&	GetPosition() const { return m_Frame->GetPosition(); }
		virtual void					SetScale(float fScale) { m_Frame->SetScale(fScale); OnChangedPRS(); }
		virtual float					GetScale() const { return m_Frame->GetScale(); }
		virtual void					SetRotation(float fAngle) { m_Frame->SetRotation(fAngle); OnChangedPRS(); }
		virtual float					GetRotation() const { return m_Frame->GetRotation(); }

		virtual void					SetColor(const sysmath::C_Vector4 & color);
		virtual void					SetAlpha(float fAlpha);
		virtual const sysmath::C_Vector4		&	GetColor() const { return m_Frame->GetColor(); }

		//@ SetSize
		virtual void						SetSize(u32 inWidth, u32 inHeight) { m_Frame->SetSize(inWidth, inHeight); }
		//@ GetSize
		virtual const sysmath::C_Point	&	GetSize() const { return m_Frame->GetSize(); }
		//@ GetWidth
		virtual u32							GetWidth() const { return m_Frame->GetSize().x; }
		//@ GetHeight
		virtual u32							GetHeight() const { return m_Frame->GetSize().y; }

		//@ GetPivot = GetCenter
		//virtual sysmath::C_Vector2		GetPivot() const;
		//@ AddAnimation
		virtual void						AddAnimation(syscore::T_AnimationTrack anim);
		//@ NoteTrackNotify
		virtual void						NoteTrackNotify(T_Hash32 eventId, T_Hash32 param1, T_Hash32 param2);

		DECLARE_RTTI


	protected:
		//@ OnChangedPRS - Notification callback callde when enitty/frame  PRS changed to children  can override and reset other frames positions!
		virtual void						OnChangedPRS() {};


	public:
		static sysutils::C_HashName		C_ENTITY_TYPENAME;

	protected:
		//@ copy c-tor
		C_BaseEntity(const C_BaseEntity & copy);

		syscore::T_Frame	m_Frame;			//engine frame
		T_Operator			m_animCtrl;		//for now only 1 controller, letter more
	};
}}