/*
* filename:			C_BaseEntity.cpp
*
* author:			Kral Jozef
* date:				07/22/2009		23:20
* version:			1.00
* brief:
*/

#include "C_BaseEntity.h"
#include <SysMath/C_Rect.h>
#include <SysCore/Core/C_RenderManager.h>
#include <SysUtils/Xml/C_XmlNode.h>
#include <SysUtils/C_TraceClient.h>
#include <SysCore/Core/C_TextureManager.h>
#include <GameFramework/Controllers/C_AnimController.h>
#include <SysAudio/C_AudioManager.h>
#include <GameFramework/Core/C_EventManager.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace sysmath;
	using namespace sysutils;
	using namespace syscore;
	using namespace framework;
	using namespace sysaudio;

	DEFINE_RTTI(C_BaseEntity, I_Entity)

	C_HashName C_BaseEntity::C_ENTITY_TYPENAME = C_HashName("BASE");


	//////////////////////////////////////////////////////////////////////////
	//@ copy c-tor
	//////////////////////////////////////////////////////////////////////////
	C_BaseEntity::C_BaseEntity(const C_BaseEntity & copy)
		: T_Super(copy)
	{
		//m_Frame = T_Visual(new C_Visual(*copy.m_Frame.get()));
		m_Frame = copy.m_Frame->Clone();
		if (copy.m_animCtrl)
		{
			T_EntityAnimController animCtrl = boost::dynamic_pointer_cast<C_AnimController<I_Entity> >(copy.m_animCtrl);
			m_animCtrl = T_Operator(new C_AnimController<I_Entity>(animCtrl->GetAnimTrack(), *this, NULL));
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetFrame
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BaseEntity::SetFrame(T_Frame frame)
	{
		m_hashName = frame->GetHashName();
		m_Frame = frame;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ AddToRender
	//////////////////////////////////////////////////////////////////////////
	/*void C_BaseEntity::AddToRender(u32 renderLayer)
	{
		T_RenderManager::GetInstance().Add(m_Frame, renderLayer);
	}*/


	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetPosition
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BaseEntity::SetPosition(const C_Vector2 & vctPos)
	{
		m_Frame->SetPosition(vctPos);
		OnChangedPRS();
	}

	void C_BaseEntity::SetPosition(float inPosX, float inPosY)
	{
		m_Frame->SetPosition(C_Vector2(inPosX, inPosY));
		OnChangedPRS();
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ SetColor
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BaseEntity::SetColor(const scmath::C_Vector4 & color)
	{
		m_Frame->SetColor(color);
	}
	void C_BaseEntity::SetAlpha(float fAlpha)
	{
		m_Frame->GetColor().m_w = fAlpha;
	}


	/*
	//////////////////////////////////////////////////////////////////////////
	//
	// NOTIFICATIONS
	//
	//////////////////////////////////////////////////////////////////////////
	void C_Entity::NotifyOperatorFinish(I_BaseOperator * callerOp)
	{
		class C_FindFunctor
		{
		public:
			C_FindFunctor(I_BaseOperator * callerOp) : m_callerOp(callerOp) {};
			bool operator()(const T_Operator & op)
			{
				if (op.get() == m_callerOp)
					return true;
				return false;
			}
			//bool op
		private:
			I_BaseOperator * m_callerOp;
		};

		C_FindFunctor	ftor(callerOp);
		T_OperatorSet::iterator iter = std::find_if(m_operators.begin(), m_operators.end(), ftor);
		if (iter != m_operators.end())
		{
			//mozem ho zrusit alebo opat zaktivovat
			m_operators.erase(iter);
			return;
		}

		SC_ASSERT(false && "Unknown operator!");
	}*/


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Update
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BaseEntity::Update(u32 inDeltaTime)
	{
		if (m_animCtrl)
		{
			m_animCtrl->Update(inDeltaTime);
		}
	}



	//////////////////////////////////////////////////////////////////////////
	//
	//@ AddAnimation
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BaseEntity::AddAnimation(T_AnimationTrack anim)
	{
		m_animCtrl = T_Operator(new C_AnimController<I_Entity>(anim, *this, NULL));
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ NoteTrackNotify
	//
	//////////////////////////////////////////////////////////////////////////
	void C_BaseEntity::NoteTrackNotify(T_Hash32 eventId, T_Hash32 param1, T_Hash32 param2)
	{
		T_EventManager::GetInstance().ExecuteEvent(eventId, param1, param2);
	}
}}