/*
* filename:			I_EntityCreator.h
*
* author:			Kral Jozef
* date:				10/12/2010		20:41
* version:			1.00
* brief:
*/

#pragma once

#include "I_Entity.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ I_EntityCreator
	class I_EntityCreator
	{
	public:
		//@ d-tor
		virtual ~I_EntityCreator() {};

		//@ CreateEntity
		virtual T_Entity			CreateEntity() const = 0;

		//@ GetRTTI
		virtual const C_RTTI	&	GetRTTI() const = 0;
	};


	typedef boost::shared_ptr<I_EntityCreator>		T_EntityCreator;

}}