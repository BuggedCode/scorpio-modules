/*
* filename:			C_GameFramework.h
*
* author:			Kral Jozef
* date:				05/23/2009		16:41
* version:			1.00
* brief:				Initialisation part for module GameFramework
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <SysUtils/C_String.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_GameFramework
	class GAMEFRAMEWORK_API C_GameFramework
	{
	public:
		//@ Initialize module
		static bool			Initialize();

		//@ Deinitialize module
		static bool			Deinitialize();
	};

}}