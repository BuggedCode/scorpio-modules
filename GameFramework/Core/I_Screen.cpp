/*
* filename:			I_Screen.cpp
*
* author:			Kral Jozef
* date:				05/17/2009		17:05
* version:			1.00
* brief:
*/

#include "I_Screen.h"
#include <SysUtils/Xml/C_XmlFile.h>
#include <SysUtils/C_TraceClient.h>
#include <GameFramework/gui/C_WidgetFactory.h>
#include <SysCore/Core/C_TextureManager.h>
#include <GameFramework/Operators/C_FadeOperator.h>
#include <GameFramework/Core/C_TickModulesManager.h>
#include <GameFramework/C_GameFramework.h>
#include <SysUtils/Utils/C_StringUtils.h>
#include <SysUtils/FileSystem/C_FileSystemManager.h>
#include <SysCore/Core/C_AnimationManager.h>
#include <SysUtils/Xml/C_XmlDocument.h>
#include <GameFramework/Controllers/C_AnimController.h>
#include <SysCore/Helpers/C_ScgLoader.h>
#include <SysCore/Framework/FrameworkTypes.h>
#include <SysAudio/C_AudioManager.h>
#include <GameFramework/Operators/C_VolumeFader.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	DEFINE_ROOT_RTTI(I_Screen)

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::syscore;
	using namespace scorpio::sysaudio;

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	I_Screen::I_Screen(const C_HashName & screenID, u16 screenWidth, u16 screenHeight, u32 renderLayer)
		 : m_screenID(screenID), m_width(screenWidth), m_height(screenHeight), m_bFinished(false), m_active(true), 
		   m_fadeScreenColor(0.f, 0.f, 0.f, 1.f), m_scene(renderLayer)
	{
		m_CurrMusicVolume = 0.f;	//just fro C_VolumeFader
		m_FadeInOutStep = 1.f;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ InitScreenFromXml
	bool I_Screen::InitScreenFromXml(const T_Stream inputStream, const char * screenName, I_WidgetListener * widgetListener)
	{
		C_XmlFile file;
		bool bRes = file.Load(inputStream);
		if (!bRes)
			return false;

		//parse xml
		if (!file.IsValid())
			return false;

		const C_XmlNode * root = file.GetRoot();
		if (!root || !root->IsValid())
			return false;

		if (root->GetName() != "screen_definition")
			return false;

		C_XmlNode::const_child_iterator it_begin = root->BeginChild("screen");
		C_XmlNode::const_child_iterator it_end	= root->EndChild("screen");

		//@ hladam moju spravnu nodu iterovanim
		const C_XmlNode * screenNode = NULL;
		for (; it_begin != it_end; ++it_begin)
		{
			const C_XmlNode & tempNode = *it_begin;
			if (!tempNode.IsValid())
				continue;

			const C_XmlAttribute * screenNameAtt = tempNode.GetAttribute("name");
			if (!screenNameAtt)
			{
				TRACE_E("screen node has no defined 'name' attribute!")
				continue;
			}

			const C_String tmpScreenName = screenNameAtt->GetValue();
			if (tmpScreenName == screenName)
			{
				screenNode = &tempNode;
				break;
			}
		}

		if (!screenNode)
		{
			TRACE_FE("Couldn't find Screen Node with name %s", screenName);
			return false;
		}

		if (!screenNode->IsValid())
		{
			TRACE_FE("Node %s is corrupted", screenName);
			return false;
		}

		//////////////////////////////////////////////////////////////////////////
		//@ Init Music track
		//////////////////////////////////////////////////////////////////////////
		const C_XmlNode * musicNode = screenNode->GetNode("Music");
		if (musicNode)
		{
			C_String strMusicFileName = (*musicNode)["file"];
			C_String strMusicID = (*musicNode)["uid"];
			if (!strMusicFileName.empty() && !strMusicID.empty())
			{
				m_Music = T_Music( new C_Music(C_HashName(strMusicID).GetHash()));
				T_Stream audioStream = T_FileSystemManager::GetInstance().Open(strMusicFileName);
				if (m_Music->Load(audioStream, true))	//loop
				{
					//@ first need to set VolumeMultiplier!! after add into manager -> volume will be set correctly multiplier * global music volume form manager
					C_String volMult = (*musicNode)["volMult"];
					if (!volMult.empty())
					{
						m_Music->SetVolumeMult(volMult.ToFloat());
					}

					bool bRes = T_AudioManager::GetInstance().AddMusic(m_Music);	//music has to be in manager because of volume control
					if (bRes)
					{
						m_MusicVolFadeOp = T_Operator(new C_VolumeFader(m_CurrMusicVolume, this, T_AudioManager::GetInstance().GetMusicVolume()));	//just for one step, in case there is no defined fadeOut/fadeIn steps in gui.xml for screen, if there are! - step will be replaced
					}
					else
					{
						//@ delete music
						m_Music.reset();
						//m_Music = T_AudioManager::GetInstance().GetMusic(m_Music->GetID());	//delete music and replace it! NO IT IS CONFUSING
					}
				}
				else
				{
					//if load fail reset Music Ptr
					m_Music.reset();
				}
			}
		}

		C_ScgLoader loader;
		//////////////////////////////////////////////////////////////////////////
		//@ parse mesh def file
		const C_XmlNode * meshFileNode = screenNode->GetNode("Meshes");
		if (meshFileNode && meshFileNode->IsValid())
		{
			const C_XmlAttribute * att = meshFileNode->GetAttribute("file");
			if (att)
			{
				TRACE_FI("Screen: %s - parsing mesh data file %s", screenName, att->GetValue().c_str());
				C_String relPath = att->GetValue();
				T_Stream stream = T_FileSystemManager::GetInstance().Open(relPath);
				if (stream)
				{
					bool bLoadRes = loader.Load(stream);	//load meshes into mesh manager and create frames(frames we dont need now)
					TRACE_FU(bLoadRes, "MeshSet: %s load %s!", relPath.c_str(), bLoadRes ? "SUCCESFULL" : "FAILED");
				}
				else
				{
					TRACE_FE("File for MeshSet: %s DOESN'T EXISTS!", relPath.c_str(), "FAILED");
				}
			}
			else
			{
				TRACE_FE("Screen: %s - 'Meshes' node attribute Problem", screenName);
			}
		}

		//@ ANIM
		T_XmlNodeList animList = screenNode->SelectNodes("Anims");
		if (animList)
		{
			for (T_ConstXmlNodePtrVector::iterator iter = animList->begin(); iter != animList->end(); ++iter)
			{
				//load adequate animation set
				C_String animFile = (**iter)["file"];
				T_AnimationManager::GetInstance().AddSetIntoQueue(animFile);
				bool bLoadRes = T_AnimationManager::GetInstance().LoadAllAnimationSetFromQueue();
				TRACE_FU(bLoadRes, "Animation file: %s load %s!", animFile.c_str(), bLoadRes ? "SUCCESFULL" : "FAILED");
			}
		}

		//////////////////////////////////////////////////////////////////////////
		//@ FADEIN & FADEOUT PARAMETER - optional
		const C_XmlNode * tmpNode = screenNode->GetNode("fade_in_out_step");
		if (tmpNode && tmpNode->IsValid())
		{
			if (!tmpNode->Read(m_FadeInOutStep))
			{
				TRACE_FE("FadeIn-step broken in file: '%s'", inputStream->GetFileName());
				return false;
			}

			m_fadeInOp = T_Operator(new C_FadeOperator(m_fadeScreenColor.m_w, this, -m_FadeInOutStep));

			float fMusVal = T_AudioManager::GetInstance().GetMusicVolume();
			if (m_MusicVolFadeOp)
			{
				boost::shared_ptr<C_VolumeFader> volfad = boost::dynamic_pointer_cast<C_VolumeFader>(m_MusicVolFadeOp);
				volfad->SetStep(fMusVal * m_FadeInOutStep);
			}

			m_fadeOutOp = T_Operator(new C_FadeOperator(m_fadeScreenColor.m_w, this, m_FadeInOutStep));
		}

		//@ pokud je fading aktivovany
		if (m_fadeInOp || m_fadeOutOp)
			m_fadeScreen = T_Graphics(new sysrender::C_Graphic());


		//load of widgets
		it_begin = screenNode->BeginChild("widget");
		it_end = screenNode->EndChild("widget");

		for (; it_begin != it_end; ++it_begin)
		{
			const C_XmlNode & widgetNode = *it_begin;
			if (!widgetNode.IsValid())
				continue;

			const C_XmlAttribute * wTypeAtt = widgetNode.GetAttribute("type");
			SC_ASSERT(wTypeAtt);
			if (!wTypeAtt)
			{
				TRACE_E("Missing attribute of widget 'type'");
				continue;
			}

			S_WidgetDescriptor desc(widgetListener);
			desc.m_typeName = wTypeAtt->GetValue();

			C_HashName widgetUIDhash;
			wTypeAtt = widgetNode.GetAttribute("uid");
			if (wTypeAtt)
			{
				C_String str = wTypeAtt->GetValue();
				widgetUIDhash = str;
			}

			desc.m_hashNameUID = widgetUIDhash;

			T_Widget widget = C_WidgetFactory::GetInstance().CreateWidget(desc);
			if (!widget)
			{
				TRACE_FE("Unknown type of widget: %s", desc.m_typeName.c_str());
				continue;
			}

			//@ Important part - if there is in meshManager definition for widget - init widget from Mesh otherwise from xmlNode
			T_Frame frame = loader.GetFrame(widget->GetUID().GetHash());
			bool bResInit = true;
			if (frame)
			{
				bResInit &= widget->InitFromFrame(frame);
			}

			//load widget properties - even if widget is inicialized from mesh
			bResInit &= widget->InitFromXmlNode(widgetNode);

			if (bResInit)
			{
				bool bResAdd = m_widgetManager.Add(widget);
				if (!bResAdd)
					TRACE_FE("Can not add widget of type: %s into widgetManager", desc.m_typeName.c_str());
			}
			else
			{
				TRACE_FE("Can not initialize widget of type: %s from xml/mesh", desc.m_typeName.c_str());
			}
		}
		
		
		//@ load SceneList if any
		tmpNode = screenNode->GetNode("SceneList");
		if (tmpNode && tmpNode->IsValid())
		{
			bRes &= InitFromSceneList((*tmpNode)["file"], loader);
		}

		return bRes;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ InitFromSceneList
	//
	//////////////////////////////////////////////////////////////////////////
	bool I_Screen::InitFromSceneList(const char * fileName, C_ScgLoader & loader)
	{
      T_Stream stream = T_FileSystemManager::GetInstance().Open(fileName);
		C_XmlDocument xmlDoc;
		bool bRes = xmlDoc.Load(stream);
		if (!bRes)
			return false;

		const C_XmlNode * sceneList = xmlDoc.SelectNode("SceneList");
		if (!sceneList)
		{
			TRACE_FE("Can not find SceneList node in file: %s", fileName);
			return false;
		}

		//@ FRAMES
		T_XmlNodeList nodeList = sceneList->SelectNodes("Frame");
		for (T_ConstXmlNodePtrVector::iterator iter = nodeList->begin(); iter != nodeList->end(); ++iter)
		{
			const C_XmlNode * node = *iter;
			C_String name = (*node)["name"];

			T_Frame frame = loader.GetFrame(C_HashName(name));
			if (!frame)
			{
				TRACE_FE("Can not find frame: %s in loader.", name.c_str());
				continue;
			}

			m_scene.Add(frame);

			//@ find animation
			C_String animPath = (*node)["animation"];
			T_AnimationTrack anim;
			if (!animPath.empty())
			{
				anim = T_AnimationManager::GetInstance().GetAnimation(frame->GetHashName());
				if (!anim)
				{
					TRACE_FE("Can not load animation file: %s", animPath.c_str());
					continue;
				}
			}

			OnAddFrameIntoScene(frame, anim);
		}
		

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//@ Render
	//////////////////////////////////////////////////////////////////////////
	void I_Screen::Render(I_TransformationCallback & callback) const
	{
		m_widgetManager.Draw(callback);
		m_windowManager.Draw(callback);
		DrawFader();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ DebugDraw
	//
	//////////////////////////////////////////////////////////////////////////
#ifndef MASTER
	void I_Screen::DebugDraw(I_TransformationCallback & callback) const
	{
		m_widgetManager.DebugDraw(callback);
	}
#endif



	//////////////////////////////////////////////////////////////////////////
	//@ Update
	//////////////////////////////////////////////////////////////////////////
	void I_Screen::Update(u32 tickDelta)
	{
		m_entityList.Update(tickDelta);

		if (m_active)
			m_widgetManager.Update(tickDelta);

		if (m_fadeInOp)
			m_fadeInOp->Update(tickDelta);
		if (m_fadeOutOp)
			m_fadeOutOp->Update(tickDelta);
		if (m_MusicVolFadeOp)
			m_MusicVolFadeOp->Update(tickDelta);
	}



	//////////////////////////////////////////////////////////////////////////
	//@ DrawFader
	//////////////////////////////////////////////////////////////////////////
	void I_Screen::DrawFader() const
	{
		if (m_fadeScreen)
			m_fadeScreen->RenderRect(C_Vector2(m_width/2.f, m_height/2.f), C_Point(m_width, m_height), m_fadeScreenColor, true, 0.f, 1.f);
	}




	//////////////////////////////////////////////////////////////////////////
	//@ ProcessEvents
	//////////////////////////////////////////////////////////////////////////
	bool I_Screen::ProcessEvents(T_WindowEvent & event)
	{
		if (!m_active)
			return true;

		switch(event.type)
		{
			//////////////////////////////////////////////////////////////////////////
			//@ MOUSE EVENTS
		case MOUSE_MOTION_EVENT:
			OnMouseMove(event.motion.x, event.motion.y);
			m_widgetManager.MouseMove(event.motion.x, event.motion.y);
			break;

		case MOUSE_BUTTON_DOWN_EVENT:
			{
				switch(event.button.button)
				{
				case MOUSE_WHEEL_UP:
					OnWheelUp();
					break;

				case MOUSE_WHEEL_DOWN:
					OnWheelDown();
					break;

				default:
					{
						if (event.button.button == LEFT_MOUSE_BUTTON)
						{
							OnLMButtonDown(event.motion.x, event.motion.y);
							m_widgetManager.MouseDown(event.motion.x, event.motion.y, event.button.button);
						}
						else if (event.button.button == RIGHT_MOUSE_BUTTON)
						{
							OnRMButtonDown(event.motion.x, event.motion.y);
						}
					}
				};	//switch
			}
			break;

		case MOUSE_BUTTON_UP_EVENT:
			if (event.button.button == LEFT_MOUSE_BUTTON)
			{
				OnLMButtonUp(event.motion.x, event.motion.y);
				m_widgetManager.MouseUp(event.motion.x, event.motion.y, event.button.button);
			}
			else if (event.button.button == RIGHT_MOUSE_BUTTON)
			{
				OnRMButtonUp(event.motion.x, event.motion.y);
			}
			break;

		case KEY_DOWN_EVENT:
			//widget manageru sa to bude posielat len ak ma nejaky widget focus - editbox
			OnKeyDown(event.key.keysym.sym);
			m_widgetManager.OnKeyDown(event.key.keysym.sym);	//Esc z dialogov/editBoxy
			break;

		case KEY_UP_EVENT:
			OnKeyUp(event.key.keysym.sym);
			m_widgetManager.OnKeyUp(event.key.keysym.sym);		//Esc z dialogov/editBoxy
			break;

		};	//switch

		return true;
	}



	//////////////////////////////////////////////////////////////////////////
	//@ NotifyOperatorFinish
	//////////////////////////////////////////////////////////////////////////
	void I_Screen::NotifyOperatorFinish(I_BaseOperator * callerOp)
	{
		//////////////////////////////////////////////////////////////////////////
		if (callerOp == m_fadeInOp.get())
		{
			OnFadeInFinish();//fade in skoncil screen je aktivni
			T_TickModuleManager::GetInstance().CallEvent(E_TME_FADEIN_FINISH);	//send message to all ticked modules
		}
		else if (callerOp == m_fadeOutOp.get())
		{
			OnFadeOutFinish();//fade out skoncil screen by mal byt finished -> true
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ StartFadeOutScreen
	//
	//////////////////////////////////////////////////////////////////////////
	void I_Screen::StartFadeOutScreen()
	{
		//@ JK mozno zakomentujem!
		SC_ASSERT(m_active);	//scren nie je aktovny
		SC_ASSERT(!m_fadeInOp->IsActive());	//faderIn je aktivny!
		m_active = false;
		m_fadeOutOp->Activate();
		if (m_MusicVolFadeOp)
		{
			m_CurrMusicVolume = T_AudioManager::GetInstance().GetMusicVolume();
			m_StoredMasterMusicVolume = m_CurrMusicVolume;
			//@ update step, maybe master volume was changed, need to update CurrentState in operator & end state & step
			m_MusicVolFadeOp = T_Operator( new C_VolumeFader(m_CurrMusicVolume, this, m_CurrMusicVolume * m_FadeInOutStep * -1.f));
			m_MusicVolFadeOp->Activate();
		}

		//T_TickModuleManager::GetInstance().CallEvent(E_TME_FADEOUT_START);	//send message to all ticked modules
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ StartFadeIn
	//
	//////////////////////////////////////////////////////////////////////////
	void I_Screen::StartFadeInScreen()
	{
		m_active = false;
		m_fadeInOp->Activate();

		if (m_MusicVolFadeOp)
		{
			m_MusicVolFadeOp->Activate();
			//@ update step only when fadeOut occured
			boost::shared_ptr<C_VolumeFader> volfad = boost::dynamic_pointer_cast<C_VolumeFader>(m_MusicVolFadeOp);
			if (volfad->GetStep() < 0.f)
			{
				C_VolumeFader * volFader = new C_VolumeFader(m_CurrMusicVolume, this, m_StoredMasterMusicVolume * volfad->GetStep() * -1.f);
				m_MusicVolFadeOp = T_Operator(volFader);
				//@ need to setup EndState, coz VolFader natively works on interval 0-1
				volFader->SetEndState(m_StoredMasterMusicVolume);
				m_MusicVolFadeOp->Activate();
			}
		}
	}


}}