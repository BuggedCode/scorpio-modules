/*
* filename:			C_ParameterSet.h
*
* author:			Kral Jozef
* date:				10/12/2011		18:17
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <SysUtils/C_String.h>
#include <map>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_ParameterSet
	class GAMEFRAMEWORK_API C_ParameterSet
	{
	public:
		//@ Initialize
		void							Initialize(sysutils::C_String strParams);
		//@ GetValue
		sysutils::C_String		GetValue(T_Hash32 key) const;

	private:
		typedef std::map<T_Hash32, sysutils::C_String>	T_ParamMap;
		T_ParamMap					m_Container;

	};
}}
