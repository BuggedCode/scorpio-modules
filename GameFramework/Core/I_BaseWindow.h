/*
* filename:			I_BaseWindow.h
*
* author:			Kral Jozef
* date:				03/18/2009		20:14
* version:			1.00
* brief:				Base interface for window management
*/

#pragma once

#include <Common/Assert.h>
#include <SysMath/C_Rect.h>
#include <SysUtils/C_String.h>
#include <SysCore/Framework/FrameworkTypes.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ I_BaseWindow - it's not clear interface
	class I_BaseWindow
	{
	public:
		/*//@ c-tor
		I_BaseWindow(sysmath::C_Point & inPos, sysmath::C_Point inSize, sysutils::C_String strName, I_BaseWindow * inParent, bool bModal = false)
			: m_bEnabled(true), m_bModal(bModal), m_rect(inPos.x, inPos.y, inSize.x, inSize.y), m_name(strName), m_parent(inParent)
		{
			//
		}*/

		//@ c-tor
		I_BaseWindow(s32 inPosX, s32 inPosY, u32 inWidth, u32 inHeight, sysutils::C_String strName, I_BaseWindow * inParent, bool bModal = false)
			: m_bEnabled(true), m_bModal(bModal), m_rect(inPosX, inPosY, inWidth, inHeight), m_name(strName), m_parent(inParent)
		{


		}

		//@ d-tor
		virtual ~I_BaseWindow() {};

		//@ SetPos
		void						SetPosition(s32 inPosX, s32 inPosY)
		{
			m_rect.MoveTo(inPosX, inPosY);
		}

		//@ SetSize
		void						SetSize(u32 inWidth, u32 inHeight)
		{
			m_rect.m_right = m_rect.m_left + inWidth;
			m_rect.m_bottom = m_rect.m_top + inHeight;
		}

		//@ SetRectangle
		void						SetRectangle(const sysmath::C_Rect & inRect) { m_rect = inRect; }
		//@ SetEnabled
		void						SetEnabled(bool bEnabled) { m_bEnabled = bEnabled; }
		//@ SetModal
		void						SetModal(bool bModal) { m_bModal = bModal; }
		//@ SetParent
		void						SetParent(I_BaseWindow * inParent)
		{
			m_parent = inParent;
			SC_ASSERT(false);	//preparentuj aj childy z old parenta! preparentovanie bude private a friend bude winmanager bude se to volat na nom!
		}


		//@ GetPos
		sysmath::C_Point		GetPosition() const { return sysmath::C_Point(m_rect.m_left, m_rect.m_top); }
		//@ GetWidth
		u32						GetWidth() const { return m_rect.GetWidth(); }
		//@ GetHeight
		u32						GetHeight() const { return m_rect.GetHeight(); }
		//@ GetName
		sysutils::C_String	GetName() const { return m_name; }
		//@ IsEnabled
		bool						IsEnabled() const { return m_bEnabled; }	//cyclic test throught all parents
		//@ IsModal
		bool						IsModal() const { return m_bModal; }
		//@ GetParent
		I_BaseWindow		*	GetParent() const { return m_parent; }

		//////////////////////////////////////////////////////////////////////////

		//@ HandleEvent
		//@ ret bool - if you want skip processing message to others window return false
		virtual bool			HandleEvent(T_WindowEvent & event) { return true; }

		//////////////////////////////////////////////////////////////////////////

		//@ ScreenToClient
		//@ Convert between top-level screen and client coordinates
		void						ScreenToClient(sysmath::C_Point & pPoint) const
		{
			//TODO zatial nepodporujem parentovanie, neprepocitavam hierarchicky
			pPoint.x -= m_rect.m_left;
			pPoint.y -= m_rect.m_top;
		}

		//@ ClientToScreen
		//@ Convert between client and top-level screen coordinates.
		void						ClientToScreen(sysmath::C_Point & pPoint) const
		{
			//TODO zatial nepodporujem parentovanie, neprepocitavam hierarchicky
			pPoint.x += m_rect.m_left;
			pPoint.y += m_rect.m_top;
		}

	//private:
		//////////////////////////////////////////////////////////////////////////
		//@ PRIVATE METHODES - no one should call immediately (private with friend win manager)

		//@ Draw - render contaent of window, draw in window coord space
		virtual void			Draw() = 0;

		//@ DbgDraw - draw debug info - its called after Draw, draw in world coord space
		virtual void			DbgDraw() = 0;


	
	protected:
		bool						m_bEnabled;		//flag indicate if wnd is enabled
		bool						m_bModal;		//flag indicate if wnd is modal
		sysmath::C_Rect		m_rect;			//window coord in screen space
		sysutils::C_String	m_name;

	private:
		I_BaseWindow		*	m_parent;		//parent window
	};

}}
