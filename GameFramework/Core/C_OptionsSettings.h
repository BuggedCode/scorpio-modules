/*
* filename:			C_OptionsSettings.h
*
* author:			Kral Jozef
* date:				06/08/2009		20:27
* version:			1.00
* brief:				structore holds options settings
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_OptionsSettings
	class GAMEFRAMEWORK_API C_OptionsSettings
	{
	public:
		//@ c-tor
		C_OptionsSettings() : m_fMusicVolume(1.f), m_fSoundFXVolume(1.f), m_bFullscreen(false) {};

		float		m_fMusicVolume;	//<0, 1>
		float		m_fSoundFXVolume;	//<0, 1>
		bool		m_bFullscreen;		//<true, false>
	};

}}
