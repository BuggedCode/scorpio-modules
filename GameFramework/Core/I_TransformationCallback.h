/*
* filename:			I_TransformationCallback.h
*
* author:			Kral Jozef
* date:				05/16/2009		10:10
* version:			1.00
* brief:				Window manager camn transform each window to its local space
*/

#pragma once

#include <Common/I_NonCopyAble.h>
#include <SysMath/C_Point.h>
#include <SysMath/C_Vector2.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ I_TransformationCallback
	class I_TransformationCallback : public I_NonCopyAble
	{
	public:
		//@ Translate screen
		virtual void								Translate(s32 translateX, s32 translateY) {};
		//@ AddTranslation
		virtual void								AddTranslation(s32 offsetX, s32 offsetY) {};
		//@ Rotate screen
		virtual void								Rotate(float fAngle) {};
		//@ Scale screen
		virtual void								Scale(float fScale) {};
		//@ AddScale
		virtual void								AddScale(float fScale) {};
		//@ SetDefaultWorldView - set default screen transformation
		virtual void								SetDefaultTransformation() = 0;

		//@ Pair functions
		//@ PushAndTransform
		virtual void								PushAndTransform(const sysmath::C_Vector2 & vctTransl, float fRotAngle, float fScale) {};
		//@ PopTransform
		virtual void								PopTransform() {};
	};
}}
