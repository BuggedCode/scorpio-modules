/*
* filename:			C_WindowManager.cpp
*
* author:			Kral Jozef
* date:				03/18/2009		23:23
* version:			1.00
* brief:
*/

#include "C_WindowManager.h"
#include <SysUtils/C_TraceClient.h>
#include <Common/macros.h>
#include <SysMath/C_Point.h>
#include "I_Screen.h"

namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;

	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	C_WindowManager::~C_WindowManager()
	{
		SC_ASSERT(m_wndMap.empty());
	}

	//////////////////////////////////////////////////////////////////////////
	//@ AddWindow
	bool C_WindowManager::AddWindow(C_Window & window)
	{
		SC_ASSERT(&window && "Attemp to insert invalid window to manager!");

		C_HashName	hashName(window.GetName());
		std::pair<T_WindowMap::iterator, bool>	bRes = m_wndMap.insert(T_WindowMap::value_type(hashName, &window));
		if (!bRes.second)
		{
			TRACE_FE("Couldn't insert window %s into manager!", window.GetName().c_str());
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//@ RemoveWindow
	C_Window * C_WindowManager::RemoveWindow(const C_String wndName)
	{
		C_HashName	hashName(wndName);
		T_WindowMap::iterator iter = m_wndMap.find(hashName);
		if (iter == m_wndMap.end())
		{
			TRACE_FE("Window %s is not in manager!", wndName.c_str());
			return NULL;
		}

		C_Window * retWnd = iter->second;
		m_wndMap.erase(iter);
		return retWnd;
	}

	
	/*//////////////////////////////////////////////////////////////////////////
	//@ Clear
	void C_WindowManager::Clear()
	{
		for (T_WindowMap::iterator it = m_wndMap.begin(); it != m_wndMap.end(); ++it)
		{
			C_Window * wnd = it->second;
			SC_ASSERT(wnd);
			SAFE_DELETE(wnd);
		}

		m_wndMap.clear();
	}*/


	//////////////////////////////////////////////////////////////////////////
	//@ Draw
	void C_WindowManager::Draw(I_TransformationCallback & callback) const
	{
		for (T_WindowMap::const_iterator it = m_wndMap.begin(); it != m_wndMap.end(); ++it)
		{
			C_Window * wnd = it->second;
			SC_ASSERT(wnd);

			if (!wnd->IsEnabled())
				continue;

			C_Point pos = wnd->GetPosition();

			//@ transform to window local coord system + add window translation to main render window translation
			callback.SetDefaultTransformation();
			callback.Translate(pos.x, -pos.y);	//depend on gr. device

			//@ Draw
			wnd->Draw();

			if (m_dbgDrawEnabled)
				wnd->DbgDraw();	//draw in world coord space
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//@ ProcessEvents
	bool C_WindowManager::ProcessEvents(T_WindowEvent & event)
	{
		for (T_WindowMap::iterator it = m_wndMap.begin(); it != m_wndMap.end(); ++it)
		{
			C_Window * wnd = it->second;
			SC_ASSERT(wnd);

			if (!wnd->IsEnabled())
				continue;
			
			if (!wnd->HandleEvent(event))
				return false;
		}

		return true;
	}

}}