/*
* filename:			TickModuleData.h
*
* author:			Kral Jozef
* date:				01/09/2011		8:52
* version:			1.00
* brief:				Base data class used as parameters in I_TickModule Functions
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Common/C_RTTI.h>
//#include "C_PlayerProfile.h"
#include <SysUtils/C_HashName.h>
#include <SysCore/Core/C_Scene.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ I_TickModuleData
	class I_TickModuleData
	{
	public:
		//@ d-tor
		virtual ~I_TickModuleData() {};

		DECLARE_ROOT_RTTI
	};


	//////////////////////////////////////////////////////////////////////////
	//@ C_GameTickData
	class GAMEFRAMEWORK_API C_GameTickData : public I_TickModuleData
	{
	public:
		//@ c-tor
		C_GameTickData() {};

		DECLARE_RTTI

	public:
		u32		m_tickDelta;		//result tick delta
		float		m_multiplicator;	//multiplicator

	};


	//////////////////////////////////////////////////////////////////////////
	// C_GameInitData
	class GAMEFRAMEWORK_API C_GameInitData : public I_TickModuleData
	{
	public:
		//@ c-tor
		C_GameInitData(sysutils::C_HashName levelUID, bool bMiniGame)
			: m_LevelUID(levelUID), m_bMiniGame(bMiniGame), m_Scene(NULL)
		{
			//////////////////////////////////////////////////////////////////////////
		}

		DECLARE_RTTI

	public:
		//C_PlayerProfile		m_PlayerProfile;
		//@ LevelInfo/MiniGameInfo
		sysutils::C_HashName	m_LevelUID;
		bool						m_bMiniGame;
		sysutils::C_String	m_MiniGameFolder;	//platfrom Independent relative Mini Game folder - just in case m_bMiniGame is TRUE
		sysutils::C_String	m_HudDefFileName;	//fileName of hude definition file - relative from exe
		syscore::C_Scene	*	m_Scene;	//scene - optional parameter - for game level
	};

}}

