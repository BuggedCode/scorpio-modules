/*
* filename:			C_PlayerProfiles.cpp
*
* author:			Kral Jozef
* date:				04/19/2011		9:47
* version:			1.00
* brief:
*/

#include "C_PlayerProfiles.h"
#include <Common/PlatformDef.h>
#include <algorithm>

#ifdef PLATFORM_WIN32
	#pragma warning(push)
	#pragma warning(disable : 4267)	//disable warning boost::archive::binary_oarchive oa(ofs);
	#pragma warning(disable : 4996)	//disable warning boost::archive::binary_iarchive oa(ifs);
	#include <boost/archive/binary_oarchive.hpp>
	#include <boost/archive/binary_iarchive.hpp>
#endif
#include <fstream>


namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Add
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PlayerProfiles::Add(T_PlayerProfile newProfile, bool bSetToCurrent)
	{
		m_Profiles.push_back(newProfile);
		if (bSetToCurrent)
			m_CurrProfile = newProfile->GetGuid();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PlayerProfiles::Remove(const T_PlayerProfile profile)
	{
		if (m_CurrProfile == profile->GetGuid())
			m_CurrProfile = C_GUID();	//reset

		T_Profiles::iterator iter = std::find(m_Profiles.begin(), m_Profiles.end(), profile);
		SC_ASSERT(iter != m_Profiles.end());
		m_Profiles.erase(iter);
	}

	class C_ProfileComparer
	{
	public:
		C_ProfileComparer(const C_GUID & profileGuid)
			: m_ProfileGuid(profileGuid) {};

		bool operator()(T_PlayerProfile profile)
		{
			return (profile->GetGuid() == m_ProfileGuid);
		}
	private:
		C_GUID	m_ProfileGuid;
	};


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Remove
	//
	//////////////////////////////////////////////////////////////////////////
	void C_PlayerProfiles::Remove(const sysutils::C_GUID & profileGuid)
	{
		C_ProfileComparer pred(profileGuid);
		T_Profiles::iterator iter = std::find_if(m_Profiles.begin(), m_Profiles.end(), pred);
		SC_ASSERT(iter != m_Profiles.end());
		m_Profiles.erase(iter);

		if (m_CurrProfile == profileGuid)
			m_CurrProfile = C_GUID();	//reset
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetCurrentProfile
	//
	//////////////////////////////////////////////////////////////////////////
	const T_PlayerProfile C_PlayerProfiles::GetCurrentProfile() const
	{
		C_ProfileComparer pred(m_CurrProfile);
		T_Profiles::const_iterator iter = std::find_if(m_Profiles.begin(), m_Profiles.end(), pred);
		SC_ASSERT(iter != m_Profiles.end());
		if (iter == m_Profiles.end())
			return T_PlayerProfile();

		return *iter;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Save
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PlayerProfiles::Save(const char * fileName) const
	{
#ifdef PLATFORM_WIN32
		std::ofstream ofs(fileName);
		ofs << (u32)m_Profiles.size();
		boost::archive::binary_oarchive oa(ofs);

		for (T_Profiles::const_iterator iter = m_Profiles.begin(); iter != m_Profiles.end(); ++iter)
		{
			T_PlayerProfile prof = *iter;
			oa << (*prof);
		}

		oa << m_CurrProfile;
#endif
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ Load
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_PlayerProfiles::Load(const char * fileName)
	{
#ifdef PLATFORM_WIN32
		std::ifstream ifs(fileName);
		if (!ifs.is_open())
			return false;

		u32 nCount = 0;
		ifs >> nCount;
		boost::archive::binary_iarchive ia(ifs);

		for (u32 i = 0; i < nCount; ++i)
		{
			T_PlayerProfile profile = T_PlayerProfile(new C_PlayerProfile());
			ia >> (*profile);
			m_Profiles.push_back(profile);
		}

		ia >> m_CurrProfile;
#endif

		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ serialize
	//
	//////////////////////////////////////////////////////////////////////////
	/*template<class Archive>
	void C_PlayerProfiles::serialize(Archive & ar, const u32 version)
	{
		u8 count = (u8)m_Profiles.size();
		ar & count;
		for (T_Profiles::iterator iter = )
	}*/


#pragma warning(pop)

}}