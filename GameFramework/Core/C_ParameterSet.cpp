/*
* filename:			C_ParameterSet.cpp
*
* author:			Kral Jozef
* date:				10/12/2011		18:17
* version:			1.00
* brief:
*/

#include "C_ParameterSet.h"
#include <SysUtils/C_TraceClient.h>
#include <SysUtils/C_FNVHash.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Initialize
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ParameterSet::Initialize(C_String strParams)
	{
		//@ parse by '|' and after that by ':' and create keys and values
		T_StringVector pairs;
		strParams.Split('|', pairs);
		for(T_StringVector::iterator iter = pairs.begin(); iter != pairs.end(); ++iter)
		{
			T_StringVector keyValue;
			C_String str = (*iter);
			str.Split(':', keyValue);
			if (keyValue.size() != 2)
			{
				TRACE_FE("Wrong parameter: %s", str.c_str());
				continue;
			}

			T_Hash32 keyHash = C_FNVHash::computeHash32(keyValue[0]);
			C_String valueStr = keyValue[1];

			m_Container.insert(T_ParamMap::value_type(keyHash, valueStr));
		}
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GetValue
	//
	//////////////////////////////////////////////////////////////////////////
	C_String C_ParameterSet::GetValue(T_Hash32 key) const
	{
		T_ParamMap::const_iterator iter = m_Container.find(key);
		if (iter != m_Container.end())
			return iter->second;
		
		TRACE_FW("Unknown key of parameter: %d", key);
		//SC_ASSERT(false);
		return C_String();
	}
}}
