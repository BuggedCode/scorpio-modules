/*
* filename:			C_TickModulesManager.h
*
* author:			Kral Jozef
* date:				03/22/2009		21:58
* version:			1.00
* brief:				Manager for tick modules
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Common/types.h>
#include <Common/C_Singleton.h>
#include <vector>
#include "I_TickModule.h"
#include "TickEvents.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_TickModulesManager
	class GAMEFRAMEWORK_API C_TickModulesManager
	{
		friend class C_Singleton<C_TickModulesManager>;
	public:
		//@ Register module
		//@ moduleUID - unique module ID
		bool					RegisterModule(I_TickModule * module);
		//@ Unregister module
		bool					UnregisterModule(const I_TickModule & module);

		//@ Tick
		//@ currentTick - current time in milliseconds
		//void					Tick(u32 deltaTick);
		//@ CallEvent
		void					CallEvent(E_TickModuleEventType eventType, I_TickModuleData * contextData = NULL);	//I_ContextParam

	private:
		//@ c-tor
		C_TickModulesManager() {};
		//@ d-tor
		~C_TickModulesManager() {};


		typedef std::vector<I_TickModule*>		T_ModuleVector;
		T_ModuleVector		m_registeredModules;	//registrovane modulu pre eventy
	};

	typedef C_Singleton<C_TickModulesManager>	T_TickModuleManager;
}}

//@ explicit instatiation
//template <class T> T * C_Singleton<T>::m_instance = NULL;
GAMEFRAMEWORK_EXP_TEMP_INST template class GAMEFRAMEWORK_API C_Singleton<scorpio::framework::C_TickModulesManager>;
