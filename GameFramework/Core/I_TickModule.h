/*
* filename:			I_TickModule.h
*
* author:			Kral Jozef
* date:				03/22/2009		21:54
* version:			1.00
* brief:				Interface for tick module
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Common/I_NonCopyAble.h>
#include <SysUtils/C_HashName.h>
#include "TickModuleData.h"

namespace scorpio{ namespace framework{

#define D_MAX_MODULES_COUNT	20

	//////////////////////////////////////////////////////////////////////////
	//@ I_TickModule
	class GAMEFRAMEWORK_API I_TickModule : public I_NonCopyAble
	{
	public:
		//@ c-tor
		I_TickModule(const sysutils::C_HashName & moduleID);

		//@ d-tor
		virtual ~I_TickModule() {};

		//@ SysytemInitialize == E_TE_SYSTEM_INIT, inicializace applikace
		virtual bool			SystemInit() = 0;
		//@ SysytemDeinitialize == E_TE_SYSTEM_DONE
		virtual bool			SystemDone() = 0;
		//@ Tick == E_TE_TICK
		//@ deltatTick from previous tick in milliseconds
		virtual void			Tick(C_GameTickData & tickData) = 0;
		//@ Pause
		virtual void			Pause(bool bPaused) { m_Paused = bPaused; }

		//@ GameInit == E_TE_GAME_INIT, inicializace levelu
		virtual bool			GameInit(C_GameInitData & initData) = 0;
		//@ GameDone == E_TE_GAME_DONE
		virtual bool			GameDone() = 0;
		// FadeInFinish == E_TME_FADEIN_FINISH
		virtual void			FadeInFinish() {};
		//@ GamePostInit = E_TME_GAME_POST_INIT - post initialization
		virtual void			GamePostInit(C_GameInitData & initData) {};

		//@ GetID
		const sysutils::C_HashName	&	GetID() const { return m_moduleID; }
		//@ IsPaused
		bool						IsPaused() const { return m_Paused; }

	private:
		sysutils::C_HashName	m_moduleID;
		bool						m_Paused;
		
		static u32				m_modulesCount;
		static I_TickModule	*	m_modules[D_MAX_MODULES_COUNT];
	};

}}
