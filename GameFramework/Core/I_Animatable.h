/*
* filename:			I_Animatable.h
*
* author:			Kral Jozef
* date:				9/19/2011		18:57
* version:			1.00
* brief:
*/


#pragma once

#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ I_Animatable
	class I_Animatable
	{
	public:
		//@ SetPositionRel
		virtual void						SetPositionRel(const sysmath::C_Vector2 & vctPos) {};
		//@ SetRotation
		virtual void						SetRotation(float fAngle) {};
		//@ SetScale
		virtual void						SetScale(float fScale) {};
		//@ SetColor
		virtual void						SetAlpha(float fAlpha) {};
		//@ SetVelocity
		virtual void						SetVelocity(float fVelocity) {};
		//@ NoteTrackNotify
		virtual void						NoteTrackNotify(T_Hash32 eventId, T_Hash32 param1, T_Hash32 param2) {};
	};

	typedef boost::shared_ptr<I_Animatable> T_Animatable;

}}
