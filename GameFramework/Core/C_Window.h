/*
* filename:			C_Window.h
*
* author:			Kral Jozef
* date:				03/18/2009		21:44
* version:			1.00
* brief:				C_Window class
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include "I_BaseWindow.h"
#include <boost/shared_ptr.hpp>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_Window
	class GAMEFRAMEWORK_API C_Window : public I_BaseWindow
	{
	public:
		//@ c-tor
		C_Window(u32 inPosX, u32 inPosY, u32 inWidth, u32 inHeight, sysutils::C_String strName, I_BaseWindow * inParent, bool bModal = false);
		//@ d-tor
		~C_Window();

		//@ Draw
		void						Draw();
		//@ DbgDraw
		void						DbgDraw();

	protected:
	};

	typedef boost::shared_ptr<C_Window>		T_Window;

}}
