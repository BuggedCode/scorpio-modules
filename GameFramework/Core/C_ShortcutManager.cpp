/*
* filename:			C_ShortcutManager.cpp
*
* author:			Kral Jozef
* date:				9/19/2011		21:32
* version:			1.00
* brief:
*/

#include "C_ShortcutManager.h"
#include <SysUtils/C_FNVHash.h>
#include <SysUtils/C_TraceClient.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;

	//////////////////////////////////////////////////////////////////////////
	//
	//@ d-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_ShortcutManager::~C_ShortcutManager()
	{
		TRACE_FU(m_ShortcutMap.empty(), "Shortcut manager deinitialization: %s", m_ShortcutMap.empty() ? "OK" : "FAILED");
		SC_ASSERT(m_ShortcutMap.empty());
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ RegisterShortcut
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ShortcutManager::RegisterShortcut(T_Shortcut shortcut)
	{
#ifdef MASTER
		SC_CTASSERT(false);	//need to ifdef shortcut manager
#endif
		T_Hash32 hash = C_ShortcutManager::GenerateHash(shortcut->GetModifier(), shortcut->GetKey());
		std::pair<T_ShortcutMap::iterator, bool> ibRet = m_ShortcutMap.insert(T_ShortcutMap::value_type(hash, shortcut));
		if (!ibRet.second)
		{
			TRACE_FE("Shortcut: %s already registered to: ", shortcut->GetDescription().c_str(), ibRet.first->second->GetDescription().c_str());
			SC_ASSERT(false);	//already registered shortcut!
		}
		return ibRet.second;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ UnregisterShortcut
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_ShortcutManager::UnregisterShortcut(E_KeyboardModifier modifier, E_KeyboardLayout key)
	{
		T_Hash32 hash = C_ShortcutManager::GenerateHash(modifier, key);
		u32 nCount = (u32)m_ShortcutMap.erase(hash);
		SC_ASSERT(nCount == 1);
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ GenerateHash
	//
	//////////////////////////////////////////////////////////////////////////
	T_Hash32 C_ShortcutManager::GenerateHash(E_KeyboardModifier modifier, E_KeyboardLayout key)
	{
		//@ GetHash
		u64 value = modifier;
		value = value << 32;
		value |= key;
		u32 hash = C_FNVHash::computeHash32(&value, 8);
		return hash;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ ProcessShortcut
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ShortcutManager::ProcessShortcut(E_KeyboardModifier keyModifier, E_KeyboardLayout key)
	{
		T_Hash32 hash = GenerateHash(keyModifier, key);
		T_ShortcutMap::iterator iter = m_ShortcutMap.find(hash);
		if (iter != m_ShortcutMap.end())
			iter->second->Execute();
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ AttachWindow
	//
	//////////////////////////////////////////////////////////////////////////
	void C_ShortcutManager::AttachWindow(T_DebugWindow window)
	{
		m_Window = window;
		SC_ASSERT(m_Window);
		m_Window->AddItem();

		for (T_ShortcutMap::iterator iter = m_ShortcutMap.begin(); iter != m_ShortcutMap.end(); ++iter)
		{
			m_Window->AddText( iter->second->GetDescription() );
		}
	}

}}