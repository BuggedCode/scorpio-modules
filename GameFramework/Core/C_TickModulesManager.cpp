/*
* filename:			C_TickModulesManager.cpp
*
* author:			Kral Jozef
* date:				03/22/2009		22:03
* version:			1.00
* brief:				Manager for tick modules
*/

#include "C_TickModulesManager.h"
#include <Common/Assert.h>
#include <algorithm>
#include <SysUtils/C_TraceClient.h>
#include <functional>
#include <boost/bind.hpp>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ RegisterModule
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_TickModulesManager::RegisterModule(I_TickModule * module)
	{
		SC_ASSERT(module);
		if (!module)
			return false;

		T_ModuleVector::iterator iter = std::find(m_registeredModules.begin(), m_registeredModules.end(), module);
		if (iter != m_registeredModules.end())
		{
			SC_ASSERT(false);
			TRACE_FE("Module %s is already registered!", module->GetID().GetName());
			return false;
		}

		m_registeredModules.push_back(module);
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ UnregisterModule
	//
	//////////////////////////////////////////////////////////////////////////
	bool C_TickModulesManager::UnregisterModule(const I_TickModule & module)
	{
		T_ModuleVector::iterator iter = std::find(m_registeredModules.begin(), m_registeredModules.end(), &module);
		if (iter == m_registeredModules.end())
		{
			TRACE_FE("Couldn't unregister module %s, module is not registered!", module.GetID().GetName());
			return false;
		}

		m_registeredModules.erase(iter);
		return true;
	}


	//////////////////////////////////////////////////////////////////////////
	//
	//@ CallEvent
	//
	//////////////////////////////////////////////////////////////////////////
	void C_TickModulesManager::CallEvent(E_TickModuleEventType eventType, I_TickModuleData * contextData)
	{
		//TODO FOR_EACH
		switch(eventType)
		{
		case E_TME_SYSTEM_INIT:
			std::for_each(m_registeredModules.begin(), m_registeredModules.end(), std::mem_fun(&I_TickModule::SystemInit));
			break;

		case E_TME_SYSTEM_DONE:
			std::for_each(m_registeredModules.begin(), m_registeredModules.end(), std::mem_fun(&I_TickModule::SystemDone));
			break;

		case E_TME_TICK:
			{
				C_GameTickData * gtcData = DynamicCast<C_GameTickData*>(contextData);
				SC_ASSERT(gtcData);
				for (T_ModuleVector::iterator iter = m_registeredModules.begin(); iter != m_registeredModules.end(); ++iter)
				{
					if (!(*iter)->IsPaused())
						(*iter)->Tick(*gtcData);
				}
			}
			break;

		case E_TME_GAME_INIT:
			{
				C_GameInitData * gicData = DynamicCast<C_GameInitData*>(contextData);
				SC_ASSERT(gicData);
				std::for_each(m_registeredModules.begin(), m_registeredModules.end(), boost::bind(&I_TickModule::GameInit, _1, *gicData));
			}
			break;

		case E_TME_GAME_DONE:
			std::for_each(m_registeredModules.begin(), m_registeredModules.end(), std::mem_fun(&I_TickModule::GameDone));
			break;

		case E_TME_FADEIN_FINISH:
			std::for_each(m_registeredModules.begin(), m_registeredModules.end(), std::mem_fun(&I_TickModule::FadeInFinish));
			break;

		case E_TME_GAME_POST_INIT:
			{
				C_GameInitData * gicData = DynamicCast<C_GameInitData*>(contextData);
				SC_ASSERT(gicData);
				std::for_each(m_registeredModules.begin(), m_registeredModules.end(), boost::bind(&I_TickModule::GamePostInit, _1, *gicData));
			}
			break;

		}
		return;
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//@ Tick
	//
	//////////////////////////////////////////////////////////////////////////
	/*void C_TickModulesManager::Tick(u32 deltaTick)
	{
		for (T_ModuleVector::iterator iter = m_registeredModules.begin(); iter != m_registeredModules.end(); ++iter)
		{
			I_TickModule * module = *iter;
			SC_ASSERT(module);
			module->Tick(deltaTick);
		}
	}*/
}}