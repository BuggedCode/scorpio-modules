/*
* filename:			C_InputManager.h
*
* author:			Kral Jozef
* date:				10/20/2011		22:47
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Common/C_Singleton.h>
#include <SysCore/Framework/FrameworkTypes.h>
#include <Common/Assert.h>

namespace scorpio{ namespace framework{

	enum E_KeyInput
	{
		E_KI_UP = 0,
		E_KI_DOWN,
		E_KI_LEFT,
		E_KI_RIGHT,
		E_KI_WEPON_MENU,

		E_KI_LAST,	//MUST BE LAST
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_InputManager
	class GAMEFRAMEWORK_API C_InputManager
	{
		friend class C_Singleton<C_InputManager>;
	public:

		E_KeyboardLayout	operator[](E_KeyInput keyInput) const
		{
			SC_ASSERT(keyInput < E_KI_LAST);
			return m_KeyBindings[keyInput];
		}


	private:
		//@ c-tor
		C_InputManager();
		//@ c-tor
		//C_TextureManager(sysutils::C_String dataDir);
		//@ d-tor
		~C_InputManager() {};

		E_KeyboardLayout	m_KeyBindings[E_KI_LAST];	//primary keys

	};

	typedef C_Singleton<C_InputManager>	T_InputManager;
}}

GAMEFRAMEWORK_EXP_TEMP_INST template class GAMEFRAMEWORK_API C_Singleton<scorpio::framework::C_InputManager>;