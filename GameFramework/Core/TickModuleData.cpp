/*
* filename:			TickModuleData.cpp
*
* author:			Kral Jozef
* date:				01/09/2011		9:02
* version:			1.00
* brief:
*/

#include "TickModuleData.h"

namespace scorpio{ namespace framework{

	DEFINE_ROOT_RTTI(I_TickModuleData);
	DEFINE_RTTI(C_GameTickData, I_TickModuleData);
	DEFINE_RTTI(C_GameInitData, I_TickModuleData);

}}