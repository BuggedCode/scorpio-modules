/*
* filename:			C_InputManager.cpp
*
* author:			Kral Jozef
* date:				10/20/2011		22:48
* version:			1.00
* brief:
*/


#include "C_InputManager.h"


namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//
	//@ c-tor
	//
	//////////////////////////////////////////////////////////////////////////
	C_InputManager::C_InputManager()
	{
		m_KeyBindings[E_KI_UP] = SDLK_w;
		m_KeyBindings[E_KI_DOWN] = SDLK_s;
		m_KeyBindings[E_KI_LEFT] = SDLK_a;
		m_KeyBindings[E_KI_RIGHT] = SDLK_d;

		m_KeyBindings[E_KI_WEPON_MENU] = SDLK_LCTRL;
	}

}}