/*
* filename:			C_Window.cpp
*
* author:			Kral Jozef
* date:				03/18/2009		21:48
* version:			1.00
* brief:
*/

#include "C_Window.h"
#include <Common/macros.h>
#include <SysMath/C_Vector4.h>
#include <SysCore/Render/C_Graphic.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	using namespace scorpio::sysutils;
	using namespace scorpio::sysmath;
	using namespace scorpio::sysrender;

	static const C_Vector4 C_DBG_COLOR(0.8f, 0.8f, 0.1f, 0.8f);

	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	C_Window::C_Window(u32 inPosX, u32 inPosY, u32 inWidth, u32 inHeight, C_String strName, I_BaseWindow * inParent, bool bModal)
		: I_BaseWindow(inPosX, inPosY, inWidth, inHeight, strName, inParent, bModal)
	{
		//////////////////////////////////////////////////////////////////////////
	}

	//////////////////////////////////////////////////////////////////////////
	//@ d-tor
	C_Window::~C_Window()
	{
	}

	//////////////////////////////////////////////////////////////////////////
	//@ Draw
	void C_Window::Draw()
	{
		//blit backgroun
	}

	//////////////////////////////////////////////////////////////////////////
	//@ DbgDraw
	void C_Window::DbgDraw()
	{
		//ccw draw bounding of window
		C_Graphic graph;
		C_Vector2 wndPivot(m_rect.GetWidth() / 2.f, m_rect.GetHeight() / 2.f);
		graph.RenderRect(wndPivot, C_Point(m_rect.GetWidth(), m_rect.GetHeight()), C_DBG_COLOR, false, 0.f, 1.f);
	}
}}