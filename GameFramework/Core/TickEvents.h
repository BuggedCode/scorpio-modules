/*
* filename:			TickEvents.h
*
* author:			Kral Jozef
* date:				12/25/2009		15:09
* version:			1.00
* brief:			Global enum for tick events
*/

#pragma once

namespace scorpio{ namespace framework{

	enum E_TickModuleEventType
	{
		E_TME_NONE = 0,
		E_TME_SYSTEM_INIT,		//inicializace systemu, v nasom pripade C_Application::Initialize
		E_TME_SYSTEM_DONE,		//deiunicializace systemu C_Application::Deinitialize
		E_TME_TICK,					//tick
		E_TME_GAME_INIT,			//inicializace hry, v nasom pripade konkretneho levelu
		E_TME_GAME_POST_INIT,	//Post initialization, needed in some cases
		E_TME_GAME_DONE,			//deinicializace hry

		//E_TME_MINI_GAME_INIT,	//Init MiniGame
		//E_TME_MINI_GAME_DONE,	//Deinit MiniGame

		E_TME_FADEIN_FINISH,		//screen ukoncil fadeIn
		//E_TME_FADEOUT_START,
	};
}}
