/*
* filename:			C_WindowManager.h
*
* author:			Kral Jozef
* date:				03/18/2009		22:16
* version:			1.00
* brief:				logical manager of windows
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Common/I_NonCopyAble.h>
#include <map>
#include "C_Window.h"
#include <SysUtils/C_HashName.h>
#include "I_TransformationCallback.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_WindowManager
	class GAMEFRAMEWORK_API C_WindowManager : public I_NonCopyAble
	{
	public:
		//@ d-tor
		~C_WindowManager();

		//@ AddWindow
		bool					AddWindow(C_Window & window);
		//@ RemoveWindow
		C_Window			*	RemoveWindow(const sysutils::C_String wndName);

		//@ EnableDbgDraw
		void					EnableDbgDraw(bool bEnable) { m_dbgDrawEnabled = bEnable; }
		//@ IsDbgDrawEnabled
		bool					IsDbgDrawEnabled() const { return m_dbgDrawEnabled; }

		//@ Draw - all enabled windows
		//@ param - screen - which we use for local wnd transformations
		void					Draw(I_TransformationCallback & callback) const;

		//@ ProcessEvents
		bool					ProcessEvents(T_WindowEvent & event);

	private:
		typedef std::map<sysutils::C_HashName, C_Window*>	T_WindowMap;	//predelat na smart pointre
		
		T_WindowMap			m_wndMap;
		bool					m_dbgDrawEnabled;		//flag which indicate if debug draw to windows is called
	};

}}