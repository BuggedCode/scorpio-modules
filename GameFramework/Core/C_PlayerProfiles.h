/*
* filename:			C_PlayerProfiles.h
*
* author:			Kral Jozef
* date:				04/19/2011		9:42
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include "C_PlayerProfile.h"

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_PlayerProfiles
	class GAMEFRAMEWORK_API C_PlayerProfiles
	{
	public:
		//@ Add
		void				Add(T_PlayerProfile newProfile, bool bSetToCurrent = false);
		//@ Remove
		void				Remove(const T_PlayerProfile profile);
		//@ Remove
		void				Remove(const sysutils::C_GUID & profileGuid);
		//@ DoForAllProfiles
		//@ Func - functor/visitor enumerate every profile in vector, in retvalue from func is false - skip enumeration
		template<class Func>
		void				DoForAllProfiles(Func & func) const
		{
			for (T_Profiles::const_iterator iter = m_Profiles.begin(); iter != m_Profiles.end(); ++iter)
			{
				if (!func(*iter))
					return;
			}
		}
		//@ SetCurrent
		void				SetCurrent(const sysutils::C_GUID & profileGuid) { m_CurrProfile = profileGuid; }
		//@ GetCurrent
		const sysutils::C_GUID	&	GetCurrent() const { return m_CurrProfile; }
		//@ GetCurrentProfile
		const T_PlayerProfile	GetCurrentProfile() const;
		//@ GetCount
		u8					GetCount() const { return (u8)m_Profiles.size(); }
		//@ Save
		//@ fileName - abs filePath
		bool				Save(const char * fileName) const;
		//@ Load
		//@ fileName - abs filePath
		bool				Load(const char * fileName);

	private:
		typedef std::vector<T_PlayerProfile>	T_Profiles;
		T_Profiles			m_Profiles;
		sysutils::C_GUID	m_CurrProfile;
	};

}}