/*
* filename:			I_TickModule.cpp
*
* author:			Kral Jozef
* date:				12/13/2009		14:54
* version:			1.00
* brief:
*/

#include "I_TickModule.h"
#include <GameFramework/Core/C_TickModulesManager.h>
#include <Common/Assert.h>
#include <SysMemManager/globalNewDelete.h>

namespace scorpio{ namespace framework{

	u32 I_TickModule::m_modulesCount = 0;
	I_TickModule * I_TickModule::m_modules[D_MAX_MODULES_COUNT];


	//////////////////////////////////////////////////////////////////////////
	//@ c-tor
	//////////////////////////////////////////////////////////////////////////
	I_TickModule::I_TickModule(const sysutils::C_HashName & moduleID)
		: m_moduleID(moduleID), m_Paused(false)
	{
		//@ instancia kazdeho tick modulu sa zaregistruje do globalneho zoznamu
		m_modules[m_modulesCount++] = this;
		SC_ASSERT(m_modulesCount < D_MAX_MODULES_COUNT);
	}


}}