/*
* filename:			C_PlayerProfile.h
*
* author:			Kral Jozef
* date:				01/09/2011		10:38
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <SysUtils/C_String.h>
#include <SysUtils/C_GUID.h>
#include <boost/shared_ptr.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>
#include <string>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_PlayerProfile
	class GAMEFRAMEWORK_API C_PlayerProfile
	{
	public:
		//@ c-tor - because serialization
		C_PlayerProfile()	: m_Score(0) {};
		//@ c-tor
		C_PlayerProfile(sysutils::C_String playerName)
			: m_PlayerName(playerName), m_Score(0)
		{
			m_Guid.Generate();
		}

		//@ GetGuid
		const sysutils::C_GUID		GetGuid() const { return m_Guid; }
		//@ operator==
		bool operator== (const C_PlayerProfile & profile) const { return m_Guid == profile.GetGuid(); }
		//@ operator==
		bool operator== (const sysutils::C_GUID & profileGuid) const { return m_Guid == profileGuid; }
		//@ GetName
		sysutils::C_String			GetName() const { return m_PlayerName; }
		//@ GetScore
		u32								GetScore() const { return m_Score; }

		//@ serialize
		friend class boost::serialization::access;
		/*template<class Archive>
		void								serialize(Archive & ar, const u32 version)
		{
			std::string strPlayerName(m_PlayerName.c_str());
			ar & strPlayerName;
			ar & m_Score;
			ar & m_Guid;
		}*/

		//@ save
		template<class Archive>
		void								save(Archive & ar, const u32 version) const
		{
			std::string strPlayerName(m_PlayerName.c_str());
			ar & strPlayerName;
			ar & m_Score;
			ar & m_Guid;
		}
		//@ load
		template<class Archive>
		void								load(Archive & ar, const u32 version)
		{
			std::string strPlayerName;
			ar & strPlayerName;
			ar & m_Score;
			ar & m_Guid;

			m_PlayerName = strPlayerName.c_str();
		}


		BOOST_SERIALIZATION_SPLIT_MEMBER()


	private:
		sysutils::C_String		m_PlayerName;
		u32							m_Score;
		sysutils::C_GUID			m_Guid;
	};


	typedef boost::shared_ptr<C_PlayerProfile>	T_PlayerProfile;

}}