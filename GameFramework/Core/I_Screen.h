/*
* filename:			I_Screen.h
*
* author:			Kral Jozef
* date:				03/30/2009		21:18
* version:			1.00
* brief:				Interface for Screen
*/

#pragma once

#include <Common/I_NonCopyAble.h>
#include <SysCore/Framework/FrameworkTypes.h>
#include <GameFramework/GameFrameworkApi.h>
#include <GameFramework/gui/C_WidgetManager.h>
#include <GameFramework/Operators/I_BaseOperator.h>
#include "C_WindowManager.h"

#include <SysCore/Frames/C_Visual.h>
#include <SysUtils/C_HashName.h>
#include <SysUtils/FileSystem/C_Stream.h>
#include <SysMath/C_Point.h>
#include <GameFramework/Entities/C_EntityList.h>
#include <SysCore/Core/C_Scene.h>
#include <SysCore/Helpers/SceneNodeVisitors.h>
#include <SysAudio/C_Music.h>

namespace scorpio{
	namespace sysutils{
		class C_XmlFile;
	}
	namespace syscore{
		class C_ScgLoader;
	}
}

namespace scorpio{ namespace framework{
	
	typedef boost::shared_ptr<class I_Screen>		T_Screen;

	//////////////////////////////////////////////////////////////////////////
	//@ I_Screen
	class GAMEFRAMEWORK_API I_Screen : public framework::I_OperatorListener
	{
	public:
		//@ c-tor
		I_Screen(const sysutils::C_HashName & screenID, u16 screenWidth, u16 screenHeight, u32 renderLayer);
		//@ d-tor
		virtual ~I_Screen() {};

		//@ GetScreenID
		const sysutils::C_HashName	&	GetScreenID() const { return m_screenID; }

		//@ Render - main render manager call for active scren
		void						Render(I_TransformationCallback & callback) const;

#ifndef MASTER
		//@ DebugDraw
		virtual void			DebugDraw(I_TransformationCallback & callback) const;
#endif


		//@ Update - tick module call for updating active screen
		//@ param tickDelta - in milliseconds
		virtual void			Update(u32 tickDelta);

		//@ process window events - do not virtualize this function!
		bool						ProcessEvents(T_WindowEvent & event);

		//@ Initialize
		virtual bool			Initialize() = 0;

		//@ PopFromTopOfStack - called when this screen is popped from top of the stack or another one is pushed onto top of the screen stack 
		//@ - when screen is leaving top of the stack, basic behaviour is set scene to invisible
		virtual void			PopFromTopOfStack() 
		{
			m_scene.SetVisible(false);
			if (m_Music)
				m_Music->Stop();
		}
		
		//@ PushToTopOfStack - called when this screen is pushed onto top of the screen stack, basic behaviour is set scene to visible
		virtual void			PushToTopOfStack()
		{ 
			m_scene.SetVisible(true);
			if (m_Music)
				m_Music->Play();
		}

		//@ Reset - reset opf input states of widgetManager
		virtual void			Reset() { m_widgetManager.ResetInputStates(); }

		//@ IsFinished
		bool						IsFinished() const { return m_bFinished; }

		//@ PushModal - fe. called from screen manager when AppDeactivated
		void						PushModal(T_Dialog dialog) { m_widgetManager.PushModal(dialog); }
		//@ PopModal - fe. called from screen manager when AppActivated
		T_Dialog					PopModal() { return m_widgetManager.PopModal(); }
		//@ GetReturnWidgetUID
		sysutils::C_HashName	GetReturnWidgetUID() const { return m_ReturnWidgetUID; }

	protected:
		//@ z xml filu natiahne info o screene screenName
		//@ inputStream - xml stream
		bool						InitScreenFromXml(const sysutils::T_Stream inputStream, const char * screenName, I_WidgetListener * widgetListener);

		// virtual input events

		//@ OnMouseMove
		virtual void			OnMouseMove(s32 inMousePosX, s32 inMousePosY) {};
		//@ Left MouseButton down
		virtual void			OnLMButtonDown(s32 inMousePosX, s32 inMousePosY) {};
		//@ Left MouseButton up
		virtual void			OnLMButtonUp(s32 inMousePosX, s32 inMousePosY) {};
		//@ Right MouseButton down
		virtual void			OnRMButtonDown(s32 inMousePosX, s32 inMousePosY) {};
		//@ Right MouseButton up
		virtual void			OnRMButtonUp(s32 inMousePosX, s32 inMousePosY) {};
		//@ OnKeyDown
		virtual void			OnKeyDown(E_KeyboardLayout keyCode) {};
		//@ OnKeyUp
		virtual void			OnKeyUp(E_KeyboardLayout keyCode) {};
		//@ OnWheelUp
		virtual void			OnWheelUp() {};
		//@ OnWheelDown
		virtual void			OnWheelDown() {};

		//@ callbacks
		virtual void			OnAddFrameIntoScene(syscore::T_Frame frame, syscore::T_AnimationTrack anim) {};

		//@ FADING FUNCTIONS

		//@ DrawFader
		void						DrawFader() const;

	public:
		//@ sometimes we want activate screen on stack, in case another screen is popped
		//@ StartFadeInScreen
		void						StartFadeInScreen();

	protected:
		//@ InitFromSceneList
		//@ fileName = platform independend SceneList xml based FileName
		bool						InitFromSceneList(const char * fileName, syscore::C_ScgLoader & loader);

		//@ StartFadeOutScreen
		void						StartFadeOutScreen();

		// OperatorListener interface
		virtual void			NotifyOperatorFinish(framework::I_BaseOperator * callerOp);

		//////////////////////////////////////////////////////////////////////////
		//@ FADE OPERATOR  INTERAFCE
		virtual void			OnFadeInFinish() { m_active = true; }

		virtual void			OnFadeOutFinish()
		{ 
			m_bFinished = true;
		}

		
		DECLARE_ROOT_RTTI

	protected:
		sysutils::C_HashName	m_screenID;

		C_WidgetManager		m_widgetManager;
		C_WindowManager		m_windowManager;

		u16						m_width;				//screen width - mostly application window width
		u16						m_height;			//screen height - mostly application window height

		bool						m_active;			//screen is active - not in fadeIn/fadeOut mode
		bool						m_bFinished;		//screen je ukonceny - mrtvy!

		//@ fading parameters
		T_Graphics				m_fadeScreen;		//fade clona
		sysmath::C_Vector4	m_fadeScreenColor;//color clony
		framework::T_Operator	m_fadeInOp;
		framework::T_Operator	m_fadeOutOp;
		syscore::C_Scene		m_scene;				//scene
		C_EntityList			m_entityList;	//visuals can not be animated - everything in scene which is animated is entity!
		
		sysutils::C_HashName m_ReturnWidgetUID;	//widgetUID which cause Finalization Screen
		sysaudio::T_Music		m_Music;			//music fro this screen - can be NULL, need to be updated for streaming in Update
		framework::T_Operator	m_MusicVolFadeOp;	//music volume fadeIn operator
		//framework::T_Operator	m_MusicVolFadeOutOp;	//music volume fadeOut operator
		float						m_CurrMusicVolume;	//memory storage for actual volume value for C_VolumeFader
		float						m_StoredMasterMusicVolume;	//master Music Volume for Volume fadein, need to remember masterVolume
		float						m_FadeInOutStep;	//fadeIn/fadeOut step form volume/screen fader
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_ScreenCmpFunctor
	class C_ScreenCmpFunctor
	{
	public:
		//@ c-tor
		C_ScreenCmpFunctor(sysutils::C_HashName screenID) : m_screenID(screenID) {};
		
		//@ funcion operaor
		bool operator()(T_Screen screen)
		{
			if (screen->GetScreenID() == m_screenID)
				return true;
			return false;
		}

	private:
		sysutils::C_HashName	m_screenID;
	};

}}
