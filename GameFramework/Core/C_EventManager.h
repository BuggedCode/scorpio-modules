/*
* filename:			C_EventManager.h
*
* author:			Kral Jozef
* date:				05/06/2011		14:21
* version:			1.00
* brief:				Central Place for handling events "for distributing events to correct functions"
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Common/C_Singleton.h>
#include <map>
#include <SysUtils/I_CallbackPointer.h>
#include <SysUtils/C_TraceClient.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ C_EventManager
	class C_EventManager
	{
		friend class C_Singleton<C_EventManager>;

		template<class T>
		class C_EventHandler : public sysutils::I_CallbackPointer
		{
			typedef void (T::*T_MemberFunc)(T_Hash32, T_Hash32);
		public:
			//@ c-tor
			C_EventHandler(T * obj, T_MemberFunc func) : m_Object(obj), m_Function(func) {};


			//@ Execute
			virtual void		Execute(T_Hash32 param1, T_Hash32 param2)
			{
				SC_ASSERT(m_Object);
				(m_Object->*m_Function)(param1, param2);
			}

		private:
			T				*	m_Object;
			T_MemberFunc	m_Function;
		};


	public:
		//@ RegisterEventHandler
		template<class T, typename Func>
		void					RegisterEventHandler(T_Hash32 eventId, T * obj, Func func)
		{
			sysutils::T_CallbackPtr eventHandler = sysutils::T_CallbackPtr(new C_EventHandler<T>(obj, func));
			std::pair<T_EventHandlerMap::iterator, bool> res;
			res = m_EventHandlerMap.insert(T_EventHandlerMap::value_type(eventId, eventHandler));
			if (!res.second)
			{
				TRACE_FW("Trying to register EventHandler for Event: %d, which ALREADY has EventHandler!", eventId);
			}
		}
		//@ UnregisterEventHandler
		void					UnregisterEventHandler(T_Hash32 eventId)
		{
			T_EventHandlerMap::iterator iter = m_EventHandlerMap.find(eventId);
			if (iter != m_EventHandlerMap.end())
			{
				m_EventHandlerMap.erase(iter);
				return;
			}

			TRACE_FW("Unregistering of unknown event: %d", eventId);
		}

		//@ ExecuteEvent
		void					ExecuteEvent(T_Hash32 eventId, T_Hash32 param1, T_Hash32 param2)
		{
			T_EventHandlerMap::iterator iter = m_EventHandlerMap.find(eventId);
			if (iter != m_EventHandlerMap.end())
			{
				iter->second->Execute(param1, param2);
				return;
			}

			TRACE_FW("Executing of unknown Event: %d", eventId);
		}

	private:
		//@ c-tor
		C_EventManager() {};
		//@ copy c-tor
		C_EventManager(const C_EventManager & copy);	//not implemented
		//@ assignment operator
		C_EventManager& operator=(const C_EventManager & copy);	//not implemented
		//@ d-tor
		~C_EventManager() {};


	private:
		typedef std::map<T_Hash32, sysutils::T_CallbackPtr>	T_EventHandlerMap;
		T_EventHandlerMap	m_EventHandlerMap;
	};


	typedef C_Singleton<C_EventManager>	T_EventManager;

}}

GAMEFRAMEWORK_EXP_TEMP_INST template class GAMEFRAMEWORK_API C_Singleton<scorpio::framework::C_EventManager>;