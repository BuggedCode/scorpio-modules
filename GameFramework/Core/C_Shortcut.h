/*
* filename:			C_Shortcut.h
*
* author:			Kral Jozef
* date:				9/19/2011		23:10
* version:			1.00
* brief:
*/

#pragma once

#include <boost/shared_ptr.hpp>
#include <SysCore/Framework/FrameworkTypes.h>
#include <SysUtils/C_String.h>

namespace scorpio{ namespace framework{

	//////////////////////////////////////////////////////////////////////////
	//@ I_Shortcut
	class I_Shortcut
	{
	public:
		//@ c-tor
		I_Shortcut(sysutils::C_String description, E_KeyboardModifier modifier, E_KeyboardLayout key)
			: m_Description(description), m_Modifier(modifier), m_Key(key) {};

		//@ Execute
		virtual void			Execute() = 0;

		E_KeyboardModifier		GetModifier() const { return m_Modifier; }
		E_KeyboardLayout			GetKey() const { return m_Key; }
		sysutils::C_String		GetDescription() const { return m_Description; }

	private:
		E_KeyboardModifier		m_Modifier;
		sysutils::C_String		m_Description;
		E_KeyboardLayout			m_Key;
	};

	//////////////////////////////////////////////////////////////////////////
	//@ C_Shortcut
	template<class T>
	class C_Shortcut : public I_Shortcut
	{
	public:
		typedef void (T::*T_MemberFunc)();
		//@ c-tor
		C_Shortcut(T * obj, T_MemberFunc func, sysutils::C_String description, E_KeyboardModifier modifier, E_KeyboardLayout key) 
			: I_Shortcut(description, modifier, key), m_Object(obj), m_Function(func) {};
		
		//@ Execute			
		virtual void		Execute()
		{
			SC_ASSERT(m_Object);
			(m_Object->*m_Function)();
		}

	private:
		T							*	m_Object;
		T_MemberFunc				m_Function;
	};

	typedef boost::shared_ptr<I_Shortcut>	T_Shortcut;
}}