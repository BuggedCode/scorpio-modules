/*
* filename:			C_ShortcutManager.h
*
* author:			Kral Jozef
* date:				9/19/2011		21:30
* version:			1.00
* brief:
*/

#pragma once

#include <GameFramework/GameFrameworkApi.h>
#include <Common/C_Singleton.h>
#include <map>
#include <GameFramework/Core/C_Shortcut.h>
#include <GameFramework/DbgUtils/C_DebugWindow.h>

namespace scorpio{ namespace framework{


	//////////////////////////////////////////////////////////////////////////
	//@ C_ShortcutManager
	class GAMEFRAMEWORK_API C_ShortcutManager
	{
		friend class C_Singleton<C_ShortcutManager>;
	public:
		//@ RegisterShortcut
		bool					RegisterShortcut(T_Shortcut shortcut);
		//@ UnregisterShortcut - not so safety (can add context for checking)
		bool					UnregisterShortcut(E_KeyboardModifier modifier, E_KeyboardLayout key);

		//@ CreateShortcut
		//@ obj - instance of context
		//@ func - pointer to member method of class T
		template<class T, typename Func>
		static T_Shortcut	CreateShortcut(T * obj, Func func, sysutils::C_String description, E_KeyboardModifier modifier, E_KeyboardLayout key)
		{
			T_Shortcut shortcut = T_Shortcut(new C_Shortcut<T>(obj, func, description, modifier, key));
			return shortcut;
		}

		//@ ProcessShortcut
		void					ProcessShortcut(E_KeyboardModifier keyModifier, E_KeyboardLayout key);

		//@ AttachWindow
		void					AttachWindow(T_DebugWindow window);
		//@ DetachWindow
		void					DetachWindow(T_DebugWindow window) { m_Window.reset(); }

	private:
		//@ GenerateHash
		static T_Hash32	GenerateHash(E_KeyboardModifier modifier, E_KeyboardLayout key);

	private:
		//@ d-tor
		~C_ShortcutManager();


	private:
		typedef std::map<T_Hash32, T_Shortcut>	T_ShortcutMap;
		T_ShortcutMap		m_ShortcutMap;
		T_DebugWindow		m_Window;
	};

	typedef C_Singleton<C_ShortcutManager>	T_ShortcutManager;
}}

GAMEFRAMEWORK_EXP_TEMP_INST template class GAMEFRAMEWORK_API C_Singleton<scorpio::framework::C_ShortcutManager>;